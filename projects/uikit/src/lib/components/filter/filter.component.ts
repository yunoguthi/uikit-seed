import {
  OnInit,
  OnDestroy,
  AfterViewInit,
  ContentChild, ViewChild, EventEmitter, Output, Component, Input, TemplateRef
} from '@angular/core';
import {Subscription} from 'rxjs';
import {FormGroupDirective} from '@angular/forms';
import {MatDialog, MatDialogRef, MatInput} from '@angular/material';
import {Hotkey, HotkeysService} from 'angular2-hotkeys';
import {FilterService} from './filter.service';

@Component({
  selector: 'uikit-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss'],
})
export class FilterComponent implements OnInit, AfterViewInit, OnDestroy {

  @Output() clickPesquisa: EventEmitter<any> = new EventEmitter();
  @Input() disabled: boolean;
  @ViewChild('search') searchInput: MatInput;
  @ViewChild('filterDialog') template: TemplateRef<{}>;
  @ContentChild(FormGroupDirective) filtersForm: FormGroupDirective;
  public filterBadge$ = this.filterService.filterBadge$;
  public showFilters$ = this.filterService.showFilters$;
  protected subscriptions: Subscription[] = [];
  dialogRef: MatDialogRef<any>;

  constructor(protected hotkeysService: HotkeysService,
              protected filterService: FilterService,
              public dialog: MatDialog) {
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.hotkeysService.add(new Hotkey('alt+3', (event: KeyboardEvent): boolean => {
      this.searchInput.focus();
      return false;
    }));

    this.hotkeysService.add(new Hotkey('ctrl+alt+3', (event: KeyboardEvent): boolean => {
      this.toggleFilters();
      return false;
    }));
  }

  ngOnDestroy(): void {
    if (this.subscriptions) {
      this.subscriptions.forEach(subscription => {
        if (subscription && subscription.closed === false) {
          subscription.unsubscribe();
        }
      });
    }
  }

  pesquisar() {
    this.clickPesquisa.emit(this.searchInput.value);
    this.clickPesquisa.subscribe(() => {
      this.searchInput.focus();
    });
  }

  toggleFilters() {
    this.dialogRef = this.dialog.open(this.template);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
