import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({providedIn: 'root'})
export class FilterService {

  protected filterBadgeSubject = new BehaviorSubject<number>(0);
  public filterBadge$ = this.filterBadgeSubject.asObservable();
  protected showFiltersSubject = new BehaviorSubject<boolean>(false);
  public showFilters$ = this.showFiltersSubject.asObservable();

  constructor() {
  }

  setCountFilter(count: number) {
    this.filterBadgeSubject.next(count);
  }

  setShowFilters() {
    this.showFiltersSubject.next(!this.showFiltersSubject.getValue());
  }
}
