import { Component, OnInit, Input, ViewChild, AfterViewInit, ElementRef, Renderer2, Output, EventEmitter } from '@angular/core';
import { Location } from '@angular/common';
import { MatButton } from '@angular/material';

export enum SaveButtonSelection {
  Salvar = 'Salvar',
  SalvarVoltar = 'Salvar e voltar',
  SalvarNovo = 'Salvar e novo',
}

@Component({
  selector: 'uikit-save-button',
  templateUrl: './save-button.component.html',
  styleUrls: ['./save-button.component.scss']
})
export class SaveButtonComponent implements AfterViewInit {

  // @ViewChild(MatButton) saveButton: MatButton;
  // @ViewChild('uikitSubmitButton', { read: ElementRef }) protected submitButton: ElementRef;
  // @ViewChild('uikitResetButton', { read: ElementRef }) protected resetButton: ElementRef;

  /** Use the parent form to save (using submit), new (reset) and back (history) */
  // @Input() automaticTrigger = false;

  @Output() save = new EventEmitter<SaveButtonSelection>();
  // @Output() clickAtSave = new EventEmitter<SaveButtonSelection>();
  // @Output() clickAtSaveAndBack = new EventEmitter<SaveButtonSelection>();
  // @Output() clickAtSaveAndNew = new EventEmitter<SaveButtonSelection>();

  @ViewChild('uikitSubmitButton') protected submitButton: MatButton;
  @ViewChild('uikitResetButton') protected resetButton: MatButton;

  @Input() public selection: SaveButtonSelection = SaveButtonSelection.Salvar;

  @Input() public disabled = false;

  constructor(
    protected renderer: Renderer2,
    protected ngLocation: Location,
  ) { }

  ngAfterViewInit() {



  }

  saveAction() {
    console.log(SaveButtonSelection.Salvar);
    this.selection = SaveButtonSelection.Salvar;
    // if (this.automaticTrigger) {
    //   this.action();
    // }
    this.save.emit(SaveButtonSelection.Salvar);
  }

  saveBackAction() {
    console.log(SaveButtonSelection.SalvarVoltar);
    this.selection = SaveButtonSelection.SalvarVoltar;
    // if (this.automaticTrigger) {
    //   this.action();
    // }
    this.save.emit(SaveButtonSelection.SalvarVoltar);
  }

  saveNewAction() {
    console.log(SaveButtonSelection.SalvarNovo);
    this.selection = SaveButtonSelection.SalvarNovo;
    // if (this.automaticTrigger) {
    //   this.action();
    // }
    this.save.emit(SaveButtonSelection.SalvarNovo);
  }

  // submit() {
  //   const saveButtonElement = this.saveButton.nativeElement as HTMLButtonElement;
  //   this.renderer.setAttribute(saveButtonElement, `type`, `submit`);
  //   saveButtonElement.click();
  //   if (this.selection === SaveButtonSelection.SalvarNovo) {

  //   }
  // }

  action() {
    if (this.selection === SaveButtonSelection.Salvar) {
      return this.saveAction();
    } else if (this.selection === SaveButtonSelection.SalvarNovo) {
      return this.saveNewAction();
    } else if (this.selection === SaveButtonSelection.SalvarVoltar) {
      return this.saveBackAction();
    }
  }

  // action() {
  //   const submitButtonElementRef = this.submitButton._elementRef;
  //   const submitButtonElement = submitButtonElementRef.nativeElement as HTMLButtonElement;
  //   submitButtonElement.click();
  //   if (this.selection === SaveButtonSelection.SalvarNovo) {
  //     const resetButtonElementRef = this.resetButton._elementRef;
  //     const resetButtonElement = resetButtonElementRef.nativeElement as HTMLButtonElement;
  //     resetButtonElement.click();
  //   } else if (this.selection === SaveButtonSelection.SalvarVoltar) {
  //     this.ngLocation.back();
  //   }
  // }

}
