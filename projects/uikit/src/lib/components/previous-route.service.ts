import {Injectable} from '@angular/core';
import {NavigationStart, Router, RouterEvent, NavigationEnd} from '@angular/router';
import {PlatformLocation} from '@angular/common';
import { BehaviorSubject } from 'rxjs';
import { filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PreviousRouteService {

  private history = [];

  // protected previousUrlSubject = new BehaviorSubject<string>(null);
  // public previousUrl$ = this.previousUrlSubject.asObservable().pipe(distinctUntilChanged());
  // public previousUrl = null;

  // protected currentUrlSubject = new BehaviorSubject<string>(null);
  // public currentUrl$ = this.currentUrlSubject.asObservable().pipe(distinctUntilChanged());
  // public currentUrl = null;

  public desabilitarBotaoVoltar: BehaviorSubject<boolean>;
  private rotasAcessadas = [];

  constructor(private router: Router) {
    console.log('constructor PreviousRouteService');

    this.desabilitarBotaoVoltar = new BehaviorSubject(true);

    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(({ urlAfterRedirects }: NavigationEnd) => {
        this.history = [ ...this.history, urlAfterRedirects ];

        this.desabilitarBotaoVoltar.next(this.rotasAcessadas.length === 0);
        this.rotasAcessadas = [ ...this.rotasAcessadas, urlAfterRedirects ];
      });

    // this.router.events
    //   .pipe(filter((event: RouterEvent) => event instanceof NavigationEnd))
    //   .subscribe((event: NavigationEnd) => {
    //     console.log(`prev -> ${this.currentUrl}`);
    //     console.log(`curr -> ${event.urlAfterRedirects}`);
    //     this.previousUrl.next(this.currentUrl);
    //     this.currentUrl = event.urlAfterRedirects;
    //   });

    // this.previousUrl$.subscribe((previousUrl) => {
    //   this.previousUrl = previousUrl;
    // });
    // this.currentUrl$.subscribe((currentUrl) => {
    //   this.currentUrl = currentUrl;
    // });
    // this.currentUrlSubject.next(this.router.url);
    // router.events.subscribe(event => {
    //   if (event instanceof NavigationEnd) {
    //     this.previousUrlSubject.next(this.currentUrlSubject.getValue());
    //     this.currentUrlSubject.next(event.url);
    //   }
    // });
  }

  public getHistory(): string[] {
    return this.history;
  }

  public getPreviousUrl(): string {
    return this.history[this.history.length - 2];
  }

  public removerRotaAcessada = () => this.rotasAcessadas.splice(this.rotasAcessadas.length - 2, 2);

  // public get getPreviousUrl() {
  //   return this.previousUrl;
  // }

  // public get getCurrentUrl() {
  //   return this.currentUrl;
  // }
}
