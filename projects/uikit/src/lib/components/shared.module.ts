import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SaveButtonComponent} from './save-button/save-button.component';
import {
  MatBadgeModule,
  MatButtonModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule
} from '@angular/material';
import {FilterComponent} from './filter/filter.component';

@NgModule({
  providers: [],
  declarations: [SaveButtonComponent, FilterComponent],
  exports: [
    SaveButtonComponent,
    FilterComponent,
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatFormFieldModule,
    MatInputModule,
    MatBadgeModule,
    MatDialogModule
  ],
  entryComponents: [SaveButtonComponent, FilterComponent]
})
export class UikitComponentsModule {
}
