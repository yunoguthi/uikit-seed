import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ID } from '@datorama/akita';

export enum DialogResult {
  Confirmed = 'Confirmado',
  Cancelled = 'Cancelado'
}

export enum ButtonDialog {
  YesNo,
  OkCancel
}

export interface DialogOptions {
  title: string;
  description: string;
  items?: any[];
  button?: ButtonDialog;
}

// @dynamic
@Component({
  selector: 'uikit-simple-dialog',
  templateUrl: './simple-dialog.component.html',
  styleUrls: ['./simple-dialog.component.scss']
})
export class SimpleDialogComponent implements OnInit {

  columns: string[];

  cancelButtonText = 'Cancelar';
  confirmButtonText = 'Ok';

  constructor(
    public dialogRef: MatDialogRef<any, DialogResult>,
    @Inject(MAT_DIALOG_DATA) public data: DialogOptions
  ) {
    if (data.button === ButtonDialog.YesNo) {
      this.confirmButtonText = 'Sim';
      this.cancelButtonText = 'Não';
    }
    if (data.items && data.items.length > 0) {
      this.columns = Object.keys(data.items[0]);
    }
  }

  onCancelClick(): void {
    this.dialogRef.close(DialogResult.Cancelled);
  }

  onConfirmClick(): void {
    this.dialogRef.close(DialogResult.Confirmed);
  }


  ngOnInit() {

  }

}
