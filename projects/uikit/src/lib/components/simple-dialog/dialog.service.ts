import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { SimpleDialogComponent, DialogResult, DialogOptions } from './simple-dialog.component';
import { ID } from '@datorama/akita';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  constructor(protected dialog: MatDialog) { }

  show(description: string, items?: any[], title: string = 'Confirmação', width: string = '450px') {
    const dialogRef = this.dialog.open<SimpleDialogComponent, DialogOptions, DialogResult>(SimpleDialogComponent, {
      width: '450px',
      data: {
        title: title,
        description: description,
        items: items
      }
    });

    return dialogRef.afterClosed();

  }
}
