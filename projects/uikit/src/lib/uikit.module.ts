import { NgModule, Optional, SkipSelf } from '@angular/core';
import { LayoutModule } from './layout/layout.module';
import { MatPaginatorIntl } from '@angular/material';
import { MatPaginatorIntlPtBr } from './utils/mat-paginator-ptbr';
import { OperationsModule } from './operations/operations.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { OfflineInterceptor } from './offline/offline-interceptor';

@NgModule({
  entryComponents: [],
  declarations: [],
  imports: [
    // OperationsModule,
  ],
  exports: [
    // OperationsModule,
    LayoutModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: OfflineInterceptor, multi: true },
    { provide: MatPaginatorIntl, useClass: MatPaginatorIntlPtBr }
  ],

})
export class UikitModule {
  constructor(@Optional() @SkipSelf() parentModule: UikitModule) {
    if (parentModule) {
      throw new Error('UikitModule já foi carregado! Importe ele somente no AppModule.');
    }
  }
}
