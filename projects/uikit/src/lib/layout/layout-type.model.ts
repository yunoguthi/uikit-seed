export enum LayoutType {
  normal,
  fullscreen,
  noshell,
  noshellfullscreen,
  noshellnobreadcrumb,
  noshellnobreadcrumbfullscreen
}

export interface FsDocumentElement extends HTMLElement {
  msRequestFullscreen?: () => void;
  mozRequestFullScreen?: () => void;
}
