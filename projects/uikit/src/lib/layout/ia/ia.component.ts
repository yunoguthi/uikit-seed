import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { NavQuery } from './../nav/state/nav.query';
import { Subscription } from 'rxjs';
import { NavService } from './../nav/state/nav.service';

@Component({
  selector: 'uikit-ia',
  templateUrl: './ia.component.html',
  styleUrls: ['./ia.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IaComponent implements OnInit, OnDestroy {

  public rightnav$ = this.navQuery.rightnav$;
  public rightnav: { opened: boolean; pinned: boolean };
  protected subscription = new Subscription();

  constructor(
    protected navQuery: NavQuery,
    protected navService: NavService) { }

  ngOnInit() {
    this.subscription.add(
      this.rightnav$.subscribe(rightnav => {
        this.rightnav = rightnav;
      })
    );
  }


  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  rightNavToggle() {
    this.navService.toggleRightNav();
  }
  rightNavOpen() {
    this.navService.openRightNav();
  }
  rightNavClose() {
    this.navService.closeRightNav();
  }
  togglePinRightNav() {
    this.navService.togglePinRightNav();
  }

}
