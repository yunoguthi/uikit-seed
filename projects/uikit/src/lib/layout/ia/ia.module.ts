import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IaComponent } from './ia.component';
import { MatIconModule, MatToolbarModule, MatFormFieldModule } from '@angular/material';

@NgModule({
  declarations: [IaComponent],
  imports: [
    CommonModule,
    MatIconModule,
    MatToolbarModule,
    MatFormFieldModule
  ],
  exports: [IaComponent]
})
export class IaModule { }
