import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {LayoutComponent} from './layout.component';
import {HeaderComponent} from './header/header.component';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatButtonModule} from '@angular/material/button';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {MatBadgeModule} from '@angular/material/badge';
import {MatTooltipModule} from '@angular/material/tooltip';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatListModule} from '@angular/material/list';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';

import {NavComponent} from './nav/nav.component';
import {MenuComponent} from './nav/menu/menu.component';
import {FavnavComponent} from './nav/favnav/favnav.component';
import {BreadcrumbComponent} from './nav/breadcrumb/breadcrumb.component';
import {SearchComponent} from './header/search/search.component';
import {NotificationComponent} from './header/notification/notification.component';
import {SysteminfoComponent} from './header/systeminfo/systeminfo.component';
import {AccessibilityComponent} from './header/accessibility/accessibility.component';
import {UserinfoComponent} from './header/userinfo/userinfo.component';
import {MatChipsModule} from '@angular/material/chips';

import {MatSliderModule} from '@angular/material/slider';

import {LayoutService} from './layout.service';
import {FavNavsService} from './nav/favnav/state/favnavs.service';

import {ScrollingModule} from '@angular/cdk/scrolling';
import {CdkStepperModule} from '@angular/cdk/stepper';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatTabsModule} from '@angular/material/tabs';
import {MatSelectModule} from '@angular/material/select';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MenuItemComponent} from './nav/menu/menu-item/menu-item.component';
import {RouterModule} from '@angular/router';

import {HotkeyModule} from 'angular2-hotkeys';
import {MatSnackBarModule, MatTableModule, MatTreeModule} from '@angular/material';
import {NotificacaoComponent} from './header/notification/notificacao/notificacao.component';
import {SimpleDialogComponent} from '../components/simple-dialog/simple-dialog.component';
import {DialogService} from '../components/simple-dialog/dialog.service';
import {IaComponent} from './ia/ia.component';
import {BreadcrumbModule} from './nav/breadcrumb/breadcrumbs/breadcrumb.module';
import {UikitSharedModule} from '../shared/shared.module';

import {ToastModule} from './toast/toast.module';
import {PreviousRouteService} from '../components/previous-route.service';
import {HighlightComponent} from './nav/highlight/highlight.component';
import {MenuSearchComponent} from "./nav/menu-search/menu-search.component";
import {UikitRrippleService} from "../utils/uikit-ripple.service";

@NgModule({
  providers: [
    LayoutService,
    FavNavsService,
    DialogService,
    PreviousRouteService,
    UikitRrippleService
  ],
  declarations: [
    LayoutComponent,
    HeaderComponent,
    NavComponent,
    MenuComponent,
    FavnavComponent,
    SearchComponent,
    HighlightComponent,
    NotificationComponent,
    SysteminfoComponent,
    AccessibilityComponent,
    UserinfoComponent,
    BreadcrumbComponent,
    MenuItemComponent,
    NotificacaoComponent,
    SimpleDialogComponent,
    BreadcrumbComponent,
    MenuSearchComponent,
    IaComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    HotkeyModule.forRoot(),
    MatSidenavModule,
    MatSnackBarModule,
    MatToolbarModule,
    MatButtonModule,
    MatMenuModule,
    MatIconModule,
    MatBadgeModule,
    MatTooltipModule,
    DragDropModule,
    MatExpansionModule,
    MatListModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatInputModule,
    ScrollingModule,
    MatChipsModule,
    MatSliderModule,
    CdkStepperModule,
    MatSlideToggleModule,
    MatTabsModule,
    MatSelectModule,
    MatPaginatorModule,
    MatTableModule,
    MatTreeModule,
    BreadcrumbModule,
    UikitSharedModule,
    ToastModule,
  ],
  exports: [
    MatIconModule,
    MatButtonModule,
    MatSnackBarModule,
    LayoutComponent,
    HeaderComponent,
    NavComponent,
    MenuComponent,
    FavnavComponent,
    MatListModule,
    SearchComponent,
    NotificationComponent,
    SysteminfoComponent,
    AccessibilityComponent,
    UserinfoComponent,
    MenuItemComponent,
    HotkeyModule,
    NotificacaoComponent,
    SimpleDialogComponent,
    BreadcrumbModule,
    MenuSearchComponent,
    ToastModule
  ],
  entryComponents: [
    LayoutComponent,
    HeaderComponent,
    NavComponent,
    MenuComponent,
    FavnavComponent,
    SearchComponent,
    NotificationComponent,
    SysteminfoComponent,
    AccessibilityComponent,
    UserinfoComponent,
    MenuItemComponent,
    NotificacaoComponent,
    SimpleDialogComponent,
    BreadcrumbComponent,
    IaComponent
  ]
})
export class LayoutModule {
  constructor(private previousRouteService: PreviousRouteService) {
  }
}
