import {CdkScrollable, ScrollDispatcher} from '@angular/cdk/overlay';
import {DOCUMENT} from '@angular/common';
import {
  AfterContentInit,
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ContentChildren,
  ElementRef,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChild
} from '@angular/core';
import {MatSidenav} from '@angular/material';
import {ActivatedRoute, NavigationEnd, NavigationStart, Router} from '@angular/router';

import {Hotkey, HotkeysService} from 'angular2-hotkeys';
import {BehaviorSubject, merge, Subscription, timer} from 'rxjs';
import {filter, map} from 'rxjs/operators';
import {UpdateInfoService} from '../update/update-info.service';
import {UpdateService} from '../update/update.service';
import {NotificacaoComponent} from './header/notification/notificacao/notificacao.component';
import {FsDocumentElement, LayoutType} from './layout-type.model';
import {LayoutService} from './layout.service';
import {BreadcrumbService} from './nav/breadcrumb/breadcrumbs/breadcrumb.service';
import {FavNavsQuery} from './nav/favnav/state/favnavs.query';
import {FavNavsService} from './nav/favnav/state/favnavs.service';

import {NavQuery} from './nav/state/nav.query';
import {NavService} from './nav/state/nav.service';
import {ToastService} from './toast/toast.service';
import {FormBuilder} from '@angular/forms';
import {UikitRrippleService} from "../utils/uikit-ripple.service";

import { PreviousRouteService } from '../components/previous-route.service';

@Component({
  selector: 'uikit-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LayoutComponent implements OnInit, OnDestroy, AfterViewInit, AfterContentInit {

  protected hasActionsSubject = new BehaviorSubject<boolean>(false);
  public hasActions$ = this.hasActionsSubject.asObservable();

  @ViewChild('myClickFullscreen') myClickFullscreen: ElementRef<HTMLElement>;

  public showShell = this.layoutService.showShell$;
  public hiddenBreadcrumb = this.layoutService.showBreadcrumb$;
  public showFullScrren = this.layoutService.showFullScreen$;
  public hiddenShell = this.layoutService.hiddenShell$;

  @Input() public showNotifications = false;
  @Input() public showSystemInfo = false;
  @Input() public showUserInfo = false;
  @Input() public showEnvironment = false;
  @Input() public showIa = false;
  @Input() public environment: string = null;
  @Input() public allowedTypes = [
    'normal',
    'fullscreen',
    'noshell',
    'noshellfullscreen',
    'noshellnobreadcrumb',
    'noshellnobreadcrumbfullscreen'
  ];
  @Input() public defaultType = 'normal';
  @Input() public showShortcuts = true;

  protected subscription = new Subscription();

  public isFixed$ = this.layoutService.isFixed$;
  public isMobile$ = this.layoutService.isMobile$;

  public rightnav$ = this.navQuery.rightnav$;
  public rightnav: { opened: boolean; pinned: boolean };
  public leftnav$ = this.navQuery.leftnav$;
  public leftnav: { opened: boolean; pinned: boolean };

  public data$ = new BehaviorSubject<any>(null);

  possuiRecursoDeFavoritos = this.favNavsService.possuiRecursoDeFavoritos;

  private scrollSubscription: Subscription;

  @ContentChildren(NotificacaoComponent, {descendants: false})
  public notificacoesProjetadas: QueryList<NotificacaoComponent>;

  @ViewChild(MatSidenav) sidenav: MatSidenav;

  public notificacoes$ = this.layoutService.notificacoes$;

  private closeWhenNotPinnedLeftSubscription: Subscription;
  private closeWhenNotPinnedRightSubscription: Subscription;

  public hasInstallOption$ = this.updateInfoService.hasInstallOption$;
  public hasUpdate$ = this.updateInfoService.hasUpdate$;
  public favorites$ = this.favNavsQuery.favorites$;

  constructor(
    protected navQuery: NavQuery,
    protected favNavsQuery: FavNavsQuery,
    protected navService: NavService,
    public scroll: ScrollDispatcher,
    protected layoutService: LayoutService,
    protected router: Router,
    protected activatedRoute: ActivatedRoute,
    protected updateInfoService: UpdateInfoService,
    protected updateService: UpdateService,
    protected favNavsService: FavNavsService,
    protected hotkeysService: HotkeysService,
    protected breadcrumbService: BreadcrumbService,
    protected toastService: ToastService,
    protected uikitRrippleService: UikitRrippleService,
    private fb: FormBuilder,
    @Inject(DOCUMENT) private document: any,
    protected previousRouteService: PreviousRouteService,
  ) {
  }

  ngAfterContentInit(): void {

    this.notificacoesProjetadas.changes.subscribe(() => {
      const notificacoesModel = this.notificacoesProjetadas.toArray().map(r => {
        return {
          nome: r.nome,
          icone: r.icone,
          descricao: r.descricao,
          data: r.data,
          read: r.read
        };
      });
      this.layoutService.setNotificacoes(notificacoesModel);
    });
    this.notificacoesProjetadas.notifyOnChanges();
  }

  ngOnInit() {
    this.uikitRrippleService.init();

    this.subscription.add(
      this.leftnav$.subscribe(leftnav => {
        this.leftnav = leftnav;
      })
    );

    this.subscription.add(
      (this.closeWhenNotPinnedLeftSubscription = this.router.events
        .pipe(filter(event => event instanceof NavigationStart))
        .subscribe(event => {
          if (this.leftnav.pinned === false) {
            this.navService.closeLeftNav();
          }
        }))
    );

    this.subscription.add(
      this.rightnav$.subscribe(rightnav => {
        this.rightnav = rightnav;
      })
    );

    this.subscription.add(
      (this.closeWhenNotPinnedRightSubscription = this.router.events
        .pipe(filter(event => event instanceof NavigationStart))
        .subscribe(event => {
          if (this.rightnav.pinned === false) {
            this.navService.closeRightNav();
          }
        }))
    );

    const nagivate = this.router.events.pipe(filter(event => event instanceof NavigationEnd));
    const params = this.activatedRoute.params;

    merge(nagivate, params).pipe(
      map(() => {
        let child = this.activatedRoute.firstChild;
        while (child) {
          if (child.firstChild) {
            child = child.firstChild;
          } else if (child.snapshot.data) {
            return child.snapshot.data;
          } else {
            return null;
          }
        }
        return null;
      }))
      .subscribe(customData => {
        let route = this.router.routerState.root.snapshot;
        while (route.firstChild != null) {
          route = route.firstChild;
        }
        const data = route.data;


        const favItem = this.getFavItem(data);
        // console.log('layout - nagivate, params', favItem);
        // favItem.icon = customData ? customData.icon : null;
        this.data$.next(favItem);
      });

    // When connection become offline, then put the entire app with grayscale (0.8)
    window.addEventListener('online', () => {
      document.querySelector('body').style.filter = '';
      if (localStorage.getItem('offline') === 'true') {
        timer(600).subscribe(() => this.toastService.success('Conexão de rede restabelecida!'));
        localStorage.removeItem('offline');
      }
    });
    window.addEventListener('offline', () => {
      document.querySelector('body').style.filter = 'grayscale(0.8)';
      localStorage.setItem('offline', 'true');
      timer(600).subscribe(() => this.toastService.warning('Sem conexão de rede!'));
    });

    this.subscription.add(
      (this.scrollSubscription = this.scroll
        .scrolled()
        .pipe(
          map(
            (data: CdkScrollable) => {
              if (data) {
                const element = data.getElementRef();
                if (element) {
                  return element.nativeElement.scrollTop;
                }
              }
            }
          )
        )
        .subscribe((scrollY: number) => {
          if (scrollY) {
            this.layoutService.setFixed(scrollY > 0);
          }
        }))
    );

    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(() => {
        this.setLayoutTypeByRouteEvent();
        this.updateIfHasActions();
      });
    setTimeout(() => {
      this.updateIfHasActions();
    }, 0);
  }

  toggleMobile() {
    this.layoutService.toggleMobile();
  }

  updateIfHasActions() {
    const elements = document.getElementsByClassName('uikit-actions');
    const element = elements.item(0);
    if (element != null) {
      const hasChildNodes = element.hasChildNodes();
      this.hasActionsSubject.next(hasChildNodes);
    } else {
      this.hasActionsSubject.next(false);
    }
  }

  ngAfterViewInit() {

    this.hotkeysService.add(new Hotkey('ctrl+m', (event: KeyboardEvent): boolean => {
      this.leftNavToggle();
      return false;
    }));
    this.hotkeysService.add(new Hotkey('ctrl+alt+j', (event: KeyboardEvent): boolean => {
      this.rightNavToggle();
      return false;
    }));

    this.isVerifyTypeShellApply();
  }

  private getFavItem(customData) {
    const data = customData;
    const lastBreadCrumb = this.breadcrumbService.breadcrumbs[this.breadcrumbService.breadcrumbs.length - 1];
    if (lastBreadCrumb != null) {
      const title = lastBreadCrumb.title; // this.breadcrumbService.getTitleFormatted(data.breadcrumb, this.activatedRoute.snapshot);
      return {link: this.router.url, title, icon: data ? data.icon : null};
    }
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  leftNavToggle() {
    this.navService.toggleLeftNav();
  }

  leftNavClose() {
    this.navService.closeLeftNav();
  }

  rightNavToggle() {
    this.navService.toggleRightNav();
  }

  rightNavClose() {
    this.navService.closeRightNav();
  }

  navsClose() {
    if (this.leftnav.opened && !this.leftnav.pinned) {
      this.navService.toggleLeftNav();
    }

    if (this.rightnav.opened && !this.rightnav.pinned) {
      this.navService.toggleRightNav();
    }
  }

  install() {
    this.updateInfoService.install();
  }

  update() {
    this.updateService.update();
  }

  public isVerifyTypeShellApply() {

    this.showFullScrren.subscribe((isShowFullScreen: boolean) => {
      if (isShowFullScreen) {
        const fsDocElem = this.document.documentElement as FsDocumentElement;
        // document.documentElement as FsDocumentElement;
        this.openFullscreen(fsDocElem);
      }
    });

  }

  public openFullscreen(fsDocElem: any): void {
    /*debugger;*/
    if (fsDocElem.requestFullscreen) {
      timer(600).subscribe(() => {
        fsDocElem.requestFullscreen().catch(error => console.error(error));
      });
    } else if (fsDocElem.msRequestFullscreen) {
      try {
        timer(600).subscribe(() => {
          fsDocElem.msRequestFullscreen().catch(error => console.error(error));
        });
      } catch (error) {
        console.error(error);
      }
    } else if (fsDocElem.mozRequestFullScreen) {
      try {
        timer(600).subscribe(() => {
          fsDocElem.mozRequestFullScreen().catch(error => console.error(error));
        });
      } catch (error) {
        console.error(error);
      }
    } else if (fsDocElem.webkitRequestFullscreen) {
      try {
        timer(600).subscribe(() => {
          fsDocElem.webkitRequestFullscreen().catch(error => console.error(error));
        });
      } catch (error) {
        console.error(error);
      }
    }
    timer(900).subscribe(() => {
      (this.myClickFullscreen.nativeElement as HTMLElement).click();
    });
  }

  private closeFullscreen(): void {
    if (this.document.exitFullscreen) {
      this.document.exitFullscreen();
    } else if (this.document.mozCancelFullScreen) {
      /* Firefox */
      this.document.mozCancelFullScreen();
    } else if (this.document.webkitExitFullscreen) {
      /* Chrome, Safari and Opera */
      this.document.webkitExitFullscreen();
    } else if (this.document.msExitFullscreen) {
      /* IE/Edge */
      this.document.msExitFullscreen();
    }
  }

  /*** Region RouteLayoutApplication
   * Modo de apresentação das tela aplicado de forma totalmente configurável a forma geral e por rota.
   * ***/
  public setLayoutTypeByRouteEvent() {
    const queryType = this.router.routerState.root.snapshot.queryParamMap.get('type');
    const dataRota = this.getData();

    let typeDesejado = 'normal';
    if (queryType) {
      // usuario informou via url (tem prioridade sobre todos)
      typeDesejado = queryType;
    } else if (dataRota && dataRota.defaultType) {
      // esta configurado na rota (tem prioridade somente sobre o global - sobrescrita)
      typeDesejado = dataRota.defaultType;
    } else {
      typeDesejado = this.defaultType;
      // busca o global
    }

    if (typeDesejado === 'noshell') {
      this.navService.toggleLeftNav();
    }

    const defaultTypeRoute = (dataRota && dataRota.defaultType) ? dataRota.defaultType : this.defaultType;
    if (dataRota && dataRota.allowedTypes && dataRota.allowedTypes.length > 0) {
      // existe configuracao de tipos permitidos na rota
      const ehPermitidoPelaRota = dataRota.allowedTypes.some(type => type === typeDesejado);
      if (ehPermitidoPelaRota) {
        this.layoutService.setType(LayoutType[typeDesejado]);
      } else {
        console.error(`O parametro ${typeDesejado} não está definido como permitido, favor verificar!`);
        this.layoutService.setType(LayoutType[defaultTypeRoute]);
      }
    } else {
      // não existe configuração de tipos permitidos na rota devo então ver do componente layout (global da aplicação)
      const allowedTypesLayout = this.allowedTypes;
      const ehPermitidoGlobalmente = allowedTypesLayout.some(type => type === typeDesejado);
      if (ehPermitidoGlobalmente) {
        this.layoutService.setType(LayoutType[typeDesejado]);
      } else {
        console.error(`O parametro ${typeDesejado} não está definido como permitido, favor verificar!`);
        this.layoutService.setType(LayoutType[defaultTypeRoute]);
      }
    }
    if ((typeDesejado === 'normal' || defaultTypeRoute === 'normal') && this.document.fullscreenElement) {
      this.closeFullscreen();
    }
  }

  private getData() {
    let route = this.router.routerState.root.snapshot;
    while (route.firstChild != null) {
      route = route.firstChild;
      if (route.routeConfig === null) {
        continue;
      }
      if (!route.routeConfig.path) {
        continue;
      }
      if (!route.data.defaultType) {
        continue;
      }
      return route.data as { defaultType: string, allowedTypes: string[] };
    }
  }

  /*** EndRegion RouteLayoutApplication ***/
}
