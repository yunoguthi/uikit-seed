import {AfterContentInit, Component, ContentChildren, Input, OnDestroy, OnInit, QueryList} from '@angular/core';
import {Router} from '@angular/router';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';
import {FavNavsService} from '../../favnav/state/favnavs.service';
import {FavNavsQuery} from '../../favnav/state/favnavs.query';
import {MenuItem} from './menu-item.model';
import {NestedTreeControl} from '@angular/cdk/tree';
import {map} from 'rxjs/operators';
import {MenuSearchService} from '../../menu-search/menu-search.service';

@Component({
  selector: 'uikit-menu-item',
  templateUrl: './menu-item.component.html',
  styles: []
})
export class MenuItemComponent implements OnInit, AfterContentInit, OnDestroy {
  @Input() public id: string;
  @Input() public icon?: string;
  @Input() public link?: any;
  @Input() public title: string;
  @Input() public tags?: string[];
  @ContentChildren(MenuItemComponent, {descendants: false}) children: QueryList<MenuItemComponent>;

  constructor(
    private router: Router,
    private favNavsService: FavNavsService,
    private favNavsQuery: FavNavsQuery,
    protected menuSearchService: MenuSearchService,
  ) {
  }

  public hasItems$ = new BehaviorSubject<boolean>(false);

  protected childrenSubscription: Subscription;
  protected isFavoritedSubscription: Subscription;

  public isFavorited$: Observable<boolean>;
  public isFavorited = false;
  public treeControlMobile = new NestedTreeControl<MenuItem>(node => node.children);
  public possuiRecursoDeFavoritos = this.favNavsService.possuiRecursoDeFavoritos;
  public treeControl = new NestedTreeControl<MenuItem>(node => node.children);
  public favorites$ = this.favNavsQuery.favorites$;
  public favoritesMobile$;
  public menuItems$ = this.menuSearchService.menuItems$;

  protected menuItem;

  public hasChild = (_: number, node: MenuItem) => !!node.children && node.children.length > 0;

  ngOnInit() {
    const menuItem: MenuItem = {
      id: this.id,
      title: this.title,
      link: this.link,
      icon: this.icon,
      tags: this.tags
    };
    this.menuItem = menuItem;
    this.isFavorited$ = this.favNavsQuery.isFavorited(menuItem);
    this.isFavorited$.subscribe(r => this.isFavorited = r);

    this.favoritesMobile$ = this.favorites$.pipe(map((itens: Array<MenuItem>) => {
      const favoritesMobile: Array<MenuItem> = [{title: 'Meus Favoritos', icon: 'fas fa-star', children: itens} as any];
      return favoritesMobile;
    }));
  }

  protected updateHasItems() {
    this.hasItems$.next(this.children.length - 1 > 0);
  }

  ngAfterContentInit(): void {
    this.updateHasItems();
  }

  ngOnDestroy(): void {
    if (this.childrenSubscription && this.childrenSubscription.closed === false) {
      this.childrenSubscription.unsubscribe();
    }
    if (this.isFavoritedSubscription && this.isFavoritedSubscription.closed === false) {
      this.isFavoritedSubscription.unsubscribe();
    }
  }

  public getIsFavorited(menu: MenuItem) {
    const item = {link: menu.link, title: menu.title, icon: menu.icon};
    return this.favNavsQuery.getIsFavorited(item as MenuItem);
  }

  public toggleFavorito(menu: MenuItem) {
    const item = {link: menu.link, title: menu.title, icon: menu.icon};
    this.favNavsService.toggleItem(item as MenuItem);
  }
}
