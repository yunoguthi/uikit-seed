import { ID, guid } from '@datorama/akita';

export interface MenuItem {
  id: ID;
  children?: MenuItem[];
  title: string;
  icon?: string;
  link?: any;
  tags?: string[];
  nodeId?: ID;
}

export function createItem(item: MenuItem) {
  return {
    ...item,
    id: guid()
  } as MenuItem;
}
