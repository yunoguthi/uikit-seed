import { Injectable } from '@angular/core';
import { NavQuery } from '../../state/nav.query';

@Injectable({
  providedIn: 'root'
})
export class MenuItemService {

  public hasFavoritosService = this.navQuery.hasFavoritosService;

  constructor(protected navQuery: NavQuery) { }
}
