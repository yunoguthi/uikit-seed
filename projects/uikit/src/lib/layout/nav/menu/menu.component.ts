import {
  AfterContentInit,
  ChangeDetectionStrategy,
  Component,
  ContentChildren,
  Input,
  OnInit,
  QueryList
} from '@angular/core';
import {MatExpansionPanel} from '@angular/material';
import {FavNavsQuery} from '../favnav/state/favnavs.query';
import {Observable} from 'rxjs';
import {MenuItem} from './menu-item/menu-item.model';
import {LayoutService} from '../../layout.service';
import {MenuItemComponent} from './menu-item/menu-item.component';
import {guid} from '@datorama/akita';
import {map} from 'rxjs/operators';

@Component({
  selector: 'uikit-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  viewProviders: [MatExpansionPanel]
})
export class MenuComponent implements OnInit, AfterContentInit {

  @Input() itensRecursivo: Observable<MenuItem[]>;

  @ContentChildren(MenuItemComponent, {descendants: false})
  private children: QueryList<MenuItemComponent>;
  public favorites$ = this.favNavsQuery.favorites$;
  public favoritesMobile$;

  constructor(
    protected favNavsQuery: FavNavsQuery,
    protected layoutService: LayoutService, ) {
  }

  ngOnInit() {
    this.favoritesMobile$ = this.favorites$.pipe(map((itens: Array<MenuItem>) => {
      const favoritesMobile: Array<MenuItem> = [{title: 'Meus Favoritos', icon: 'fas fa-star', children: itens} as any];
      return favoritesMobile;
    }));
  }

  ngAfterContentInit(): void {
    const functionMap = (r: MenuItemComponent) => {
      const menuItem = {
        id: r.id,
        title: r.title,
        link: r.link,
        icon: r.icon,
        tags: r.tags
      } as MenuItem;

      if (r.children.length > 1) {
        if (!r.id) {
          r.id = guid();
        }
        menuItem.nodeId = r.id;
        menuItem.children = r.children.filter(b => b !== r).map(functionMap);
      }

      return menuItem;
    };

    const menuItems = this.children.toArray().map(functionMap);

    if (this.itensRecursivo) {
      this.itensRecursivo.subscribe((c) => menuItems.push(...c));
    }

    if (menuItems && menuItems.length > 0) {
      this.layoutService.setMenuItems(menuItems);
    }
  }
}
