import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output, QueryList,
  ViewChild, ViewChildren
} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MenuSearchService} from './menu-search.service';
import {Hotkey, HotkeysService} from 'angular2-hotkeys';
import {MatInput} from '@angular/material/input';
import {NavService} from '../state/nav.service';
import {DOWN_ARROW, ENTER, ESCAPE, UP_ARROW} from '@angular/cdk/keycodes';
import {HighlightComponent} from '../highlight/highlight.component';
import {ActiveDescendantKeyManager} from '@angular/cdk/a11y';
import {map} from 'rxjs/operators';
import {MenuItem} from '../menu/menu-item/menu-item.model';
import {NestedTreeControl} from '@angular/cdk/tree';
import {Router} from '@angular/router';

@Component({
  selector: 'uikit-menu-search',
  templateUrl: './menu-search.component.html',
  styleUrls: ['./menu-search.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuSearchComponent implements OnInit, AfterViewInit {
  @Input() placeholderText = document.body.getBoundingClientRect().width > 970 ? 'Pesquisa (Ctrl + Alt + 3)' : 'Pesquisa';
  @Output() eventSearch: EventEmitter<any> = new EventEmitter();
  @ViewChild('searchOpen') searchOpen: MatInput;

  @ViewChildren(HighlightComponent) itensMenu: QueryList<HighlightComponent>;
  private keyManager: ActiveDescendantKeyManager<HighlightComponent>;
  public treeControl = new NestedTreeControl<MenuItem>(node => node.children);
  public menuItems$ = this.menuSearchService.menuItems$;

  stateForm: FormGroup = this.fb.group({
    searchGroups: '',
  });

  constructor(
    private fb: FormBuilder,
    protected menuSearchService: MenuSearchService,
    public hotkeysService: HotkeysService,
    protected navService: NavService,
    protected router: Router,
  ) {
  }

  public ngOnInit(): void {
    this.menuSearchService.buscar('');
    this.stateForm.get('searchGroups').valueChanges.subscribe(value => {
      this.menuSearchService.buscar(value);
    });
  }

  public ngAfterViewInit(): void {
    this.hotkeysService.add(new Hotkey('ctrl+alt+3', (event: KeyboardEvent): boolean => {
      this.navService.toggleLeftNav();
      this.searchOpen.focus();
      return false;
    }));

    if (this.itensMenu) {
      this.keyManager = new ActiveDescendantKeyManager(this.itensMenu).withWrap().withTypeAhead();
    }
  }

  public goToLink(item: MenuItem): void {
    this.router.navigate([item.link]);
  }

  public onKeyup(event: any): void {
    switch (event.keyCode) {
      case ENTER:
        this.goToLink(this.keyManager.activeItem.item);
        break;
      case ESCAPE:
        this.menuSearchService.buscar('');
        this.searchOpen.value = '';
        break;
      case UP_ARROW:
      case DOWN_ARROW:
        this.keyManager.onKeydown(event);
        this.activeItem(this.keyManager.activeItem.item, event.keyCode === UP_ARROW);
        this.expandAllNode();
        break;
    }
  }

  private expandAllNode() {
    this.menuItems$.pipe(map((menuItens: MenuItem[]) => {
      const funcItens = (itens: MenuItem[]) => {
        itens.forEach((item) => {
          if (item.children) {
            this.treeControl.expand(item);
            funcItens(item.children);
          }
        });
      };

      funcItens(menuItens);
    })).subscribe();
  }

  private activeItem(item: MenuItem, up: boolean): void {
    if (item.children && item.children.length > 0) {
      if (up) {
        this.keyManager.setPreviousItemActive();
      } else {
        this.keyManager.setNextItemActive();
      }
      this.activeItem(this.keyManager.activeItem.item, up);
    }
  }
}
