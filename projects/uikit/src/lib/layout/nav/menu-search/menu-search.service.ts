import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {MenuItem} from '../menu/menu-item/menu-item.model';
import {LayoutService} from '../../layout.service';
import * as Fuse from 'fuse.js';
import {map} from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class MenuSearchService {
  private options = {
    keys: ['title', 'tags',
      'children.title', 'children.tags',
      'children.children.title', 'children.children.title.tags',
      'children.children.children.title', 'children.children.children.title.tags']
  };
  private menuItemsSubject$ = new BehaviorSubject<MenuItem[]>([]);
  public menuItems$ = this.menuItemsSubject$.asObservable();

  constructor(protected layoutService: LayoutService) {
  }

  public buscar(value: string) {

    if (!value || value === '') {
      this.layoutService.menuItems$.subscribe((itens) => this.menuItemsSubject$.next(itens));
    } else if (this.menuItems$) {

      let menuItens: MenuItem[] = [];
      this.menuItems$.pipe(map(listMenuItem => this.applyFilter(listMenuItem, value, this.options)))
        .subscribe((itens) => menuItens = this.groupReturnFilter(itens, value));
      this.menuItemsSubject$.next(menuItens);

    } else {

      this.layoutService.menuItems$.pipe(map(listMenuItem => this.applyFilter(listMenuItem, value, this.options)))
        .subscribe((itens) => this.menuItemsSubject$.next(this.groupReturnFilter(itens, value)));
    }
  }

  private applyFilter(listMenuItem: MenuItem[], value: string, options: { keys: string[] }) {
    const fuse = new Fuse(listMenuItem, options);
    return fuse.search(value);
  }

  private groupReturnFilter(itens: MenuItem[], value: string): MenuItem[] {
    const menuItens: MenuItem[] = [];
    const functionCreateItem = (menuItem: MenuItem) => {
      return {
        title: menuItem.title,
        icon: menuItem.icon,
        link: menuItem.link,
        tags: menuItem.tags,
        nodeId: menuItem.nodeId,
      } as MenuItem;
    };

    const functionMapItem = (menuItem: MenuItem) => {
      if (menuItem.children && menuItem.children.length > 0) {
        this.applyFilter(menuItem.children, value, this.options).map(itemIt => functionMapItem(itemIt));
      } else {
        menuItens.push(functionCreateItem(menuItem));
      }
    };

    itens.map((menuItem: MenuItem) => functionMapItem(menuItem));
    return menuItens;
  }
}

