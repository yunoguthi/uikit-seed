import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { NavQuery } from './state/nav.query';
import { NavService } from './state/nav.service';

@Component({
  selector: 'uikit-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavComponent implements OnInit {
  public leftNav$ = this.navQuery.leftnav$;

  public hasFavoritosService = this.navQuery.hasFavoritosService;

  constructor(
    protected navService: NavService,
    protected navQuery: NavQuery,
    ) {}

  ngOnInit() {}

  togglePinLeftNav() {
    this.navService.togglePinLeftNav();
  }
}
