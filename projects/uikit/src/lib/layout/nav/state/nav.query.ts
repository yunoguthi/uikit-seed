import { Injectable, Optional, Inject } from '@angular/core';
import { QueryEntity, Query } from '@datorama/akita';
import { NavStore, NavState } from './nav.store';
import { FavoritosService, FAVORITOS_SERVICE_TOKEN } from '../../../utils/favoritos.service';

@Injectable({
  providedIn: 'root'
})
export class NavQuery extends Query<NavState> {

  public leftnav$ = this.select(session => session.leftnav);
  public rightnav$ = this.select(session => session.rightnav);

  public isLeftPinned$ = this.select(session => session.leftnav.pinned);
  public isRightPinned$ = this.select(session => session.rightnav.pinned);

  public hasFavoritosService = this.favoritosService != null;

  constructor(
    protected store: NavStore,
    @Optional() @Inject(FAVORITOS_SERVICE_TOKEN) protected favoritosService: FavoritosService
    ) {
    super(store);
  }



}
