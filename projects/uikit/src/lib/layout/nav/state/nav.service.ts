import { Injectable } from '@angular/core';
import { NavStore } from './nav.store';

@Injectable({ providedIn: 'root' })
export class NavService {
  constructor(private navStore: NavStore) {}

  toggleLeftNav() {
    this.navStore.update(state => ({
      leftnav: {
        ...state.leftnav,
        opened: !state.leftnav.opened,
        pinned: false
      }
    }));
  }

  openLeftNav() {
    this.navStore.update(state => ({
      leftnav: {
        ...state.leftnav,
        opened: true
      }
    }));
  }

  closeLeftNav() {
    this.navStore.update(state => ({
      leftnav: {
        ...state.leftnav,
        opened: false,
        pinned: false
      }
    }));
  }

  pinLeftNav() {
    this.navStore.update(state => ({
      leftnav: {
        ...state.leftnav,
        pinned: true
      }
    }));
  }

  unpinLeftNav() {
    this.navStore.update(state => ({
      leftnav: {
        ...state.leftnav,
        pinned: false
      }
    }));
  }

  togglePinLeftNav() {
    this.navStore.update(state => ({
      leftnav: {
        ...state.leftnav,
        pinned: !state.leftnav.pinned
      }
    }));
  }










  toggleRightNav() {
    this.navStore.update(state => ({
      rightnav: {
        ...state.rightnav,
        opened: !state.rightnav.opened,
        pinned: false
      }
    }));
  }

  openRightNav() {
    this.navStore.update(state => ({
      rightnav: {
        ...state.rightnav,
        opened: true
      }
    }));
  }

  closeRightNav() {
    this.navStore.update(state => ({
      rightnav: {
        ...state.rightnav,
        opened: false,
        pinned: false
      }
    }));
  }

  pinRightNav() {
    this.navStore.update(state => ({
      rightnav: {
        ...state.rightnav,
        pinned: true
      }
    }));
  }

  unpinRightNav() {
    this.navStore.update(state => ({
      rightnav: {
        ...state.rightnav,
        pinned: false
      }
    }));
  }

  togglePinRightNav() {
    this.navStore.update(state => ({
      rightnav: {
        ...state.rightnav,
        pinned: !state.rightnav.pinned
      }
    }));
  }
}
