import { Injectable } from '@angular/core';
import { EntityState, EntityStore, StoreConfig, Store } from '@datorama/akita';



export interface NavState {
  leftnav: {
    opened: boolean;
    pinned: boolean;
  };
  rightnav: {
    opened: boolean;
    pinned: boolean;
  };
}

export function createInitialState(): NavState {
  return {
    leftnav: {
      opened: false,
      pinned: false
    },
    rightnav: {
      opened: false,
      pinned: false
    }
  };
}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'nav' })
export class NavStore extends Store<NavState> {
  constructor() {
    super(createInitialState());
  }
}
