import { Component, HostBinding, Input } from '@angular/core';
import { Highlightable } from '@angular/cdk/a11y';

@Component({
  selector: 'uikit-highlight',
  template: `
    <li [class.disabled]='disabled'>
      <ng-content></ng-content>
    </li>
  `,
  styles: [],
})
export class HighlightComponent implements Highlightable {
  @Input() item;
  @Input() disabled = false;
  private _isActive = false;

  @HostBinding('class.active') get isActive() {
    return this._isActive;
  }

  setActiveStyles() {
    this._isActive = true;
  }

  setInactiveStyles() {
    this._isActive = false;
  }

  getLabel(): string {
    return '';
  }
}
