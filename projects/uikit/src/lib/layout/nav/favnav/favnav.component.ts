import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { NavQuery } from '../state/nav.query';
import { FavNavsQuery } from './state/favnavs.query';
import { Subscription } from 'rxjs';
import { FavNavsService } from './state/favnavs.service';
import { MenuItem } from '../menu/menu-item/menu-item.model';

@Component({
  selector: 'uikit-favnav',
  templateUrl: './favnav.component.html',
  styleUrls: ['./favnav.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FavnavComponent implements OnInit, OnDestroy {


  public leftNav$ = this.navQuery.leftnav$;

  favorites$ = this.favNavsQuery.favorites$;
  favorites: MenuItem[] = [
    // { title: 'aaa', icon: 'fas fa-info-circle' },
    // { title: 'bbb', icon: 'fas fa-info-circle' },
    // { title: 'ccc', icon: 'fas fa-info-circle' },
    // { title: 'ddd', icon: 'fas fa-info-circle' },
  ];

  favoritesSubscription: Subscription;
  // favorites = [
  //   'fas fa-info-circle',
  //   'fas fa-file-contract',
  //   'fas fa-hard-hat',
  //   'fas fa-plug',
  //   'fas fa-moon',
  //   'fas fa-sms',
  //   'fas fa-comments',
  //   'fas fa-ethernet'
  // ];

  constructor(
    protected navQuery: NavQuery,
    protected favNavsQuery: FavNavsQuery,
    protected favNavsService: FavNavsService
  ) { }

  ngOnInit() {
    this.favoritesSubscription = this.favorites$.subscribe((favs) => {
      const newArrary = favs.slice();
      this.favorites.length = 0;
      this.favorites.push(...newArrary);
    });
  }

  ngOnDestroy(): void {
    if (this.favoritesSubscription && !this.favoritesSubscription.closed) {
      this.favoritesSubscription.unsubscribe();
    }
  }

  drop(event: CdkDragDrop<MenuItem[]>) {
    this.favNavsService.moverItens(event.previousIndex, event.currentIndex);
  }
}
