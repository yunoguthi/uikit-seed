import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FavnavComponent } from './favnav.component';

describe('FavnavComponent', () => {
  let component: FavnavComponent;
  let fixture: ComponentFixture<FavnavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FavnavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavnavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
