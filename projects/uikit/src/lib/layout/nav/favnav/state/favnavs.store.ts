import { Injectable } from '@angular/core';
import { EntityState, EntityStore, StoreConfig, transaction } from '@datorama/akita';
import { MenuItem } from '../../menu/menu-item/menu-item.model';

export interface FavNavsState extends EntityState<MenuItem> { }

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'favnavs' })
export class FavNavsStore extends EntityStore<FavNavsState, MenuItem> {
  constructor() {
    super();
  }
}
