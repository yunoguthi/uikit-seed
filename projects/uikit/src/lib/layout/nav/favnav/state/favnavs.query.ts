import { Injectable } from '@angular/core';
import { ID, QueryEntity, isDefined, toBoolean, isNil } from '@datorama/akita';
import { FavNavsStore, FavNavsState } from './favnavs.store';
import { MenuItem } from '../../menu/menu-item/menu-item.model';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class FavNavsQuery extends QueryEntity<FavNavsState, MenuItem> {
  favorites$ = this.selectAll();

  constructor(
    protected store: FavNavsStore,
    protected router: Router,
    protected ngLocation: Location) {
    super(store);
  }

  isFavorited(menuItem: MenuItem): Observable<boolean> {
    return this.selectAll({
      filterBy: item => {
        const menuItemLink = this.router.createUrlTree(Array.isArray(menuItem.link) ? menuItem.link : [menuItem.link]).toString();
        return item.link === menuItemLink;
      }
    }).pipe(map(value => toBoolean(value)));
  }

  getIsFavorited(menuItem: MenuItem): boolean {
    return this.getAll({
      filterBy: item => {
        const menuItemLink = this.router.createUrlTree(Array.isArray(menuItem.link) ? menuItem.link : [menuItem.link]).toString();
        return item.link === menuItemLink;
      }
    }).length > 0;
  }

  getFavorited(menuItem: MenuItem): MenuItem {
    return this.getAll({
      filterBy: item => {
        const menuItemLink = this.router.createUrlTree(Array.isArray(menuItem.link) ? menuItem.link : [menuItem.link]).toString();
        return item.link === menuItemLink;
      }
    }).filter(item => isDefined(item))[0];
  }
}
