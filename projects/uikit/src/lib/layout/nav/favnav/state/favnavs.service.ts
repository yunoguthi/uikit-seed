import {Inject, Injectable, Optional} from '@angular/core';
import {FavNavsStore} from './favnavs.store';
import {createItem, MenuItem} from '../../menu/menu-item/menu-item.model';
import {FavNavsQuery} from './favnavs.query';
import {moveItemInArray} from '@angular/cdk/drag-drop';
import {FAVORITOS_SERVICE_TOKEN, FavoritosService} from '../../../../utils/favoritos.service';
import {Router} from '@angular/router';
import {isDefined} from '@datorama/akita';
import {UserService} from '../../../../shared/auth/authentication/user.service';


@Injectable({ providedIn: 'root' })
export class FavNavsService {
  possuiRecursoDeFavoritos = this.favoritosService != null;

  constructor(
    private favNavsStore: FavNavsStore,
    private favNavsQuery: FavNavsQuery,
    private router: Router,
    private userService: UserService,
    @Optional() @Inject(FAVORITOS_SERVICE_TOKEN) private favoritosService: FavoritosService) {
      this.possuiRecursoDeFavoritos = this.favoritosService != null;

      if (this.possuiRecursoDeFavoritos) {
        // Para cada alteração do usuário, devo então buscar os favoritos deste
        this.userService.user$.subscribe(user => {

          this.favoritosService.buscar()
          .subscribe((menuItens) => {
            this.favNavsStore.set(menuItens);
          });

        });

        this.favoritosService.buscar()
          .subscribe((menuItens) => {
            this.favNavsStore.set(menuItens);
          });
      }

    }

  toggleItem(item: MenuItem) {
    const itemFavorited = this.favNavsQuery.getFavorited(item);

    if (isDefined(itemFavorited)) {
      this.remover(itemFavorited);
    } else {
      if (isDefined(item.link)) {
        const parsedUrl = this.router.createUrlTree(Array.isArray(item.link) ? item.link : [item.link]);
        item.link = parsedUrl.toString();
      }

      const newItem = createItem(item);
      this.adicionar(newItem);
    }
  }

  moverItens(indiceOrigem: number, indiceDestino: number) {
    const itens = this.favNavsQuery.getAll();
    moveItemInArray(itens, indiceOrigem, indiceDestino);
    this.substituirColecao(itens);
  }

  private adicionar(item: MenuItem) {
    this.favNavsStore.add(item);
    this.atualizar();
  }

  private remover(item: MenuItem) {
    this.favNavsStore.remove(item.id);
    this.atualizar();
  }

  private substituirColecao(itens: MenuItem[]) {
    this.favNavsStore.set(itens);
    this.atualizar();
  }

  private atualizar() {
    if (this.possuiRecursoDeFavoritos) {
      const itens = this.favNavsQuery.getAll();
      this.favoritosService.salvar(itens)
        // .pipe(
        //   retryWhen(errors => errors.pipe(delay(1000), take(5)))
        // )
      .subscribe();
    }
  }
}
