import { Component } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
@Component({
  selector: '[uikit-filters]',
  template: `
    <ng-container *uikitFiltersView>
      <ng-content></ng-content>
    </ng-container>
  `
})
export class FiltersComponent {

}
