import { ViewContainerRef, Directive, OnInit } from '@angular/core';
import { BreadcrumbService } from '../breadcrumb.service';
import { BaseContainerDirective } from '../base/base-container.directive';

@Directive({
  selector: '[uikitFiltersContainer]'
})
export class FiltersContainerDirective extends BaseContainerDirective {

  observableTemplate = () => this.breadcrumbService.filters$;

  constructor(
    protected viewContainerRef: ViewContainerRef,
    protected breadcrumbService: BreadcrumbService
  ) {
    super(viewContainerRef, breadcrumbService);
  }
}
