import { Directive, TemplateRef } from '@angular/core';
import { BaseViewDirective } from '../base/base-view.directive';
import { BreadcrumbService } from '../breadcrumb.service';


@Directive({
  selector: '[uikitFiltersView]'
})
export class FiltersViewDirective extends BaseViewDirective {
  setTemplate(template: TemplateRef<any>) {
    this.breadcrumbService.filters = this.templateRef;
  }

  constructor(
    protected templateRef: TemplateRef<any>,
    protected breadcrumbService: BreadcrumbService
  ) {
    super(templateRef, breadcrumbService);
  }
}
