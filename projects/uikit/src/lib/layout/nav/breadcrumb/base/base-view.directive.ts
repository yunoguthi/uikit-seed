import { OnInit, TemplateRef } from '@angular/core';
import { BreadcrumbService } from '../breadcrumb.service';


export abstract class BaseViewDirective implements OnInit {
  constructor(
    protected templateRef: TemplateRef<any>,
    protected breadcrumbService: BreadcrumbService
  ) {
  }

  abstract setTemplate(template: TemplateRef<any>);

  ngOnInit() {
    this.setTemplate(this.templateRef);
    // this.breadcrumbService.aside$.next(this.templateRef);
  }
}
