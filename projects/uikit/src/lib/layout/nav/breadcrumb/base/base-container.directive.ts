import { OnInit, ViewContainerRef, Directive } from '@angular/core';
import { BreadcrumbService } from '../breadcrumb.service';
import { Observable } from 'rxjs';


export abstract class BaseContainerDirective implements OnInit {
  constructor(
    protected viewContainerRef: ViewContainerRef,
    protected breadcrumbService: BreadcrumbService
  ) {
  }

  abstract observableTemplate: () => Observable<any>;

  ngOnInit() {
    const subject = this.observableTemplate();
    subject.subscribe((template) => {
      this.viewContainerRef.clear();
      this.viewContainerRef.createEmbeddedView(template as any);
    });
  }
}
