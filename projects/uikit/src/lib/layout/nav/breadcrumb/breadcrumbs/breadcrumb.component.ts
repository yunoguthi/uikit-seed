import { Component, OnInit, ApplicationRef } from '@angular/core';

import { Breadcrumb } from './breadcrumb';
import { BreadcrumbService } from './breadcrumb.service';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'breadcrumb',
    template:
    `<div #template>
    <ng-content></ng-content>
</div>
<div class="container" *ngIf="template.children.length == 0">
  <span *ngFor="let route of (breadcrumbs$ | async)">
  <a mat-button *ngIf="!route.terminal" href="" [routerLink]="[route.link]">{{ route.title }}</a>
  <a mat-button *ngIf="route.terminal">{{ route.title }}</a>
  </span>
</div>`
})
export class BreadcrumbComponent implements OnInit {

  protected breadcrumbsSubject = new BehaviorSubject<Breadcrumb[]>([]);
  public breadcrumbs$ = this.breadcrumbsSubject.asObservable();

    // breadcrumbs: Breadcrumb[];

    constructor(private breadcrumbService: BreadcrumbService, private router: Router, private applicationRef: ApplicationRef) {
      this.breadcrumbService.breadcrumbChanged.subscribe((crumbs: Breadcrumb[]) => { this.onBreadcrumbChange(crumbs); });
    }

    ngOnInit(): void {
      this.breadcrumbsSubject.next(this.breadcrumbService.breadcrumbs);
    }

    private onBreadcrumbChange(crumbs: Breadcrumb[]) {

        this.breadcrumbsSubject.next(crumbs);
        // this.applicationRef.tick();
    }
}
