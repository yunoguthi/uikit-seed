import { Route } from "@angular/router";

export class Breadcrumb {
    title: string;
    terminal: boolean;
    link: string;
    route: Route | null;
}
