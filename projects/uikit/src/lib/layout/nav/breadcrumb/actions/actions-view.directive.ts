import { Directive, TemplateRef } from '@angular/core';
import { BaseViewDirective } from '../base/base-view.directive';
import { BreadcrumbService } from '../breadcrumb.service';


@Directive({
  selector: '[uikitActionsView]'
})
export class ActionsViewDirective extends BaseViewDirective {
  setTemplate(template: TemplateRef<any>) {
    this.breadcrumbService.actions = this.templateRef;
  }

  constructor(
    protected templateRef: TemplateRef<any>,
    protected breadcrumbService: BreadcrumbService
  ) {
    super(templateRef, breadcrumbService);
  }
}
