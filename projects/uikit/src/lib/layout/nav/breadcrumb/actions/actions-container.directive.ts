import { ViewContainerRef, Directive, OnInit } from '@angular/core';
import { BreadcrumbService } from '../breadcrumb.service';
import { BaseContainerDirective } from '../base/base-container.directive';

@Directive({
  selector: '[uikitActionsContainer]'
})
export class ActionsContainerDirective extends BaseContainerDirective {

  observableTemplate = () => this.breadcrumbService.actions$;


  constructor(
    protected viewContainerRef: ViewContainerRef,
    protected breadcrumbService: BreadcrumbService
  ) {
    super(viewContainerRef, breadcrumbService);
  }
}
