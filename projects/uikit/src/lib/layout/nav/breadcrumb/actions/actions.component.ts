import { Component } from '@angular/core';
@Component({
  selector: '[uikit-actions]',
  template: '<ng-container *uikitActionsView><ng-content></ng-content></ng-container>'
})
export class ActionsComponent { }
