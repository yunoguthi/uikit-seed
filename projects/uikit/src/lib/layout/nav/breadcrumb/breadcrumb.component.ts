import { Component, OnInit, ChangeDetectionStrategy, ApplicationRef, ViewChild, TemplateRef, OnDestroy, AfterContentInit, ContentChild, ElementRef, Renderer2, NgZone, ChangeDetectorRef } from '@angular/core';
import { LayoutService } from '../../layout.service';
import { distinctUntilChanged, filter, map, flatMap, debounce, debounceTime } from 'rxjs/operators';
import { Router, NavigationStart, NavigationEnd, Event, ActivatedRoute, RoutesRecognized, RouterStateSnapshot } from '@angular/router';
import { Subscription, BehaviorSubject, Observable } from 'rxjs';
import { MenuItem } from '../menu/menu-item/menu-item.model';
import { FavNavsService } from '../favnav/state/favnavs.service';
import { FavNavsQuery } from '../favnav/state/favnavs.query';
import { BreadcrumbService } from './breadcrumbs/breadcrumb.service';
import { PlatformLocation, Location } from '@angular/common';
import { ScrollDispatcher } from '@angular/cdk/scrolling';
import { PreviousRouteService } from '../../../components/previous-route.service';


@Component({
  selector: 'uikit-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class BreadcrumbComponent implements OnInit, OnDestroy {

  protected subscription = new Subscription();

  public menuItem$ = new BehaviorSubject<MenuItem>(null);
  public menuItemSubscription$ = Subscription.EMPTY;

  public isFavorited$: Observable<boolean>;
  public isFavorited = false;
  public isMobile$ = this.layoutService.isMobile$;

  public navigationEndSubscription = Subscription.EMPTY;
  public data$ = new BehaviorSubject<any>(null);

  @ContentChild('uikitActions') actions: ElementRef;
  protected hasActionsSubject = new BehaviorSubject<boolean>(false);
  public hasActions$ = this.hasActionsSubject.asObservable();

  @ViewChild('templateActions') templateActions: TemplateRef<any>;
  @ViewChild('templateFinder') templateFinder: TemplateRef<any>;
  @ViewChild('templateFilters') templateFilters: TemplateRef<any>;
  @ViewChild('templatePaginator') templatePaginator: TemplateRef<any>;

  public possuiRecursoDeFavoritos = this.favNavsService.possuiRecursoDeFavoritos;

  hiddenShell$ = this.layoutService.hiddenShell$;

  constructor(
    public layoutService: LayoutService,
    protected applicationRef: ApplicationRef,
    protected router: Router,
    protected activatedRoute: ActivatedRoute,
    protected favNavsService: FavNavsService,
    protected favNavsQuery: FavNavsQuery,
    protected renderer: Renderer2,
    protected zone: NgZone,
    protected changeDetector: ChangeDetectorRef,
    protected breadcrumbService: BreadcrumbService,
    protected angularLocation: Location,
    protected platformLocation: PlatformLocation,
    private scrollDispatcher: ScrollDispatcher,
    protected previousRouteService: PreviousRouteService,
  ) {
    this.subscription.add(this.scrollDispatcher.scrolled().pipe(debounceTime(75)).subscribe(x => {
      setTimeout(() => this.applicationRef.tick());
    }));
  }

  public isFixed$ = this.layoutService.isFixed$.pipe(distinctUntilChanged());

  public desabilitarBotaoVoltar$: Observable<boolean>;

  ngOnInit() {
    this.desabilitarBotaoVoltar$ = this.previousRouteService.desabilitarBotaoVoltar;

    // this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe(nav => {
    //   const params = this.activatedRoute.snapshot.params;
    //   console.log('params', params);
    // });

    // this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe(nav => {
    // // this.activatedRoute.params.subscribe(params => {
    //   const params = this.activatedRoute.snapshot.params;
    //   const data = this.activatedRoute.root.firstChild.snapshot.data;
    //   if (data && data.breadcrumb) {
    //     let title: string = data.breadcrumb;
    //     const parametros = Object.keys(params);
    //     parametros.forEach(parametro => {
    //       title = title.replace(`:${parametro}`, params[parametro]);
    //     });
    //     this.breadcrumbService.changeBreadcrumb(this.activatedRoute.snapshot, data.breadcrumb);
    //   }
    // });

    //
    // this.breadcrumbService.breadcrumbChanged.subscribe(crumbs => {
    //   this.breadcrumbs = crumbs.map(crumb => ({ label: crumb.displayName, url: `#${crumb.url}` }));
    // });


    //

    this.activatedRoute.paramMap.pipe(map(() => {
      let child = this.activatedRoute.firstChild;
      while (child) {
        if (child.firstChild) {
          child = child.firstChild;
        } else if (child.snapshot.data) {
          return child.snapshot.data;
        } else {
          return null;
        }
      }
      return null;
    }))
    .subscribe(customData => {
      let route = this.router.routerState.root.snapshot;
      while (route.firstChild != null) {
        route = route.firstChild;
      }
      const data = route.data;

      const favItem = this.getFavItem(data);
      // favItem.icon = customData ? customData.icon : null;

      this.data$.next(favItem);
      if (favItem) {
        this.isFavorited$ = this.favNavsQuery.isFavorited(favItem as MenuItem);
        this.isFavorited$.subscribe(r => this.isFavorited = r);
      }
    });

    this.navigationEndSubscription = this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
      map(() => {
        let child = this.activatedRoute.firstChild;
        while (child) {
          if (child.firstChild) {
            child = child.firstChild;
          } else if (child.snapshot.data) {
            return child.snapshot.data;
          } else {
            return null;
          }
        }
        return null;
      }))
      .subscribe(customData => {
        let route = this.router.routerState.root.snapshot;
        while (route.firstChild != null) {
          route = route.firstChild;
        }
        const data = route.data;



        const favItem = this.getFavItem(data);
        // favItem.icon = customData ? customData.icon : null;
        this.data$.next(favItem);


        if (favItem) {
          this.isFavorited$ = this.favNavsQuery.isFavorited(favItem as MenuItem);
          this.isFavorited$.subscribe(r => this.isFavorited = r);
        }
      });





    this.data$.subscribe(data => {

      this.menuItemSubscription$ = this.layoutService.menuItemsFlattened$.pipe(
        flatMap(r => r),
        filter(r => {
          if (data == null || data.id == null) {
            return false;
          } else {
            const retorno = r.id === data.id;
            return retorno;
          }
        })
      ).subscribe(item => {
        this.menuItem$.next(item);



      });
    });

  }
  private getFavItem(customData) {
    const data = customData;
    const lastBreadCrumb = this.breadcrumbService.breadcrumbs[this.breadcrumbService.breadcrumbs.length - 1];
    if (lastBreadCrumb != null) {
      const title = lastBreadCrumb.title; // this.breadcrumbService.getTitleFormatted(data.breadcrumb, this.activatedRoute.snapshot);
      const favItem = { link: this.router.url, title, icon: data ? data.icon : null };
      return favItem;
    }
  }

  updateIfHasActions() {
    const elements = document.getElementsByClassName('uikit-actions');
    const element = elements.item(0);
    if (element != null) {
      const hasChildNodes = element.hasChildNodes();
      this.hasActionsSubject.next(hasChildNodes);
    } else {
      this.hasActionsSubject.next(false);
    }
  }




  toggleFavoritos() {
    const customData = this.data$.getValue();
    const favItem = this.getFavItem(customData);
    this.favNavsService.toggleItem(favItem as MenuItem);
  }

  ngOnDestroy(): void {
    if (this.navigationEndSubscription && this.navigationEndSubscription.closed == false) {
      this.navigationEndSubscription.unsubscribe();
    }

    if (this.menuItemSubscription$ && this.menuItemSubscription$.closed == false) {
      this.menuItemSubscription$.unsubscribe();
    }

    if (this.subscription && this.subscription.closed == false) {
      this.subscription.unsubscribe();
    }

  }

  public goBack() {
    //    if (document.referrer.indexOf(location.host) !== -1) {
          //history.go(-1);
    this.previousRouteService.removerRotaAcessada();
    this.angularLocation.back();
      //  } else {
    //      this.router.navigate([`../`], { relativeTo: this.activatedRoute });
        //}
      }
}
