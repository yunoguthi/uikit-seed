import { Directive, TemplateRef } from '@angular/core';
import { BaseViewDirective } from '../base/base-view.directive';
import { BreadcrumbService } from '../breadcrumb.service';


@Directive({
  selector: '[uikitFinderView]'
})
export class FinderViewDirective extends BaseViewDirective {
  setTemplate(template: TemplateRef<any>) {
    this.breadcrumbService.finder = this.templateRef;
  }

  constructor(
    protected templateRef: TemplateRef<any>,
    protected breadcrumbService: BreadcrumbService
  ) {
    super(templateRef, breadcrumbService);
  }
}
