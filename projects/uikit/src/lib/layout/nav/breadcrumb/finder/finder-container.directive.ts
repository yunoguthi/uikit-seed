import { ViewContainerRef, Directive, OnInit } from '@angular/core';
import { BreadcrumbService } from '../breadcrumb.service';
import { BaseContainerDirective } from '../base/base-container.directive';

@Directive({
  selector: '[uikitFinderContainer]'
})
export class FinderContainerDirective extends BaseContainerDirective {

  observableTemplate = () => this.breadcrumbService.finder$;

  constructor(
    protected viewContainerRef: ViewContainerRef,
    protected breadcrumbService: BreadcrumbService
  ) {
    super(viewContainerRef, breadcrumbService);
  }
}
