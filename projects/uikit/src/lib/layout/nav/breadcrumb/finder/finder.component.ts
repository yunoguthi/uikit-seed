import { Component } from '@angular/core';
@Component({
  selector: '[uikit-finder]',
  template: '<ng-container *uikitFinderView><ng-content></ng-content></ng-container>'
})
export class FinderComponent { }
