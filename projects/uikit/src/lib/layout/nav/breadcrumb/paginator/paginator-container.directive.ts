import { ViewContainerRef, Directive, OnInit } from '@angular/core';
import { BreadcrumbService } from '../breadcrumb.service';
import { BaseContainerDirective } from '../base/base-container.directive';

@Directive({
  selector: '[uikitPaginatorContainer]'
})
export class PaginatorContainerDirective extends BaseContainerDirective {

  observableTemplate = () => this.breadcrumbService.paginator$;

  constructor(
    protected viewContainerRef: ViewContainerRef,
    protected breadcrumbService: BreadcrumbService
  ) {
    super(viewContainerRef, breadcrumbService);
  }
}
