import { Directive, TemplateRef } from '@angular/core';
import { BaseViewDirective } from '../base/base-view.directive';
import { BreadcrumbService } from '../breadcrumb.service';


@Directive({
  selector: '[uikitPaginatorView]'
})
export class PaginatorViewDirective extends BaseViewDirective {
  setTemplate(template: TemplateRef<any>) {
    this.breadcrumbService.paginator = this.templateRef;
  }

  constructor(
    protected templateRef: TemplateRef<any>,
    protected breadcrumbService: BreadcrumbService
  ) {
    super(templateRef, breadcrumbService);
  }
}
