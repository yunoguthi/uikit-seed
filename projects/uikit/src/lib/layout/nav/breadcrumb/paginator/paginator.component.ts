import { Component } from '@angular/core';
@Component({
  selector: '[uikit-paginator]',
  template: '<ng-container *uikitPaginatorView><ng-content></ng-content></ng-container>'
})
export class PaginatorComponent { }
