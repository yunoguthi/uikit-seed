import { GlobalConfig } from 'ngx-toastr';
import { ToastAction } from './toast.action';

export interface ToastConfig extends GlobalConfig {
  /**
   * Display message toast
   * default: empty
   */
  message: string;

  /**
   * Display title toast
   * default: empty
   */
  title?: string;

  /**
   * Dysplay error
   * default: null
   */
  error?: Error;

  /**
   * Dysplay icon
   * default: null
   */
  icon?: string;

  /**
   * objectAction : { display: string, action: (() => any)}
   * default: null
   */
  objectAction: ToastAction;
}
