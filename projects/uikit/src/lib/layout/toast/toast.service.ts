import {Injectable} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {ToastConfig} from './toast.config';
import {ToastComponent} from './toast.component';
import {Toaster, ToastAction} from './toast.action';
import {Subscription, Observable, of, timer} from 'rxjs';

@Injectable({providedIn: 'root'})
export class ToastService {

  private loadingSubscription: Subscription;
  private loadingToaster: Toaster<any>;
  private defaultConfig: Partial<ToastConfig> = {
    timeOut: 10000,
    toastComponent: ToastComponent,
    progressBar: true,
    positionClass: 'toast-bottom-right',
    extendedTimeOut: 5000,
  };

  constructor(protected toastrService: ToastrService) {
  }

  public success(message: string, title?: string, objectAction?: ToastAction): Toaster<any> {
    const config = this.applyConfig({message, title, objectAction});
    return this.toastrService.success(config.message, config.title, config);
  }

  public error(message: string, error: Error, title?: string, objectAction?: ToastAction): Toaster<any> {
    const config = this.applyConfig({
      message,
      title,
      error,
      objectAction,
      disableTimeOut: true,
      preventDuplicates: true,
      progressBar: false,
      autoDismiss: false,
      positionClass: 'toast-center-center',
      tapToDismiss: false,
      closeButton: true
    });

    return this.toastrService.error(config.message, config.title, config);
  }

  public info(message: string, title?: string, objectAction?: ToastAction): Toaster<any> {
    const disableTimeOut = objectAction != null;
    const config = this.applyConfig({message, title, objectAction, disableTimeOut});
    return this.toastrService.info(config.message, config.title, config);
  }

  public warning(message: string, title?: string, objectAction?: ToastAction): Toaster<any> {
    const disableTimeOut = objectAction != null;
    const config = this.applyConfig({message, title, objectAction, disableTimeOut});
    return this.toastrService.warning(config.message, config.title, config);
  }

  public loading(isLoading$: Observable<boolean>, message: string = 'Carregando...', title?: string, objectAction?: ToastAction): void {
    if (this.loadingSubscription && this.loadingSubscription.closed === false) {
      if (this.loadingToaster && this.loadingToaster.toastRef) {
        try {
          this.loadingToaster.toastRef.close();
        } catch (error) {
        }
      }

      this.loadingSubscription.unsubscribe();
    }

    this.loadingSubscription = isLoading$.subscribe(isLoading => {
      timer().subscribe(() => {
        if (isLoading) {

          const config = this.applyConfig({
            message: message === '' ? 'Carregando...' : message,
            title,
            objectAction,
            disableTimeOut: true,
            preventDuplicates: true,
            progressBar: false,
            autoDismiss: false,
            positionClass: 'toast-top-center',
            tapToDismiss: false
          });

          this.loadingToaster = this.toastrService.show(config.message, config.title, config, 'toast-loading');

        } else {
          if (this.loadingToaster) {
            this.loadingToaster.toastRef.close();
          }
        }
      });
    });
  }

  public clear(toastId?: number): void {
    this.toastrService.clear(toastId);
  }

  public show(config: Partial<ToastConfig>): Toaster<any> {
    config = this.applyConfig(config);
    return this.toastrService.show(config.message, config.title, config);
  }

  private applyConfig(override: Partial<ToastConfig> = {}): Partial<ToastConfig> {
    if (override.objectAction && override.objectAction.display.length > 17) {
      override.objectAction.display = override.objectAction.display.substring(0, 17);
    }

    return {...this.defaultConfig, ...override};
  }
}
