import { ActiveToast } from 'ngx-toastr';

export class ToastAction {
  /*
    Descrição do link do ação no toast --Máximo de caracteres 17
  */
  display: string;

  /*
    Ação a ser executada ao clicar no link atribuida no display
  */
  action: ((value?: any) => any);
}

export interface Toaster<T> extends ActiveToast<T> {

}
