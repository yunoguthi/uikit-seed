import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ToastComponent} from '../toast/toast.component';
import {ToastService} from '../toast/toast.service';
import {
  ToastrModule,
  ToastNoAnimationModule,
  ToastContainerModule
} from 'ngx-toastr';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  entryComponents: [ToastComponent],
  declarations: [ToastComponent],
  imports: [
    CommonModule,
    ToastNoAnimationModule,
    ToastrModule.forRoot(),
    ToastContainerModule,
    BrowserAnimationsModule,
  ],
  exports: [ToastComponent],
  providers: [ToastService]
})
export class ToastModule {
}
