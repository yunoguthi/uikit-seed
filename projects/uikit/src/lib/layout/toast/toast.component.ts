import {
  animate,
  state,
  style,
  transition,
  trigger
} from '@angular/animations';
import {AfterViewInit, Component, Renderer2} from '@angular/core';
import {Toast, ToastrService, ToastPackage} from 'ngx-toastr';
import {ToastConfig} from './toast.config';
import {ToastRef} from 'ngx-toastr/toastr/toast-injector';
import {Hotkey, HotkeysService} from 'angular2-hotkeys';

@Component({
  selector: '[uikit-toast-component]',
  templateUrl: './toast.component.html',
  styleUrls: ['./toast.component.scss'],
  animations: [
    trigger('flyInOut', [
      state('inactive', style({opacity: 0})),
      state('active', style({opacity: 1})),
      state('removed', style({opacity: 0})),
      transition(
        'inactive => active',
        animate('{{ easeTime }}ms {{ easing }}')
      ),
      transition(
        'active => removed',
        animate('{{ easeTime }}ms {{ easing }}')
      )
    ])
  ],
  preserveWhitespaces: false,
})
export class ToastComponent extends Toast implements AfterViewInit {
  options: ToastConfig;
  protected divCenter: any;
  blockBodyToast: boolean;

  constructor(
    protected toastrService: ToastrService,
    public toastPackage: ToastPackage,
    protected renderer: Renderer2,
    public hotkeysService: HotkeysService,
  ) {
    super(toastrService, toastPackage);
  }

  action(event: Event) {
    event.stopPropagation();
    this.options.objectAction.action();
    this.toastPackage.triggerAction();
    return false;
  }

  ngAfterViewInit(): void {
    if (this.options.error) {

      this.hotkeysService.add(new Hotkey('esc', (event: KeyboardEvent): boolean => {
        this.toastrService.clear(this.toastrService.currentlyActive);
        this.remove();
        return false;
      }));

      const toast = this.toastrService.toasts.find(c => c.toastId === this.toastPackage.toastId && !c.toastRef.isInactive());

      if (toast) {
        this.divCenter = this.renderer.createElement('div');
        this.renderer.addClass(this.divCenter, 'block-toast');

        const existBlockToast = this.toastrService.toasts.some((c) => this.isBlockBodyToast(c.toastRef));

        if (!existBlockToast) {
          this.blockBodyToast = true;
          document.body.appendChild(this.divCenter);
        }

        toast.onHidden.subscribe(() => {
          if (this.blockBodyToast) {
            document.body.removeChild(this.divCenter);
          }
        });
      }
    }
  }

  isBlockBodyToast(toast: ToastRef<any>): boolean {
    if (toast && (toast.componentInstance instanceof ToastComponent)) {
      return toast.componentInstance.blockBodyToast;
    }

    return false;
  }
}
