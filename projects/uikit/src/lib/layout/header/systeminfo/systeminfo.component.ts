import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

export interface Section {
  name: string;
  date: string;
  description: string;
  read: boolean;
}

@Component({
  selector: 'uikit-systeminfo',
  templateUrl: './systeminfo.component.html',
  styleUrls: ['./systeminfo.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SysteminfoComponent implements OnInit {
  folders: Section[] = [
    {
      name: '2.0.2',
      date: '04/02/2019',
      description:
        'Nesta versão reunimos grandes demandas de melhoria do sistema.',
      read: true
    },
    {
      name: '2.0.1.1',
      date: '27/11/2018',
      description:
        'Para esta versão foi feito um trabalho detalhado de correção, uniformização e melhoria da funcionalidade de redistribuição de processos, muitos tribunais estavam reportando e encaminhando códigos pontuais de correção da redistribuição. A equipe do CNJ fez um trabalho detalhado de revisão e readequação da rotina, simplificando o código, melhorando a usabilidade e padronizando as operações. Com esse trabalho foram atendidas outras 30 demandas de 12 tribunais diferentes.',
      read: false
    }
  ];

  constructor() {}

  ngOnInit() {}
}
