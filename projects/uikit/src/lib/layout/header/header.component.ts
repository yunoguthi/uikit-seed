import {ChangeDetectionStrategy, Component, Input, OnInit, Renderer2, ViewChild} from '@angular/core';
import {NavQuery} from '../nav/state/nav.query';
import {NavService} from '../nav/state/nav.service';
import {MatToolbar} from '@angular/material';
import {LayoutService} from '../layout.service';

@Component({
  selector: 'uikit-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent implements OnInit {
  constructor(protected navQuery: NavQuery, protected navService: NavService,
              protected layoutService: LayoutService,
              protected renderer: Renderer2) {
  }

  @Input() showNotifications = false;
  @Input() showUserInfo = false;
  @Input() showSystemInfo = false;
  @Input() showIa = false;
  @ViewChild(MatToolbar) matToolbar: MatToolbar;
  @Input() showShortcuts = true;

  public leftNav$ = this.navQuery.leftnav$;
  public rightNav$ = this.navQuery.rightnav$;

  ngOnInit() {
  }

  toggle() {
    this.navService.toggleLeftNav();
  }

  rightNavToggle() {
    this.navService.toggleRightNav();
  }

}
