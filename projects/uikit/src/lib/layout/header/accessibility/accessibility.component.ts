import { Component, OnInit, ChangeDetectionStrategy, ViewChild, AfterViewInit, Renderer2, Input } from '@angular/core';
import { MatSlider } from '@angular/material/slider';
import { MatSlideToggle } from '@angular/material';

@Component({
  selector: 'uikit-accessibility',
  templateUrl: './accessibility.component.html',
  styleUrls: ['./accessibility.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AccessibilityComponent implements OnInit, AfterViewInit {

  @Input() showIa = false;
  @Input() showShortcuts = true;

  @ViewChild(MatSlider) slider: MatSlider;
  @ViewChild(MatSlideToggle) toggleContrast: MatSlideToggle;


  constructor(private renderer: Renderer2) { }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.slider.valueChange.subscribe(valor => {
      this.renderer.setStyle(document.body, 'font-size', valor + 'em');
    });

    this.toggleContrast.toggleChange.subscribe(() => {

      if(!this.toggleContrast.checked) {
        this.renderer.setAttribute(document.querySelector('.logotipo .is-mobile'), 'src', '../../assets/images/uikit-logotipo-mobile-bw.svg');
        this.renderer.setAttribute(document.querySelector('.logotipo .is-desktop'), 'src', '../../assets/images/uikit-logotipo-bw.svg');
        this.renderer.addClass(document.body, 'dark');
      } else {
        this.renderer.setAttribute(document.querySelector('.logotipo .is-mobile'), 'src', '../../assets/images/uikit-logotipo-mobile.svg');
        this.renderer.setAttribute(document.querySelector('.logotipo .is-desktop'), 'src', '../../assets/images/uikit-logotipo.svg');
        this.renderer.removeClass(document.body, 'dark');
      }

    });
  }

}
