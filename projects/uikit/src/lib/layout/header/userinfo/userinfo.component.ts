import {Component, OnInit, ChangeDetectionStrategy} from '@angular/core';

import {AuthService} from '../../../shared/auth/auth.service';
import {ToastService} from '../../toast/toast.service';

@Component({
  selector: 'uikit-userinfo',
  templateUrl: './userinfo.component.html',
  styleUrls: ['./userinfo.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserinfoComponent implements OnInit {
  user$ = this.authService.user.user$;

  constructor(
    protected authService: AuthService,
    protected toastService: ToastService) {
  }

  ngOnInit() {

  }

  public login() {
    return this.authService.authentication.login()
      .catch(error => {
        this.toastService.error('Error ao realizar o login!', error as Error);
      });
  }

  public logout() {
    try {
      this.authService.authentication.logout();
    } catch (error) {
      this.toastService.error('Error ao realizar o logout!', error as Error);
    }
  }
}
