import {animate, state, style, transition, trigger} from '@angular/animations';

export function searchAnimation() {
  return trigger('searchAnimation', [
    state('true', style({transform: 'translateX(0%)'})),
    state('false', style({transform: 'translateX(100%)'})),
    transition('false => true', animate('0.2s ease-in-out', style({transform: 'translateX(0%)'}))),
    transition('true => false', animate('0.2s ease-in-out', style({transform: 'translateX(100%)'})))
  ]);
}
