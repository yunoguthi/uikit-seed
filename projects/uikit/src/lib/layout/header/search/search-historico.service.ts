import {Injectable} from '@angular/core';
import {LocalRepository} from '../../../shared/repositorio/local-repository';
import {ILocalRepository} from '../../../shared/repositorio/ilocal-repository';
import {from, Observable, of} from 'rxjs';
import * as _ from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class SearchHistoricoService {

  private readonly repositorio: ILocalRepository;

  constructor(repositorio: LocalRepository) {
    this.repositorio = repositorio;
  }

  obterHistorico() {
    let listaHistorico;
    const key = 'historicoPesquisa';
    const item = this.repositorio.obterItem<any>(key);
    if (item === false) {
      this.repositorio.salvarItem(key, []);
    } else {
      listaHistorico = item;
    }
    return listaHistorico;
  }

  salvarHistorico(historico: any): Observable<any> {
    let listaHistorico;
    const key = 'historicoPesquisa';
    const itemAddHistorage = this.repositorio.obterItem<any>(key);
    const itemRetorno = _.xorWith([historico], itemAddHistorage, this.isEqual);

    if (itemRetorno.length > 3) {
      listaHistorico = _.take(itemRetorno, 3);
    } else {
      listaHistorico = itemRetorno;
    }
    this.repositorio.salvarItem(key, listaHistorico);
    return of(null);
  }

  private isEqual(p1: any, p2: any) {
    return _.isEqual({x: p1.title, y: p1.title}, {x: p2.title, y: p2.title});
  }
}
