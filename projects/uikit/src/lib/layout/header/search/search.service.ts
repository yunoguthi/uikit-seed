import { Inject, Injectable, Optional } from '@angular/core';
import { combineLatest, Observable, of } from 'rxjs';
import { GrupoPesquisa, ItemPesquisa } from './search.model';
import { SEARCH_TOKEN, SearchAbstractService } from '../../../utils/abstract/search-abstract.service';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(@Optional() @Inject(SEARCH_TOKEN) protected services: SearchAbstractService[]) {

  }

  buscar(term: string): Observable<(GrupoPesquisa | ItemPesquisa)[]> {

    if (!this.services) {
      console.error('Não existe serviço de busca configurado! Ler documentação para mais informações, de como utilizar.');
      return of();
    }

    const searchObservables = this.services.map(searchService => searchService.searchGroup(term));
    const combinedObservable = combineLatest(searchObservables);
    return combinedObservable;
  }

  adicionarGrupoPesquisa(group: GrupoPesquisa[]) {
    this.services.map(groupSearch => groupSearch.addGroup(group));
  }
}
