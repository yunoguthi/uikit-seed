import {GrupoPesquisa} from '../search.model';
import {Injectable} from '@angular/core';
import {EntityState, EntityStore, StoreConfig, transaction} from '@datorama/akita';

export interface SearchState extends EntityState<GrupoPesquisa> {
}

@Injectable({
  providedIn: 'root'
})
@StoreConfig({
  name: 'search'
})
export class SearchStore extends EntityStore<SearchState, GrupoPesquisa> {
  constructor() {
    super();
  }

  @transaction()
  adicionarGrupoPesquisa(valueJaSetado, groupItem) {
    // valueJaSetado.push(groupItem);
    const item = [...valueJaSetado, ...groupItem];
    this.set(item);
  }

  @transaction()
  addGroupBase(groupItem) {
    this.set(groupItem);
  }

  @transaction()
  removerGrupoPesquisa(title: string) {
    this.remove(title);
  }
}
