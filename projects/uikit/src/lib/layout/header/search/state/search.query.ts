import {SearchState, SearchStore} from './search.store';
import {GrupoPesquisa} from '../search.model';
import {map} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {QueryEntity} from '@datorama/akita';

@Injectable({
  providedIn: 'root'
})
export class SearchQuery extends QueryEntity<SearchState, GrupoPesquisa> {
  constructor(protected store: SearchStore) {
    super(store);
  }

  /*searchGroupData$ = this.selectAll().pipe(
    map(this.getGrupoPesquisaData.bind(this))
  );


  getGrupoPesquisaData(students: Array<GrupoPesquisa>): { [key: string]: Array<GrupoPesquisa> } {
    return students.reduce((
      { title: nArray, items: qArray},
      { title, items }) => {
      return {
        title: [...nArray, title],
        items: [...qArray, items]
      };
    }, { title: [], items: []});
  }*/
}
