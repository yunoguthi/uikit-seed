import * as _ from 'lodash';
import {ID} from '@datorama/akita';

export interface ItemPesquisa {
  id: ID;
  title: string;
  icon?: string;
  tags: string[];
  link: string;
}

export interface GrupoPesquisa {
  id: ID;
  title: string;
  icon?: string;
  isHeader?: boolean;
  isAviso?: boolean;
  items: ItemPesquisa[] | GrupoPesquisa[];
}

export function criarNovoGrupo() {
  return {
    title: '',
    items: []
  } as GrupoPesquisa;
}

export function popularItens(itemParam: any[]) {
  return _.map(itemParam, (item) => {
    return {
      id: item.id,
      title: item.title,
      tags: item.tags,
      link: item.link
    } as ItemPesquisa;
  });
}
