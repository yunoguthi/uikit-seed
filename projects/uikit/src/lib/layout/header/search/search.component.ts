import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ContentChildren,
  EventEmitter,
  Input,
  OnInit,
  Output,
  QueryList,
  ViewChild,
  ViewChildren,
  Renderer2,
  OnDestroy
} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MenuItemComponent} from '../../nav/menu/menu-item/menu-item.component';
import {SearchService} from './search.service';
import {Hotkey, HotkeysService} from 'angular2-hotkeys';
import {MatInput, MatButton} from '@angular/material';
import {Router} from '@angular/router';
import {GrupoPesquisa, ItemPesquisa} from './search.model';
import {searchAnimation} from './slide-animations';
import {SearchHistoricoService} from './search-historico.service';
import * as _ from 'lodash';
import {guid} from '@datorama/akita';
import {timer, Subscription} from 'rxjs';
import {ActiveDescendantKeyManager} from '@angular/cdk/a11y';
import {ENTER, ESCAPE, UP_ARROW, DOWN_ARROW} from '@angular/cdk/keycodes';
import {HighlightComponent} from '../../nav/highlight/highlight.component';

@Component({
  selector: 'uikit-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  animations: [searchAnimation()],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() grupoPesquisado: GrupoPesquisa;
  @Output() adicionarGrupo = new EventEmitter<GrupoPesquisa>();
  @Output() removerGrupo = new EventEmitter<GrupoPesquisa>();

  @Input()
  placeholderText = 'Pesquisa (Ctrl + Alt + 3)';

  @ContentChildren(MenuItemComponent, {descendants: false}) children: QueryList<MenuItemComponent>;
  @ViewChildren(HighlightComponent) items: QueryList<HighlightComponent>;
  private keyManager: ActiveDescendantKeyManager<HighlightComponent>;

  stateForm: FormGroup = this.fb.group({
    searchGroups: '',
  });

  searchOpened: boolean;
  searchGroups: GrupoPesquisa[] = [];
  subscription = new Subscription();

  searchGroupOptions: GrupoPesquisa[];
  highlight: string;
  openSearch = false;
  itemPesquisado: string;
  @ViewChild('search') searchInput: MatInput;
  @ViewChild('searchOpen') searchOpen: MatInput;
  @ViewChild('buttonClose') buttonClose: MatButton;

  public isAnimating = false;

  get hasResults() {
    return this.openSearch && this.searchGroupOptions.length;
  }

  constructor(
    private fb: FormBuilder,
    protected searchService: SearchService,
    public hotkeysService: HotkeysService,
    protected router: Router,
    private searchHistorico: SearchHistoricoService,
    protected renderer: Renderer2,
  ) {
  }

  ngAfterViewInit(): void {

    if (document.body.getBoundingClientRect().width <= 970) {
      this.placeholderText = 'Pesquisa';
    }

    this.hotkeysService.add(new Hotkey('ctrl+alt+3', (event: KeyboardEvent): boolean => {
      this.searchOpen.focus();
      return false; // Prevent bubbling
    }));

    timer(1000).subscribe(() => {
      this.searchOpen.focus();
    });

    this.keyManager = new ActiveDescendantKeyManager(this.items).withWrap().withTypeAhead();
    this.renderer.setStyle(this.buttonClose._elementRef.nativeElement, 'display', 'none');
  }

  ngOnInit(): void {
    this.stateForm.get('searchGroups').valueChanges.subscribe(value => {
      this.itemPesquisado = value;
      this.searchGroupOptions = [];
      this.searchService.buscar(value)
        .subscribe((itensMenuPesquisa) => {
            if (itensMenuPesquisa && itensMenuPesquisa.length > 0) {
              _.map(itensMenuPesquisa, (item) => this.searchGroupOptions.push(item));
            } else {
              this.searchGroupOptions.push(
                {
                  id: guid(),
                  isAviso: true,
                  title: `Não encontramos '${value}'`
                } as GrupoPesquisa
              );
            }
          }
        );
    });

    this.searchGroupOptions = this.searchHistorico.obterHistorico();
  }

  openToSearch(event: any, searchContainer: any): void {
    if (!this.searchOpened
      && event
      && event.code === 'KeyA'
      && event.key.length > 0) {
      this.searchOpened = true;
      this.open(searchContainer);
    }
  }

  onBlur(searchContainer: any) {
    this.subscription.add(timer(150).subscribe(() => {
      if (!this.searchOpen.focused) {
        this.close();
        this.renderer.removeAttribute(searchContainer, 'opened');
        this.renderer.setStyle(this.buttonClose._elementRef.nativeElement, 'display', 'none');
      }
    }));
  }

  closeToSearch(): void {
    if (!this.searchOpen.focused) {
      this.close();
    }
  }

  open(searchContainer: any): void {
    this.searchOpen.focus();
    this.renderer.setAttribute(searchContainer, 'opened', '');
    this.renderer.removeStyle(this.buttonClose._elementRef.nativeElement, 'display');
  }

  close(): void {
    this.searchOpened = false;
    (document.activeElement as HTMLElement).blur();
    this.stateForm.controls.searchGroups.setValue('');
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  toggle(searchContainer: any): void {
    this.searchOpened = (!this.searchOpened);
    if (this.searchOpened) {
      this.open(searchContainer);
    }
    this.stateForm.controls.searchGroups.setValue('');
  }

  goToAddHistoric(item: ItemPesquisa, group: (ItemPesquisa | GrupoPesquisa)): void {
    this.searchHistorico.salvarHistorico(group);
    this.router.navigate([item.link[0]]);
    this.closeToSearch();
  }

  get heightSize(): string {
    return null;
  }

  onKeyup(event: any): void {
    switch (event.keyCode) {
      case ENTER:
        const item = this.keyManager.activeItem.item;
        this.goToAddHistoric(item, item);
        break;
      case ESCAPE:
        this.close();
        break;
      case UP_ARROW:
      case DOWN_ARROW:
        this.keyManager.onKeydown(event);
        this.activeItem(this.keyManager.activeItem.item, event.keyCode === UP_ARROW);
        break;
      default:
        this.keyManager.setFirstItemActive();
        this.activeItem(this.keyManager.activeItem.item, false);
        break;
    }
  }

  activeItem(item: GrupoPesquisa | ItemPesquisa, up: boolean): void {
    if (item as GrupoPesquisa && (item as GrupoPesquisa).items && (item as GrupoPesquisa).items.length > 0) {
      if (up) {
        this.keyManager.setPreviousItemActive();
      } else {
        this.keyManager.setNextItemActive();
      }
      this.activeItem(this.keyManager.activeItem.item, up);
    }
  }

  onMouseoverItem(item: any): void {
    this.keyManager.setActiveItem(item);
  }
}
