import { Component, OnInit, ChangeDetectionStrategy, Input, Output } from '@angular/core';

@Component({
  selector: 'uikit-notificacao',
  template: `
  <a mat-list-item [class.is-read]="read">
  <div>
    <h4 *ngIf="nome">
      <mat-icon *ngIf="icone" mat-list-icon class="{{ icone }}"></mat-icon>
      {{ nome }}
    </h4>
    <p>
      {{ descricao }}
    </p>
  </div>
  <mat-icon matListIcon class="fa-lg far" [class.fa-eye]="!read" [class.fa-eye-slash]="read"></mat-icon>
  `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NotificacaoComponent implements OnInit {

  @Input() nome?: string;
  @Input() icone?: string;
  @Input() descricao: string;
  @Input() data: string;
  @Input() read: boolean;

  // @Input() click: any;

  constructor() { }

  ngOnInit() {
  }

}
