export interface Notificacao {
  icone: string;
  nome: string;
  descricao: string;
  data: string;
  read: boolean;
}
