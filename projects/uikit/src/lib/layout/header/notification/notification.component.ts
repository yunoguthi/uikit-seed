import { Component, OnInit, ChangeDetectionStrategy, QueryList, ContentChildren, AfterContentInit, OnDestroy } from '@angular/core';
import { NotificacaoComponent } from './notificacao/notificacao.component';
import { BehaviorSubject, Subscription } from 'rxjs';
import {  flatMap, filter, toArray } from 'rxjs/operators';
import { LayoutService } from '../../layout.service';

export interface Section {
  icon: string;
  name: string;
  description: string;
  read: boolean;
}

@Component({
  selector: 'uikit-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NotificationComponent implements OnInit, OnDestroy, AfterContentInit {


  folders: Section[] = [
    {
      icon: 'fas fa-file-alt',
      name: 'Photos',
      description:
        'Despachar o processo 000912.25 para o gabinete do Dr. Ricardo, no dia 15/12/2018. Ligar para confirmar recebimento.',
      read: true
    },
    {
      icon: 'fas fa-file-alt',
      name: 'Photos',
      description:
        'Despachar o processo 000912.2335 para o gabinete do Dr. Ricardo, no dia 15/12/2018. Ligar para confirmar recebimento.',
      read: false
    },
    {
      icon: 'fas fa-file-alt',
      name: 'Photos',
      description:
        'Despachar o processo 000912.23235 para o gabinete do Dr. Ricardo, no dia 15/12/2018. Ligar para confirmar recebimento.',
      read: false
    }
  ];

  // @ContentChildren(NotificacaoComponent, { descendants: true }) public notificacoesProjetadas: QueryList<NotificacaoComponent>;
  // public quantidadeDeNotificacoes = new BehaviorSubject<number>(0);
  public notificacoes$ = this.layoutService.notificacoes$;
  // public notificacoesNaoLidas$ = this.layoutService.notificacoes$.pipe(flatMap(r => r), filter(r => r.read == false), toArray());
  public quantidadeDeNotificacoesNaoLidas$ = new BehaviorSubject<number>(0);

  public subscriptions: Subscription[] = [];

  constructor(protected layoutService: LayoutService) { }

  ngOnInit() {
    this.notificacoes$.subscribe(r => {
      const notNaoLidas = r.filter(a => a.read === false);
      this.quantidadeDeNotificacoesNaoLidas$.next(notNaoLidas.length);
    });
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => {
      if(s && !s.closed) {
        s.unsubscribe();
      }
    });
  }

  ngAfterContentInit(): void {
    // this.notificacoesProjetadas.changes.subscribe(r => {
    //   const notificacoesNaoLidas = this.notificacoesProjetadas.filter(notif => notif.read === false);
    //   this.quantidadeDeNotificacoes.next(notificacoesNaoLidas.length);
    // });
    // this.notificacoesProjetadas.notifyOnChanges();
  }
}
