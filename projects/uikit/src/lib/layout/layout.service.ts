import {Injectable, OnInit} from '@angular/core';
import {MenuItem} from './nav/menu/menu-item/menu-item.model';
import {map} from 'rxjs/operators';
import {BehaviorSubject} from 'rxjs';
import {Notificacao} from './header/notification/notificacao/notificacao.model';
import {LayoutType} from './layout-type.model';

@Injectable()
export class LayoutService {

  isMobileSubject$ = new BehaviorSubject<boolean>(false);
  public isMobile$ = this.isMobileSubject$.asObservable();

  protected hiddenShellSubject$ = new BehaviorSubject<boolean>(false);
  public hiddenShell$ = this.hiddenShellSubject$.asObservable();

  protected showShellSubject$ = new BehaviorSubject<boolean>(true);
  public showShell$ = this.showShellSubject$.asObservable();

  protected showBreadcrumbSubject$ = new BehaviorSubject<boolean>(true);
  public showBreadcrumb$ = this.showBreadcrumbSubject$.asObservable();

  protected showFullScreenSubject$ = new BehaviorSubject<boolean>(false);
  public showFullScreen$ = this.showFullScreenSubject$.asObservable();

  constructor() {
  }

  private isFixedSubject$ = new BehaviorSubject<boolean>(false);
  public isFixed$ = this.isFixedSubject$.asObservable();

  private menuItemsSubject$ = new BehaviorSubject<MenuItem[]>([]);
  public menuItems$ = this.menuItemsSubject$.asObservable();

  private notificacoesSubject$ = new BehaviorSubject<Notificacao[]>([]);
  public notificacoes$ = this.notificacoesSubject$.asObservable();

  public menuItemsFlattened$ = this.menuItems$.pipe(map(menuItems => {
    const menuItemsFlattened = [] as MenuItem[];
    const flattenObject = (obj: MenuItem) => {
      if (obj.children) {
        obj.children.forEach((value) => {
          flattenObject(value);
        });
      }
      menuItemsFlattened.push(obj);
    };
    menuItems.forEach(flattenObject);
    return menuItemsFlattened;
  }));

  setNotificacoes(notificacoesModel: Notificacao[]): any {
    this.notificacoesSubject$.next(notificacoesModel);
  }

  public setFixed(fixed: boolean) {
    this.isFixedSubject$.next(fixed);
  }

  setMenuItems(menuItems: MenuItem[]) {
    this.menuItemsSubject$.next(menuItems);
  }

  toggleMobile() {
    this.isMobileSubject$.next(!this.isMobileSubject$.getValue());
  }

  private noShell(noShell: boolean) {
    this.hiddenShellSubject$.next(noShell);
  }

  private showShell(mostrarShell: boolean) {
    this.showShellSubject$.next(mostrarShell);
  }

  private showBreadcrumb(mostrarBreadcrumb: boolean) {
    this.showBreadcrumbSubject$.next(mostrarBreadcrumb);
  }

  private showFullScreen(mostraFullScreen: boolean) {
    this.showFullScreenSubject$.next(mostraFullScreen);
  }

  private noShellShowBreadFullScreen() {
    this.noShell(false);
    this.showFullScreen(true);
  }

  private noShellNoBreadcrumb() {
    this.noShell(false);
    this.showBreadcrumb(false);
  }

  private noShellNoBreadcrumbFullScreen() {
    this.noShellShowBreadFullScreen();
    this.showBreadcrumb(true);
  }

  public setType(layoutType: LayoutType): void {
    // const el = document.body;
    if (LayoutType.fullscreen === layoutType) {
      this.showFullScreen(true);
      this.noShell(true);
      this.showBreadcrumb(true);
    } else if (LayoutType.noshell === layoutType) {
      this.noShell(false);
      this.showBreadcrumb(true);
    } else if (LayoutType.noshellfullscreen === layoutType) {
      this.noShellShowBreadFullScreen();
    } else if (LayoutType.noshellnobreadcrumb === layoutType) {
      this.noShellNoBreadcrumb();
    } else if (LayoutType.noshellnobreadcrumbfullscreen === layoutType) {
      this.noShellNoBreadcrumbFullScreen();
    } else {
      this.noShell(true);
      this.showBreadcrumb(true);
      this.showShell(true);
    }
  }
}
