import {
  Component,
  OnInit,
  ViewEncapsulation,
  ChangeDetectionStrategy
} from '@angular/core';
import { Location } from '@angular/common';

declare function require(path: string);

@Component({
  selector: 'uikit-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NotFoundComponent implements OnInit {
  imageSrc = require('@cnj/uikit/lib/theme/uikit/images/error_404.png');
  constructor(
    private ngLocation: Location
  ) { }

  ngOnInit() {}

  public goBack() {
    this.ngLocation.back();
  }

}
