import { Injectable } from '@angular/core';
import { fromEvent, throwError, Observable } from 'rxjs';
import { mapTo, retryWhen, switchMap, filter } from 'rxjs/operators';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';

@Injectable()
export class OfflineInterceptor implements HttpInterceptor {
  private onlineChanges$ = fromEvent(window, 'online').pipe(mapTo(true));

  get isOnline() {
    return navigator.onLine;
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req)
    .pipe(
      retryWhen(errors => {
        if (this.isOnline) {
          return errors.pipe(switchMap(err => throwError(err)));
        } else if (req.method === 'GET') {
          // Não vamos fazer retry se não for GET (imutabilidade)
          return this.onlineChanges$;
        }
      })
    );
  }
}
