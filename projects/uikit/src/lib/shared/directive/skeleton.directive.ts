import {
  Directive,
  Input,
  ElementRef,
  Renderer2,
} from '@angular/core';
import {SkeletonService} from "./skeleton.service";

@Directive({
  selector: '[uikitSkeleton]'
})
export class SkeletonDirective {
  @Input('uikitSkeleton') set isLoading(condition) {
    if (condition) {
      this.skeletonService.show();
    } else {
      this.skeletonService.hide();
    }
  }

  protected skeletonService: SkeletonService

  constructor(protected el: ElementRef,
              protected renderer: Renderer2,) {
    this.skeletonService = new SkeletonService(el, renderer);
  }
}
