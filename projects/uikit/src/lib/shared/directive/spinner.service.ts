import {ComponentFactoryResolver, ElementRef, Renderer2, ViewContainerRef} from '@angular/core';
import {MatSpinner} from "@angular/material/progress-spinner";

export class SpinnerService {
  protected spinner: MatSpinner;
  protected divCenter: any;

  constructor(
    protected el: ElementRef,
    protected renderer: Renderer2,
    protected viewContainerRef: ViewContainerRef,
    protected componentFactoryResolver: ComponentFactoryResolver,
  ) {
    this.Init();
  }

  public Init() {
    const factory = this.componentFactoryResolver.resolveComponentFactory(MatSpinner);
    const componentRef = this.viewContainerRef.createComponent(factory);
    this.divCenter = this.renderer.createElement('div');

    this.spinner = componentRef.instance;
    this.spinner.strokeWidth = 3;
    this.spinner.diameter = 24;
    this.renderer.addClass(this.divCenter, 'uikit-container-spinner');
    this.renderer.addClass(this.spinner._elementRef.nativeElement, 'uikit-spinner');
    this.renderer.setStyle(this.spinner._elementRef.nativeElement, 'display', 'none');

    const spanButton = this.el.nativeElement.querySelector('.mat-button-wrapper') as HTMLSpanElement;
    if (spanButton) {
      this.renderer.setStyle(spanButton, 'display', 'flex');
      this.renderer.setStyle(spanButton, 'align-items', 'center');
      this.renderer.setStyle(spanButton, 'justify-content', 'center');
    }
  }

  public hide(): void {
    this.renderer.setStyle(this.spinner._elementRef.nativeElement, 'display', 'none');
    this.renderer.removeChild(this.el.nativeElement.firstChild, this.spinner._elementRef.nativeElement);
    this.renderer.removeChild(this.el.nativeElement.firstChild, this.divCenter);
    this.el.nativeElement.disabled = false;
  }

  public show(): void {
    this.renderer.appendChild(this.el.nativeElement.firstChild, this.divCenter);
    this.renderer.appendChild(this.el.nativeElement.firstChild, this.spinner._elementRef.nativeElement);
    this.renderer.setStyle(this.spinner._elementRef.nativeElement, 'display', 'inherit');
    this.el.nativeElement.disabled = true;
  }
}
