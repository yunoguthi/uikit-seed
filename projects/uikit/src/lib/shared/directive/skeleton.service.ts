import {ElementRef, Renderer2} from '@angular/core';

export class SkeletonService {

  constructor(
    protected el: ElementRef,
    protected renderer: Renderer2
  ) {
  }

  public show() {
    this.renderer.addClass(this.el.nativeElement, 'is-loading');
    this.el.nativeElement.disabled = false;
  }

  public hide() {
    this.renderer.removeClass(this.el.nativeElement, 'is-loading');
    this.el.nativeElement.disabled = true;
  }
}
