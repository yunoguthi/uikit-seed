import {
  Directive,
  Input,
  ElementRef,
  Renderer2,
  ViewContainerRef,
  ComponentFactoryResolver,
} from '@angular/core';
import {SpinnerService} from "./spinner.service";

@Directive({
  selector: '[uikitSpinner]'
})
export class SpinnerDirective {
  @Input('uikitSpinner') set isLoading(condition) {
    if (condition) {
      this.spinnerService.show();
    } else {
      this.spinnerService.hide();
    }
  }

  protected spinnerService: SpinnerService;

  constructor(
    protected el: ElementRef,
    protected renderer: Renderer2,
    protected viewContainerRef: ViewContainerRef,
    protected componentFactoryResolver: ComponentFactoryResolver,
  ) {
    this.spinnerService = new SpinnerService(el, renderer, viewContainerRef, componentFactoryResolver);
  }
}
