import {
  Directive,
  Input,
  ElementRef,
  Renderer2,
  Host,
  Optional,
  Self, ViewContainerRef, ComponentFactoryResolver,
} from '@angular/core';
import {MatTable} from "@angular/material/table";
import {SkeletonService} from "./skeleton.service";
import {SpinnerService} from "./spinner.service";

@Directive({
  selector: '[uikitLoading]'
})
export class LoadingDirective {
  @Input('uikitLoading') set isLoading(condition) {
    if (condition) {
      this.show();
    } else {
      this.hide();
    }
  }

  protected skeletonService: SkeletonService;
  protected spinnerService: SpinnerService;

  constructor(
    protected el: ElementRef,
    protected renderer: Renderer2,
    protected viewContainerRef: ViewContainerRef,
    protected componentFactoryResolver: ComponentFactoryResolver,
    @Host() @Self() @Optional() public matTable: MatTable<any>
  ) {
    if (matTable) {
      this.skeletonService = new SkeletonService(el, renderer);
    } else {
      this.spinnerService = new SpinnerService(el, renderer, viewContainerRef, componentFactoryResolver);
    }
  }

  show() {
    if (this.matTable) {
      this.skeletonService.show();
    } else {
      this.spinnerService.show();
    }
  }

  hide() {
    if (this.matTable) {
      this.skeletonService.hide();
    } else {
      this.spinnerService.hide();
    }
  }
}
