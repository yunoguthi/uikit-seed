import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AutoFocusDirective} from '../utils/auto-focus.directive';
import {HighlightPipe} from './pipes/highlight.pipe';
import {LoadingDirective} from './directive/loading.directive';
import {MatSpinner} from '@angular/material';
import {SpinnerDirective} from './directive/spinner.directive';
import {SkeletonDirective} from './directive/skeleton.directive';

@NgModule({
  declarations: [
    AutoFocusDirective,
    LoadingDirective,
    SpinnerDirective,
    SkeletonDirective,
    HighlightPipe
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    AutoFocusDirective,
    LoadingDirective,
    SpinnerDirective,
    SkeletonDirective,
    HighlightPipe,
  ],
  entryComponents: [MatSpinner]
})
export class UikitSharedModule {
}
