import { InjectionToken } from '@angular/core';
import { IAuthorizationManager } from './authorization.service';

export let IAuthorizationManagerToken = new InjectionToken<IAuthorizationManager>('IAuthorizationManager');
