import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthorizationGuardService } from './authorization-guard.service';


@Injectable()
export class RouteAuthorizationGuardService extends AuthorizationGuardService {
  async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {

    const permissionToCheck = route.data && route.data.authorize as { action: string, resource: string };

    if (permissionToCheck) {
      console.log(`Verificando autorização para a rota '${state.url}'...`);
      const hasPermission = await this.authorizationService.authorize(permissionToCheck.action, permissionToCheck.resource);
      console.log(`Autorização para a rota '${state.url}' foi '${(hasPermission ? 'concedida' : 'negada')}'!`);
      if (!hasPermission) {
        return this.deny(state);
      } else {
        return this.allow();
      }
    } else {
      throw new Error('Sem permissão para verificar!');
     }

  }
}
