import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { AuthorizationConfigToken, AuthorizationConfig } from '../authorization-config.token';
import { UserService } from '../../authentication/user.service';
import { IAuthorizationManager } from '../authorization.service';

@Injectable({
  providedIn: 'root'
})
export class XacmlAuthorizationManager implements IAuthorizationManager {
  constructor(
    protected userService: UserService,
    protected httpClient: HttpClient,
    @Inject(AuthorizationConfigToken) protected authorizationConfig: AuthorizationConfig,
  ) {
  }
  public async authorize(action: string, resource: string): Promise<boolean> {

    // https://docs.wso2.com/display/IS570/Entitlement+with+REST+APIs
    // WSO2 - ${environment.settings.authentication.authority}api/identity/entitlement/decision/pdp

    return this.httpClient
      .post(`${this.authorizationConfig.xacmlConfig.policyDecisionEndpoint}`,
        {
          Request: {
            AccessSubject: {
              Attribute: [
                {
                  AttributeId: 'subject-id',
                  Value: `${this.userService.userValue.sub}`,
                  DataType: 'string',
                  IncludeInResult: true
                }
              ]
            },
            Resource: {
              Attribute: [
                {
                  AttributeId: 'resource-id',
                  Value: resource,
                  DataType: 'string',
                  IncludeInResult: true
                }
              ]
            },
            Action: {
              Attribute: [
                {
                  AttributeId: 'action-id',
                  Value: action,
                  DataType: 'string',
                  IncludeInResult: true
                }
              ]
            }
          }
        }
      )
      .toPromise()
      .then((result: { Response: { Decision: 'Permit' | 'Deny' }[] }) => {
        if (result && result.Response && result.Response[0]) {
          const resposta = result.Response[0];
          if (resposta.Decision === 'Permit') {
            return true;
          }
        }
        return false;
      });
  }
}
