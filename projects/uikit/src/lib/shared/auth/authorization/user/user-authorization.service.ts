import { UserWithAuthorizations } from './../../authentication/user.model';
import { Injectable, Inject } from '@angular/core';
import { UserService } from '../../authentication/user.service';
import { IAuthorizationManager } from '../authorization.service';

@Injectable({
  providedIn: 'root'
})
export class UserAuthorizationManager implements IAuthorizationManager {
  constructor(protected userService: UserService) {
  }
  public async authorize(action: string, resource: string): Promise<boolean> {

    const userWithAuthorizations = this.userService.userValue as UserWithAuthorizations;

    if (userWithAuthorizations.authenticated) {

      if (userWithAuthorizations.authorizations != null && userWithAuthorizations.authorizations.length > 0) {

        const usuarioPossuiPermissao = userWithAuthorizations.authorizations.some(permissao => {
          const ehMesmoRecurso = permissao.resource === resource;
          const ehMesmaAcao = permissao.action.includes(action);

          const possuiPermissao = ehMesmoRecurso && ehMesmaAcao;
          return possuiPermissao;
        });

        if (usuarioPossuiPermissao) {
          return true;
        }

      }
    }

    return false;
  }
}
