import { InjectionToken } from '@angular/core';

export const AuthorizationConfigToken = new InjectionToken<AuthorizationConfig>('AuthorizationConfigToken');

export interface AuthorizationConfig {
  clientId: string;
  /** Necessário para o UmaAuthorization ('User Managed Access') */
  umaConfig?: {
    /** Usado pelo UmaAuthorizationManager */
    permissionEndpoint?: string;
    /** Usado pelo UmaAuthorizationManager e KeycloakAuthenticationManager */
    tokenEndpoint?: string;
  };
  /** Necessário para o XacmlAuthorization ('eXtensible Access Control Markup Language') */
  xacmlConfig?: {
    /** Usado pelo Wso2AuthorizationManager */
    policyDecisionEndpoint?: string;
    /** Usado pelo Wso2AuthenticationManager */
    entitlementsAllEndpoint?: string;
  };
}
