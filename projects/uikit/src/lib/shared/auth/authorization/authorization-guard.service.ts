import { AuthenticationGuardService } from './authentication-guard.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from '../authentication/user.service';
import { Location, PlatformLocation } from '@angular/common';
import { GuardService } from './guard.service';
import { AuthorizationService } from './authorization.service';


@Injectable()
export abstract class AuthorizationGuardService extends GuardService {
  constructor(
    protected authorizationService: AuthorizationService,
    protected router: Router,
    protected angularLocation: Location,
    protected platformLocation: PlatformLocation
  ) {
    super(router, angularLocation, platformLocation);
  }

  async abstract canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean>;

}
