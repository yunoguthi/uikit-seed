import { AuthorizationConfig, AuthorizationConfigToken } from './../authorization-config.token';
import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { switchMap } from 'rxjs/operators';
import { UserService } from '../../authentication/user.service';
import { IAuthorizationManager } from '../authorization.service';


@Injectable({
  providedIn: 'root'
})
export class UmaAuthorizationManager implements IAuthorizationManager {
  constructor(
    protected httpClient: HttpClient,
    @Inject(AuthorizationConfigToken) protected authorizationConfig: AuthorizationConfig,
  ) {
  }
  public async authorize(action: string, resource: string): Promise<boolean> {

    // WSO2 - ${environment.settings.authentication.authority}api/identity/oauth2/uma/permission/v1.0/permission
    // KEYCLOAK - ${environment.settings.authentication.authority}authz/protection/permission

    return this.httpClient.post(`${this.authorizationConfig.umaConfig.permissionEndpoint}`,
      [
        {
          resource_id: resource,
          resource_scopes: [ action ]
        }
      ]
    ).pipe(switchMap((result: { ticket: string }) => {


      // WSO2 - `${environment.settings.authentication.authority}oauth2/token`,
      // KEYCLOAK - `${environment.settings.authentication.authority}protocol/openid-connect/token`,

      return this.httpClient.post(`${this.authorizationConfig.umaConfig.tokenEndpoint}`,
        new HttpParams()
        .set('grant_type', 'urn:ietf:params:oauth:grant-type:uma-ticket')
        // .set('audience', client)
        .set('response_mode', 'decision')
        .set('ticket', result.ticket)
        .toString(),
        {
          headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
        }
      );

    })).toPromise().then((result: { result: boolean }) => {

      return result.result;

    });

  }
}
