import { Injectable, Inject, Optional } from '@angular/core';
import { IAuthorizationManagerToken } from './authorization-service.token';
import { AuthorizationConfigToken, AuthorizationConfig } from './authorization-config.token';


export interface IAuthorizationManager {
  /**
   * Método responsável por verificar se o usuário possui permissão de executar a ação '{action}' no recurso '{resource}'
   * Attribute Based - https://en.wikipedia.org/wiki/Attribute-based_access_control
   */
  authorize(action: string, resource: string): Promise<boolean>;
}

@Injectable()
export class AuthorizationService {
  constructor(
    @Inject(IAuthorizationManagerToken) @Optional() private authorizationService?: IAuthorizationManager
  ) {

  }

  private validate() {
    if (this.authorizationService == null) {
      throw new Error('Deve ser registrado um "AuthorizationManager" (IAuthorizationManagerToken) para que se verifique autorização!');
    }
  }

  public async authorize(action: string, resource: string): Promise<boolean> {

    this.validate();

    // if (!client) {
    //   if ((!this.authorizationConfig) || (!this.authorizationConfig.clientId)) {
    //     throw Error(`Não é possível verificar autorização sem identificar o 'client'!`);
    //   }
    //   client = this.authorizationConfig.clientId;
    // }

    console.log(`Verificando autorização para executar a ação '${action}' no recurso '${resource}'...`);
    const hasPermission = await this.authorizationService.authorize(action, resource);
    console.log(`Autorização para executar a ação '${action}' no recurso '${resource}' foi '${(hasPermission ? 'concedida' : 'negada')}'!`);
    return hasPermission;
  }

  // /** Verifica se existe no usuário uma propriedade com o nome da `claimName` informada e com o valor da `claimValue` informado. */
  // public checkClaimPermission(claimName: string, claimValue?: string): boolean {
  //   const user = this.userService.userValue;
  //   const userHasClaim = user.hasOwnProperty(claimName);
  //   if (!userHasClaim) {
  //     return false;
  //   }
  //   if (claimValue) {
  //     const userClaimValue = user[claimName];
  //     if (userClaimValue === claimValue) {
  //       return true;
  //     }
  //   } else {
  //     return true;
  //   }
  //   return false;
  // }

  // public isInRole(role: string): boolean {
  //   const user = this.userService.userValue;
  //   const isInRole = user.roles.indexOf(role) > -1;
  //   return isInRole;
  // }
}
