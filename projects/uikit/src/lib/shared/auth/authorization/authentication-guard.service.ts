import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from '../authentication/user.service';
import { Location, PlatformLocation } from '@angular/common';
import { GuardService } from './guard.service';

import { map } from 'rxjs/operators';

@Injectable()
export class AuthenticationGuardService extends GuardService {

  constructor(
    protected identityService: UserService,
    protected router: Router,
    protected angularLocation: Location,
    protected platformLocation: PlatformLocation
  ) {
    super(router, angularLocation, platformLocation);
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Promise<boolean> | Observable<boolean> {
    console.debug('AuthenticationGuardService!', this.identityService.userValue);
    const mapped = this.identityService.user$
    .pipe(map(user => {
      console.debug('Verificando se usuário está autenticado!', user);
      if (user.authenticated) {
        return this.allow();
      } else {
        const data = route.data;
        let login: 'manual' | 'auto' = 'auto';
        if (data && data.login == 'manual') {
          login = 'manual';
        }

        let setCallback = true;
        if (data && data.setCallback == false) {
          setCallback = false;
        }
        return this.deny(state, login, setCallback);
      }
    }));
    // const ret = map.catch(() => {
    //   return this.deny();
    // });
    return mapped;
  }


}
