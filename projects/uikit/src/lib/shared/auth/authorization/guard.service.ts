import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivateChild, UrlTree } from '@angular/router';
import { Location, PlatformLocation } from '@angular/common';
import { Observable } from 'rxjs';

export abstract class GuardService implements CanActivate, CanActivateChild {


  protected get routerStateSnapshot() {
    return this.router.routerState.snapshot;
  }

  protected get activatedRouteSnapshot() {
    return this.router.routerState.root.snapshot;
  }

  constructor(
    protected router: Router,
    protected angularLocation: Location,
    protected platformLocation: PlatformLocation,
  ) {
  }

  // tslint:disable-next-line:max-line-length
  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    return this.canActivate(childRoute, state);
  }

  abstract canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Promise<boolean> | Observable<boolean>;

  allow(): boolean {
    return true;
  }

  deny(state: RouterStateSnapshot, login: 'auto' | 'manual' = 'manual', setCallback = true): boolean {

    if (setCallback) {

      const baseHref = this.platformLocation.getBaseHrefFromDOM();
      const origin = window.location.origin;

      const completeUrlToBaseHref = origin + baseHref;


      const angularRouteWithBaseHref = this.angularLocation.prepareExternalUrl(state.url);

      let angularRouteWithoutBaseHref = angularRouteWithBaseHref;
      if (angularRouteWithBaseHref.startsWith(baseHref)) {
        angularRouteWithoutBaseHref = angularRouteWithBaseHref.replace(baseHref, '');
      }

      const completeRoute = completeUrlToBaseHref + angularRouteWithoutBaseHref;

      console.log('Setando after callback como: ', completeRoute);
      localStorage.setItem('authentication-callback', completeRoute);
    }else{
      localStorage.removeItem('authentication-callback');
    }

    // const uri = this.angularLocation.prepareExternalUrl(state.url);
    // localStorage.setItem(location.host + ':callback', uri);
    let parametros = { queryParams: { login: login } };
    if (login == 'manual') {
      parametros = undefined;
    }

    this.router.navigate(['unauthorized'], parametros );
    return false;
  }

}
