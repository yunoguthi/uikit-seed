import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';
import { AuthenticationService } from '../../authentication/authentication.service';
import { UserService } from '../../authentication/user.service';
import { Router } from '@angular/router';
import { PlatformLocation, LocationStrategy, HashLocationStrategy } from '@angular/common';


@Component({
  selector: 'uikit-protected-route',
  template: ``,
  styles: [],
})
export class ProtectedRouteComponent implements OnInit {
  constructor(private userService: UserService, protected router: Router, private platformLocation: PlatformLocation,
    private locationStrategy: LocationStrategy) { }

  ngOnInit() {


  }




}
