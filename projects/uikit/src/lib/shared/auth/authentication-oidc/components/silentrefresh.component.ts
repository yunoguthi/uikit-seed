import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';


@Component({
  selector: 'uikit-silentrefresh',
  template: ``,
  styles: [],
  encapsulation: ViewEncapsulation.ShadowDom,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SilentRefreshComponent implements OnInit {

  constructor() { }

  ngOnInit() {

  }

}
