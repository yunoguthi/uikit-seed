import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';
import { AuthenticationService } from '../../authentication/authentication.service';
import { UserService } from '../../authentication/user.service';


@Component({
  selector: 'uikit-callback',
  template: ``,
  styles: [],
  encapsulation: ViewEncapsulation.ShadowDom,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CallbackComponent implements OnInit {
  constructor(
    protected authenticationService: AuthenticationService,
    protected userService: UserService
    ) { }

  ngOnInit() {
  }

}
