import { Component, OnInit, ApplicationRef, Inject, OnDestroy } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { LogService } from './../../../../utils/log/log.service';
import { ToastService } from '../../../../layout/toast/toast.service';
import { OidcAuthenticationService } from '../oidc-authentication.service';
import { WebStorageStateStore, UserManager } from 'oidc-client';
import { IAuthenticationServiceToken } from '../../authentication/abstraction/provider-authentication-service.token';
import { IProviderAuthenticationService } from '../../authentication/abstraction/provider-authentication.service';
import { timer, Subscription, BehaviorSubject } from 'rxjs';
import { AuthenticationService } from '../../authentication/authentication.service';
import { UserService } from '../../authentication/user.service';
import { Router } from '@angular/router';
import { HashLocationStrategy, PlatformLocation, LocationStrategy } from '@angular/common';

@Component({
  selector: 'uikit-login-iframe',
  template: `
  <div class="row-container">
    <iframe [hidden]="(isLoading$ | async)" #myIframe [src]="url ? url : null" frameBorder="0" (load)="url ? onLoad(myIframe) : null" class="second-row"></iframe>
  </div>
  `,
  styles: [
    '.iframe { height: 100%; width: 100% border: none; }',
    '.row-container {display: flex; width: 100%; height: 100%; flex-direction: column; overflow: hidden;}',
    '.first-row {background-color: lime; }',
    '.second-row { flex-grow: 1; border: none; margin: 0; padding: 0; }'
  ],
})
export class LoginIframeComponent implements OnInit, OnDestroy {
  public url: any;

  contador = 0;

  public isLoading$ = new BehaviorSubject(true);

  private subscription = new Subscription();

  constructor(private sanitizer: DomSanitizer, protected applicationRef: ApplicationRef, protected toastService: ToastService,
    @Inject(IAuthenticationServiceToken) private providerAuthenticationService: IProviderAuthenticationService,
    protected userService: UserService, protected router: Router, private platformLocation: PlatformLocation,
    private locationStrategy: LocationStrategy

  ) { }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  ngOnInit() {
    this.url = this.sanitizer.bypassSecurityTrustResourceUrl('./protected-route');
    this.toastService.loading(this.isLoading$);
  }

  onLoad(myIframe) {
    this.isLoading$.next(false);

    (myIframe as HTMLIFrameElement).onloadstart = () => {
      // console.log('start loading iframe');
      this.isLoading$.next(true);
    };


    // console.log(`A página do IFrame terminou de carregar!`);
    const oidcProviderAuthenticationService = this.providerAuthenticationService as unknown as OidcAuthenticationService;

    const userManagerArg = {
      ...oidcProviderAuthenticationService.authenticationSettings,
      userStore: new WebStorageStateStore({ store: window.localStorage })
    };
    const userManager = new UserManager(userManagerArg);
    this.subscription.add(timer(0, 100).subscribe(() => {
      userManager.getUser().then(user => {
        // console.log(`Usuário encontrado: `, user);
        if (user) {
          oidcProviderAuthenticationService.loadOidcUser(user);
          // console.log(`Setando usuário encontrado: `, user);
          // const usuario = oidcProviderAuthenticationService.transform(user);
          // this.userService.load(usuario);
          // this.goToLastUri();

          // oidcProviderAuthenticationService.user$.subscribe(usuario => {
          //   console.log(`Carregando usuário encontrado: `, user);
          //   this.userService.load(usuario);
          // });
        }
      });
    }));




    // if(this.oidcAuthenticationService.user$.subscribe(a => a.authenticated))
    // this.oidcAuthenticationService.renewUser().then(() => this.applicationRef.tick());

    // const currentIframePage = null;
    // try {
    //   const currentIframePage = (myIframe as HTMLIFrameElement).contentWindow.location.href;
    // } catch (error) {
    //   this.toastService.error('Não foi possivel obter a informação da página do IFrame!')
    // }

    // console.log(`A página '${currentIframePage}' do IFrame terminou de carregar!`);
    // // this.logService && this.logService.debug();
    // // Caso seja callback, então deve
    // if (currentIframePage.indexOf('/callback') !== -1) {
    //   // Atualizar a aplicação
    //   // this.logService && this.logService.debug(`Atualizando a aplicação!`);
    //   console.log('Atualizando a aplicação!');
    //   this.applicationRef.tick();
    // }
  }

  // TODO: Este método está repetido, devo criar um service para ele (AuthenticationService)
  protected goToLastUri() {
    const lastUri = localStorage.getItem('authentication-callback');


    const baseHref = this.platformLocation.getBaseHrefFromDOM();
    const origin = window.location.origin;
    let hashStrategy = '';
    if (this.locationStrategy instanceof HashLocationStrategy) {
      hashStrategy = '#';
    }
    const completeUrlToBaseHref = origin + baseHref + hashStrategy;


    if (lastUri && (lastUri.indexOf('protected-route') == -1)) {



      const uriToNavigate = lastUri.replace(completeUrlToBaseHref, '');


      // let uriToNavigate = lastUri;
      // if (uriToNavigate.startsWith(baseHref)) {
      //   uriToNavigate = uriToNavigate.replace(baseHref, '');
      // }

      localStorage.removeItem('authentication-callback');
      console.debug('Navegando para página: ', lastUri);
      this.router.navigateByUrl(uriToNavigate);
    } else {
      this.router.navigateByUrl('');
    }
  }

}
