import { OpenIDConnectUser } from './../authentication/user.model';
import { Injectable } from '@angular/core';
import { Router, NavigationStart, ActivatedRoute } from '@angular/router';
import { filter } from 'rxjs/operators';
import { UserManager, WebStorageStateStore } from 'oidc-client';
import { ProviderAuthenticationService } from '../authentication/provider-authentication.service';
import { OpenIDConnectSettings } from './oidc-module';
import {
  PlatformLocation,
  HashLocationStrategy,
  LocationStrategy,
} from '@angular/common';
// @dynamic

@Injectable()
export class OidcAuthenticationService extends ProviderAuthenticationService<
  Oidc.User,
  OpenIDConnectUser,
  void,
  boolean
> {
  protected userManager: Oidc.UserManager = null;

  public userValue: Oidc.User = null;

  get needRenewUser() {
    return this.userValue != null && this.userValue.expired === true;
  }

  renewUser() {
    return this.userManager
      .signinSilent()
      .then((user) => () => this.loadOidcUser(user));
  }

  constructor(
    public authenticationSettings: Oidc.UserManagerSettings &
      OpenIDConnectSettings,
    protected router: Router,
    protected activatedRoute: ActivatedRoute,
    protected platformLocation: PlatformLocation,
    protected locationStrategy: LocationStrategy
  ) {
    super();

    const userManagerArg = {
      ...authenticationSettings,
      userStore: new WebStorageStateStore({ store: window.localStorage }),
    };
    this.userManager = new UserManager(userManagerArg);
    this.userManager.events.addUserLoaded((user) => {
      this.loadOidcUser(user);
    });
    this.userManager.events.addAccessTokenExpired((event) => {
      this.userManager.signinSilent().catch((error) => {
        console.error(error);
        this.userManager.removeUser();
        super.logout();
      });
    });
    this.userManager.events.addSilentRenewError((error) => {
      console.error(error);
      this.userManager.removeUser();
      super.logout();
    });
    this.userManager.getUser().then((user) => {
      this.loadOidcUser(user);
    });

    if (this.authenticationSettings.automaticSilentRenew) {
      this.router.events
        .pipe(filter((event) => event instanceof NavigationStart))
        .pipe(
          filter((event: NavigationStart) =>
            new RegExp(this.authenticationSettings.silent_redirect_uri).test(
              location.href
            )
          )
        )
        .subscribe((event: NavigationStart) => {
          this.userManager.signinSilentCallback().catch((err) => {
            console.error(err);
            this.userManager.startSilentRenew();
          });
        });
    }

    this.router.events
      .pipe(
        filter((event: NavigationStart) =>
          new RegExp(this.authenticationSettings.redirect_uri).test(
            location.href
          )
        )
      )
      .subscribe((event: NavigationStart) => {
        this.userManager.signinRedirectCallback();
      });
  }

  public loadOidcUser(user) {
    this.userValue = user;
    this.loadProviderUser(user);
  }

  public transform(user: Oidc.User): OpenIDConnectUser {
    const destinationUser = {} as OpenIDConnectUser;
    let combinedUser = Object.assign(destinationUser, user);
    // O OIDC_Client usa a propriedade profile para colocar claims,
    // portanto devemos fazer o assign deste com o objetivo de manter
    // no usuário também as informações 'não mapeadas'
    if (user.profile) {
      combinedUser = Object.assign(combinedUser, user.profile);
    }

    combinedUser.authenticated = true;

    combinedUser.sub = user.profile.sub || null;
    combinedUser.name = user.profile.name || null;
    combinedUser.preferred_username = user.profile.preferred_username || null;
    combinedUser.given_name = user.profile.given_name || null;
    combinedUser.nickname = user.profile.nickname || null;
    combinedUser.email = user.profile.email || null;
    combinedUser.picture =
      (user.profile.picture && user.profile.picture[0]) || null;
    combinedUser.client_id = user.profile.client_id || null;
    combinedUser.access_token = user.access_token || null;
    combinedUser.id_token = user.id_token || null;
    combinedUser.roles = user.profile.roles || null;

    return combinedUser;
  }

  public login(args?) {
    const baseHref = this.platformLocation.getBaseHrefFromDOM();
    const routeActiveIsUnauthorized = this.router.isActive(
      baseHref + 'unauthorized',
      false
    );
    if (
      this.authenticationSettings.login_mode === 'redirect' ||
      (this.authenticationSettings.login_mode === 'iframe' &&
        routeActiveIsUnauthorized)
    ) {
      return this.userManager.signinRedirect(args).catch((error) => {
        const erro = error as Error;
        if ('Network Error' === erro.message) {
          const message =
            'Não foi possível se conectar ao servidor de autenticação!';
          const errorToThrow = new Error(message);
          errorToThrow.stack = erro.stack;
          errorToThrow.name = 'Erro de conexão';
          throw errorToThrow;
        } else {
          throw erro;
        }
        // console.error(error);
      });
    } else if (
      this.authenticationSettings.login_mode === 'iframe' &&
      routeActiveIsUnauthorized === false
    ) {
      return this.router.navigateByUrl(baseHref + 'login');
    } else if (this.authenticationSettings.login_mode === 'popup') {
      return this.userManager.signinPopup(args).catch((error) => {
        const erro = error as Error;
        if ('Network Error' === erro.message) {
          const message =
            'Não foi possível se conectar ao servidor de autenticação!';
          const errorToThrow = new Error(message);
          errorToThrow.stack = erro.stack;
          errorToThrow.name = 'Erro de conexão';
          throw errorToThrow;
        } else {
          throw erro;
        }
        // console.error(error);
      });
    }
  }

  public logout(ssoLogout = true) {
    if (ssoLogout) {
      const isOnline = navigator.onLine;
      if (isOnline) {
        return this.userManager.createSignoutRequest().then((req) => {
          console.log('Request logout: ', req);
          const ifrm = document.createElement('iframe');
          ifrm.setAttribute('src', req.url);
          ifrm.style.width = '640px';
          ifrm.style.height = '480px';
          ifrm.style.display = 'none';
          ifrm.onload = () => {
            console.log('IFrame do logout terminou de carregar!');
            super.logout().then(() => {
              this.userManager.removeUser();
              ifrm.parentElement.removeChild(ifrm);
            });
          };
          document.body.appendChild(ifrm);
        });
        // return this.userManager.signoutRedirect()
        //   .then(() => super.logout())
        //   .catch((error) => {
        //     console.error(error);
        //     super.logout();
        //     throw error;
        //   });
      } else {
        const message =
          'Não foi possível se conectar ao servidor de autenticação!';
        const errorToThrow = new Error(message);
        errorToThrow.name = 'Erro de conexão';
        throw errorToThrow;
      }
    } else {
      super.logout();
    }
  }
}
