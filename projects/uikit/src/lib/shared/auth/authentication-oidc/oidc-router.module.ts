import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CallbackComponent } from './components/callback.component';
import { SilentRefreshComponent } from './components/silentrefresh.component';
import { ProtectedRouteComponent } from './components/protected-route.component';
import { LoginIframeComponent } from './components/login-iframe.component';
import { AuthenticationGuardService } from '../authorization/authentication-guard.service';


const routes: Routes = [
  { path: 'callback', component: CallbackComponent, data: { allowedTypes: [ 'noshellnobreadcrumb' ], defaultType: 'noshellnobreadcrumb' } },
  { path: 'silentrefresh', component: SilentRefreshComponent, data: { allowedTypes: [ 'noshellnobreadcrumb' ], defaultType: 'noshellnobreadcrumb' } },
  { path: 'login', component: LoginIframeComponent },
  { path: 'protected-route', component: ProtectedRouteComponent, canActivate: [ AuthenticationGuardService ],
    data: { login: 'auto', setCallback: false, allowedTypes: [ 'noshellnobreadcrumb' ], defaultType: 'noshellnobreadcrumb' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OidcRoutingModule { }
