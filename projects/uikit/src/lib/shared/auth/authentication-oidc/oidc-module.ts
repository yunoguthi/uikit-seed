import { IAuthenticationServiceToken } from './../authentication/abstraction/provider-authentication-service.token';
// import { OidcUserTransformationService } from `./oidc-user-transformation.service`;
import { UserService } from './../authentication/user.service';
import { OidcAuthenticationService } from './oidc-authentication.service';
import {
  NgModule, ModuleWithProviders, InjectionToken, ValueProvider, Inject,
  Injectable, Type, Optional, forwardRef, Provider, APP_INITIALIZER
} from '@angular/core';
import { OidcRoutingModule } from './oidc-router.module';
import { CallbackComponent } from './components/callback.component';
import { SilentRefreshComponent } from './components/silentrefresh.component';
import { ActivatedRoute, Router } from '@angular/router';
import { IAuthenticationManager } from '../authentication/authentication-manager';
import { IAuthenticationManagerToken } from '../authentication/authentication-manager.token';
import { UserManagerSettings, UserManager, WebStorageStateStore } from 'oidc-client';
import {PlatformLocation, LocationStrategy, CommonModule} from '@angular/common';
import { LoginIframeComponent } from './components/login-iframe.component';
import { ProtectedRouteComponent } from './components/protected-route.component';

// export type OpenIDConnectSettings = Oidc.UserManagerSettings & { client_uri: string };
export interface OpenIDConnectSettings extends UserManagerSettings {
  client_uri: string;
  /** Modo de como efetuar o login. (Padrão é 'iframe' quando não informado) */
  login_mode?: 'redirect' | 'popup' | 'iframe';
}
export const OIDC_CONFIG = new InjectionToken<OpenIDConnectSettings>(`OIDC_CONFIG`);


@Injectable()
export class OidcConfigDepHolder {
  constructor(
    @Inject(OIDC_CONFIG) public openIDConnectSettings: OpenIDConnectSettings
  ) {

  }
}

// @Injectable()
// export class AuthManagerDepHolder {
//   constructor(
//     @Inject(IAuthenticationManagerToken) @Optional() public authenticationManager: IAuthenticationManager
//   ) {
//   }
// }

export function InitOidcAuthenticationService(
  oidcConfigDepHolder: OidcConfigDepHolder, router: Router, platformLocation: PlatformLocation,
  activatedRoute: ActivatedRoute, locationStrategy: LocationStrategy) {

    const clientSettings = oidcConfigDepHolder.openIDConnectSettings;

    clientSettings.login_mode = clientSettings.login_mode ? clientSettings.login_mode : 'iframe';


    const baseHref = platformLocation.getBaseHrefFromDOM();
    const origin = window.location.origin;
    const completeUrlToBaseHref = origin + baseHref;


    const baseClientUri = clientSettings.client_uri || completeUrlToBaseHref;

    const defaults: UserManagerSettings = {
      redirect_uri: `${baseClientUri}callback`,
      post_logout_redirect_uri: `${baseClientUri}`,
      response_type: `id_token token`,
      scope: `openid profile email roles offline_access`,
      silent_redirect_uri: `${baseClientUri}silentrefresh`,
      automaticSilentRenew: true,
      filterProtocolClaims: false,
      loadUserInfo: true
    };

    const finalSettings = Object.assign(defaults, clientSettings);

    return new OidcAuthenticationService(finalSettings, router, activatedRoute, platformLocation, locationStrategy);
}

@NgModule({
  declarations: [CallbackComponent, SilentRefreshComponent, ProtectedRouteComponent, LoginIframeComponent],
  imports: [OidcRoutingModule, CommonModule],
  providers: [
    OidcConfigDepHolder,
    // { provide: APP_INITIALIZER, useFactory: initializeUser, deps: [], multi: true },

    {
      // tslint:disable-next-line:object-literal-shorthand
      provide: IAuthenticationServiceToken,
      useFactory: InitOidcAuthenticationService,
      deps:
        [
          OidcConfigDepHolder,
          // ActivatedRoute,
          Router,
          PlatformLocation,
          ActivatedRoute,
          LocationStrategy,
        ]
    },
    // OidcUserTransformationService
  ]
})
export class OidcAuthModule {
}

