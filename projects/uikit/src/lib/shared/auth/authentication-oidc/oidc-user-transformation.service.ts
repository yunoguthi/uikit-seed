// import { User, OpenIDConnectUser } from '../authentication/user.model';
// import { ProviderUserTransformationService } from '../authentication/user-transformation.service';

// export class OidcUserTransformationService extends ProviderUserTransformationService<Oidc.User, OpenIDConnectUser> {
//   public transform(user: Oidc.User): OpenIDConnectUser {

//     const combinedUser = super.transform(user);

//     combinedUser.sub = user.profile.sub || null;
//     combinedUser.preferred_username = user.profile.name || user.profile.preferred_username || null;
//     combinedUser.name = user.profile.name || user.profile.preferred_username || null;
//     combinedUser.email = user.profile.email || null;
//     combinedUser.picture = user.profile.picture && user.profile.picture[0] || null;
//     combinedUser.client_id = user.profile.client_id || null;
//     combinedUser.access_token = user.access_token || null;
//     combinedUser.id_token = user.id_token || null;
//     combinedUser.roles = user.profile.roles || null;

//     return combinedUser;
//   }
// }
