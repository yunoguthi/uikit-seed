import { catchError, mergeMap } from 'rxjs/operators';
import { Injectable, Optional, Inject, inject, ReflectiveInjector, Injector } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError, from } from 'rxjs';
import { OidcAuthenticationService } from './oidc-authentication.service';
import { LogService } from '../../../utils/log/log.service';
import { UserService } from '../authentication/user.service';
import { OAuthUser, OpenIDConnectUser, User } from '../authentication/user.model';
import { IAuthenticationServiceToken } from '../authentication/abstraction/provider-authentication-service.token';
import { IProviderAuthenticationService } from '../authentication/abstraction/provider-authentication.service';

@Injectable()
export class OidcAuthHttpInterceptor implements HttpInterceptor {

  constructor(
    protected userService: UserService,
    private injector: Injector,
    @Optional() protected logService?: LogService,
  ) {

  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    let authReq = req;

    if (!req.headers.has('Authorization')) {
      if (this.userService && this.userService.userValue && this.userService.userValue.authenticated) {

        const token =
          (this.userService.userValue as OAuthUser).access_token ||
          (this.userService.userValue as OpenIDConnectUser).id_token;

        let httpHeaders = req.headers.set('Authorization', `Bearer ${token}`);

        if (!req.headers.has('content-type')) {
          httpHeaders = httpHeaders.set('content-type', 'application/json');
        }

        authReq = req.clone({
          headers: httpHeaders
        });

        if (this.logService) {
          this.logService.debug(`Não possui 'Authorization' header, foi adicionado.`);
        }
      }
    }

    const httpHandle = next
      .handle(authReq)
      .pipe(catchError((error: HttpErrorResponse) => {
        if (error.status === 401) {
          const authenticationService = this.injector.get(IAuthenticationServiceToken) as OidcAuthenticationService;
          if (authenticationService.needRenewUser) {
            return from(authenticationService.renewUser()).pipe(mergeMap(() => {
              return this.intercept(req, next);
            }));
          }
        }
        if (this.logService) {
          this.logService.error(JSON.stringify(error));
        }
        return throwError(error);
      }));

    return httpHandle;

  }

}
