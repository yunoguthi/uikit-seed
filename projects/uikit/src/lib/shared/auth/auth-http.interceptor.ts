import { catchError } from 'rxjs/operators';
import { Injectable, Optional } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { UserService } from './authentication/user.service';
import { OAuthUser, OpenIDConnectUser } from './authentication/user.model';
import { LogService } from '../../utils/log/log.service';

@Injectable()
export class AuthHttpInterceptor implements HttpInterceptor {

  constructor(
    protected userService: UserService,
    @Optional() protected logService: LogService
  ) {

  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    let authReq = req;

    if (!req.headers.has('Authorization')) {
      if (this.userService && this.userService.userValue && this.userService.userValue.authenticated) {
        const token =
          (this.userService.userValue as OAuthUser).access_token ||
          (this.userService.userValue as OpenIDConnectUser).id_token;

        let httpHeaders = req.headers.set('Authorization', `Bearer ${token}`);

        if (!req.headers.has('content-type')) {
          httpHeaders = httpHeaders.set('content-type', 'application/json');
        }

        authReq = req.clone({
          headers: httpHeaders
        });

        if (this.logService) {
          this.logService.debug(`Não possui 'Authorization' header, foi adicionado.`);
        }
      }
    }

    const httpHandle = next
      .handle(authReq)
      .pipe(catchError((error) => {
        if (this.logService) {
          this.logService.error(error);
        }
        // return the error to the method that called it
        return throwError(error);
      }));

    return httpHandle;

  }
}
