import { AuthService } from './auth.service';
import { AuthenticationGuardService } from './authorization/authentication-guard.service';
import { RouteAuthorizationGuardService } from './authorization/route-authorization-guard.service';
import { AuthorizationService } from './authorization/authorization.service';
import { UserService } from './authentication/user.service';
import { NgModule, Type, ModuleWithProviders } from '@angular/core';
import { AuthenticationService } from './authentication/authentication.service';
import { AuthorizationGuardService } from './authorization/authorization-guard.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthHttpInterceptor } from './auth-http.interceptor';
import { ProviderUserTransformationService } from './authentication/user-transformation.service';
import { OidcAuthHttpInterceptor } from './authentication-oidc/auth-http.interceptor';


@NgModule({
  providers: [
    AuthenticationGuardService,
    AuthenticationService,
    // ProviderUserTransformationService,
    UserService,

    AuthorizationService,
    // AuthorizationGuardService,
    RouteAuthorizationGuardService,

    {
      provide: HTTP_INTERCEPTORS,
      useClass: OidcAuthHttpInterceptor,
      multi: true,
     },

    AuthService
  ]
}
)
export class AuthModule {
  // static forRoot(
  //   authenticationProviders: (any[] | Type<any> | ModuleWithProviders)[]
  // ): (any[] | Type<any> | ModuleWithProviders)[] {
  //   const moduleConfig = [
  //     authenticationProviders,
  //     {
  //       ngModule: AuthModule,
  //       providers: [
  //         AuthenticationGuardService,
  //         AuthenticationService,
  //         IdentityService,

  //         AuthorizationService,
  //         AuthorizationGuardService,
  //         RouteAuthorizationGuardService,
  //       ]
  //     }
  //   ] as (any[] | Type<any> | ModuleWithProviders)[];

  //   return moduleConfig;
  //   // moduleConfig.push(authenticationProviders);
  //   // //const retornoAtribuido = Object.assign(moduleConfig, authenticationProviders);

  //   // return moduleConfig;
  // }
}
