export interface User {
  /** Identificador único do usuário naquele provedor de identidade */
  sub: string;
  /** Endereço do serviço provedor de identidade */
  iss?: string;
  /** Informa se o usuário foi autenticado */
  authenticated: boolean;
  /** Url da imagem do usuário */
  picture?: string;
  /** Nome do usuário */
  name: string;
  /** Nome escolhido pelo usuario no provedor de identidade */
  preferred_username?: string;
  /** Nome de exibição do usuário */
  given_name?: string;
  /** Apelido do usuário */
  nickname?: string;
  /** Email principal do usuário */
  email: string;
  /** CPF do usuário */
  cpf?: string;
  /** Perfis do usuário para o sistema */
  roles?: Array<Role>;
}

export interface Role {
  /** Identificador do perfil do usuário no sistema */
  id?: string;
  /** Nome do perfil do usuário no sistema */
  name: string;
}

export type OAuthUser = User & {
  access_token: string;
  client_id: string;
};

export type OpenIDConnectUser = OAuthUser & {
  id_token: string;
};

export type UserWithAuthorizations = User & {
  authorizations?: Array<{
    action: Array<string>;
    resource: string;
    client?: string;
  }>;
};
