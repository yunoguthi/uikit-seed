import { InjectionToken } from '@angular/core';
import { IProviderAuthenticationService } from './provider-authentication.service';


// tslint:disable-next-line:max-line-length
export const IAuthenticationServiceToken = new InjectionToken<IProviderAuthenticationService>('IProviderAuthenticationService');
