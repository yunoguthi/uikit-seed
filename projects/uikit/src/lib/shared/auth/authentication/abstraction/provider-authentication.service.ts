import { User } from '../user.model';
import { BehaviorSubject, Observable } from 'rxjs';



// export abstract class ProviderUserTransformationService<TUserProvider, TUser extends User = User> {
//   protected abstract transform(providerUser: TUserProvider): TUser;
// }


// tslint:disable-next-line:max-line-length
export interface IProviderAuthenticationService<TUser extends User = User, TLoginParam extends any = any, TLogoutParam extends any = any> {
  user$: Observable<TUser>;
  login(param: TLoginParam): Promise<void>;
  logout(param?: TLogoutParam): Promise<void>;
}

// Provedor (Basic, OAuth, OpenID, Firebase, Yolo, Google, etc)
// tslint:disable-next-line:max-line-length
// export interface IProviderAuthenticationService<TUserProvider extends any = any, TUser extends User = User, TLoginParam extends any = any, TLogoutParam extends any = any>
//   extends IAuthenticationService<TUser, TLoginParam, TLogoutParam>, ProviderUserTransformationService<TUserProvider, TUser> {

// }


// export abstract class ProviderAuthenticationService<TUser extends User = User> implements IProviderAuthenticationService {
//   private userSubject: BehaviorSubject<TUser> = new BehaviorSubject<TUser>(null);
//   public user$ = this.userSubject.asObservable();
//   abstract login<TLoginParam>(param: TLoginParam): Promise<void>;

//   load(user: TUser) {
//     this.userSubject.next(user);
//   }
// }



// export class OidcClientAuthenticationSedrvice extends ProviderAuthenticationService {
//   authenticate<TLoginParam>(param: TLoginParam): Promise<void> {
//     return new Promise<void>((resolve) => {

//       resolve();
//     });
//   }

// }
