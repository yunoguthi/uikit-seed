import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';
import { map } from 'rxjs/operators';
import { AuthorizationConfig, AuthorizationConfigToken } from '../../authorization/authorization-config.token';
import { OpenIDConnectUser, UserWithAuthorizations } from '../user.model';
import { IAuthenticationManager } from '../authentication-manager';

@Injectable({
  providedIn: 'root'
})
export class KeycloakAuthenticationManager implements IAuthenticationManager {
  constructor(
    protected httpClient: HttpClient,
    @Inject(AuthorizationConfigToken)
    protected authorizationConfig: AuthorizationConfig
  ) {}
  public async transform(
    user: OpenIDConnectUser
  ): Promise<OpenIDConnectUser & UserWithAuthorizations> {

    // Faço request para buscar todas as permissões do usuário (resource owner) para um client específico
    // Neste caso uso o padrão User Managed Access para retornar as informações
    return this.httpClient
      .post(`${this.authorizationConfig.umaConfig.tokenEndpoint}`,
        new HttpParams()
        .set('grant_type', 'urn:ietf:params:oauth:grant-type:uma-ticket')
        .set('audience', `${this.authorizationConfig.clientId}`)
        .toString(),
        {
          headers: new HttpHeaders()
          .set('Content-Type', 'application/x-www-form-urlencoded')
          .set('Authorization', `Bearer ${user.access_token}`)
        }
      )
      // tslint:disable-next-line:max-line-length
      .pipe(map((result: { upgraded: boolean; access_token: string; expires_in: number; refresh_expires_in: number; refresh_token: string; token_type: string; }) => {
        // Devo então buscar o payload do access_token e incluir (substituindo o access_token anterior) as informações de autorização
        const accessToken = result.access_token.split('.')[1].replace('-', '+').replace('_', '/');
        const accessTokenPayload1 = JSON.parse(atob(accessToken));

        const usuarioFinal1 = Object.assign(user, accessTokenPayload1) as OpenIDConnectUser;
        usuarioFinal1.access_token = result.access_token;
        (usuarioFinal1 as any).access_token = result.access_token;

        return usuarioFinal1 as any;
      }))
      // tslint:disable-next-line:max-line-length
      .pipe(map((user: OpenIDConnectUser & { authorization: { permissions: Array<{ scopes: Array<string>, rsid: string, rsname: string }> } }) => {
        // Devo agora retornar as informações do usuário no formado esperado (OpenIDConnectUser & UserWithAuthorizations)
        let userToReturn = JSON.parse(JSON.stringify(user)) as OpenIDConnectUser & UserWithAuthorizations &
          { authorization: { permissions: Array<{ scopes: Array<string>, rsid: string, rsname: string }> } };

        delete userToReturn['authorization'];

        userToReturn.authorizations = user.authorization.permissions.map(permission => {
          return { action: permission.scopes, resource: permission.rsname, resourceId: permission.rsid };
        });

        return userToReturn;
      }))
      .toPromise();

  }
}
