import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

import { Injectable, Inject } from '@angular/core';
import { map, flatMap } from 'rxjs/operators';
import { IAuthenticationManager } from '../authentication-manager';
import { AuthorizationConfig, AuthorizationConfigToken } from '../../authorization/authorization-config.token';
import { OpenIDConnectUser, UserWithAuthorizations } from '../user.model';



const groupBy = function<T>(items: Array<T>, keyFunction: (items: T) => any) {
  const groups = {};
  items.forEach(function(el) {
      var key = keyFunction(el);
      if (key in groups == false) {
          groups[key] = [];
      }
      groups[key].push(el);
  });
  return Object.keys(groups).map(function(key) {
      return {
          key: key,
          values: groups[key] as T[]
      };
  });
};









interface EntitledAttributesDTOs {
  resourceName: null;
    action: null;
    environment: null;
    allActions: false;
    allResources: false;
    attributeDTOs: Array<{
      attributeValue: string,
      attributeDataType: 'http://www.w3.org/2001/XMLSchema#string',
      attributeId: 'urn:oasis:names:tc:xacml:1.0:resource:resource-id' | 'urn:oasis:names:tc:xacml:1.0:subject:subject-id' | 'urn:oasis:names:tc:xacml:1.0:action:action-id',
      category: 'urn:oasis:names:tc:xacml:3.0:attribute-category:resource' | 'urn:oasis:names:tc:xacml:1.0:subject-category:access-subject' | 'urn:oasis:names:tc:xacml:3.0:attribute-category:action',
    }>;
  }

interface IResultEntitlementAllRequest {
  entitledResultSetDTO: {
    entitledAttributesDTOs: Array<EntitledAttributesDTOs>,
  };
}

@Injectable({
  providedIn: 'root'
})
export class Wso2AuthenticationManager implements IAuthenticationManager {
  constructor(
    protected httpClient: HttpClient,
    @Inject(AuthorizationConfigToken)
    protected authorizationConfig: AuthorizationConfig
  ) {}
  public async transform(
    user: OpenIDConnectUser
  ): Promise<OpenIDConnectUser & UserWithAuthorizations> {

    // Faço request para buscar todas as permissões do usuário (resource owner) para um client específico
    // Neste caso uso o padrão User Managed Access para retornar as informações

    // https://localhost:9443/api/identity/entitlement/decision/entitlements-all
    return this.httpClient
      .post(`${this.authorizationConfig.xacmlConfig.entitlementsAllEndpoint}`,
        {
          identifier: '',
          givenAttributes: []
        },
        {
          headers: new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json')
          .set('Authorization', `Bearer ${user.access_token}`)
        }
      )
      .pipe(map((result: IResultEntitlementAllRequest) => result.entitledResultSetDTO.entitledAttributesDTOs))
      .pipe(map((result) => {
        const authorizations = groupBy(result,
          // tslint:disable-next-line:max-line-length
          (item) => item.attributeDTOs.find(attr => attr.attributeId === 'urn:oasis:names:tc:xacml:1.0:resource:resource-id').attributeValue
        );

        const permissions = authorizations.map(perm => ({
          resource: perm.key,
          action: perm.values.map(r => r.attributeDTOs.find(a => a.attributeId === 'urn:oasis:names:tc:xacml:1.0:action:action-id').attributeValue)
        }));


        const returnUser = { ...user } as OpenIDConnectUser & UserWithAuthorizations;
        returnUser.authorizations = permissions;


        return returnUser;

      }))
      .toPromise();

  }
}


