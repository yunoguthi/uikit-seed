import { InjectionToken } from '@angular/core';
import { IAuthenticationManager } from './authentication-manager';

export const IAuthenticationManagerToken = new InjectionToken<IAuthenticationManager>('IAuthenticationManager');
