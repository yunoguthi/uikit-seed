// import { USER_TRANSFORMATION, IProviderUserTransformation } from './provider-user-transformation.service';
// import { ProviderUserTransformationService } from './user-transformation.service';
// import { BehaviorSubject } from 'rxjs';
// import { Injectable, Inject, Optional } from '@angular/core';
// import { BaseProviderAuthenticationService } from './provider-authentication.service';
// import { UserService } from './user.service';
// import { User } from './user.model';
// import { Router } from '@angular/router';
// import { IAuthenticationManagerToken } from './authentication-manager.token';
// import { IAuthenticationManager } from './authentication-manager';
// import { HttpClient } from '@angular/common/http';

// @Injectable()
// // tslint:disable-next-line:max-line-length
// export abstract class BaseAuthenticationService<UserType extends User, TLoginParams extends any, TLogoutParams extends any> extends BaseProviderAuthenticationService<TLoginParams, TLogoutParams> {
//   protected providerUser: BehaviorSubject<UserType> = new BehaviorSubject<UserType>(null);

//   abstract login(params?: TLoginParams): Promise<UserType>;

//   internalLogin(params?: any): Promise<void> {
//     return this.login(params).then((user) => {
//       // if (user) {
//       //   // Transforma do Framework para o do Produto (caso exista)
//       //   if (this.authenticationManager) {
//       //     this.authenticationManager.authenticate(user).then((userAuthenticated) => this.load(userAuthenticated));
//       //   } else {
//       //     this.load(user);
//       //   }
//       // }

//     });
//   }

//   protected goToLastUri() {
//     const lastUri = localStorage.getItem(location.host + ':callback');

//     if (lastUri) {
//       localStorage.removeItem(location.host + ':callback');
//       this.router.navigateByUrl(lastUri);
//     }
//   }

//   constructor(
//     protected userService: UserService,
//     protected providerUserTransformationService: ProviderUserTransformationService<UserType, User>,
//     protected router: Router,
//     @Inject(IAuthenticationManagerToken) @Optional() protected authenticationManager?: IAuthenticationManager,
//     @Optional() protected httpClient?: HttpClient,
//   ) {
//     super(userService);
//     this.providerUser.subscribe(user => {
//       if (user) {
//         // Transforma do Provider para o do Framework
//         const transformedUser = this.providerUserTransformationService.transform(user);

//         // Transforma do Framework para o do Produto (caso exista)
//         if (this.authenticationManager) {
//           this.authenticationManager.authenticate(transformedUser).then((userAuthenticated) => this.load(userAuthenticated));
//         } else {
//           this.load(transformedUser);
//         }

//       }
//     });
//   }

//   private load(providerTransformedUser: User) {
//     if (providerTransformedUser.authenticated) {
//       this.userService.load(providerTransformedUser);
//       this.goToLastUri();
//     }
//   }
// }

