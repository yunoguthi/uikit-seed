import { Injectable } from '@angular/core';
import { User } from './user.model';

@Injectable()
export class ProviderUserTransformationService<SourceUser, DestinationUser extends User> {

  constructor() { }

  public transform(user: SourceUser): DestinationUser {
    const destinationUser: DestinationUser = <any>{};
    const combinedUser = Object.assign(destinationUser, user);
    combinedUser.authenticated = true;
    return combinedUser;
  }
}

