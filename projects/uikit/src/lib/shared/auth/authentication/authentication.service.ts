import {Inject, Injectable, Optional} from '@angular/core';
import { Location, PlatformLocation, LocationStrategy, HashLocationStrategy } from '@angular/common';
import {Router} from '@angular/router';
import {User} from './user.model';
import {IAuthenticationServiceToken} from './abstraction/provider-authentication-service.token';
import {IAuthenticationManagerToken} from './authentication-manager.token';
import {IAuthenticationManager} from './authentication-manager';
import {UserService} from './user.service';
import {IProviderAuthenticationService} from './abstraction/provider-authentication.service';

@Injectable()
export class AuthenticationService<TLoginParam = any, TLogoutParam = any> {


  constructor(
    protected userService: UserService,
    private ngLocation: Location,
    private platformLocation: PlatformLocation,
    private locationStrategy: LocationStrategy,
    private router: Router,
    // tslint:disable-next-line:max-line-length
    @Inject(IAuthenticationServiceToken) @Optional() private providerAuthenticationService?: IProviderAuthenticationService<User>,
    @Inject(IAuthenticationManagerToken) @Optional() protected authenticationManager?: IAuthenticationManager,
  ) {
    if (this.providerAuthenticationService) {
      this.providerAuthenticationService.user$.subscribe(user => {
        // Transforma do Framework para o do Produto (caso exista)
        if (this.authenticationManager && user) {
          this.authenticationManager.transform(user).then((userAuthenticated) => this.load(userAuthenticated));
        } else {
          this.load(user);
        }
      });
    }
  }

  login(args?: { param?: TLoginParam, enderecoParaVoltar?: string }): Promise<void> {
    const isOnline = navigator.onLine;
    if (isOnline) {

      if (args && args.enderecoParaVoltar) {
        localStorage.setItem('authentication-callback', args.enderecoParaVoltar);
      }

      const lastUri = localStorage.getItem('authentication-callback');
      if (!lastUri) {
        localStorage.setItem('authentication-callback', window.location.href);
      }
      const param = args && args.param;
      return this.providerAuthenticationService.login(param);
    } else {
      throw new Error('Não é possível se autenticar sem conexão de rede!');
    }
  }

  private load(providerTransformedUser: User) {
    if (providerTransformedUser != null && providerTransformedUser.authenticated) {
      this.userService.load(providerTransformedUser);
      this.goToLastUri();
    } else {
      this.userService.unload();
    }
  }

  protected goToLastUri() {
    const lastUri = localStorage.getItem('authentication-callback');


    const baseHref = this.platformLocation.getBaseHrefFromDOM();
    const origin = window.location.origin;
    let hashStrategy = '';
    if (this.locationStrategy instanceof HashLocationStrategy) {
      hashStrategy = '#';
    }
    const completeUrlToBaseHref = origin + baseHref + hashStrategy;


    if (lastUri && (lastUri.indexOf('protected-route') == -1)) {



      const uriToNavigate = lastUri.replace(completeUrlToBaseHref, '');


      // let uriToNavigate = lastUri;
      // if (uriToNavigate.startsWith(baseHref)) {
      //   uriToNavigate = uriToNavigate.replace(baseHref, '');
      // }

      localStorage.removeItem('authentication-callback');
      console.debug('Navegando para página: ', lastUri);
      this.router.navigateByUrl(uriToNavigate);
    } else {
      this.router.navigateByUrl('');
    }
  }

  logout(param?: TLogoutParam): Promise<void> {
    return this.providerAuthenticationService.logout(param);
  }

}
