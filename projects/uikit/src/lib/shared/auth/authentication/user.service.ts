import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { User, Role } from './user.model';
import { Injectable } from '@angular/core';

export class AnonymousUser implements User {
  sub: string = null;
  authenticated = false;
  name = 'Anônimo';
  email: string = null;
}

@Injectable()
export class UserService<TUser extends User = User> {
  protected anonymousUser = new AnonymousUser();

  private userSubject$: BehaviorSubject<TUser> = new BehaviorSubject<TUser>(this.anonymousUser as TUser);
  public user$ = this.userSubject$.asObservable();
  public get userValue() {
    return this.userSubject$.getValue();
  }

  /** Seta o usuário (não deve ser usado na aplicação [somente em testes]).
   * Para login e logout utilize o serviço 'AuthenticationService'
   */
  public load(user: TUser) {
    if (!user) {
      throw Error('É obrigatório informar um usuário!');
    }
    this.userSubject$.next(user);
  }

  /** Seta o usuário como o anônimo (não deve ser usado na aplicação [somente em testes]).
   * Para login e logout utilize o serviço 'AuthenticationService'
   */
  public unload() {
    this.userSubject$.next(this.anonymousUser as TUser);
  }
}
