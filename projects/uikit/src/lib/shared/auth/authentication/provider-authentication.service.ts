import { UserService } from './user.service';
import { IProviderAuthenticationService } from './abstraction/provider-authentication.service';
import { User } from './user.model';
import { BehaviorSubject } from 'rxjs';
import { map, tap } from 'rxjs/operators';



// tslint:disable-next-line:max-line-length
export abstract class ProviderAuthenticationService<TUserProvider extends any = any, TUser extends User = User, TLoginParam extends any = any, TLogoutParam extends any = any> implements IProviderAuthenticationService {

  private userProviderSubject: BehaviorSubject<TUserProvider> = new BehaviorSubject<TUserProvider>(null);
  // protected userProvider$ = this.userProviderSubject.asObservable();

  private userSubject: BehaviorSubject<TUser> = new BehaviorSubject<TUser>(null);
  public user$ = this.userSubject.asObservable();

  abstract login(param?: TLoginParam): Promise<void>;
  logout(param?: TLogoutParam): Promise<void> {
    return new Promise((resolve, reject) => {
      this.loadProviderUser(null);
      resolve();
    });
  }
  constructor() {
    this.userProviderSubject.subscribe(userProvider => {
      if (userProvider) {
        const user = this.transform(userProvider);
        this.loadUser(user);
      } else {
        this.loadUser(null);
      }
    });
  }
  protected abstract transform(providerUser: TUserProvider): TUser;
  private loadUser(user: TUser) {
    this.userSubject.next(user);
  }
  protected loadProviderUser(user: TUserProvider) {
    this.userProviderSubject.next(user);
  }
}


// // tslint:disable-next-line:max-line-length
// export abstract class BaseProviderAuthenticationService<TLoginParams extends any, TLogoutParams extends any> implements IAuthenticationService<TLoginParams, TLogoutParams> {
//   public abstract login(param: TLoginParams): Promise<User>;
//   constructor(protected userService: UserService) {}
//   public logout(param: TLogoutParams): Promise<void> {
//     return new Promise((resolve, reject) => {
//       try {
//         this.userService.unload();
//         resolve();
//       } catch (error) {
//         reject(error);
//       }
//     });
//   }
// }
