import { Injectable, InjectionToken } from '@angular/core';
import { User } from './user.model';


export let USER_TRANSFORMATION = new InjectionToken<IProviderUserTransformation>('USER_TRANSFORMATION');

export interface IProviderUserTransformation {
  transform(user: User): User;
}

// @Injectable()
// export class ProviderUserService<SourceUser, DestinationUser extends User> {

//   constructor() { }

//   public transform(user: SourceUser): DestinationUser {
//     const destinationUser: DestinationUser = <any>{};
//     const combinedUser = Object.assign(destinationUser, user);
//     combinedUser.isAuthenticated = true;
//     return combinedUser;
//   }
// }

