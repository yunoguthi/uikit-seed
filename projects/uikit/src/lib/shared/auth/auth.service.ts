import { Injectable, Optional } from '@angular/core';
import { AuthModule } from './auth.module';
import { AuthenticationService } from './authentication/authentication.service';
import { AuthorizationService } from './authorization/authorization.service';
import { UserService } from './authentication/user.service';

@Injectable()
export class AuthService {

  constructor(
    public user: UserService,
    @Optional() public authentication?: AuthenticationService,
    @Optional() public authorization?: AuthorizationService,
  ) { }


}
