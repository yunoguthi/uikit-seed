export function isFilterComposite(possivelFiltroComposto: FiltroComposto | Filtro): possivelFiltroComposto is FiltroComposto {
  return (possivelFiltroComposto as FiltroComposto).filtros !== undefined;
}

export enum FiltroCompostoOperador {
  E = 'and',
  Ou = 'or',
}

export interface FiltroComposto {
  operador: FiltroCompostoOperador;
  filtros: Array<FiltroComposto | Filtro>;
}

export interface Filtro<TCampo = any> {
  campo: string;
  operador: FiltroOperador;
  valor?: TCampo; // pode ser null (no caso do tipo do filtro `campo eq null`)
}

export enum FiltroOperador {
  Igual = 'Igual',
  Diferente = 'Diferente',
  ComecaCom = 'Começa com',
  TerminaCom = 'Termina com',
  Contem = 'Contém',
  NaoContem = 'Não contém',
  MaiorOuIgual = 'Maior ou igual',
  Maior = 'Maior',
  Menor = 'Menor',
  MenorOuIgual = 'Menor ou igual',
  Em = 'Em',
  NaoEm = 'Não em'
}

// const filtros = [{ campo: 'nome', operator: FilterLogicOperator.Igual, value: 'João' }] as FilterDescriptor[];
// const filtro = { operator: 'and', filters: filtros } as FilterComposite;
