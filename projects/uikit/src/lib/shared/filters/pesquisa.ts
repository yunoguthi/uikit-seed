import { FiltroOperador } from './filter';

export enum SearchMode {
  FullTextSearch,
  Filtro,
}

export interface Pesquisa {
  termo: string;
  config?: {
    modo?: SearchMode;
    filtro?: {
      campos: string[];
      operador?: FiltroOperador;
    }
  };
}
