export interface Ordenacao {
  campo: string;
  direcao: 'asc' | 'desc';
}
