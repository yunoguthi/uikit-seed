export interface Paginacao {
  /**
   * Pagina (NÃO É iniciado em zero)
   *
   */
  pagina: number;

  /**
   * Quantidade de itens por página
   *
   */
  itensPorPagina: number;
}
