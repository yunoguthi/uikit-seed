import {BaseSerializerService} from '../base-serializer.service';
import {Injectable, Inject, Optional, InjectionToken} from '@angular/core';
import {Filtro, FiltroCompostoOperador, FiltroOperador} from '../../filter';
import {ParametrosDaRequisicao} from '../../request-parameters';
import {Paginacao} from '../../page';

export const ODataSerializerServiceConfigToken = new InjectionToken<ODataSerializerServiceConfig>('ODataSerializerServiceConfigToken');

export interface ODataSerializerServiceConfig {
  requestCount: boolean;
}

@Injectable({providedIn: 'root'})
export class ODataSerializerService extends BaseSerializerService {
  protected pesquisaFiltroOperadorPadrao: FiltroOperador = FiltroOperador.Contem;
  protected suportaPesquisa = false;
  protected suportaFiltros = true;
  protected suportaPaginacao = true;
  protected suportaOrdenacao = true;

  pesquisaClausulaFormato = `$search={pesquisa}`;

  oDataConfig: ODataSerializerServiceConfig;

  constructor(@Inject(ODataSerializerServiceConfigToken) @Optional() oDataSerializerServiceConfig?: ODataSerializerServiceConfig) {
    super();
    const defaultParameter = {requestCount: true} as ODataSerializerServiceConfig;
    this.oDataConfig = Object.assign(defaultParameter, oDataSerializerServiceConfig);
  }


  protected serializeInterceptor(parametro: ParametrosDaRequisicao): string {
    if (this.oDataConfig.requestCount) {
      return `$count=true`;
    }
  }

  ordenacaoClausulaFormato = `$orderby={ordenacoes}`;
  ordenacaoFormato = `{campo} {direcao}`;
  ordenacoesSeparador = `, `;

  protected paginacaoPaginaInterceptor(paginacao: Paginacao) {
    const paginaZeroBased = paginacao.pagina - 1;
    const pagina = paginaZeroBased * paginacao.itensPorPagina; // Para OData é $skip, então significa 'pula itens' e não 'traga a página'.
    return pagina;
  }

  paginacaoPaginaClausulaFormato = `$skip={pagina}`;
  paginacaoItensPorPaginaClausulaFormato = `$take={itensPorPagina}`;

  filtroClausulaFormato = `$filter={filtros}`;
  filtroFormato = `({filtro})`;

  protected getFiltroOperadorMapeado(filtroOperador: FiltroOperador): string {
    switch (filtroOperador) {
      case FiltroOperador.ComecaCom: {
        return `startswith({campo},{valor})`;
      }
      case FiltroOperador.Diferente: {
        return `{campo} ne {valor}`;
      }
      case FiltroOperador.Contem: {
        return `contains({campo},{valor})`;
      }
      case FiltroOperador.Igual: {
        return `{campo} eq {valor}`;
      }
      case FiltroOperador.Maior: {
        return `{campo} gt {valor}`;
      }
      case FiltroOperador.MaiorOuIgual: {
        return `{campo} ge {valor}`;
      }
      case FiltroOperador.Menor: {
        return `{campo} lt {valor}`;
      }
      case FiltroOperador.MenorOuIgual: {
        return `{campo} le {valor}`;
      }
      case FiltroOperador.Em: {
        return `{campo} in ({valor})`;
      }
      case FiltroOperador.NaoContem: {
        return `contains({campo},{valor}) eq false`;
      }
      case FiltroOperador.TerminaCom: {
        return `endswith({campo},{valor})`;
      }
    }
    throw new Error(`Não foi encontrado um operador equivalente a '${filtroOperador}'!`);
  }

  protected getFiltroCompostoOperadorMapeado(filtroCompostoOperador: FiltroCompostoOperador): string {
    switch (filtroCompostoOperador) {
      case FiltroCompostoOperador.E: {
        return ` and `;
      }
      case FiltroCompostoOperador.Ou: {
        return ` or `;
      }
    }
    throw new Error(`Não foi encontrado um operador equivalente a '${filtroCompostoOperador}'!`);
  }

  protected filtroValorInterceptor(filtro: Filtro): string {
    const valor = filtro.valor;
    if (valor instanceof Date) {
      return valor.toISOString();
    } else if (Array.isArray(valor)) {
      return "'" + valor.join("','") + "'";
    } else if (typeof valor === 'string') {
      return `'${valor}'`;
    }
    return valor;
  }
}
