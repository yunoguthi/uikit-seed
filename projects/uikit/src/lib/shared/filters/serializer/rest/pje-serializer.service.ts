import {BaseSerializerService} from '../base-serializer.service';
import {Inject, Injectable, InjectionToken, Optional} from '@angular/core';
import {Filtro, FiltroCompostoOperador, FiltroOperador} from '../../filter';
import {ParametrosDaRequisicao} from '../../request-parameters';
import {Paginacao} from '../../page';

export const PjeDataSerializerServiceConfigToken =
  new InjectionToken<PjeDataSerializerServiceConfig>('PjeDataSerializerServiceConfigToken');

export interface PjeDataSerializerServiceConfig {
  requestCount: boolean;
}

@Injectable({providedIn: 'root'})
export class PjeSerializerService extends BaseSerializerService {
  protected suportaPesquisa = false;
  protected suportaFiltros = true;
  protected suportaPaginacao = true;
  protected suportaOrdenacao = true;
  protected ordenacaoClausulaFormato = `order={ordenacoes}`;
  protected ordenacaoFormato = `{"{campo}":"{direcao}"}`;
  protected ordenacoesSeparador = `, `;
  protected paginacaoPaginaClausulaFormato = `page={"page":{pagina}, `;
  protected paginacaoItensPorPaginaClausulaFormato = `"size":{itensPorPagina}}`;
  protected filtroClausulaFormato = `filter={{filtros}}`;
  protected filtroFormato = `{filtro}`;
  protected pesquisaClausulaFormato = `simpleFilter={pesquisa}`;
  protected pesquisaFiltroCompostoOperadorPadrao = FiltroCompostoOperador.E;

  public dataConfig: PjeDataSerializerServiceConfig;

  constructor(@Inject(PjeDataSerializerServiceConfigToken) @Optional() oDataSerializerServiceConfig?: PjeDataSerializerServiceConfig) {
    super();
    const defaultParameter = {requestCount: true} as PjeDataSerializerServiceConfig;
    this.dataConfig = Object.assign(defaultParameter, oDataSerializerServiceConfig);
  }

  protected paginacaoPaginaInterceptor(paginacao: Paginacao) {
    const paginaZeroBased = paginacao.pagina - 1;
    return paginaZeroBased * paginacao.itensPorPagina;
  }

  protected getFiltroOperadorMapeado(filtroOperador: FiltroOperador): string {
    switch (filtroOperador) {
      case FiltroOperador.Igual: {
        return `"{campo}": {"eq": "{valor}"}`;
      }
      case FiltroOperador.Menor: {
        return `"{campo}": {"lt": "{valor}"}`;
      }
      case FiltroOperador.Maior: {
        return `"{campo}": {"gt": "{valor}"}`;
      }
      case FiltroOperador.MenorOuIgual: {
        return `"{campo}": {"le": "{valor}"}`;
      }
      case FiltroOperador.MaiorOuIgual: {
        return `"{campo}": {"ge": "{valor}"}`;
      }
      case FiltroOperador.Em: {
        return `"{campo}": {"in": ({valor})}`;
      }
      case FiltroOperador.NaoEm: {
        return `"{campo}": {"not in": ({valor})}`;
      }
      case FiltroOperador.ComecaCom: {
        return `"{campo}": {"starts-with": "{valor}"}`;
      }
      case FiltroOperador.TerminaCom: {
        return `"{campo}": {"ends-with": "{valor}"}`;
      }
      case FiltroOperador.Contem: {
        return `"{campo}": {"contains": "{valor}"}`;
      }
      case FiltroOperador.NaoContem: {
        return `"{campo}": {"not-contains": "{valor}"}`;
      }
    }
    throw new Error(`Não foi encontrado um operador equivalente a '${filtroOperador}'!`);
  }

  serializePaginacao(parametrosDaRequisicao: ParametrosDaRequisicao): string {
    if (!this.suportaPaginacao) {
      throw new Error('Este serializador não suporta paginacao!');
    }

    const serializaveis: string[] = [];

    const paginacaoPaginaClausulaFormato = this.paginacaoPaginaClausulaFormato;
    const paginacaoPaginaParaFormatar = {
      pagina: this.paginacaoPaginaInterceptor(parametrosDaRequisicao.paginacao),
    };
    const paginacaoPaginaSerializado = this.format(paginacaoPaginaClausulaFormato, paginacaoPaginaParaFormatar);
    serializaveis.push(paginacaoPaginaSerializado);

    const paginacaoItensPorPaginaClausulaFormato = this.paginacaoItensPorPaginaClausulaFormato;
    const paginacaoItensPorPaginaParaFormatar = {
      itensPorPagina: this.paginacaoItensPorPaginaInterceptor(parametrosDaRequisicao.paginacao),
    };
    const paginacaoItensPorPaginaSerializado = this.format(paginacaoItensPorPaginaClausulaFormato, paginacaoItensPorPaginaParaFormatar);
    serializaveis.push(paginacaoItensPorPaginaSerializado);

    return this.tratarArray(serializaveis, '');
  }

  protected getFiltroCompostoOperadorMapeado(filtroCompostoOperador: FiltroCompostoOperador): string {
    if (filtroCompostoOperador === FiltroCompostoOperador.E) {
      return ',';
    } else {
      // tslint:disable-next-line:max-line-length
      throw new Error('Rest puro não suporta filtros que utilizam "ou" como lógica! Implemente seu próprio serializador para customizar como gerar a requisição devidamente!');
    }
  }

  protected filtroValorInterceptor(filtro: Filtro): string {
    const valor = filtro.valor;
    if (valor instanceof Date) {
      return valor.toISOString();
    } else if (Array.isArray(valor)) {
      return `'${valor.join('\',\'')}'`;
    } else if (typeof valor === 'string') {
      return `'${valor}'`;
    }
    return valor;
  }
}
