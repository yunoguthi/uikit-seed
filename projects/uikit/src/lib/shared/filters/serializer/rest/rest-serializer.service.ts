import {BaseSerializerService} from '../base-serializer.service';
import {Injectable} from '@angular/core';
import {
  FiltroCompostoOperador,
  FiltroOperador,
} from '../../filter';

@Injectable({providedIn: 'root'})
export class RestSerializerService extends BaseSerializerService {
  protected pesquisaFiltroOperadorPadrao: FiltroOperador = FiltroOperador.Contem;
  protected suportaPesquisa = false;
  protected suportaFiltros = true;
  protected suportaPaginacao = false;
  protected suportaOrdenacao = false;

  protected getFiltroOperadorMapeado(filtroOperador: FiltroOperador): string {
    if (filtroOperador === FiltroOperador.Igual) {
      return '{campo}={valor}';
    } else {
      // tslint:disable-next-line:max-line-length
      throw new Error('Rest puro não suporta filtro que não seja o igual! Implemente seu próprio serializador para customizar como gerar a requisição devidamente!');
    }
  }

  protected getFiltroCompostoOperadorMapeado(filtroCompostoOperador: FiltroCompostoOperador): string {
    if (filtroCompostoOperador === FiltroCompostoOperador.E) {
      return '&';
    } else {
      // tslint:disable-next-line:max-line-length
      throw new Error('Rest puro não suporta filtros que utilizam "ou" como lógica! Implemente seu próprio serializador para customizar como gerar a requisição devidamente!');
    }
  }

  protected quantidadeDeFiltrosAlinhadosSuportados = 1;
  protected filtroClausulaFormato: string = '{filtros}';
  protected filtroFormato: string = '{filtro}';

  protected paginacaoPaginaClausulaFormato: string;
  protected paginacaoItensPorPaginaClausulaFormato: string;

  protected ordenacaoClausulaFormato: string;
  protected ordenacaoFormato: string;
  protected ordenacoesSeparador: string;
}
