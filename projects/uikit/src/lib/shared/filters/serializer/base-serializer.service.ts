import {InjectionToken} from '@angular/core';
import {FiltroComposto, Filtro, FiltroOperador, isFilterComposite, FiltroCompostoOperador} from '../filter';
import {ParametrosDaRequisicao} from '../request-parameters';
import {Serializer} from './serializer';
import {Ordenacao} from '../sort';
import {Paginacao} from '../page';
import {SearchMode, Pesquisa} from '../pesquisa';

export const SERIALIZER_TOKEN = new InjectionToken<Serializer>('Serializer');

export abstract class BaseSerializerService implements Serializer {
  protected abstract getFiltroOperadorMapeado(filtroOperador: FiltroOperador): string;

  protected abstract getFiltroCompostoOperadorMapeado(FiltroCompostoOperador: FiltroCompostoOperador): string;

  /**
   * Formato que sera serializado a
   * Tag correspondente a query string
   * enviada na requizição da pesquisa.
   *
   * A palavra chave para substituição pelo filtro é `{filtros}`
   * */
  protected abstract filtroClausulaFormato: string;
  /**
   *  Corresponde a tag que sera aplicada na formatação dos filtros.
   *
   *  A palavra chave para substituição pelo filtro é `{filtro}`
   * */
  protected abstract filtroFormato: string;

  /**
   * Habilita suporte para modo de pesquisa.
   * */
  protected abstract suportaPesquisa: boolean;
  /**
   * Habilita suporte para modo de pesquisa utilizando a lógica com filtros.
   * */
  protected abstract suportaFiltros: boolean;
  /**
   *  Habilita suporte para modo de pesquisa utilizando a lógica com paginação.
   * */
  protected abstract suportaPaginacao: boolean;
  /**
   * Habilita suporte para modo de pesquisa utilizando a lógica com ordenação.
   * */
  protected abstract suportaOrdenacao: boolean;
  /**
   * Formato que sera serializado a
   * Tag correspondente a query string 'skip'
   * enviada na requisição.
   *
   * A palavra chave para substituição é `{pagina}`
   * */
  protected abstract paginacaoPaginaClausulaFormato: string;
  /**
   *  Corresponde a tag que sera aplicada na formatação das clausula dos itens por página.
   *
   *  A palavra chave para substituição é `{itensPorPagina}`
   * */
  protected abstract paginacaoItensPorPaginaClausulaFormato: string;
  /**
   * Formato que sera serializado a
   * Tag correspondente a query string
   * enviada na requizição da ordernação da pesquisa.
   *
   * A palavra chave para substituição é `{ordenacoes}`
   * */
  protected abstract ordenacaoClausulaFormato: string;
  /**
   *  Corresponde a tag que sera aplicada na formatação das clausula de ordenação.
   *
   *  As palavras chaves para substituição é `{campo}` e `{direcao}`
   * */
  protected abstract ordenacaoFormato: string;
  /**
   *  Corresponde ao separador que sera aplicado na serialização das ordenações.
   * */
  protected abstract ordenacoesSeparador: string;

  protected quantidadeDeFiltrosAlinhadosSuportados = Number.MAX_SAFE_INTEGER;

  public serializeFiltros(parametrosDaRequisicao: ParametrosDaRequisicao): string {
    if (!this.suportaFiltros) {
      throw new Error('Este serializador não suporta filtros!');
    }
    const formatoDaClausula = this.filtroClausulaFormato;
    const filtrosCompostoSerializado = this.serializeFiltroComposto(parametrosDaRequisicao.filtros);
    if (filtrosCompostoSerializado != null) {
      const filtrosSerializado = this.format(formatoDaClausula, {filtros: filtrosCompostoSerializado});
      return filtrosSerializado;
    } else {
      return null;
    }
  }

  public serializeFiltroComposto(filtroComposto: FiltroComposto, nivel = 1): string {
    if (nivel > this.quantidadeDeFiltrosAlinhadosSuportados) {
      throw new Error(`A quantidade de filtros alinhados suportados é '${this.quantidadeDeFiltrosAlinhadosSuportados}' e foi alcançado o nível '${nivel}'!`);
    }
    const filtrosSerializadosArray = filtroComposto.filtros.map(filtro => {
      if (isFilterComposite(filtro)) {
        const formatoDosFiltros = this.filtroFormato;
        const proximoNivel = nivel + 1;
        const filtrosCompostoSerializado = this.serializeFiltroComposto(filtro, proximoNivel);
        const filtrosSerializado = this.format(formatoDosFiltros, {filtro: filtrosCompostoSerializado});
        return filtrosSerializado;
      } else {
        return this.serializeFiltro(filtro);
      }
    });
    let filtroCompostoOperador = null;
    if (filtrosSerializadosArray.length > 1) {
      filtroCompostoOperador = this.getFiltroCompostoOperadorMapeado(filtroComposto.operador);
    }
    if (filtrosSerializadosArray.length > 0) {
      const filtrosSerializados = this.tratarArray(filtrosSerializadosArray, filtroCompostoOperador);
      return filtrosSerializados;
    } else {
      return null;
    }
  }

  public serializeFiltro(filtro: Filtro): string {
    const filtroOperador = this.getFiltroOperadorMapeado(filtro.operador);
    const campo = this.filtroCampoInterceptor(filtro);
    const valor = this.filtroValorInterceptor(filtro);
    const filtroSerializado = this.format(filtroOperador, {campo, valor});
    return filtroSerializado;
  }

  protected tratarArray(stringArray: string[], join = '&') {
    const filtrosSerializados = stringArray.filter(s => s !== '' && s != null).join(join);
    return filtrosSerializados;
  }

  protected filtroValorInterceptor(filtro: Filtro): string {
    return filtro.valor;
  }

  protected filtroCampoInterceptor(filtro: Filtro): string {
    return filtro.campo;
  }

  protected pesquisaClausulaFormato?: string;

  protected pesquisaTermoInterceptor(pesquisa: Pesquisa): string {
    return pesquisa.termo;
  }

  protected pesquisaFiltroOperadorPadrao: FiltroOperador = FiltroOperador.Contem;
  protected pesquisaFiltroCompostoOperadorPadrao: FiltroCompostoOperador = FiltroCompostoOperador.Ou;

  protected get pesquisaModoPadrao(): SearchMode {
    if (this.suportaPesquisa) {
      return SearchMode.FullTextSearch;
    } else {
      if (this.suportaFiltros) {
        return SearchMode.Filtro;
      }
    }
  }

  public serializePesquisa(parametrosDaRequisicao: ParametrosDaRequisicao): string {
    const termo = this.pesquisaTermoInterceptor(parametrosDaRequisicao.pesquisa);
    if (parametrosDaRequisicao.pesquisa.config == null) {
      parametrosDaRequisicao.pesquisa.config = {modo: this.pesquisaModoPadrao};
    }
    if (parametrosDaRequisicao.pesquisa.config.modo == null) {
      // Se não informar as configurações, deve prevalecer o que o serializador definiu como padrao
      parametrosDaRequisicao.pesquisa.config.modo = this.pesquisaModoPadrao;
    }
    if ((this.suportaFiltros && !this.suportaPesquisa) &&
      (parametrosDaRequisicao.pesquisa.config.modo === SearchMode.Filtro || this.pesquisaClausulaFormato == null)) {
      // Este é o caso quando a pesquisa é realizada por meio de filtro simples
      if (parametrosDaRequisicao.pesquisa.config.filtro.campos == null ||
        parametrosDaRequisicao.pesquisa.config.filtro.campos.length <= 0) {
        throw new Error('Na pesquisa com modo Filtro, os campos devem ser informados!');
      }
      const operador = parametrosDaRequisicao.pesquisa.config.filtro.operador ?
        parametrosDaRequisicao.pesquisa.config.filtro.operador : this.pesquisaFiltroOperadorPadrao;
      const filtros = parametrosDaRequisicao.pesquisa.config.filtro.campos.map(campo => {
        const filtro = {campo, operador, valor: termo} as Filtro;
        return filtro;
      });
      const filtroComposto = {operador: this.pesquisaFiltroCompostoOperadorPadrao, filtros} as FiltroComposto;
      parametrosDaRequisicao.filtros.filtros.push(filtroComposto);
      return null;
    } else {
      if (!this.suportaPesquisa) {
        throw new Error('Este serializador não suporta pesquisa!');
      }
      const clausula = this.pesquisaClausulaFormato;
      const clausulaSerializada = this.format(clausula, {pesquisa: termo});
      return clausulaSerializada;
    }
  }

  serialize(parametros: ParametrosDaRequisicao): string {
    const serializaveis: string[] = [];

    if (parametros.pesquisa) {
      const serializacaoDaPesquisa = this.serializePesquisa(parametros);
      serializaveis.push(serializacaoDaPesquisa);
    }

    if (parametros.filtros) {
      const serializacaoDosFiltros = this.serializeFiltros(parametros);
      serializaveis.push(serializacaoDosFiltros);
    }

    if (parametros.paginacao) {
      const serializacaoDaPaginacao = this.serializePaginacao(parametros);
      serializaveis.push(serializacaoDaPaginacao);
    }

    if (parametros.ordenacoes) {
      const serializacaoDaOrdenacao = this.serializeOrdenacao(parametros);
      serializaveis.push(serializacaoDaOrdenacao);
    }

    if (this.serializeInterceptor) {
      const parametroSerializado = this.serializeInterceptor(parametros);
      serializaveis.push(parametroSerializado);
    }

    const parametrosSerializados = this.tratarArray(serializaveis);
    return parametrosSerializados;
  }

  protected serializeInterceptor?(parametros: ParametrosDaRequisicao): string;

  serializePaginacao(parametrosDaRequisicao: ParametrosDaRequisicao): string {
    if (!this.suportaPaginacao) {
      throw new Error('Este serializador não suporta paginacao!');
    }

    const serializaveis: string[] = [];

    const paginacaoPaginaClausulaFormato = this.paginacaoPaginaClausulaFormato;
    const paginacaoPaginaParaFormatar = {
      pagina: this.paginacaoPaginaInterceptor(parametrosDaRequisicao.paginacao),
    };
    const paginacaoPaginaSerializado = this.format(paginacaoPaginaClausulaFormato, paginacaoPaginaParaFormatar);
    serializaveis.push(paginacaoPaginaSerializado);

    const paginacaoItensPorPaginaClausulaFormato = this.paginacaoItensPorPaginaClausulaFormato;
    const paginacaoItensPorPaginaParaFormatar = {
      itensPorPagina: this.paginacaoItensPorPaginaInterceptor(parametrosDaRequisicao.paginacao),
    };
    const paginacaoItensPorPaginaSerializado = this.format(paginacaoItensPorPaginaClausulaFormato, paginacaoItensPorPaginaParaFormatar);
    serializaveis.push(paginacaoItensPorPaginaSerializado);

    const parametrosSerializados = this.tratarArray(serializaveis);
    return parametrosSerializados;
  }

  protected paginacaoPaginaInterceptor(paginacao: Paginacao) {
    return paginacao.pagina;
  }

  protected paginacaoItensPorPaginaInterceptor(paginacao: Paginacao) {
    return paginacao.itensPorPagina;
  }

  serializeOrdenacao(parametrosDaRequisicao: ParametrosDaRequisicao): string {
    if (!this.suportaOrdenacao) {
      throw new Error('Este serializador não suporta ordenacao!');
    }
    // Name asc, Description desc
    const paginacaoArray = parametrosDaRequisicao.ordenacoes.map(ordenacao => {
      const ordenacaoFormato = this.ordenacaoFormato; // `{campo} {direcao}`;
      const ordenacaoParaFormatar = {
        campo: this.ordenacaoCampoInterceptor(ordenacao),
        direcao: this.ordenacaoDirecaoInterceptor(ordenacao),
      } as Ordenacao;
      const ordenacoesSerializadas = this.format(ordenacaoFormato, ordenacaoParaFormatar);
      return ordenacoesSerializadas;
    });

    const ordenacoesSeparador = this.ordenacoesSeparador;
    const parametrosSerializados = this.tratarArray(paginacaoArray, ordenacoesSeparador);

    const ordenacaoClausula = this.ordenacaoClausulaFormato; // `$orderby={ordenacoes}`;
    const clausulaFormatada = this.format(ordenacaoClausula, {ordenacoes: parametrosSerializados});

    return clausulaFormatada;
  }

  protected ordenacaoCampoInterceptor(ordenacao: Ordenacao) {
    return ordenacao.campo;
  }

  protected ordenacaoDirecaoInterceptor(ordenacao: Ordenacao) {
    return ordenacao.direcao;
  }

  protected format(formato: string, ...args: any) {
    if (typeof args[0] !== 'object') {
      return formato.replace(/{\d+}/g, (m) => {
        const index = Number(m.replace(/\D/g, ''));
        return (args[index] ? args[index] : m);
      });
    } else {
      const obj = args[0];

      return formato.replace(/{\w+}/g, (m) => {
        const key = m.replace(/{|}/g, '');
        return (obj.hasOwnProperty(key) ? obj[key] : m);
      });
    }
  }
}





