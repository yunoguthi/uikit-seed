import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'highlight'})
export class HighlightPipe implements PipeTransform {

  transform(value: string, term: string): string {

    if (!value) {
      return '';
    }

    const regex = this.createRegex(term);
    if (!regex) {
      return value;
    }

    // tslint:disable-next-line:only-arrow-functions
    value = value.replace(regex, function (matched, group1) {
      return '<strong>' + group1 + '</strong>';
    });

    return value;
  }

  createRegex(input) {
    input = input || '';
    input = this.createReplacements(input);
    input = input.replace(/^[^A-z\u00C0-\u00ff]+|[^A-z\u00C0-\u00ff]+$/g, '');
    input = input.replace(/^\||\|$/g, '');
    if (input) {
      const re = '(' + input + ')';
      return new RegExp(re, 'i');
    }
    return null;
  }


  createReplacements(str) {
    const replacements = [
      '[aàáâãäå]',
      '(æ|oe)',
      '[cç]',
      '[eèéêë]',
      '[iìíîï]',
      '[nñ]',
      '[oòóôõö]',
      '[uùúûü]',
      '[yýÿ]'
    ];

    return replacements.reduce((item, regexStr) => {
      return item.replace(new RegExp(regexStr, 'gi'), regexStr);
    }, str);

  }
}
