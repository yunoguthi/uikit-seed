import {ILocalRepository} from './ilocal-repository';
import {Injectable} from '@angular/core';
import {LocalStorageService} from './localstorage.service';

@Injectable({
  providedIn: 'root'
})
export class LocalRepository implements ILocalRepository {

  constructor(private localStorageService: LocalStorageService) {
  }

  salvarItem<T>(key: string, data: T) {
    this.localStorageService.set(key, data);
  }

  obterItem<T>(key: any): T {
    return this.localStorageService.get(key) as T;
  }

  deletar(key: any): void {
    this.localStorageService.delete(key);
  }
}
