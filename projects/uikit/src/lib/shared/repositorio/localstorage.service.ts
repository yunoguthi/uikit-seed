import {Injectable} from '@angular/core';

const localStorage = <any> window.localStorage;

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  get(key: any): any {

    const json = localStorage.getItem(key);

    try {
      const parse = JSON.parse(json);

      return ((parse === '' || parse === 'null' || !parse) ? false : parse);
    } catch (e) {
      return json;
    }
  }

  set(key, value): void {

    if (typeof value === 'object') {
      value = JSON.stringify(value);
    }

    localStorage.setItem(key, value);
  }

  delete(key): any {

    try {
      return localStorage.removeItem(key);
    } catch (e) {
      return null;
    }
  }
}
