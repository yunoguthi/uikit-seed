export interface ILocalRepository {
  salvarItem<T>(key: string, data: T);

  obterItem<T>(key): T;

  deletar(key: any): void;

}
