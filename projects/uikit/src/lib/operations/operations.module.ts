import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OperationsComponent } from './operations.component';
import {
  MatProgressSpinnerModule, MatSnackBarModule, MatListModule,
  MatExpansionModule, MatButtonModule, MatIconModule, MatTooltipModule
} from '@angular/material';
import { OperationsService } from './operations.service';
import { OperationsManagerService } from './operations-manager.service';

@NgModule({
  entryComponents: [OperationsComponent],
  declarations: [OperationsComponent],
  imports: [
    CommonModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatListModule,
    MatExpansionModule,
    MatButtonModule,
    MatIconModule,
    MatTooltipModule
  ],
  exports: [OperationsComponent],
  providers: [OperationsService, OperationsManagerService]
})
export class OperationsModule {
  constructor(protected operationsManagerService: OperationsManagerService) {
  }
}
