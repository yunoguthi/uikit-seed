import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { OperationsService } from './operations.service';
import { Observable } from 'rxjs';
import { OperationsSnackBar } from './operations.snackbar';
import { trigger, style, state, transition, animate } from '@angular/animations';
import { OperationsManagerService } from './operations-manager.service';

@Component({
  selector: 'uikit-operations',
  templateUrl: './operations.component.html',
  styleUrls: ['./operations.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger('fadeInOut', [
      state('void', style({
        opacity: 0
      })),
      transition('void => *', animate(250)),
      transition('* => void', animate(500)),
    ]),
  ]
})
export class OperationsComponent implements OnInit {

  public operations$: Observable<OperationsSnackBar[]>;
  public isLoading$: Observable<boolean>;

  constructor(
    protected operationsService: OperationsService,
    protected operationsManagerService: OperationsManagerService
  ) { }

  ngOnInit() {
    this.operations$ = this.operationsService.operations$;
    this.isLoading$ = this.operationsService.isLoading$;
  }

  close() {
    this.operationsManagerService.close();
  }
}
