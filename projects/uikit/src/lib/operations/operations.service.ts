import { Injectable } from '@angular/core';
import { BehaviorSubject, timer, Observable, of } from 'rxjs';
import { OperationsSnackBar, OperationState } from './operations.snackbar';
import { distinctUntilChanged } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OperationsService {
  private operationsSubject$ = new BehaviorSubject<OperationsSnackBar[]>([]);
  public operations$ = this.operationsSubject$.asObservable();

  private isLoadingBehaviourSubject$ = new BehaviorSubject<boolean>(false);
  public isLoading$ = this.isLoadingBehaviourSubject$.asObservable().pipe(distinctUntilChanged());
  public isLoading = false;

  constructor() {

  }

  public create<T>(
    description = 'Carregando',
    operation?: Observable<T>,
    undoOperation?: (() => Observable<any>) | PromiseLike<any> | any,
    allowToCancel = false,
    createAsProcessing = true,
  ): Observable<T> & { snackbar: OperationsSnackBar } {
    const operationsSnackBar = new OperationsSnackBar(description, allowToCancel);

    if (operation) {
      operationsSnackBar.setOperation(operation);
    }

    if (undoOperation) {
      operationsSnackBar.setUndoOperation(undoOperation);
    }

    this.addOperation(operationsSnackBar);

    if (createAsProcessing) {
      operationsSnackBar.setAsStarted();
    }

    const operationDefault = operationsSnackBar.operation$ ? operationsSnackBar.operation$ : of();
    return Object.assign(operationDefault, { snackbar: operationsSnackBar });
  }

  public cancelAll(cancellable = (operation: OperationsSnackBar) => operation.canCancel) {
    this.operationsSubject$.getValue().forEach(operation => {
      if (cancellable(operation)) {
        operation.cancel();
      }
    });
  }

  private operationIsFinished(operationState: OperationState) {
    return operationState === OperationState.Processed ||
      operationState === OperationState.Cancelled ||
      operationState === OperationState.Undone ||
      operationState === OperationState.Error;
  }

  private checkLoading() {
    const operations = this.operationsSubject$.getValue();
    const allFinished = operations.every(l => this.operationIsFinished(l.operationState));
    this.isLoading = allFinished === false;
    this.isLoadingBehaviourSubject$.next(this.isLoading);
  }

  private addOperation(loadingDialog: OperationsSnackBar) {
    const actualLoadingDialogs = this.operationsSubject$.getValue();
    loadingDialog.operationState$.subscribe(os => {
      this.checkLoading();
      if (this.operationIsFinished(os)) {
        timer(2500).subscribe(() => {
          if (this.operationIsFinished(os)) {
            this.removeOperation(loadingDialog);
          }
        });
      }
    });

    actualLoadingDialogs.push(loadingDialog);
    this.operationsSubject$.next(actualLoadingDialogs);
  }

  private removeOperation(loadingDialog: OperationsSnackBar) {
    const actualLoadingDialogs = this.operationsSubject$.getValue();
    const index = actualLoadingDialogs.indexOf(loadingDialog);
    if (index !== -1) {
      actualLoadingDialogs.splice(index, 1);
      this.operationsSubject$.next(actualLoadingDialogs);
    }
  }
}
