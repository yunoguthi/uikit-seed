import { Subscription, BehaviorSubject, Observable, combineLatest, throwError } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';

export enum OperationState {
  Idle = 'idle',
  Processing = 'processing',
  Processed = 'processed',
  Cancelling = 'cancelling',
  Cancelled = 'cancelled',
  Undoing = 'undoing',
  Undone = 'undone',
  Error = 'error'
}

export class OperationsSnackBar {

  private operationStateSubject$ = new BehaviorSubject<OperationState>(OperationState.Idle);
  public operationState$ = this.operationStateSubject$.asObservable();
  public operationState = OperationState.Processing;

  public error: Error = null;

  public operation$: Observable<any> = null;

  protected undoOperation: (() => any) | Observable<any> | PromiseLike<any> = null;
  private isUndoableSubject$ = new BehaviorSubject<boolean>(false);
  protected isUndoable$ = this.isUndoableSubject$.asObservable();
  protected isUndoable = false;

  private canUndoSubject$ = new BehaviorSubject<boolean>(false);
  public canUndo$ = this.canUndoSubject$.asObservable();
  public canUndo = false;


  protected cancelOperation: Subscription = null;
  private isCancellableSubject$ = new BehaviorSubject<boolean>(false);
  protected isCancellable$ = this.isCancellableSubject$.asObservable();
  protected isCancellable = false;

  private canCancelSubject$ = new BehaviorSubject<boolean>(false);
  public canCancel$ = this.canCancelSubject$.asObservable();
  public canCancel = false;

  constructor(
    public description: string,
    public allowToCancel = false
  ) {
    // Mantem atualizado as variáveis 'não observável'
    this.operationState$.subscribe((state) => this.operationState = state);
    this.isUndoable$.subscribe(isUndoable => this.isUndoable = isUndoable);
    this.isCancellable$.subscribe(isCancellable => this.isCancellable = isCancellable);

    combineLatest(this.operationState$, this.isCancellable$).subscribe((value) => {
      const state = value[0];
      const isCancellable = value[1];
      if (state === OperationState.Processing && this.allowToCancel) {
        this.canCancel = isCancellable;
      } else {
        this.canCancel = false;
      }
      this.canCancelSubject$.next(this.canCancel);
    });

    combineLatest(this.operationState$, this.isUndoable$).subscribe((value) => {
      const state = value[0];
      const isUndoable = value[1];
      if (state === OperationState.Processed) {
        this.canUndo = isUndoable;
      } else {
        this.canUndo = false;
      }
      this.canUndoSubject$.next(this.canUndo);
    });
  }

  public setCancelOperation(subscription: Subscription) {
    if (!subscription) {
      throw Error('Não é possível setar uma operação de cancelar com a subscrição nula (null) ou indefinida (undefined)!');
    }
    if (subscription && subscription.closed === false) {
      this.cancelOperation = subscription;
      this.isCancellableSubject$.next(true);
    }
  }

  public setUndoOperation(undoFunction: (() => any) | Observable<any> | PromiseLike<any>) {
    if (!undoFunction) {
      throw Error('Não é possível setar uma operação de desfazer com a operação nula (null) ou indefinida (undefined)!');
    } else {
      this.undoOperation = undoFunction;
      this.isUndoableSubject$.next(true);
    }
  }

  public setOperation(operation: Observable<any>) {
    const operationObservavel = operation
      .pipe(
        catchError((error) => {
          return this.setError(error);
        }),
        finalize(() => {
          this.setAsProcessed();
        }),
      );

    this.operation$ = operationObservavel;
  }

  public setAsProcessed(): void {
    this.setState(OperationState.Processed);
  }

  public setError(error: Error) {
    this.operationStateSubject$.next(OperationState.Error);
    this.error = error;
    return throwError(error);
  }

  protected setState(operationState: OperationState) {
    this.operationStateSubject$.next(operationState);
  }

  setAsStarted() {
    this.setState(OperationState.Processing);
  }

  public undo() {
    if (!this.isUndoableSubject$.getValue()) {
      throw Error('Não é possível desfazer esta operação!');
    }
    let retorno = null;
    this.operationStateSubject$.next(OperationState.Undoing);

    const undoOperation = this.undoOperation;
    const observable = undoOperation as Observable<any>;
    const isObservable = observable.pipe;
    if (isObservable) {
      retorno = observable.pipe(catchError(error => this.setError(error)))
        .subscribe(() => this.operationStateSubject$.next(OperationState.Undone));
    }

    const promise = undoOperation as PromiseLike<any>;
    const isPromise = promise.then;
    if (isPromise) {
      retorno = promise.then(
        () => this.operationStateSubject$.next(OperationState.Undone),
        (error) => this.setError(error)
      );
    }

    const undoLambda = undoOperation as (() => any);
    const isUndoLambda = undoLambda.call;
    if ((!isObservable) && (!isPromise) && isUndoLambda) {
      try {
        retorno = undoLambda();
        this.operationStateSubject$.next(OperationState.Undone);
      } catch (error) {
        this.setError(error);
        throw error;
      }
    }

    return retorno;
  }


  public cancel() {
    if (!this.canCancel) {
      throw Error('Não é possível cancelar esta operação!');
    }
    this.operationStateSubject$.next(OperationState.Cancelling);
    if (this.cancelOperation && this.cancelOperation.closed === false) {
      this.cancelOperation.add(() => {
        this.operationStateSubject$.next(OperationState.Cancelled);
        this.isCancellableSubject$.next(false);
      });
      this.cancelOperation.unsubscribe();
    }
  }
}
