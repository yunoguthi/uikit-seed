import { Injectable } from '@angular/core';
import { OperationsService } from './operations.service';
import { timer } from 'rxjs';
import { OperationsComponent } from './operations.component';
import { MatSnackBar } from '@angular/material';

@Injectable()
export class OperationsManagerService {
  private isLoadSnackbarActive() {
    return !(this.snackBar._openedSnackBarRef &&
      this.snackBar._openedSnackBarRef.instance instanceof OperationsComponent);
  }

  constructor(protected operationsService: OperationsService, protected snackBar: MatSnackBar) {

    // Atualiza a variavel conforme o observable atualizar de valor
    this.operationsService.isLoading$.subscribe((isLoading) => {
      if (isLoading) {
        // Verifico se o snackbar (de operações) já está sendo exibida
        // para evitar abrir outra (ocasionando em um fecha/abre desnecessário)
        if (this.isLoadSnackbarActive()) {

          const loadingMaterialDialog = this.snackBar.openFromComponent(OperationsComponent,
            {
              verticalPosition: 'bottom',
              horizontalPosition: 'center',
              panelClass: 'operation-dialog',
              announcementMessage: 'Processando...',
            }
          );
        }
      } else {
        timer(2500).subscribe((loading) => {
          if (!this.operationsService.isLoading) {
            this.close();
          }
        });
      }
    });

  }

  public close() {
    this.snackBar.dismiss();
  }
}
