import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatDialogModule, MatProgressSpinnerModule } from '@angular/material';
import { UpdateComponent } from './update.component';
import { UpdateService } from './update.service';
import { UpdateInfoService } from './update-info.service';


@NgModule({
  entryComponents: [UpdateComponent],
  declarations: [UpdateComponent],
  imports: [
    CommonModule,
    MatDialogModule,
    MatProgressSpinnerModule
  ],
  exports: [UpdateComponent]
})
export class UpdateModule {
  constructor(public updateService: UpdateService,
    public updateInfoService: UpdateInfoService) {

  }
}
