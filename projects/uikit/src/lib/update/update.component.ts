import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { UpdateInfoService } from './update-info.service';

@Component({
  selector: 'app-update',
  template: `
  <mat-progress-spinner [color]="'primary'" [mode]="'indeterminate'"></mat-progress-spinner>
  <div *ngIf="!(hasUpdate$ | async);else hasUpdate">
    Carregando...
  </div>
  <ng-template #hasUpdate>
    Atualizando...
  </ng-template>
  `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UpdateComponent {

  public hasUpdate$ = this.updateInfoService.hasUpdate$;

  constructor(protected updateInfoService: UpdateInfoService) { }

}
