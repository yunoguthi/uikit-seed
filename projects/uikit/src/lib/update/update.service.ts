import { Injectable, Optional, ApplicationRef, OnDestroy, AfterViewInit, NgZone } from '@angular/core';
import { SwUpdate, UpdateAvailableEvent, UpdateActivatedEvent } from '@angular/service-worker';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material';
import { UpdateComponent } from './update.component';
import { first, concat, filter } from 'rxjs/operators';
import { combineLatest, interval, Subscription } from 'rxjs';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { UpdateInfoService } from './update-info.service';
import { LogService } from '../utils/log/log.service';
import { resetStores } from '@datorama/akita';
import { ToastService } from '../layout/toast/toast.service';

@Injectable({ providedIn: 'root' })
export class UpdateService implements OnDestroy {

  private primeiroAcesso = true;
  private hasUpdate = false;
  private updateSubscription = Subscription.EMPTY;

  private matDialogRef: MatDialogRef<UpdateComponent> = null;

  constructor(
    protected matDialog: MatDialog,
    protected toastService: ToastService,
    protected appRef: ApplicationRef,
    protected ngZone: NgZone,
    protected router: Router,
    @Optional() protected swUpdate: SwUpdate,
    @Optional() protected logService: LogService,
    protected updateInfoService: UpdateInfoService
  ) {

    if (this.swUpdate && this.swUpdate.isEnabled) {

      navigator.serviceWorker.getRegistrations().then(registrations => {
        const possuiSwRegistrado = registrations.length > 0;
        if (possuiSwRegistrado) {
          // Mostra dialogo para carregando atualizações
          this.matDialogRef = this.matDialog.open(UpdateComponent,
            { disableClose: true, hasBackdrop: true } as MatDialogConfig<UpdateComponent>);
        }
      });

      this.updateSubscription = this.updateInfoService.hasUpdate$.subscribe(hasUpdate => this.hasUpdate = hasUpdate);

      this.swUpdate.available.subscribe((event: UpdateAvailableEvent) => {
        this.updateInfoService.setUpdateAsAvailable();
        this.logService.info(`Current version is: ${JSON.stringify(event.current)}`);
        this.logService.info(`Available version is: ${JSON.stringify(event.available)}`);

        if (this.primeiroAcesso) {
          this.update();
        } else {
          if (this.matDialogRef) {
            this.matDialogRef.close();
          }
        }
      });

      this.swUpdate.activated.subscribe((event: UpdateActivatedEvent) => {
        this.logService.info(`Old version was: ${JSON.stringify(event.previous)}`);
        this.logService.info(`New version is: ${JSON.stringify(event.current)}`);

        localStorage.setItem('updated', 'true');
        this.logService.debug(`Atualizando...`);

        try {
          resetStores();
        } catch (error) {
          this.logService.error(error);
        }
        document.location.reload(true);
      });

      const appIsStable$ = appRef.isStable.pipe(first(isStable => isStable === true));
      const everyTime$ = interval(1 * 60 * 5000);
      const everyTimeOnceAppIsStable$ = appIsStable$.pipe(concat(everyTime$));

      everyTimeOnceAppIsStable$.subscribe(() => this.checkUpdate());

      this.checkUpdate();
    }

    if (localStorage.getItem('updated') === 'true') {
      this.toastService.success('Atualização realizada com sucesso!');
      localStorage.removeItem('updated');
    }
  }

  ngOnDestroy(): void {
    if (this.updateSubscription && this.updateSubscription.closed) {
      this.updateSubscription.unsubscribe();
    }
  }

  public update() {
    if (this.matDialogRef == null) {
      this.matDialogRef = this.matDialog.open(UpdateComponent,
        { disableClose: true, hasBackdrop: true } as MatDialogConfig<UpdateComponent>);
    }

    this.swUpdate.activateUpdate();
  }

  private checkUpdate() {
    this.logService.debug(`Verificando por atualização...`);
    this.swUpdate.checkForUpdate()
      .then(() => {
        this.logService.debug('checkForUpdate - then');
        this.ngZone.run(() => {
          this.primeiroAcesso = false;
          if (!this.hasUpdate) {
            this.logService.debug(`this.matDialogRef`);
            if (this.matDialogRef) {
              this.logService.debug(`this.matDialogRef.close()`);
              this.matDialogRef.close();
            }
          }
        });
      }).catch(error => {
        this.logService.debug('checkForUpdate - error');
        this.logService.debug(error);
        this.ngZone.run(() => {
          let erro = null;
          if (error.message) {
            erro = error.message;
          } else {
            erro = error;
          }
          this.logService.error(erro);
          if (this.matDialogRef) {
            this.matDialogRef.close();
          }
        });
      });
  }
}
