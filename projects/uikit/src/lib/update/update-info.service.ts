import { Injectable, Optional } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { SwUpdate } from '@angular/service-worker';
import { LogService } from '../utils/log/log.service';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class UpdateInfoService {

  protected hasUpdateSubject$ = new BehaviorSubject<boolean>(false);
  public hasUpdate$ = this.hasUpdateSubject$.asObservable();

  protected installPromptEventSubject$ = new BehaviorSubject<any>(null);
  protected installPromptEvent$ = this.installPromptEventSubject$.asObservable();

  public hasInstallOption$ = this.installPromptEvent$.pipe(map(r => r != null));

  constructor(
    @Optional() protected swUpdate: SwUpdate,
    protected logService: LogService) {



    window.addEventListener('beforeinstallprompt', (event) => {
      this.logService.debug('beforeinstallprompt fired!');
      // Prevent Chrome <= 67 from automatically showing the prompt
      // event.preventDefault();


      // Stash the event so it can be triggered later.
      this.installPromptEventSubject$.next(event);

      // Update the install UI to notify the user app can be installed
      // (doing that by the btnInstall that is displayed if there a installPromptEvent instance)
      //(<any>document.querySelector('#btnInstall')).disabled = false;
    });

  }

  public setUpdateAsAvailable() {
    this.logService.debug(`Atualização encontrada!`);
    this.hasUpdateSubject$.next(true);
  }


  public install() {
    this.logService.debug('install fired!');
    const installPromptEvent = this.installPromptEventSubject$.getValue();
    if (installPromptEvent) {
      this.logService.debug('installPromptEvent');
      installPromptEvent.prompt();
      // Wait for the user to respond to the prompt
      installPromptEvent.userChoice.then((choice) => {
        if (choice.outcome === 'accepted') {
          this.logService.debug('User accepted the A2HS prompt');
        } else {
          this.logService.debug('User dismissed the A2HS prompt');
        }
        // Clear the saved prompt since it can't be used again
        this.installPromptEventSubject$.next(null);
      });
    }
  }


}
