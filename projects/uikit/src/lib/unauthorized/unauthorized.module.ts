import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UnauthorizedRoutingModule } from './unauthorized-routing.module';
import { UnauthorizedComponent } from './unauthorized.component';
import { MatButtonModule } from '@angular/material';

@NgModule({
  entryComponents: [],
  declarations: [UnauthorizedComponent],
  imports: [
    CommonModule,
    UnauthorizedRoutingModule,
    MatButtonModule
  ],
})
export class UnauthorizedModule { }
