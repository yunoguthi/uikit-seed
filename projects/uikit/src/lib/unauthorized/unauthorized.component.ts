import {ChangeDetectionStrategy, Component, OnInit, Inject} from '@angular/core';
import {Location} from '@angular/common';
import {Router} from '@angular/router';
import {AuthenticationService} from '../shared/auth/authentication/authentication.service';
import {UserService} from '../shared/auth/authentication/user.service';
import {map} from 'rxjs/operators';
import {ToastService} from '../layout/toast/toast.service';
import {PreviousRouteService} from '../components/previous-route.service';
import { IAuthenticationServiceToken } from '../shared/auth/authentication/abstraction/provider-authentication-service.token';
import { IProviderAuthenticationService } from '../shared/auth/authentication/abstraction/provider-authentication.service';
import {OidcAuthenticationService} from '../shared/auth/authentication-oidc/oidc-authentication.service';

declare function require(path: string);

@Component({
  selector: 'uikit-unauthorized',
  templateUrl: './unauthorized.component.html',
  styleUrls: ['./unauthorized.component.scss']
})
export class UnauthorizedComponent implements OnInit {
  imageSrc = require('@cnj/uikit/lib/theme/uikit/images/error_403.png');
  public isAuthenticated$ = this.userService.user$.pipe(map((user) => user.authenticated));
  public isLoading = false;
  constructor(
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private ngLocation: Location,
    private toastService: ToastService,
    private previousRouteService: PreviousRouteService,
    private router: Router,
    @Inject(IAuthenticationServiceToken) private providerAuthenticationService: IProviderAuthenticationService,
  ) {
  }

  ngOnInit() {
    if ((this.providerAuthenticationService as OidcAuthenticationService).needRenewUser) {
      this.isLoading = true;
    }

    const loginQueryParam = this.router.routerState.root.snapshot.queryParamMap.get('login');
    if (loginQueryParam === 'auto' && (!(this.providerAuthenticationService as OidcAuthenticationService).needRenewUser)) {
      this.login();
    }
  }

  public login() {
      try {
        const paginaAnterior = this.previousRouteService.getPreviousUrl();
        this.authenticationService.login({enderecoParaVoltar: paginaAnterior});
      } catch (error) {
        this.toastService.error('Error ao realizar o login!', error as Error);
      }
  }

  public goBack() {
    this.ngLocation.back();
  }

}
