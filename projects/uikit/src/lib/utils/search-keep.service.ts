import {Injectable} from '@angular/core';
import {GrupoPesquisa, ItemPesquisa} from '../layout/header/search/search.model';
import {SearchQuery} from '../layout/header/search/state/search.query';
import {SearchStore} from '../layout/header/search/state/search.store';
import * as _ from 'lodash';
import {SearchAbstractService} from './abstract/search-abstract.service';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {MenuItem} from '../layout/nav/menu/menu-item/menu-item.model';
import {LayoutService} from '../layout/layout.service';
import {guid} from '@datorama/akita';
import * as Fuse from 'fuse.js';


@Injectable()
export class SearchKeepService implements SearchAbstractService {

  protected searchGroups: Observable<MenuItem[]> = this.layoutService.menuItemsFlattened$;
  protected searchGroupsAll: Observable<MenuItem[]> = this.layoutService.menuItems$;

  constructor(protected layoutService: LayoutService,
              private searchQuery: SearchQuery,
              private searchStore: SearchStore) {
  }

  /*
* Método responsável por adicionar um novo grupo na pesquisas.
* */
  addGroup(groups: GrupoPesquisa[]) {
    const itensGroups = this.searchQuery.getAll();
    if (_.head(itensGroups) !== undefined) {
      this.searchStore.adicionarGrupoPesquisa(itensGroups, groups);
    } else {
      this.searchStore.addGroupBase(groups);
    }
  }

  /*
    * Método responsável por remover um grupo da pesquisa.
    * */
  removeGroup(group: GrupoPesquisa) {
    this.searchStore.removerGrupoPesquisa(group.title);
  }

  /*
   * Método responsável por realizar a pesquisa dos grupo na pesquisa.
   * */
  searchGroup(term: string): Observable<GrupoPesquisa | ItemPesquisa> {
    return this._filterGroup(term);
  }

  private _filterGroup(value: string): Observable<GrupoPesquisa | ItemPesquisa> {
    const options = {
      keys: ['title', 'tags',
        'children.title', 'children.tags',
        'children.children.title', 'children.children.title.tags',
        'children.children.children.title', 'children.children.children.title.tags']
    };

    return this.searchGroupsAll.pipe(
      map(listMenuItem => this.applyFilter(listMenuItem, value, options)),
      map(itens => this.groupReturnFilter(itens)));
  }

  private applyFilter(listMenuItem: MenuItem[], value: string, options: { keys: string[] }) {
    const fuse = new Fuse(listMenuItem, options);
    return fuse.search(value);
  }

  private groupReturnFilter(itens: MenuItem[]): GrupoPesquisa | ItemPesquisa {

    const functionMapItem = (group: MenuItem) => {
      return {
        id: group.id ? group.id : guid(),
        title: group.title,
        icon: group.icon,
        link: group.link,
        tags: group.tags
      } as ItemPesquisa;
    };

    const functionMapGroup = (group: MenuItem) => {

      let groupMenu: GrupoPesquisa | ItemPesquisa;

      if (group.link) {
        groupMenu = functionMapItem(group);
      } else {
        groupMenu = {
          id: group.id ? group.id : guid(),
          title: group.title,
          icon: group.icon,
          isHeader: true,
        } as GrupoPesquisa;

        if (group.children && group.children.length > 1) {

          const itemFilhos = group.children
            .filter(b => b !== group)
            .map((itemIt) => {
              if (itemIt.children && itemIt.children.length > 1) {
                return functionMapGroup(itemIt);
              } else {
                return functionMapItem(itemIt);
              }
            });

          if (itemFilhos as ItemPesquisa[]) {
            groupMenu.items = itemFilhos as ItemPesquisa[];
          } else {
            groupMenu.items = itemFilhos as GrupoPesquisa[];
          }
        }
      }

      return groupMenu;
    };

    return {
      id: guid(),
      title: 'Menu',
      isHeader: true,
      items: itens.map(functionMapGroup)
    } as GrupoPesquisa;
  }
}
