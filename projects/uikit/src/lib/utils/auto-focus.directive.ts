import {AfterContentInit, Directive, ElementRef, Input} from '@angular/core';

@Directive({
  selector: '[autofocus]'
})
export class AutoFocusDirective implements AfterContentInit {

  @Input() public appAutoFocus: boolean;

  public constructor(private el: ElementRef) {

  }

  public ngAfterContentInit() {

    setTimeout(() => {
      this.hideKeyboard(this.el.nativeElement);
    }, 500);

  }

  hideKeyboard(el: any) {
    const agent = window.navigator.userAgent.toLowerCase();
    const att = document.createAttribute('readonly');
    el.setAttributeNode(att);
    setTimeout(() => {
      el.blur();
      const isSafari = (agent.indexOf('safari') !== -1) && (!(agent.indexOf('chrome') > -1));
      if (!isSafari) {
        el.focus();
      }
      el.removeAttribute('readonly');
    }, 100);
  }
}
