import {Injectable, InjectionToken} from '@angular/core';
import {GrupoPesquisa, ItemPesquisa} from '../../layout/header/search/search.model';
import {Observable} from 'rxjs';

export const SEARCH_TOKEN = new InjectionToken<SearchAbstractService>('SearchAbstractService');

@Injectable()
export abstract class SearchAbstractService {

  protected constructor() {
  }

  abstract searchGroup(term: string): Observable<GrupoPesquisa | ItemPesquisa>;

  abstract addGroup(group: GrupoPesquisa[]);

  abstract removeGroup(group: GrupoPesquisa);
}
