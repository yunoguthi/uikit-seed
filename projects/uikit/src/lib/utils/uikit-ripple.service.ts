import {Injectable, OnInit} from '@angular/core';

@Injectable({ providedIn: 'root' })
export class UikitRrippleService {
  constructor() {
  }

  init() {
    document.addEventListener('mousedown', this.rippleMouseDown, false);
  }

  rippleMouseDown(e) {
    const parentNode = 'parentNode';

    const isVisible = (el) => {
      return !!(el.offsetWidth || el.offsetHeight);
    }

    const selectorMatches = (el, selector) => {
      const matches = 'matches';
      const webkitMatchesSelector = 'webkitMatchesSelector';
      const mozMatchesSelector = 'mozMatchesSelector';
      const msMatchesSelector = 'msMatchesSelector';
      const p = Element.prototype;
      const f = p[matches] || p[webkitMatchesSelector] || p[mozMatchesSelector] || p[msMatchesSelector] || function (s) {
        return [].indexOf.call(document.querySelectorAll(s), this) !== -1;
      };
      return f.call(el, selector);
    }

    const addClass = (element, className) => {
      if (element.classList) {
        element.classList.add(className);
      } else {
        element.className += ' ' + className;
      }
    }

    const hasClass = (element, className) => {
      if (element.classList) {
        return element.classList.contains(className);
      } else {
        return new RegExp('(^| )' + className + '( |$)', 'gi').test(element.className);
      }
    }

    const removeClass = (element, className) => {
      if (element.classList) {
        element.classList.remove(className);
      } else {
        element.className = element.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
      }
    }

    const getOffset = (el) => {
      const rect = el.getBoundingClientRect();

      return {
        top: rect.top + (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0),
        left: rect.left + (window.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft || 0),
      };
    }

    const rippleEffect = (element, e) => {
      if (element.querySelector('.ink') === null) {
        const inkEl = document.createElement('span');
        addClass(inkEl, 'ink');

        if (hasClass(element, 'ripplelink') && element.querySelector('span')) {
          element.querySelector('span').insertAdjacentHTML('afterend', '<span class=\'ink\'></span>');
        } else {
          element.appendChild(inkEl);
        }
      }

      const ink = element.querySelector('.ink');
      removeClass(ink, 'ripple-animate');

      if (!ink.offsetHeight && !ink.offsetWidth) {
        const d = Math.max(element.offsetWidth, element.offsetHeight);
        ink.style.height = d + 'px';
        ink.style.width = d + 'px';
      }

      const x = e.pageX - getOffset(element).left - (ink.offsetWidth / 2);
      const y = e.pageY - getOffset(element).top - (ink.offsetHeight / 2);

      ink.style.top = y + 'px';
      ink.style.left = x + 'px';
      ink.style.pointerEvents = 'none';
      addClass(ink, 'ripple-animate');
    }

    for (let target = e.target; target && target !== this; target = target[parentNode]) {
      if (!isVisible(target)) {
        continue;
      }

      if (selectorMatches(target, '.ripplelink, .ui-button, .ui-listbox-item, .ui-multiselect-item, .ui-fieldset-toggler')) {
        const element = target;
        rippleEffect(element, e);
        break;
      }
    }
  }
}
