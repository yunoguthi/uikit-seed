import { BehaviorSubject, Subscription } from 'rxjs';
import { Injectable, OnDestroy, Inject, InjectionToken, Optional } from '@angular/core';

export enum LogLevel {
  Debug = 0,
  Info = 1,
  Warn = 2,
  Error = 3
}

export interface Log {
  message: string;
  level: LogLevel;
}

@Injectable()
export class LogService {

  protected loggerSubject$ = new BehaviorSubject<Log>({ message: 'Log iniciado!', level: LogLevel.Debug });

  public logger$ = this.loggerSubject$.asObservable();

  private infosToMessage(...infos: any[]) {
    return infos.join(' ');
  }

  protected log(level = LogLevel.Debug, ...infos: any[]) {
    const message = this.infosToMessage(...infos);
    this.loggerSubject$.next({ message, level });
  }

  public debug(...data: any[]): void {
    this.log(LogLevel.Debug, ...data);
  }

  public info(...data: any[]): void {
    this.log(LogLevel.Info, ...data);
  }

  public warn(...data: any[]): void {
    this.log(LogLevel.Warn, ...data);
  }

  public error(...data: any[]): void {
    this.log(LogLevel.Error, ...data);
  }

}


export const LOG_CONSUMER_SERVICE = new InjectionToken<LogConsumer>('LOG_CONSUMER_SERVICE');

@Injectable()
export class LogConsumersService {
  constructor(
    protected logService: LogService,
    @Inject(LOG_CONSUMER_SERVICE) @Optional() services?: LogConsumer[],
  ) {
    if (services && services.length > 0) {
      services.forEach(service => service.configure(logService));
    }
  }
}

export abstract class LogConsumer implements OnDestroy {

  private logSubscription: Subscription = new Subscription();

  public minimalConsumerLevel: LogLevel = LogLevel.Debug;

  protected consume(log: Log) {
    if (log.level >= this.minimalConsumerLevel) {
      if (log.level > 0) {
        this.consumeDebug(log.message);
      } else if (log.level > 1) {
        this.consumeInfo(log.message);
      } else if (log.level > 2) {
        this.consumeWarn(log.message);
      } else if (log.level > 3) {
        this.consumeError(log.message);
      }
    }
  }

  public configure(logService: LogService) {
    this.logSubscription.add(logService.logger$.subscribe((log) => this.consume(log)));
  }

  protected abstract consumeDebug(message: string);
  protected abstract consumeInfo(message: string);
  protected abstract consumeWarn(message: string);
  protected abstract consumeError(message: string);


  ngOnDestroy(): void {
    if (this.logSubscription && !this.logSubscription.closed) {
      this.logSubscription.unsubscribe();
    }
  }
}

@Injectable()
export class LogConsoleConsumerService extends LogConsumer {
  protected consumeDebug(message: string) {
    console.debug(message);
  }
  protected consumeInfo(message: string) {
    console.info(message);
  }
  protected consumeWarn(message: string) {
    console.warn(message);
  }
  protected consumeError(message: string) {
    console.error(message);
  }
}
