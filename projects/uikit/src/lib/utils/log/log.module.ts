import { NgModule, SkipSelf, Optional } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LogService, LogConsumersService } from './log.service';

@NgModule({
  declarations: [

  ],
  imports: [
    CommonModule
  ],
  providers: [
    LogService,
    LogConsumersService
  ],
  exports: [

  ]
})
export class LogModule {
  constructor(
    public logConsumersService: LogConsumersService,
    @Optional() @SkipSelf() parentModule?: LogModule,
    ) {
      if (parentModule) {
        throw new Error(
          'LogModule is already loaded. Import it in the AppModule only');
      }
  }
}
