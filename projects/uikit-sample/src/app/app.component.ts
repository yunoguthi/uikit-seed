import { Component } from '@angular/core';

@Component({
  selector: 'uikit-sample-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor() {}
}
