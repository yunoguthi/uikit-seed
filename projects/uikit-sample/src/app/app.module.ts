import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { environment } from '../environments/environment';
import { AkitaNgDevtools } from '@datorama/akita-ngdevtools';
import { AkitaNgRouterStoreModule } from '@datorama/akita-ng-router-store';
import { HttpClientModule } from '@angular/common/http';
import { UikitModule, NotFoundModule, UnauthorizedModule, AuthModule, OidcAuthModule, OIDC_CONFIG } from 'projects/uikit/src/public_api';

/*export function initializeApp(appConfigService: AppConfigService) {
  return (): Promise<AppConfig> => appConfigService.load();
}*/

const configServiceFactory = (): Oidc.UserManagerSettings => {

  //const authenticationSettings = AppConfigService.settings.authentication;
  //return authenticationSettings;
  return {};
};

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    UikitModule,
    NotFoundModule,
    UnauthorizedModule,
    AuthModule,
    OidcAuthModule,
    environment.production ? [] : [AkitaNgDevtools.forRoot(), AkitaNgRouterStoreModule.forRoot()],
    AppRoutingModule
  ],
  providers: [
    /*AppConfigService,
    { provide: APP_INITIALIZER, useFactory: initializeApp, deps: [AppConfigService], multi: true },*/
    { provide: OIDC_CONFIG, useFactory: configServiceFactory }
  ],
  bootstrap: [AppComponent],
  exports: [],
  entryComponents: []
})
export class AppModule {}
