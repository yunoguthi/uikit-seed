import { Component, OnInit, ViewEncapsulation } from '@angular/core';

export interface Section {
  description: string;
  read: boolean;
}

@Component({
  selector: 'uikit-sample-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DashboardComponent implements OnInit {
  typesOfShoes: Section[] = [
    {
      description:
        'Despachar o processo 000912.2323.423.3535 para o gabinete do Dr. Ricardo, no dia 15/12/2018. Ligar para confirmar recebimento.',
      read: true
    },
    {
      description:
        'Despachar o processo 000912.2323.423.3535 para o gabinete do Dr. Ricardo, no dia 15/12/2018. Ligar para confirmar recebimento.',
      read: false
    },
    {
      description:
        'Despachar o processo 000912.2323.423.3535 para o gabinete do Dr. Ricardo, no dia 15/12/2018. Ligar para confirmar recebimento.',
      read: false
    }
  ];
  constructor() {}

  ngOnInit() {}
}
