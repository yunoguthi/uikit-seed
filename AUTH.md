# Autenticação e Autorização

## Introdução

No mundo da autenticação e autorização existem múltiplos protocolos/padrões (Kerberus, LDAP, NTLM, etc), mas o mais utilizado para aplicações ["Single Page Application"](https://pt.wikipedia.org/wiki/Aplicativo_de_p%C3%A1gina_%C3%BAnica) é o [OAuth 2](https://oauth.net/2/). Mas este nem sempre é suficiente para abranger todos os casos.

_Vamos contextualizar sobre alguns dos mais conhecidos e utilizados atualmente em aplicações web:_

- ### OAuth 2

  [OAuth 2](https://oauth.net/2/) basicamente foca em padronizar **como o usuario** (Resource Owner) **concede acesso** por meio de um serviço de autorização (Authorization Server - que também é comumente usado como Identity Provider) **a um recurso** (Resource Server / API) **para um sistema** (Client).
    > **Resumo:** As informações de autorização ficam disponíveis no `access_token` obtido por meio do [token endpoint](https://tools.ietf.org/html/rfc6749#section-3.2) (OAuth 2).

- ### OpenID Connect

  O padrão [OAuth 2](https://oauth.net/2/) foi extendido criando-se o padrão [OpenID Connect](https://openid.net/connect/) para incluir também as informações da identidade do usuário (Resource Owner).
    > **Resumo:** As informações de autenticação ficam disponíveis no `id_token` (OpenID Connect) e podem ser obtidos também por meio do [userinfo endpoint](https://openid.net/specs/openid-connect-core-1_0.html#UserInfo).

- ### User Managed Access (UMA)

  O padrão [OAuth 2](https://oauth.net/2/) foi extendido também para padronizar a autorização granular baseado em atributos ([ABAC](https://en.wikipedia.org/wiki/Attribute-based_access_control)) assim como outros cenários ('compartilhamento' de permissão entre usuários), criando-se então o padrão [User Managed Access](https://kantarainitiative.org/confluence/display/uma/Home) ([Wiki](https://en.wikipedia.org/wiki/User-Managed_Access), [Artigo 1](https://medium.com/@dewni.matheesha/user-managed-access-uma-2-0-bcecb1d535b3)).
    > **Resumo:** As verificações de permissão são validadas no [token endpoint](https://tools.ietf.org/html/draft-maler-oauth-umagrant-00#section-3.3.1), que utiliza do [endpoint de permissão](https://tools.ietf.org/html/draft-maler-oauth-umafedauthz-00#section-4.1) para criação do ticket (que é necessário para validar a autorização no token endpoint)
    >  > _O endpoint de autorização é muitas vezes chamado/considerado também como [Policy Enforcement Point (PEP)](https://ldapwiki.com/wiki/Policy%20Enforcement%20Point) ou [Policy Decision Point (PDP)](https://ldapwiki.com/wiki/Policy%20Decision%20Point)_


- ### eXtensible Access Control Markup Language (XACML)

  O padrão [XACML](https://www.oasis-open.org/committees/xacml/) ([Wiki](https://en.wikipedia.org/wiki/XACML)) foi criado com o objetivo de representar e avaliar as políticas de autorização e de controle de acesso.

    > _Permite deixar no repositório de código-fonte de um produto, todas as regras para ser importada em um serviço de autorização (deixando desacoplado a escolha do serviço de autorização, contanto que suporte xacml)._

  _Ela foi criada em 2003 baseada em linguagem de marcação XML e vem sendo evoluída desde então até [suportando modelo REST](https://en.wikipedia.org/wiki/XACML#New_in_XACML_3.0) ([com JSON](https://en.wikipedia.org/wiki/XACML#The_JSON_Profile_of_XACML)) em suas últimas versões._

  > **Resumo:** As verificações de permissão são validadas no [Policy Decision Point](http://docs.oasis-open.org/xacml/xacml-rest/v1.0/cos01/xacml-rest-v1.0-cos01.html#_Toc399235417) com [resposta no formato JSON](https://docs.oasis-open.org/xacml/xacml-json-http/v1.1/os/xacml-json-http-v1.1-os.pdf).
    > > _É uma especificação um tanto completa, mas também complexa, devido o objetivo de suportar a criação de regras, políticas e validações de autorização (com um sistema de votação). Se tratarmos sobre qual o padrão que permite definir os mais complexos cenários de autorização, com certeza o XACML é que pode ser considerado o mais robusto meio para tal._

### Conceitos

#### Authentication Server/Service

É a ferramenta responsável por autenticar o usuário. Nela está contida a forma de se "informar credenciais" e "retornar as informações do usuário".

##### Provedores de autenticação

  São as formas de se autenticar naquele Authentication Server, como por exemplo:

- Login e Senha
- PJe
- Google
- Facebook

##### Single Sign On (SSO)

Single Sign On é um conceito atribuído a um recurso de ferramentas de Autenticação onde múltiplos sistemas se autenticam.

O fato do usuário se autenticar no endereço (domínio) da ferramenta de autenticação (exemplo: https://sso.cnj.jus.br), faz com que os demais sistemas que utilizam deste não necessitem pedir que o usuário se autentique novamente.

> Cenário de exemplo do SSO com OAuth2:
> 
> 1. Usuário acessa o sistema1 (ex: https://sistema1.cnj.jus.br)
> 2. Sistema1 verifica que o usuário não esta autenticado no endereço/domínio dele (pela inexistência do access_token válido no navegador/localstorage) e solicita a autenticação (redirecionando para o endereço da ferramenta de SSO - ex: https://sso.cnj.jus.br)
> 3. SSO verifica que o usuário não esta autenticado no endereço/domínio dele (armazenado no navegador - semelhante como o sistema1 faz) e mostra tela de login
> 4. Usuário faz a autenticação
> 5. SSO guarda os dados do usuário autenticado no navegador no endereço dele e redireciona para o endereço do Sistema1
> 6. Sistema1 recebe as informações (via parâmetros querystring) e guarda os dados do usuário autenticado no navegador no endereço dele
>    > Neste momento o usuário está autenticado no sistema1 e no sso e pode utilizar o sistema1 normalmente
> 7. O usuário acessa o sistema2 (ex: https://sistema2.cnj.jus.br)
> 8. Assim como o Sistema1, o Sistema2 verifica que o usuário não esta autenticado no endereço/domínio dele e solicita a autenticação (redirecionando para o endereço da ferramenta de SSO - ex: https://sso.cnj.jus.br)
> 9. SSO verifica que o usuário **já está autenticado** no endereço/domínio dele e redireciona para o Sistema2 sem que peça para o usuário se autenticar novamente
> 
> ![Single Sign On](sso.jpg)



#### Authorization Server/Service

É a ferramenta responsável por informar se um usuário específico pode executar uma determinada operação.

Normalmente nesta ferramenta está o gerenciamento de perfis e permissão a nível granular (baseado em atributo).


## Conclusão

> _Obs: Normalmente a ferramenta "Authentication Server/Service" e "Authorization Server/Service" é a mesma, como no caso do [Keycloak](https://www.keycloak.org/) e [WSO2 Identity Server](https://wso2.com/identity-and-access-management/). No entanto, isto não é uma regra, podendo ser ferramentas diferentes (apesar de não-usual)._

Todas as especificações descritas são suportados no UIKit, no entanto, alguns padrões mínimos foram estabelecidos com o objetivo de simplicidade e flexibilidade.

# Autenticação

## Utilização

- Como autenticar um usuário:

```typescript
  // app.component.ts
  constructor(protected authenticationService: AuthenticationService) { }
  
  login() {
    this.authenticationService.login();
    // Login solicitado (não necessariamente foi executado)
    // OAuth2 (que é o padrão) precisa redirecionar para autenticar,
    // portanto para saber quando o usuário se autentica,
    // deve-se fazer o subscribe do `this.userService.user$`
    // e utilizar o `user.authenticated`
  }

  logout() {
    this.authenticationService.logout();
    // Logout solicitado (não necessariamente foi executado)
    // OAuth2 (que é o padrão) 'pode' (não obrigatóriamente)
    // redirecionar para autenticar (desautenticando do SSO),
    // portanto para saber quando o usuário se desautenticou,
    // deve-se fazer o subscribe do `this.userService.user$`
    // e utilizar o `user.authenticated`
  }
```

- Para proteger uma tela/componente para ser acessada SOMENTE por um usuário autenticado utilize a classe Guard ([CanActivate](https://angular.io/api/router/CanActivate)) `AuthenticationGuardService`:

```typescript
  // app-routing.module.ts
  const routes: Routes = [
  {
    path: 'teste',
    component: TesteComponent,
    canActivate: [ AuthenticationGuardService ],
    data: { login: 'manual' } // Este parâmetro permite configurar se deve solicitar o login manualmente ou automaticamente (caso o usuário não esteja autenticado) por meio dos valores 'auto' ou 'manual'. O padrão é 'auto' (se omitido).
  }
  ];
  //...
```

  > Obs: Esta aplicação deve ser feita para todas as telas que precisem de usuário autenticado. No caso de ser a aplicação inteira, é recomendado colocar em todas as rotas pais e informar no `canActivateChild` o `AuthenticationGuardService`. Neste caso, todas as rotas dentro do módulo estarão forçadas a ter o usuário autenticado.

  Exemplo:

```typescript
  // app-routing.module.ts
  const routes: Routes = [
  {
    path: 'breweries',
    loadChildren: './breweries/breweries.module#BreweriesModule',
    canActivateChild: [ AuthenticationGuardService ]
  }
  ];
  //...
```
  
> **Alternativamente**, é possível (porém **não recomendado**) fazer uma tratativa única para autenticar o usuário e bloquear seu acesso a outras funcionalidades:
>
> ```typescript
> // app.component.ts
>   ngOnInit() {
>     // Em cada mudança de rota (navegação entre páginas/componentes) ou de usuário (ex: anônimo -> autenticado)
>     const mudancaDeRota = this.router.events.pipe(filter(event => event instanceof NavigationEnd));
>     const mudancaDeUsuario = this.userService.user$;
>     combineLatest(mudancaDeUsuario, mudancaDeRota).subscribe((valores) => {
>       const user = valores[0];
>       // Verifico se usuário não está autenticado
>       if (user.authenticated === false) {
>         // Se não está autenticado, verifico se já foi solicitada a autenticação
>         // (faço o controle da solicitação de autenticação setando uma flag no localStorage)
>         const foiSolicitadaAutenticacao = localStorage.getItem('solicitada-autenticacao');
>         // Observação: A necessidade de fazer o controle é que o usuário inicial da aplicação é o anônimo
>         // (portanto authenticated = false) e só após o carregamento é emitido (pelo Observable)
>         // o novo usuário autenticado.
>         if (!foiSolicitadaAutenticacao) {
>           // Caso não tenha sido solicitada a autenticação, então seto a flag
>           localStorage.setItem('solicitada-autenticacao', 'true');
>           // Solicito a autenticação
>           this.authenticationService.login();
>         } else {
>           // Caso já tenha sido solicitada a autenticação, então redireciono
>           // para a página de 'não autorizado', pois o usuário pode ter cancelado a autenticação
>           // Passo parâmetro de `type=noshellnobreadcrumb` para não exibir o Shell e Breadcrumb
>           // (não permitindo que o usuário navegue em outras páginas)
>           this.router.navigate(['unauthorized'], { queryParams: { type: 'noshellnobreadcrumb' } });
>           // Nesta página o usuário terá a possibilidade de se autenticar,
>           // posteriormente voltando para a página original.
>         }
>       }
>     });
>   }
> ```
> A forma CORRETA de se bloquear o acesso de um usuário não autenticado é protegendo as rotas pelo parâmetro `canActivate` ou `canActivateChild` usando o `AuthenticationGuardService`.

## Resumo (TLDR)

- Se você quer só customizar (ou complementar) informações no usuário autenticado _(mantendo a autenticação por OAuth2)_, **implemente `IAuthenticationManager`** (veja tópico: [Interceptador do Usuário](#interceptador-do-usuário))
- Se você quer mudar a forma de autenticação, **estenda `ProviderAuthenticationService`** (veja tópico: [Customização](#customização))


## Provedores

Para permitir a máxima flexibilidade (em um cenário onde uma instituição utilize um meio customizado), foi criado uma abstração de autenticação que permite a implementação por qualquer meio disponível.

### Abstração

A interface [`IProviderAuthenticationService`](./projects/uikit/src/lib/shared/auth/authentication/abstraction/provider-authentication.service.ts) contida no pacote `@cnj/uikit` é o contrato necessário para se customizar ([implementando](https://www.typescriptlang.org/docs/handbook/interfaces.html#implementing-an-interface) e [registrando](https://angular.io/guide/hierarchical-dependency-injection#ngmodule-level-injectors)) uma forma de se autenticar (login/logout) em uma instituição/empresa.

Mas é recomendado [estender a classe abstrata](https://www.typescriptlang.org/docs/handbook/classes.html#abstract-classes) [`ProviderAuthenticationService`](./projects/uikit/src/lib/shared/auth/authentication/provider-authentication.service.ts) que ajuda na implementação dos métodos necessários para a criação de um provedor de autenticação customizado.


### Customização

É permitido criar uma forma customizada de se autenticar.

  Para criar seu provedor de autenticação é só [herdar](https://www.typescriptlang.org/docs/handbook/classes.html#inheritance) a classe [`ProviderAuthenticationService`](./projects/uikit/src/lib/shared/auth/authentication/provider-authentication.service.ts) e implementar os métodos:

- `login(param?: TLoginParam): Promise<void>` (obrigatório)
  > Que deverá usar o método `this.loadProviderUser(providerUser)` para carregar o usuário
- `transform(providerUser: TUserProvider): TUser` (obrigatório)
  > Que será utilizado para transformar o usuário do seu provedor para o da framework
- `logout(param?: TLogoutParam): Promise<void>` (opcional)
  > Se implementado, será utilizado para executar o logout do usuário _(utilize o `super.logout(param)` para deslogar o usuário)_

#### Exemplo

##### CustomAuthenticationService

```typescript
import { User, ProviderAuthenticationService } from '@cnj/uikit';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

/** Tipo contendo as informações necessárias para autenticar o usuário */
export interface Credential {
  username: string;
  password: string;
}

/** Resposta da API que autentica o usuário */
export interface UserResponse {
  id: number;
  name: string;
  email: string;
}

/** Os parâmetros genéricos são:
 * 1- TUserProvider (UserResponse neste exemplo): É o 'tipo do usuário do seu provider' (pode ser a tipagem da resposta de uma api como neste exemplo)
 * 2- TUser (User neste exemplo): É o 'tipo do usuário do framework' (pode até ser um usuário customizado, contanto que estenda o User - Ex: TjxxUser)
 * 3- TLoginParam (Credential neste exemplo): É o tipo do parâmetro utilizado no método de login
 * 4- TLogoutParam (void neste exemplo): É o tipo do parâmetro utilizado no método de logout
 * OBS: Estes parâmetros são 'opcionais' (pode usar sem informá-los, ex: `class CustomProvider extends ProviderAuthenticationService {...}`), mas informar estes tipos ajuda na implementação de seu provedor customizado.
 */
@Injectable()
export class MyCustomAuthenticationService extends ProviderAuthenticationService<UserResponse, User, Credential, void> {
  constructor(protected httpClient: HttpClient) {
    super();
  }
  /** Use este método para fazer o login do usuário e use o método `this.loadProviderUser(user)` para setar o usuário autenticado com sucesso */
  login(credential?: Credential): Promise<void> {
    return this.httpClient.post<UserResponse>('http://localhost:5000/api/authentication/login',
      { username: credential.username, password: credential.password })
      .toPromise().then(userResponse => this.loadProviderUser(userResponse));
  }
  /** Use este método para converter o usuário (ou por exemplo uma resposta de api) para um usuário da framework (`User`) */
  protected transform(usuario: UserResponse): User {
    const user = { sub: usuario.id.toString(), name: usuario.name, email: usuario.email, authenticated: true };
    return user;
  }
  /** Use este método para fazer algum tratamento específico (ex: registrar logoff)
   * Chame o método `super.logout(param)` para deslogar na aplicação.
   */
  logout(param?: void): Promise<void> {
    return super.logout(param);
  }
}
```

##### LoginComponent

```typescript
import { Component, OnInit } from '@angular/core';
import { Credential } from '../custom-authentication';
import { FormControl, FormBuilder } from '@angular/forms';
import { AuthenticationService } from '@cnj/uikit';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm = this.formBuilder.group({
    username: new FormControl(),
    password: new FormControl()
  });

  constructor(
    // Informar os parâmetros genericos aqui é para facilitar na tipagem do método login (mas é opcional, contanto que se passe o argumento necessário)
    // Sem os argumentos ficaria:
    // protected authenticationService: AuthenticationService,
    protected authenticationService: AuthenticationService<Credential, void>,
    private formBuilder: FormBuilder,
  ) {}

  ngOnInit() {}

  login() {
    const credential = {
      username: this.loginForm.value.username,
      password: this.loginForm.value.password
    };
    this.authenticationService.login({ param: credential });
  }
}
```


### Implementação Padrão

Foi criado e definido como padrão no projeto raiz (apesar de permitir ser alterado/customizado) a Autenticação via OpenID Connect, devido este ser a mais completa especificação utilizada para autenticação/identificação do usuário (resource owner).

- Configuração: `src/assets/config/config.json`

  ```json
  {
    "authentication": {
      "authority": "http://localhost:8080/auth/realms/master/",
      "client_id": "localhost",
      "client_uri": "http://localhost:4200/",
      "scope": "openid profile email roles offline_access"
    }
  }
  ```

  - `authority`: É a URL da ferramenta de autenticação (compatível com OAuth2)
  - `client_id`: É o identificado da aplicação cadastrada na ferramenta de autenticação
  - `client_uri`: É o endereço da aplicação (servirá para retornar a ela após o login e logout na ferramenta)
  - `scope`: São as informações necessárias para sua aplicação funcionar. É por meio delas que sua aplicação pede para a ferramenta de autenticação informações (claims) como nome (scope: profile), email (scope: email), perfis (scope: roles), etc. _Basicamente um scope é um agrupador de claims (informações)._
    > Este parâmetro (`scope`) é importante no caso de consumir múltiplas APIs onde nesta é obrigatório a presença de algumas informações, seja para validar regra de negócio (ex: somente usuário maior de idade pode fazer esta operação), verificar permissão (ex: somente usuário com perfil admin pode fazer esta operação) ou auditar (ex: registrar log que usuário X por meio do sistema Y executou uma ação Z).
    >
    > > _Observação: É uma prática recomendada que na documentação de uma API, se tenha escrito quais são os scopes necessários para o consumo desta. Que a partir disto, um sistema (frontend) pode pedir este scope na autenticação com o objetivo de consumir esta API._
    >
    > > _Veja mais informações sobre scope em: [Especificação](https://tools.ietf.org/html/rfc6749#section-3.3), [Artigo 1](https://www.oauth.com/oauth2-servers/scope/defining-scopes/) e [Artigo 2](https://docs.apigee.com/api-platform/security/oauth/working-scopes)_

  > _As configurações foram herdadas da biblioteca [oidc-client](https://github.com/IdentityModel/oidc-client-js/wiki#required-settings) mas incluída a informação do `client_uri` que serve de base para os campos `redirect_uri` (adicionado callback), `silent_redirect_uri` (adicionado silentrefresh) e `post_logout_redirect_uri` (mesmo valor do client_uri) quando não forem informados._
  > > Outros parâmetros podem ser utilizados para a ferramenta de autenticação por meio das configurações descrito em: https://github.com/IdentityModel/oidc-client-js/wiki#optional-authorization-request-settings .
  > >
  > > > _Como por exemplo em cenários mais complexos onde na ferramenta de autenticação é permitido se utilizar de múltiplos provedores (ex: google, facebook, pje, etc), e a sua aplicação deseja forçar a utilização de somente um destes, neste caso é comum utilizar o `acr_values` ou `extraQueryParams` para tal._
  > > > > No Keycloak é por meio do parâmetro `kc_idp_hint` ([Clique aqui para maiores informações](https://www.keycloak.org/docs/7.0/server_admin/#_client_suggested_idp))
  > > > > Ex:
  > > > > ```typescript
  > > > > "authentication": {
  > > > >   ...
  > > > >   "extraQueryParams": {
  > > > >     "kc_idp_hint": "pje"
  > > > >   }
  > > > > }
  > > > > ```
    > > Exemplo: É possível informar vários parâmetros necessários para configurar com uma ferramenta de OAuth2. Segue abaixo exemplo para teste com a ferramenta [WSO2 Identity Server](https://hub.docker.com/r/wso2/wso2is):
  > > ```json
  > > "authentication": {
  > >   "authority": "https://localhost:9443/",
  > >   "client_id": "1SkGEOVdVPc05rsjyXRHjfZsboMa",
  > >   "redirect_uri": "http://localhost:4200/callback",
  > >   "post_logout_redirect_uri": "http://localhost:4200/",
  > >   "response_type": "id_token token",
  > >   "scope": "openid profile email roles offline_access",
  > >   "silent_redirect_uri": "http://localhost:4200/silentrefresh",
  > >   "automaticSilentRenew": true,
  > >   "silentRequestTimeout": 10000,
  > >   "filterProtocolClaims": true,
  > >   "loadUserInfo": true,
  > >   "metadata": {
  > >     "issuer": "https://localhost:9443/oauth2/token",
  > >     "authorization_endpoint": "https://localhost:9443/oauth2/authorize",
  > >     "token_endpoint": "https://localhost:9443/oauth2/token",
  > >     "userinfo_endpoint": "https://localhost:9443/oauth2/userinfo",
  > >     "revocation_endpoint": "https://localhost:9443/oidc/logout",
  > >     "endsession_endpoint": "https://localhost:9443/oidc/logout",
  > >     "jwks_uri": "https://localhost:9443/oauth2/jwks"
  > >   }
  > > }
  > > ```

- Registro: `src/app/app.module.ts`

  ```typescript
  /** Fábrica responsável por buscar as configurações de autenticação */
  const configServiceFactory = (): OpenIDConnectSettings => {
    return environment.settings.authentication;
  };
  // ...
  /** Provider responsável por setar as configurações de autenticação */
  { provide: OIDC_CONFIG, useFactory: configServiceFactory },
  ```

## Interceptador do Usuário

Foi criado um meio para permitir a interceptação "antes da autenticação" como oportunidade de se fazer algum tratamento específico ou customizar as informações do usuário.

### Implementação

```typescript
import { HttpClient } from '@angular/common/http';
import { IAuthenticationManager, User, UserWithAuthorizations } from '@cnj/uikit';
import { Injectable } from '@angular/core';
import { environment } from './environment';
import { map } from 'rxjs/operators';

export interface Permission {
  resource: string;
  action: string;
}

export interface TjxxUser extends UserWithAuthorizations {
  // Qualquer outra informação necessária no seu produto/instituição (ex: matrícula, departamento, etc)
}

/**
 * Exemplo de um interceptador que inclui no usuário autenticado as informações de autorização.
 * Os parâmetros genéricos são:
 * 1- TFrameworkUser (User no exemplo): É usado para tipar o usuário do provider de autenticação (no caso do provider default (OpenID Connect) é o `OpenIDConnectUser`)
 * 2- TApplicationUser (TjxxUser no exemplo): É usado para tipar o usuário que será carregado na aplicação
 * OBS: Estes parâmetros são 'opcionais' (pode usar sem informá-los, ex: `class TjxxAuthenticationManager extends IAuthenticationManager {...}`), mas informar estes tipos ajuda na implementação.
 */
@Injectable()
export class TjxxAuthenticationManager implements IAuthenticationManager<OpenIDConnectUser, TjxxUser> {
  constructor(protected httpClient: HttpClient) {
  }
  transform(user: OpenIDConnectUser): Promise<TjxxUser> {
    const identificadorDoSistema = environment.settings.authentication.client_id;
    const identificadorDoUsuario = user.sub;
    return this.httpClient
      .get<Array<Permission>>(`http://localhost:5000/api/authorization/${identificadorDoSistema}/permissions/${identificadorDoUsuario}`)
      .pipe(map(response => {
        const authorizationsObject = { authorizations: response };
        const userToReturn = Object.assign(user, authorizationsObject) as TjxxUser;
        return userToReturn;
      }))
      .toPromise();
  }
}

```

- Registro: `src/app/app.module.ts`

```typescript
    { provide: IAuthenticationManagerToken, useClass: TjxxAuthenticationManager },
```

> Permite a utilização de cenários como:
> - Complementação do usuário autenticado com informações da instituição ou do produto (ex: matrícula, departamento, etc)
> - Complementação do usuário autenticado com informações de autorização (para ser utilizado nas verificações de permissão no sistema)


### Provedores

Direto da biblioteca `@cnj/uikit` já temos disponível os seguintes AuthenticationManagers:

- KeycloakAuthenticationManager (para a ferramenta [Keycloak](https://www.keycloak.org/))
  ```typescript
  { provide: IAuthenticationManagerToken, useClass: KeycloakAuthenticationManager },
  ```

- Wso2AuthenticationManager (para a ferramenta [WSO2 Identity Server](https://wso2.com/identity-and-access-management/))

  ```typescript
  { provide: IAuthenticationManagerToken, useClass: Wso2AuthenticationManager },
  ```

O objetivo destes provedores é a de buscar as informações de autorização para complementar o usuário autenticado. Este manager deve ser utilizado quando:
- A ferramenta de SSO for o [Keycloak](https://www.keycloak.org/) ou o [WSO2 Identity Server](https://wso2.com/identity-and-access-management/)
- As autorizações forem gerenciadas nesta ferramenta (citada anteriormente)
- E foi decidido usar a estratégia de verificação de autorização offline (não havendo necessidade de fazer um request http para verificar a autorização).
  > Existe a opção de verificação de autorização em real-time (fazendo requisição para cada verificação), veja mais no tópico de autorização _(para saber as motivações em que este cenário é preferido)_.

Caso não informe um AuthenticationManager, não haverá transformação de usuário autenticado, e prevalecerá o usuário do Provedor de Autenticação definido (o padrão é o usuário do tipo `OpenIDConnectUser` utilizado pelo `OidcAuthModule`). Porém, não ocorrerá a busca das informações de autorização (no momento da autenticação).
  > Só não informe um AuthenticationManager quando sua aplicação se encaixar em um dos seguintes cenários:
  >
  > - A aplicação não utiliza autenticação
  >   > Aplicação é pública (não necessita de autenticação) ou a autenticação é opcional (dando mais recursos ou acessos se autenticado)
  > - A aplicação não utiliza autorização baseada em atributos/permissões ([Attribute Based Access Control](https://en.wikipedia.org/wiki/Attribute-based_access_control#Attributes))
  >   > Autorização baseada em atributos é a utilização mais recomendada (veja mais no tópico autorização)
  > - A aplicação utiliza autorização baseada em papéis/perfis ([Role Based Access Control](https://en.wikipedia.org/wiki/Role-based_access_control))
  >   > Não recomendado (veja mais no tópico autorização)
  > - A aplicação irá verificar a autorização em tempo-real no momento da verificação 
  >   > Neste caso é dispensando a necessidade de armazenar as permissões/autorizações préviamente no usuário autenticado

> Q: Porque foi criado um AuthenticationManager específico para o Keycloak e WSO2 e não baseado em um padrão?
>
> R: Não existe um padrão que defina como as ferramentas devem retornar todas as permissões/autorizações de um usuário para um sistema, portanto a forma de consumir a API assim como sua resposta é de um formato diferente para cada ferramenta.


# Informações

Para se obter informações do usuário autenticado, deve-se utilizar do `UserService` definido no `@cnj/uikit`.
Neste serviço, as informações do usuário podem ser obtidas de duas formas: 

- Como [`Observable`](https://angular.io/guide/observables) (recomendado):

  ```typescript
  this.userService.user$
  ```

- Como propriedade (não recomendado):

  ```typescript
  const nomeDoUsuário = this.userService.userValue.name;
  ```

## Padrões

As informações do usuário autenticado está definido no arquivo `[user.model.ts](./projects/uikit/src/lib/shared/auth/authentication/user.model.ts)` com os seguintes tipos:

```typescript
export interface User {
  /** Identificador único do usuário naquele provedor de identidade */
  sub: string;
  /** Endereço do serviço provedor de identidade */
  iss?: string;
  /** Informa se o usuário foi autenticado */
  authenticated: boolean;
  /** Url da imagem do usuário */
  picture?: string;
  /** Nome do usuário */
  name: string;
  /** Nome de exibição escolhido pelo usuário */
  preferred_username?: string;
  /** Email principal do usuário */
  email: string;
  /** CPF do usuário */
  cpf?: string;
  /** Perfis do usuário para o sistema */
  roles?: Array<Role>;
}

export interface Role {
  /** Identificador do perfil do usuário no sistema */
  id?: string;
  /** Nome do perfil do usuário no sistema */
  name: string;
}

export type OAuthUser = User & {
  access_token: string;
  client_id: string;
};

export type OpenIDConnectUser = OAuthUser & {
  id_token: string;
};

export type UserWithAuthorizations = User & {
  authorizations?: Array<{ action: Array<string>; resource: string; client?: string }>;
  };
```

O tipo padrão utilizado pelo UIKit é o `OpenIDConnectUser` visto a utilização de autenticação baseado em [OpenID Connect](https://openid.net/connect/).

## Exemplo:

```typescript
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthenticationService, AuthorizationService, UserService, User } from '@cnj/uikit';

@Component({
  selector: 'app-algum',
  templateUrl: './algum.component.html',
  styleUrls: ['./algum.component.css']
})
export class AlgumComponent implements OnInit, OnDestroy {
  /**
   * O parâmetro genério "TUser" (Usado como User) é para tipar o usuário provido pelo serviço. Pode ser usado para setar a tipagem do usuário customizado definido pelo IAuthenticationManager ou IProviderAuthenticationService.
   */
  constructor(protected userService: UserService<User>) { }

  // Defina uma variável no seu componente contendo as informações do usuário para uso como async no html (ex: `<div>{{ (this.user$ | async).name }}</div>`)
  user$ = this.userService.user$;

  subscription = new Subscription();

  nomeDoUsuarioRetornadoEmMomentoEspecifico: string;
  nomeDoUsuarioSempreAtualizado: string;

  ngOnInit() {
    // Busca a informação do usuário somente naquele momento
    this.nomeDoUsuarioRetornadoEmMomentoEspecifico = this.userService.userValue.name;

    // Inscrição em toda mudança do usuário para buscar a informação
    this.subscription.add(
      this.userService.user$.subscribe(usuario => {
        this.nomeDoUsuarioSempreAtualizado = usuario.name;
      })
    );
  }

  ngOnDestroy() {
    if (this.subscription && this.subscription.closed === false) {
      this.subscription.unsubscribe();
    }
  }
}
```

# Autorização

## Utilização

Para verificar permissão, utilize os seguintes meios:

- Para verificar em código, use o serviço `AuthorizationService` com o método
`authorize(action: string, resource: string): Promise<boolean>`.

  Exemplo:

  ```typescript
  // algum.component.ts
  const possuiPermissao = await this.authorizationService.authorize('Cadastrar', 'Usuário');
  ```

- Para proteger rotas utilize a classe Guard ([CanActivate](https://angular.io/api/router/CanActivate)) `RouteAuthorizationGuardService`

  Exemplo:

  ```typescript
  // app-routing.module.ts
  {
    path: 'teste',
    component: TesteComponent,
    canActivate: [RouteAuthorizationGuardService],
    data: { authorize: { action: 'Acessar', resource: 'Teste' } }
  }
  ```

  > **Nota:** A forma de se verificar autorização baseado em perfil ([Role Based Access Control (RBAC)](https://en.wikipedia.org/wiki/Role-based_access_control)) não é recomendado visto que esse modelo traz um acoplamento do código da sua aplicação com a existência de perfis pré-definidos, e num cenário onde se crie, altere ou exclua perfis, é necessário modificar o código para ser aplicado.
  >
  > No modelo baseado em atributos ([Attribute Based Access Control(ABAC)](https://en.wikipedia.org/wiki/Attribute-based_access_control#Attributes)), os perfis são gerenciados pelo Authorization Server para atribuir as permissões. Desta forma, as aplicações não precisam conhecer dos perfis, mas sim das permissões que o usuário possui. Neste modelo, a gestão dos perfis serve como um agrupador das permissões que um usuário (ou grupo) possui.

## Resumo (TLDR)

- Se você quer utilizar as autorizações baseada no usuário autenticado (**recomendado**), mude para `UserAuthorizationManager` e informe todas as permissões do usuário numa implementação do `IAuthenticationManager` (veja tópico: [Interceptador do Usuário](#interceptador-do-usuário))
- Se você quer controlar a forma como autorização é realizada no seu projeto, implemente `IAuthorizationManager` (veja tópico: [Implementação de Autorização](#implementação-de-autorização))
- Se você quer utilizar as autorizações controladas por uma ferramenta e verificar em tempo-real nestas, utilize `UmaAuthorizationManager`
- Se você quer utilizar as autorizações controladas por uma ferramenta que suporta XACML e gostaria de manter salvo as políticas padrões no repositório do seu produto, utilize `XacmlAuthorizationManager`

## Provedores

É disponível no `@cnj/uikit` os seguintes provedores:

- ### `UserAuthorizationManager` **(RECOMENDADO)**

    Este manager, se baseia nas informações de autorizações [`UserWithAuthorizations`](./projects/uikit/src/lib/shared/auth/authentication/user.model.ts) do usuário autenticado. Este manager é **offline**, ou seja, não faz requisição para verificar essa permissão (ele parte do princípio que estas informações já foram buscadas e estão na propriedade `authorizations` com o tipo `Array<{ action: Array<string>; resource: string; client?: string }>`).

  ```typescript
  { provide: IAuthorizationManagerToken, useClass: UserAuthorizationManager },
  ```

    > **Observação:** Por mais que este seja o método **RECOMENDADO**, faz com que o desenvolvedor/arquiteto do produto implemente o `IAuthenticationManager` para que complemente o usuário com as informações de autorização (veja como em: [Interceptador do Usuário](#interceptador_do_usuário)).

- ### `UmaAuthorizationManager`

    Este manager se baseia no padrão [User Managed Access (UMA)](https://kantarainitiative.org/confluence/display/LC/User+Managed+Access) para verificar a autorização do usuário. Ele é **online**, ou seja, a cada verificação de autorização os seguintes passos são realizados para se verificar a permissão do usuário:

    1) É feita uma requisição para o [Permission Endpoint](https://docs.kantarainitiative.org/uma/wg/rec-oauth-uma-federated-authz-2.0.html#permission-endpoint) buscando o ticket de permissão (necessário para próxima etapa da verificação da autorização)

    2) É feita uma requisição para o [Token Endpoint](https://docs.kantarainitiative.org/uma/wg/rec-oauth-uma-grant-2.0.html#rfc.section.3.3.4) questionando o "Authorization Server" sobre a decisão da solicitação de autorização

    > **Observação:** Este foi o método padrão definido no [UIKit-seed](https://git.cnj.jus.br/uikit/uikit-seed/blob/master/src/app/app.module.ts#L94) _(mesmo não sendo o recomendado)_, visto ser o único que funciona "[out-of-box](https://pt.wikipedia.org/wiki/Out-of-box_experience)" baseado em um padrão suportado pelas ferramentas mais utilizadas de SSO.

    > _Suportado (e testado) nas ferramentas [Keycloak](https://www.keycloak.org/docs/7.0/authorization_services/#_service_user_managed_access) e [WSO2 Identity Server](https://docs.wso2.com/display/IS580/User+Managed+Access)_

  ```typescript
  { provide: IAuthorizationManagerToken, useClass: UmaAuthorizationManager },
  ```

- ### `XacmlAuthorizationManager`

    Este manager se baseia no padrão [eXtensible Access Control Markup Language (XACML)](https://www.oasis-open.org/committees/xacml/) para verificar a autorização do usuário. Ele é **online**, ou seja, a cada verificação de autorização os seguintes passos são realizados para se verificar a permissão do usuário:

    1) É feita uma requisição para o [Policy Decision Point (PDP)](https://docs.wso2.com/display/IS580/Entitlement+with+REST+APIs) questionando o "Authorization Server" sobre a decisão da solicitação de autorização

    > **Observação:** Outra vantagem do uso do XACML é a de poder salvar as políticas padrões do seu produto junto ao repositório do mesmo (Ex: Perfis, Permissões dos Perfis, etc). Permitindo carregá-las em outra ferramenta (ou na própria em outro ambiente) que suporte este mesmo padrão (permitindo o desacoplamento de ferramenta de autorização), simplificando até mesmo um caso de teste automatizado (que valide autorizações).

    > _Suportado (e testado) na ferramenta [WSO2 Identity Server](https://docs.wso2.com/display/IS580/Access+Control+and+Entitlement+Management#AccessControlandEntitlementManagement-IntroducingXACML)_

  ```typescript
  { provide: IAuthorizationManagerToken, useClass: XacmlAuthorizationManager },
  ```

> Q: Quando uso um authorization manager online (`UmaAuthorizationManager` ou `XacmlAuthorizationManager`) ou offline (`UserAuthorizationManager`)?
>
> R: Se utiliza um authorization manager online quando o responsável por verificar autorização é um serviço. _Uma situação como ter auditoria no serviço de autorização sobre a 'concessão ou negação da autorização' registrada no momento da verificação é um caso para uso de um authorization manager online._
> > _É importante ressaltar que isso degrada a experiência do usuário visto que deverá aguardar a requisição e resposta de uma checagem de permissão. Este é o motivo deste não ser o método recomendado para verificação de permissão._


## Implementação de autorização

Caso deseje customizar a forma de se verificar uma autorização no UIKit, implemente a interface `IAuthorizationManager` como no exemplo abaixo:

```typescript
import { IAuthorizationManager, UserService } from '@cnj/uikit';
import { Injectable } from '@angular/core';

@Injectable()
export class CustomAuthorizationManager implements IAuthorizationManager {
  constructor(protected userService: UserService) { }
  async authorize(action: string, resource: string): Promise<boolean> {
    const usuario = this.userService.userValue;
    // Verifico se o usuário está autenticado
    if (usuario.authenticated === true) {
      // Verifico se o usuário tem permissão de executar a ação solicitada no recurso solicitado
      // Neste ponto pode-se:
      // - Utilizar uma API para verificar permissão (também chamado como Authorization Server)
      // - Utilizar de políticas para estabelecer estas permissões
      // - Até mesmo fazer um mapeamento manual com um perfil como neste exemplo (não recomendado)
      if (action === 'Cadastrar' && resource === 'Usuário') {
        const usuarioEhAdministrador = usuario.roles.some(perfil => perfil.name === 'admin');
        if (usuarioEhAdministrador) {
          return true;
        }
      }
    }
    // Qualquer cenário é negação por padrão
    return false;
  }
}
```


# Exemplo


 Neste exemplo, utilizamos o [Keycloak](https://www.keycloak.org/) para simular uma autenticação (permitindo um desenvolvimento desacoplado). 

1 - Utilize o comando docker para levantar rapidamente um keycloack e configurar a aplicação: `docker run -p 8080:8080 -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin jboss/keycloak`

2 - Cadastre um client (sistema):

 - `Client ID` = Utilizado para identificar o sistema (é usado nas configurações da aplicação)
 - `Root URL` = Endereço da aplicação (local ou em ambiente) - Ex: `http://localhost:4200/`

3 - Ative o `Implicit Flow Enabled` e Salve

4 - Configure a aplicação no `./src/assets/config/config.json` (e no `config-docker.json`):

```json
...
 "authentication": {
   "authority": "http://localhost:8080/auth/realms/master/",
   "client_id": "localhost",
   "client_uri": "http://localhost:4200/",
   "scope": "openid profile email roles offline_access"
 }
...
```
