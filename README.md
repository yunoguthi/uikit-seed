# UIKIT.PJe.JUS

## Introdução

Repositório (GIT) com o código da biblioteca (NodeJs) para Angular (7+) contendo:
- Layout
- Componentes
- Temas
seguindo as instruções do [UIKit](https://git.cnj.jus.br/rogersborchia/ui.jus).

## Iniciando

### Ferramentas

- [GIT](https://git-scm.com/) _(Obrigatório)_
- [NodeJS](https://nodejs.org) _(Obrigatório)_
- [Angular CLI](https://cli.angular.io/) _(Obrigatório)_
- [Docker](https://www.docker.com/) _(Obrigatório - Obs: para publicar)_
- [VSCode](https://code.visualstudio.com/) _(Recomendado)_

### Desenvolvendo


0 - Configure para usar o registry do CNJ
```sh
npm config set registry https://nexus.cnj.jus.br/repository/npm-all/
```

1 - Clone, instale dependências e build _(com watch fazer o build a cada alteração)_:
```sh
git clone https://git.cnj.jus.br/uikit/uikit.git

cd uikit
npm install
ng build uikit --watch
```

2 - _(EM OUTRO SHELL)_ Faça o [link](https://medium.com/dailyjs/how-to-use-npm-link-7375b6219557) do projeto uikit

```sh
cd dist/uikit #importante fazer de dentro deste diretório
npm link
```

3 - _(EM OUTRO SHELL)_ Clone o repositório do [UIKIT-SEED](https://git.cnj.jus.br/uikit/uikit-seed.git), instale dependências:
```sh
git clone https://git.cnj.jus.br/uikit/uikit-seed.git

cd uikit
npm install
```

4 - Faça o link com o projeto/biblioteca do uikit:
```sh
npm link @cnj/uikit
```

5 - Inicie o projeto raiz:
```sh
ng serve
```

### Publicando

1 - Coloque a versão release (de produção) desejada nos arquivos (Ex: `1.0.1`):
- `package.json`
- `projects/uikit/package.json`

2 - Rode o comando abaixo informando as seguintes variáveis de build _(`--build-arg`)_:
  - **`TAG`**: Informado a versão de desenvolvimento (environment) - Ex: `alpha.1` - _(default é vazio)_
  - `NPM_REGISTRY`: Informado o registry de onde será buscado os pacotes _(default: `https://nexus.cnj.jus.br/repository/npm-all/`)_
  - `NPM_REGISTRY_PUBLISH`: Informado o registry onde será publicado _(default: `https://nexus.cnj.jus.br/repository/npm-internal/`)_
  - `NPM_REGISTRY_PUBLISH_SCOPE`: O escopo do pacote _(default: `@cnj`)_
  - `NPM_USER`: Usuário do registry para publicar
  - `NPM_PASS`: Senha do registry para publicar
  - `NPM_EMAIL`: Email do registry para publicar

**Exemplo:**
```sh
docker-compose -f docker-compose.build.yml build --build-arg TAG=alpha.1 --build-arg NPM_REGISTRY=https://nexus.cnj.jus.br/repository/npm-all/ --build-arg NPM_REGISTRY_PUBLISH=https://nexus.cnj.jus.br/repository/npm-internal/ --build-arg NPM_REGISTRY_PUBLISH_SCOPE=@cnj --build-arg NPM_USER=admin --build-arg NPM_PASS=admin123 --build-arg NPM_EMAIL=exemplo@cnj.jus.br
```

_Observação: No exemplo, o pacote será publicado com a versão: `1.0.1-alpha.1`_
_Se não informado a TAG, será publicado como produção/stable - Ex: `1.0.1`_

3 - **Caso necessário**, faça o login na sua máquina e copie as informações do registry do arquivo `.npmrc` global da sua máquina (windows: `%HOMEPATH%\.npmrc`) para o arquivo `.npmrc` do projeto local.

**Exemplo:**
```
@cnj:registry=https://nexus.cnj.jus.br/repository/npm-internal/
//nexus.cnj.jus.br/repository/npm-internal/:_authToken=NpmToken.XXXXXXXXXXXXXXXXXXXXXXX
```


## Biblioteca

### Layout

#### Estilos (scss)
Para facilitar a manutenção e criação de novos estilos ou componentes, foi desenvolvido um tema para o UIKit utilizando como base o Angular Material (biblioteca do Material Design).

Os arquivos .scss encontram-se dentro da pasta `uikit/srv/lib/theme/uikit` e tem como estrutura:

> /componentes
> - Estilos criados para os componentes próprios do UIKit.
> - Ex: filtros, breadcrumb, search, userinfo e etc..

> /fonts
> - Arquivos do google webfonts (para execução independente de acesso à internet)

> /material
> - Estilos do material design que sofreram alterações para se adequar ao layout do UIKit e devem sobrescrever todos os estilos nativos da biblioteca.
> - Ex: badge, chips, cards.

> _arquivos soltos
> - Foram definidos na raiz do thema, estilos genéricos, utilizados tanto nos /componentes quanto /material.

##### Variaveis 
> Cores e Tipos
> - Para a definição dos esquemas de cores e tipografias, foi gerado dentro do proprio material uma nova variação de cores (theme-light), que pode ser editada em `/material/colors.scss`.
> - O material utiliza 3 tons como default, sendo: `primary` (azul), `accent` (laranja) e `warn` (vermelho)
> - Mais detalhes da utilização dos 3 tons em: https://material.angular.io/guide/theming

> Variaveis 
> - Otimizando a reutilização das cores, tipos e tamanhos de fontes do Material Design, foi gerado o arquivo `/_variables.scss` que extende algumas configurações nativas do Material Design. 

Definições de color:
```scss
$color-primary;
$color-accent;
$color-warn;
$icon-color;
$text-color;
$secondary-text-color;
```
Definições de font-size:
```scss
$title-menu-font-size
$font-size-body
$font-size-caption
$font-size-breadcrumb
$font-size-title
$font-size-subtitle
$font-size-subheading
```

#### Shell (componente da estrutura)
Para começar a utilizar o shell (header, menu e content) do UIKit em seu projeto, insira no `app.component.html`:
```html
<uikit-layout>
  <router-outlet></router-outlet>
</uikit-layout>
```
##### Menus:
```html
<uikit-layout>
  <div layout-nav-menu>
    <uikit-menu>
        <uikit-menu-item title="Item 1" icon="fa-lg fas fa-address-book" [link]="['/menu1']"></uikit-menu-item>
        <uikit-menu-item title="Categoria" icon="fa-lg fas fa-address-book">
        <uikit-menu-item title="Item 2" icon="fa-lg fas fa-address-book" [link]="['/menu2']"></uikit-menu-item>
      </uikit-menu-item>
    </uikit-menu>
  </div>

  <div layout-nav-menu-extras>
    Estrutura livre que será exibida no menu
  </div>
  <router-outlet></router-outlet>
</uikit-layout>
```
* Dentro da tag `layout-nav-menu-extras`, existe maior flexibilidade e pode ser inserido qualquer estrutura HTML.
* Dentro da tag `layout-nav-menu`, **obrigatóriamente** devem ser inseridos apenas os `<uikit-menu-item>`
* - Após inserir os menus, deve ser feita a configuração dentro das rotas `-routing.module.ts` de cada tela, da seguinte forma:
```ts
const routes: Routes = [
  {
    path: ':id',
    data: { breadcrumb: 'Detalhes ( :id )', icon: 'fas fa-info-circle mat-icon-no-color' }
  }
];
```
##### Notificações
Ative as notificações no header (identificado por um sino localizado na parte superior, direita do sistema) por meio do atributo `[showNotifications]="true"`
```html
<uikit-layout [showNotifications]="true">
  <div layout-header-notifications>
    <uikit-notificacao nome="Nome1" descricao="Descricao1" icone="fas fa-info-circle" [read]="true"></uikit-notificacao>
  </div>
  <router-outlet></router-outlet>
</uikit-layout>
```
* A listagem de notificações deve ser realizada dentro da tag `<div layout-header-notifications>`

##### User Info
Ative as informações do usuário (identificado pela foto de login localizado na parte superior, direita do sistema) por meio do atributo `[showUserInfo]="true"`
```html
<uikit-layout [showUserInfo]="true">
  <div layout-header-userinfo>
    Bem vindo {{ nome }}.
    Selecione o perfil desejado: 
    ...
  </div>
  <router-outlet></router-outlet>
</uikit-layout>
```

##### System Info
Ative as informações do sistema (changelog) (identificado por um (i) localizado na parte superior, direita do sistema) por meio do atributo `[showSystemInfo]="true"`
```html
<uikit-layout [showSystemInfo]="true">
  <div layout-header-systeminfo>
    <mat-nav-list>
      <a mat-list-item>
        <div>
          <h4>1.0</h4>
          <span class="date">04/02/2019</span>
          <p> Versão inicial do sistema.</p>
        </div>
        <mat-icon matListIcon class="fa-lg far"></mat-icon>
      </a>
    </mat-nav-list>
  </div>
  <router-outlet></router-outlet>
</uikit-layout>
```

### Nova tela
Ao iniciar a criação de uma nova tela, dentro do arquivo `.component.html` insira o systemgrid do bootstrap para encapsular todo o conteúdo da sua tela: 
```html
<div class="container-fluid">
  <div class="row">
    <div class="col">
      ...
    </div>
  </div>
</div>
```

### Componentes
#### Breadcrumb
Para alimentar o breadcrumb de suas telas com paginação, botões de ação e filtro, insira dentro do arquivo `.component.html` de sua tela>
##### Botões de Ação
```html
<div uikit-actions>
  <button mat-fab color="primary">
    <mat-icon class="fas fa-plus"></mat-icon> Ação principal
  </button>
  <button mat-mini-fab color="" disabled>
    <mat-icon class="fas fa-print"></mat-icon>
  </button>
  <button mat-mini-fab color="" disabled>
    <mat-icon class="fas fa-download"></mat-icon>
  </button>
  <button mat-mini-fab color="">
    <mat-icon class="fas fa-trash-alt"></mat-icon>
  </button>
</div>
```
* A primeira e mais importante ação da sua tela, deve ser inserida com `mat-fab color="primary"`
* As ações secundárias, devem ser inseridas com `mat-mini-fab color=""`
* Ações em lote ou que necessitem de outras interações dentro da sua tela, para que possam ser liberadas, devem vir com `disabled` até serem liberadas

##### Filtros
```html
<uikit-filter (onPesquisar)="pesquisar()" [disabled]="!filtersForm.valid">

    <div #moreFilters class="uikit-filters" formGroupName="more">
        <div class="container-fluid">
            <div class="row">

                <div class="col">
                    <mat-form-field appearance="outline">
                        <mat-label>Id</mat-label>
                        <input #idInput="matInput" matInput placeholder="Id" formControlName="id" cdkFocusInitial/>
                    </mat-form-field>
                </div>

                <div class="col">
                    <mat-form-field appearance="outline">
                        <mat-label>Name</mat-label>
                        <input matInput placeholder="Name"
                               formControlName="name"/>
                    </mat-form-field>
                </div>
                
                <button type="submit" mat-raised-button color="primary" (click)="aplicarFiltros()">Filtrar
                </button>
            </div>

            <div class="row" *ngIf="(filters$ | async).length > 0">
                <div class="col">

                    <ul class="filters-chips">
                        <li><b>Filtros Aplicados:</b></li>
                        <li *ngFor="let filterComposite of  (filters$ | async)">
                            {{ filterComposite.campo != null ? ' de ' + filterComposite.campo : ' de pesquisa rápida' }} {{filterComposite.operador}}
                            :
                            <ng-container>
                                <mat-chip [removable]="true" (removed)="removeFilter(filterComposite)">
                                    {{ filterComposite.valor }}
                                    <mat-icon matChipRemove class="fas fa-times"></mat-icon>
                                </mat-chip>
                                <span> ou</span>
                            </ng-container>
                        </li>
                        <li>
                            <a mat-flat-button (click)="clearFilters()">
                                Limpar todos os filtros
                                <mat-icon class="fas fa-eraser"></mat-icon>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

</uikit-filter>
```
* `uikit-filter`, é o componente responsavel pela estruturação dos filtros, contendo as ações de busca rápida e mais filtros.
* o exemplo a cima demonstra como utilizar o componente `uikit-filter`, passando como evento `clickPesquisa` para execução da busca rápida.
* O Botão `mais filtros` quando clicado abre todo o conteúdo do escopo `uikit-filter` em uma modal.
O exemplo contém a demonstração de como montar a árvore de filtros já aplicados.

* Parâmetros do componente:
    `clickPesquisa`: para execução do evento de busca rápida.
    `disabled`: para habilitar ou desabilitar o botão de pesquisa.
    
##### Paginação
```html
<div uikit-paginator>
  <mat-paginator [length]="100" [pageSize]="10" [pageSizeOptions]="[5, 10, 25, 100]">
  </mat-paginator>
</div>
```
* A paginação do Angular Material foi customizada para o breadcrumb do UiKit, utilize os recursos da documentação do Angular Material dentro da tag `<div uikit-paginator>`


##### Operações (Task manager)

Snackbar onde aparece as tarefas executadas pelo usuário.

```ts
// Ação
const observable = this.httpClient.get<{}>('API_URL');
const subscription = observable.subscribe();

// Desfazer
const observableDesfazer = this.httpClient.get<{}>('API_URL');

// Operações (Task manager)
const operation = this.operationsService.create('Buscando processos', subscription, observableDesfazer);
```

Os parâmetros `subscriptionToCancel` e `undoOperation` são opcionais, porém ao informado permite o cancelamento e o desfazer das operações.

Observação: Caso não informado os parâmetros, cabe ao desenvolvedor informar a finalização da operação executando o método `operation.setAsProcessed();`

### Configurações

#### Armazenamento

Para armazenar as configurações da aplicação, utilize o arquivo `./src/assets/config/config.json` para desenvolvimento local e o `./src/assets/config/config-docker.json` quando utilizando [Docker](https://www.docker.com/).

> **Motivação:** Usar o environment.ts (puro do angular cli) não é recomendado visto que acopla a compilação às variáveis. 
> 
> O recomendado é armazenar/ler de um arquivo `.json` modificado ao iniciar a aplicação (docker `entrypoint.sh`) lendo de variável de ambiente.
> https://12factor.net/pt_br/config 

#### Exemplo

##### Código

- `app.component.ts` _(seu arquivo .ts)_
```ts
import { environment } from './environment';
...
ngOnInit() {
  const url = environment.api.url;
}
```

- `app-config.model.ts` (interface para tipar fortemente as configurações)
```ts
export interface AppConfig {
  api: {
    url: string;
  };
}
```

##### Config

- Local
```json
{
  "api": {
    "url": "http://sistema.cnj.jus.br/api/"
  }
}
```

- Docker
```json
{
  "api": {
    "url": "${API_URL}"
  }
}
```

  > Observação: No `docker-compose` utilize [variáveis de ambiente](https://docs.docker.com/compose/environment-variables/) para substituir o valor da url.
  > 
  > Exemplo:
  > ```yaml
  > version: '3'
  > 
  > services:
  >   sistema.cnj.jus.br:
  >     image: ${DOCKER_REGISTRY}sistema.cnj.jus.br:${VERSION:-latest}
  >     ports:
  >       - 80:80
  >       - 443:443
  >     environment:
  >       API_URL: 'http://sistema.cnj.jus.br/api/'
  >     labels:
  >       kompose.service.expose: 'sistema.cnj.jus.br'
  > ```


## Toast

> #### ToastService tem como base o ToastrService da ngx-toastr, tendo os seguintes métodos:
>
> - **Sucesso**: `success(message: string, title?: string, objectAction?: ToastAction): Toaster<any>`
> - **Erro**: `error(message: string, error: Error, title?: string, objectAction?: ToastAction): Toaster<any>`
> - **Informação**: `info(message: string, title?: string, objectAction?: ToastAction): Toaster<any>`
> - **Aviso**: `warning(message: string, title?: string, objectAction?: ToastAction): Toaster<any>`
> - **Aviso**: `loading(isLoading$: Observable<boolean>, message: string = 'Carregando...', title?: string, objectAction?: ToastAction): void`
> - **Show**: `show(config: Partial<ToastConfig>): Toaster<any>`
> - **Clear**: `clear(toastId?: number): void`

> #### Toast personalizado:
>
> O serviço de toast contem o método **Show** que possibilita a personalização do toast pelo parâmetro **`config: Partial<ToastConfig>`** que contem os seguintes atributos:

> #### ToastConfig:
>
> - **message: _string_**, -- Mesagem do toast;
> - **title?: _string_**,-- Título do toast;
> - **error?: _Error_**,-- Detalha o erro no toast;
> - **icon?: _string_**,-- Icone que sera apresentado ao lado esquerdo do toast;
> - **disableTimeOut: _boolean_**, -- Desativa o tempo limite e o tempo limite estendido, _default: false_;
> - **timeOut: _number_**, -- Tempo de vida do toast em milissegundos, default: 5000;
> - **closeButton: _boolean_**, -- Habilita o botão Fechar, _default: false_;
> - **extendedTimeOut: _number_**, -- tempo de vida após o usuário passa o mouse sobre o toast em milissegundos, _default: 1000_;
> - **progressBar: _boolean_**, -- Habilita barra de progresso, _default: true_;
> - **progressAnimation: _ProgressAnimationType_**, -- Alterar a pisição da animação da barra de progresso, _default: decreasing_;
> - **enableHtml: _boolean_**, -- Habilita renderização de HTML na mensagem, _default: false_;
> - **toastClass: _string_**, -- Classe de estilização do componente, _default: ngx-toastr_;
> - **positionClass: _string_**, -- Classe de posicionamento do toast, por padrão é posicionado ao lado inferior direito da tela:
>   > - **toast-bottom-right**: Direito inferior;
>   > - **toast-bottom-left**: Esquerdo inferior;
>   > - **toast-bottom-center**: Centro inferior;
>   > - **toast-bottom-full-center**: Largura total inferior;
>   > - **toast-top-right**: Direito superior;
>   > - **toast-top-left**: Esquerdo superior;
>   > - **toast-top-center**: Centro superior;
>   > - **toast-top-full-center**: Largura total superior;
> - **titleClass: _string_**, -- Classe de estilização do título, _default: toast-title_;
> - **messageClass: _string_**, -- Classe de estilização da mensagem,_default: toast-message_;
> - **easing: _string_**, -- Animação do toast, _default: ease-in_;
> - **easeTime: _string | number_**, -- Tempo da animação do toast, _default: 300_;
> - **tapToDismiss: _boolean_**, -- Habilita o fechamento do toast no click, _default: true_;
> - **toastComponent?: _ComponentType<any>_**, -- Componente de definição do toast, _default: ToastComponent_
>   > - _O Compomente toast pode ser personalizado_,
>   >   herdando-se de *ToastComponent --@cnj/uikit* ou *Toast --ngx-toastr*  e sobrescrever o *options* para ser do tipo *ToastConfig* ex options: ToastConfig;
> - **objectAction: _ToastAction_, -- default: null**, -- Atribuição de ação,

```js
export class ToastAction {
  /*
      Descrição do link do ação no toast --Máximo de caracteres 17
    */
  display: string;

  /*
      Ação a ser executada ao clicar no link atribuida no display
    */
  action: (value?: any) => any;
}
```

> #### Como usar:

> Importar o ToastService no componente, ex:

```js
import { ToastService } from '@cnj/uikit';
```

> Inserir o `toastService: ToastService` no construtor do componente
> Caso queria informar um toast de **info, warning, error ou success**, com a configuração padrão e sem título:

```js
this.toastService.success('mensagem do toast.');
this.toastService.warning('mensagem do toast.');
this.toastService.info('mensagem do toast.');
```

> Caso queria informar com título:

```js
this.toastService.success('mensagem do toast.', 'título do toast');
this.toastService.warning('mensagem do toast.', 'título do toast');
this.toastService.info('mensagem do toast.', 'título do toast');
```

> Erro:

```js
try {
  throw Error('Not implemented expetion!');
} catch (error) {
  config.error = error;
  this.toastService.error('mensagem do toast.', undefined, error);
  this.toastService.error('mensagem do toast.', 'título do toast', error);
}
```

> Loading:

```js

  import { AfterViewInit, Component, OnInit, OnDestroy } from '@angular/core';
  import { Subscription, Observable } from 'rxjs';
  import { ToastService } from '@cnj/uikit';

  @Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
  })
  export class AppComponent implements OnInit  {

    protected isLoading$: Observable<boolean>;

    constructor(
      protected toasterService: ToastService
    ) { }

    ngOnInit() {
     this.isLoading$ = new Observable(subscriber => {
        subscriber.next(true);
        setTimeout(() => {
          subscriber.next(false);
        }, 5000);
      });

      this.toasterService.loading(this.isLoading$);
    }
  }

```

> Toast personalizado:

```js
this.toastService.show({
	message: string,
	title?: string,
	icon?: string,
	error?: Error,
	disableTimeOut: boolean
	timeOut: number,
	extendedTimeOut: number,
	progressBar: boolean,
	progressAnimation: ProgressAnimationType,
	enableHtml: boolean,
	toastClass: string,
	positionClass: string,
	titleClass: string,
	messageClass: string,
	easing: string,
	easeTime: string | number,
	tapToDismiss: boolean,
	toastComponent: ComponentType<any>,
	objectAction?: {
		display: string,
		action: ((value?: any) => any)
	},
});
```


### Autenticação/Autorização

É utilizado o OAuth 2 com o flow implicit (recomendado para Single Page Applications).


#### Configuração

No arquivo de configuração (`config.json` e `config-docker.json`), utilize o nó de `authentication` para informar os valores do OAuth 2 (implicit flow).

#### Autenticação:
- `AuthenticationService` com os métodos:
  - `login(): Promise<void>`
  - `logout(): Promise<void>`


- `AuthenticationGuardService` para proteger rotas para usuário autenticado:

  ```typescript
  // app-routing.module.ts
  const routes: Routes = [
  {
    path: 'breweries',
    loadChildren: './breweries/breweries.module#BreweriesModule',
    canActivateChild: [ AuthenticationGuardService ]
  }
  ];
  //...
  ```

  Maiores detalhes veja em: [Auth](./AUTH.md#autenticação)

#### Autorização:
- `AuthorizationService` com o método:

  - `authorize(action: string, resource: string): Promise<boolean>`

  Exemplo:
  ```typescript
  // Verificação por ação
  const possuiPermissao = this.authorizationService.authorize('Cadastrar', 'Usuario');
  ```

- `RouteAuthorizationGuardService` para proteger rotas para usuário com permissão (autorização):

    Exemplo:
    ```typescript
    // app-routing.module.ts
    {
      path: 'teste',
      component: TesteComponent,
      canActivate: [RouteAuthorizationGuardService],
      data: { authorize: { action: 'Acessar', resource: 'Teste' } }
    }
    ```

    Maiores detalhes veja em: [Auth](./AUTH.md#autorização)

#### Informações:
- `UserService` com as propriedades:
  - `user$: Observable<User>`
  - `userValue: User`

  `User` possui as seguintes informações:
  ```typescript
  {
    /** Identificador único do usuário naquele provedor de identidade */
    sub: string;
    /** Endereço do serviço provedor de identidade */
    iss?: string;
    /** Informa se o usuário foi autenticado */
    authenticated: boolean;
    /** Url da imagem do usuário */
    picture?: string;
    /** Nome do usuário */
    name: string;
    /** Nome de exibição escolhido pelo usuário */
    preferred_username?: string;
    /** Email principal do usuário */
    email: string;
    /** CPF do usuário */
    cpf?: string;
    /** Perfis do usuário para o sistema */
    roles?: Array<Role>;
  }
  ```

  Maiores detalhes veja em: [Auth](./AUTH.md#informações)

> **Observações:**
 Como exemplo, pode-se utilizar o [Keycloak](https://www.keycloak.org/) para simular uma autenticação (permitindo um desenvolvimento desacoplado). 
>
> 1 - Utilize o comando docker para levantar rapidamente um keycloack e configurar a aplicação: `docker run -p 8080:8080 -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin jboss/keycloak`
>
> 2 - Cadastre um client (sistema):
>
>  - `Client ID` = Utilizado para identificar o sistema (é usado nas configurações da aplicação)
>  - `Root URL` = Endereço da aplicação (local ou em ambiente) - Ex: `http://localhost:4200/`
> 
> 3 - Ative o `Implicit Flow Enabled` e Salve
> 
> 4 - Configure a aplicação no `./src/assets/config/config.json` (e no `config-docker.json`):
> ```json
> ...
>  "authentication": {
>    "authority": "http://localhost:8080/auth/realms/master/",
>    "client_id": "localhost",
>    "client_uri": "http://localhost:4200/",
>    "scope": "openid profile email roles offline_access"
>  }
> ...
> ```


## Repositórios Vinculados
> #### UIKit-seed
> - Contém o projeto raiz que deverá ser clonado e utilizado como base ao iniciar um novo projeto
> - O seed utiliza o tema gerado no UIKit
> - https://git.cnj.jus.br/uikit/uikit-seed

> #### UIKit.PJe.Jus - Design
> - Projeto contendo repositórios referente a Interface Gráfica dos sistemas da Justiça
> - https://git.cnj.jus.br/uikit/ui.pje.jus


#### Inclusão do Logo:
> O logo a ser incluido deve atentar dois fatores, medida e coloração. 
> 
> 1 - A imagem do logo para desktop e mobile não deve ter a altura superior a 45px
> 
> 2 - A imagem do logo para desktop não deve ter a largura superior a 200px
> 
> 3 - A imagem do logo para mobile não deve ter a largura superior a 45px
> 
> 4 - Ao incluir o logo tanto para desktop quanto para mobile deve ser composto um arquivo alternativo na cor branca (padrão negativo da marca utilizada) para interagir com o modo dark do layout.
>
> 5 - A classe que define o logotipo em desktop é: logodesk' 
>
> 6 - A classe que define o logotipo no mobile é: logomobile' 
>
> 7 - Para realizar a inclusão é necessário incluir o endereço de sua imagem no url do content conforme exemplo abaixo, as imagens brancas (Padrão negativo da marca) devem ser incluidas dentro da classe dark, conforme exemplo abaixo. 
>
Exemplo para inclusão do logo:
```scss
.logomobile {
  content: url('assets/images/uikit-logotipo-mobile.svg');
}

.logodesk {
  content: url('assets/images/uikit-logotipo.svg');
}

.dark {

  .logomobile {
    content: url('assets/images/uikit-logotipo-mobile-bw.svg');
  }

  .logodesk {
    content: url('assets/images/uikit-logotipo-bw.svg');
  }

}
```
