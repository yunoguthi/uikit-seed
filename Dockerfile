# FROM node:10-alpine AS build
FROM node:10-alpine

ARG NPM_REGISTRY
ARG NPM_REGISTRY_PUBLISH
ARG NPM_REGISTRY_PUBLISH_SCOPE
ARG OUTPUT=dist
ARG TAG=latest
ARG PROJECT
ARG NPM_USER
ARG NPM_PASS
ARG NPM_EMAIL


ENV NPM_REGISTRY=${NPM_REGISTRY:-https://registry.npmjs.org/}

WORKDIR /usr/src

# COPY .npmrc ./
RUN touch .npmrc

RUN npm config set registry ${NPM_REGISTRY}
RUN if [ -z "${NPM_REGISTRY_PUBLISH_SCOPE}" ]; then echo ''; else npm config set ${NPM_REGISTRY_PUBLISH_SCOPE}:registry ${NPM_REGISTRY_PUBLISH}; fi

RUN npm install -g @angular/cli@7.3.9 npm-cli-adduser
RUN if [ -z "${NPM_REGISTRY_PUBLISH_SCOPE}" ]; then npm-cli-adduser -u ${NPM_USER} -p ${NPM_PASS} -e ${NPM_EMAIL} -r ${NPM_REGISTRY_PUBLISH}; else npm-cli-adduser -u ${NPM_USER} -p ${NPM_PASS} -e ${NPM_EMAIL} -r ${NPM_REGISTRY_PUBLISH} -s ${NPM_REGISTRY_PUBLISH_SCOPE}; fi

RUN cat .npmrc

COPY package.json package-lock.json ./

RUN npm ci

COPY . .

RUN npm run build -- ${PROJECT}


# FROM node:10-alpine AS publish

# ARG NPM_REGISTRY
# ARG OUTPUT=dist
# ARG TAG=latest
# ARG PROJECT

# WORKDIR /usr/src/app

# COPY --from=build /usr/src/app /usr/src/app


RUN if [ -z "${TAG}" ]; then echo ''; else cd ${OUTPUT}/${PROJECT} && npm version $(node -p "require('./package.json').version")-${TAG}; fi

# RUN node -p "require('./package.json').version"

RUN npm publish ./${OUTPUT}/${PROJECT}
