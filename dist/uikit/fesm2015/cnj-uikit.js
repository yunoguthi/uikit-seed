import { Injectable, InjectionToken, Inject, Optional, defineInjectable, inject, Component, ChangeDetectionStrategy, Renderer2, ApplicationRef, NgZone, Input, EventEmitter, ViewChild, ContentChildren, ChangeDetectorRef, ContentChild, HostBinding, Output, ViewChildren, NgModule, Directive, ElementRef, Pipe, ViewContainerRef, ComponentFactoryResolver, Host, Self, SkipSelf, Injector, ViewEncapsulation } from '@angular/core';
import { Location, DOCUMENT, PlatformLocation, HashLocationStrategy, LocationStrategy, CommonModule } from '@angular/common';
import { FormBuilder, FormsModule, ReactiveFormsModule, FormGroupDirective } from '@angular/forms';
import { ScrollDispatcher } from '@angular/cdk/overlay';
import { MatDialog, MatSidenav, MatToolbar, MatExpansionPanel, MatSlideToggle, MatSpinner as MatSpinner$1, MatSnackBarModule, MatTableModule, MatTreeModule, MatPaginatorIntl, MatDialogModule, MatProgressSpinnerModule, MatButtonModule as MatButtonModule$1, MatSnackBar, MatIconModule as MatIconModule$1, MatMenuModule as MatMenuModule$1, MatFormFieldModule as MatFormFieldModule$1, MatInputModule as MatInputModule$1, MatBadgeModule as MatBadgeModule$1 } from '@angular/material';
import { Router, NavigationEnd, NavigationStart, ActivatedRoute, RouterModule, RouteReuseStrategy } from '@angular/router';
import { Hotkey, HotkeysService, HotkeyModule } from 'angular2-hotkeys';
import { BehaviorSubject, Subscription, timer, interval, merge, of, combineLatest, fromEvent, throwError, from, defer } from 'rxjs';
import { map, first, concat, filter, distinctUntilChanged, debounceTime, flatMap, mapTo, retryWhen, switchMap, catchError, mergeMap, finalize } from 'rxjs/operators';
import { SwUpdate } from '@angular/service-worker';
import { resetStores, EntityStore, StoreConfig, QueryEntity, toBoolean, isDefined, guid, Store, Query, transaction } from '@datorama/akita';
import { Toast, ToastrService, ToastPackage, ToastNoAnimationModule, ToastrModule, ToastContainerModule } from 'ngx-toastr';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { MatDialog as MatDialog$1, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { __decorate, __metadata, __awaiter } from 'tslib';
import { moveItemInArray, DragDropModule } from '@angular/cdk/drag-drop';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatBadgeModule } from '@angular/material/badge';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatListModule } from '@angular/material/list';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { NestedTreeControl } from '@angular/cdk/tree';
import * as Fuse from 'fuse.js';
import { ScrollDispatcher as ScrollDispatcher$1, ScrollingModule } from '@angular/cdk/scrolling';
import { xorWith, take, isEqual, map as map$1, head } from 'lodash';
import { ActiveDescendantKeyManager } from '@angular/cdk/a11y';
import { DOWN_ARROW, UP_ARROW, ESCAPE, ENTER } from '@angular/cdk/keycodes';
import { MatSlider, MatSliderModule } from '@angular/material/slider';
import { MatChipsModule } from '@angular/material/chips';
import { CdkStepperModule } from '@angular/cdk/stepper';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatTabsModule } from '@angular/material/tabs';
import { MatSelectModule } from '@angular/material/select';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTable } from '@angular/material/table';
import { MatSpinner } from '@angular/material/progress-spinner';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS, HttpParams, HttpHeaders, HttpClient } from '@angular/common/http';
import { WebStorageStateStore, UserManager } from 'oidc-client';
import { DomSanitizer } from '@angular/platform-browser';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {number} */
const LogLevel = {
    Debug: 0,
    Info: 1,
    Warn: 2,
    Error: 3,
};
LogLevel[LogLevel.Debug] = 'Debug';
LogLevel[LogLevel.Info] = 'Info';
LogLevel[LogLevel.Warn] = 'Warn';
LogLevel[LogLevel.Error] = 'Error';
/**
 * @record
 */
function Log() { }
if (false) {
    /** @type {?} */
    Log.prototype.message;
    /** @type {?} */
    Log.prototype.level;
}
class LogService {
    constructor() {
        this.loggerSubject$ = new BehaviorSubject({ message: 'Log iniciado!', level: LogLevel.Debug });
        this.logger$ = this.loggerSubject$.asObservable();
    }
    /**
     * @private
     * @param {...?} infos
     * @return {?}
     */
    infosToMessage(...infos) {
        return infos.join(' ');
    }
    /**
     * @protected
     * @param {?=} level
     * @param {...?} infos
     * @return {?}
     */
    log(level = LogLevel.Debug, ...infos) {
        /** @type {?} */
        const message = this.infosToMessage(...infos);
        this.loggerSubject$.next({ message, level });
    }
    /**
     * @param {...?} data
     * @return {?}
     */
    debug(...data) {
        this.log(LogLevel.Debug, ...data);
    }
    /**
     * @param {...?} data
     * @return {?}
     */
    info(...data) {
        this.log(LogLevel.Info, ...data);
    }
    /**
     * @param {...?} data
     * @return {?}
     */
    warn(...data) {
        this.log(LogLevel.Warn, ...data);
    }
    /**
     * @param {...?} data
     * @return {?}
     */
    error(...data) {
        this.log(LogLevel.Error, ...data);
    }
}
LogService.decorators = [
    { type: Injectable }
];
if (false) {
    /**
     * @type {?}
     * @protected
     */
    LogService.prototype.loggerSubject$;
    /** @type {?} */
    LogService.prototype.logger$;
}
/** @type {?} */
const LOG_CONSUMER_SERVICE = new InjectionToken('LOG_CONSUMER_SERVICE');
class LogConsumersService {
    /**
     * @param {?} logService
     * @param {?=} services
     */
    constructor(logService, services) {
        this.logService = logService;
        if (services && services.length > 0) {
            services.forEach((/**
             * @param {?} service
             * @return {?}
             */
            service => service.configure(logService)));
        }
    }
}
LogConsumersService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
LogConsumersService.ctorParameters = () => [
    { type: LogService },
    { type: Array, decorators: [{ type: Inject, args: [LOG_CONSUMER_SERVICE,] }, { type: Optional }] }
];
if (false) {
    /**
     * @type {?}
     * @protected
     */
    LogConsumersService.prototype.logService;
}
/**
 * @abstract
 */
class LogConsumer {
    constructor() {
        this.logSubscription = new Subscription();
        this.minimalConsumerLevel = LogLevel.Debug;
    }
    /**
     * @protected
     * @param {?} log
     * @return {?}
     */
    consume(log) {
        if (log.level >= this.minimalConsumerLevel) {
            if (log.level > 0) {
                this.consumeDebug(log.message);
            }
            else if (log.level > 1) {
                this.consumeInfo(log.message);
            }
            else if (log.level > 2) {
                this.consumeWarn(log.message);
            }
            else if (log.level > 3) {
                this.consumeError(log.message);
            }
        }
    }
    /**
     * @param {?} logService
     * @return {?}
     */
    configure(logService) {
        this.logSubscription.add(logService.logger$.subscribe((/**
         * @param {?} log
         * @return {?}
         */
        (log) => this.consume(log))));
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.logSubscription && !this.logSubscription.closed) {
            this.logSubscription.unsubscribe();
        }
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    LogConsumer.prototype.logSubscription;
    /** @type {?} */
    LogConsumer.prototype.minimalConsumerLevel;
    /**
     * @abstract
     * @protected
     * @param {?} message
     * @return {?}
     */
    LogConsumer.prototype.consumeDebug = function (message) { };
    /**
     * @abstract
     * @protected
     * @param {?} message
     * @return {?}
     */
    LogConsumer.prototype.consumeInfo = function (message) { };
    /**
     * @abstract
     * @protected
     * @param {?} message
     * @return {?}
     */
    LogConsumer.prototype.consumeWarn = function (message) { };
    /**
     * @abstract
     * @protected
     * @param {?} message
     * @return {?}
     */
    LogConsumer.prototype.consumeError = function (message) { };
}
class LogConsoleConsumerService extends LogConsumer {
    /**
     * @protected
     * @param {?} message
     * @return {?}
     */
    consumeDebug(message) {
        console.debug(message);
    }
    /**
     * @protected
     * @param {?} message
     * @return {?}
     */
    consumeInfo(message) {
        console.info(message);
    }
    /**
     * @protected
     * @param {?} message
     * @return {?}
     */
    consumeWarn(message) {
        console.warn(message);
    }
    /**
     * @protected
     * @param {?} message
     * @return {?}
     */
    consumeError(message) {
        console.error(message);
    }
}
LogConsoleConsumerService.decorators = [
    { type: Injectable }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class UpdateInfoService {
    /**
     * @param {?} swUpdate
     * @param {?} logService
     */
    constructor(swUpdate, logService) {
        this.swUpdate = swUpdate;
        this.logService = logService;
        this.hasUpdateSubject$ = new BehaviorSubject(false);
        this.hasUpdate$ = this.hasUpdateSubject$.asObservable();
        this.installPromptEventSubject$ = new BehaviorSubject(null);
        this.installPromptEvent$ = this.installPromptEventSubject$.asObservable();
        this.hasInstallOption$ = this.installPromptEvent$.pipe(map((/**
         * @param {?} r
         * @return {?}
         */
        r => r != null)));
        window.addEventListener('beforeinstallprompt', (/**
         * @param {?} event
         * @return {?}
         */
        (event) => {
            this.logService.debug('beforeinstallprompt fired!');
            // Prevent Chrome <= 67 from automatically showing the prompt
            // event.preventDefault();
            // Stash the event so it can be triggered later.
            this.installPromptEventSubject$.next(event);
            // Update the install UI to notify the user app can be installed
            // (doing that by the btnInstall that is displayed if there a installPromptEvent instance)
            //(<any>document.querySelector('#btnInstall')).disabled = false;
        }));
    }
    /**
     * @return {?}
     */
    setUpdateAsAvailable() {
        this.logService.debug(`Atualização encontrada!`);
        this.hasUpdateSubject$.next(true);
    }
    /**
     * @return {?}
     */
    install() {
        this.logService.debug('install fired!');
        /** @type {?} */
        const installPromptEvent = this.installPromptEventSubject$.getValue();
        if (installPromptEvent) {
            this.logService.debug('installPromptEvent');
            installPromptEvent.prompt();
            // Wait for the user to respond to the prompt
            installPromptEvent.userChoice.then((/**
             * @param {?} choice
             * @return {?}
             */
            (choice) => {
                if (choice.outcome === 'accepted') {
                    this.logService.debug('User accepted the A2HS prompt');
                }
                else {
                    this.logService.debug('User dismissed the A2HS prompt');
                }
                // Clear the saved prompt since it can't be used again
                this.installPromptEventSubject$.next(null);
            }));
        }
    }
}
UpdateInfoService.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */
UpdateInfoService.ctorParameters = () => [
    { type: SwUpdate, decorators: [{ type: Optional }] },
    { type: LogService }
];
/** @nocollapse */ UpdateInfoService.ngInjectableDef = defineInjectable({ factory: function UpdateInfoService_Factory() { return new UpdateInfoService(inject(SwUpdate, 8), inject(LogService)); }, token: UpdateInfoService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @protected
     */
    UpdateInfoService.prototype.hasUpdateSubject$;
    /** @type {?} */
    UpdateInfoService.prototype.hasUpdate$;
    /**
     * @type {?}
     * @protected
     */
    UpdateInfoService.prototype.installPromptEventSubject$;
    /**
     * @type {?}
     * @protected
     */
    UpdateInfoService.prototype.installPromptEvent$;
    /** @type {?} */
    UpdateInfoService.prototype.hasInstallOption$;
    /**
     * @type {?}
     * @protected
     */
    UpdateInfoService.prototype.swUpdate;
    /**
     * @type {?}
     * @protected
     */
    UpdateInfoService.prototype.logService;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class UpdateComponent {
    /**
     * @param {?} updateInfoService
     */
    constructor(updateInfoService) {
        this.updateInfoService = updateInfoService;
        this.hasUpdate$ = this.updateInfoService.hasUpdate$;
    }
}
UpdateComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-update',
                template: `
  <mat-progress-spinner [color]="'primary'" [mode]="'indeterminate'"></mat-progress-spinner>
  <div *ngIf="!(hasUpdate$ | async);else hasUpdate">
    Carregando...
  </div>
  <ng-template #hasUpdate>
    Atualizando...
  </ng-template>
  `,
                changeDetection: ChangeDetectionStrategy.OnPush
            }] }
];
/** @nocollapse */
UpdateComponent.ctorParameters = () => [
    { type: UpdateInfoService }
];
if (false) {
    /** @type {?} */
    UpdateComponent.prototype.hasUpdate$;
    /**
     * @type {?}
     * @protected
     */
    UpdateComponent.prototype.updateInfoService;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class ToastComponent extends Toast {
    /**
     * @param {?} toastrService
     * @param {?} toastPackage
     * @param {?} renderer
     * @param {?} hotkeysService
     */
    constructor(toastrService, toastPackage, renderer, hotkeysService) {
        super(toastrService, toastPackage);
        this.toastrService = toastrService;
        this.toastPackage = toastPackage;
        this.renderer = renderer;
        this.hotkeysService = hotkeysService;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    action(event) {
        event.stopPropagation();
        this.options.objectAction.action();
        this.toastPackage.triggerAction();
        return false;
    }
    /**
     * @return {?}
     */
    ngAfterViewInit() {
        if (this.options.error) {
            this.hotkeysService.add(new Hotkey('esc', (/**
             * @param {?} event
             * @return {?}
             */
            (event) => {
                this.toastrService.clear(this.toastrService.currentlyActive);
                this.remove();
                return false;
            })));
            /** @type {?} */
            const toast = this.toastrService.toasts.find((/**
             * @param {?} c
             * @return {?}
             */
            c => c.toastId === this.toastPackage.toastId && !c.toastRef.isInactive()));
            if (toast) {
                this.divCenter = this.renderer.createElement('div');
                this.renderer.addClass(this.divCenter, 'block-toast');
                /** @type {?} */
                const existBlockToast = this.toastrService.toasts.some((/**
                 * @param {?} c
                 * @return {?}
                 */
                (c) => this.isBlockBodyToast(c.toastRef)));
                if (!existBlockToast) {
                    this.blockBodyToast = true;
                    document.body.appendChild(this.divCenter);
                }
                toast.onHidden.subscribe((/**
                 * @return {?}
                 */
                () => {
                    if (this.blockBodyToast) {
                        document.body.removeChild(this.divCenter);
                    }
                }));
            }
        }
    }
    /**
     * @param {?} toast
     * @return {?}
     */
    isBlockBodyToast(toast) {
        if (toast && (toast.componentInstance instanceof ToastComponent)) {
            return toast.componentInstance.blockBodyToast;
        }
        return false;
    }
}
ToastComponent.decorators = [
    { type: Component, args: [{
                selector: '[uikit-toast-component]',
                template: "<div *ngIf=\"!options.error\" class=\"toast-content-left\">\n  <div class=\"toast-content-icon\" *ngIf=\"options.icon\">\n    <i [class]=\"options.icon\"></i>\n  </div>\n</div>\n<div *ngIf=\"options.error\" class=\"toast-content-topo\">\n  <p>Ops! Algo aconteceu.</p>\n</div>\n<button *ngIf=\"options.closeButton\" (click)=\"remove()\" class=\"toast-close-button\" aria-label=\"Close\">\n  <span aria-hidden=\"true\">&times;</span>\n</button>\n<div class=\"row\" [style.display]=\"state.value === 'inactive' ? 'none' : ''\">\n  <div class=\"col-9\">\n    <div *ngIf=\"options.title\" [class]=\"options.titleClass\" [attr.aria-label]=\"title\">\n      {{ options.title }}\n    </div>\n    <div *ngIf=\"options.message && options.enableHtml\" role=\"alert\" aria-live=\"polite\" [class]=\"options.messageClass\"\n         [innerHTML]=\"message\">\n    </div>\n    <div *ngIf=\"options.message && !options.enableHtml\" role=\"alert\" aria-live=\"polite\" [class]=\"options.messageClass\"\n         [attr.aria-label]=\"message\">\n      {{ options.message }}\n    </div>\n  </div>\n  <div *ngIf=\"options.objectAction && options.objectAction.display && !options.error\"\n       class=\"col-3 text-right toast-action\">\n    <a class=\"btn btn-link btn-sm\" (click)=\"action($event)\">\n      {{ options.objectAction.display }}\n    </a>\n  </div>\n</div>\n<div *ngIf=\"options.error\">\n  <div class=\"icone\"><i [class]=\"options.icon\"></i></div>\n  <details class=\"toast-error-details\">\n\n    <summary>{{options.error.name}}</summary>\n    <div class=\"toast-details-error\" id=\"style-3\">\n      <p>Message: {{options.error.message}}</p>\n      <p>Stack: {{options.error.stack}}</p>\n    </div>\n  </details>\n</div>\n\n<div *ngIf=\"options.progressBar\">\n  <div class=\"toast-progress\" [style.width]=\"width + '%'\"></div>\n</div>\n",
                animations: [
                    trigger('flyInOut', [
                        state('inactive', style({ opacity: 0 })),
                        state('active', style({ opacity: 1 })),
                        state('removed', style({ opacity: 0 })),
                        transition('inactive => active', animate('{{ easeTime }}ms {{ easing }}')),
                        transition('active => removed', animate('{{ easeTime }}ms {{ easing }}'))
                    ])
                ],
                preserveWhitespaces: false,
                styles: [".btn-link{font-size:11px}.toast-action{text-align:center;text-transform:uppercase}"]
            }] }
];
/** @nocollapse */
ToastComponent.ctorParameters = () => [
    { type: ToastrService },
    { type: ToastPackage },
    { type: Renderer2 },
    { type: HotkeysService }
];
if (false) {
    /** @type {?} */
    ToastComponent.prototype.options;
    /**
     * @type {?}
     * @protected
     */
    ToastComponent.prototype.divCenter;
    /** @type {?} */
    ToastComponent.prototype.blockBodyToast;
    /**
     * @type {?}
     * @protected
     */
    ToastComponent.prototype.toastrService;
    /** @type {?} */
    ToastComponent.prototype.toastPackage;
    /**
     * @type {?}
     * @protected
     */
    ToastComponent.prototype.renderer;
    /** @type {?} */
    ToastComponent.prototype.hotkeysService;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class ToastService {
    /**
     * @param {?} toastrService
     */
    constructor(toastrService) {
        this.toastrService = toastrService;
        this.defaultConfig = {
            timeOut: 10000,
            toastComponent: ToastComponent,
            progressBar: true,
            positionClass: 'toast-bottom-right',
            extendedTimeOut: 5000,
        };
    }
    /**
     * @param {?} message
     * @param {?=} title
     * @param {?=} objectAction
     * @return {?}
     */
    success(message, title, objectAction) {
        /** @type {?} */
        const config = this.applyConfig({ message, title, objectAction });
        return this.toastrService.success(config.message, config.title, config);
    }
    /**
     * @param {?} message
     * @param {?} error
     * @param {?=} title
     * @param {?=} objectAction
     * @return {?}
     */
    error(message, error, title, objectAction) {
        /** @type {?} */
        const config = this.applyConfig({
            message,
            title,
            error,
            objectAction,
            disableTimeOut: true,
            preventDuplicates: true,
            progressBar: false,
            autoDismiss: false,
            positionClass: 'toast-center-center',
            tapToDismiss: false,
            closeButton: true
        });
        return this.toastrService.error(config.message, config.title, config);
    }
    /**
     * @param {?} message
     * @param {?=} title
     * @param {?=} objectAction
     * @return {?}
     */
    info(message, title, objectAction) {
        /** @type {?} */
        const disableTimeOut = objectAction != null;
        /** @type {?} */
        const config = this.applyConfig({ message, title, objectAction, disableTimeOut });
        return this.toastrService.info(config.message, config.title, config);
    }
    /**
     * @param {?} message
     * @param {?=} title
     * @param {?=} objectAction
     * @return {?}
     */
    warning(message, title, objectAction) {
        /** @type {?} */
        const disableTimeOut = objectAction != null;
        /** @type {?} */
        const config = this.applyConfig({ message, title, objectAction, disableTimeOut });
        return this.toastrService.warning(config.message, config.title, config);
    }
    /**
     * @param {?} isLoading$
     * @param {?=} message
     * @param {?=} title
     * @param {?=} objectAction
     * @return {?}
     */
    loading(isLoading$, message = 'Carregando...', title, objectAction) {
        if (this.loadingSubscription && this.loadingSubscription.closed === false) {
            if (this.loadingToaster && this.loadingToaster.toastRef) {
                try {
                    this.loadingToaster.toastRef.close();
                }
                catch (error) {
                }
            }
            this.loadingSubscription.unsubscribe();
        }
        this.loadingSubscription = isLoading$.subscribe((/**
         * @param {?} isLoading
         * @return {?}
         */
        isLoading => {
            timer().subscribe((/**
             * @return {?}
             */
            () => {
                if (isLoading) {
                    /** @type {?} */
                    const config = this.applyConfig({
                        message: message === '' ? 'Carregando...' : message,
                        title,
                        objectAction,
                        disableTimeOut: true,
                        preventDuplicates: true,
                        progressBar: false,
                        autoDismiss: false,
                        positionClass: 'toast-top-center',
                        tapToDismiss: false
                    });
                    this.loadingToaster = this.toastrService.show(config.message, config.title, config, 'toast-loading');
                }
                else {
                    if (this.loadingToaster) {
                        this.loadingToaster.toastRef.close();
                    }
                }
            }));
        }));
    }
    /**
     * @param {?=} toastId
     * @return {?}
     */
    clear(toastId) {
        this.toastrService.clear(toastId);
    }
    /**
     * @param {?} config
     * @return {?}
     */
    show(config) {
        config = this.applyConfig(config);
        return this.toastrService.show(config.message, config.title, config);
    }
    /**
     * @private
     * @param {?=} override
     * @return {?}
     */
    applyConfig(override = {}) {
        if (override.objectAction && override.objectAction.display.length > 17) {
            override.objectAction.display = override.objectAction.display.substring(0, 17);
        }
        return Object.assign({}, this.defaultConfig, override);
    }
}
ToastService.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */
ToastService.ctorParameters = () => [
    { type: ToastrService }
];
/** @nocollapse */ ToastService.ngInjectableDef = defineInjectable({ factory: function ToastService_Factory() { return new ToastService(inject(ToastrService)); }, token: ToastService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    ToastService.prototype.loadingSubscription;
    /**
     * @type {?}
     * @private
     */
    ToastService.prototype.loadingToaster;
    /**
     * @type {?}
     * @private
     */
    ToastService.prototype.defaultConfig;
    /**
     * @type {?}
     * @protected
     */
    ToastService.prototype.toastrService;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class UpdateService {
    /**
     * @param {?} matDialog
     * @param {?} toastService
     * @param {?} appRef
     * @param {?} ngZone
     * @param {?} router
     * @param {?} swUpdate
     * @param {?} logService
     * @param {?} updateInfoService
     */
    constructor(matDialog, toastService, appRef, ngZone, router, swUpdate, logService, updateInfoService) {
        this.matDialog = matDialog;
        this.toastService = toastService;
        this.appRef = appRef;
        this.ngZone = ngZone;
        this.router = router;
        this.swUpdate = swUpdate;
        this.logService = logService;
        this.updateInfoService = updateInfoService;
        this.primeiroAcesso = true;
        this.hasUpdate = false;
        this.updateSubscription = Subscription.EMPTY;
        this.matDialogRef = null;
        if (this.swUpdate && this.swUpdate.isEnabled) {
            navigator.serviceWorker.getRegistrations().then((/**
             * @param {?} registrations
             * @return {?}
             */
            registrations => {
                /** @type {?} */
                const possuiSwRegistrado = registrations.length > 0;
                if (possuiSwRegistrado) {
                    // Mostra dialogo para carregando atualizações
                    this.matDialogRef = this.matDialog.open(UpdateComponent, (/** @type {?} */ ({ disableClose: true, hasBackdrop: true })));
                }
            }));
            this.updateSubscription = this.updateInfoService.hasUpdate$.subscribe((/**
             * @param {?} hasUpdate
             * @return {?}
             */
            hasUpdate => this.hasUpdate = hasUpdate));
            this.swUpdate.available.subscribe((/**
             * @param {?} event
             * @return {?}
             */
            (event) => {
                this.updateInfoService.setUpdateAsAvailable();
                this.logService.info(`Current version is: ${JSON.stringify(event.current)}`);
                this.logService.info(`Available version is: ${JSON.stringify(event.available)}`);
                if (this.primeiroAcesso) {
                    this.update();
                }
                else {
                    if (this.matDialogRef) {
                        this.matDialogRef.close();
                    }
                }
            }));
            this.swUpdate.activated.subscribe((/**
             * @param {?} event
             * @return {?}
             */
            (event) => {
                this.logService.info(`Old version was: ${JSON.stringify(event.previous)}`);
                this.logService.info(`New version is: ${JSON.stringify(event.current)}`);
                localStorage.setItem('updated', 'true');
                this.logService.debug(`Atualizando...`);
                try {
                    resetStores();
                }
                catch (error) {
                    this.logService.error(error);
                }
                document.location.reload(true);
            }));
            /** @type {?} */
            const appIsStable$ = appRef.isStable.pipe(first((/**
             * @param {?} isStable
             * @return {?}
             */
            isStable => isStable === true)));
            /** @type {?} */
            const everyTime$ = interval(1 * 60 * 5000);
            /** @type {?} */
            const everyTimeOnceAppIsStable$ = appIsStable$.pipe(concat(everyTime$));
            everyTimeOnceAppIsStable$.subscribe((/**
             * @return {?}
             */
            () => this.checkUpdate()));
            this.checkUpdate();
        }
        if (localStorage.getItem('updated') === 'true') {
            this.toastService.success('Atualização realizada com sucesso!');
            localStorage.removeItem('updated');
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.updateSubscription && this.updateSubscription.closed) {
            this.updateSubscription.unsubscribe();
        }
    }
    /**
     * @return {?}
     */
    update() {
        if (this.matDialogRef == null) {
            this.matDialogRef = this.matDialog.open(UpdateComponent, (/** @type {?} */ ({ disableClose: true, hasBackdrop: true })));
        }
        this.swUpdate.activateUpdate();
    }
    /**
     * @private
     * @return {?}
     */
    checkUpdate() {
        this.logService.debug(`Verificando por atualização...`);
        this.swUpdate.checkForUpdate()
            .then((/**
         * @return {?}
         */
        () => {
            this.logService.debug('checkForUpdate - then');
            this.ngZone.run((/**
             * @return {?}
             */
            () => {
                this.primeiroAcesso = false;
                if (!this.hasUpdate) {
                    this.logService.debug(`this.matDialogRef`);
                    if (this.matDialogRef) {
                        this.logService.debug(`this.matDialogRef.close()`);
                        this.matDialogRef.close();
                    }
                }
            }));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            this.logService.debug('checkForUpdate - error');
            this.logService.debug(error);
            this.ngZone.run((/**
             * @return {?}
             */
            () => {
                /** @type {?} */
                let erro = null;
                if (error.message) {
                    erro = error.message;
                }
                else {
                    erro = error;
                }
                this.logService.error(erro);
                if (this.matDialogRef) {
                    this.matDialogRef.close();
                }
            }));
        }));
    }
}
UpdateService.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */
UpdateService.ctorParameters = () => [
    { type: MatDialog },
    { type: ToastService },
    { type: ApplicationRef },
    { type: NgZone },
    { type: Router },
    { type: SwUpdate, decorators: [{ type: Optional }] },
    { type: LogService, decorators: [{ type: Optional }] },
    { type: UpdateInfoService }
];
/** @nocollapse */ UpdateService.ngInjectableDef = defineInjectable({ factory: function UpdateService_Factory() { return new UpdateService(inject(MatDialog$1), inject(ToastService), inject(ApplicationRef), inject(NgZone), inject(Router), inject(SwUpdate, 8), inject(LogService, 8), inject(UpdateInfoService)); }, token: UpdateService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    UpdateService.prototype.primeiroAcesso;
    /**
     * @type {?}
     * @private
     */
    UpdateService.prototype.hasUpdate;
    /**
     * @type {?}
     * @private
     */
    UpdateService.prototype.updateSubscription;
    /**
     * @type {?}
     * @private
     */
    UpdateService.prototype.matDialogRef;
    /**
     * @type {?}
     * @protected
     */
    UpdateService.prototype.matDialog;
    /**
     * @type {?}
     * @protected
     */
    UpdateService.prototype.toastService;
    /**
     * @type {?}
     * @protected
     */
    UpdateService.prototype.appRef;
    /**
     * @type {?}
     * @protected
     */
    UpdateService.prototype.ngZone;
    /**
     * @type {?}
     * @protected
     */
    UpdateService.prototype.router;
    /**
     * @type {?}
     * @protected
     */
    UpdateService.prototype.swUpdate;
    /**
     * @type {?}
     * @protected
     */
    UpdateService.prototype.logService;
    /**
     * @type {?}
     * @protected
     */
    UpdateService.prototype.updateInfoService;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NotificacaoComponent {
    // @Input() click: any;
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
NotificacaoComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-notificacao',
                template: `
  <a mat-list-item [class.is-read]="read">
  <div>
    <h4 *ngIf="nome">
      <mat-icon *ngIf="icone" mat-list-icon class="{{ icone }}"></mat-icon>
      {{ nome }}
    </h4>
    <p>
      {{ descricao }}
    </p>
  </div>
  <mat-icon matListIcon class="fa-lg far" [class.fa-eye]="!read" [class.fa-eye-slash]="read"></mat-icon>
  `,
                changeDetection: ChangeDetectionStrategy.OnPush
            }] }
];
/** @nocollapse */
NotificacaoComponent.ctorParameters = () => [];
NotificacaoComponent.propDecorators = {
    nome: [{ type: Input }],
    icone: [{ type: Input }],
    descricao: [{ type: Input }],
    data: [{ type: Input }],
    read: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    NotificacaoComponent.prototype.nome;
    /** @type {?} */
    NotificacaoComponent.prototype.icone;
    /** @type {?} */
    NotificacaoComponent.prototype.descricao;
    /** @type {?} */
    NotificacaoComponent.prototype.data;
    /** @type {?} */
    NotificacaoComponent.prototype.read;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {number} */
const LayoutType = {
    normal: 0,
    fullscreen: 1,
    noshell: 2,
    noshellfullscreen: 3,
    noshellnobreadcrumb: 4,
    noshellnobreadcrumbfullscreen: 5,
};
LayoutType[LayoutType.normal] = 'normal';
LayoutType[LayoutType.fullscreen] = 'fullscreen';
LayoutType[LayoutType.noshell] = 'noshell';
LayoutType[LayoutType.noshellfullscreen] = 'noshellfullscreen';
LayoutType[LayoutType.noshellnobreadcrumb] = 'noshellnobreadcrumb';
LayoutType[LayoutType.noshellnobreadcrumbfullscreen] = 'noshellnobreadcrumbfullscreen';
/**
 * @record
 */
function FsDocumentElement() { }
if (false) {
    /** @type {?|undefined} */
    FsDocumentElement.prototype.msRequestFullscreen;
    /** @type {?|undefined} */
    FsDocumentElement.prototype.mozRequestFullScreen;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class LayoutService {
    constructor() {
        this.isMobileSubject$ = new BehaviorSubject(false);
        this.isMobile$ = this.isMobileSubject$.asObservable();
        this.hiddenShellSubject$ = new BehaviorSubject(false);
        this.hiddenShell$ = this.hiddenShellSubject$.asObservable();
        this.showShellSubject$ = new BehaviorSubject(true);
        this.showShell$ = this.showShellSubject$.asObservable();
        this.showBreadcrumbSubject$ = new BehaviorSubject(true);
        this.showBreadcrumb$ = this.showBreadcrumbSubject$.asObservable();
        this.showFullScreenSubject$ = new BehaviorSubject(false);
        this.showFullScreen$ = this.showFullScreenSubject$.asObservable();
        this.isFixedSubject$ = new BehaviorSubject(false);
        this.isFixed$ = this.isFixedSubject$.asObservable();
        this.menuItemsSubject$ = new BehaviorSubject([]);
        this.menuItems$ = this.menuItemsSubject$.asObservable();
        this.notificacoesSubject$ = new BehaviorSubject([]);
        this.notificacoes$ = this.notificacoesSubject$.asObservable();
        this.menuItemsFlattened$ = this.menuItems$.pipe(map((/**
         * @param {?} menuItems
         * @return {?}
         */
        menuItems => {
            /** @type {?} */
            const menuItemsFlattened = (/** @type {?} */ ([]));
            /** @type {?} */
            const flattenObject = (/**
             * @param {?} obj
             * @return {?}
             */
            (obj) => {
                if (obj.children) {
                    obj.children.forEach((/**
                     * @param {?} value
                     * @return {?}
                     */
                    (value) => {
                        flattenObject(value);
                    }));
                }
                menuItemsFlattened.push(obj);
            });
            menuItems.forEach(flattenObject);
            return menuItemsFlattened;
        })));
    }
    /**
     * @param {?} notificacoesModel
     * @return {?}
     */
    setNotificacoes(notificacoesModel) {
        this.notificacoesSubject$.next(notificacoesModel);
    }
    /**
     * @param {?} fixed
     * @return {?}
     */
    setFixed(fixed) {
        this.isFixedSubject$.next(fixed);
    }
    /**
     * @param {?} menuItems
     * @return {?}
     */
    setMenuItems(menuItems) {
        this.menuItemsSubject$.next(menuItems);
    }
    /**
     * @return {?}
     */
    toggleMobile() {
        this.isMobileSubject$.next(!this.isMobileSubject$.getValue());
    }
    /**
     * @private
     * @param {?} noShell
     * @return {?}
     */
    noShell(noShell) {
        this.hiddenShellSubject$.next(noShell);
    }
    /**
     * @private
     * @param {?} mostrarShell
     * @return {?}
     */
    showShell(mostrarShell) {
        this.showShellSubject$.next(mostrarShell);
    }
    /**
     * @private
     * @param {?} mostrarBreadcrumb
     * @return {?}
     */
    showBreadcrumb(mostrarBreadcrumb) {
        this.showBreadcrumbSubject$.next(mostrarBreadcrumb);
    }
    /**
     * @private
     * @param {?} mostraFullScreen
     * @return {?}
     */
    showFullScreen(mostraFullScreen) {
        this.showFullScreenSubject$.next(mostraFullScreen);
    }
    /**
     * @private
     * @return {?}
     */
    noShellShowBreadFullScreen() {
        this.noShell(false);
        this.showFullScreen(true);
    }
    /**
     * @private
     * @return {?}
     */
    noShellNoBreadcrumb() {
        this.noShell(false);
        this.showBreadcrumb(false);
    }
    /**
     * @private
     * @return {?}
     */
    noShellNoBreadcrumbFullScreen() {
        this.noShellShowBreadFullScreen();
        this.showBreadcrumb(true);
    }
    /**
     * @param {?} layoutType
     * @return {?}
     */
    setType(layoutType) {
        // const el = document.body;
        if (LayoutType.fullscreen === layoutType) {
            this.showFullScreen(true);
            this.noShell(true);
            this.showBreadcrumb(true);
        }
        else if (LayoutType.noshell === layoutType) {
            this.noShell(false);
            this.showBreadcrumb(true);
        }
        else if (LayoutType.noshellfullscreen === layoutType) {
            this.noShellShowBreadFullScreen();
        }
        else if (LayoutType.noshellnobreadcrumb === layoutType) {
            this.noShellNoBreadcrumb();
        }
        else if (LayoutType.noshellnobreadcrumbfullscreen === layoutType) {
            this.noShellNoBreadcrumbFullScreen();
        }
        else {
            this.noShell(true);
            this.showBreadcrumb(true);
            this.showShell(true);
        }
    }
}
LayoutService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
LayoutService.ctorParameters = () => [];
if (false) {
    /** @type {?} */
    LayoutService.prototype.isMobileSubject$;
    /** @type {?} */
    LayoutService.prototype.isMobile$;
    /**
     * @type {?}
     * @protected
     */
    LayoutService.prototype.hiddenShellSubject$;
    /** @type {?} */
    LayoutService.prototype.hiddenShell$;
    /**
     * @type {?}
     * @protected
     */
    LayoutService.prototype.showShellSubject$;
    /** @type {?} */
    LayoutService.prototype.showShell$;
    /**
     * @type {?}
     * @protected
     */
    LayoutService.prototype.showBreadcrumbSubject$;
    /** @type {?} */
    LayoutService.prototype.showBreadcrumb$;
    /**
     * @type {?}
     * @protected
     */
    LayoutService.prototype.showFullScreenSubject$;
    /** @type {?} */
    LayoutService.prototype.showFullScreen$;
    /**
     * @type {?}
     * @private
     */
    LayoutService.prototype.isFixedSubject$;
    /** @type {?} */
    LayoutService.prototype.isFixed$;
    /**
     * @type {?}
     * @private
     */
    LayoutService.prototype.menuItemsSubject$;
    /** @type {?} */
    LayoutService.prototype.menuItems$;
    /**
     * @type {?}
     * @private
     */
    LayoutService.prototype.notificacoesSubject$;
    /** @type {?} */
    LayoutService.prototype.notificacoes$;
    /** @type {?} */
    LayoutService.prototype.menuItemsFlattened$;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BreadcrumbService {
    /**
     * @param {?} router
     */
    constructor(router) {
        this.router = router;
        this.breadcrumbChanged = new EventEmitter(false);
        this.breadcrumbs = new Array();
        this.router.events.subscribe((/**
         * @param {?} routeEvent
         * @return {?}
         */
        (routeEvent) => { this.onRouteEvent(routeEvent); }));
    }
    /**
     * @param {?} route
     * @param {?} name
     * @return {?}
     */
    changeBreadcrumb(route, name) {
        /** @type {?} */
        const rootUrl = this.createRootUrl(route);
        /** @type {?} */
        const breadcrumb = this.breadcrumbs.find((/**
         * @param {?} bc
         * @return {?}
         */
        function (bc) { return bc.link === rootUrl; }));
        if (!breadcrumb) {
            return;
        }
        breadcrumb.title = name;
        this.breadcrumbChanged.emit(this.breadcrumbs);
    }
    /**
     * @private
     * @param {?} routeEvent
     * @return {?}
     */
    onRouteEvent(routeEvent) {
        if (!(routeEvent instanceof NavigationEnd)) {
            return;
        }
        /** @type {?} */
        let route = this.router.routerState.root.snapshot;
        /** @type {?} */
        let url = '';
        /** @type {?} */
        var breadCrumbIndex = 0;
        /** @type {?} */
        var newCrumbs = [];
        while (route.firstChild != null) {
            route = route.firstChild;
            if (route.routeConfig === null) {
                continue;
            }
            if (!route.routeConfig.path) {
                continue;
            }
            url += `/${this.createUrl(route)}`;
            if (!route.data['breadcrumb']) {
                continue;
            }
            /** @type {?} */
            var newCrumb = this.createBreadcrumb(route, url)
            // if (breadCrumbIndex < this.breadcrumbs.length) {
            //   var existing = this.breadcrumbs[breadCrumbIndex++];
            //   if (existing && existing.route == route.routeConfig) {
            //     newCrumb.title = existing.title;
            //   }
            // }
            ;
            // if (breadCrumbIndex < this.breadcrumbs.length) {
            //   var existing = this.breadcrumbs[breadCrumbIndex++];
            //   if (existing && existing.route == route.routeConfig) {
            //     newCrumb.title = existing.title;
            //   }
            // }
            newCrumbs.push(newCrumb);
        }
        this.breadcrumbs = newCrumbs;
        this.breadcrumbChanged.emit(this.breadcrumbs);
    }
    /**
     * @private
     * @param {?} route
     * @param {?} link
     * @return {?}
     */
    createBreadcrumb(route, link) {
        // Generates display text from data
        // -- Dynamic route params when ':[id]'
        /** @type {?} */
        const breadcrumb = route.data['breadcrumb'];
        /** @type {?} */
        let d = this.getTitleFormatted(breadcrumb, route);
        return {
            title: d,
            terminal: this.isTerminal(route),
            link: link,
            route: route.routeConfig
        };
    }
    /**
     * @param {?} breadcrumb
     * @param {?} route
     * @return {?}
     */
    getTitleFormatted(breadcrumb, route) {
        /** @type {?} */
        let d = '';
        /** @type {?} */
        const split = breadcrumb.split(' ');
        split.forEach((/**
         * @param {?} s
         * @return {?}
         */
        (s) => {
            d += `${s.indexOf(':') > -1 ? (route.params[s.slice(1)] ? route.params[s.slice(1)] : '') : s} `;
        }));
        d = d.slice(0, -1);
        return d;
    }
    /**
     * @private
     * @param {?} route
     * @return {?}
     */
    isTerminal(route) {
        return route.firstChild === null
            || route.firstChild.routeConfig === null
            || !route.firstChild.routeConfig.path;
    }
    /**
     * @private
     * @param {?} route
     * @return {?}
     */
    createUrl(route) {
        return route.url.map((/**
         * @param {?} s
         * @return {?}
         */
        function (s) { return s.toString(); })).join('/');
    }
    /**
     * @private
     * @param {?} route
     * @return {?}
     */
    createRootUrl(route) {
        /** @type {?} */
        let url = '';
        /** @type {?} */
        let next = route.root;
        while (next.firstChild !== null) {
            next = next.firstChild;
            if (next.routeConfig === null) {
                continue;
            }
            if (!next.routeConfig.path) {
                continue;
            }
            url += `/${this.createUrl(next)}`;
            if (next === route) {
                break;
            }
        }
        return url;
    }
}
BreadcrumbService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
BreadcrumbService.ctorParameters = () => [
    { type: Router }
];
if (false) {
    /** @type {?} */
    BreadcrumbService.prototype.breadcrumbChanged;
    /** @type {?} */
    BreadcrumbService.prototype.breadcrumbs;
    /**
     * @type {?}
     * @private
     */
    BreadcrumbService.prototype.router;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function FavNavsState() { }
let FavNavsStore = class FavNavsStore extends EntityStore {
    constructor() {
        super();
    }
};
FavNavsStore.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */
FavNavsStore.ctorParameters = () => [];
/** @nocollapse */ FavNavsStore.ngInjectableDef = defineInjectable({ factory: function FavNavsStore_Factory() { return new FavNavsStore(); }, token: FavNavsStore, providedIn: "root" });
FavNavsStore = __decorate([
    StoreConfig({ name: 'favnavs' }),
    __metadata("design:paramtypes", [])
], FavNavsStore);

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class FavNavsQuery extends QueryEntity {
    /**
     * @param {?} store
     * @param {?} router
     * @param {?} ngLocation
     */
    constructor(store, router, ngLocation) {
        super(store);
        this.store = store;
        this.router = router;
        this.ngLocation = ngLocation;
        this.favorites$ = this.selectAll();
    }
    /**
     * @param {?} menuItem
     * @return {?}
     */
    isFavorited(menuItem) {
        return this.selectAll({
            filterBy: (/**
             * @param {?} item
             * @return {?}
             */
            item => {
                /** @type {?} */
                const menuItemLink = this.router.createUrlTree(Array.isArray(menuItem.link) ? menuItem.link : [menuItem.link]).toString();
                return item.link === menuItemLink;
            })
        }).pipe(map((/**
         * @param {?} value
         * @return {?}
         */
        value => toBoolean(value))));
    }
    /**
     * @param {?} menuItem
     * @return {?}
     */
    getIsFavorited(menuItem) {
        return this.getAll({
            filterBy: (/**
             * @param {?} item
             * @return {?}
             */
            item => {
                /** @type {?} */
                const menuItemLink = this.router.createUrlTree(Array.isArray(menuItem.link) ? menuItem.link : [menuItem.link]).toString();
                return item.link === menuItemLink;
            })
        }).length > 0;
    }
    /**
     * @param {?} menuItem
     * @return {?}
     */
    getFavorited(menuItem) {
        return this.getAll({
            filterBy: (/**
             * @param {?} item
             * @return {?}
             */
            item => {
                /** @type {?} */
                const menuItemLink = this.router.createUrlTree(Array.isArray(menuItem.link) ? menuItem.link : [menuItem.link]).toString();
                return item.link === menuItemLink;
            })
        }).filter((/**
         * @param {?} item
         * @return {?}
         */
        item => isDefined(item)))[0];
    }
}
FavNavsQuery.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */
FavNavsQuery.ctorParameters = () => [
    { type: FavNavsStore },
    { type: Router },
    { type: Location }
];
/** @nocollapse */ FavNavsQuery.ngInjectableDef = defineInjectable({ factory: function FavNavsQuery_Factory() { return new FavNavsQuery(inject(FavNavsStore), inject(Router), inject(Location)); }, token: FavNavsQuery, providedIn: "root" });
if (false) {
    /** @type {?} */
    FavNavsQuery.prototype.favorites$;
    /**
     * @type {?}
     * @protected
     */
    FavNavsQuery.prototype.store;
    /**
     * @type {?}
     * @protected
     */
    FavNavsQuery.prototype.router;
    /**
     * @type {?}
     * @protected
     */
    FavNavsQuery.prototype.ngLocation;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function MenuItem() { }
if (false) {
    /** @type {?} */
    MenuItem.prototype.id;
    /** @type {?|undefined} */
    MenuItem.prototype.children;
    /** @type {?} */
    MenuItem.prototype.title;
    /** @type {?|undefined} */
    MenuItem.prototype.icon;
    /** @type {?|undefined} */
    MenuItem.prototype.link;
    /** @type {?|undefined} */
    MenuItem.prototype.tags;
    /** @type {?|undefined} */
    MenuItem.prototype.nodeId;
}
/**
 * @param {?} item
 * @return {?}
 */
function createItem(item) {
    return (/** @type {?} */ (Object.assign({}, item, { id: guid() })));
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const FAVORITOS_SERVICE_TOKEN = new InjectionToken('FavoritosService');
/**
 * @abstract
 */
class FavoritosService {
    constructor() { }
}
FavoritosService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
FavoritosService.ctorParameters = () => [];
if (false) {
    /**
     * Salva os favoritos do usuário
     * @abstract
     * @param {?} itens Itens da lista de favoritos
     * @return {?}
     */
    FavoritosService.prototype.salvar = function (itens) { };
    /**
     * Retorna os favoritos do usuário
     * @abstract
     * @return {?}
     */
    FavoritosService.prototype.buscar = function () { };
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class AnonymousUser {
    constructor() {
        this.sub = null;
        this.authenticated = false;
        this.name = 'Anônimo';
        this.email = null;
    }
}
if (false) {
    /** @type {?} */
    AnonymousUser.prototype.sub;
    /** @type {?} */
    AnonymousUser.prototype.authenticated;
    /** @type {?} */
    AnonymousUser.prototype.name;
    /** @type {?} */
    AnonymousUser.prototype.email;
}
/**
 * @template TUser
 */
class UserService {
    constructor() {
        this.anonymousUser = new AnonymousUser();
        this.userSubject$ = new BehaviorSubject((/** @type {?} */ (this.anonymousUser)));
        this.user$ = this.userSubject$.asObservable();
    }
    /**
     * @return {?}
     */
    get userValue() {
        return this.userSubject$.getValue();
    }
    /**
     * Seta o usuário (não deve ser usado na aplicação [somente em testes]).
     * Para login e logout utilize o serviço 'AuthenticationService'
     * @param {?} user
     * @return {?}
     */
    load(user) {
        if (!user) {
            throw Error('É obrigatório informar um usuário!');
        }
        this.userSubject$.next(user);
    }
    /**
     * Seta o usuário como o anônimo (não deve ser usado na aplicação [somente em testes]).
     * Para login e logout utilize o serviço 'AuthenticationService'
     * @return {?}
     */
    unload() {
        this.userSubject$.next((/** @type {?} */ (this.anonymousUser)));
    }
}
UserService.decorators = [
    { type: Injectable }
];
if (false) {
    /**
     * @type {?}
     * @protected
     */
    UserService.prototype.anonymousUser;
    /**
     * @type {?}
     * @private
     */
    UserService.prototype.userSubject$;
    /** @type {?} */
    UserService.prototype.user$;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class FavNavsService {
    /**
     * @param {?} favNavsStore
     * @param {?} favNavsQuery
     * @param {?} router
     * @param {?} userService
     * @param {?} favoritosService
     */
    constructor(favNavsStore, favNavsQuery, router, userService, favoritosService) {
        this.favNavsStore = favNavsStore;
        this.favNavsQuery = favNavsQuery;
        this.router = router;
        this.userService = userService;
        this.favoritosService = favoritosService;
        this.possuiRecursoDeFavoritos = this.favoritosService != null;
        this.possuiRecursoDeFavoritos = this.favoritosService != null;
        if (this.possuiRecursoDeFavoritos) {
            // Para cada alteração do usuário, devo então buscar os favoritos deste
            this.userService.user$.subscribe((/**
             * @param {?} user
             * @return {?}
             */
            user => {
                this.favoritosService.buscar()
                    .subscribe((/**
                 * @param {?} menuItens
                 * @return {?}
                 */
                (menuItens) => {
                    this.favNavsStore.set(menuItens);
                }));
            }));
            this.favoritosService.buscar()
                .subscribe((/**
             * @param {?} menuItens
             * @return {?}
             */
            (menuItens) => {
                this.favNavsStore.set(menuItens);
            }));
        }
    }
    /**
     * @param {?} item
     * @return {?}
     */
    toggleItem(item) {
        /** @type {?} */
        const itemFavorited = this.favNavsQuery.getFavorited(item);
        if (isDefined(itemFavorited)) {
            this.remover(itemFavorited);
        }
        else {
            if (isDefined(item.link)) {
                /** @type {?} */
                const parsedUrl = this.router.createUrlTree(Array.isArray(item.link) ? item.link : [item.link]);
                item.link = parsedUrl.toString();
            }
            /** @type {?} */
            const newItem = createItem(item);
            this.adicionar(newItem);
        }
    }
    /**
     * @param {?} indiceOrigem
     * @param {?} indiceDestino
     * @return {?}
     */
    moverItens(indiceOrigem, indiceDestino) {
        /** @type {?} */
        const itens = this.favNavsQuery.getAll();
        moveItemInArray(itens, indiceOrigem, indiceDestino);
        this.substituirColecao(itens);
    }
    /**
     * @private
     * @param {?} item
     * @return {?}
     */
    adicionar(item) {
        this.favNavsStore.add(item);
        this.atualizar();
    }
    /**
     * @private
     * @param {?} item
     * @return {?}
     */
    remover(item) {
        this.favNavsStore.remove(item.id);
        this.atualizar();
    }
    /**
     * @private
     * @param {?} itens
     * @return {?}
     */
    substituirColecao(itens) {
        this.favNavsStore.set(itens);
        this.atualizar();
    }
    /**
     * @private
     * @return {?}
     */
    atualizar() {
        if (this.possuiRecursoDeFavoritos) {
            /** @type {?} */
            const itens = this.favNavsQuery.getAll();
            this.favoritosService.salvar(itens)
                // .pipe(
                //   retryWhen(errors => errors.pipe(delay(1000), take(5)))
                // )
                .subscribe();
        }
    }
}
FavNavsService.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */
FavNavsService.ctorParameters = () => [
    { type: FavNavsStore },
    { type: FavNavsQuery },
    { type: Router },
    { type: UserService },
    { type: FavoritosService, decorators: [{ type: Optional }, { type: Inject, args: [FAVORITOS_SERVICE_TOKEN,] }] }
];
/** @nocollapse */ FavNavsService.ngInjectableDef = defineInjectable({ factory: function FavNavsService_Factory() { return new FavNavsService(inject(FavNavsStore), inject(FavNavsQuery), inject(Router), inject(UserService), inject(FAVORITOS_SERVICE_TOKEN, 8)); }, token: FavNavsService, providedIn: "root" });
if (false) {
    /** @type {?} */
    FavNavsService.prototype.possuiRecursoDeFavoritos;
    /**
     * @type {?}
     * @private
     */
    FavNavsService.prototype.favNavsStore;
    /**
     * @type {?}
     * @private
     */
    FavNavsService.prototype.favNavsQuery;
    /**
     * @type {?}
     * @private
     */
    FavNavsService.prototype.router;
    /**
     * @type {?}
     * @private
     */
    FavNavsService.prototype.userService;
    /**
     * @type {?}
     * @private
     */
    FavNavsService.prototype.favoritosService;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function NavState() { }
if (false) {
    /** @type {?} */
    NavState.prototype.leftnav;
    /** @type {?} */
    NavState.prototype.rightnav;
}
/**
 * @return {?}
 */
function createInitialState() {
    return {
        leftnav: {
            opened: false,
            pinned: false
        },
        rightnav: {
            opened: false,
            pinned: false
        }
    };
}
let NavStore = class NavStore extends Store {
    constructor() {
        super(createInitialState());
    }
};
NavStore.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */
NavStore.ctorParameters = () => [];
/** @nocollapse */ NavStore.ngInjectableDef = defineInjectable({ factory: function NavStore_Factory() { return new NavStore(); }, token: NavStore, providedIn: "root" });
NavStore = __decorate([
    StoreConfig({ name: 'nav' }),
    __metadata("design:paramtypes", [])
], NavStore);

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NavQuery extends Query {
    /**
     * @param {?} store
     * @param {?} favoritosService
     */
    constructor(store, favoritosService) {
        super(store);
        this.store = store;
        this.favoritosService = favoritosService;
        this.leftnav$ = this.select((/**
         * @param {?} session
         * @return {?}
         */
        session => session.leftnav));
        this.rightnav$ = this.select((/**
         * @param {?} session
         * @return {?}
         */
        session => session.rightnav));
        this.isLeftPinned$ = this.select((/**
         * @param {?} session
         * @return {?}
         */
        session => session.leftnav.pinned));
        this.isRightPinned$ = this.select((/**
         * @param {?} session
         * @return {?}
         */
        session => session.rightnav.pinned));
        this.hasFavoritosService = this.favoritosService != null;
    }
}
NavQuery.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
NavQuery.ctorParameters = () => [
    { type: NavStore },
    { type: FavoritosService, decorators: [{ type: Optional }, { type: Inject, args: [FAVORITOS_SERVICE_TOKEN,] }] }
];
/** @nocollapse */ NavQuery.ngInjectableDef = defineInjectable({ factory: function NavQuery_Factory() { return new NavQuery(inject(NavStore), inject(FAVORITOS_SERVICE_TOKEN, 8)); }, token: NavQuery, providedIn: "root" });
if (false) {
    /** @type {?} */
    NavQuery.prototype.leftnav$;
    /** @type {?} */
    NavQuery.prototype.rightnav$;
    /** @type {?} */
    NavQuery.prototype.isLeftPinned$;
    /** @type {?} */
    NavQuery.prototype.isRightPinned$;
    /** @type {?} */
    NavQuery.prototype.hasFavoritosService;
    /**
     * @type {?}
     * @protected
     */
    NavQuery.prototype.store;
    /**
     * @type {?}
     * @protected
     */
    NavQuery.prototype.favoritosService;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NavService {
    /**
     * @param {?} navStore
     */
    constructor(navStore) {
        this.navStore = navStore;
    }
    /**
     * @return {?}
     */
    toggleLeftNav() {
        this.navStore.update((/**
         * @param {?} state
         * @return {?}
         */
        state => ({
            leftnav: Object.assign({}, state.leftnav, { opened: !state.leftnav.opened, pinned: false })
        })));
    }
    /**
     * @return {?}
     */
    openLeftNav() {
        this.navStore.update((/**
         * @param {?} state
         * @return {?}
         */
        state => ({
            leftnav: Object.assign({}, state.leftnav, { opened: true })
        })));
    }
    /**
     * @return {?}
     */
    closeLeftNav() {
        this.navStore.update((/**
         * @param {?} state
         * @return {?}
         */
        state => ({
            leftnav: Object.assign({}, state.leftnav, { opened: false, pinned: false })
        })));
    }
    /**
     * @return {?}
     */
    pinLeftNav() {
        this.navStore.update((/**
         * @param {?} state
         * @return {?}
         */
        state => ({
            leftnav: Object.assign({}, state.leftnav, { pinned: true })
        })));
    }
    /**
     * @return {?}
     */
    unpinLeftNav() {
        this.navStore.update((/**
         * @param {?} state
         * @return {?}
         */
        state => ({
            leftnav: Object.assign({}, state.leftnav, { pinned: false })
        })));
    }
    /**
     * @return {?}
     */
    togglePinLeftNav() {
        this.navStore.update((/**
         * @param {?} state
         * @return {?}
         */
        state => ({
            leftnav: Object.assign({}, state.leftnav, { pinned: !state.leftnav.pinned })
        })));
    }
    /**
     * @return {?}
     */
    toggleRightNav() {
        this.navStore.update((/**
         * @param {?} state
         * @return {?}
         */
        state => ({
            rightnav: Object.assign({}, state.rightnav, { opened: !state.rightnav.opened, pinned: false })
        })));
    }
    /**
     * @return {?}
     */
    openRightNav() {
        this.navStore.update((/**
         * @param {?} state
         * @return {?}
         */
        state => ({
            rightnav: Object.assign({}, state.rightnav, { opened: true })
        })));
    }
    /**
     * @return {?}
     */
    closeRightNav() {
        this.navStore.update((/**
         * @param {?} state
         * @return {?}
         */
        state => ({
            rightnav: Object.assign({}, state.rightnav, { opened: false, pinned: false })
        })));
    }
    /**
     * @return {?}
     */
    pinRightNav() {
        this.navStore.update((/**
         * @param {?} state
         * @return {?}
         */
        state => ({
            rightnav: Object.assign({}, state.rightnav, { pinned: true })
        })));
    }
    /**
     * @return {?}
     */
    unpinRightNav() {
        this.navStore.update((/**
         * @param {?} state
         * @return {?}
         */
        state => ({
            rightnav: Object.assign({}, state.rightnav, { pinned: false })
        })));
    }
    /**
     * @return {?}
     */
    togglePinRightNav() {
        this.navStore.update((/**
         * @param {?} state
         * @return {?}
         */
        state => ({
            rightnav: Object.assign({}, state.rightnav, { pinned: !state.rightnav.pinned })
        })));
    }
}
NavService.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */
NavService.ctorParameters = () => [
    { type: NavStore }
];
/** @nocollapse */ NavService.ngInjectableDef = defineInjectable({ factory: function NavService_Factory() { return new NavService(inject(NavStore)); }, token: NavService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    NavService.prototype.navStore;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class UikitRrippleService {
    constructor() {
    }
    /**
     * @return {?}
     */
    init() {
        document.addEventListener('mousedown', this.rippleMouseDown, false);
    }
    /**
     * @param {?} e
     * @return {?}
     */
    rippleMouseDown(e) {
        /** @type {?} */
        const parentNode = 'parentNode';
        /** @type {?} */
        const isVisible = (/**
         * @param {?} el
         * @return {?}
         */
        (el) => {
            return !!(el.offsetWidth || el.offsetHeight);
        });
        /** @type {?} */
        const selectorMatches = (/**
         * @param {?} el
         * @param {?} selector
         * @return {?}
         */
        (el, selector) => {
            /** @type {?} */
            const matches = 'matches';
            /** @type {?} */
            const webkitMatchesSelector = 'webkitMatchesSelector';
            /** @type {?} */
            const mozMatchesSelector = 'mozMatchesSelector';
            /** @type {?} */
            const msMatchesSelector = 'msMatchesSelector';
            /** @type {?} */
            const p = Element.prototype;
            /** @type {?} */
            const f = p[matches] || p[webkitMatchesSelector] || p[mozMatchesSelector] || p[msMatchesSelector] || (/**
             * @param {?} s
             * @return {?}
             */
            function (s) {
                return [].indexOf.call(document.querySelectorAll(s), this) !== -1;
            });
            return f.call(el, selector);
        });
        /** @type {?} */
        const addClass = (/**
         * @param {?} element
         * @param {?} className
         * @return {?}
         */
        (element, className) => {
            if (element.classList) {
                element.classList.add(className);
            }
            else {
                element.className += ' ' + className;
            }
        });
        /** @type {?} */
        const hasClass = (/**
         * @param {?} element
         * @param {?} className
         * @return {?}
         */
        (element, className) => {
            if (element.classList) {
                return element.classList.contains(className);
            }
            else {
                return new RegExp('(^| )' + className + '( |$)', 'gi').test(element.className);
            }
        });
        /** @type {?} */
        const removeClass = (/**
         * @param {?} element
         * @param {?} className
         * @return {?}
         */
        (element, className) => {
            if (element.classList) {
                element.classList.remove(className);
            }
            else {
                element.className = element.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
            }
        });
        /** @type {?} */
        const getOffset = (/**
         * @param {?} el
         * @return {?}
         */
        (el) => {
            /** @type {?} */
            const rect = el.getBoundingClientRect();
            return {
                top: rect.top + (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0),
                left: rect.left + (window.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft || 0),
            };
        });
        /** @type {?} */
        const rippleEffect = (/**
         * @param {?} element
         * @param {?} e
         * @return {?}
         */
        (element, e) => {
            if (element.querySelector('.ink') === null) {
                /** @type {?} */
                const inkEl = document.createElement('span');
                addClass(inkEl, 'ink');
                if (hasClass(element, 'ripplelink') && element.querySelector('span')) {
                    element.querySelector('span').insertAdjacentHTML('afterend', '<span class=\'ink\'></span>');
                }
                else {
                    element.appendChild(inkEl);
                }
            }
            /** @type {?} */
            const ink = element.querySelector('.ink');
            removeClass(ink, 'ripple-animate');
            if (!ink.offsetHeight && !ink.offsetWidth) {
                /** @type {?} */
                const d = Math.max(element.offsetWidth, element.offsetHeight);
                ink.style.height = d + 'px';
                ink.style.width = d + 'px';
            }
            /** @type {?} */
            const x = e.pageX - getOffset(element).left - (ink.offsetWidth / 2);
            /** @type {?} */
            const y = e.pageY - getOffset(element).top - (ink.offsetHeight / 2);
            ink.style.top = y + 'px';
            ink.style.left = x + 'px';
            ink.style.pointerEvents = 'none';
            addClass(ink, 'ripple-animate');
        });
        for (let target = e.target; target && target !== this; target = target[parentNode]) {
            if (!isVisible(target)) {
                continue;
            }
            if (selectorMatches(target, '.ripplelink, .ui-button, .ui-listbox-item, .ui-multiselect-item, .ui-fieldset-toggler')) {
                /** @type {?} */
                const element = target;
                rippleEffect(element, e);
                break;
            }
        }
    }
}
UikitRrippleService.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */
UikitRrippleService.ctorParameters = () => [];
/** @nocollapse */ UikitRrippleService.ngInjectableDef = defineInjectable({ factory: function UikitRrippleService_Factory() { return new UikitRrippleService(); }, token: UikitRrippleService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class PreviousRouteService {
    /**
     * @param {?} router
     */
    constructor(router) {
        this.router = router;
        this.history = [];
        this.rotasAcessadas = [];
        this.removerRotaAcessada = (/**
         * @return {?}
         */
        () => this.rotasAcessadas.splice(this.rotasAcessadas.length - 2, 2));
        console.log('constructor PreviousRouteService');
        this.desabilitarBotaoVoltar = new BehaviorSubject(true);
        this.router.events
            .pipe(filter((/**
         * @param {?} event
         * @return {?}
         */
        event => event instanceof NavigationEnd)))
            .subscribe((/**
         * @param {?} __0
         * @return {?}
         */
        ({ urlAfterRedirects }) => {
            this.history = [...this.history, urlAfterRedirects];
            this.desabilitarBotaoVoltar.next(this.rotasAcessadas.length === 0);
            this.rotasAcessadas = [...this.rotasAcessadas, urlAfterRedirects];
        }));
        // this.router.events
        //   .pipe(filter((event: RouterEvent) => event instanceof NavigationEnd))
        //   .subscribe((event: NavigationEnd) => {
        //     console.log(`prev -> ${this.currentUrl}`);
        //     console.log(`curr -> ${event.urlAfterRedirects}`);
        //     this.previousUrl.next(this.currentUrl);
        //     this.currentUrl = event.urlAfterRedirects;
        //   });
        // this.previousUrl$.subscribe((previousUrl) => {
        //   this.previousUrl = previousUrl;
        // });
        // this.currentUrl$.subscribe((currentUrl) => {
        //   this.currentUrl = currentUrl;
        // });
        // this.currentUrlSubject.next(this.router.url);
        // router.events.subscribe(event => {
        //   if (event instanceof NavigationEnd) {
        //     this.previousUrlSubject.next(this.currentUrlSubject.getValue());
        //     this.currentUrlSubject.next(event.url);
        //   }
        // });
    }
    /**
     * @return {?}
     */
    getHistory() {
        return this.history;
    }
    /**
     * @return {?}
     */
    getPreviousUrl() {
        return this.history[this.history.length - 2];
    }
}
PreviousRouteService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
PreviousRouteService.ctorParameters = () => [
    { type: Router }
];
/** @nocollapse */ PreviousRouteService.ngInjectableDef = defineInjectable({ factory: function PreviousRouteService_Factory() { return new PreviousRouteService(inject(Router)); }, token: PreviousRouteService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    PreviousRouteService.prototype.history;
    /** @type {?} */
    PreviousRouteService.prototype.desabilitarBotaoVoltar;
    /**
     * @type {?}
     * @private
     */
    PreviousRouteService.prototype.rotasAcessadas;
    /** @type {?} */
    PreviousRouteService.prototype.removerRotaAcessada;
    /**
     * @type {?}
     * @private
     */
    PreviousRouteService.prototype.router;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class LayoutComponent {
    /**
     * @param {?} navQuery
     * @param {?} favNavsQuery
     * @param {?} navService
     * @param {?} scroll
     * @param {?} layoutService
     * @param {?} router
     * @param {?} activatedRoute
     * @param {?} updateInfoService
     * @param {?} updateService
     * @param {?} favNavsService
     * @param {?} hotkeysService
     * @param {?} breadcrumbService
     * @param {?} toastService
     * @param {?} uikitRrippleService
     * @param {?} fb
     * @param {?} document
     * @param {?} previousRouteService
     */
    constructor(navQuery, favNavsQuery, navService, scroll, layoutService, router, activatedRoute, updateInfoService, updateService, favNavsService, hotkeysService, breadcrumbService, toastService, uikitRrippleService, fb, document, previousRouteService) {
        this.navQuery = navQuery;
        this.favNavsQuery = favNavsQuery;
        this.navService = navService;
        this.scroll = scroll;
        this.layoutService = layoutService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.updateInfoService = updateInfoService;
        this.updateService = updateService;
        this.favNavsService = favNavsService;
        this.hotkeysService = hotkeysService;
        this.breadcrumbService = breadcrumbService;
        this.toastService = toastService;
        this.uikitRrippleService = uikitRrippleService;
        this.fb = fb;
        this.document = document;
        this.previousRouteService = previousRouteService;
        this.hasActionsSubject = new BehaviorSubject(false);
        this.hasActions$ = this.hasActionsSubject.asObservable();
        this.showShell = this.layoutService.showShell$;
        this.hiddenBreadcrumb = this.layoutService.showBreadcrumb$;
        this.showFullScrren = this.layoutService.showFullScreen$;
        this.hiddenShell = this.layoutService.hiddenShell$;
        this.showNotifications = false;
        this.showSystemInfo = false;
        this.showUserInfo = false;
        this.showEnvironment = false;
        this.showIa = false;
        this.environment = null;
        this.allowedTypes = [
            'normal',
            'fullscreen',
            'noshell',
            'noshellfullscreen',
            'noshellnobreadcrumb',
            'noshellnobreadcrumbfullscreen'
        ];
        this.defaultType = 'normal';
        this.showShortcuts = true;
        this.subscription = new Subscription();
        this.isFixed$ = this.layoutService.isFixed$;
        this.isMobile$ = this.layoutService.isMobile$;
        this.rightnav$ = this.navQuery.rightnav$;
        this.leftnav$ = this.navQuery.leftnav$;
        this.data$ = new BehaviorSubject(null);
        this.possuiRecursoDeFavoritos = this.favNavsService.possuiRecursoDeFavoritos;
        this.notificacoes$ = this.layoutService.notificacoes$;
        this.hasInstallOption$ = this.updateInfoService.hasInstallOption$;
        this.hasUpdate$ = this.updateInfoService.hasUpdate$;
        this.favorites$ = this.favNavsQuery.favorites$;
    }
    /**
     * @return {?}
     */
    ngAfterContentInit() {
        this.notificacoesProjetadas.changes.subscribe((/**
         * @return {?}
         */
        () => {
            /** @type {?} */
            const notificacoesModel = this.notificacoesProjetadas.toArray().map((/**
             * @param {?} r
             * @return {?}
             */
            r => {
                return {
                    nome: r.nome,
                    icone: r.icone,
                    descricao: r.descricao,
                    data: r.data,
                    read: r.read
                };
            }));
            this.layoutService.setNotificacoes(notificacoesModel);
        }));
        this.notificacoesProjetadas.notifyOnChanges();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.uikitRrippleService.init();
        this.subscription.add(this.leftnav$.subscribe((/**
         * @param {?} leftnav
         * @return {?}
         */
        leftnav => {
            this.leftnav = leftnav;
        })));
        this.subscription.add((this.closeWhenNotPinnedLeftSubscription = this.router.events
            .pipe(filter((/**
         * @param {?} event
         * @return {?}
         */
        event => event instanceof NavigationStart)))
            .subscribe((/**
         * @param {?} event
         * @return {?}
         */
        event => {
            if (this.leftnav.pinned === false) {
                this.navService.closeLeftNav();
            }
        }))));
        this.subscription.add(this.rightnav$.subscribe((/**
         * @param {?} rightnav
         * @return {?}
         */
        rightnav => {
            this.rightnav = rightnav;
        })));
        this.subscription.add((this.closeWhenNotPinnedRightSubscription = this.router.events
            .pipe(filter((/**
         * @param {?} event
         * @return {?}
         */
        event => event instanceof NavigationStart)))
            .subscribe((/**
         * @param {?} event
         * @return {?}
         */
        event => {
            if (this.rightnav.pinned === false) {
                this.navService.closeRightNav();
            }
        }))));
        /** @type {?} */
        const nagivate = this.router.events.pipe(filter((/**
         * @param {?} event
         * @return {?}
         */
        event => event instanceof NavigationEnd)));
        /** @type {?} */
        const params = this.activatedRoute.params;
        merge(nagivate, params).pipe(map((/**
         * @return {?}
         */
        () => {
            /** @type {?} */
            let child = this.activatedRoute.firstChild;
            while (child) {
                if (child.firstChild) {
                    child = child.firstChild;
                }
                else if (child.snapshot.data) {
                    return child.snapshot.data;
                }
                else {
                    return null;
                }
            }
            return null;
        })))
            .subscribe((/**
         * @param {?} customData
         * @return {?}
         */
        customData => {
            /** @type {?} */
            let route = this.router.routerState.root.snapshot;
            while (route.firstChild != null) {
                route = route.firstChild;
            }
            /** @type {?} */
            const data = route.data;
            /** @type {?} */
            const favItem = this.getFavItem(data);
            // console.log('layout - nagivate, params', favItem);
            // favItem.icon = customData ? customData.icon : null;
            this.data$.next(favItem);
        }));
        // When connection become offline, then put the entire app with grayscale (0.8)
        window.addEventListener('online', (/**
         * @return {?}
         */
        () => {
            document.querySelector('body').style.filter = '';
            if (localStorage.getItem('offline') === 'true') {
                timer(600).subscribe((/**
                 * @return {?}
                 */
                () => this.toastService.success('Conexão de rede restabelecida!')));
                localStorage.removeItem('offline');
            }
        }));
        window.addEventListener('offline', (/**
         * @return {?}
         */
        () => {
            document.querySelector('body').style.filter = 'grayscale(0.8)';
            localStorage.setItem('offline', 'true');
            timer(600).subscribe((/**
             * @return {?}
             */
            () => this.toastService.warning('Sem conexão de rede!')));
        }));
        this.subscription.add((this.scrollSubscription = this.scroll
            .scrolled()
            .pipe(map((/**
         * @param {?} data
         * @return {?}
         */
        (data) => {
            if (data) {
                /** @type {?} */
                const element = data.getElementRef();
                if (element) {
                    return element.nativeElement.scrollTop;
                }
            }
        })))
            .subscribe((/**
         * @param {?} scrollY
         * @return {?}
         */
        (scrollY) => {
            if (scrollY) {
                this.layoutService.setFixed(scrollY > 0);
            }
        }))));
        this.router.events
            .pipe(filter((/**
         * @param {?} event
         * @return {?}
         */
        event => event instanceof NavigationEnd)))
            .subscribe((/**
         * @return {?}
         */
        () => {
            this.setLayoutTypeByRouteEvent();
            this.updateIfHasActions();
        }));
        setTimeout((/**
         * @return {?}
         */
        () => {
            this.updateIfHasActions();
        }), 0);
    }
    /**
     * @return {?}
     */
    toggleMobile() {
        this.layoutService.toggleMobile();
    }
    /**
     * @return {?}
     */
    updateIfHasActions() {
        /** @type {?} */
        const elements = document.getElementsByClassName('uikit-actions');
        /** @type {?} */
        const element = elements.item(0);
        if (element != null) {
            /** @type {?} */
            const hasChildNodes = element.hasChildNodes();
            this.hasActionsSubject.next(hasChildNodes);
        }
        else {
            this.hasActionsSubject.next(false);
        }
    }
    /**
     * @return {?}
     */
    ngAfterViewInit() {
        this.hotkeysService.add(new Hotkey('ctrl+m', (/**
         * @param {?} event
         * @return {?}
         */
        (event) => {
            this.leftNavToggle();
            return false;
        })));
        this.hotkeysService.add(new Hotkey('ctrl+alt+j', (/**
         * @param {?} event
         * @return {?}
         */
        (event) => {
            this.rightNavToggle();
            return false;
        })));
        this.isVerifyTypeShellApply();
    }
    /**
     * @private
     * @param {?} customData
     * @return {?}
     */
    getFavItem(customData) {
        /** @type {?} */
        const data = customData;
        /** @type {?} */
        const lastBreadCrumb = this.breadcrumbService.breadcrumbs[this.breadcrumbService.breadcrumbs.length - 1];
        if (lastBreadCrumb != null) {
            /** @type {?} */
            const title = lastBreadCrumb.title;
            return { link: this.router.url, title, icon: data ? data.icon : null };
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
    /**
     * @return {?}
     */
    leftNavToggle() {
        this.navService.toggleLeftNav();
    }
    /**
     * @return {?}
     */
    leftNavClose() {
        this.navService.closeLeftNav();
    }
    /**
     * @return {?}
     */
    rightNavToggle() {
        this.navService.toggleRightNav();
    }
    /**
     * @return {?}
     */
    rightNavClose() {
        this.navService.closeRightNav();
    }
    /**
     * @return {?}
     */
    navsClose() {
        if (this.leftnav.opened && !this.leftnav.pinned) {
            this.navService.toggleLeftNav();
        }
        if (this.rightnav.opened && !this.rightnav.pinned) {
            this.navService.toggleRightNav();
        }
    }
    /**
     * @return {?}
     */
    install() {
        this.updateInfoService.install();
    }
    /**
     * @return {?}
     */
    update() {
        this.updateService.update();
    }
    /**
     * @return {?}
     */
    isVerifyTypeShellApply() {
        this.showFullScrren.subscribe((/**
         * @param {?} isShowFullScreen
         * @return {?}
         */
        (isShowFullScreen) => {
            if (isShowFullScreen) {
                /** @type {?} */
                const fsDocElem = (/** @type {?} */ (this.document.documentElement));
                // document.documentElement as FsDocumentElement;
                this.openFullscreen(fsDocElem);
            }
        }));
    }
    /**
     * @param {?} fsDocElem
     * @return {?}
     */
    openFullscreen(fsDocElem) {
        /*debugger;*/
        if (fsDocElem.requestFullscreen) {
            timer(600).subscribe((/**
             * @return {?}
             */
            () => {
                fsDocElem.requestFullscreen().catch((/**
                 * @param {?} error
                 * @return {?}
                 */
                error => console.error(error)));
            }));
        }
        else if (fsDocElem.msRequestFullscreen) {
            try {
                timer(600).subscribe((/**
                 * @return {?}
                 */
                () => {
                    fsDocElem.msRequestFullscreen().catch((/**
                     * @param {?} error
                     * @return {?}
                     */
                    error => console.error(error)));
                }));
            }
            catch (error) {
                console.error(error);
            }
        }
        else if (fsDocElem.mozRequestFullScreen) {
            try {
                timer(600).subscribe((/**
                 * @return {?}
                 */
                () => {
                    fsDocElem.mozRequestFullScreen().catch((/**
                     * @param {?} error
                     * @return {?}
                     */
                    error => console.error(error)));
                }));
            }
            catch (error) {
                console.error(error);
            }
        }
        else if (fsDocElem.webkitRequestFullscreen) {
            try {
                timer(600).subscribe((/**
                 * @return {?}
                 */
                () => {
                    fsDocElem.webkitRequestFullscreen().catch((/**
                     * @param {?} error
                     * @return {?}
                     */
                    error => console.error(error)));
                }));
            }
            catch (error) {
                console.error(error);
            }
        }
        timer(900).subscribe((/**
         * @return {?}
         */
        () => {
            ((/** @type {?} */ (this.myClickFullscreen.nativeElement))).click();
        }));
    }
    /**
     * @private
     * @return {?}
     */
    closeFullscreen() {
        if (this.document.exitFullscreen) {
            this.document.exitFullscreen();
        }
        else if (this.document.mozCancelFullScreen) {
            /* Firefox */
            this.document.mozCancelFullScreen();
        }
        else if (this.document.webkitExitFullscreen) {
            /* Chrome, Safari and Opera */
            this.document.webkitExitFullscreen();
        }
        else if (this.document.msExitFullscreen) {
            /* IE/Edge */
            this.document.msExitFullscreen();
        }
    }
    /**
     * Region RouteLayoutApplication
     * Modo de apresentação das tela aplicado de forma totalmente configurável a forma geral e por rota.
     * **
     * @return {?}
     */
    setLayoutTypeByRouteEvent() {
        /** @type {?} */
        const queryType = this.router.routerState.root.snapshot.queryParamMap.get('type');
        /** @type {?} */
        const dataRota = this.getData();
        /** @type {?} */
        let typeDesejado = 'normal';
        if (queryType) {
            // usuario informou via url (tem prioridade sobre todos)
            typeDesejado = queryType;
        }
        else if (dataRota && dataRota.defaultType) {
            // esta configurado na rota (tem prioridade somente sobre o global - sobrescrita)
            typeDesejado = dataRota.defaultType;
        }
        else {
            typeDesejado = this.defaultType;
            // busca o global
        }
        if (typeDesejado === 'noshell') {
            this.navService.toggleLeftNav();
        }
        /** @type {?} */
        const defaultTypeRoute = (dataRota && dataRota.defaultType) ? dataRota.defaultType : this.defaultType;
        if (dataRota && dataRota.allowedTypes && dataRota.allowedTypes.length > 0) {
            // existe configuracao de tipos permitidos na rota
            /** @type {?} */
            const ehPermitidoPelaRota = dataRota.allowedTypes.some((/**
             * @param {?} type
             * @return {?}
             */
            type => type === typeDesejado));
            if (ehPermitidoPelaRota) {
                this.layoutService.setType(LayoutType[typeDesejado]);
            }
            else {
                console.error(`O parametro ${typeDesejado} não está definido como permitido, favor verificar!`);
                this.layoutService.setType(LayoutType[defaultTypeRoute]);
            }
        }
        else {
            // não existe configuração de tipos permitidos na rota devo então ver do componente layout (global da aplicação)
            /** @type {?} */
            const allowedTypesLayout = this.allowedTypes;
            /** @type {?} */
            const ehPermitidoGlobalmente = allowedTypesLayout.some((/**
             * @param {?} type
             * @return {?}
             */
            type => type === typeDesejado));
            if (ehPermitidoGlobalmente) {
                this.layoutService.setType(LayoutType[typeDesejado]);
            }
            else {
                console.error(`O parametro ${typeDesejado} não está definido como permitido, favor verificar!`);
                this.layoutService.setType(LayoutType[defaultTypeRoute]);
            }
        }
        if ((typeDesejado === 'normal' || defaultTypeRoute === 'normal') && this.document.fullscreenElement) {
            this.closeFullscreen();
        }
    }
    /**
     * @private
     * @return {?}
     */
    getData() {
        /** @type {?} */
        let route = this.router.routerState.root.snapshot;
        while (route.firstChild != null) {
            route = route.firstChild;
            if (route.routeConfig === null) {
                continue;
            }
            if (!route.routeConfig.path) {
                continue;
            }
            if (!route.data.defaultType) {
                continue;
            }
            return (/** @type {?} */ (route.data));
        }
    }
}
LayoutComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-layout',
                template: "<button #myClickFullscreen style=\"display: none;\"></button>\n<mat-sidenav-container\n  attr.data-environment=\"{{ environment }}\" [class.environment]=\"!showEnvironment\" *ngIf=\"(showShell | async)\"\n  [hasBackdrop]=\"(!(leftnav$ | async).pinned && (leftnav$ | async).opened) || (!(rightnav$ | async).pinned && (rightnav$ | async).opened)\"\n  (backdropClick)=\"navsClose();\"\n  [class.is-fixed]=\"(isFixed$ | async)\"\n  [class.is-mobileAction]=\"(isMobile$ | async)\"\n  [class.is-rightPinned]=\"(rightnav$ | async).pinned\" [class.is-leftPinned]=\"(leftnav$ | async).pinned\"\n  [class.is-favorite]=\"(possuiRecursoDeFavoritos) && (favorites$ | async).length > 0\">\n  <mat-sidenav class=\"uikit-nav\" #sidenav position=\"start\" disableClose=\"true\"\n               *ngIf=\"(hiddenShell | async)\"\n               [mode]=\"(leftnav$ | async).pinned ? 'side' : 'over'\" [opened]=\"(leftnav$ | async).opened\"\n               (keydown.escape)=\"leftNavClose()\" [fixedInViewport]=\"true\">\n    <uikit-nav>\n      <div nav-menu>\n        <ng-content select=\"[uikit-menu]\"></ng-content>\n        <ng-content select=\"[layout-nav-menu-extras]\"></ng-content>\n      </div>\n    </uikit-nav>\n  </mat-sidenav>\n\n  <mat-sidenav *ngIf=\"showIa\"\n               #aikitSideNav class=\"aikit-nav\" disableClose=\"true\" position=\"end\"\n               [opened]=\"(rightnav$ | async).opened\"\n               [mode]=\"(rightnav$ | async).pinned ? 'side' : 'over'\"\n               (keydown.escape)=\"rightNavClose()\" [fixedInViewport]=\"false\">\n\n    <uikit-ia></uikit-ia>\n\n  </mat-sidenav>\n  <mat-sidenav-content cdkScrollable [ngClass]=\"{\n  'noshell': !(hiddenShell | async),\n  'noshellnobreadcrumb': !(hiddenBreadcrumb | async)\n  }\">\n    <uikit-header [showNotifications]=\"showNotifications\"\n                  [showSystemInfo]=\"showSystemInfo\"\n                  [showUserInfo]=\"showUserInfo\" [showIa]=\"showIa\" [showShortcuts]=\"showShortcuts\">\n\n      <div header-notifications>\n        <ng-container *ngTemplateOutlet=\"notificacoes\"></ng-container>\n      </div>\n\n      <div header-systeminfo>\n        <ng-content select=\"[layout-header-systeminfo]\"></ng-content>\n\n        <mat-action-list *ngIf=\"(hasInstallOption$ | async) || (hasUpdate$ | async)\">\n          <!-- <mat-divider></mat-divider>-->\n          <a mat-list-item (click)=\"install()\" *ngIf=\"(hasInstallOption$ | async)\">Instalar sistema</a>\n          <a mat-list-item (click)=\"update()\" *ngIf=\"(hasUpdate$ | async)\">Atualizar vers\u00E3o</a>\n        </mat-action-list>\n      </div>\n\n      <div header-userinfo>\n\n        <ng-content select=\"[layout-header-userinfo]\"></ng-content>\n\n      </div>\n\n    </uikit-header>\n    <uikit-breadcrumb *ngIf=\"(hiddenBreadcrumb | async)\"></uikit-breadcrumb>\n\n    <div [hidden]=\"!(hasActions$ | async)\">\n      <button class=\"nav-mobile is-mobile\" mat-fab (click)=\"toggleMobile()\">\n        <mat-icon class=\"fas fa-ellipsis-h\" [class.fa-times]=\"(isMobile$ | async)\"></mat-icon>\n      </button>\n    </div>\n\n    <ng-container *ngTemplateOutlet=\"projecao\"></ng-container>\n  </mat-sidenav-content>\n</mat-sidenav-container>\n\n<ng-template *ngIf=\"(showShell | async)\">\n  <ng-container *ngTemplateOutlet=\"projecao\"></ng-container>\n</ng-template>\n\n<ng-template #projecao>\n  <div style=\"height: 100%\" [ngClass]=\"{'noshell' : !(showShell | async),\n                   'noshellnobreadcrumb': !(hiddenBreadcrumb | async)}\">\n    <ng-content></ng-content>\n  </div>\n</ng-template>\n\n<div hidden>\n  <ng-content select=\"[layout-nav-menu]\"></ng-content>\n</div>\n<div hidden>\n  <ng-content select=\"[layout-header-notifications]\"></ng-content>\n</div>\n\n<ng-template #notificacoes>\n  <ng-container *ngFor=\"let notificacao of (notificacoes$ | async)\">\n    <uikit-notificacao [nome]=\"notificacao.nome\" [icone]=\"notificacao.icone\" [descricao]=\"notificacao.descricao\"\n                       [data]=\"notificacao.data\" [read]=\"notificacao.read\"></uikit-notificacao>\n  </ng-container>\n</ng-template>\n\n",
                changeDetection: ChangeDetectionStrategy.OnPush,
                styles: [""]
            }] }
];
/** @nocollapse */
LayoutComponent.ctorParameters = () => [
    { type: NavQuery },
    { type: FavNavsQuery },
    { type: NavService },
    { type: ScrollDispatcher },
    { type: LayoutService },
    { type: Router },
    { type: ActivatedRoute },
    { type: UpdateInfoService },
    { type: UpdateService },
    { type: FavNavsService },
    { type: HotkeysService },
    { type: BreadcrumbService },
    { type: ToastService },
    { type: UikitRrippleService },
    { type: FormBuilder },
    { type: undefined, decorators: [{ type: Inject, args: [DOCUMENT,] }] },
    { type: PreviousRouteService }
];
LayoutComponent.propDecorators = {
    myClickFullscreen: [{ type: ViewChild, args: ['myClickFullscreen',] }],
    showNotifications: [{ type: Input }],
    showSystemInfo: [{ type: Input }],
    showUserInfo: [{ type: Input }],
    showEnvironment: [{ type: Input }],
    showIa: [{ type: Input }],
    environment: [{ type: Input }],
    allowedTypes: [{ type: Input }],
    defaultType: [{ type: Input }],
    showShortcuts: [{ type: Input }],
    notificacoesProjetadas: [{ type: ContentChildren, args: [NotificacaoComponent, { descendants: false },] }],
    sidenav: [{ type: ViewChild, args: [MatSidenav,] }]
};
if (false) {
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.hasActionsSubject;
    /** @type {?} */
    LayoutComponent.prototype.hasActions$;
    /** @type {?} */
    LayoutComponent.prototype.myClickFullscreen;
    /** @type {?} */
    LayoutComponent.prototype.showShell;
    /** @type {?} */
    LayoutComponent.prototype.hiddenBreadcrumb;
    /** @type {?} */
    LayoutComponent.prototype.showFullScrren;
    /** @type {?} */
    LayoutComponent.prototype.hiddenShell;
    /** @type {?} */
    LayoutComponent.prototype.showNotifications;
    /** @type {?} */
    LayoutComponent.prototype.showSystemInfo;
    /** @type {?} */
    LayoutComponent.prototype.showUserInfo;
    /** @type {?} */
    LayoutComponent.prototype.showEnvironment;
    /** @type {?} */
    LayoutComponent.prototype.showIa;
    /** @type {?} */
    LayoutComponent.prototype.environment;
    /** @type {?} */
    LayoutComponent.prototype.allowedTypes;
    /** @type {?} */
    LayoutComponent.prototype.defaultType;
    /** @type {?} */
    LayoutComponent.prototype.showShortcuts;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.subscription;
    /** @type {?} */
    LayoutComponent.prototype.isFixed$;
    /** @type {?} */
    LayoutComponent.prototype.isMobile$;
    /** @type {?} */
    LayoutComponent.prototype.rightnav$;
    /** @type {?} */
    LayoutComponent.prototype.rightnav;
    /** @type {?} */
    LayoutComponent.prototype.leftnav$;
    /** @type {?} */
    LayoutComponent.prototype.leftnav;
    /** @type {?} */
    LayoutComponent.prototype.data$;
    /** @type {?} */
    LayoutComponent.prototype.possuiRecursoDeFavoritos;
    /**
     * @type {?}
     * @private
     */
    LayoutComponent.prototype.scrollSubscription;
    /** @type {?} */
    LayoutComponent.prototype.notificacoesProjetadas;
    /** @type {?} */
    LayoutComponent.prototype.sidenav;
    /** @type {?} */
    LayoutComponent.prototype.notificacoes$;
    /**
     * @type {?}
     * @private
     */
    LayoutComponent.prototype.closeWhenNotPinnedLeftSubscription;
    /**
     * @type {?}
     * @private
     */
    LayoutComponent.prototype.closeWhenNotPinnedRightSubscription;
    /** @type {?} */
    LayoutComponent.prototype.hasInstallOption$;
    /** @type {?} */
    LayoutComponent.prototype.hasUpdate$;
    /** @type {?} */
    LayoutComponent.prototype.favorites$;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.navQuery;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.favNavsQuery;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.navService;
    /** @type {?} */
    LayoutComponent.prototype.scroll;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.layoutService;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.router;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.activatedRoute;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.updateInfoService;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.updateService;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.favNavsService;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.hotkeysService;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.breadcrumbService;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.toastService;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.uikitRrippleService;
    /**
     * @type {?}
     * @private
     */
    LayoutComponent.prototype.fb;
    /**
     * @type {?}
     * @private
     */
    LayoutComponent.prototype.document;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.previousRouteService;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class HeaderComponent {
    /**
     * @param {?} navQuery
     * @param {?} navService
     * @param {?} layoutService
     * @param {?} renderer
     */
    constructor(navQuery, navService, layoutService, renderer) {
        this.navQuery = navQuery;
        this.navService = navService;
        this.layoutService = layoutService;
        this.renderer = renderer;
        this.showNotifications = false;
        this.showUserInfo = false;
        this.showSystemInfo = false;
        this.showIa = false;
        this.showShortcuts = true;
        this.leftNav$ = this.navQuery.leftnav$;
        this.rightNav$ = this.navQuery.rightnav$;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @return {?}
     */
    toggle() {
        this.navService.toggleLeftNav();
    }
    /**
     * @return {?}
     */
    rightNavToggle() {
        this.navService.toggleRightNav();
    }
}
HeaderComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-header',
                template: "<mat-toolbar>\n\n  <button tabindex=\"1\" mat-flat-button color=\"primary\" type=\"button\" aria-label=\"Abri menu de navega\u00E7\u00E3o\"\n    aria-controls=\"navigation\" matTooltip=\"Abrir menu de navega\u00E7\u00E3o\" (click)=\"toggle()\"\n    class=\"btn-hamburger hamburger--slider\" [class.is-active]=\"(leftNav$ | async).opened\">\n    <span class=\"hamburger-box\">\n      <span class=\"hamburger-inner\"></span>\n    </span>\n  </button>\n\n  <span class=\"logotipo\">\n    <a href=\"#\" tabindex=\"1\">\n      <div class=\"logomobile is-mobile\"></div>\n      <div class=\"logodesk is-desktop\"></div>\n    </a>\n  </span>\n\n  <span class=\"header-nav\">\n    <uikit-notification *ngIf=\"showNotifications == true\">\n      <div notifications>\n        <ng-content select=\"[header-notifications]\"></ng-content>\n      </div>\n    </uikit-notification>\n    <uikit-systeminfo *ngIf=\"showSystemInfo == true\">\n      <div systeminfo>\n        <ng-content select=\"[header-systeminfo]\"></ng-content>\n      </div>\n    </uikit-systeminfo>\n    <uikit-accessibility [showIa]=\"showIa\" [showShortcuts]=\"showShortcuts\"></uikit-accessibility>\n    <uikit-userinfo *ngIf=\"showUserInfo == true\">\n      <div userinfo>\n\n        <ng-content select=\"[header-userinfo]\"></ng-content>\n\n      </div>\n    </uikit-userinfo>\n\n    <button *ngIf=\"showIa\" class=\"aikit-button\" mat-flat-button color=\"primary\" type=\"button\"\n      aria-label=\"Abri menu de navega\u00E7\u00E3o\" aria-controls=\"navigation\" (click)=\"rightNavToggle()\"\n      matTooltip=\"Abri menu de navega\u00E7\u00E3o\" matTooltip=\"Notifica\u00E7\u00F5es\" aria-label=\"Notifica\u00E7\u00F5es\" matBadge=\"2\"\n      matBadgeColor=\"accent\" matBadgeSize=\"small\" tabindex=\"7\">\n      <img src=\"../../assets/images/iakit-logotipo-mobile.svg\" />\n    </button>\n  </span>\n</mat-toolbar>",
                changeDetection: ChangeDetectionStrategy.OnPush,
                styles: [""]
            }] }
];
/** @nocollapse */
HeaderComponent.ctorParameters = () => [
    { type: NavQuery },
    { type: NavService },
    { type: LayoutService },
    { type: Renderer2 }
];
HeaderComponent.propDecorators = {
    showNotifications: [{ type: Input }],
    showUserInfo: [{ type: Input }],
    showSystemInfo: [{ type: Input }],
    showIa: [{ type: Input }],
    matToolbar: [{ type: ViewChild, args: [MatToolbar,] }],
    showShortcuts: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    HeaderComponent.prototype.showNotifications;
    /** @type {?} */
    HeaderComponent.prototype.showUserInfo;
    /** @type {?} */
    HeaderComponent.prototype.showSystemInfo;
    /** @type {?} */
    HeaderComponent.prototype.showIa;
    /** @type {?} */
    HeaderComponent.prototype.matToolbar;
    /** @type {?} */
    HeaderComponent.prototype.showShortcuts;
    /** @type {?} */
    HeaderComponent.prototype.leftNav$;
    /** @type {?} */
    HeaderComponent.prototype.rightNav$;
    /**
     * @type {?}
     * @protected
     */
    HeaderComponent.prototype.navQuery;
    /**
     * @type {?}
     * @protected
     */
    HeaderComponent.prototype.navService;
    /**
     * @type {?}
     * @protected
     */
    HeaderComponent.prototype.layoutService;
    /**
     * @type {?}
     * @protected
     */
    HeaderComponent.prototype.renderer;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NavComponent {
    /**
     * @param {?} navService
     * @param {?} navQuery
     */
    constructor(navService, navQuery) {
        this.navService = navService;
        this.navQuery = navQuery;
        this.leftNav$ = this.navQuery.leftnav$;
        this.hasFavoritosService = this.navQuery.hasFavoritosService;
    }
    /**
     * @return {?}
     */
    ngOnInit() { }
    /**
     * @return {?}
     */
    togglePinLeftNav() {
        this.navService.togglePinLeftNav();
    }
}
NavComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-nav',
                template: "<button tabindex=\"-1\" mat-icon-button color=\"primary\" class=\"btn-attach\" (click)=\"togglePinLeftNav()\"\n        [class.is-active]=\"(leftNav$ | async).opened\" [class.is-pinned]=\"(leftNav$ | async).pinned\">\n  <mat-icon class=\"fas fa-thumbtack\" aria-label=\"Fixar menu na tela\"></mat-icon>\n</button>\n\n<uikit-menu>\n  <div menu>\n\n      <ng-content select=\"[nav-menu]\"></ng-content>\n\n  </div>\n</uikit-menu>\n<uikit-favnav *ngIf=\"hasFavoritosService\"></uikit-favnav>\n",
                changeDetection: ChangeDetectionStrategy.OnPush,
                styles: [""]
            }] }
];
/** @nocollapse */
NavComponent.ctorParameters = () => [
    { type: NavService },
    { type: NavQuery }
];
if (false) {
    /** @type {?} */
    NavComponent.prototype.leftNav$;
    /** @type {?} */
    NavComponent.prototype.hasFavoritosService;
    /**
     * @type {?}
     * @protected
     */
    NavComponent.prototype.navService;
    /**
     * @type {?}
     * @protected
     */
    NavComponent.prototype.navQuery;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class MenuSearchService {
    /**
     * @param {?} layoutService
     */
    constructor(layoutService) {
        this.layoutService = layoutService;
        this.options = {
            keys: ['title', 'tags',
                'children.title', 'children.tags',
                'children.children.title', 'children.children.title.tags',
                'children.children.children.title', 'children.children.children.title.tags']
        };
        this.menuItemsSubject$ = new BehaviorSubject([]);
        this.menuItems$ = this.menuItemsSubject$.asObservable();
    }
    /**
     * @param {?} value
     * @return {?}
     */
    buscar(value) {
        if (!value || value === '') {
            this.layoutService.menuItems$.subscribe((/**
             * @param {?} itens
             * @return {?}
             */
            (itens) => this.menuItemsSubject$.next(itens)));
        }
        else if (this.menuItems$) {
            /** @type {?} */
            let menuItens = [];
            this.menuItems$.pipe(map((/**
             * @param {?} listMenuItem
             * @return {?}
             */
            listMenuItem => this.applyFilter(listMenuItem, value, this.options))))
                .subscribe((/**
             * @param {?} itens
             * @return {?}
             */
            (itens) => menuItens = this.groupReturnFilter(itens, value)));
            this.menuItemsSubject$.next(menuItens);
        }
        else {
            this.layoutService.menuItems$.pipe(map((/**
             * @param {?} listMenuItem
             * @return {?}
             */
            listMenuItem => this.applyFilter(listMenuItem, value, this.options))))
                .subscribe((/**
             * @param {?} itens
             * @return {?}
             */
            (itens) => this.menuItemsSubject$.next(this.groupReturnFilter(itens, value))));
        }
    }
    /**
     * @private
     * @param {?} listMenuItem
     * @param {?} value
     * @param {?} options
     * @return {?}
     */
    applyFilter(listMenuItem, value, options) {
        /** @type {?} */
        const fuse = new Fuse(listMenuItem, options);
        return fuse.search(value);
    }
    /**
     * @private
     * @param {?} itens
     * @param {?} value
     * @return {?}
     */
    groupReturnFilter(itens, value) {
        /** @type {?} */
        const menuItens = [];
        /** @type {?} */
        const functionCreateItem = (/**
         * @param {?} menuItem
         * @return {?}
         */
        (menuItem) => {
            return (/** @type {?} */ ({
                title: menuItem.title,
                icon: menuItem.icon,
                link: menuItem.link,
                tags: menuItem.tags,
                nodeId: menuItem.nodeId,
            }));
        });
        /** @type {?} */
        const functionMapItem = (/**
         * @param {?} menuItem
         * @return {?}
         */
        (menuItem) => {
            if (menuItem.children && menuItem.children.length > 0) {
                this.applyFilter(menuItem.children, value, this.options).map((/**
                 * @param {?} itemIt
                 * @return {?}
                 */
                itemIt => functionMapItem(itemIt)));
            }
            else {
                menuItens.push(functionCreateItem(menuItem));
            }
        });
        itens.map((/**
         * @param {?} menuItem
         * @return {?}
         */
        (menuItem) => functionMapItem(menuItem)));
        return menuItens;
    }
}
MenuSearchService.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */
MenuSearchService.ctorParameters = () => [
    { type: LayoutService }
];
/** @nocollapse */ MenuSearchService.ngInjectableDef = defineInjectable({ factory: function MenuSearchService_Factory() { return new MenuSearchService(inject(LayoutService)); }, token: MenuSearchService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    MenuSearchService.prototype.options;
    /**
     * @type {?}
     * @private
     */
    MenuSearchService.prototype.menuItemsSubject$;
    /** @type {?} */
    MenuSearchService.prototype.menuItems$;
    /**
     * @type {?}
     * @protected
     */
    MenuSearchService.prototype.layoutService;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class MenuItemComponent {
    /**
     * @param {?} router
     * @param {?} favNavsService
     * @param {?} favNavsQuery
     * @param {?} menuSearchService
     */
    constructor(router, favNavsService, favNavsQuery, menuSearchService) {
        this.router = router;
        this.favNavsService = favNavsService;
        this.favNavsQuery = favNavsQuery;
        this.menuSearchService = menuSearchService;
        this.hasItems$ = new BehaviorSubject(false);
        this.isFavorited = false;
        this.treeControlMobile = new NestedTreeControl((/**
         * @param {?} node
         * @return {?}
         */
        node => node.children));
        this.possuiRecursoDeFavoritos = this.favNavsService.possuiRecursoDeFavoritos;
        this.treeControl = new NestedTreeControl((/**
         * @param {?} node
         * @return {?}
         */
        node => node.children));
        this.favorites$ = this.favNavsQuery.favorites$;
        this.menuItems$ = this.menuSearchService.menuItems$;
        this.hasChild = (/**
         * @param {?} _
         * @param {?} node
         * @return {?}
         */
        (_, node) => !!node.children && node.children.length > 0);
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        /** @type {?} */
        const menuItem = {
            id: this.id,
            title: this.title,
            link: this.link,
            icon: this.icon,
            tags: this.tags
        };
        this.menuItem = menuItem;
        this.isFavorited$ = this.favNavsQuery.isFavorited(menuItem);
        this.isFavorited$.subscribe((/**
         * @param {?} r
         * @return {?}
         */
        r => this.isFavorited = r));
        this.favoritesMobile$ = this.favorites$.pipe(map((/**
         * @param {?} itens
         * @return {?}
         */
        (itens) => {
            /** @type {?} */
            const favoritesMobile = [(/** @type {?} */ ({ title: 'Meus Favoritos', icon: 'fas fa-star', children: itens }))];
            return favoritesMobile;
        })));
    }
    /**
     * @protected
     * @return {?}
     */
    updateHasItems() {
        this.hasItems$.next(this.children.length - 1 > 0);
    }
    /**
     * @return {?}
     */
    ngAfterContentInit() {
        this.updateHasItems();
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.childrenSubscription && this.childrenSubscription.closed === false) {
            this.childrenSubscription.unsubscribe();
        }
        if (this.isFavoritedSubscription && this.isFavoritedSubscription.closed === false) {
            this.isFavoritedSubscription.unsubscribe();
        }
    }
    /**
     * @param {?} menu
     * @return {?}
     */
    getIsFavorited(menu) {
        /** @type {?} */
        const item = { link: menu.link, title: menu.title, icon: menu.icon };
        return this.favNavsQuery.getIsFavorited((/** @type {?} */ (item)));
    }
    /**
     * @param {?} menu
     * @return {?}
     */
    toggleFavorito(menu) {
        /** @type {?} */
        const item = { link: menu.link, title: menu.title, icon: menu.icon };
        this.favNavsService.toggleItem((/** @type {?} */ (item)));
    }
}
MenuItemComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-menu-item',
                template: "<mat-tree [dataSource]=\"favoritesMobile$ | async\" [treeControl]=\"treeControlMobile\"\n          class=\"menu-tree is-mobile\"\n          *ngIf=\"(favorites$ | async).length > 0\">\n\n  <mat-nested-tree-node *matTreeNodeDef=\"let node; when: hasChild\">\n    <button mat-button matTreeNodeToggle\n            [attr.aria-label]=\"'toggle ' + node.title\">\n      <mat-icon class=\"{{ node.icon }}\"></mat-icon>\n      {{ node.title }}\n\n      <mat-icon\n        class=\"{{ treeControlMobile.isExpanded(node) ? 'fas fa-caret-up' : 'fas fa-caret-down' }}\"></mat-icon>\n    </button>\n\n    <ul class=\"menu-tree-collapse\" [class.menu-tree-hidden]=\"!treeControlMobile.isExpanded(node)\">\n      <ng-container matTreeNodeOutlet></ng-container>\n    </ul>\n  </mat-nested-tree-node>\n\n  <mat-tree-node *matTreeNodeDef=\"let node\" matTreeNodeToggle>\n    <uikit-highlight [item]=\"node\">\n      <a role=\"option\" [routerLinkActive]=\"'active'\" [routerLink]=\"node.link ? node.link : ''\"\n         [routerLinkActiveOptions]=\"{exact:true}\" mat-button matTooltip=\"Ir para '{{ node.title }}'\"\n         attr.aria-label=\"Ir para {{ node.title }}\">\n        <mat-icon class=\"{{ node.icon }}\"></mat-icon>\n        {{ node.title }}\n      </a>\n    </uikit-highlight>\n  </mat-tree-node>\n</mat-tree>\n\n<mat-tree [dataSource]=\"menuItems$ | async\" [treeControl]=\"treeControl\" class=\"menu-tree\">\n\n  <!-- This is the tree node template for leaf nodes -->\n  <mat-tree-node *matTreeNodeDef=\"let node\" matTreeNodeToggle>\n    <uikit-highlight [item]=\"node\">\n      <!-- use a disabled button to provide padding for tree leaf -->\n      <a role=\"option\" [routerLinkActive]=\"'active'\" [routerLink]=\"node.link ? node.link : ''\"\n         [routerLinkActiveOptions]=\"{exact:true}\" mat-button matTooltip=\"Ir para '{{ node.title }}'\"\n         attr.aria-label=\"Ir para {{ node.title }}\">\n        <mat-icon class=\"{{ node.icon }}\"></mat-icon>\n        {{ node.title }}\n      </a>\n      <button mat-icon-button *ngIf=\"possuiRecursoDeFavoritos && node.link\">\n        <mat-icon class=\"fa-lg fas fa-star\"\n                  [matTooltip]=\"!getIsFavorited(node) ? 'Adicionar \\'' + node.title + '\\' aos favoritos' : 'Remover \\'' + node.title + '\\' dos favoritos'\"\n                  (click)=\"toggleFavorito(node)\" [color]=\"getIsFavorited(node) ? 'accent' : ''\">\n        </mat-icon>\n      </button>\n    </uikit-highlight>\n  </mat-tree-node>\n  <!-- This is the tree node template for expandable nodes -->\n  <mat-nested-tree-node *matTreeNodeDef=\"let node; when: hasChild\">\n    <li>\n      <button mat-button matTreeNodeToggle\n              [attr.aria-label]=\"'toggle ' + node.title\">\n        <mat-icon class=\"{{ node.icon }}\"></mat-icon>\n        {{ node.title }}\n        <mat-icon class=\"{{ treeControl.isExpanded(node) ? 'fas fa-caret-up' : 'fas fa-caret-down' }}\"></mat-icon>\n      </button>\n      <ul class=\"menu-tree-collapse\" [class.menu-tree-hidden]=\"!treeControl.isExpanded(node)\">\n        <ng-container matTreeNodeOutlet></ng-container>\n      </ul>\n    </li>\n  </mat-nested-tree-node>\n</mat-tree>\n"
            }] }
];
/** @nocollapse */
MenuItemComponent.ctorParameters = () => [
    { type: Router },
    { type: FavNavsService },
    { type: FavNavsQuery },
    { type: MenuSearchService }
];
MenuItemComponent.propDecorators = {
    id: [{ type: Input }],
    icon: [{ type: Input }],
    link: [{ type: Input }],
    title: [{ type: Input }],
    tags: [{ type: Input }],
    children: [{ type: ContentChildren, args: [MenuItemComponent, { descendants: false },] }]
};
if (false) {
    /** @type {?} */
    MenuItemComponent.prototype.id;
    /** @type {?} */
    MenuItemComponent.prototype.icon;
    /** @type {?} */
    MenuItemComponent.prototype.link;
    /** @type {?} */
    MenuItemComponent.prototype.title;
    /** @type {?} */
    MenuItemComponent.prototype.tags;
    /** @type {?} */
    MenuItemComponent.prototype.children;
    /** @type {?} */
    MenuItemComponent.prototype.hasItems$;
    /**
     * @type {?}
     * @protected
     */
    MenuItemComponent.prototype.childrenSubscription;
    /**
     * @type {?}
     * @protected
     */
    MenuItemComponent.prototype.isFavoritedSubscription;
    /** @type {?} */
    MenuItemComponent.prototype.isFavorited$;
    /** @type {?} */
    MenuItemComponent.prototype.isFavorited;
    /** @type {?} */
    MenuItemComponent.prototype.treeControlMobile;
    /** @type {?} */
    MenuItemComponent.prototype.possuiRecursoDeFavoritos;
    /** @type {?} */
    MenuItemComponent.prototype.treeControl;
    /** @type {?} */
    MenuItemComponent.prototype.favorites$;
    /** @type {?} */
    MenuItemComponent.prototype.favoritesMobile$;
    /** @type {?} */
    MenuItemComponent.prototype.menuItems$;
    /**
     * @type {?}
     * @protected
     */
    MenuItemComponent.prototype.menuItem;
    /** @type {?} */
    MenuItemComponent.prototype.hasChild;
    /**
     * @type {?}
     * @private
     */
    MenuItemComponent.prototype.router;
    /**
     * @type {?}
     * @private
     */
    MenuItemComponent.prototype.favNavsService;
    /**
     * @type {?}
     * @private
     */
    MenuItemComponent.prototype.favNavsQuery;
    /**
     * @type {?}
     * @protected
     */
    MenuItemComponent.prototype.menuSearchService;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class MenuComponent {
    /**
     * @param {?} favNavsQuery
     * @param {?} layoutService
     */
    constructor(favNavsQuery, layoutService) {
        this.favNavsQuery = favNavsQuery;
        this.layoutService = layoutService;
        this.favorites$ = this.favNavsQuery.favorites$;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.favoritesMobile$ = this.favorites$.pipe(map((/**
         * @param {?} itens
         * @return {?}
         */
        (itens) => {
            /** @type {?} */
            const favoritesMobile = [(/** @type {?} */ ({ title: 'Meus Favoritos', icon: 'fas fa-star', children: itens }))];
            return favoritesMobile;
        })));
    }
    /**
     * @return {?}
     */
    ngAfterContentInit() {
        /** @type {?} */
        const functionMap = (/**
         * @param {?} r
         * @return {?}
         */
        (r) => {
            /** @type {?} */
            const menuItem = (/** @type {?} */ ({
                id: r.id,
                title: r.title,
                link: r.link,
                icon: r.icon,
                tags: r.tags
            }));
            if (r.children.length > 1) {
                if (!r.id) {
                    r.id = guid();
                }
                menuItem.nodeId = r.id;
                menuItem.children = r.children.filter((/**
                 * @param {?} b
                 * @return {?}
                 */
                b => b !== r)).map(functionMap);
            }
            return menuItem;
        });
        /** @type {?} */
        const menuItems = this.children.toArray().map(functionMap);
        if (this.itensRecursivo) {
            this.itensRecursivo.subscribe((/**
             * @param {?} c
             * @return {?}
             */
            (c) => menuItems.push(...c)));
        }
        if (menuItems && menuItems.length > 0) {
            this.layoutService.setMenuItems(menuItems);
        }
    }
}
MenuComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-menu',
                template: "<mat-list tabindex=\"-1\" role=\"list\" (click)=\"$event.stopPropagation()\">\n  <uikit-menu-search></uikit-menu-search>\n  <ng-content select=\"[menu]\"></ng-content>\n</mat-list>\n",
                changeDetection: ChangeDetectionStrategy.OnPush,
                viewProviders: [MatExpansionPanel],
                styles: [""]
            }] }
];
/** @nocollapse */
MenuComponent.ctorParameters = () => [
    { type: FavNavsQuery },
    { type: LayoutService }
];
MenuComponent.propDecorators = {
    itensRecursivo: [{ type: Input }],
    children: [{ type: ContentChildren, args: [MenuItemComponent, { descendants: false },] }]
};
if (false) {
    /** @type {?} */
    MenuComponent.prototype.itensRecursivo;
    /**
     * @type {?}
     * @private
     */
    MenuComponent.prototype.children;
    /** @type {?} */
    MenuComponent.prototype.favorites$;
    /** @type {?} */
    MenuComponent.prototype.favoritesMobile$;
    /**
     * @type {?}
     * @protected
     */
    MenuComponent.prototype.favNavsQuery;
    /**
     * @type {?}
     * @protected
     */
    MenuComponent.prototype.layoutService;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class FavnavComponent {
    // favorites = [
    //   'fas fa-info-circle',
    //   'fas fa-file-contract',
    //   'fas fa-hard-hat',
    //   'fas fa-plug',
    //   'fas fa-moon',
    //   'fas fa-sms',
    //   'fas fa-comments',
    //   'fas fa-ethernet'
    // ];
    /**
     * @param {?} navQuery
     * @param {?} favNavsQuery
     * @param {?} favNavsService
     */
    constructor(navQuery, favNavsQuery, favNavsService) {
        this.navQuery = navQuery;
        this.favNavsQuery = favNavsQuery;
        this.favNavsService = favNavsService;
        this.leftNav$ = this.navQuery.leftnav$;
        this.favorites$ = this.favNavsQuery.favorites$;
        this.favorites = [
        // { title: 'aaa', icon: 'fas fa-info-circle' },
        // { title: 'bbb', icon: 'fas fa-info-circle' },
        // { title: 'ccc', icon: 'fas fa-info-circle' },
        // { title: 'ddd', icon: 'fas fa-info-circle' },
        ];
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.favoritesSubscription = this.favorites$.subscribe((/**
         * @param {?} favs
         * @return {?}
         */
        (favs) => {
            /** @type {?} */
            const newArrary = favs.slice();
            this.favorites.length = 0;
            this.favorites.push(...newArrary);
        }));
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.favoritesSubscription && !this.favoritesSubscription.closed) {
            this.favoritesSubscription.unsubscribe();
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    drop(event) {
        this.favNavsService.moverItens(event.previousIndex, event.currentIndex);
    }
}
FavnavComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-favnav',
                template: "<!-- favnav -->\n<div class=\"favnav\" [class.is-closed]=\"(favorites$ | async).length === 0\" [class.is-active]=\"(leftNav$ | async).opened\">\n  <div cdkDropList (cdkDropListDropped)=\"drop($event)\">\n    <a [routerLinkActive]=\"'active'\" [routerLink]=\"favorite.link\" [routerLinkActiveOptions]=\"{exact:true}\" mat-icon-button\n      *ngFor=\"let favorite of (favorites$ | async)\" cdkDrag matTooltip=\"Ir para '{{ favorite.title }}'\"\n      attr.aria-label=\"Ir para '{{ favorite.title }}'\" [matTooltipPosition]=\"'right'\">\n\n      <div class=\"favnav-placeholder\" *cdkDragPlaceholder></div>\n      <mat-icon class=\"fa-lg {{ favorite.icon }}\"></mat-icon>\n    </a>\n  </div>\n</div>\n",
                changeDetection: ChangeDetectionStrategy.OnPush,
                styles: [""]
            }] }
];
/** @nocollapse */
FavnavComponent.ctorParameters = () => [
    { type: NavQuery },
    { type: FavNavsQuery },
    { type: FavNavsService }
];
if (false) {
    /** @type {?} */
    FavnavComponent.prototype.leftNav$;
    /** @type {?} */
    FavnavComponent.prototype.favorites$;
    /** @type {?} */
    FavnavComponent.prototype.favorites;
    /** @type {?} */
    FavnavComponent.prototype.favoritesSubscription;
    /**
     * @type {?}
     * @protected
     */
    FavnavComponent.prototype.navQuery;
    /**
     * @type {?}
     * @protected
     */
    FavnavComponent.prototype.favNavsQuery;
    /**
     * @type {?}
     * @protected
     */
    FavnavComponent.prototype.favNavsService;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BreadcrumbComponent {
    /**
     * @param {?} layoutService
     * @param {?} applicationRef
     * @param {?} router
     * @param {?} activatedRoute
     * @param {?} favNavsService
     * @param {?} favNavsQuery
     * @param {?} renderer
     * @param {?} zone
     * @param {?} changeDetector
     * @param {?} breadcrumbService
     * @param {?} angularLocation
     * @param {?} platformLocation
     * @param {?} scrollDispatcher
     * @param {?} previousRouteService
     */
    constructor(layoutService, applicationRef, router, activatedRoute, favNavsService, favNavsQuery, renderer, zone, changeDetector, breadcrumbService, angularLocation, platformLocation, scrollDispatcher, previousRouteService) {
        this.layoutService = layoutService;
        this.applicationRef = applicationRef;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.favNavsService = favNavsService;
        this.favNavsQuery = favNavsQuery;
        this.renderer = renderer;
        this.zone = zone;
        this.changeDetector = changeDetector;
        this.breadcrumbService = breadcrumbService;
        this.angularLocation = angularLocation;
        this.platformLocation = platformLocation;
        this.scrollDispatcher = scrollDispatcher;
        this.previousRouteService = previousRouteService;
        this.subscription = new Subscription();
        this.menuItem$ = new BehaviorSubject(null);
        this.menuItemSubscription$ = Subscription.EMPTY;
        this.isFavorited = false;
        this.isMobile$ = this.layoutService.isMobile$;
        this.navigationEndSubscription = Subscription.EMPTY;
        this.data$ = new BehaviorSubject(null);
        this.hasActionsSubject = new BehaviorSubject(false);
        this.hasActions$ = this.hasActionsSubject.asObservable();
        this.possuiRecursoDeFavoritos = this.favNavsService.possuiRecursoDeFavoritos;
        this.hiddenShell$ = this.layoutService.hiddenShell$;
        this.isFixed$ = this.layoutService.isFixed$.pipe(distinctUntilChanged());
        this.subscription.add(this.scrollDispatcher.scrolled().pipe(debounceTime(75)).subscribe((/**
         * @param {?} x
         * @return {?}
         */
        x => {
            setTimeout((/**
             * @return {?}
             */
            () => this.applicationRef.tick()));
        })));
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.desabilitarBotaoVoltar$ = this.previousRouteService.desabilitarBotaoVoltar;
        // this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe(nav => {
        //   const params = this.activatedRoute.snapshot.params;
        //   console.log('params', params);
        // });
        // this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe(nav => {
        // // this.activatedRoute.params.subscribe(params => {
        //   const params = this.activatedRoute.snapshot.params;
        //   const data = this.activatedRoute.root.firstChild.snapshot.data;
        //   if (data && data.breadcrumb) {
        //     let title: string = data.breadcrumb;
        //     const parametros = Object.keys(params);
        //     parametros.forEach(parametro => {
        //       title = title.replace(`:${parametro}`, params[parametro]);
        //     });
        //     this.breadcrumbService.changeBreadcrumb(this.activatedRoute.snapshot, data.breadcrumb);
        //   }
        // });
        //
        // this.breadcrumbService.breadcrumbChanged.subscribe(crumbs => {
        //   this.breadcrumbs = crumbs.map(crumb => ({ label: crumb.displayName, url: `#${crumb.url}` }));
        // });
        //
        this.activatedRoute.paramMap.pipe(map((/**
         * @return {?}
         */
        () => {
            /** @type {?} */
            let child = this.activatedRoute.firstChild;
            while (child) {
                if (child.firstChild) {
                    child = child.firstChild;
                }
                else if (child.snapshot.data) {
                    return child.snapshot.data;
                }
                else {
                    return null;
                }
            }
            return null;
        })))
            .subscribe((/**
         * @param {?} customData
         * @return {?}
         */
        customData => {
            /** @type {?} */
            let route = this.router.routerState.root.snapshot;
            while (route.firstChild != null) {
                route = route.firstChild;
            }
            /** @type {?} */
            const data = route.data;
            /** @type {?} */
            const favItem = this.getFavItem(data);
            // favItem.icon = customData ? customData.icon : null;
            this.data$.next(favItem);
            if (favItem) {
                this.isFavorited$ = this.favNavsQuery.isFavorited((/** @type {?} */ (favItem)));
                this.isFavorited$.subscribe((/**
                 * @param {?} r
                 * @return {?}
                 */
                r => this.isFavorited = r));
            }
        }));
        this.navigationEndSubscription = this.router.events.pipe(filter((/**
         * @param {?} event
         * @return {?}
         */
        event => event instanceof NavigationEnd)), map((/**
         * @return {?}
         */
        () => {
            /** @type {?} */
            let child = this.activatedRoute.firstChild;
            while (child) {
                if (child.firstChild) {
                    child = child.firstChild;
                }
                else if (child.snapshot.data) {
                    return child.snapshot.data;
                }
                else {
                    return null;
                }
            }
            return null;
        })))
            .subscribe((/**
         * @param {?} customData
         * @return {?}
         */
        customData => {
            /** @type {?} */
            let route = this.router.routerState.root.snapshot;
            while (route.firstChild != null) {
                route = route.firstChild;
            }
            /** @type {?} */
            const data = route.data;
            /** @type {?} */
            const favItem = this.getFavItem(data);
            // favItem.icon = customData ? customData.icon : null;
            this.data$.next(favItem);
            if (favItem) {
                this.isFavorited$ = this.favNavsQuery.isFavorited((/** @type {?} */ (favItem)));
                this.isFavorited$.subscribe((/**
                 * @param {?} r
                 * @return {?}
                 */
                r => this.isFavorited = r));
            }
        }));
        this.data$.subscribe((/**
         * @param {?} data
         * @return {?}
         */
        data => {
            this.menuItemSubscription$ = this.layoutService.menuItemsFlattened$.pipe(flatMap((/**
             * @param {?} r
             * @return {?}
             */
            r => r)), filter((/**
             * @param {?} r
             * @return {?}
             */
            r => {
                if (data == null || data.id == null) {
                    return false;
                }
                else {
                    /** @type {?} */
                    const retorno = r.id === data.id;
                    return retorno;
                }
            }))).subscribe((/**
             * @param {?} item
             * @return {?}
             */
            item => {
                this.menuItem$.next(item);
            }));
        }));
    }
    /**
     * @private
     * @param {?} customData
     * @return {?}
     */
    getFavItem(customData) {
        /** @type {?} */
        const data = customData;
        /** @type {?} */
        const lastBreadCrumb = this.breadcrumbService.breadcrumbs[this.breadcrumbService.breadcrumbs.length - 1];
        if (lastBreadCrumb != null) {
            /** @type {?} */
            const title = lastBreadCrumb.title;
            // this.breadcrumbService.getTitleFormatted(data.breadcrumb, this.activatedRoute.snapshot);
            /** @type {?} */
            const favItem = { link: this.router.url, title, icon: data ? data.icon : null };
            return favItem;
        }
    }
    /**
     * @return {?}
     */
    updateIfHasActions() {
        /** @type {?} */
        const elements = document.getElementsByClassName('uikit-actions');
        /** @type {?} */
        const element = elements.item(0);
        if (element != null) {
            /** @type {?} */
            const hasChildNodes = element.hasChildNodes();
            this.hasActionsSubject.next(hasChildNodes);
        }
        else {
            this.hasActionsSubject.next(false);
        }
    }
    /**
     * @return {?}
     */
    toggleFavoritos() {
        /** @type {?} */
        const customData = this.data$.getValue();
        /** @type {?} */
        const favItem = this.getFavItem(customData);
        this.favNavsService.toggleItem((/** @type {?} */ (favItem)));
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.navigationEndSubscription && this.navigationEndSubscription.closed == false) {
            this.navigationEndSubscription.unsubscribe();
        }
        if (this.menuItemSubscription$ && this.menuItemSubscription$.closed == false) {
            this.menuItemSubscription$.unsubscribe();
        }
        if (this.subscription && this.subscription.closed == false) {
            this.subscription.unsubscribe();
        }
    }
    /**
     * @return {?}
     */
    goBack() {
        //    if (document.referrer.indexOf(location.host) !== -1) {
        //history.go(-1);
        this.previousRouteService.removerRotaAcessada();
        this.angularLocation.back();
        //  } else {
        //      this.router.navigate([`../`], { relativeTo: this.activatedRoute });
        //}
    }
}
BreadcrumbComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-breadcrumb',
                template: "<div class=\"breadcrumb\"\n     [ngClass]=\"{'noshell': !(hiddenShell$ | async)}\"\n     *ngIf=\"(data$ | async) != null && (data$ | async).title != null\">\n\n  <button [disabled]=\"desabilitarBotaoVoltar$ | async\" type=\"button\" mat-icon-button color=\"\"  (click)=\"goBack()\">\n    <mat-icon class=\"fas fa-chevron-left\"></mat-icon>\n  </button>\n\n  <button tabindex=\"0\" *ngIf=\"possuiRecursoDeFavoritos; else naoPossuiRecursoDeFavoritos\" mat-icon-button\n    [class.btn-favorite]=\"possuiRecursoDeFavoritos\" [class.active]=\"isFavorited$ | async\"\n    (click)=\"toggleFavoritos()\">\n    <mat-icon class=\"fa-lg {{ (data$ | async)?.icon }}\"></mat-icon>\n  </button>\n\n  <ng-template #naoPossuiRecursoDeFavoritos>\n    <button tabindex=\"-1\"  mat-icon-button class=\"btn-favorite\" [class.btn-favorite]=\"possuiRecursoDeFavoritos\"\n      [class.active]=\"isFavorited$ | async\">\n      <mat-icon class=\"fa-lg {{ (data$ | async)?.icon }}\"></mat-icon>\n    </button>\n  </ng-template>\n\n  <breadcrumb #parent>\n    <ng-container\n     *ngFor=\"let route of (parent.breadcrumbs$ | async)\">\n      <a mat-button *ngIf=\"!route.terminal\" tabindex=\"1\" href=\"\" [routerLink]=\"[route.link]\">{{ route.title }}</a>\n      <a mat-button *ngIf=\"route.terminal\">{{ route.title }}</a>\n    </ng-container>\n  </breadcrumb>\n\n</div>\n",
                changeDetection: ChangeDetectionStrategy.Default,
                styles: [""]
            }] }
];
/** @nocollapse */
BreadcrumbComponent.ctorParameters = () => [
    { type: LayoutService },
    { type: ApplicationRef },
    { type: Router },
    { type: ActivatedRoute },
    { type: FavNavsService },
    { type: FavNavsQuery },
    { type: Renderer2 },
    { type: NgZone },
    { type: ChangeDetectorRef },
    { type: BreadcrumbService },
    { type: Location },
    { type: PlatformLocation },
    { type: ScrollDispatcher$1 },
    { type: PreviousRouteService }
];
BreadcrumbComponent.propDecorators = {
    actions: [{ type: ContentChild, args: ['uikitActions',] }],
    templateActions: [{ type: ViewChild, args: ['templateActions',] }],
    templateFinder: [{ type: ViewChild, args: ['templateFinder',] }],
    templateFilters: [{ type: ViewChild, args: ['templateFilters',] }],
    templatePaginator: [{ type: ViewChild, args: ['templatePaginator',] }]
};
if (false) {
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.subscription;
    /** @type {?} */
    BreadcrumbComponent.prototype.menuItem$;
    /** @type {?} */
    BreadcrumbComponent.prototype.menuItemSubscription$;
    /** @type {?} */
    BreadcrumbComponent.prototype.isFavorited$;
    /** @type {?} */
    BreadcrumbComponent.prototype.isFavorited;
    /** @type {?} */
    BreadcrumbComponent.prototype.isMobile$;
    /** @type {?} */
    BreadcrumbComponent.prototype.navigationEndSubscription;
    /** @type {?} */
    BreadcrumbComponent.prototype.data$;
    /** @type {?} */
    BreadcrumbComponent.prototype.actions;
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.hasActionsSubject;
    /** @type {?} */
    BreadcrumbComponent.prototype.hasActions$;
    /** @type {?} */
    BreadcrumbComponent.prototype.templateActions;
    /** @type {?} */
    BreadcrumbComponent.prototype.templateFinder;
    /** @type {?} */
    BreadcrumbComponent.prototype.templateFilters;
    /** @type {?} */
    BreadcrumbComponent.prototype.templatePaginator;
    /** @type {?} */
    BreadcrumbComponent.prototype.possuiRecursoDeFavoritos;
    /** @type {?} */
    BreadcrumbComponent.prototype.hiddenShell$;
    /** @type {?} */
    BreadcrumbComponent.prototype.isFixed$;
    /** @type {?} */
    BreadcrumbComponent.prototype.desabilitarBotaoVoltar$;
    /** @type {?} */
    BreadcrumbComponent.prototype.layoutService;
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.applicationRef;
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.router;
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.activatedRoute;
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.favNavsService;
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.favNavsQuery;
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.renderer;
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.zone;
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.changeDetector;
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.breadcrumbService;
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.angularLocation;
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.platformLocation;
    /**
     * @type {?}
     * @private
     */
    BreadcrumbComponent.prototype.scrollDispatcher;
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.previousRouteService;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const SEARCH_TOKEN = new InjectionToken('SearchAbstractService');
/**
 * @abstract
 */
class SearchAbstractService {
    /**
     * @protected
     */
    constructor() {
    }
}
SearchAbstractService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
SearchAbstractService.ctorParameters = () => [];
if (false) {
    /**
     * @abstract
     * @param {?} term
     * @return {?}
     */
    SearchAbstractService.prototype.searchGroup = function (term) { };
    /**
     * @abstract
     * @param {?} group
     * @return {?}
     */
    SearchAbstractService.prototype.addGroup = function (group) { };
    /**
     * @abstract
     * @param {?} group
     * @return {?}
     */
    SearchAbstractService.prototype.removeGroup = function (group) { };
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SearchService {
    /**
     * @param {?} services
     */
    constructor(services) {
        this.services = services;
    }
    /**
     * @param {?} term
     * @return {?}
     */
    buscar(term) {
        if (!this.services) {
            console.error('Não existe serviço de busca configurado! Ler documentação para mais informações, de como utilizar.');
            return of();
        }
        /** @type {?} */
        const searchObservables = this.services.map((/**
         * @param {?} searchService
         * @return {?}
         */
        searchService => searchService.searchGroup(term)));
        /** @type {?} */
        const combinedObservable = combineLatest(searchObservables);
        return combinedObservable;
    }
    /**
     * @param {?} group
     * @return {?}
     */
    adicionarGrupoPesquisa(group) {
        this.services.map((/**
         * @param {?} groupSearch
         * @return {?}
         */
        groupSearch => groupSearch.addGroup(group)));
    }
}
SearchService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
SearchService.ctorParameters = () => [
    { type: Array, decorators: [{ type: Optional }, { type: Inject, args: [SEARCH_TOKEN,] }] }
];
/** @nocollapse */ SearchService.ngInjectableDef = defineInjectable({ factory: function SearchService_Factory() { return new SearchService(inject(SEARCH_TOKEN, 8)); }, token: SearchService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @protected
     */
    SearchService.prototype.services;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @return {?}
 */
function searchAnimation() {
    return trigger('searchAnimation', [
        state('true', style({ transform: 'translateX(0%)' })),
        state('false', style({ transform: 'translateX(100%)' })),
        transition('false => true', animate('0.2s ease-in-out', style({ transform: 'translateX(0%)' }))),
        transition('true => false', animate('0.2s ease-in-out', style({ transform: 'translateX(100%)' })))
    ]);
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const localStorage$1 = (/** @type {?} */ (window.localStorage));
class LocalStorageService {
    /**
     * @param {?} key
     * @return {?}
     */
    get(key) {
        /** @type {?} */
        const json = localStorage$1.getItem(key);
        try {
            /** @type {?} */
            const parse = JSON.parse(json);
            return ((parse === '' || parse === 'null' || !parse) ? false : parse);
        }
        catch (e) {
            return json;
        }
    }
    /**
     * @param {?} key
     * @param {?} value
     * @return {?}
     */
    set(key, value) {
        if (typeof value === 'object') {
            value = JSON.stringify(value);
        }
        localStorage$1.setItem(key, value);
    }
    /**
     * @param {?} key
     * @return {?}
     */
    delete(key) {
        try {
            return localStorage$1.removeItem(key);
        }
        catch (e) {
            return null;
        }
    }
}
LocalStorageService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */ LocalStorageService.ngInjectableDef = defineInjectable({ factory: function LocalStorageService_Factory() { return new LocalStorageService(); }, token: LocalStorageService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class LocalRepository {
    /**
     * @param {?} localStorageService
     */
    constructor(localStorageService) {
        this.localStorageService = localStorageService;
    }
    /**
     * @template T
     * @param {?} key
     * @param {?} data
     * @return {?}
     */
    salvarItem(key, data) {
        this.localStorageService.set(key, data);
    }
    /**
     * @template T
     * @param {?} key
     * @return {?}
     */
    obterItem(key) {
        return (/** @type {?} */ (this.localStorageService.get(key)));
    }
    /**
     * @param {?} key
     * @return {?}
     */
    deletar(key) {
        this.localStorageService.delete(key);
    }
}
LocalRepository.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
LocalRepository.ctorParameters = () => [
    { type: LocalStorageService }
];
/** @nocollapse */ LocalRepository.ngInjectableDef = defineInjectable({ factory: function LocalRepository_Factory() { return new LocalRepository(inject(LocalStorageService)); }, token: LocalRepository, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    LocalRepository.prototype.localStorageService;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SearchHistoricoService {
    /**
     * @param {?} repositorio
     */
    constructor(repositorio) {
        this.repositorio = repositorio;
    }
    /**
     * @return {?}
     */
    obterHistorico() {
        /** @type {?} */
        let listaHistorico;
        /** @type {?} */
        const key = 'historicoPesquisa';
        /** @type {?} */
        const item = this.repositorio.obterItem(key);
        if (item === false) {
            this.repositorio.salvarItem(key, []);
        }
        else {
            listaHistorico = item;
        }
        return listaHistorico;
    }
    /**
     * @param {?} historico
     * @return {?}
     */
    salvarHistorico(historico) {
        /** @type {?} */
        let listaHistorico;
        /** @type {?} */
        const key = 'historicoPesquisa';
        /** @type {?} */
        const itemAddHistorage = this.repositorio.obterItem(key);
        /** @type {?} */
        const itemRetorno = xorWith([historico], itemAddHistorage, this.isEqual);
        if (itemRetorno.length > 3) {
            listaHistorico = take(itemRetorno, 3);
        }
        else {
            listaHistorico = itemRetorno;
        }
        this.repositorio.salvarItem(key, listaHistorico);
        return of(null);
    }
    /**
     * @private
     * @param {?} p1
     * @param {?} p2
     * @return {?}
     */
    isEqual(p1, p2) {
        return isEqual({ x: p1.title, y: p1.title }, { x: p2.title, y: p2.title });
    }
}
SearchHistoricoService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
SearchHistoricoService.ctorParameters = () => [
    { type: LocalRepository }
];
/** @nocollapse */ SearchHistoricoService.ngInjectableDef = defineInjectable({ factory: function SearchHistoricoService_Factory() { return new SearchHistoricoService(inject(LocalRepository)); }, token: SearchHistoricoService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    SearchHistoricoService.prototype.repositorio;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class HighlightComponent {
    constructor() {
        this.disabled = false;
        this._isActive = false;
    }
    /**
     * @return {?}
     */
    get isActive() {
        return this._isActive;
    }
    /**
     * @return {?}
     */
    setActiveStyles() {
        this._isActive = true;
    }
    /**
     * @return {?}
     */
    setInactiveStyles() {
        this._isActive = false;
    }
    /**
     * @return {?}
     */
    getLabel() {
        return '';
    }
}
HighlightComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-highlight',
                template: `
    <li [class.disabled]='disabled'>
      <ng-content></ng-content>
    </li>
  `
            }] }
];
HighlightComponent.propDecorators = {
    item: [{ type: Input }],
    disabled: [{ type: Input }],
    isActive: [{ type: HostBinding, args: ['class.active',] }]
};
if (false) {
    /** @type {?} */
    HighlightComponent.prototype.item;
    /** @type {?} */
    HighlightComponent.prototype.disabled;
    /**
     * @type {?}
     * @private
     */
    HighlightComponent.prototype._isActive;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SearchComponent {
    /**
     * @param {?} fb
     * @param {?} searchService
     * @param {?} hotkeysService
     * @param {?} router
     * @param {?} searchHistorico
     * @param {?} renderer
     */
    constructor(fb, searchService, hotkeysService, router, searchHistorico, renderer) {
        this.fb = fb;
        this.searchService = searchService;
        this.hotkeysService = hotkeysService;
        this.router = router;
        this.searchHistorico = searchHistorico;
        this.renderer = renderer;
        this.adicionarGrupo = new EventEmitter();
        this.removerGrupo = new EventEmitter();
        this.placeholderText = 'Pesquisa (Ctrl + Alt + 3)';
        this.stateForm = this.fb.group({
            searchGroups: '',
        });
        this.searchGroups = [];
        this.subscription = new Subscription();
        this.openSearch = false;
        this.isAnimating = false;
    }
    /**
     * @return {?}
     */
    get hasResults() {
        return this.openSearch && this.searchGroupOptions.length;
    }
    /**
     * @return {?}
     */
    ngAfterViewInit() {
        if (document.body.getBoundingClientRect().width <= 970) {
            this.placeholderText = 'Pesquisa';
        }
        this.hotkeysService.add(new Hotkey('ctrl+alt+3', (/**
         * @param {?} event
         * @return {?}
         */
        (event) => {
            this.searchOpen.focus();
            return false; // Prevent bubbling
        })));
        timer(1000).subscribe((/**
         * @return {?}
         */
        () => {
            this.searchOpen.focus();
        }));
        this.keyManager = new ActiveDescendantKeyManager(this.items).withWrap().withTypeAhead();
        this.renderer.setStyle(this.buttonClose._elementRef.nativeElement, 'display', 'none');
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.stateForm.get('searchGroups').valueChanges.subscribe((/**
         * @param {?} value
         * @return {?}
         */
        value => {
            this.itemPesquisado = value;
            this.searchGroupOptions = [];
            this.searchService.buscar(value)
                .subscribe((/**
             * @param {?} itensMenuPesquisa
             * @return {?}
             */
            (itensMenuPesquisa) => {
                if (itensMenuPesquisa && itensMenuPesquisa.length > 0) {
                    map$1(itensMenuPesquisa, (/**
                     * @param {?} item
                     * @return {?}
                     */
                    (item) => this.searchGroupOptions.push(item)));
                }
                else {
                    this.searchGroupOptions.push((/** @type {?} */ ({
                        id: guid(),
                        isAviso: true,
                        title: `Não encontramos '${value}'`
                    })));
                }
            }));
        }));
        this.searchGroupOptions = this.searchHistorico.obterHistorico();
    }
    /**
     * @param {?} event
     * @param {?} searchContainer
     * @return {?}
     */
    openToSearch(event, searchContainer) {
        if (!this.searchOpened
            && event
            && event.code === 'KeyA'
            && event.key.length > 0) {
            this.searchOpened = true;
            this.open(searchContainer);
        }
    }
    /**
     * @param {?} searchContainer
     * @return {?}
     */
    onBlur(searchContainer) {
        this.subscription.add(timer(150).subscribe((/**
         * @return {?}
         */
        () => {
            if (!this.searchOpen.focused) {
                this.close();
                this.renderer.removeAttribute(searchContainer, 'opened');
                this.renderer.setStyle(this.buttonClose._elementRef.nativeElement, 'display', 'none');
            }
        })));
    }
    /**
     * @return {?}
     */
    closeToSearch() {
        if (!this.searchOpen.focused) {
            this.close();
        }
    }
    /**
     * @param {?} searchContainer
     * @return {?}
     */
    open(searchContainer) {
        this.searchOpen.focus();
        this.renderer.setAttribute(searchContainer, 'opened', '');
        this.renderer.removeStyle(this.buttonClose._elementRef.nativeElement, 'display');
    }
    /**
     * @return {?}
     */
    close() {
        this.searchOpened = false;
        ((/** @type {?} */ (document.activeElement))).blur();
        this.stateForm.controls.searchGroups.setValue('');
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
    /**
     * @param {?} searchContainer
     * @return {?}
     */
    toggle(searchContainer) {
        this.searchOpened = (!this.searchOpened);
        if (this.searchOpened) {
            this.open(searchContainer);
        }
        this.stateForm.controls.searchGroups.setValue('');
    }
    /**
     * @param {?} item
     * @param {?} group
     * @return {?}
     */
    goToAddHistoric(item, group) {
        this.searchHistorico.salvarHistorico(group);
        this.router.navigate([item.link[0]]);
        this.closeToSearch();
    }
    /**
     * @return {?}
     */
    get heightSize() {
        return null;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onKeyup(event) {
        switch (event.keyCode) {
            case ENTER:
                /** @type {?} */
                const item = this.keyManager.activeItem.item;
                this.goToAddHistoric(item, item);
                break;
            case ESCAPE:
                this.close();
                break;
            case UP_ARROW:
            case DOWN_ARROW:
                this.keyManager.onKeydown(event);
                this.activeItem(this.keyManager.activeItem.item, event.keyCode === UP_ARROW);
                break;
            default:
                this.keyManager.setFirstItemActive();
                this.activeItem(this.keyManager.activeItem.item, false);
                break;
        }
    }
    /**
     * @param {?} item
     * @param {?} up
     * @return {?}
     */
    activeItem(item, up) {
        if ((/** @type {?} */ (item)) && ((/** @type {?} */ (item))).items && ((/** @type {?} */ (item))).items.length > 0) {
            if (up) {
                this.keyManager.setPreviousItemActive();
            }
            else {
                this.keyManager.setNextItemActive();
            }
            this.activeItem(this.keyManager.activeItem.item, up);
        }
    }
    /**
     * @param {?} item
     * @return {?}
     */
    onMouseoverItem(item) {
        this.keyManager.setActiveItem(item);
    }
}
SearchComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-search',
                template: "<form [formGroup]=\"stateForm\" [class.is-opened]=\"searchOpened\" autocomplete=\"off\" >\n  <div search-container #searchContainer [attr.animating]=\"isAnimating ? '' : null\"\n    [attr.with-results]=\"hasResults ? '' : null\">\n\n    <button mat-icon-button class=\"btn-back\" (click)=\"toggle(searchContainer)\">\n      <i class='fa fa-search icon-search' search-icon></i>\n    </button>\n    <input tabindex=\"3\" matInput (keypress)=\"openToSearch($event,searchContainer)\" #searchOpen=\"matInput\"\n      placeholder=\"Pesquisa (Ctrl + Alt + 3)\" placeholder=\"{{placeholderText}}\" formControlName=\"searchGroups\"\n      (keyup)=\"onKeyup($event)\" (blur)=\"onBlur(searchContainer)\" />\n    <button mat-icon-button class=\"icon-sobre\" #buttonClose=\"matButton\" (click)=\"searchGroupOptions = []; toggle(searchContainer)\">\n      <i class='fa fa-times icon-search' search-icon></i>\n    </button>\n\n    <div resultados-container [ngStyle]='{ height: heightSize }'>\n      <div *ngFor='let groupMenu of searchGroupOptions' [attr.header]='groupMenu.isHeader && !groupMenu.isAviso'>\n        <div group-header *ngIf='!groupMenu.isAviso'>\n          <span>{{groupMenu.title}}</span>\n        </div>\n        <ng-template [ngTemplateOutlet]=\"conteudoMenu\" [ngTemplateOutletContext]=\"{ $implicit: groupMenu }\">\n        </ng-template>\n\n      </div>\n    </div>\n  </div>\n</form>\n\n<ng-template #conteudoMenu let-list>\n  <ul name=\"listaMenus\">\n    <uikit-highlight #searchitem *ngFor='let item of list.items' [attr.header]='item.isHeader && !item.isAviso' [item]=\"item\">\n      <div class=\"search-item-title\">\n        <div group-header *ngIf='item.isHeader && !item.isAviso'>\n          <span>\n            <i class=\"first-letter\" [attr.data-first]=\"item.title.substr(0, 1)\" *ngIf='!item.icon'> </i>\n            <i class='{{item.icon}}' *ngIf='item.icon'></i>\n            {{item.title}}\n          </span>\n        </div>\n\n        <a resultado (mouseover)=\"onMouseoverItem(searchitem)\" (click)='goToAddHistoric(item, item);$event.preventDefault()' href='#'\n          *ngIf='!item.isHeader && !item.isAviso'>\n          <span innerHTML='{{item.title | highlight: itemPesquisado}}'></span>\n        </a>\n\n        <ul *ngIf=\"item.items?.length > 0\">\n          <ng-container *ngTemplateOutlet=\"conteudoMenu; context:{ $implicit: item }\"></ng-container>\n        </ul>\n\n        <div aviso *ngIf='item.isAviso'>\n          {{item.title}}\n        </div>\n      </div>\n    </uikit-highlight>\n  </ul>\n</ng-template>\n\n<div hidden>\n  <ng-content select=\"[search]\"></ng-content>\n</div>\n",
                animations: [searchAnimation()],
                changeDetection: ChangeDetectionStrategy.OnPush,
                styles: ["@media screen and (max-width:970px){.btn-back{right:0!important;top:5px}[search-container]{position:relative!important;-webkit-transition:.1s ease-in-out!important;transition:.1s ease-in-out!important;right:0!important}[search-container] [search-container]{width:40px}[search-container] i[search-icon]{-webkit-transition:.1s ease-in-out,none;transition:.1s ease-in-out,none;width:40px;height:40px;line-height:40px;top:0;right:0;z-index:2;font-size:15px;color:#004bcb;text-align:center}[search-container] input{min-width:300px!important;max-width:970px!important;width:100%!important;color:#004bcb;position:fixed;top:0}[search-container]:not([opened]) input{margin-top:-100%!important}[search-container]:not([opened]) i{border-left:0!important}[search-container][opened]{right:0!important}[search-container][opened] input{min-width:300px!important;max-width:970px!important;width:100%!important;border-radius:0!important;height:63px!important;left:0;right:0;position:fixed;background:#f5f5f5;border-bottom:1px solid #dce4ec;top:0}[search-container][opened] [resultados-container]{left:0;margin-left:0!important;width:100%!important;min-width:300px!important}[search-container][opened] .fa-search{display:none}[search-container][opened] .icon-sobre{right:20px;position:fixed;color:#004bcb}}[search-container]{display:block;border-radius:0;z-index:1099;-webkit-user-select:none;right:170px;top:0}[search-container] input::-webkit-input-placeholder{color:#004bcb}[search-container] input:-moz-placeholder{color:#004bcb}[search-container] input::-moz-placeholder{color:#004bcb}[search-container] input:-ms-input-placeholder{color:#004bcb}[search-container] input{font-size:13px;color:#004bcb;padding:0 10px 0 40px;background:#ecf2fe;border-radius:5px;box-sizing:border-box;line-height:34px;font-weight:400;outline:0;border:none;border-bottom:2px solid #004bcb;width:250px;margin-bottom:3px;height:45px;-webkit-transition:.1s ease-in-out,none,.15s linear;transition:.1s ease-in-out,none,.15s linear}[search-container] i[search-icon]{-webkit-transition:.1s ease-in-out,none;transition:.1s ease-in-out,none;width:40px;height:40px;line-height:40px;right:0;position:absolute;top:0;z-index:2;font-size:15px;color:#004bcb;text-align:center}[search-container] .btn-back{right:-40px;top:5px}[search-container] button[clear-button]{top:7px;height:56px;width:39px;line-height:54px;position:absolute;right:0;z-index:3;border:none;visibility:hidden;background:0 0!important}[search-container] button[clear-button]:hover{box-shadow:inset 0 -56px 0 #003795}[search-container] button[clear-button] i{font-size:15px;font-weight:100}[search-container] [resultados-container]{visibility:hidden;display:none;height:0;position:fixed;padding-bottom:0}[search-container] div,[search-container] ul{padding:0 10px 1px;margin:0;list-style:none}[search-container] div div,[search-container] div li,[search-container] ul div,[search-container] ul li{line-height:1}[search-container] div div:not([header]):first-child::before,[search-container] div div[header]+div::before,[search-container] div div[header]+li::before,[search-container] div li:not([header]):first-child::before,[search-container] div li[header]+div::before,[search-container] div li[header]+li::before,[search-container] ul div:not([header]):first-child::before,[search-container] ul div[header]+div::before,[search-container] ul div[header]+li::before,[search-container] ul li:not([header]):first-child::before,[search-container] ul li[header]+div::before,[search-container] ul li[header]+li::before{height:15px!important;top:0}[search-container] div div i,[search-container] div li i,[search-container] ul div i,[search-container] ul li i{color:#aaa;margin-left:-4px}[search-container] div div a[resultado],[search-container] div li a[resultado],[search-container] ul div a[resultado],[search-container] ul li a[resultado]{padding-left:22px;padding-right:22px;display:block;text-decoration:none}[search-container] div div:not([header]) a,[search-container] div li:not([header]) a,[search-container] ul div:not([header]) a,[search-container] ul li:not([header]) a{display:block}[search-container] div div:not([header])::before,[search-container] div li:not([header])::before,[search-container] ul div:not([header])::before,[search-container] ul li:not([header])::before{clear:both;content:'';border-left:1px dotted #788896;border-bottom:1px dotted #788896;display:block;float:left;margin:0 5px;height:25px;width:15px;position:relative;top:-11px}[search-container] div div:not([header]) span,[search-container] div li:not([header]) span,[search-container] ul div:not([header]) span,[search-container] ul li:not([header]) span{display:block;text-decoration:none;line-height:24px;color:#fff;outline:0;font-size:14px!important;cursor:pointer;-ms-user-select:none;-moz-user-select:none;-webkit-user-select:none;font-weight:400;padding:0 10px 0 1px;-webkit-transition:.1s ease-in-out;transition:.1s ease-in-out}[search-container] div div:not([header]) span strong,[search-container] div li:not([header]) span strong,[search-container] ul div:not([header]) span strong,[search-container] ul li:not([header]) span strong{font-size:14px!important;text-decoration:underline;font-weight:400!important}[search-container] div div[header],[search-container] div li[header],[search-container] ul div[header],[search-container] ul li[header]{margin-top:8px}[search-container] div div[header]>[group-header],[search-container] div li[header]>[group-header],[search-container] ul div[header]>[group-header],[search-container] ul li[header]>[group-header]{padding-left:1px;padding-bottom:1px}[search-container] div div[header]>[group-header] span,[search-container] div li[header]>[group-header] span,[search-container] ul div[header]>[group-header] span,[search-container] ul li[header]>[group-header] span{list-style-type:none;outline:0;padding:0;text-transform:uppercase;cursor:pointer;-ms-user-select:none;-moz-user-select:none;-webkit-user-select:none;font-weight:500;font-size:13px!important;padding-left:10px!important;color:rgba(255,255,255,.5);margin-top:-4px}[search-container] div div[header]>[group-header] span.zmdi,[search-container] div li[header]>[group-header] span.zmdi,[search-container] ul div[header]>[group-header] span.zmdi,[search-container] ul li[header]>[group-header] span.zmdi{position:relative;left:-3px}[search-container] div div[header]>[group-header] .first-letter,[search-container] div li[header]>[group-header] .first-letter,[search-container] ul div[header]>[group-header] .first-letter,[search-container] ul li[header]>[group-header] .first-letter{color:rgba(128,128,128,.5);width:23px;height:23px;padding-top:7px;display:block;text-align:center;float:left;-webkit-transition:.1s linear,none;transition:.1s linear,none;z-index:2;position:relative;font-style:normal;font-weight:700;margin-left:-14px;font-size:13px;background:#002e7c}[search-container] div div[header]>[group-header] .first-letter:before,[search-container] div li[header]>[group-header] .first-letter:before,[search-container] ul div[header]>[group-header] .first-letter:before,[search-container] ul li[header]>[group-header] .first-letter:before{content:attr(data-first);position:absolute;margin-left:-5px;margin-top:-3px}[search-container] div div [aviso],[search-container] div li [aviso],[search-container] ul div [aviso],[search-container] ul li [aviso]{font-size:14px!important;padding:7px 0}[search-container][opened]{margin-top:0}[search-container][opened] button[clear-button]{top:2px!important;visibility:visible}[search-container][opened] button[clear-button] i{color:#002e7c}[search-container][opened] [resultados-container]{background:#fff;border-right:1px solid #dce4ec;border-left:1px solid #dce4ec;border-bottom:1px solid #dce4ec;margin-top:5px;margin-left:40px;min-width:600px;visibility:visible;display:block;overflow:auto;height:auto;max-height:calc(100vh - 250px)}[search-container][opened] input{color:#004bcb;padding:15px 15px 15px 40px;width:600px;height:45px;line-height:34px;top:0;margin-top:0;margin-bottom:3px}[search-container][opened] input::-webkit-input-placeholder{color:#004bcb}[search-container][opened] input:-moz-placeholder{color:#004bcb}[search-container][opened] input::-moz-placeholder{color:#004bcb}[search-container][opened] input:-ms-input-placeholder{color:#004bcb}[search-container][opened] i.icon-search{top:-15px;height:67px;line-height:70px;color:#007bff}[search-container][opened] i.icon-search:hover{background:0 0}[search-container][opened] .icon-sobre{margin-left:-40px}@media all and (max-width:1024px){.is-horizontal-menu [search-container]{margin-left:6px;right:10px!important}.is-horizontal-menu [search-container] i{border:none!important;width:30px!important;font-size:13px!important}.is-horizontal-menu [search-container] input{border:none;background:#ecf2fe;width:30px;height:30px;text-align:center;border-radius:20px;position:relative;vertical-align:middle;line-height:30px;padding:0 30px 0 0;margin:3px 0 0;cursor:pointer}.is-horizontal-menu [search-container][opened]{width:auto}.is-horizontal-menu [search-container][opened] button[clear-button]{width:29px;padding:0}.is-horizontal-menu [search-container][opened] button[clear-button] i{top:-2px!important;position:relative;right:1px}.is-horizontal-menu [search-container][opened] input{width:600px!important;border-radius:0;text-align:left;padding-left:15px;padding-right:35px;color:#004bcb;cursor:auto!important}.is-horizontal-menu [search-container][opened] input::-webkit-input-placeholder{color:#004bcb}.is-horizontal-menu [search-container][opened] input:-moz-placeholder{color:#004bcb}.is-horizontal-menu [search-container][opened] input::-moz-placeholder{color:#004bcb}.is-horizontal-menu [search-container][opened] input:-ms-input-placeholder{color:#004bcb}}.btn-primary,[search-container] button[clear-button]{color:#fff;background-color:#007bff;border-color:#007bff}.btn-primary:hover,[search-container] button:hover[clear-button]{color:#fff;background-color:#0069d9;border-color:#0062cc}.btn-primary.focus,.btn-primary:focus,[search-container] button.focus[clear-button],[search-container] button:focus[clear-button]{box-shadow:0 0 0 .2rem rgba(0,123,255,.5)}.btn-primary.disabled,.btn-primary:disabled,[search-container] button.disabled[clear-button],[search-container] button:disabled[clear-button]{background-color:#007bff;border-color:#007bff}.btn-primary:not([disabled]):not(.disabled).active,.btn-primary:not([disabled]):not(.disabled):active,.show>.btn-primary.dropdown-toggle,[search-container] .show>button.dropdown-toggle[clear-button],[search-container] button:not([disabled]):not(.disabled).active[clear-button],[search-container] button:not([disabled]):not(.disabled):active[clear-button]{color:#fff;background-color:#0062cc;border-color:#005cbf;box-shadow:0 0 0 .2rem rgba(0,123,255,.5)}"]
            }] }
];
/** @nocollapse */
SearchComponent.ctorParameters = () => [
    { type: FormBuilder },
    { type: SearchService },
    { type: HotkeysService },
    { type: Router },
    { type: SearchHistoricoService },
    { type: Renderer2 }
];
SearchComponent.propDecorators = {
    grupoPesquisado: [{ type: Input }],
    adicionarGrupo: [{ type: Output }],
    removerGrupo: [{ type: Output }],
    placeholderText: [{ type: Input }],
    children: [{ type: ContentChildren, args: [MenuItemComponent, { descendants: false },] }],
    items: [{ type: ViewChildren, args: [HighlightComponent,] }],
    searchInput: [{ type: ViewChild, args: ['search',] }],
    searchOpen: [{ type: ViewChild, args: ['searchOpen',] }],
    buttonClose: [{ type: ViewChild, args: ['buttonClose',] }]
};
if (false) {
    /** @type {?} */
    SearchComponent.prototype.grupoPesquisado;
    /** @type {?} */
    SearchComponent.prototype.adicionarGrupo;
    /** @type {?} */
    SearchComponent.prototype.removerGrupo;
    /** @type {?} */
    SearchComponent.prototype.placeholderText;
    /** @type {?} */
    SearchComponent.prototype.children;
    /** @type {?} */
    SearchComponent.prototype.items;
    /**
     * @type {?}
     * @private
     */
    SearchComponent.prototype.keyManager;
    /** @type {?} */
    SearchComponent.prototype.stateForm;
    /** @type {?} */
    SearchComponent.prototype.searchOpened;
    /** @type {?} */
    SearchComponent.prototype.searchGroups;
    /** @type {?} */
    SearchComponent.prototype.subscription;
    /** @type {?} */
    SearchComponent.prototype.searchGroupOptions;
    /** @type {?} */
    SearchComponent.prototype.highlight;
    /** @type {?} */
    SearchComponent.prototype.openSearch;
    /** @type {?} */
    SearchComponent.prototype.itemPesquisado;
    /** @type {?} */
    SearchComponent.prototype.searchInput;
    /** @type {?} */
    SearchComponent.prototype.searchOpen;
    /** @type {?} */
    SearchComponent.prototype.buttonClose;
    /** @type {?} */
    SearchComponent.prototype.isAnimating;
    /**
     * @type {?}
     * @private
     */
    SearchComponent.prototype.fb;
    /**
     * @type {?}
     * @protected
     */
    SearchComponent.prototype.searchService;
    /** @type {?} */
    SearchComponent.prototype.hotkeysService;
    /**
     * @type {?}
     * @protected
     */
    SearchComponent.prototype.router;
    /**
     * @type {?}
     * @private
     */
    SearchComponent.prototype.searchHistorico;
    /**
     * @type {?}
     * @protected
     */
    SearchComponent.prototype.renderer;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function Section() { }
if (false) {
    /** @type {?} */
    Section.prototype.icon;
    /** @type {?} */
    Section.prototype.name;
    /** @type {?} */
    Section.prototype.description;
    /** @type {?} */
    Section.prototype.read;
}
class NotificationComponent {
    /**
     * @param {?} layoutService
     */
    constructor(layoutService) {
        this.layoutService = layoutService;
        this.folders = [
            {
                icon: 'fas fa-file-alt',
                name: 'Photos',
                description: 'Despachar o processo 000912.25 para o gabinete do Dr. Ricardo, no dia 15/12/2018. Ligar para confirmar recebimento.',
                read: true
            },
            {
                icon: 'fas fa-file-alt',
                name: 'Photos',
                description: 'Despachar o processo 000912.2335 para o gabinete do Dr. Ricardo, no dia 15/12/2018. Ligar para confirmar recebimento.',
                read: false
            },
            {
                icon: 'fas fa-file-alt',
                name: 'Photos',
                description: 'Despachar o processo 000912.23235 para o gabinete do Dr. Ricardo, no dia 15/12/2018. Ligar para confirmar recebimento.',
                read: false
            }
        ];
        // @ContentChildren(NotificacaoComponent, { descendants: true }) public notificacoesProjetadas: QueryList<NotificacaoComponent>;
        // public quantidadeDeNotificacoes = new BehaviorSubject<number>(0);
        this.notificacoes$ = this.layoutService.notificacoes$;
        // public notificacoesNaoLidas$ = this.layoutService.notificacoes$.pipe(flatMap(r => r), filter(r => r.read == false), toArray());
        this.quantidadeDeNotificacoesNaoLidas$ = new BehaviorSubject(0);
        this.subscriptions = [];
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.notificacoes$.subscribe((/**
         * @param {?} r
         * @return {?}
         */
        r => {
            /** @type {?} */
            const notNaoLidas = r.filter((/**
             * @param {?} a
             * @return {?}
             */
            a => a.read === false));
            this.quantidadeDeNotificacoesNaoLidas$.next(notNaoLidas.length);
        }));
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.subscriptions.forEach((/**
         * @param {?} s
         * @return {?}
         */
        s => {
            if (s && !s.closed) {
                s.unsubscribe();
            }
        }));
    }
    /**
     * @return {?}
     */
    ngAfterContentInit() {
        // this.notificacoesProjetadas.changes.subscribe(r => {
        //   const notificacoesNaoLidas = this.notificacoesProjetadas.filter(notif => notif.read === false);
        //   this.quantidadeDeNotificacoes.next(notificacoesNaoLidas.length);
        // });
        // this.notificacoesProjetadas.notifyOnChanges();
    }
}
NotificationComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-notification',
                template: "<mat-menu #notificationMenu=\"matMenu\">\n  <mat-list class=\"dropdown-list list-reader\" (click)=\"$event.stopPropagation()\">\n    <h3 mat-subheader>\n      Notifica\u00E7\u00F5es\n        <mat-chip color=\"accent\" selected>{{ (quantidadeDeNotificacoesNaoLidas$ | async) }} nova(s)</mat-chip>\n    </h3>\n    <mat-nav-list>\n\n\n\n        <uikit-notificacao [nome]=\"not.nome\" [icone]=\"not.icone\" [descricao]=\"not.descricao\" [data]=\"not.data\" [read]=\"not.read\" *ngFor=\"let not of (notificacoes$ | async)\" nome=\"teste\" descricao=\"awdadwa\" read=\"false\" ></uikit-notificacao>\n\n\n    </mat-nav-list>\n  </mat-list>\n\n  <!-- <mat-action-list>\n      <a  mat-list-item routerLink=\".\">ver todos</a>\n    </mat-action-list> -->\n</mat-menu>\n\n<button\n  tabindex=\"3\"\n  mat-icon-button\n  color=\"primary\"\n  [matMenuTriggerFor]=\"notificationMenu\"\n  matTooltip=\"Notifica\u00E7\u00F5es\"\n  aria-label=\"Notifica\u00E7\u00F5es\"\n  matBadge=\"{{ (quantidadeDeNotificacoesNaoLidas$ | async) }}\"\n  matBadgeColor=\"accent\"\n  matBadgeSize=\"small\"\n>\n  <mat-icon class=\"fa-2x far fa-bell\"></mat-icon>\n</button>\n",
                changeDetection: ChangeDetectionStrategy.OnPush,
                styles: [""]
            }] }
];
/** @nocollapse */
NotificationComponent.ctorParameters = () => [
    { type: LayoutService }
];
if (false) {
    /** @type {?} */
    NotificationComponent.prototype.folders;
    /** @type {?} */
    NotificationComponent.prototype.notificacoes$;
    /** @type {?} */
    NotificationComponent.prototype.quantidadeDeNotificacoesNaoLidas$;
    /** @type {?} */
    NotificationComponent.prototype.subscriptions;
    /**
     * @type {?}
     * @protected
     */
    NotificationComponent.prototype.layoutService;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function Section$1() { }
if (false) {
    /** @type {?} */
    Section$1.prototype.name;
    /** @type {?} */
    Section$1.prototype.date;
    /** @type {?} */
    Section$1.prototype.description;
    /** @type {?} */
    Section$1.prototype.read;
}
class SysteminfoComponent {
    constructor() {
        this.folders = [
            {
                name: '2.0.2',
                date: '04/02/2019',
                description: 'Nesta versão reunimos grandes demandas de melhoria do sistema.',
                read: true
            },
            {
                name: '2.0.1.1',
                date: '27/11/2018',
                description: 'Para esta versão foi feito um trabalho detalhado de correção, uniformização e melhoria da funcionalidade de redistribuição de processos, muitos tribunais estavam reportando e encaminhando códigos pontuais de correção da redistribuição. A equipe do CNJ fez um trabalho detalhado de revisão e readequação da rotina, simplificando o código, melhorando a usabilidade e padronizando as operações. Com esse trabalho foram atendidas outras 30 demandas de 12 tribunais diferentes.',
                read: false
            }
        ];
    }
    /**
     * @return {?}
     */
    ngOnInit() { }
}
SysteminfoComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-systeminfo',
                template: "<mat-menu #systemInfo=\"matMenu\">\n  <mat-list class=\"dropdown-list list-reader\" (click)=\"$event.stopPropagation()\">\n    <h3 mat-subheader>\n      Informa\u00E7\u00F5es do sistema\n    </h3>\n    <ng-content></ng-content>\n    <!-- <mat-nav-list>\n      <a mat-list-item *ngFor=\"let folder of folders\" [class.is-read]=\"folder.read\">\n        <div>\n          <h4>\n            {{ folder.name }}\n          </h4>\n          <span class=\"date\">{{ folder.date }}</span>\n          <p>\n            {{ folder.description }}\n          </p>\n        </div>\n        <mat-icon matListIcon class=\"fa-lg far\" [class.fa-eye]=\"!folder.read\" [class.fa-eye-slash]=\"folder.read\"></mat-icon>\n\n        <mat-divider></mat-divider>\n      </a>\n    </mat-nav-list> -->\n  </mat-list>\n\n  <!-- <mat-action-list>\n      <a  mat-list-item routerLink=\".\">ver todos</a>\n    </mat-action-list> -->\n</mat-menu>\n\n<button\n  mat-icon-button\n  tabindex=\"4\"\n  color=\"primary\"\n  [matMenuTriggerFor]=\"systemInfo\"\n  matTooltip=\"Informa\u00E7\u00F5es do sistema\"\n  aria-label=\"Informa\u00E7\u00F5es do sistema\"\n>\n  <mat-icon class=\"fa-2x fas fa-info-circle\"></mat-icon>\n</button>\n",
                changeDetection: ChangeDetectionStrategy.OnPush,
                styles: [""]
            }] }
];
/** @nocollapse */
SysteminfoComponent.ctorParameters = () => [];
if (false) {
    /** @type {?} */
    SysteminfoComponent.prototype.folders;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class AccessibilityComponent {
    /**
     * @param {?} renderer
     */
    constructor(renderer) {
        this.renderer = renderer;
        this.showIa = false;
        this.showShortcuts = true;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @return {?}
     */
    ngAfterViewInit() {
        this.slider.valueChange.subscribe((/**
         * @param {?} valor
         * @return {?}
         */
        valor => {
            this.renderer.setStyle(document.body, 'font-size', valor + 'em');
        }));
        this.toggleContrast.toggleChange.subscribe((/**
         * @return {?}
         */
        () => {
            if (!this.toggleContrast.checked) {
                this.renderer.setAttribute(document.querySelector('.logotipo .is-mobile'), 'src', '../../assets/images/uikit-logotipo-mobile-bw.svg');
                this.renderer.setAttribute(document.querySelector('.logotipo .is-desktop'), 'src', '../../assets/images/uikit-logotipo-bw.svg');
                this.renderer.addClass(document.body, 'dark');
            }
            else {
                this.renderer.setAttribute(document.querySelector('.logotipo .is-mobile'), 'src', '../../assets/images/uikit-logotipo-mobile.svg');
                this.renderer.setAttribute(document.querySelector('.logotipo .is-desktop'), 'src', '../../assets/images/uikit-logotipo.svg');
                this.renderer.removeClass(document.body, 'dark');
            }
        }));
    }
}
AccessibilityComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-accessibility',
                template: "<mat-menu #accessibilityMenu=\"matMenu\">\n  <mat-list\n    class=\"dropdown-list\"\n    (click)=\"$event.stopPropagation()\"\n    style=\"display: contents;\"\n  >\n    <h3 mat-subheader>\n      Acessibilidade\n    </h3>\n\n    <mat-list>\n      <mat-list-item class=\"tamanho-fonte\">\n        <div mat-line>\n          <h4>Tamanho das fontes</h4>\n          <mat-slider\n            #slider\n            [max]=\"1.2\"\n            [min]=\"0.8\"\n            [step]=\"0.1\"\n            [value]=\"1\"\n            color=\"primary\"\n          >\n          </mat-slider>\n        </div>\n        <mat-divider></mat-divider>\n      </mat-list-item>\n\n      <mat-list-item class=\"contraste\">\n        <div mat-line>\n          <h4>Contraste</h4>\n          <p>\n            <mat-slide-toggle #toggleContrast color=\"primary\">{{\n              !toggleContrast.checked ? 'Ligar' : 'Desligar'\n            }}</mat-slide-toggle>\n          </p>\n        </div>\n        <mat-divider></mat-divider>\n      </mat-list-item>\n\n      <mat-list-item *ngIf=\"showShortcuts\">\n        <div mat-line>\n          <h4>Teclas de atalho</h4>\n\n          <p mat-line>\n            Pesquisa Principal\n            <mat-chip-list>\n              <mat-chip>ctrl</mat-chip> + <mat-chip>alt</mat-chip> +\n              <mat-chip color=\"primary\" selected>f</mat-chip>\n            </mat-chip-list>\n          </p>\n\n          <p mat-line>\n            Salvar conte\u00FAdo\n            <mat-chip-list>\n              <mat-chip>ctrl</mat-chip> +\n              <mat-chip color=\"primary\" selected>s</mat-chip>\n            </mat-chip-list>\n          </p>\n\n          <p mat-line>\n            Novo conte\u00FAdo\n            <mat-chip-list>\n              <mat-chip>ctrl</mat-chip> + <mat-chip>alt</mat-chip> +\n              <mat-chip color=\"primary\" selected>n</mat-chip>\n            </mat-chip-list>\n          </p>\n\n          <p mat-line>\n            Pesquisar conte\u00FAdo\n            <mat-chip-list>\n              <mat-chip>alt</mat-chip> +\n              <mat-chip color=\"primary\" selected>3</mat-chip>\n            </mat-chip-list>\n          </p>\n\n          <p mat-line>\n            Filtro avan\u00E7ado\n            <mat-chip-list>\n              <mat-chip>ctrl</mat-chip> + <mat-chip>alt</mat-chip> +\n              <mat-chip color=\"primary\" selected>3</mat-chip>\n            </mat-chip-list>\n          </p>\n\n          <p mat-line>\n            Voltar/Pr\u00F3xima p\u00E1gina\n            <mat-chip-list>\n              <mat-chip><mat-icon class=\"fas fa-arrow-left\" style=\"width: auto; margin: 0px !important;\"></mat-icon></mat-chip>\n              ou\n              <mat-chip><mat-icon class=\"fas fa-arrow-right\" style=\"width: auto; margin: 0px !important;\"></mat-icon></mat-chip>\n            </mat-chip-list>\n          </p>\n\n          <p mat-line>\n            Menu Principal\n            <mat-chip-list>\n              <mat-chip>ctrl</mat-chip> +\n              <mat-chip color=\"primary\" selected>m</mat-chip>\n            </mat-chip-list>\n          </p>\n\n          <p mat-line *ngIf=\"showIa\">\n            Falar com a Judi\n            <mat-chip-list>\n              <mat-chip>ctrl</mat-chip> + <mat-chip>alt</mat-chip> +\n              <mat-chip color=\"primary\" selected>j</mat-chip>\n            </mat-chip-list>\n          </p>\n        </div>\n        <mat-divider></mat-divider>\n      </mat-list-item>\n    </mat-list>\n  </mat-list>\n\n  <!-- <mat-action-list>\n    <a mat-list-item routerLink=\".\">ver todos</a>\n  </mat-action-list> -->\n</mat-menu>\n\n<button\n  mat-icon-button\n  color=\"primary\"\n  [matMenuTriggerFor]=\"accessibilityMenu\"\n  matTooltip=\"Acessibilidade\"\n  aria-label=\"Acessibilidade\"\n  tabindex=\"5\"\n>\n  <mat-icon class=\"fa-2x fas fa-universal-access acessibility\"></mat-icon>\n</button>\n",
                changeDetection: ChangeDetectionStrategy.OnPush,
                styles: [".mat-line .mat-slider-horizontal{max-width:275px;cursor:pointer}.mat-slider-horizontal{width:100%}"]
            }] }
];
/** @nocollapse */
AccessibilityComponent.ctorParameters = () => [
    { type: Renderer2 }
];
AccessibilityComponent.propDecorators = {
    showIa: [{ type: Input }],
    showShortcuts: [{ type: Input }],
    slider: [{ type: ViewChild, args: [MatSlider,] }],
    toggleContrast: [{ type: ViewChild, args: [MatSlideToggle,] }]
};
if (false) {
    /** @type {?} */
    AccessibilityComponent.prototype.showIa;
    /** @type {?} */
    AccessibilityComponent.prototype.showShortcuts;
    /** @type {?} */
    AccessibilityComponent.prototype.slider;
    /** @type {?} */
    AccessibilityComponent.prototype.toggleContrast;
    /**
     * @type {?}
     * @private
     */
    AccessibilityComponent.prototype.renderer;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
// tslint:disable-next-line:max-line-length
/** @type {?} */
const IAuthenticationServiceToken = new InjectionToken('IProviderAuthenticationService');

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const IAuthenticationManagerToken = new InjectionToken('IAuthenticationManager');

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @template TLoginParam, TLogoutParam
 */
class AuthenticationService {
    /**
     * @param {?} userService
     * @param {?} ngLocation
     * @param {?} platformLocation
     * @param {?} locationStrategy
     * @param {?} router
     * @param {?=} providerAuthenticationService
     * @param {?=} authenticationManager
     */
    constructor(userService, ngLocation, platformLocation, locationStrategy, router, providerAuthenticationService, authenticationManager) {
        this.userService = userService;
        this.ngLocation = ngLocation;
        this.platformLocation = platformLocation;
        this.locationStrategy = locationStrategy;
        this.router = router;
        this.providerAuthenticationService = providerAuthenticationService;
        this.authenticationManager = authenticationManager;
        if (this.providerAuthenticationService) {
            this.providerAuthenticationService.user$.subscribe((/**
             * @param {?} user
             * @return {?}
             */
            user => {
                // Transforma do Framework para o do Produto (caso exista)
                if (this.authenticationManager && user) {
                    this.authenticationManager.transform(user).then((/**
                     * @param {?} userAuthenticated
                     * @return {?}
                     */
                    (userAuthenticated) => this.load(userAuthenticated)));
                }
                else {
                    this.load(user);
                }
            }));
        }
    }
    /**
     * @param {?=} args
     * @return {?}
     */
    login(args) {
        /** @type {?} */
        const isOnline = navigator.onLine;
        if (isOnline) {
            if (args && args.enderecoParaVoltar) {
                localStorage.setItem('authentication-callback', args.enderecoParaVoltar);
            }
            /** @type {?} */
            const lastUri = localStorage.getItem('authentication-callback');
            if (!lastUri) {
                localStorage.setItem('authentication-callback', window.location.href);
            }
            /** @type {?} */
            const param = args && args.param;
            return this.providerAuthenticationService.login(param);
        }
        else {
            throw new Error('Não é possível se autenticar sem conexão de rede!');
        }
    }
    /**
     * @private
     * @param {?} providerTransformedUser
     * @return {?}
     */
    load(providerTransformedUser) {
        if (providerTransformedUser != null && providerTransformedUser.authenticated) {
            this.userService.load(providerTransformedUser);
            this.goToLastUri();
        }
        else {
            this.userService.unload();
        }
    }
    /**
     * @protected
     * @return {?}
     */
    goToLastUri() {
        /** @type {?} */
        const lastUri = localStorage.getItem('authentication-callback');
        /** @type {?} */
        const baseHref = this.platformLocation.getBaseHrefFromDOM();
        /** @type {?} */
        const origin = window.location.origin;
        /** @type {?} */
        let hashStrategy = '';
        if (this.locationStrategy instanceof HashLocationStrategy) {
            hashStrategy = '#';
        }
        /** @type {?} */
        const completeUrlToBaseHref = origin + baseHref + hashStrategy;
        if (lastUri && (lastUri.indexOf('protected-route') == -1)) {
            /** @type {?} */
            const uriToNavigate = lastUri.replace(completeUrlToBaseHref, '');
            // let uriToNavigate = lastUri;
            // if (uriToNavigate.startsWith(baseHref)) {
            //   uriToNavigate = uriToNavigate.replace(baseHref, '');
            // }
            localStorage.removeItem('authentication-callback');
            console.debug('Navegando para página: ', lastUri);
            this.router.navigateByUrl(uriToNavigate);
        }
        else {
            this.router.navigateByUrl('');
        }
    }
    /**
     * @param {?=} param
     * @return {?}
     */
    logout(param) {
        return this.providerAuthenticationService.logout(param);
    }
}
AuthenticationService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
AuthenticationService.ctorParameters = () => [
    { type: UserService },
    { type: Location },
    { type: PlatformLocation },
    { type: LocationStrategy },
    { type: Router },
    { type: undefined, decorators: [{ type: Inject, args: [IAuthenticationServiceToken,] }, { type: Optional }] },
    { type: undefined, decorators: [{ type: Inject, args: [IAuthenticationManagerToken,] }, { type: Optional }] }
];
if (false) {
    /**
     * @type {?}
     * @protected
     */
    AuthenticationService.prototype.userService;
    /**
     * @type {?}
     * @private
     */
    AuthenticationService.prototype.ngLocation;
    /**
     * @type {?}
     * @private
     */
    AuthenticationService.prototype.platformLocation;
    /**
     * @type {?}
     * @private
     */
    AuthenticationService.prototype.locationStrategy;
    /**
     * @type {?}
     * @private
     */
    AuthenticationService.prototype.router;
    /**
     * @type {?}
     * @private
     */
    AuthenticationService.prototype.providerAuthenticationService;
    /**
     * @type {?}
     * @protected
     */
    AuthenticationService.prototype.authenticationManager;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
let IAuthorizationManagerToken = new InjectionToken('IAuthorizationManager');

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function IAuthorizationManager() { }
if (false) {
    /**
     * Método responsável por verificar se o usuário possui permissão de executar a ação '{action}' no recurso '{resource}'
     * Attribute Based - https://en.wikipedia.org/wiki/Attribute-based_access_control
     * @param {?} action
     * @param {?} resource
     * @return {?}
     */
    IAuthorizationManager.prototype.authorize = function (action, resource) { };
}
class AuthorizationService {
    /**
     * @param {?=} authorizationService
     */
    constructor(authorizationService) {
        this.authorizationService = authorizationService;
    }
    /**
     * @private
     * @return {?}
     */
    validate() {
        if (this.authorizationService == null) {
            throw new Error('Deve ser registrado um "AuthorizationManager" (IAuthorizationManagerToken) para que se verifique autorização!');
        }
    }
    /**
     * @param {?} action
     * @param {?} resource
     * @return {?}
     */
    authorize(action, resource) {
        return __awaiter(this, void 0, void 0, /** @this {!AuthorizationService} */ function* () {
            this.validate();
            // if (!client) {
            //   if ((!this.authorizationConfig) || (!this.authorizationConfig.clientId)) {
            //     throw Error(`Não é possível verificar autorização sem identificar o 'client'!`);
            //   }
            //   client = this.authorizationConfig.clientId;
            // }
            console.log(`Verificando autorização para executar a ação '${action}' no recurso '${resource}'...`);
            /** @type {?} */
            const hasPermission = yield this.authorizationService.authorize(action, resource);
            console.log(`Autorização para executar a ação '${action}' no recurso '${resource}' foi '${(hasPermission ? 'concedida' : 'negada')}'!`);
            return hasPermission;
        });
    }
}
AuthorizationService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
AuthorizationService.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [IAuthorizationManagerToken,] }, { type: Optional }] }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    AuthorizationService.prototype.authorizationService;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class AuthService {
    /**
     * @param {?} user
     * @param {?=} authentication
     * @param {?=} authorization
     */
    constructor(user, authentication, authorization) {
        this.user = user;
        this.authentication = authentication;
        this.authorization = authorization;
    }
}
AuthService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
AuthService.ctorParameters = () => [
    { type: UserService },
    { type: AuthenticationService, decorators: [{ type: Optional }] },
    { type: AuthorizationService, decorators: [{ type: Optional }] }
];
if (false) {
    /** @type {?} */
    AuthService.prototype.user;
    /** @type {?} */
    AuthService.prototype.authentication;
    /** @type {?} */
    AuthService.prototype.authorization;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class UserinfoComponent {
    /**
     * @param {?} authService
     * @param {?} toastService
     */
    constructor(authService, toastService) {
        this.authService = authService;
        this.toastService = toastService;
        this.user$ = this.authService.user.user$;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @return {?}
     */
    login() {
        return this.authService.authentication.login()
            .catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            this.toastService.error('Error ao realizar o login!', (/** @type {?} */ (error)));
        }));
    }
    /**
     * @return {?}
     */
    logout() {
        try {
            this.authService.authentication.logout();
        }
        catch (error) {
            this.toastService.error('Error ao realizar o logout!', (/** @type {?} */ (error)));
        }
    }
}
UserinfoComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-userinfo',
                template: "<button *ngIf=\"(user$ | async).authenticated === false; else notAuthenticated\" mat-raised-button (click)=\"login()\"\n  class=\"btn-user-login\" color=\"primary\" tabindex=\"6\">\n  <span>Fazer Login</span>\n</button>\n\n<ng-template #notAuthenticated>\n  <mat-menu #userInfoMenu=\"matMenu\">\n    <mat-list class=\"dropdown-list\" (click)=\"$event.stopPropagation()\">\n      <div class=\"logged-info\">\n        <mat-list>\n          <mat-list-item>\n            <div mat-line>\n              <h4>\n\n                <b>\n                  {{ (user$ | async).nickname ? (user$ | async).nickname : '' }}\n                </b>\n\n                <br>\n\n                <i style=\"font-size: 13px;\">\n                  {{ (user$ | async).nickname && (user$ | async).name ? ' registrado(a) civilmente como ' : '' }}\n                </i>\n\n                <br>\n\n                <b style=\"font-size: 13px;\">\n                  {{ (user$ | async).name ? (user$ | async).name : '' }}\n                </b>\n\n              </h4>\n            </div>\n            <div mat-line *ngIf=\"(user$ | async).email\">\n              <h4>{{ (user$ | async).email }}</h4>\n            </div>\n          </mat-list-item>\n        </mat-list>\n      </div>\n\n      <mat-list *ngIf=\"(user$ | async) as user\">\n        <mat-list-item class=\"profiles\" *ngIf=\"user.roles as roles\">\n          <div mat-line>\n            <h4>\n              <mat-icon class=\"fas fa-id-card\"></mat-icon>\n              {{ roles.length === 0 ? 'Sem perfil' : (roles.length === 1 ? 'Perfil' : 'Perfis') }}\n            </h4>\n          </div>\n          <div mat-line *ngFor=\"let role of roles\">\n            {{ role }}\n          </div>\n        </mat-list-item>\n      </mat-list>\n\n      <mat-list>\n        <mat-list-item style=\"min-height: auto !important;\">\n          <div mat-line>\n            <ng-content></ng-content>\n          </div>\n        </mat-list-item>\n      </mat-list>\n\n      <mat-divider></mat-divider>\n      <mat-action-list>\n        <a (click)=\"logout()\" mat-list-item>Sair</a>\n      </mat-action-list>\n    </mat-list>\n  </mat-menu>\n\n  <button tabindex=\"6\" mat-button class=\"btn-user-info\" color=\"primary\" [matMenuTriggerFor]=\"userInfoMenu\"\n    matTooltip=\"Usu\u00E1rio Autenticado\" aria-label=\"Usu\u00E1rio Autenticado\">\n    <div class=\"userinfo\">\n      <div class=\"photo\">\n        <div class=\"picture\">\n          <mat-icon *ngIf=\"!(user$ | async).picture\" aria-label=\"Example icon-button with a heart icon\"\n            class=\"fas fa-user\"></mat-icon>\n          <img *ngIf=\"(user$ | async).picture\" [src]=\"(user$ | async).picture\" alt=\"Foto do usu\u00E1rio logado\" />\n        </div>\n        <mat-icon class=\"fas fa-chevron-circle-down\"></mat-icon>\n      </div>\n\n      <div class=\"info\">\n        <span class=\"name\">{{ (user$ | async).nickname || (user$ | async).given_name || (user$ | async).name }}</span>\n        <!-- <span *ngFor=\"let role of (user$ | async).roles\" class=\"location\">{{ role }}</span> -->\n      </div>\n    </div>\n  </button>\n</ng-template>\n",
                changeDetection: ChangeDetectionStrategy.OnPush,
                styles: [""]
            }] }
];
/** @nocollapse */
UserinfoComponent.ctorParameters = () => [
    { type: AuthService },
    { type: ToastService }
];
if (false) {
    /** @type {?} */
    UserinfoComponent.prototype.user$;
    /**
     * @type {?}
     * @protected
     */
    UserinfoComponent.prototype.authService;
    /**
     * @type {?}
     * @protected
     */
    UserinfoComponent.prototype.toastService;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
const DialogResult = {
    Confirmed: 'Confirmado',
    Cancelled: 'Cancelado',
};
/** @enum {number} */
const ButtonDialog = {
    YesNo: 0,
    OkCancel: 1,
};
ButtonDialog[ButtonDialog.YesNo] = 'YesNo';
ButtonDialog[ButtonDialog.OkCancel] = 'OkCancel';
/**
 * @record
 */
function DialogOptions() { }
if (false) {
    /** @type {?} */
    DialogOptions.prototype.title;
    /** @type {?} */
    DialogOptions.prototype.description;
    /** @type {?|undefined} */
    DialogOptions.prototype.items;
    /** @type {?|undefined} */
    DialogOptions.prototype.button;
}
// @dynamic
class SimpleDialogComponent {
    /**
     * @param {?} dialogRef
     * @param {?} data
     */
    constructor(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.cancelButtonText = 'Cancelar';
        this.confirmButtonText = 'Ok';
        if (data.button === ButtonDialog.YesNo) {
            this.confirmButtonText = 'Sim';
            this.cancelButtonText = 'Não';
        }
        if (data.items && data.items.length > 0) {
            this.columns = Object.keys(data.items[0]);
        }
    }
    /**
     * @return {?}
     */
    onCancelClick() {
        this.dialogRef.close(DialogResult.Cancelled);
    }
    /**
     * @return {?}
     */
    onConfirmClick() {
        this.dialogRef.close(DialogResult.Confirmed);
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
SimpleDialogComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-simple-dialog',
                template: "<h3 class=\"mat-dialog-title\">{{ data.title }}</h3>\n<div class=\"mat-dialog-content mat-typography\">\n  <p>{{ data.description }}</p>\n  <mat-list dense *ngIf=\"data.items != null && data.items.length > 0\">\n\n\n    <table mat-table #table [dataSource]=\"data.items\" mdSort>\n      <ng-container *ngFor=\"let col of columns\" matColumnDef={{col}}>\n        <th mat-header-cell *matHeaderCellDef md-sort-header> {{ col }}</th>\n        <td mat-cell *matCellDef=\"let row\"> {{row[col]}}</td>\n      </ng-container>\n      <tr mat-header-row *matHeaderRowDef=\"columns\"></tr>\n      <tr mat-row *matRowDef=\"let row; columns: columns;\"></tr>\n    </table>\n\n\n  </mat-list>\n</div>\n<div class=\"mat-dialog-actions\" align=\"end\">\n  <button mat-button (click)=\"onCancelClick()\">{{ cancelButtonText }}</button>\n  <button mat-button (click)=\"onConfirmClick()\" cdkFocusInitial>{{ confirmButtonText }}</button>\n</div>\n",
                styles: [""]
            }] }
];
/** @nocollapse */
SimpleDialogComponent.ctorParameters = () => [
    { type: MatDialogRef },
    { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] }
];
if (false) {
    /** @type {?} */
    SimpleDialogComponent.prototype.columns;
    /** @type {?} */
    SimpleDialogComponent.prototype.cancelButtonText;
    /** @type {?} */
    SimpleDialogComponent.prototype.confirmButtonText;
    /** @type {?} */
    SimpleDialogComponent.prototype.dialogRef;
    /** @type {?} */
    SimpleDialogComponent.prototype.data;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class DialogService {
    /**
     * @param {?} dialog
     */
    constructor(dialog) {
        this.dialog = dialog;
    }
    /**
     * @param {?} description
     * @param {?=} items
     * @param {?=} title
     * @param {?=} width
     * @return {?}
     */
    show(description, items, title = 'Confirmação', width = '450px') {
        /** @type {?} */
        const dialogRef = this.dialog.open(SimpleDialogComponent, {
            width: '450px',
            data: {
                title: title,
                description: description,
                items: items
            }
        });
        return dialogRef.afterClosed();
    }
}
DialogService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
DialogService.ctorParameters = () => [
    { type: MatDialog }
];
/** @nocollapse */ DialogService.ngInjectableDef = defineInjectable({ factory: function DialogService_Factory() { return new DialogService(inject(MatDialog$1)); }, token: DialogService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @protected
     */
    DialogService.prototype.dialog;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class IaComponent {
    /**
     * @param {?} navQuery
     * @param {?} navService
     */
    constructor(navQuery, navService) {
        this.navQuery = navQuery;
        this.navService = navService;
        this.rightnav$ = this.navQuery.rightnav$;
        this.subscription = new Subscription();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.subscription.add(this.rightnav$.subscribe((/**
         * @param {?} rightnav
         * @return {?}
         */
        rightnav => {
            this.rightnav = rightnav;
        })));
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
    /**
     * @return {?}
     */
    rightNavToggle() {
        this.navService.toggleRightNav();
    }
    /**
     * @return {?}
     */
    rightNavOpen() {
        this.navService.openRightNav();
    }
    /**
     * @return {?}
     */
    rightNavClose() {
        this.navService.closeRightNav();
    }
    /**
     * @return {?}
     */
    togglePinRightNav() {
        this.navService.togglePinRightNav();
    }
}
IaComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-ia',
                template: "<mat-toolbar>\n  <button\n    class=\"aikit-button\"\n    mat-flat-button\n    type=\"button\"\n    aria-label=\"Abri menu de navega\u00E7\u00E3o\"\n    aria-controls=\"navigation\"\n    matTooltip=\"Abri menu de navega\u00E7\u00E3o\"\n    (click)=\"rightNavToggle()\"\n    tabindex=\"-1\"\n  >\n    <mat-icon class=\"fas fa-times\" aria-label=\"Fixar menu na tela\"></mat-icon>\n  </button>\n\n  <button\n    mat-icon-button\n    color=\"primary\"\n    class=\"btn-attach-chat\"\n    (click)=\"togglePinRightNav()\"\n    [class.is-active]=\"(rightnav$ | async).opened\"\n    [class.is-pinned]=\"(rightnav$ | async).pinned\"\n    tabindex=\"-1\"\n  >\n    <mat-icon\n      class=\"fas fa-thumbtack\"\n      aria-label=\"Fixar menu na tela\"\n    ></mat-icon>\n  </button>\n\n  <img src=\"../../assets/images/iakit-logotipo-mobile.svg\" class=\"is-mobile\" />\n  <img src=\"../../assets/images/iakit-logotipo.svg\" class=\"is-desktop\" />\n</mat-toolbar>\n\n<div class=\"chat\">\n  <div class=\"messages\">\n    <div class=\"message left\">\n      <picture> </picture>\n      <div>Oi, meu nome \u00E9 Judi, em que posso ajudar?</div>\n    </div>\n\n    <div class=\"message right\">\n      <picture>\n        <mat-icon\n          aria-label=\"Example icon-button with a heart icon\"\n          class=\"fas fa-user\"\n        ></mat-icon>\n      </picture>\n      <div>Mensagem do Usu\u00E1rio</div>\n    </div>\n  </div>\n</div>\n\n<div class=\"actions\">\n  <form>\n    <mat-form-field color=\"primary\" appearance=\"fill\">\n      <mat-label>Criar uma nova anota\u00E7\u00E3o</mat-label>\n      <input matInput #message maxlength=\"256\" placeholder=\"Message\" cdkFocusInitial />\n    </mat-form-field>\n    <button mat-flat-button color=\"primary\">ENVIAR</button>\n  </form>\n</div>\n",
                changeDetection: ChangeDetectionStrategy.OnPush,
                styles: [""]
            }] }
];
/** @nocollapse */
IaComponent.ctorParameters = () => [
    { type: NavQuery },
    { type: NavService }
];
if (false) {
    /** @type {?} */
    IaComponent.prototype.rightnav$;
    /** @type {?} */
    IaComponent.prototype.rightnav;
    /**
     * @type {?}
     * @protected
     */
    IaComponent.prototype.subscription;
    /**
     * @type {?}
     * @protected
     */
    IaComponent.prototype.navQuery;
    /**
     * @type {?}
     * @protected
     */
    IaComponent.prototype.navService;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BreadcrumbComponent$1 {
    // breadcrumbs: Breadcrumb[];
    /**
     * @param {?} breadcrumbService
     * @param {?} router
     * @param {?} applicationRef
     */
    constructor(breadcrumbService, router, applicationRef) {
        this.breadcrumbService = breadcrumbService;
        this.router = router;
        this.applicationRef = applicationRef;
        this.breadcrumbsSubject = new BehaviorSubject([]);
        this.breadcrumbs$ = this.breadcrumbsSubject.asObservable();
        this.breadcrumbService.breadcrumbChanged.subscribe((/**
         * @param {?} crumbs
         * @return {?}
         */
        (crumbs) => { this.onBreadcrumbChange(crumbs); }));
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.breadcrumbsSubject.next(this.breadcrumbService.breadcrumbs);
    }
    /**
     * @private
     * @param {?} crumbs
     * @return {?}
     */
    onBreadcrumbChange(crumbs) {
        this.breadcrumbsSubject.next(crumbs);
        // this.applicationRef.tick();
    }
}
BreadcrumbComponent$1.decorators = [
    { type: Component, args: [{
                // tslint:disable-next-line:component-selector
                selector: 'breadcrumb',
                template: `<div #template>
    <ng-content></ng-content>
</div>
<div class="container" *ngIf="template.children.length == 0">
  <span *ngFor="let route of (breadcrumbs$ | async)">
  <a mat-button *ngIf="!route.terminal" href="" [routerLink]="[route.link]">{{ route.title }}</a>
  <a mat-button *ngIf="route.terminal">{{ route.title }}</a>
  </span>
</div>`
            }] }
];
/** @nocollapse */
BreadcrumbComponent$1.ctorParameters = () => [
    { type: BreadcrumbService },
    { type: Router },
    { type: ApplicationRef }
];
if (false) {
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent$1.prototype.breadcrumbsSubject;
    /** @type {?} */
    BreadcrumbComponent$1.prototype.breadcrumbs$;
    /**
     * @type {?}
     * @private
     */
    BreadcrumbComponent$1.prototype.breadcrumbService;
    /**
     * @type {?}
     * @private
     */
    BreadcrumbComponent$1.prototype.router;
    /**
     * @type {?}
     * @private
     */
    BreadcrumbComponent$1.prototype.applicationRef;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @param {?} router
 * @return {?}
 */
function breadcrumbServiceFactory(router) {
    return new BreadcrumbService(router);
}
class BreadcrumbModule {
}
BreadcrumbModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule, RouterModule],
                providers: [
                    { provide: BreadcrumbService, useFactory: breadcrumbServiceFactory, deps: [Router] }
                ],
                declarations: [BreadcrumbComponent$1],
                exports: [BreadcrumbComponent$1]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class AutoFocusDirective {
    /**
     * @param {?} el
     */
    constructor(el) {
        this.el = el;
    }
    /**
     * @return {?}
     */
    ngAfterContentInit() {
        setTimeout((/**
         * @return {?}
         */
        () => {
            this.hideKeyboard(this.el.nativeElement);
        }), 500);
    }
    /**
     * @param {?} el
     * @return {?}
     */
    hideKeyboard(el) {
        /** @type {?} */
        const agent = window.navigator.userAgent.toLowerCase();
        /** @type {?} */
        const att = document.createAttribute('readonly');
        el.setAttributeNode(att);
        setTimeout((/**
         * @return {?}
         */
        () => {
            el.blur();
            /** @type {?} */
            const isSafari = (agent.indexOf('safari') !== -1) && (!(agent.indexOf('chrome') > -1));
            if (!isSafari) {
                el.focus();
            }
            el.removeAttribute('readonly');
        }), 100);
    }
}
AutoFocusDirective.decorators = [
    { type: Directive, args: [{
                selector: '[autofocus]'
            },] }
];
/** @nocollapse */
AutoFocusDirective.ctorParameters = () => [
    { type: ElementRef }
];
AutoFocusDirective.propDecorators = {
    appAutoFocus: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    AutoFocusDirective.prototype.appAutoFocus;
    /**
     * @type {?}
     * @private
     */
    AutoFocusDirective.prototype.el;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class HighlightPipe {
    /**
     * @param {?} value
     * @param {?} term
     * @return {?}
     */
    transform(value, term) {
        if (!value) {
            return '';
        }
        /** @type {?} */
        const regex = this.createRegex(term);
        if (!regex) {
            return value;
        }
        // tslint:disable-next-line:only-arrow-functions
        value = value.replace(regex, (/**
         * @param {?} matched
         * @param {?} group1
         * @return {?}
         */
        function (matched, group1) {
            return '<strong>' + group1 + '</strong>';
        }));
        return value;
    }
    /**
     * @param {?} input
     * @return {?}
     */
    createRegex(input) {
        input = input || '';
        input = this.createReplacements(input);
        input = input.replace(/^[^A-z\u00C0-\u00ff]+|[^A-z\u00C0-\u00ff]+$/g, '');
        input = input.replace(/^\||\|$/g, '');
        if (input) {
            /** @type {?} */
            const re = '(' + input + ')';
            return new RegExp(re, 'i');
        }
        return null;
    }
    /**
     * @param {?} str
     * @return {?}
     */
    createReplacements(str) {
        /** @type {?} */
        const replacements = [
            '[aàáâãäå]',
            '(æ|oe)',
            '[cç]',
            '[eèéêë]',
            '[iìíîï]',
            '[nñ]',
            '[oòóôõö]',
            '[uùúûü]',
            '[yýÿ]'
        ];
        return replacements.reduce((/**
         * @param {?} item
         * @param {?} regexStr
         * @return {?}
         */
        (item, regexStr) => {
            return item.replace(new RegExp(regexStr, 'gi'), regexStr);
        }), str);
    }
}
HighlightPipe.decorators = [
    { type: Pipe, args: [{ name: 'highlight' },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SkeletonService {
    /**
     * @param {?} el
     * @param {?} renderer
     */
    constructor(el, renderer) {
        this.el = el;
        this.renderer = renderer;
    }
    /**
     * @return {?}
     */
    show() {
        this.renderer.addClass(this.el.nativeElement, 'is-loading');
        this.el.nativeElement.disabled = false;
    }
    /**
     * @return {?}
     */
    hide() {
        this.renderer.removeClass(this.el.nativeElement, 'is-loading');
        this.el.nativeElement.disabled = true;
    }
}
if (false) {
    /**
     * @type {?}
     * @protected
     */
    SkeletonService.prototype.el;
    /**
     * @type {?}
     * @protected
     */
    SkeletonService.prototype.renderer;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SpinnerService {
    /**
     * @param {?} el
     * @param {?} renderer
     * @param {?} viewContainerRef
     * @param {?} componentFactoryResolver
     */
    constructor(el, renderer, viewContainerRef, componentFactoryResolver) {
        this.el = el;
        this.renderer = renderer;
        this.viewContainerRef = viewContainerRef;
        this.componentFactoryResolver = componentFactoryResolver;
        this.Init();
    }
    /**
     * @return {?}
     */
    Init() {
        /** @type {?} */
        const factory = this.componentFactoryResolver.resolveComponentFactory(MatSpinner);
        /** @type {?} */
        const componentRef = this.viewContainerRef.createComponent(factory);
        this.divCenter = this.renderer.createElement('div');
        this.spinner = componentRef.instance;
        this.spinner.strokeWidth = 3;
        this.spinner.diameter = 24;
        this.renderer.addClass(this.divCenter, 'uikit-container-spinner');
        this.renderer.addClass(this.spinner._elementRef.nativeElement, 'uikit-spinner');
        this.renderer.setStyle(this.spinner._elementRef.nativeElement, 'display', 'none');
        /** @type {?} */
        const spanButton = (/** @type {?} */ (this.el.nativeElement.querySelector('.mat-button-wrapper')));
        if (spanButton) {
            this.renderer.setStyle(spanButton, 'display', 'flex');
            this.renderer.setStyle(spanButton, 'align-items', 'center');
            this.renderer.setStyle(spanButton, 'justify-content', 'center');
        }
    }
    /**
     * @return {?}
     */
    hide() {
        this.renderer.setStyle(this.spinner._elementRef.nativeElement, 'display', 'none');
        this.renderer.removeChild(this.el.nativeElement.firstChild, this.spinner._elementRef.nativeElement);
        this.renderer.removeChild(this.el.nativeElement.firstChild, this.divCenter);
        this.el.nativeElement.disabled = false;
    }
    /**
     * @return {?}
     */
    show() {
        this.renderer.appendChild(this.el.nativeElement.firstChild, this.divCenter);
        this.renderer.appendChild(this.el.nativeElement.firstChild, this.spinner._elementRef.nativeElement);
        this.renderer.setStyle(this.spinner._elementRef.nativeElement, 'display', 'inherit');
        this.el.nativeElement.disabled = true;
    }
}
if (false) {
    /**
     * @type {?}
     * @protected
     */
    SpinnerService.prototype.spinner;
    /**
     * @type {?}
     * @protected
     */
    SpinnerService.prototype.divCenter;
    /**
     * @type {?}
     * @protected
     */
    SpinnerService.prototype.el;
    /**
     * @type {?}
     * @protected
     */
    SpinnerService.prototype.renderer;
    /**
     * @type {?}
     * @protected
     */
    SpinnerService.prototype.viewContainerRef;
    /**
     * @type {?}
     * @protected
     */
    SpinnerService.prototype.componentFactoryResolver;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class LoadingDirective {
    /**
     * @param {?} el
     * @param {?} renderer
     * @param {?} viewContainerRef
     * @param {?} componentFactoryResolver
     * @param {?} matTable
     */
    constructor(el, renderer, viewContainerRef, componentFactoryResolver, matTable) {
        this.el = el;
        this.renderer = renderer;
        this.viewContainerRef = viewContainerRef;
        this.componentFactoryResolver = componentFactoryResolver;
        this.matTable = matTable;
        if (matTable) {
            this.skeletonService = new SkeletonService(el, renderer);
        }
        else {
            this.spinnerService = new SpinnerService(el, renderer, viewContainerRef, componentFactoryResolver);
        }
    }
    /**
     * @param {?} condition
     * @return {?}
     */
    set isLoading(condition) {
        if (condition) {
            this.show();
        }
        else {
            this.hide();
        }
    }
    /**
     * @return {?}
     */
    show() {
        if (this.matTable) {
            this.skeletonService.show();
        }
        else {
            this.spinnerService.show();
        }
    }
    /**
     * @return {?}
     */
    hide() {
        if (this.matTable) {
            this.skeletonService.hide();
        }
        else {
            this.spinnerService.hide();
        }
    }
}
LoadingDirective.decorators = [
    { type: Directive, args: [{
                selector: '[uikitLoading]'
            },] }
];
/** @nocollapse */
LoadingDirective.ctorParameters = () => [
    { type: ElementRef },
    { type: Renderer2 },
    { type: ViewContainerRef },
    { type: ComponentFactoryResolver },
    { type: MatTable, decorators: [{ type: Host }, { type: Self }, { type: Optional }] }
];
LoadingDirective.propDecorators = {
    isLoading: [{ type: Input, args: ['uikitLoading',] }]
};
if (false) {
    /**
     * @type {?}
     * @protected
     */
    LoadingDirective.prototype.skeletonService;
    /**
     * @type {?}
     * @protected
     */
    LoadingDirective.prototype.spinnerService;
    /**
     * @type {?}
     * @protected
     */
    LoadingDirective.prototype.el;
    /**
     * @type {?}
     * @protected
     */
    LoadingDirective.prototype.renderer;
    /**
     * @type {?}
     * @protected
     */
    LoadingDirective.prototype.viewContainerRef;
    /**
     * @type {?}
     * @protected
     */
    LoadingDirective.prototype.componentFactoryResolver;
    /** @type {?} */
    LoadingDirective.prototype.matTable;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SpinnerDirective {
    /**
     * @param {?} el
     * @param {?} renderer
     * @param {?} viewContainerRef
     * @param {?} componentFactoryResolver
     */
    constructor(el, renderer, viewContainerRef, componentFactoryResolver) {
        this.el = el;
        this.renderer = renderer;
        this.viewContainerRef = viewContainerRef;
        this.componentFactoryResolver = componentFactoryResolver;
        this.spinnerService = new SpinnerService(el, renderer, viewContainerRef, componentFactoryResolver);
    }
    /**
     * @param {?} condition
     * @return {?}
     */
    set isLoading(condition) {
        if (condition) {
            this.spinnerService.show();
        }
        else {
            this.spinnerService.hide();
        }
    }
}
SpinnerDirective.decorators = [
    { type: Directive, args: [{
                selector: '[uikitSpinner]'
            },] }
];
/** @nocollapse */
SpinnerDirective.ctorParameters = () => [
    { type: ElementRef },
    { type: Renderer2 },
    { type: ViewContainerRef },
    { type: ComponentFactoryResolver }
];
SpinnerDirective.propDecorators = {
    isLoading: [{ type: Input, args: ['uikitSpinner',] }]
};
if (false) {
    /**
     * @type {?}
     * @protected
     */
    SpinnerDirective.prototype.spinnerService;
    /**
     * @type {?}
     * @protected
     */
    SpinnerDirective.prototype.el;
    /**
     * @type {?}
     * @protected
     */
    SpinnerDirective.prototype.renderer;
    /**
     * @type {?}
     * @protected
     */
    SpinnerDirective.prototype.viewContainerRef;
    /**
     * @type {?}
     * @protected
     */
    SpinnerDirective.prototype.componentFactoryResolver;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SkeletonDirective {
    /**
     * @param {?} el
     * @param {?} renderer
     */
    constructor(el, renderer) {
        this.el = el;
        this.renderer = renderer;
        this.skeletonService = new SkeletonService(el, renderer);
    }
    /**
     * @param {?} condition
     * @return {?}
     */
    set isLoading(condition) {
        if (condition) {
            this.skeletonService.show();
        }
        else {
            this.skeletonService.hide();
        }
    }
}
SkeletonDirective.decorators = [
    { type: Directive, args: [{
                selector: '[uikitSkeleton]'
            },] }
];
/** @nocollapse */
SkeletonDirective.ctorParameters = () => [
    { type: ElementRef },
    { type: Renderer2 }
];
SkeletonDirective.propDecorators = {
    isLoading: [{ type: Input, args: ['uikitSkeleton',] }]
};
if (false) {
    /**
     * @type {?}
     * @protected
     */
    SkeletonDirective.prototype.skeletonService;
    /**
     * @type {?}
     * @protected
     */
    SkeletonDirective.prototype.el;
    /**
     * @type {?}
     * @protected
     */
    SkeletonDirective.prototype.renderer;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class UikitSharedModule {
}
UikitSharedModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    AutoFocusDirective,
                    LoadingDirective,
                    SpinnerDirective,
                    SkeletonDirective,
                    HighlightPipe
                ],
                imports: [
                    CommonModule,
                ],
                exports: [
                    AutoFocusDirective,
                    LoadingDirective,
                    SpinnerDirective,
                    SkeletonDirective,
                    HighlightPipe,
                ],
                entryComponents: [MatSpinner$1]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class ToastModule {
}
ToastModule.decorators = [
    { type: NgModule, args: [{
                entryComponents: [ToastComponent],
                declarations: [ToastComponent],
                imports: [
                    CommonModule,
                    ToastNoAnimationModule,
                    ToastrModule.forRoot(),
                    ToastContainerModule,
                    BrowserAnimationsModule,
                ],
                exports: [ToastComponent],
                providers: [ToastService]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class MenuSearchComponent {
    /**
     * @param {?} fb
     * @param {?} menuSearchService
     * @param {?} hotkeysService
     * @param {?} navService
     * @param {?} router
     */
    constructor(fb, menuSearchService, hotkeysService, navService, router) {
        this.fb = fb;
        this.menuSearchService = menuSearchService;
        this.hotkeysService = hotkeysService;
        this.navService = navService;
        this.router = router;
        this.placeholderText = document.body.getBoundingClientRect().width > 970 ? 'Pesquisa (Ctrl + Alt + 3)' : 'Pesquisa';
        this.eventSearch = new EventEmitter();
        this.treeControl = new NestedTreeControl((/**
         * @param {?} node
         * @return {?}
         */
        node => node.children));
        this.menuItems$ = this.menuSearchService.menuItems$;
        this.stateForm = this.fb.group({
            searchGroups: '',
        });
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.menuSearchService.buscar('');
        this.stateForm.get('searchGroups').valueChanges.subscribe((/**
         * @param {?} value
         * @return {?}
         */
        value => {
            this.menuSearchService.buscar(value);
        }));
    }
    /**
     * @return {?}
     */
    ngAfterViewInit() {
        this.hotkeysService.add(new Hotkey('ctrl+alt+3', (/**
         * @param {?} event
         * @return {?}
         */
        (event) => {
            this.navService.toggleLeftNav();
            this.searchOpen.focus();
            return false;
        })));
        if (this.itensMenu) {
            this.keyManager = new ActiveDescendantKeyManager(this.itensMenu).withWrap().withTypeAhead();
        }
    }
    /**
     * @param {?} item
     * @return {?}
     */
    goToLink(item) {
        this.router.navigate([item.link]);
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onKeyup(event) {
        switch (event.keyCode) {
            case ENTER:
                this.goToLink(this.keyManager.activeItem.item);
                break;
            case ESCAPE:
                this.menuSearchService.buscar('');
                this.searchOpen.value = '';
                break;
            case UP_ARROW:
            case DOWN_ARROW:
                this.keyManager.onKeydown(event);
                this.activeItem(this.keyManager.activeItem.item, event.keyCode === UP_ARROW);
                this.expandAllNode();
                break;
        }
    }
    /**
     * @private
     * @return {?}
     */
    expandAllNode() {
        this.menuItems$.pipe(map((/**
         * @param {?} menuItens
         * @return {?}
         */
        (menuItens) => {
            /** @type {?} */
            const funcItens = (/**
             * @param {?} itens
             * @return {?}
             */
            (itens) => {
                itens.forEach((/**
                 * @param {?} item
                 * @return {?}
                 */
                (item) => {
                    if (item.children) {
                        this.treeControl.expand(item);
                        funcItens(item.children);
                    }
                }));
            });
            funcItens(menuItens);
        }))).subscribe();
    }
    /**
     * @private
     * @param {?} item
     * @param {?} up
     * @return {?}
     */
    activeItem(item, up) {
        if (item.children && item.children.length > 0) {
            if (up) {
                this.keyManager.setPreviousItemActive();
            }
            else {
                this.keyManager.setNextItemActive();
            }
            this.activeItem(this.keyManager.activeItem.item, up);
        }
    }
}
MenuSearchComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-menu-search',
                template: "<form [formGroup]=\"stateForm\" autocomplete=\"off\">\n  <div search-menu-container>\n    <input class=\"search-menu\" tabindex=\"3\" matInput #searchOpen=\"matInput\" placeholder=\"{{placeholderText}}\" (keyup)=\"onKeyup($event)\"\n           formControlName=\"searchGroups\"/>\n\n    <uikit-menu-item></uikit-menu-item>\n  </div>\n</form>\n",
                changeDetection: ChangeDetectionStrategy.OnPush,
                styles: [".search-menu{background-image:url(\"data:image/svg+xml,%3Csvg aria-hidden='true' focusable='false' data-prefix='fas' data-icon='search' role='img' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512' class='svg-inline--fa fa-search fa-w-16 fa-2x' style=' color: %23004bcb; font-size: 16px;%0A'%3E%3Cpath fill='currentColor' d='M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z' class=''%3E%3C/path%3E%3C/svg%3E\");background-repeat:no-repeat;background-position:right;background-size:5%;background-position-x:97%}.icon-back{position:absolute;padding:13px;margin-left:85%}input{color:#004bcb;font-size:13px;padding:15px 15px 15px 18px;width:600px;height:45px;line-height:34px;top:0;margin-top:0;margin-bottom:3px;background:#edf3ff;border-top:1px solid #d9e3f7;border-bottom:1px solid #d9e3f7}input::-webkit-input-placeholder{color:#004bcb}input:-moz-placeholder{color:#004bcb}input::-moz-placeholder{color:#004bcb}input:-ms-input-placeholder{color:#004bcb}i[search-icon]{font-size:15px;color:#004bcb}.mat-list-base{paddint-top:0!important}.dark input{color:#fff!important;background:#555!important;border-bottom:2px solid #fff!important}.dark input::-webkit-input-placeholder{color:#fff!important}.dark input:-moz-placeholder{color:#fff!important}.dark input::-moz-placeholder{color:#fff!important}.dark input:-ms-input-placeholder{color:#fff!important}.dark i.icon-search{color:#fff!important}.dark i.icon-search:hover{background:0 0}.dark[opened] button[clear-button] i{color:#fff!important}.dark[opened] [resultados-container]{background:#555!important;color:#fff!important;border-right:1px solid #333!important;border-left:1px solid #333!important;border-bottom:1px solid #333!important}.dark[opened] [resultados-container] span{color:#fff!important;border:0!important}.dark[opened] input{background:#555!important;color:#fff!important}.dark[opened] input::-webkit-input-placeholder{color:#fff!important}.dark[opened] input:-moz-placeholder{color:#fff!important}.dark[opened] input::-moz-placeholder{color:#fff!important}.dark[opened] input:-ms-input-placeholder{color:#fff!important}.dark[opened] i.icon-search{color:#fff!important}.dark[opened] i.icon-search:hover{background:0 0}.dark[opened] .icon-sobre{margin-left:-40px}"]
            }] }
];
/** @nocollapse */
MenuSearchComponent.ctorParameters = () => [
    { type: FormBuilder },
    { type: MenuSearchService },
    { type: HotkeysService },
    { type: NavService },
    { type: Router }
];
MenuSearchComponent.propDecorators = {
    placeholderText: [{ type: Input }],
    eventSearch: [{ type: Output }],
    searchOpen: [{ type: ViewChild, args: ['searchOpen',] }],
    itensMenu: [{ type: ViewChildren, args: [HighlightComponent,] }]
};
if (false) {
    /** @type {?} */
    MenuSearchComponent.prototype.placeholderText;
    /** @type {?} */
    MenuSearchComponent.prototype.eventSearch;
    /** @type {?} */
    MenuSearchComponent.prototype.searchOpen;
    /** @type {?} */
    MenuSearchComponent.prototype.itensMenu;
    /**
     * @type {?}
     * @private
     */
    MenuSearchComponent.prototype.keyManager;
    /** @type {?} */
    MenuSearchComponent.prototype.treeControl;
    /** @type {?} */
    MenuSearchComponent.prototype.menuItems$;
    /** @type {?} */
    MenuSearchComponent.prototype.stateForm;
    /**
     * @type {?}
     * @private
     */
    MenuSearchComponent.prototype.fb;
    /**
     * @type {?}
     * @protected
     */
    MenuSearchComponent.prototype.menuSearchService;
    /** @type {?} */
    MenuSearchComponent.prototype.hotkeysService;
    /**
     * @type {?}
     * @protected
     */
    MenuSearchComponent.prototype.navService;
    /**
     * @type {?}
     * @protected
     */
    MenuSearchComponent.prototype.router;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class LayoutModule {
    /**
     * @param {?} previousRouteService
     */
    constructor(previousRouteService) {
        this.previousRouteService = previousRouteService;
    }
}
LayoutModule.decorators = [
    { type: NgModule, args: [{
                providers: [
                    LayoutService,
                    FavNavsService,
                    DialogService,
                    PreviousRouteService,
                    UikitRrippleService
                ],
                declarations: [
                    LayoutComponent,
                    HeaderComponent,
                    NavComponent,
                    MenuComponent,
                    FavnavComponent,
                    SearchComponent,
                    HighlightComponent,
                    NotificationComponent,
                    SysteminfoComponent,
                    AccessibilityComponent,
                    UserinfoComponent,
                    BreadcrumbComponent,
                    MenuItemComponent,
                    NotificacaoComponent,
                    SimpleDialogComponent,
                    BreadcrumbComponent,
                    MenuSearchComponent,
                    IaComponent
                ],
                imports: [
                    CommonModule,
                    RouterModule,
                    FormsModule,
                    ReactiveFormsModule,
                    HotkeyModule.forRoot(),
                    MatSidenavModule,
                    MatSnackBarModule,
                    MatToolbarModule,
                    MatButtonModule,
                    MatMenuModule,
                    MatIconModule,
                    MatBadgeModule,
                    MatTooltipModule,
                    DragDropModule,
                    MatExpansionModule,
                    MatListModule,
                    MatAutocompleteModule,
                    MatFormFieldModule,
                    MatInputModule,
                    ScrollingModule,
                    MatChipsModule,
                    MatSliderModule,
                    CdkStepperModule,
                    MatSlideToggleModule,
                    MatTabsModule,
                    MatSelectModule,
                    MatPaginatorModule,
                    MatTableModule,
                    MatTreeModule,
                    BreadcrumbModule,
                    UikitSharedModule,
                    ToastModule,
                ],
                exports: [
                    MatIconModule,
                    MatButtonModule,
                    MatSnackBarModule,
                    LayoutComponent,
                    HeaderComponent,
                    NavComponent,
                    MenuComponent,
                    FavnavComponent,
                    MatListModule,
                    SearchComponent,
                    NotificationComponent,
                    SysteminfoComponent,
                    AccessibilityComponent,
                    UserinfoComponent,
                    MenuItemComponent,
                    HotkeyModule,
                    NotificacaoComponent,
                    SimpleDialogComponent,
                    BreadcrumbModule,
                    MenuSearchComponent,
                    ToastModule
                ],
                entryComponents: [
                    LayoutComponent,
                    HeaderComponent,
                    NavComponent,
                    MenuComponent,
                    FavnavComponent,
                    SearchComponent,
                    NotificationComponent,
                    SysteminfoComponent,
                    AccessibilityComponent,
                    UserinfoComponent,
                    MenuItemComponent,
                    NotificacaoComponent,
                    SimpleDialogComponent,
                    BreadcrumbComponent,
                    IaComponent
                ]
            },] }
];
/** @nocollapse */
LayoutModule.ctorParameters = () => [
    { type: PreviousRouteService }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    LayoutModule.prototype.previousRouteService;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class MatPaginatorIntlPtBr extends MatPaginatorIntl {
    constructor() {
        super(...arguments);
        this.itemsPerPageLabel = 'Items por página';
        this.nextPageLabel = 'Próxima página';
        this.previousPageLabel = 'Página anterior';
        this.getRangeLabel = (/**
         * @param {?} page
         * @param {?} pageSize
         * @param {?} length
         * @return {?}
         */
        (page, pageSize, length) => {
            if (length === 0 || pageSize === 0) {
                return '0 de ' + length;
            }
            length = Math.max(length, 0);
            /** @type {?} */
            const startIndex = page * pageSize;
            /** @type {?} */
            const endIndex = startIndex < length ?
                Math.min(startIndex + pageSize, length) :
                startIndex + pageSize;
            return startIndex + 1 + ' - ' + endIndex + ' de  ' + length;
        });
    }
}
if (false) {
    /** @type {?} */
    MatPaginatorIntlPtBr.prototype.itemsPerPageLabel;
    /** @type {?} */
    MatPaginatorIntlPtBr.prototype.nextPageLabel;
    /** @type {?} */
    MatPaginatorIntlPtBr.prototype.previousPageLabel;
    /** @type {?} */
    MatPaginatorIntlPtBr.prototype.getRangeLabel;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class OfflineInterceptor {
    constructor() {
        this.onlineChanges$ = fromEvent(window, 'online').pipe(mapTo(true));
    }
    /**
     * @return {?}
     */
    get isOnline() {
        return navigator.onLine;
    }
    /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    intercept(req, next) {
        return next.handle(req)
            .pipe(retryWhen((/**
         * @param {?} errors
         * @return {?}
         */
        errors => {
            if (this.isOnline) {
                return errors.pipe(switchMap((/**
                 * @param {?} err
                 * @return {?}
                 */
                err => throwError(err))));
            }
            else if (req.method === 'GET') {
                // Não vamos fazer retry se não for GET (imutabilidade)
                return this.onlineChanges$;
            }
        })));
    }
}
OfflineInterceptor.decorators = [
    { type: Injectable }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    OfflineInterceptor.prototype.onlineChanges$;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class UikitModule {
    /**
     * @param {?} parentModule
     */
    constructor(parentModule) {
        if (parentModule) {
            throw new Error('UikitModule já foi carregado! Importe ele somente no AppModule.');
        }
    }
}
UikitModule.decorators = [
    { type: NgModule, args: [{
                entryComponents: [],
                declarations: [],
                imports: [
                // OperationsModule,
                ],
                exports: [
                    // OperationsModule,
                    LayoutModule,
                ],
                providers: [
                    { provide: HTTP_INTERCEPTORS, useClass: OfflineInterceptor, multi: true },
                    { provide: MatPaginatorIntl, useClass: MatPaginatorIntlPtBr }
                ],
            },] }
];
/** @nocollapse */
UikitModule.ctorParameters = () => [
    { type: UikitModule, decorators: [{ type: Optional }, { type: SkipSelf }] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class ToastAction {
}
if (false) {
    /** @type {?} */
    ToastAction.prototype.display;
    /** @type {?} */
    ToastAction.prototype.action;
}
/**
 * @record
 * @template T
 */
function Toaster() { }

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class UpdateModule {
    /**
     * @param {?} updateService
     * @param {?} updateInfoService
     */
    constructor(updateService, updateInfoService) {
        this.updateService = updateService;
        this.updateInfoService = updateInfoService;
    }
}
UpdateModule.decorators = [
    { type: NgModule, args: [{
                entryComponents: [UpdateComponent],
                declarations: [UpdateComponent],
                imports: [
                    CommonModule,
                    MatDialogModule,
                    MatProgressSpinnerModule
                ],
                exports: [UpdateComponent]
            },] }
];
/** @nocollapse */
UpdateModule.ctorParameters = () => [
    { type: UpdateService },
    { type: UpdateInfoService }
];
if (false) {
    /** @type {?} */
    UpdateModule.prototype.updateService;
    /** @type {?} */
    UpdateModule.prototype.updateInfoService;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class LogModule {
    /**
     * @param {?} logConsumersService
     * @param {?=} parentModule
     */
    constructor(logConsumersService, parentModule) {
        this.logConsumersService = logConsumersService;
        if (parentModule) {
            throw new Error('LogModule is already loaded. Import it in the AppModule only');
        }
    }
}
LogModule.decorators = [
    { type: NgModule, args: [{
                declarations: [],
                imports: [
                    CommonModule
                ],
                providers: [
                    LogService,
                    LogConsumersService
                ],
                exports: []
            },] }
];
/** @nocollapse */
LogModule.ctorParameters = () => [
    { type: LogConsumersService },
    { type: LogModule, decorators: [{ type: Optional }, { type: SkipSelf }] }
];
if (false) {
    /** @type {?} */
    LogModule.prototype.logConsumersService;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function SearchState() { }
let SearchStore = class SearchStore extends EntityStore {
    constructor() {
        super();
    }
    /**
     * @param {?} valueJaSetado
     * @param {?} groupItem
     * @return {?}
     */
    adicionarGrupoPesquisa(valueJaSetado, groupItem) {
        // valueJaSetado.push(groupItem);
        /** @type {?} */
        const item = [...valueJaSetado, ...groupItem];
        this.set(item);
    }
    /**
     * @param {?} groupItem
     * @return {?}
     */
    addGroupBase(groupItem) {
        this.set(groupItem);
    }
    /**
     * @param {?} title
     * @return {?}
     */
    removerGrupoPesquisa(title) {
        this.remove(title);
    }
};
SearchStore.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
SearchStore.ctorParameters = () => [];
/** @nocollapse */ SearchStore.ngInjectableDef = defineInjectable({ factory: function SearchStore_Factory() { return new SearchStore(); }, token: SearchStore, providedIn: "root" });
__decorate([
    transaction(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], SearchStore.prototype, "adicionarGrupoPesquisa", null);
__decorate([
    transaction(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], SearchStore.prototype, "addGroupBase", null);
__decorate([
    transaction(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], SearchStore.prototype, "removerGrupoPesquisa", null);
SearchStore = __decorate([
    StoreConfig({
        name: 'search'
    }),
    __metadata("design:paramtypes", [])
], SearchStore);

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SearchQuery extends QueryEntity {
    /**
     * @param {?} store
     */
    constructor(store) {
        super(store);
        this.store = store;
    }
}
SearchQuery.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
SearchQuery.ctorParameters = () => [
    { type: SearchStore }
];
/** @nocollapse */ SearchQuery.ngInjectableDef = defineInjectable({ factory: function SearchQuery_Factory() { return new SearchQuery(inject(SearchStore)); }, token: SearchQuery, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @protected
     */
    SearchQuery.prototype.store;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SearchKeepService {
    /**
     * @param {?} layoutService
     * @param {?} searchQuery
     * @param {?} searchStore
     */
    constructor(layoutService, searchQuery, searchStore) {
        this.layoutService = layoutService;
        this.searchQuery = searchQuery;
        this.searchStore = searchStore;
        this.searchGroups = this.layoutService.menuItemsFlattened$;
        this.searchGroupsAll = this.layoutService.menuItems$;
    }
    /*
    * Método responsável por adicionar um novo grupo na pesquisas.
    * */
    /**
     * @param {?} groups
     * @return {?}
     */
    addGroup(groups) {
        /** @type {?} */
        const itensGroups = this.searchQuery.getAll();
        if (head(itensGroups) !== undefined) {
            this.searchStore.adicionarGrupoPesquisa(itensGroups, groups);
        }
        else {
            this.searchStore.addGroupBase(groups);
        }
    }
    /*
        * Método responsável por remover um grupo da pesquisa.
        * */
    /**
     * @param {?} group
     * @return {?}
     */
    removeGroup(group) {
        this.searchStore.removerGrupoPesquisa(group.title);
    }
    /*
       * Método responsável por realizar a pesquisa dos grupo na pesquisa.
       * */
    /**
     * @param {?} term
     * @return {?}
     */
    searchGroup(term) {
        return this._filterGroup(term);
    }
    /**
     * @private
     * @param {?} value
     * @return {?}
     */
    _filterGroup(value) {
        /** @type {?} */
        const options = {
            keys: ['title', 'tags',
                'children.title', 'children.tags',
                'children.children.title', 'children.children.title.tags',
                'children.children.children.title', 'children.children.children.title.tags']
        };
        return this.searchGroupsAll.pipe(map((/**
         * @param {?} listMenuItem
         * @return {?}
         */
        listMenuItem => this.applyFilter(listMenuItem, value, options))), map((/**
         * @param {?} itens
         * @return {?}
         */
        itens => this.groupReturnFilter(itens))));
    }
    /**
     * @private
     * @param {?} listMenuItem
     * @param {?} value
     * @param {?} options
     * @return {?}
     */
    applyFilter(listMenuItem, value, options) {
        /** @type {?} */
        const fuse = new Fuse(listMenuItem, options);
        return fuse.search(value);
    }
    /**
     * @private
     * @param {?} itens
     * @return {?}
     */
    groupReturnFilter(itens) {
        /** @type {?} */
        const functionMapItem = (/**
         * @param {?} group
         * @return {?}
         */
        (group) => {
            return (/** @type {?} */ ({
                id: group.id ? group.id : guid(),
                title: group.title,
                icon: group.icon,
                link: group.link,
                tags: group.tags
            }));
        });
        /** @type {?} */
        const functionMapGroup = (/**
         * @param {?} group
         * @return {?}
         */
        (group) => {
            /** @type {?} */
            let groupMenu;
            if (group.link) {
                groupMenu = functionMapItem(group);
            }
            else {
                groupMenu = (/** @type {?} */ ({
                    id: group.id ? group.id : guid(),
                    title: group.title,
                    icon: group.icon,
                    isHeader: true,
                }));
                if (group.children && group.children.length > 1) {
                    /** @type {?} */
                    const itemFilhos = group.children
                        .filter((/**
                     * @param {?} b
                     * @return {?}
                     */
                    b => b !== group))
                        .map((/**
                     * @param {?} itemIt
                     * @return {?}
                     */
                    (itemIt) => {
                        if (itemIt.children && itemIt.children.length > 1) {
                            return functionMapGroup(itemIt);
                        }
                        else {
                            return functionMapItem(itemIt);
                        }
                    }));
                    if ((/** @type {?} */ (itemFilhos))) {
                        groupMenu.items = (/** @type {?} */ (itemFilhos));
                    }
                    else {
                        groupMenu.items = (/** @type {?} */ (itemFilhos));
                    }
                }
            }
            return groupMenu;
        });
        return (/** @type {?} */ ({
            id: guid(),
            title: 'Menu',
            isHeader: true,
            items: itens.map(functionMapGroup)
        }));
    }
}
SearchKeepService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
SearchKeepService.ctorParameters = () => [
    { type: LayoutService },
    { type: SearchQuery },
    { type: SearchStore }
];
if (false) {
    /**
     * @type {?}
     * @protected
     */
    SearchKeepService.prototype.searchGroups;
    /**
     * @type {?}
     * @protected
     */
    SearchKeepService.prototype.searchGroupsAll;
    /**
     * @type {?}
     * @protected
     */
    SearchKeepService.prototype.layoutService;
    /**
     * @type {?}
     * @private
     */
    SearchKeepService.prototype.searchQuery;
    /**
     * @type {?}
     * @private
     */
    SearchKeepService.prototype.searchStore;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @abstract
 */
class GuardService {
    /**
     * @param {?} router
     * @param {?} angularLocation
     * @param {?} platformLocation
     */
    constructor(router, angularLocation, platformLocation) {
        this.router = router;
        this.angularLocation = angularLocation;
        this.platformLocation = platformLocation;
    }
    /**
     * @protected
     * @return {?}
     */
    get routerStateSnapshot() {
        return this.router.routerState.snapshot;
    }
    /**
     * @protected
     * @return {?}
     */
    get activatedRouteSnapshot() {
        return this.router.routerState.root.snapshot;
    }
    // tslint:disable-next-line:max-line-length
    /**
     * @param {?} childRoute
     * @param {?} state
     * @return {?}
     */
    canActivateChild(childRoute, state) {
        return this.canActivate(childRoute, state);
    }
    /**
     * @return {?}
     */
    allow() {
        return true;
    }
    /**
     * @param {?} state
     * @param {?=} login
     * @param {?=} setCallback
     * @return {?}
     */
    deny(state, login = 'manual', setCallback = true) {
        if (setCallback) {
            /** @type {?} */
            const baseHref = this.platformLocation.getBaseHrefFromDOM();
            /** @type {?} */
            const origin = window.location.origin;
            /** @type {?} */
            const completeUrlToBaseHref = origin + baseHref;
            /** @type {?} */
            const angularRouteWithBaseHref = this.angularLocation.prepareExternalUrl(state.url);
            /** @type {?} */
            let angularRouteWithoutBaseHref = angularRouteWithBaseHref;
            if (angularRouteWithBaseHref.startsWith(baseHref)) {
                angularRouteWithoutBaseHref = angularRouteWithBaseHref.replace(baseHref, '');
            }
            /** @type {?} */
            const completeRoute = completeUrlToBaseHref + angularRouteWithoutBaseHref;
            console.log('Setando after callback como: ', completeRoute);
            localStorage.setItem('authentication-callback', completeRoute);
        }
        else {
            localStorage.removeItem('authentication-callback');
        }
        // const uri = this.angularLocation.prepareExternalUrl(state.url);
        // localStorage.setItem(location.host + ':callback', uri);
        /** @type {?} */
        let parametros = { queryParams: { login: login } };
        if (login == 'manual') {
            parametros = undefined;
        }
        this.router.navigate(['unauthorized'], parametros);
        return false;
    }
}
if (false) {
    /**
     * @type {?}
     * @protected
     */
    GuardService.prototype.router;
    /**
     * @type {?}
     * @protected
     */
    GuardService.prototype.angularLocation;
    /**
     * @type {?}
     * @protected
     */
    GuardService.prototype.platformLocation;
    /**
     * @abstract
     * @param {?} route
     * @param {?} state
     * @return {?}
     */
    GuardService.prototype.canActivate = function (route, state) { };
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class AuthenticationGuardService extends GuardService {
    /**
     * @param {?} identityService
     * @param {?} router
     * @param {?} angularLocation
     * @param {?} platformLocation
     */
    constructor(identityService, router, angularLocation, platformLocation) {
        super(router, angularLocation, platformLocation);
        this.identityService = identityService;
        this.router = router;
        this.angularLocation = angularLocation;
        this.platformLocation = platformLocation;
    }
    /**
     * @param {?} route
     * @param {?} state
     * @return {?}
     */
    canActivate(route, state) {
        console.debug('AuthenticationGuardService!', this.identityService.userValue);
        /** @type {?} */
        const mapped = this.identityService.user$
            .pipe(map((/**
         * @param {?} user
         * @return {?}
         */
        user => {
            console.debug('Verificando se usuário está autenticado!', user);
            if (user.authenticated) {
                return this.allow();
            }
            else {
                /** @type {?} */
                const data = route.data;
                /** @type {?} */
                let login = 'auto';
                if (data && data.login == 'manual') {
                    login = 'manual';
                }
                /** @type {?} */
                let setCallback = true;
                if (data && data.setCallback == false) {
                    setCallback = false;
                }
                return this.deny(state, login, setCallback);
            }
        })));
        // const ret = map.catch(() => {
        //   return this.deny();
        // });
        return mapped;
    }
}
AuthenticationGuardService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
AuthenticationGuardService.ctorParameters = () => [
    { type: UserService },
    { type: Router },
    { type: Location },
    { type: PlatformLocation }
];
if (false) {
    /**
     * @type {?}
     * @protected
     */
    AuthenticationGuardService.prototype.identityService;
    /**
     * @type {?}
     * @protected
     */
    AuthenticationGuardService.prototype.router;
    /**
     * @type {?}
     * @protected
     */
    AuthenticationGuardService.prototype.angularLocation;
    /**
     * @type {?}
     * @protected
     */
    AuthenticationGuardService.prototype.platformLocation;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @abstract
 */
class AuthorizationGuardService extends GuardService {
    /**
     * @param {?} authorizationService
     * @param {?} router
     * @param {?} angularLocation
     * @param {?} platformLocation
     */
    constructor(authorizationService, router, angularLocation, platformLocation) {
        super(router, angularLocation, platformLocation);
        this.authorizationService = authorizationService;
        this.router = router;
        this.angularLocation = angularLocation;
        this.platformLocation = platformLocation;
    }
}
AuthorizationGuardService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
AuthorizationGuardService.ctorParameters = () => [
    { type: AuthorizationService },
    { type: Router },
    { type: Location },
    { type: PlatformLocation }
];
if (false) {
    /**
     * @type {?}
     * @protected
     */
    AuthorizationGuardService.prototype.authorizationService;
    /**
     * @type {?}
     * @protected
     */
    AuthorizationGuardService.prototype.router;
    /**
     * @type {?}
     * @protected
     */
    AuthorizationGuardService.prototype.angularLocation;
    /**
     * @type {?}
     * @protected
     */
    AuthorizationGuardService.prototype.platformLocation;
    /**
     * @abstract
     * @param {?} route
     * @param {?} state
     * @return {?}
     */
    AuthorizationGuardService.prototype.canActivate = function (route, state) { };
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class RouteAuthorizationGuardService extends AuthorizationGuardService {
    /**
     * @param {?} route
     * @param {?} state
     * @return {?}
     */
    canActivate(route, state) {
        return __awaiter(this, void 0, void 0, /** @this {!RouteAuthorizationGuardService} */ function* () {
            /** @type {?} */
            const permissionToCheck = route.data && (/** @type {?} */ (route.data.authorize));
            if (permissionToCheck) {
                console.log(`Verificando autorização para a rota '${state.url}'...`);
                /** @type {?} */
                const hasPermission = yield this.authorizationService.authorize(permissionToCheck.action, permissionToCheck.resource);
                console.log(`Autorização para a rota '${state.url}' foi '${(hasPermission ? 'concedida' : 'negada')}'!`);
                if (!hasPermission) {
                    return this.deny(state);
                }
                else {
                    return this.allow();
                }
            }
            else {
                throw new Error('Sem permissão para verificar!');
            }
        });
    }
}
RouteAuthorizationGuardService.decorators = [
    { type: Injectable }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class OidcAuthHttpInterceptor {
    /**
     * @param {?} userService
     * @param {?} injector
     * @param {?=} logService
     */
    constructor(userService, injector, logService) {
        this.userService = userService;
        this.injector = injector;
        this.logService = logService;
    }
    /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    intercept(req, next) {
        /** @type {?} */
        let authReq = req;
        if (!req.headers.has('Authorization')) {
            if (this.userService && this.userService.userValue && this.userService.userValue.authenticated) {
                /** @type {?} */
                const token = ((/** @type {?} */ (this.userService.userValue))).access_token ||
                    ((/** @type {?} */ (this.userService.userValue))).id_token;
                /** @type {?} */
                let httpHeaders = req.headers.set('Authorization', `Bearer ${token}`);
                if (!req.headers.has('content-type')) {
                    httpHeaders = httpHeaders.set('content-type', 'application/json');
                }
                authReq = req.clone({
                    headers: httpHeaders
                });
                if (this.logService) {
                    this.logService.debug(`Não possui 'Authorization' header, foi adicionado.`);
                }
            }
        }
        /** @type {?} */
        const httpHandle = next
            .handle(authReq)
            .pipe(catchError((/**
         * @param {?} error
         * @return {?}
         */
        (error) => {
            if (error.status === 401) {
                /** @type {?} */
                const authenticationService = (/** @type {?} */ (this.injector.get(IAuthenticationServiceToken)));
                if (authenticationService.needRenewUser) {
                    return from(authenticationService.renewUser()).pipe(mergeMap((/**
                     * @return {?}
                     */
                    () => {
                        return this.intercept(req, next);
                    })));
                }
            }
            if (this.logService) {
                this.logService.error(JSON.stringify(error));
            }
            return throwError(error);
        })));
        return httpHandle;
    }
}
OidcAuthHttpInterceptor.decorators = [
    { type: Injectable }
];
/** @nocollapse */
OidcAuthHttpInterceptor.ctorParameters = () => [
    { type: UserService },
    { type: Injector },
    { type: LogService, decorators: [{ type: Optional }] }
];
if (false) {
    /**
     * @type {?}
     * @protected
     */
    OidcAuthHttpInterceptor.prototype.userService;
    /**
     * @type {?}
     * @private
     */
    OidcAuthHttpInterceptor.prototype.injector;
    /**
     * @type {?}
     * @protected
     */
    OidcAuthHttpInterceptor.prototype.logService;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class AuthModule {
}
AuthModule.decorators = [
    { type: NgModule, args: [{
                providers: [
                    AuthenticationGuardService,
                    AuthenticationService,
                    // ProviderUserTransformationService,
                    UserService,
                    AuthorizationService,
                    // AuthorizationGuardService,
                    RouteAuthorizationGuardService,
                    {
                        provide: HTTP_INTERCEPTORS,
                        useClass: OidcAuthHttpInterceptor,
                        multi: true,
                    },
                    AuthService
                ]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
// tslint:disable-next-line:max-line-length
/**
 * @abstract
 * @template TUserProvider, TUser, TLoginParam, TLogoutParam
 */
class ProviderAuthenticationService {
    constructor() {
        this.userProviderSubject = new BehaviorSubject(null);
        // protected userProvider$ = this.userProviderSubject.asObservable();
        this.userSubject = new BehaviorSubject(null);
        this.user$ = this.userSubject.asObservable();
        this.userProviderSubject.subscribe((/**
         * @param {?} userProvider
         * @return {?}
         */
        userProvider => {
            if (userProvider) {
                /** @type {?} */
                const user = this.transform(userProvider);
                this.loadUser(user);
            }
            else {
                this.loadUser(null);
            }
        }));
    }
    /**
     * @param {?=} param
     * @return {?}
     */
    logout(param) {
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            this.loadProviderUser(null);
            resolve();
        }));
    }
    /**
     * @private
     * @param {?} user
     * @return {?}
     */
    loadUser(user) {
        this.userSubject.next(user);
    }
    /**
     * @protected
     * @param {?} user
     * @return {?}
     */
    loadProviderUser(user) {
        this.userProviderSubject.next(user);
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    ProviderAuthenticationService.prototype.userProviderSubject;
    /**
     * @type {?}
     * @private
     */
    ProviderAuthenticationService.prototype.userSubject;
    /** @type {?} */
    ProviderAuthenticationService.prototype.user$;
    /**
     * @abstract
     * @param {?=} param
     * @return {?}
     */
    ProviderAuthenticationService.prototype.login = function (param) { };
    /**
     * @abstract
     * @protected
     * @param {?} providerUser
     * @return {?}
     */
    ProviderAuthenticationService.prototype.transform = function (providerUser) { };
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
// @dynamic
class OidcAuthenticationService extends ProviderAuthenticationService {
    /**
     * @param {?} authenticationSettings
     * @param {?} router
     * @param {?} activatedRoute
     * @param {?} platformLocation
     * @param {?} locationStrategy
     */
    constructor(authenticationSettings, router, activatedRoute, platformLocation, locationStrategy) {
        super();
        this.authenticationSettings = authenticationSettings;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.platformLocation = platformLocation;
        this.locationStrategy = locationStrategy;
        this.userManager = null;
        this.userValue = null;
        /** @type {?} */
        const userManagerArg = Object.assign({}, authenticationSettings, { userStore: new WebStorageStateStore({ store: window.localStorage }) });
        this.userManager = new UserManager(userManagerArg);
        this.userManager.events.addUserLoaded((/**
         * @param {?} user
         * @return {?}
         */
        (user) => {
            this.loadOidcUser(user);
        }));
        this.userManager.events.addAccessTokenExpired((/**
         * @param {?} event
         * @return {?}
         */
        (event) => {
            this.userManager.signinSilent().catch((/**
             * @param {?} error
             * @return {?}
             */
            (error) => {
                console.error(error);
                this.userManager.removeUser();
                super.logout();
            }));
        }));
        this.userManager.events.addSilentRenewError((/**
         * @param {?} error
         * @return {?}
         */
        (error) => {
            console.error(error);
            this.userManager.removeUser();
            super.logout();
        }));
        this.userManager.getUser().then((/**
         * @param {?} user
         * @return {?}
         */
        (user) => {
            this.loadOidcUser(user);
        }));
        if (this.authenticationSettings.automaticSilentRenew) {
            this.router.events
                .pipe(filter((/**
             * @param {?} event
             * @return {?}
             */
            (event) => event instanceof NavigationStart)))
                .pipe(filter((/**
             * @param {?} event
             * @return {?}
             */
            (event) => new RegExp(this.authenticationSettings.silent_redirect_uri).test(location.href))))
                .subscribe((/**
             * @param {?} event
             * @return {?}
             */
            (event) => {
                this.userManager.signinSilentCallback().catch((/**
                 * @param {?} err
                 * @return {?}
                 */
                (err) => {
                    console.error(err);
                    this.userManager.startSilentRenew();
                }));
            }));
        }
        this.router.events
            .pipe(filter((/**
         * @param {?} event
         * @return {?}
         */
        (event) => new RegExp(this.authenticationSettings.redirect_uri).test(location.href))))
            .subscribe((/**
         * @param {?} event
         * @return {?}
         */
        (event) => {
            this.userManager.signinRedirectCallback();
        }));
    }
    /**
     * @return {?}
     */
    get needRenewUser() {
        return this.userValue != null && this.userValue.expired === true;
    }
    /**
     * @return {?}
     */
    renewUser() {
        return this.userManager
            .signinSilent()
            .then((/**
         * @param {?} user
         * @return {?}
         */
        (user) => (/**
         * @return {?}
         */
        () => this.loadOidcUser(user))));
    }
    /**
     * @param {?} user
     * @return {?}
     */
    loadOidcUser(user) {
        this.userValue = user;
        this.loadProviderUser(user);
    }
    /**
     * @param {?} user
     * @return {?}
     */
    transform(user) {
        /** @type {?} */
        const destinationUser = (/** @type {?} */ ({}));
        /** @type {?} */
        let combinedUser = Object.assign(destinationUser, user);
        // O OIDC_Client usa a propriedade profile para colocar claims,
        // portanto devemos fazer o assign deste com o objetivo de manter
        // no usuário também as informações 'não mapeadas'
        if (user.profile) {
            combinedUser = Object.assign(combinedUser, user.profile);
        }
        combinedUser.authenticated = true;
        combinedUser.sub = user.profile.sub || null;
        combinedUser.name = user.profile.name || null;
        combinedUser.preferred_username = user.profile.preferred_username || null;
        combinedUser.given_name = user.profile.given_name || null;
        combinedUser.nickname = user.profile.nickname || null;
        combinedUser.email = user.profile.email || null;
        combinedUser.picture =
            (user.profile.picture && user.profile.picture[0]) || null;
        combinedUser.client_id = user.profile.client_id || null;
        combinedUser.access_token = user.access_token || null;
        combinedUser.id_token = user.id_token || null;
        combinedUser.roles = user.profile.roles || null;
        return combinedUser;
    }
    /**
     * @param {?=} args
     * @return {?}
     */
    login(args) {
        /** @type {?} */
        const baseHref = this.platformLocation.getBaseHrefFromDOM();
        /** @type {?} */
        const routeActiveIsUnauthorized = this.router.isActive(baseHref + 'unauthorized', false);
        if (this.authenticationSettings.login_mode === 'redirect' ||
            (this.authenticationSettings.login_mode === 'iframe' &&
                routeActiveIsUnauthorized)) {
            return this.userManager.signinRedirect(args).catch((/**
             * @param {?} error
             * @return {?}
             */
            (error) => {
                /** @type {?} */
                const erro = (/** @type {?} */ (error));
                if ('Network Error' === erro.message) {
                    /** @type {?} */
                    const message = 'Não foi possível se conectar ao servidor de autenticação!';
                    /** @type {?} */
                    const errorToThrow = new Error(message);
                    errorToThrow.stack = erro.stack;
                    errorToThrow.name = 'Erro de conexão';
                    throw errorToThrow;
                }
                else {
                    throw erro;
                }
                // console.error(error);
            }));
        }
        else if (this.authenticationSettings.login_mode === 'iframe' &&
            routeActiveIsUnauthorized === false) {
            return this.router.navigateByUrl(baseHref + 'login');
        }
        else if (this.authenticationSettings.login_mode === 'popup') {
            return this.userManager.signinPopup(args).catch((/**
             * @param {?} error
             * @return {?}
             */
            (error) => {
                /** @type {?} */
                const erro = (/** @type {?} */ (error));
                if ('Network Error' === erro.message) {
                    /** @type {?} */
                    const message = 'Não foi possível se conectar ao servidor de autenticação!';
                    /** @type {?} */
                    const errorToThrow = new Error(message);
                    errorToThrow.stack = erro.stack;
                    errorToThrow.name = 'Erro de conexão';
                    throw errorToThrow;
                }
                else {
                    throw erro;
                }
                // console.error(error);
            }));
        }
    }
    /**
     * @param {?=} ssoLogout
     * @return {?}
     */
    logout(ssoLogout = true) {
        if (ssoLogout) {
            /** @type {?} */
            const isOnline = navigator.onLine;
            if (isOnline) {
                return this.userManager.createSignoutRequest().then((/**
                 * @param {?} req
                 * @return {?}
                 */
                (req) => {
                    console.log('Request logout: ', req);
                    /** @type {?} */
                    const ifrm = document.createElement('iframe');
                    ifrm.setAttribute('src', req.url);
                    ifrm.style.width = '640px';
                    ifrm.style.height = '480px';
                    ifrm.style.display = 'none';
                    ifrm.onload = (/**
                     * @return {?}
                     */
                    () => {
                        console.log('IFrame do logout terminou de carregar!');
                        super.logout().then((/**
                         * @return {?}
                         */
                        () => {
                            this.userManager.removeUser();
                            ifrm.parentElement.removeChild(ifrm);
                        }));
                    });
                    document.body.appendChild(ifrm);
                }));
                // return this.userManager.signoutRedirect()
                //   .then(() => super.logout())
                //   .catch((error) => {
                //     console.error(error);
                //     super.logout();
                //     throw error;
                //   });
            }
            else {
                /** @type {?} */
                const message = 'Não foi possível se conectar ao servidor de autenticação!';
                /** @type {?} */
                const errorToThrow = new Error(message);
                errorToThrow.name = 'Erro de conexão';
                throw errorToThrow;
            }
        }
        else {
            super.logout();
        }
    }
}
OidcAuthenticationService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
OidcAuthenticationService.ctorParameters = () => [
    { type: undefined },
    { type: Router },
    { type: ActivatedRoute },
    { type: PlatformLocation },
    { type: LocationStrategy }
];
if (false) {
    /**
     * @type {?}
     * @protected
     */
    OidcAuthenticationService.prototype.userManager;
    /** @type {?} */
    OidcAuthenticationService.prototype.userValue;
    /** @type {?} */
    OidcAuthenticationService.prototype.authenticationSettings;
    /**
     * @type {?}
     * @protected
     */
    OidcAuthenticationService.prototype.router;
    /**
     * @type {?}
     * @protected
     */
    OidcAuthenticationService.prototype.activatedRoute;
    /**
     * @type {?}
     * @protected
     */
    OidcAuthenticationService.prototype.platformLocation;
    /**
     * @type {?}
     * @protected
     */
    OidcAuthenticationService.prototype.locationStrategy;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CallbackComponent {
    /**
     * @param {?} authenticationService
     * @param {?} userService
     */
    constructor(authenticationService, userService) {
        this.authenticationService = authenticationService;
        this.userService = userService;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
CallbackComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-callback',
                template: ``,
                encapsulation: ViewEncapsulation.ShadowDom,
                changeDetection: ChangeDetectionStrategy.OnPush
            }] }
];
/** @nocollapse */
CallbackComponent.ctorParameters = () => [
    { type: AuthenticationService },
    { type: UserService }
];
if (false) {
    /**
     * @type {?}
     * @protected
     */
    CallbackComponent.prototype.authenticationService;
    /**
     * @type {?}
     * @protected
     */
    CallbackComponent.prototype.userService;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SilentRefreshComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
SilentRefreshComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-silentrefresh',
                template: ``,
                encapsulation: ViewEncapsulation.ShadowDom,
                changeDetection: ChangeDetectionStrategy.OnPush
            }] }
];
/** @nocollapse */
SilentRefreshComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class ProtectedRouteComponent {
    /**
     * @param {?} userService
     * @param {?} router
     * @param {?} platformLocation
     * @param {?} locationStrategy
     */
    constructor(userService, router, platformLocation, locationStrategy) {
        this.userService = userService;
        this.router = router;
        this.platformLocation = platformLocation;
        this.locationStrategy = locationStrategy;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
ProtectedRouteComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-protected-route',
                template: ``
            }] }
];
/** @nocollapse */
ProtectedRouteComponent.ctorParameters = () => [
    { type: UserService },
    { type: Router },
    { type: PlatformLocation },
    { type: LocationStrategy }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    ProtectedRouteComponent.prototype.userService;
    /**
     * @type {?}
     * @protected
     */
    ProtectedRouteComponent.prototype.router;
    /**
     * @type {?}
     * @private
     */
    ProtectedRouteComponent.prototype.platformLocation;
    /**
     * @type {?}
     * @private
     */
    ProtectedRouteComponent.prototype.locationStrategy;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class LoginIframeComponent {
    /**
     * @param {?} sanitizer
     * @param {?} applicationRef
     * @param {?} toastService
     * @param {?} providerAuthenticationService
     * @param {?} userService
     * @param {?} router
     * @param {?} platformLocation
     * @param {?} locationStrategy
     */
    constructor(sanitizer, applicationRef, toastService, providerAuthenticationService, userService, router, platformLocation, locationStrategy) {
        this.sanitizer = sanitizer;
        this.applicationRef = applicationRef;
        this.toastService = toastService;
        this.providerAuthenticationService = providerAuthenticationService;
        this.userService = userService;
        this.router = router;
        this.platformLocation = platformLocation;
        this.locationStrategy = locationStrategy;
        this.contador = 0;
        this.isLoading$ = new BehaviorSubject(true);
        this.subscription = new Subscription();
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.url = this.sanitizer.bypassSecurityTrustResourceUrl('./protected-route');
        this.toastService.loading(this.isLoading$);
    }
    /**
     * @param {?} myIframe
     * @return {?}
     */
    onLoad(myIframe) {
        this.isLoading$.next(false);
        ((/** @type {?} */ (myIframe))).onloadstart = (/**
         * @return {?}
         */
        () => {
            // console.log('start loading iframe');
            this.isLoading$.next(true);
        });
        // console.log(`A página do IFrame terminou de carregar!`);
        /** @type {?} */
        const oidcProviderAuthenticationService = (/** @type {?} */ ((/** @type {?} */ (this.providerAuthenticationService))));
        /** @type {?} */
        const userManagerArg = Object.assign({}, oidcProviderAuthenticationService.authenticationSettings, { userStore: new WebStorageStateStore({ store: window.localStorage }) });
        /** @type {?} */
        const userManager = new UserManager(userManagerArg);
        this.subscription.add(timer(0, 100).subscribe((/**
         * @return {?}
         */
        () => {
            userManager.getUser().then((/**
             * @param {?} user
             * @return {?}
             */
            user => {
                // console.log(`Usuário encontrado: `, user);
                if (user) {
                    oidcProviderAuthenticationService.loadOidcUser(user);
                    // console.log(`Setando usuário encontrado: `, user);
                    // const usuario = oidcProviderAuthenticationService.transform(user);
                    // this.userService.load(usuario);
                    // this.goToLastUri();
                    // oidcProviderAuthenticationService.user$.subscribe(usuario => {
                    //   console.log(`Carregando usuário encontrado: `, user);
                    //   this.userService.load(usuario);
                    // });
                }
            }));
        })));
        // if(this.oidcAuthenticationService.user$.subscribe(a => a.authenticated))
        // this.oidcAuthenticationService.renewUser().then(() => this.applicationRef.tick());
        // const currentIframePage = null;
        // try {
        //   const currentIframePage = (myIframe as HTMLIFrameElement).contentWindow.location.href;
        // } catch (error) {
        //   this.toastService.error('Não foi possivel obter a informação da página do IFrame!')
        // }
        // console.log(`A página '${currentIframePage}' do IFrame terminou de carregar!`);
        // // this.logService && this.logService.debug();
        // // Caso seja callback, então deve
        // if (currentIframePage.indexOf('/callback') !== -1) {
        //   // Atualizar a aplicação
        //   // this.logService && this.logService.debug(`Atualizando a aplicação!`);
        //   console.log('Atualizando a aplicação!');
        //   this.applicationRef.tick();
        // }
    }
    // TODO: Este método está repetido, devo criar um service para ele (AuthenticationService)
    /**
     * @protected
     * @return {?}
     */
    goToLastUri() {
        /** @type {?} */
        const lastUri = localStorage.getItem('authentication-callback');
        /** @type {?} */
        const baseHref = this.platformLocation.getBaseHrefFromDOM();
        /** @type {?} */
        const origin = window.location.origin;
        /** @type {?} */
        let hashStrategy = '';
        if (this.locationStrategy instanceof HashLocationStrategy) {
            hashStrategy = '#';
        }
        /** @type {?} */
        const completeUrlToBaseHref = origin + baseHref + hashStrategy;
        if (lastUri && (lastUri.indexOf('protected-route') == -1)) {
            /** @type {?} */
            const uriToNavigate = lastUri.replace(completeUrlToBaseHref, '');
            // let uriToNavigate = lastUri;
            // if (uriToNavigate.startsWith(baseHref)) {
            //   uriToNavigate = uriToNavigate.replace(baseHref, '');
            // }
            localStorage.removeItem('authentication-callback');
            console.debug('Navegando para página: ', lastUri);
            this.router.navigateByUrl(uriToNavigate);
        }
        else {
            this.router.navigateByUrl('');
        }
    }
}
LoginIframeComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-login-iframe',
                template: `
  <div class="row-container">
    <iframe [hidden]="(isLoading$ | async)" #myIframe [src]="url ? url : null" frameBorder="0" (load)="url ? onLoad(myIframe) : null" class="second-row"></iframe>
  </div>
  `,
                styles: ['.iframe { height: 100%; width: 100% border: none; }',
                    '.row-container {display: flex; width: 100%; height: 100%; flex-direction: column; overflow: hidden;}',
                    '.first-row {background-color: lime; }',
                    '.second-row { flex-grow: 1; border: none; margin: 0; padding: 0; }']
            }] }
];
/** @nocollapse */
LoginIframeComponent.ctorParameters = () => [
    { type: DomSanitizer },
    { type: ApplicationRef },
    { type: ToastService },
    { type: undefined, decorators: [{ type: Inject, args: [IAuthenticationServiceToken,] }] },
    { type: UserService },
    { type: Router },
    { type: PlatformLocation },
    { type: LocationStrategy }
];
if (false) {
    /** @type {?} */
    LoginIframeComponent.prototype.url;
    /** @type {?} */
    LoginIframeComponent.prototype.contador;
    /** @type {?} */
    LoginIframeComponent.prototype.isLoading$;
    /**
     * @type {?}
     * @private
     */
    LoginIframeComponent.prototype.subscription;
    /**
     * @type {?}
     * @private
     */
    LoginIframeComponent.prototype.sanitizer;
    /**
     * @type {?}
     * @protected
     */
    LoginIframeComponent.prototype.applicationRef;
    /**
     * @type {?}
     * @protected
     */
    LoginIframeComponent.prototype.toastService;
    /**
     * @type {?}
     * @private
     */
    LoginIframeComponent.prototype.providerAuthenticationService;
    /**
     * @type {?}
     * @protected
     */
    LoginIframeComponent.prototype.userService;
    /**
     * @type {?}
     * @protected
     */
    LoginIframeComponent.prototype.router;
    /**
     * @type {?}
     * @private
     */
    LoginIframeComponent.prototype.platformLocation;
    /**
     * @type {?}
     * @private
     */
    LoginIframeComponent.prototype.locationStrategy;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
const ɵ0 = { allowedTypes: ['noshellnobreadcrumb'], defaultType: 'noshellnobreadcrumb' }, ɵ1 = { allowedTypes: ['noshellnobreadcrumb'], defaultType: 'noshellnobreadcrumb' }, ɵ2 = { login: 'auto', setCallback: false, allowedTypes: ['noshellnobreadcrumb'], defaultType: 'noshellnobreadcrumb' };
/** @type {?} */
const routes = [
    { path: 'callback', component: CallbackComponent, data: ɵ0 },
    { path: 'silentrefresh', component: SilentRefreshComponent, data: ɵ1 },
    { path: 'login', component: LoginIframeComponent },
    { path: 'protected-route', component: ProtectedRouteComponent, canActivate: [AuthenticationGuardService],
        data: ɵ2
    },
];
class OidcRoutingModule {
}
OidcRoutingModule.decorators = [
    { type: NgModule, args: [{
                imports: [RouterModule.forChild(routes)],
                exports: [RouterModule],
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function OpenIDConnectSettings() { }
if (false) {
    /** @type {?} */
    OpenIDConnectSettings.prototype.client_uri;
    /**
     * Modo de como efetuar o login. (Padrão é 'iframe' quando não informado)
     * @type {?|undefined}
     */
    OpenIDConnectSettings.prototype.login_mode;
}
/** @type {?} */
const OIDC_CONFIG = new InjectionToken(`OIDC_CONFIG`);
class OidcConfigDepHolder {
    /**
     * @param {?} openIDConnectSettings
     */
    constructor(openIDConnectSettings) {
        this.openIDConnectSettings = openIDConnectSettings;
    }
}
OidcConfigDepHolder.decorators = [
    { type: Injectable }
];
/** @nocollapse */
OidcConfigDepHolder.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [OIDC_CONFIG,] }] }
];
if (false) {
    /** @type {?} */
    OidcConfigDepHolder.prototype.openIDConnectSettings;
}
// @Injectable()
// export class AuthManagerDepHolder {
//   constructor(
//     @Inject(IAuthenticationManagerToken) @Optional() public authenticationManager: IAuthenticationManager
//   ) {
//   }
// }
/**
 * @param {?} oidcConfigDepHolder
 * @param {?} router
 * @param {?} platformLocation
 * @param {?} activatedRoute
 * @param {?} locationStrategy
 * @return {?}
 */
function InitOidcAuthenticationService(oidcConfigDepHolder, router, platformLocation, activatedRoute, locationStrategy) {
    /** @type {?} */
    const clientSettings = oidcConfigDepHolder.openIDConnectSettings;
    clientSettings.login_mode = clientSettings.login_mode ? clientSettings.login_mode : 'iframe';
    /** @type {?} */
    const baseHref = platformLocation.getBaseHrefFromDOM();
    /** @type {?} */
    const origin = window.location.origin;
    /** @type {?} */
    const completeUrlToBaseHref = origin + baseHref;
    /** @type {?} */
    const baseClientUri = clientSettings.client_uri || completeUrlToBaseHref;
    /** @type {?} */
    const defaults = {
        redirect_uri: `${baseClientUri}callback`,
        post_logout_redirect_uri: `${baseClientUri}`,
        response_type: `id_token token`,
        scope: `openid profile email roles offline_access`,
        silent_redirect_uri: `${baseClientUri}silentrefresh`,
        automaticSilentRenew: true,
        filterProtocolClaims: false,
        loadUserInfo: true
    };
    /** @type {?} */
    const finalSettings = Object.assign(defaults, clientSettings);
    return new OidcAuthenticationService(finalSettings, router, activatedRoute, platformLocation, locationStrategy);
}
class OidcAuthModule {
}
OidcAuthModule.decorators = [
    { type: NgModule, args: [{
                declarations: [CallbackComponent, SilentRefreshComponent, ProtectedRouteComponent, LoginIframeComponent],
                imports: [OidcRoutingModule, CommonModule],
                providers: [
                    OidcConfigDepHolder,
                    // { provide: APP_INITIALIZER, useFactory: initializeUser, deps: [], multi: true },
                    {
                        // tslint:disable-next-line:object-literal-shorthand
                        provide: IAuthenticationServiceToken,
                        useFactory: InitOidcAuthenticationService,
                        deps: [
                            OidcConfigDepHolder,
                            // ActivatedRoute,
                            Router,
                            PlatformLocation,
                            ActivatedRoute,
                            LocationStrategy,
                        ]
                    },
                ]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const AuthorizationConfigToken = new InjectionToken('AuthorizationConfigToken');
/**
 * @record
 */
function AuthorizationConfig() { }
if (false) {
    /** @type {?} */
    AuthorizationConfig.prototype.clientId;
    /**
     * Necessário para o UmaAuthorization ('User Managed Access')
     * @type {?|undefined}
     */
    AuthorizationConfig.prototype.umaConfig;
    /**
     * Necessário para o XacmlAuthorization ('eXtensible Access Control Markup Language')
     * @type {?|undefined}
     */
    AuthorizationConfig.prototype.xacmlConfig;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class UserAuthorizationManager {
    /**
     * @param {?} userService
     */
    constructor(userService) {
        this.userService = userService;
    }
    /**
     * @param {?} action
     * @param {?} resource
     * @return {?}
     */
    authorize(action, resource) {
        return __awaiter(this, void 0, void 0, /** @this {!UserAuthorizationManager} */ function* () {
            /** @type {?} */
            const userWithAuthorizations = (/** @type {?} */ (this.userService.userValue));
            if (userWithAuthorizations.authenticated) {
                if (userWithAuthorizations.authorizations != null && userWithAuthorizations.authorizations.length > 0) {
                    /** @type {?} */
                    const usuarioPossuiPermissao = userWithAuthorizations.authorizations.some((/**
                     * @param {?} permissao
                     * @return {?}
                     */
                    permissao => {
                        /** @type {?} */
                        const ehMesmoRecurso = permissao.resource === resource;
                        /** @type {?} */
                        const ehMesmaAcao = permissao.action.includes(action);
                        /** @type {?} */
                        const possuiPermissao = ehMesmoRecurso && ehMesmaAcao;
                        return possuiPermissao;
                    }));
                    if (usuarioPossuiPermissao) {
                        return true;
                    }
                }
            }
            return false;
        });
    }
}
UserAuthorizationManager.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
UserAuthorizationManager.ctorParameters = () => [
    { type: UserService }
];
/** @nocollapse */ UserAuthorizationManager.ngInjectableDef = defineInjectable({ factory: function UserAuthorizationManager_Factory() { return new UserAuthorizationManager(inject(UserService)); }, token: UserAuthorizationManager, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @protected
     */
    UserAuthorizationManager.prototype.userService;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class UmaAuthorizationManager {
    /**
     * @param {?} httpClient
     * @param {?} authorizationConfig
     */
    constructor(httpClient, authorizationConfig) {
        this.httpClient = httpClient;
        this.authorizationConfig = authorizationConfig;
    }
    /**
     * @param {?} action
     * @param {?} resource
     * @return {?}
     */
    authorize(action, resource) {
        return __awaiter(this, void 0, void 0, /** @this {!UmaAuthorizationManager} */ function* () {
            // WSO2 - ${environment.settings.authentication.authority}api/identity/oauth2/uma/permission/v1.0/permission
            // KEYCLOAK - ${environment.settings.authentication.authority}authz/protection/permission
            return this.httpClient.post(`${this.authorizationConfig.umaConfig.permissionEndpoint}`, [
                {
                    resource_id: resource,
                    resource_scopes: [action]
                }
            ]).pipe(switchMap((/**
             * @param {?} result
             * @return {?}
             */
            (result) => {
                // WSO2 - `${environment.settings.authentication.authority}oauth2/token`,
                // KEYCLOAK - `${environment.settings.authentication.authority}protocol/openid-connect/token`,
                return this.httpClient.post(`${this.authorizationConfig.umaConfig.tokenEndpoint}`, new HttpParams()
                    .set('grant_type', 'urn:ietf:params:oauth:grant-type:uma-ticket')
                    // .set('audience', client)
                    .set('response_mode', 'decision')
                    .set('ticket', result.ticket)
                    .toString(), {
                    headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
                });
            }))).toPromise().then((/**
             * @param {?} result
             * @return {?}
             */
            (result) => {
                return result.result;
            }));
        });
    }
}
UmaAuthorizationManager.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
UmaAuthorizationManager.ctorParameters = () => [
    { type: HttpClient },
    { type: undefined, decorators: [{ type: Inject, args: [AuthorizationConfigToken,] }] }
];
/** @nocollapse */ UmaAuthorizationManager.ngInjectableDef = defineInjectable({ factory: function UmaAuthorizationManager_Factory() { return new UmaAuthorizationManager(inject(HttpClient), inject(AuthorizationConfigToken)); }, token: UmaAuthorizationManager, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @protected
     */
    UmaAuthorizationManager.prototype.httpClient;
    /**
     * @type {?}
     * @protected
     */
    UmaAuthorizationManager.prototype.authorizationConfig;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class XacmlAuthorizationManager {
    /**
     * @param {?} userService
     * @param {?} httpClient
     * @param {?} authorizationConfig
     */
    constructor(userService, httpClient, authorizationConfig) {
        this.userService = userService;
        this.httpClient = httpClient;
        this.authorizationConfig = authorizationConfig;
    }
    /**
     * @param {?} action
     * @param {?} resource
     * @return {?}
     */
    authorize(action, resource) {
        return __awaiter(this, void 0, void 0, /** @this {!XacmlAuthorizationManager} */ function* () {
            // https://docs.wso2.com/display/IS570/Entitlement+with+REST+APIs
            // WSO2 - ${environment.settings.authentication.authority}api/identity/entitlement/decision/pdp
            return this.httpClient
                .post(`${this.authorizationConfig.xacmlConfig.policyDecisionEndpoint}`, {
                Request: {
                    AccessSubject: {
                        Attribute: [
                            {
                                AttributeId: 'subject-id',
                                Value: `${this.userService.userValue.sub}`,
                                DataType: 'string',
                                IncludeInResult: true
                            }
                        ]
                    },
                    Resource: {
                        Attribute: [
                            {
                                AttributeId: 'resource-id',
                                Value: resource,
                                DataType: 'string',
                                IncludeInResult: true
                            }
                        ]
                    },
                    Action: {
                        Attribute: [
                            {
                                AttributeId: 'action-id',
                                Value: action,
                                DataType: 'string',
                                IncludeInResult: true
                            }
                        ]
                    }
                }
            })
                .toPromise()
                .then((/**
             * @param {?} result
             * @return {?}
             */
            (result) => {
                if (result && result.Response && result.Response[0]) {
                    /** @type {?} */
                    const resposta = result.Response[0];
                    if (resposta.Decision === 'Permit') {
                        return true;
                    }
                }
                return false;
            }));
        });
    }
}
XacmlAuthorizationManager.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
XacmlAuthorizationManager.ctorParameters = () => [
    { type: UserService },
    { type: HttpClient },
    { type: undefined, decorators: [{ type: Inject, args: [AuthorizationConfigToken,] }] }
];
/** @nocollapse */ XacmlAuthorizationManager.ngInjectableDef = defineInjectable({ factory: function XacmlAuthorizationManager_Factory() { return new XacmlAuthorizationManager(inject(UserService), inject(HttpClient), inject(AuthorizationConfigToken)); }, token: XacmlAuthorizationManager, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @protected
     */
    XacmlAuthorizationManager.prototype.userService;
    /**
     * @type {?}
     * @protected
     */
    XacmlAuthorizationManager.prototype.httpClient;
    /**
     * @type {?}
     * @protected
     */
    XacmlAuthorizationManager.prototype.authorizationConfig;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function User() { }
if (false) {
    /**
     * Identificador único do usuário naquele provedor de identidade
     * @type {?}
     */
    User.prototype.sub;
    /**
     * Endereço do serviço provedor de identidade
     * @type {?|undefined}
     */
    User.prototype.iss;
    /**
     * Informa se o usuário foi autenticado
     * @type {?}
     */
    User.prototype.authenticated;
    /**
     * Url da imagem do usuário
     * @type {?|undefined}
     */
    User.prototype.picture;
    /**
     * Nome do usuário
     * @type {?}
     */
    User.prototype.name;
    /**
     * Nome escolhido pelo usuario no provedor de identidade
     * @type {?|undefined}
     */
    User.prototype.preferred_username;
    /**
     * Nome de exibição do usuário
     * @type {?|undefined}
     */
    User.prototype.given_name;
    /**
     * Apelido do usuário
     * @type {?|undefined}
     */
    User.prototype.nickname;
    /**
     * Email principal do usuário
     * @type {?}
     */
    User.prototype.email;
    /**
     * CPF do usuário
     * @type {?|undefined}
     */
    User.prototype.cpf;
    /**
     * Perfis do usuário para o sistema
     * @type {?|undefined}
     */
    User.prototype.roles;
}
/**
 * @record
 */
function Role() { }
if (false) {
    /**
     * Identificador do perfil do usuário no sistema
     * @type {?|undefined}
     */
    Role.prototype.id;
    /**
     * Nome do perfil do usuário no sistema
     * @type {?}
     */
    Role.prototype.name;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class KeycloakAuthenticationManager {
    /**
     * @param {?} httpClient
     * @param {?} authorizationConfig
     */
    constructor(httpClient, authorizationConfig) {
        this.httpClient = httpClient;
        this.authorizationConfig = authorizationConfig;
    }
    /**
     * @param {?} user
     * @return {?}
     */
    transform(user) {
        return __awaiter(this, void 0, void 0, /** @this {!KeycloakAuthenticationManager} */ function* () {
            // Faço request para buscar todas as permissões do usuário (resource owner) para um client específico
            // Neste caso uso o padrão User Managed Access para retornar as informações
            return this.httpClient
                .post(`${this.authorizationConfig.umaConfig.tokenEndpoint}`, new HttpParams()
                .set('grant_type', 'urn:ietf:params:oauth:grant-type:uma-ticket')
                .set('audience', `${this.authorizationConfig.clientId}`)
                .toString(), {
                headers: new HttpHeaders()
                    .set('Content-Type', 'application/x-www-form-urlencoded')
                    .set('Authorization', `Bearer ${user.access_token}`)
            })
                // tslint:disable-next-line:max-line-length
                .pipe(map((/**
             * @param {?} result
             * @return {?}
             */
            (result) => {
                // Devo então buscar o payload do access_token e incluir (substituindo o access_token anterior) as informações de autorização
                /** @type {?} */
                const accessToken = result.access_token.split('.')[1].replace('-', '+').replace('_', '/');
                /** @type {?} */
                const accessTokenPayload1 = JSON.parse(atob(accessToken));
                /** @type {?} */
                const usuarioFinal1 = (/** @type {?} */ (Object.assign(user, accessTokenPayload1)));
                usuarioFinal1.access_token = result.access_token;
                ((/** @type {?} */ (usuarioFinal1))).access_token = result.access_token;
                return (/** @type {?} */ (usuarioFinal1));
            })))
                // tslint:disable-next-line:max-line-length
                .pipe(map((/**
             * @param {?} user
             * @return {?}
             */
            (user) => {
                // Devo agora retornar as informações do usuário no formado esperado (OpenIDConnectUser & UserWithAuthorizations)
                /** @type {?} */
                let userToReturn = (/** @type {?} */ (JSON.parse(JSON.stringify(user))));
                delete userToReturn['authorization'];
                userToReturn.authorizations = user.authorization.permissions.map((/**
                 * @param {?} permission
                 * @return {?}
                 */
                permission => {
                    return { action: permission.scopes, resource: permission.rsname, resourceId: permission.rsid };
                }));
                return userToReturn;
            })))
                .toPromise();
        });
    }
}
KeycloakAuthenticationManager.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
KeycloakAuthenticationManager.ctorParameters = () => [
    { type: HttpClient },
    { type: undefined, decorators: [{ type: Inject, args: [AuthorizationConfigToken,] }] }
];
/** @nocollapse */ KeycloakAuthenticationManager.ngInjectableDef = defineInjectable({ factory: function KeycloakAuthenticationManager_Factory() { return new KeycloakAuthenticationManager(inject(HttpClient), inject(AuthorizationConfigToken)); }, token: KeycloakAuthenticationManager, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @protected
     */
    KeycloakAuthenticationManager.prototype.httpClient;
    /**
     * @type {?}
     * @protected
     */
    KeycloakAuthenticationManager.prototype.authorizationConfig;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const groupBy = (/**
 * @template T
 * @param {?} items
 * @param {?} keyFunction
 * @return {?}
 */
function (items, keyFunction) {
    /** @type {?} */
    const groups = {};
    items.forEach((/**
     * @param {?} el
     * @return {?}
     */
    function (el) {
        /** @type {?} */
        var key = keyFunction(el);
        if (key in groups == false) {
            groups[key] = [];
        }
        groups[key].push(el);
    }));
    return Object.keys(groups).map((/**
     * @param {?} key
     * @return {?}
     */
    function (key) {
        return {
            key: key,
            values: (/** @type {?} */ (groups[key]))
        };
    }));
});
const ɵ0$1 = groupBy;
/**
 * @record
 */
function EntitledAttributesDTOs() { }
if (false) {
    /** @type {?} */
    EntitledAttributesDTOs.prototype.resourceName;
    /** @type {?} */
    EntitledAttributesDTOs.prototype.action;
    /** @type {?} */
    EntitledAttributesDTOs.prototype.environment;
    /** @type {?} */
    EntitledAttributesDTOs.prototype.allActions;
    /** @type {?} */
    EntitledAttributesDTOs.prototype.allResources;
    /** @type {?} */
    EntitledAttributesDTOs.prototype.attributeDTOs;
}
/**
 * @record
 */
function IResultEntitlementAllRequest() { }
if (false) {
    /** @type {?} */
    IResultEntitlementAllRequest.prototype.entitledResultSetDTO;
}
class Wso2AuthenticationManager {
    /**
     * @param {?} httpClient
     * @param {?} authorizationConfig
     */
    constructor(httpClient, authorizationConfig) {
        this.httpClient = httpClient;
        this.authorizationConfig = authorizationConfig;
    }
    /**
     * @param {?} user
     * @return {?}
     */
    transform(user) {
        return __awaiter(this, void 0, void 0, /** @this {!Wso2AuthenticationManager} */ function* () {
            // Faço request para buscar todas as permissões do usuário (resource owner) para um client específico
            // Neste caso uso o padrão User Managed Access para retornar as informações
            // https://localhost:9443/api/identity/entitlement/decision/entitlements-all
            return this.httpClient
                .post(`${this.authorizationConfig.xacmlConfig.entitlementsAllEndpoint}`, {
                identifier: '',
                givenAttributes: []
            }, {
                headers: new HttpHeaders()
                    .set('Content-Type', 'application/json')
                    .set('Accept', 'application/json')
                    .set('Authorization', `Bearer ${user.access_token}`)
            })
                .pipe(map((/**
             * @param {?} result
             * @return {?}
             */
            (result) => result.entitledResultSetDTO.entitledAttributesDTOs)))
                .pipe(map((/**
             * @param {?} result
             * @return {?}
             */
            (result) => {
                /** @type {?} */
                const authorizations = groupBy(result, (
                // tslint:disable-next-line:max-line-length
                /**
                 * @param {?} item
                 * @return {?}
                 */
                (item) => item.attributeDTOs.find((/**
                 * @param {?} attr
                 * @return {?}
                 */
                attr => attr.attributeId === 'urn:oasis:names:tc:xacml:1.0:resource:resource-id')).attributeValue));
                /** @type {?} */
                const permissions = authorizations.map((/**
                 * @param {?} perm
                 * @return {?}
                 */
                perm => ({
                    resource: perm.key,
                    action: perm.values.map((/**
                     * @param {?} r
                     * @return {?}
                     */
                    r => r.attributeDTOs.find((/**
                     * @param {?} a
                     * @return {?}
                     */
                    a => a.attributeId === 'urn:oasis:names:tc:xacml:1.0:action:action-id')).attributeValue))
                })));
                /** @type {?} */
                const returnUser = (/** @type {?} */ (Object.assign({}, user)));
                returnUser.authorizations = permissions;
                return returnUser;
            })))
                .toPromise();
        });
    }
}
Wso2AuthenticationManager.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
Wso2AuthenticationManager.ctorParameters = () => [
    { type: HttpClient },
    { type: undefined, decorators: [{ type: Inject, args: [AuthorizationConfigToken,] }] }
];
/** @nocollapse */ Wso2AuthenticationManager.ngInjectableDef = defineInjectable({ factory: function Wso2AuthenticationManager_Factory() { return new Wso2AuthenticationManager(inject(HttpClient), inject(AuthorizationConfigToken)); }, token: Wso2AuthenticationManager, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @protected
     */
    Wso2AuthenticationManager.prototype.httpClient;
    /**
     * @type {?}
     * @protected
     */
    Wso2AuthenticationManager.prototype.authorizationConfig;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NotFoundComponent {
    /**
     * @param {?} ngLocation
     */
    constructor(ngLocation) {
        this.ngLocation = ngLocation;
        this.imageSrc = require('@cnj/uikit/lib/theme/uikit/images/error_404.png');
    }
    /**
     * @return {?}
     */
    ngOnInit() { }
    /**
     * @return {?}
     */
    goBack() {
        this.ngLocation.back();
    }
}
NotFoundComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-not-found',
                template: "<div class=\"container-fluid\">\n  <div class=\"row\">\n    <div class=\"col\">\n      <div class=\"error\">\n        <img src=\"{{ imageSrc }}\" />\n        <h3><b>Oops!</b> P\u00E1gina n\u00E3o encontrada</h3>\n        <button mat-raised-button color=\"primary\" (click)=\"goBack()\">Clique aqui para voltar</button>\n      </div>\n    </div>\n  </div>\n</div>\n",
                changeDetection: ChangeDetectionStrategy.OnPush,
                styles: [""]
            }] }
];
/** @nocollapse */
NotFoundComponent.ctorParameters = () => [
    { type: Location }
];
if (false) {
    /** @type {?} */
    NotFoundComponent.prototype.imageSrc;
    /**
     * @type {?}
     * @private
     */
    NotFoundComponent.prototype.ngLocation;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const routes$1 = [
    { path: 'not-found', component: NotFoundComponent }
];
class NotFoundRoutingModule {
}
NotFoundRoutingModule.decorators = [
    { type: NgModule, args: [{
                imports: [RouterModule.forChild(routes$1)],
                exports: [RouterModule]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NotFoundModule {
}
NotFoundModule.decorators = [
    { type: NgModule, args: [{
                entryComponents: [],
                declarations: [NotFoundComponent],
                imports: [
                    CommonModule,
                    NotFoundRoutingModule,
                    MatButtonModule$1
                ],
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class UnauthorizedComponent {
    /**
     * @param {?} authenticationService
     * @param {?} userService
     * @param {?} ngLocation
     * @param {?} toastService
     * @param {?} previousRouteService
     * @param {?} router
     * @param {?} providerAuthenticationService
     */
    constructor(authenticationService, userService, ngLocation, toastService, previousRouteService, router, providerAuthenticationService) {
        this.authenticationService = authenticationService;
        this.userService = userService;
        this.ngLocation = ngLocation;
        this.toastService = toastService;
        this.previousRouteService = previousRouteService;
        this.router = router;
        this.providerAuthenticationService = providerAuthenticationService;
        this.imageSrc = require('@cnj/uikit/lib/theme/uikit/images/error_403.png');
        this.isAuthenticated$ = this.userService.user$.pipe(map((/**
         * @param {?} user
         * @return {?}
         */
        (user) => user.authenticated)));
        this.isLoading = false;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (((/** @type {?} */ (this.providerAuthenticationService))).needRenewUser) {
            this.isLoading = true;
        }
        /** @type {?} */
        const loginQueryParam = this.router.routerState.root.snapshot.queryParamMap.get('login');
        if (loginQueryParam === 'auto' && (!((/** @type {?} */ (this.providerAuthenticationService))).needRenewUser)) {
            this.login();
        }
    }
    /**
     * @return {?}
     */
    login() {
        try {
            /** @type {?} */
            const paginaAnterior = this.previousRouteService.getPreviousUrl();
            this.authenticationService.login({ enderecoParaVoltar: paginaAnterior });
        }
        catch (error) {
            this.toastService.error('Error ao realizar o login!', (/** @type {?} */ (error)));
        }
    }
    /**
     * @return {?}
     */
    goBack() {
        this.ngLocation.back();
    }
}
UnauthorizedComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-unauthorized',
                template: "<div class=\"container-fluid\">\n  <div class=\"row\">\n    <div class=\"col\">\n      <div *ngIf=\"isLoading\" class=\"error\">\n          <div class=\"corpo-carregamento\">\n              <div class=\"area-quadros\">\n                <span class=\"primeiro\"></span>\n                <span class=\"segundo\"></span>\n                <span class=\"terceiro\"></span>\n                <span class=\"quarto\"></span>\n              </div>\n              <h3>Carregando...</h3>\n          </div> \n      </div>\n      <div *ngIf=\"!isLoading\" class=\"error\">\n        <div *ngIf=\"!(isAuthenticated$ | async);else alreadyAuthenticated\">\n          <!--INICIO DA IMAGEM N\u00C3O AUTENTICADO-->\n          <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"280px\" height=\"306px\" viewBox=\"0 0 293.98 317.04\">\n            <g id=\"Layer_2\" data-name=\"Layer 2\">\n                <g id=\"Layer_2-2\" data-name=\"Layer 2\">\n                    <path class=\"autenticado-1\" d=\"M251.38,47.53l.11,260.28-175,1-.6-21.86s-.27-28.07-.57-61.18L75,185.84c-.31-42.58-.52-90.33-.26-114.1.1-10.09-2-17.41,33.91-22L215.15,32.36S243.68,23.77,251.38,47.53Z\" />\n                    <polygon class=\"autenticado-2\" points=\"201.01 34.69 75.42 273.18 75.42 236.63 180.55 38.02 201.01 34.69\" />\n                    <polygon class=\"autenticado-2\" points=\"252.28 93.71 135.56 308.69 76 308.8 81.8 282.38 212.52 32.82 251.49 41.63 252.28 93.71\" />\n                    <path class=\"autenticado-3\" d=\"M75.63,245.32s22.16,11.32,35.45,0,4.35-20.46-.1-22.4C106.75,221.05,89.27,218.79,75.63,245.32Z\" />\n                    <path class=\"autenticado-3\" d=\"M67.47,232.6s21.84-14.86,18-33-16.83-14.47-20.9-11.32C60.74,191.25,49.71,206.37,67.47,232.6Z\" />\n                    <path class=\"autenticado-3\" d=\"M55.15,222.23s-7-33.13-30.34-37.51S.34,198.67,1.19,204C2.19,210.18,15.4,230.48,55.15,222.23Z\" />\n                    <path class=\"autenticado-4\" d=\"M75,185.86c-.31-42.58-.52-90.33-.27-114.11.11-10.08-2-17.41,33.91-22L215.22,32.38\" />\n                    <path class=\"autenticado-4\" d=\"M76,286.93s-.27-28.06-.57-61.17\" />\n                    <polyline class=\"autenticado-4\" points=\"251.45 47.55 251.57 307.83 78.51 308.8\" />\n                    <ellipse class=\"autenticado-3\" cx=\"238.84\" cy=\"26.4\" rx=\"25.53\" ry=\"23.96\" transform=\"matrix(0.3, -0.95, 0.95, 0.3, 142.69, 246.62)\" />\n                    <path class=\"autenticado-5\" d=\"M231.69,21V14.85s0-3.38,5.75-3.38,7.75,0,7.75,3.13v9\" />\n                    <path class=\"autenticado-6\" d=\"M248.69,23.6l-21.25,1.62v11s.12,5,10.62,5,10.63-7.37,10.63-7.37Z\" />\n                    <line class=\"autenticado-6\" x1=\"238.06\" y1=\"29.73\" x2=\"238.06\" y2=\"35.91\" />\n                    <path class=\"autenticado-3\" d=\"M230,167.05,96,173.24a6.21,6.21,0,0,1-6.38-6.33v-8.42a6.24,6.24,0,0,1,5.84-6.33l134-6.19a6.21,6.21,0,0,1,6.38,6.33v8.42A6.24,6.24,0,0,1,230,167.05Z\" />\n                    <path class=\"autenticado-3\" d=\"M230,204,96,210.17a6.21,6.21,0,0,1-6.38-6.33v-8.41a6.25,6.25,0,0,1,5.84-6.34l134-6.18a6.21,6.21,0,0,1,6.38,6.33v8.41A6.24,6.24,0,0,1,230,204Z\" />\n                    <path class=\"autenticado-7\" d=\"M231.8,233l-67,3.65c-1.74.09-3.19-1.61-3.19-3.74v-5c0-2,1.29-3.65,2.92-3.74l67-3.65c1.74-.09,3.19,1.61,3.19,3.74v5C234.71,231.24,233.43,232.89,231.8,233Z\" />\n                    <line class=\"autenticado-4\" x1=\"157.95\" y1=\"245.69\" x2=\"242.1\" y2=\"241.07\" />\n                    <rect class=\"autenticado-3\" x=\"38.98\" y=\"286.93\" width=\"47.83\" height=\"6.58\" />\n                    <path class=\"autenticado-3\" d=\"M84.19,293.52c0,10-5.68,15.28-5.68,15.28-5.1,6.23-11.65,5.42-15.88,5.42s-10.78.81-15.89-5.42c0,0-5.68-5.24-5.68-15.28Z\" />\n                    <line class=\"autenticado-4\" x1=\"32.59\" y1=\"316.04\" x2=\"93.21\" y2=\"316.04\" />\n                    <polyline class=\"autenticado-4\" points=\"15.81 199.22 59.19 224.6 63.56 286.93 69.56 202.97\" />\n                    <line class=\"autenticado-4\" x1=\"66.97\" y1=\"248.48\" x2=\"89.63\" y2=\"240.22\" />\n                    <line class=\"autenticado-4\" x1=\"5.76\" y1=\"307.83\" x2=\"24.81\" y2=\"307.83\" />\n                    <line class=\"autenticado-4\" x1=\"31.54\" y1=\"307.83\" x2=\"45.93\" y2=\"307.83\" />\n                    <line class=\"autenticado-4\" x1=\"251.45\" y1=\"307.83\" x2=\"292.98\" y2=\"307.83\" />\n                    <path class=\"autenticado-7\" d=\"M139,138.24l6.9-.4v1.58l-8.88.51-.07-14.87,2-.11Z\" />\n                    <path class=\"autenticado-7\" d=\"M147.22,133.71a6.44,6.44,0,0,1,1.28-4.11,4.56,4.56,0,0,1,3.53-1.79,4.14,4.14,0,0,1,3.57,1.38,5.85,5.85,0,0,1,1.32,4v.22a6.42,6.42,0,0,1-1.28,4.12,4.62,4.62,0,0,1-3.54,1.78,4.15,4.15,0,0,1-3.56-1.37,5.87,5.87,0,0,1-1.32-4Zm2,.11a5.16,5.16,0,0,0,.74,2.86,2.28,2.28,0,0,0,2.17,1,2.54,2.54,0,0,0,2.13-1.27,5.39,5.39,0,0,0,.72-2.94v-.22a5.15,5.15,0,0,0-.75-2.85,2.29,2.29,0,0,0-2.17-1,2.53,2.53,0,0,0-2.13,1.28,5.46,5.46,0,0,0-.71,2.93Z\" />\n                    <path class=\"autenticado-7\" d=\"M158.91,133.35A7.63,7.63,0,0,1,160,129a3.86,3.86,0,0,1,3.15-1.81,3.54,3.54,0,0,1,1.81.32,3.32,3.32,0,0,1,1.31,1.14l.23-1.45,1.57-.09.06,11.11a4.5,4.5,0,0,1-1.2,3.34,5.2,5.2,0,0,1-3.49,1.34,6.73,6.73,0,0,1-1.69-.13,6.4,6.4,0,0,1-1.6-.5l.5-1.59a4.93,4.93,0,0,0,1.28.42,6.22,6.22,0,0,0,1.48.1,3,3,0,0,0,2.1-.79,2.92,2.92,0,0,0,.64-2.07v-1.25a3.76,3.76,0,0,1-1.27,1.12,4.15,4.15,0,0,1-1.7.45,3.53,3.53,0,0,1-3.14-1.29,5.8,5.8,0,0,1-1.16-3.79Zm2,.1a4.49,4.49,0,0,0,.71,2.64,2.24,2.24,0,0,0,2.1.89,2.68,2.68,0,0,0,1.49-.5,3.41,3.41,0,0,0,1-1.23l0-5.09a2.89,2.89,0,0,0-1-1.06,2.52,2.52,0,0,0-1.48-.33,2.44,2.44,0,0,0-2.1,1.33,6.21,6.21,0,0,0-.69,3.14Z\" />\n                    <path class=\"autenticado-7\" d=\"M173.09,124l-2,.12V122l2-.11Zm.07,13.88-2,.12-.05-11,2-.12Z\" />\n                    <path class=\"autenticado-7\" d=\"M177.9,126.53l.15,1.63a4.45,4.45,0,0,1,1.35-1.44,3.75,3.75,0,0,1,1.86-.59A3.34,3.34,0,0,1,184,127a4.45,4.45,0,0,1,1,3.18l0,7-2,.12,0-6.94a2.94,2.94,0,0,0-.57-2,2.13,2.13,0,0,0-1.74-.51,2.86,2.86,0,0,0-1.5.5,3.31,3.31,0,0,0-1,1.21l0,8.06-2,.11-.05-11.05Z\" />\n                    <path class=\"autenticado-8\" d=\"M189.48,230.88l2.73-.15v.62l-3.51.2,0-5.87.78-.05Z\" />\n                    <path class=\"autenticado-8\" d=\"M192.74,229.1a2.58,2.58,0,0,1,.51-1.63,1.82,1.82,0,0,1,1.39-.71,1.66,1.66,0,0,1,1.41.55,2.29,2.29,0,0,1,.52,1.57V229a2.53,2.53,0,0,1-.51,1.62,1.84,1.84,0,0,1-1.4.71,1.66,1.66,0,0,1-1.41-.55,2.29,2.29,0,0,1-.52-1.56Zm.78,0a2,2,0,0,0,.29,1.13.9.9,0,0,0,.86.4,1,1,0,0,0,.84-.5,2.19,2.19,0,0,0,.29-1.16v-.09a2,2,0,0,0-.29-1.12.89.89,0,0,0-.86-.41,1,1,0,0,0-.84.5,2.22,2.22,0,0,0-.28,1.16Z\" />\n                    <path class=\"autenticado-8\" d=\"M197.36,229a3,3,0,0,1,.44-1.72,1.52,1.52,0,0,1,1.24-.72,1.47,1.47,0,0,1,.72.13,1.32,1.32,0,0,1,.52.45l.09-.57.62,0,0,4.39a1.74,1.74,0,0,1-.47,1.32,2.07,2.07,0,0,1-1.38.53,3.21,3.21,0,0,1-.67-.05,2.74,2.74,0,0,1-.63-.2l.2-.63a2.09,2.09,0,0,0,.5.17,2.38,2.38,0,0,0,.59,0,1.2,1.2,0,0,0,.83-.31,1.14,1.14,0,0,0,.25-.82v-.5a1.53,1.53,0,0,1-.5.45,1.58,1.58,0,0,1-.67.17,1.39,1.39,0,0,1-1.24-.51,2.23,2.23,0,0,1-.46-1.49Zm.78,0a1.74,1.74,0,0,0,.28,1,.9.9,0,0,0,.83.35,1.12,1.12,0,0,0,.59-.2,1.33,1.33,0,0,0,.39-.48v-2a1.08,1.08,0,0,0-.4-.42.91.91,0,0,0-.58-.13,1,1,0,0,0-.83.52,2.56,2.56,0,0,0-.27,1.24Z\" />\n                    <path class=\"autenticado-8\" d=\"M203,225.25l-.78,0v-.81l.78,0Zm0,5.48-.78.05,0-4.37.78,0Z\" />\n                    <path class=\"autenticado-8\" d=\"M204.86,226.26l.06.64a1.78,1.78,0,0,1,.53-.56,1.55,1.55,0,0,1,.74-.24,1.33,1.33,0,0,1,1.07.35,1.73,1.73,0,0,1,.39,1.26v2.76l-.78,0v-2.74a1.17,1.17,0,0,0-.23-.81.85.85,0,0,0-.68-.2,1.13,1.13,0,0,0-.6.2,1.28,1.28,0,0,0-.41.48l0,3.18-.78,0,0-4.36Z\" />\n                </g>\n            </g>\n           </svg>\n          <!--FIM DA IMAGEM N\u00C3O AUTENTICADO-->\n          <h3><b>Voc\u00EA n\u00E3o est\u00E1 autenticado!</b></h3>\n          <button mat-raised-button color=\"primary\" (click)=\"login()\">Fazer Login</button>\n        </div>\n        <ng-template #alreadyAuthenticated>\n          <!-- IMAGEM N\u00C3O TEM PERMISS\u00C3O-->\n          <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"280px\" height=\"306px\" viewBox=\"0 0 293.98 317.04\">\n            <g id=\"Layer_2\" data-name=\"Layer 2\">\n                <g id=\"Layer_2-2\" data-name=\"Layer 2\">\n                    <path class=\"naoautenticado-1\" d=\"M251.38,47.53l.11,260.28-175,1-.6-21.86s-.27-28.07-.57-61.18L75,185.84c-.31-42.58-.52-90.33-.26-114.1.1-10.09-2-17.41,33.91-22L215.15,32.36S243.68,23.77,251.38,47.53Z\" />\n                    <polygon class=\"naoautenticado-2\" points=\"201.01 34.69 75.42 273.18 75.42 236.63 180.55 38.02 201.01 34.69\" />\n                    <polygon class=\"naoautenticado-2\" points=\"252.28 93.71 135.56 308.69 76 308.8 81.8 282.38 212.52 32.82 251.49 41.63 252.28 93.71\" />\n                    <path class=\"naoautenticado-3\" d=\"M75.63,245.32s22.16,11.32,35.45,0,4.35-20.46-.1-22.4C106.75,221.05,89.27,218.79,75.63,245.32Z\" />\n                    <path class=\"naoautenticado-3\" d=\"M67.47,232.6s21.84-14.86,18-33-16.83-14.47-20.9-11.32C60.74,191.25,49.71,206.37,67.47,232.6Z\" />\n                    <path class=\"naoautenticado-3\" d=\"M55.15,222.23s-7-33.13-30.34-37.51S.34,198.67,1.19,204C2.19,210.18,15.4,230.48,55.15,222.23Z\" />\n                    <path class=\"naoautenticado-4\" d=\"M75,185.86c-.31-42.58-.52-90.33-.27-114.11.11-10.08-2-17.41,33.91-22L215.22,32.38\" />\n                    <path class=\"naoautenticado-4\" d=\"M76,286.93s-.27-28.06-.57-61.17\" />\n                    <polyline class=\"naoautenticado-4\" points=\"251.45 47.55 251.57 307.83 78.51 308.8\" />\n                    <ellipse class=\"naoautenticado-3\" cx=\"238.84\" cy=\"26.4\" rx=\"25.53\" ry=\"23.96\" transform=\"matrix(0.3, -0.95, 0.95, 0.3, 142.69, 246.62)\" />\n                    <path class=\"naoautenticado-5\" d=\"M231.69,21V14.85s0-3.38,5.75-3.38,7.75,0,7.75,3.13v9\" />\n                    <path class=\"naoautenticado-6\" d=\"M248.69,23.6l-21.25,1.62v11s.12,5,10.62,5,10.63-7.37,10.63-7.37Z\" />\n                    <line class=\"naoautenticado-6\" x1=\"238.06\" y1=\"29.73\" x2=\"238.06\" y2=\"35.91\" />\n                    <rect class=\"naoautenticado-3\" x=\"38.98\" y=\"286.93\" width=\"47.83\" height=\"6.58\" />\n                    <path class=\"naoautenticado-3\" d=\"M84.19,293.52c0,10-5.68,15.28-5.68,15.28-5.1,6.23-11.65,5.42-15.88,5.42s-10.78.81-15.89-5.42c0,0-5.68-5.24-5.68-15.28Z\" />\n                    <line class=\"naoautenticado-4\" x1=\"32.59\" y1=\"316.04\" x2=\"93.21\" y2=\"316.04\" />\n                    <polyline class=\"naoautenticado-4\" points=\"15.81 199.22 59.19 224.6 63.56 286.93 69.56 202.97\" />\n                    <line class=\"naoautenticado-4\" x1=\"66.97\" y1=\"248.48\" x2=\"89.63\" y2=\"240.22\" />\n                    <line class=\"naoautenticado-4\" x1=\"5.76\" y1=\"307.83\" x2=\"24.81\" y2=\"307.83\" />\n                    <line class=\"naoautenticado-4\" x1=\"31.54\" y1=\"307.83\" x2=\"45.93\" y2=\"307.83\" />\n                    <line class=\"naoautenticado-4\" x1=\"251.45\" y1=\"307.83\" x2=\"292.98\" y2=\"307.83\" />\n                    <path class=\"naoautenticado-7\" d=\"M169.64,103.69a67,67,0,1,0,67,67A67.07,67.07,0,0,0,169.64,103.69Zm0,117.23a50.3,50.3,0,0,1-50.25-50.24,49.65,49.65,0,0,1,9.3-29.1l70,70A49.55,49.55,0,0,1,169.64,220.92Zm40.94-21.14-70-70a49.55,49.55,0,0,1,29.1-9.3,50.3,50.3,0,0,1,50.24,50.24A49.62,49.62,0,0,1,210.58,199.78Z\" />\n                    <path class=\"naoautenticado-8\" d=\"M166.57,102a67,67,0,1,0,67,67A67.07,67.07,0,0,0,166.57,102Zm0,117.24A50.3,50.3,0,0,1,116.33,169a49.65,49.65,0,0,1,9.3-29.1l70,70.05A49.62,49.62,0,0,1,166.57,219.2Zm40.94-21.15-70-70a49.65,49.65,0,0,1,29.1-9.3A50.3,50.3,0,0,1,216.82,169,49.56,49.56,0,0,1,207.51,198.05Z\" />\n                </g>\n            </g>\n          </svg>\n          <!-- FIM DA IMAGEM N\u00C3O TEM PERMISS\u00C3O-->\n          <h3>Voc\u00EA n\u00E3o possui <b>permiss\u00E3o</b> para acessar esta p\u00E1gina!</h3>\n          <button mat-raised-button color=\"warn\" (click)=\"goBack()\">Clique aqui para voltar</button>\n        </ng-template>\n      </div>\n    </div>\n  </div>\n</div>\n",
                styles: ["@charset \"UTF-8\";.autenticado-1{fill:#edf3ff}.autenticado-2,.autenticado-3{fill:#d9e4fc}.autenticado-2{opacity:.15}.autenticado-3,.autenticado-4,.autenticado-5,.autenticado-6{stroke:#004bcb;stroke-miterlimit:10}.autenticado-3,.autenticado-4,.autenticado-6{stroke-linecap:round}.autenticado-3,.autenticado-4{stroke-width:2px}.autenticado-4,.autenticado-5,.autenticado-6{fill:none}.autenticado-5,.autenticado-6{stroke-width:3px}.autenticado-7{fill:#004bcb}.autenticado-8{fill:#fff}.mat-raised-button.mat-warn{background-color:#fb3e54}.naoautenticado-1,.naoautenticado-3,.naoautenticado-7{fill:#edf3ff}.naoautenticado-2{opacity:.15;fill:#fb3e54}.naoautenticado-3,.naoautenticado-4,.naoautenticado-5,.naoautenticado-6{stroke:#fb3e54;stroke-miterlimit:10}.naoautenticado-3,.naoautenticado-4,.naoautenticado-6{stroke-linecap:round}.naoautenticado-3,.naoautenticado-4{stroke-width:2px}.naoautenticado-4,.naoautenticado-5,.naoautenticado-6{fill:none}.naoautenticado-5,.naoautenticado-6{stroke-width:3px}.naoautenticado-8{fill:#fb3e54}.area-quadros{width:50px;height:50px;display:inline-block;-webkit-transform:rotate(45deg);transform:rotate(45deg);font-size:0}.area-quadros span{position:relative;width:25px;height:25px;-webkit-transform:scale(1.1);transform:scale(1.1);display:inline-block}.area-quadros span::before{content:'';background-color:#015efd;position:absolute;left:0;top:0;display:block;width:25px;height:25px;-webkit-transform-origin:100% 100%;transform-origin:100% 100%;-webkit-animation:2.5s linear infinite both folding;animation:2.5s linear infinite both folding}.area-quadros .segundo{-webkit-transform:rotateZ(90deg) scale(1.1);transform:rotateZ(90deg) scale(1.1)}.area-quadros .segundo::before{-webkit-animation-delay:.3s;animation-delay:.3s;background-color:#004bcb}.area-quadros .terceiro{-webkit-transform:rotateZ(270deg) scale(1.1);transform:rotateZ(270deg) scale(1.1)}.area-quadros .terceiro::before{-webkit-animation-delay:.9s;animation-delay:.9s;background-color:#0041af}.area-quadros .quarto{-webkit-transform:rotateZ(180deg) scale(1.1);transform:rotateZ(180deg) scale(1.1)}.area-quadros .quarto::before{-webkit-animation-delay:.6s;animation-delay:.6s;background-color:#003795}@-webkit-keyframes folding{0%,10%{-webkit-transform:perspective(140px) rotateX(-180deg);transform:perspective(140px) rotateX(-180deg);opacity:0}25%,75%{-webkit-transform:perspective(140px) rotateX(0);transform:perspective(140px) rotateX(0);opacity:1}100%,90%{-webkit-transform:perspective(140px) rotateY(180deg);transform:perspective(140px) rotateY(180deg);opacity:0}}@keyframes folding{0%,10%{-webkit-transform:perspective(140px) rotateX(-180deg);transform:perspective(140px) rotateX(-180deg);opacity:0}25%,75%{-webkit-transform:perspective(140px) rotateX(0);transform:perspective(140px) rotateX(0);opacity:1}100%,90%{-webkit-transform:perspective(140px) rotateY(180deg);transform:perspective(140px) rotateY(180deg);opacity:0}}.corpo-carregamento{position:fixed;left:50%;top:50%;margin-top:-50px;margin-left:-50px;width:100px;height:100px;text-align:center;color:#01286b}"]
            }] }
];
/** @nocollapse */
UnauthorizedComponent.ctorParameters = () => [
    { type: AuthenticationService },
    { type: UserService },
    { type: Location },
    { type: ToastService },
    { type: PreviousRouteService },
    { type: Router },
    { type: undefined, decorators: [{ type: Inject, args: [IAuthenticationServiceToken,] }] }
];
if (false) {
    /** @type {?} */
    UnauthorizedComponent.prototype.imageSrc;
    /** @type {?} */
    UnauthorizedComponent.prototype.isAuthenticated$;
    /** @type {?} */
    UnauthorizedComponent.prototype.isLoading;
    /**
     * @type {?}
     * @private
     */
    UnauthorizedComponent.prototype.authenticationService;
    /**
     * @type {?}
     * @private
     */
    UnauthorizedComponent.prototype.userService;
    /**
     * @type {?}
     * @private
     */
    UnauthorizedComponent.prototype.ngLocation;
    /**
     * @type {?}
     * @private
     */
    UnauthorizedComponent.prototype.toastService;
    /**
     * @type {?}
     * @private
     */
    UnauthorizedComponent.prototype.previousRouteService;
    /**
     * @type {?}
     * @private
     */
    UnauthorizedComponent.prototype.router;
    /**
     * @type {?}
     * @private
     */
    UnauthorizedComponent.prototype.providerAuthenticationService;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
const ɵ0$2 = {
    allowedTypes: ['normal', 'noshell', 'noshellnobreadcrumb'],
    defaultType: 'normal'
};
/** @type {?} */
const routes$2 = [
    {
        path: 'unauthorized',
        component: UnauthorizedComponent,
        data: ɵ0$2
    }
];
class UnauthorizedRoutingModule {
}
UnauthorizedRoutingModule.decorators = [
    { type: NgModule, args: [{
                imports: [RouterModule.forChild(routes$2)],
                exports: [RouterModule]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class UnauthorizedModule {
}
UnauthorizedModule.decorators = [
    { type: NgModule, args: [{
                entryComponents: [],
                declarations: [UnauthorizedComponent],
                imports: [
                    CommonModule,
                    UnauthorizedRoutingModule,
                    MatButtonModule$1
                ],
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
const OperationState = {
    Idle: 'idle',
    Processing: 'processing',
    Processed: 'processed',
    Cancelling: 'cancelling',
    Cancelled: 'cancelled',
    Undoing: 'undoing',
    Undone: 'undone',
    Error: 'error',
};
class OperationsSnackBar {
    /**
     * @param {?} description
     * @param {?=} allowToCancel
     */
    constructor(description, allowToCancel = false) {
        this.description = description;
        this.allowToCancel = allowToCancel;
        this.operationStateSubject$ = new BehaviorSubject(OperationState.Idle);
        this.operationState$ = this.operationStateSubject$.asObservable();
        this.operationState = OperationState.Processing;
        this.error = null;
        this.operation$ = null;
        this.undoOperation = null;
        this.isUndoableSubject$ = new BehaviorSubject(false);
        this.isUndoable$ = this.isUndoableSubject$.asObservable();
        this.isUndoable = false;
        this.canUndoSubject$ = new BehaviorSubject(false);
        this.canUndo$ = this.canUndoSubject$.asObservable();
        this.canUndo = false;
        this.cancelOperation = null;
        this.isCancellableSubject$ = new BehaviorSubject(false);
        this.isCancellable$ = this.isCancellableSubject$.asObservable();
        this.isCancellable = false;
        this.canCancelSubject$ = new BehaviorSubject(false);
        this.canCancel$ = this.canCancelSubject$.asObservable();
        this.canCancel = false;
        // Mantem atualizado as variáveis 'não observável'
        this.operationState$.subscribe((/**
         * @param {?} state
         * @return {?}
         */
        (state) => this.operationState = state));
        this.isUndoable$.subscribe((/**
         * @param {?} isUndoable
         * @return {?}
         */
        isUndoable => this.isUndoable = isUndoable));
        this.isCancellable$.subscribe((/**
         * @param {?} isCancellable
         * @return {?}
         */
        isCancellable => this.isCancellable = isCancellable));
        combineLatest(this.operationState$, this.isCancellable$).subscribe((/**
         * @param {?} value
         * @return {?}
         */
        (value) => {
            /** @type {?} */
            const state = value[0];
            /** @type {?} */
            const isCancellable = value[1];
            if (state === OperationState.Processing && this.allowToCancel) {
                this.canCancel = isCancellable;
            }
            else {
                this.canCancel = false;
            }
            this.canCancelSubject$.next(this.canCancel);
        }));
        combineLatest(this.operationState$, this.isUndoable$).subscribe((/**
         * @param {?} value
         * @return {?}
         */
        (value) => {
            /** @type {?} */
            const state = value[0];
            /** @type {?} */
            const isUndoable = value[1];
            if (state === OperationState.Processed) {
                this.canUndo = isUndoable;
            }
            else {
                this.canUndo = false;
            }
            this.canUndoSubject$.next(this.canUndo);
        }));
    }
    /**
     * @param {?} subscription
     * @return {?}
     */
    setCancelOperation(subscription) {
        if (!subscription) {
            throw Error('Não é possível setar uma operação de cancelar com a subscrição nula (null) ou indefinida (undefined)!');
        }
        if (subscription && subscription.closed === false) {
            this.cancelOperation = subscription;
            this.isCancellableSubject$.next(true);
        }
    }
    /**
     * @param {?} undoFunction
     * @return {?}
     */
    setUndoOperation(undoFunction) {
        if (!undoFunction) {
            throw Error('Não é possível setar uma operação de desfazer com a operação nula (null) ou indefinida (undefined)!');
        }
        else {
            this.undoOperation = undoFunction;
            this.isUndoableSubject$.next(true);
        }
    }
    /**
     * @param {?} operation
     * @return {?}
     */
    setOperation(operation) {
        /** @type {?} */
        const operationObservavel = operation
            .pipe(catchError((/**
         * @param {?} error
         * @return {?}
         */
        (error) => {
            return this.setError(error);
        })), finalize((/**
         * @return {?}
         */
        () => {
            this.setAsProcessed();
        })));
        this.operation$ = operationObservavel;
    }
    /**
     * @return {?}
     */
    setAsProcessed() {
        this.setState(OperationState.Processed);
    }
    /**
     * @param {?} error
     * @return {?}
     */
    setError(error) {
        this.operationStateSubject$.next(OperationState.Error);
        this.error = error;
        return throwError(error);
    }
    /**
     * @protected
     * @param {?} operationState
     * @return {?}
     */
    setState(operationState) {
        this.operationStateSubject$.next(operationState);
    }
    /**
     * @return {?}
     */
    setAsStarted() {
        this.setState(OperationState.Processing);
    }
    /**
     * @return {?}
     */
    undo() {
        if (!this.isUndoableSubject$.getValue()) {
            throw Error('Não é possível desfazer esta operação!');
        }
        /** @type {?} */
        let retorno = null;
        this.operationStateSubject$.next(OperationState.Undoing);
        /** @type {?} */
        const undoOperation = this.undoOperation;
        /** @type {?} */
        const observable = (/** @type {?} */ (undoOperation));
        /** @type {?} */
        const isObservable = observable.pipe;
        if (isObservable) {
            retorno = observable.pipe(catchError((/**
             * @param {?} error
             * @return {?}
             */
            error => this.setError(error))))
                .subscribe((/**
             * @return {?}
             */
            () => this.operationStateSubject$.next(OperationState.Undone)));
        }
        /** @type {?} */
        const promise = (/** @type {?} */ (undoOperation));
        /** @type {?} */
        const isPromise = promise.then;
        if (isPromise) {
            retorno = promise.then((/**
             * @return {?}
             */
            () => this.operationStateSubject$.next(OperationState.Undone)), (/**
             * @param {?} error
             * @return {?}
             */
            (error) => this.setError(error)));
        }
        /** @type {?} */
        const undoLambda = (/** @type {?} */ (undoOperation));
        /** @type {?} */
        const isUndoLambda = undoLambda.call;
        if ((!isObservable) && (!isPromise) && isUndoLambda) {
            try {
                retorno = undoLambda();
                this.operationStateSubject$.next(OperationState.Undone);
            }
            catch (error) {
                this.setError(error);
                throw error;
            }
        }
        return retorno;
    }
    /**
     * @return {?}
     */
    cancel() {
        if (!this.canCancel) {
            throw Error('Não é possível cancelar esta operação!');
        }
        this.operationStateSubject$.next(OperationState.Cancelling);
        if (this.cancelOperation && this.cancelOperation.closed === false) {
            this.cancelOperation.add((/**
             * @return {?}
             */
            () => {
                this.operationStateSubject$.next(OperationState.Cancelled);
                this.isCancellableSubject$.next(false);
            }));
            this.cancelOperation.unsubscribe();
        }
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    OperationsSnackBar.prototype.operationStateSubject$;
    /** @type {?} */
    OperationsSnackBar.prototype.operationState$;
    /** @type {?} */
    OperationsSnackBar.prototype.operationState;
    /** @type {?} */
    OperationsSnackBar.prototype.error;
    /** @type {?} */
    OperationsSnackBar.prototype.operation$;
    /**
     * @type {?}
     * @protected
     */
    OperationsSnackBar.prototype.undoOperation;
    /**
     * @type {?}
     * @private
     */
    OperationsSnackBar.prototype.isUndoableSubject$;
    /**
     * @type {?}
     * @protected
     */
    OperationsSnackBar.prototype.isUndoable$;
    /**
     * @type {?}
     * @protected
     */
    OperationsSnackBar.prototype.isUndoable;
    /**
     * @type {?}
     * @private
     */
    OperationsSnackBar.prototype.canUndoSubject$;
    /** @type {?} */
    OperationsSnackBar.prototype.canUndo$;
    /** @type {?} */
    OperationsSnackBar.prototype.canUndo;
    /**
     * @type {?}
     * @protected
     */
    OperationsSnackBar.prototype.cancelOperation;
    /**
     * @type {?}
     * @private
     */
    OperationsSnackBar.prototype.isCancellableSubject$;
    /**
     * @type {?}
     * @protected
     */
    OperationsSnackBar.prototype.isCancellable$;
    /**
     * @type {?}
     * @protected
     */
    OperationsSnackBar.prototype.isCancellable;
    /**
     * @type {?}
     * @private
     */
    OperationsSnackBar.prototype.canCancelSubject$;
    /** @type {?} */
    OperationsSnackBar.prototype.canCancel$;
    /** @type {?} */
    OperationsSnackBar.prototype.canCancel;
    /** @type {?} */
    OperationsSnackBar.prototype.description;
    /** @type {?} */
    OperationsSnackBar.prototype.allowToCancel;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class OperationsService {
    constructor() {
        this.operationsSubject$ = new BehaviorSubject([]);
        this.operations$ = this.operationsSubject$.asObservable();
        this.isLoadingBehaviourSubject$ = new BehaviorSubject(false);
        this.isLoading$ = this.isLoadingBehaviourSubject$.asObservable().pipe(distinctUntilChanged());
        this.isLoading = false;
    }
    /**
     * @template T
     * @param {?=} description
     * @param {?=} operation
     * @param {?=} undoOperation
     * @param {?=} allowToCancel
     * @param {?=} createAsProcessing
     * @return {?}
     */
    create(description = 'Carregando', operation, undoOperation, allowToCancel = false, createAsProcessing = true) {
        /** @type {?} */
        const operationsSnackBar = new OperationsSnackBar(description, allowToCancel);
        if (operation) {
            operationsSnackBar.setOperation(operation);
        }
        if (undoOperation) {
            operationsSnackBar.setUndoOperation(undoOperation);
        }
        this.addOperation(operationsSnackBar);
        if (createAsProcessing) {
            operationsSnackBar.setAsStarted();
        }
        /** @type {?} */
        const operationDefault = operationsSnackBar.operation$ ? operationsSnackBar.operation$ : of();
        return Object.assign(operationDefault, { snackbar: operationsSnackBar });
    }
    /**
     * @param {?=} cancellable
     * @return {?}
     */
    cancelAll(cancellable = (/**
     * @param {?} operation
     * @return {?}
     */
    (operation) => operation.canCancel)) {
        this.operationsSubject$.getValue().forEach((/**
         * @param {?} operation
         * @return {?}
         */
        operation => {
            if (cancellable(operation)) {
                operation.cancel();
            }
        }));
    }
    /**
     * @private
     * @param {?} operationState
     * @return {?}
     */
    operationIsFinished(operationState) {
        return operationState === OperationState.Processed ||
            operationState === OperationState.Cancelled ||
            operationState === OperationState.Undone ||
            operationState === OperationState.Error;
    }
    /**
     * @private
     * @return {?}
     */
    checkLoading() {
        /** @type {?} */
        const operations = this.operationsSubject$.getValue();
        /** @type {?} */
        const allFinished = operations.every((/**
         * @param {?} l
         * @return {?}
         */
        l => this.operationIsFinished(l.operationState)));
        this.isLoading = allFinished === false;
        this.isLoadingBehaviourSubject$.next(this.isLoading);
    }
    /**
     * @private
     * @param {?} loadingDialog
     * @return {?}
     */
    addOperation(loadingDialog) {
        /** @type {?} */
        const actualLoadingDialogs = this.operationsSubject$.getValue();
        loadingDialog.operationState$.subscribe((/**
         * @param {?} os
         * @return {?}
         */
        os => {
            this.checkLoading();
            if (this.operationIsFinished(os)) {
                timer(2500).subscribe((/**
                 * @return {?}
                 */
                () => {
                    if (this.operationIsFinished(os)) {
                        this.removeOperation(loadingDialog);
                    }
                }));
            }
        }));
        actualLoadingDialogs.push(loadingDialog);
        this.operationsSubject$.next(actualLoadingDialogs);
    }
    /**
     * @private
     * @param {?} loadingDialog
     * @return {?}
     */
    removeOperation(loadingDialog) {
        /** @type {?} */
        const actualLoadingDialogs = this.operationsSubject$.getValue();
        /** @type {?} */
        const index = actualLoadingDialogs.indexOf(loadingDialog);
        if (index !== -1) {
            actualLoadingDialogs.splice(index, 1);
            this.operationsSubject$.next(actualLoadingDialogs);
        }
    }
}
OperationsService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
OperationsService.ctorParameters = () => [];
/** @nocollapse */ OperationsService.ngInjectableDef = defineInjectable({ factory: function OperationsService_Factory() { return new OperationsService(); }, token: OperationsService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    OperationsService.prototype.operationsSubject$;
    /** @type {?} */
    OperationsService.prototype.operations$;
    /**
     * @type {?}
     * @private
     */
    OperationsService.prototype.isLoadingBehaviourSubject$;
    /** @type {?} */
    OperationsService.prototype.isLoading$;
    /** @type {?} */
    OperationsService.prototype.isLoading;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class OperationsComponent {
    /**
     * @param {?} operationsService
     * @param {?} operationsManagerService
     */
    constructor(operationsService, operationsManagerService) {
        this.operationsService = operationsService;
        this.operationsManagerService = operationsManagerService;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.operations$ = this.operationsService.operations$;
        this.isLoading$ = this.operationsService.isLoading$;
    }
    /**
     * @return {?}
     */
    close() {
        this.operationsManagerService.close();
    }
}
OperationsComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-operations',
                template: "<mat-expansion-panel [expanded]=\"(operations$ | async).length == 1\">\n\n  <mat-expansion-panel-header *ngIf=\"(operations$ | async).length > 1\">\n    <mat-panel-title>\n      <div *ngIf=\"(isLoading$ | async); else notLoading\">\n        Processando {{ (operations$ | async).length }} opera\u00E7\u00F5es...\n      </div>\n      <ng-template #notLoading>\n        Finalizado\n      </ng-template>\n    </mat-panel-title>\n\n  </mat-expansion-panel-header>\n\n  <mat-list>\n    <mat-list-item *ngFor=\"let operation of (operations$ | async)\" [@fadeInOut]>\n      <h4 matLine>{{ operation.description }}</h4>\n\n      <mat-progress-spinner\n        *ngIf=\"\n        (operation.operationState$ | async) === 'processing' ||\n        (operation.operationState$ | async) === 'cancelling' ||\n        (operation.operationState$ | async) === 'undoing'\n        \"\n        [diameter]=\"20\" mode=\"indeterminate\" color=\"primary\"></mat-progress-spinner>\n\n      <div *ngIf=\"(operation.operationState$ | async) === 'processed'\">Finalizado</div>\n      <div *ngIf=\"(operation.operationState$ | async) === 'cancelling'\">Cancelando</div>\n      <div *ngIf=\"(operation.operationState$ | async) === 'cancelled'\">Cancelado</div>\n      <div *ngIf=\"(operation.operationState$ | async) === 'undoing'\">Desfazendo</div>\n      <div *ngIf=\"(operation.operationState$ | async) === 'undone'\">Desfeito</div>\n      <div *ngIf=\"(operation.operationState$ | async) === 'error'\" matTooltip=\"{{ operation.error.message }}\">Erro</div>\n\n        <button *ngIf=\"operation.canUndo$ | async\"\n        (click)=\"operation.undo()\" mat-icon-button><mat-icon class=\"fas fa-undo\"></mat-icon></button>\n\n        <button *ngIf=\"operation.canCancel$ | async\"\n        (click)=\"operation.cancel()\" mat-icon-button><mat-icon class=\"fas fa-times-circle\"></mat-icon></button>\n\n    </mat-list-item>\n  </mat-list>\n\n</mat-expansion-panel>\n",
                encapsulation: ViewEncapsulation.None,
                animations: [
                    trigger('fadeInOut', [
                        state('void', style({
                            opacity: 0
                        })),
                        transition('void => *', animate(250)),
                        transition('* => void', animate(500)),
                    ]),
                ],
                styles: [".operation-dialog{background:0 0;padding:0;min-height:auto!important}.operation-dialog .mat-expansion-panel .mat-expansion-panel-header{background:rgba(0,0,0,.9)!important;border-radius:0;padding:0 15px}.operation-dialog .mat-expansion-panel .mat-expansion-panel-header .mat-expansion-indicator:after,.operation-dialog .mat-expansion-panel .mat-expansion-panel-header .mat-expansion-panel-header-title{color:#fff}.operation-dialog .mat-expansion-panel .mat-expansion-panel-body{padding:0 15px!important;background:rgba(0,0,0,.85)!important}.operation-dialog .mat-expansion-panel .mat-expansion-panel-body .mat-list{padding:0!important}.operation-dialog .mat-expansion-panel .mat-expansion-panel-body .mat-list .mat-list-item{min-height:initial!important;padding:10px 0!important;color:#fff!important;border-bottom:1px solid rgba(255,255,255,.1)}.operation-dialog .mat-expansion-panel .mat-expansion-panel-body .mat-list .mat-list-item:last-child{border:none}.operation-dialog .mat-list-item-content{padding:0!important}"]
            }] }
];
/** @nocollapse */
OperationsComponent.ctorParameters = () => [
    { type: OperationsService },
    { type: OperationsManagerService }
];
if (false) {
    /** @type {?} */
    OperationsComponent.prototype.operations$;
    /** @type {?} */
    OperationsComponent.prototype.isLoading$;
    /**
     * @type {?}
     * @protected
     */
    OperationsComponent.prototype.operationsService;
    /**
     * @type {?}
     * @protected
     */
    OperationsComponent.prototype.operationsManagerService;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class OperationsManagerService {
    /**
     * @param {?} operationsService
     * @param {?} snackBar
     */
    constructor(operationsService, snackBar) {
        this.operationsService = operationsService;
        this.snackBar = snackBar;
        // Atualiza a variavel conforme o observable atualizar de valor
        this.operationsService.isLoading$.subscribe((/**
         * @param {?} isLoading
         * @return {?}
         */
        (isLoading) => {
            if (isLoading) {
                // Verifico se o snackbar (de operações) já está sendo exibida
                // para evitar abrir outra (ocasionando em um fecha/abre desnecessário)
                if (this.isLoadSnackbarActive()) {
                    /** @type {?} */
                    const loadingMaterialDialog = this.snackBar.openFromComponent(OperationsComponent, {
                        verticalPosition: 'bottom',
                        horizontalPosition: 'center',
                        panelClass: 'operation-dialog',
                        announcementMessage: 'Processando...',
                    });
                }
            }
            else {
                timer(2500).subscribe((/**
                 * @param {?} loading
                 * @return {?}
                 */
                (loading) => {
                    if (!this.operationsService.isLoading) {
                        this.close();
                    }
                }));
            }
        }));
    }
    /**
     * @private
     * @return {?}
     */
    isLoadSnackbarActive() {
        return !(this.snackBar._openedSnackBarRef &&
            this.snackBar._openedSnackBarRef.instance instanceof OperationsComponent);
    }
    /**
     * @return {?}
     */
    close() {
        this.snackBar.dismiss();
    }
}
OperationsManagerService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
OperationsManagerService.ctorParameters = () => [
    { type: OperationsService },
    { type: MatSnackBar }
];
if (false) {
    /**
     * @type {?}
     * @protected
     */
    OperationsManagerService.prototype.operationsService;
    /**
     * @type {?}
     * @protected
     */
    OperationsManagerService.prototype.snackBar;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @param {?=} blackList
 * @return {?}
 */
function AutoUnsubscribe(blackList = []) {
    return (/**
     * @param {?} constructor
     * @return {?}
     */
    function (constructor) {
        /** @type {?} */
        const original = constructor.prototype.ngOnDestroy;
        constructor.prototype.ngOnDestroy = (/**
         * @return {?}
         */
        function () {
            for (let prop in this) {
                /** @type {?} */
                const property = this[prop];
                if (!blackList.includes(prop)) {
                    if (property && (typeof property.unsubscribe === "function")) {
                        property.unsubscribe();
                    }
                }
            }
            original && typeof original === 'function' && original.apply(this, arguments);
        });
    });
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
const SaveButtonSelection = {
    Salvar: 'Salvar',
    SalvarVoltar: 'Salvar e voltar',
    SalvarNovo: 'Salvar e novo',
};
class SaveButtonComponent {
    /**
     * @param {?} renderer
     * @param {?} ngLocation
     */
    constructor(renderer, ngLocation) {
        this.renderer = renderer;
        this.ngLocation = ngLocation;
        // @ViewChild(MatButton) saveButton: MatButton;
        // @ViewChild('uikitSubmitButton', { read: ElementRef }) protected submitButton: ElementRef;
        // @ViewChild('uikitResetButton', { read: ElementRef }) protected resetButton: ElementRef;
        /**
         * Use the parent form to save (using submit), new (reset) and back (history)
         */
        // @Input() automaticTrigger = false;
        this.save = new EventEmitter();
        this.selection = SaveButtonSelection.Salvar;
        this.disabled = false;
    }
    /**
     * @return {?}
     */
    ngAfterViewInit() {
    }
    /**
     * @return {?}
     */
    saveAction() {
        console.log(SaveButtonSelection.Salvar);
        this.selection = SaveButtonSelection.Salvar;
        // if (this.automaticTrigger) {
        //   this.action();
        // }
        this.save.emit(SaveButtonSelection.Salvar);
    }
    /**
     * @return {?}
     */
    saveBackAction() {
        console.log(SaveButtonSelection.SalvarVoltar);
        this.selection = SaveButtonSelection.SalvarVoltar;
        // if (this.automaticTrigger) {
        //   this.action();
        // }
        this.save.emit(SaveButtonSelection.SalvarVoltar);
    }
    /**
     * @return {?}
     */
    saveNewAction() {
        console.log(SaveButtonSelection.SalvarNovo);
        this.selection = SaveButtonSelection.SalvarNovo;
        // if (this.automaticTrigger) {
        //   this.action();
        // }
        this.save.emit(SaveButtonSelection.SalvarNovo);
    }
    // submit() {
    //   const saveButtonElement = this.saveButton.nativeElement as HTMLButtonElement;
    //   this.renderer.setAttribute(saveButtonElement, `type`, `submit`);
    //   saveButtonElement.click();
    //   if (this.selection === SaveButtonSelection.SalvarNovo) {
    //   }
    // }
    /**
     * @return {?}
     */
    action() {
        if (this.selection === SaveButtonSelection.Salvar) {
            return this.saveAction();
        }
        else if (this.selection === SaveButtonSelection.SalvarNovo) {
            return this.saveNewAction();
        }
        else if (this.selection === SaveButtonSelection.SalvarVoltar) {
            return this.saveBackAction();
        }
    }
}
SaveButtonComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-save-button',
                template: "<div class=\"btn-group\">\n  <button type=\"button\" (click)=\"action()\" [disabled]=\"disabled\" mat-fab color=\"primary\">\n    <mat-icon class=\"fas fa-save\"></mat-icon> {{ selection }}\n  </button>\n  <div [hidden]=\"true\">\n    <button mat-button #uikitSubmitButton type=\"submit\"></button>\n    <button mat-button #uikitResetButton type=\"reset\"></button>\n  </div>\n  <mat-menu #menu=\"matMenu\">\n    <button mat-menu-item [disabled]=\"disabled\" (click)=\"saveAction()\">Salvar</button>\n    <button mat-menu-item [disabled]=\"disabled\" (click)=\"saveBackAction()\">Salvar e Voltar</button>\n    <button mat-menu-item [disabled]=\"disabled\" (click)=\"saveNewAction()\">Salvar e Novo</button>\n  </mat-menu>\n\n  <button type=\"button\" [disabled]=\"disabled\" [matMenuTriggerFor]=\"menu\" mat-fab color=\"primary\">\n    <mat-icon class=\"fas fa-caret-down\"></mat-icon>\n  </button>\n</div>\n",
                styles: [""]
            }] }
];
/** @nocollapse */
SaveButtonComponent.ctorParameters = () => [
    { type: Renderer2 },
    { type: Location }
];
SaveButtonComponent.propDecorators = {
    save: [{ type: Output }],
    submitButton: [{ type: ViewChild, args: ['uikitSubmitButton',] }],
    resetButton: [{ type: ViewChild, args: ['uikitResetButton',] }],
    selection: [{ type: Input }],
    disabled: [{ type: Input }]
};
if (false) {
    /**
     * Use the parent form to save (using submit), new (reset) and back (history)
     * @type {?}
     */
    SaveButtonComponent.prototype.save;
    /**
     * @type {?}
     * @protected
     */
    SaveButtonComponent.prototype.submitButton;
    /**
     * @type {?}
     * @protected
     */
    SaveButtonComponent.prototype.resetButton;
    /** @type {?} */
    SaveButtonComponent.prototype.selection;
    /** @type {?} */
    SaveButtonComponent.prototype.disabled;
    /**
     * @type {?}
     * @protected
     */
    SaveButtonComponent.prototype.renderer;
    /**
     * @type {?}
     * @protected
     */
    SaveButtonComponent.prototype.ngLocation;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class FilterService {
    constructor() {
        this.filterBadgeSubject = new BehaviorSubject(0);
        this.filterBadge$ = this.filterBadgeSubject.asObservable();
        this.showFiltersSubject = new BehaviorSubject(false);
        this.showFilters$ = this.showFiltersSubject.asObservable();
    }
    /**
     * @param {?} count
     * @return {?}
     */
    setCountFilter(count) {
        this.filterBadgeSubject.next(count);
    }
    /**
     * @return {?}
     */
    setShowFilters() {
        this.showFiltersSubject.next(!this.showFiltersSubject.getValue());
    }
}
FilterService.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */
FilterService.ctorParameters = () => [];
/** @nocollapse */ FilterService.ngInjectableDef = defineInjectable({ factory: function FilterService_Factory() { return new FilterService(); }, token: FilterService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @protected
     */
    FilterService.prototype.filterBadgeSubject;
    /** @type {?} */
    FilterService.prototype.filterBadge$;
    /**
     * @type {?}
     * @protected
     */
    FilterService.prototype.showFiltersSubject;
    /** @type {?} */
    FilterService.prototype.showFilters$;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class FilterComponent {
    /**
     * @param {?} hotkeysService
     * @param {?} filterService
     * @param {?} dialog
     */
    constructor(hotkeysService, filterService, dialog) {
        this.hotkeysService = hotkeysService;
        this.filterService = filterService;
        this.dialog = dialog;
        this.clickPesquisa = new EventEmitter();
        this.filterBadge$ = this.filterService.filterBadge$;
        this.showFilters$ = this.filterService.showFilters$;
        this.subscriptions = [];
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @return {?}
     */
    ngAfterViewInit() {
        this.hotkeysService.add(new Hotkey('alt+3', (/**
         * @param {?} event
         * @return {?}
         */
        (event) => {
            this.searchInput.focus();
            return false;
        })));
        this.hotkeysService.add(new Hotkey('ctrl+alt+3', (/**
         * @param {?} event
         * @return {?}
         */
        (event) => {
            this.toggleFilters();
            return false;
        })));
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.subscriptions) {
            this.subscriptions.forEach((/**
             * @param {?} subscription
             * @return {?}
             */
            subscription => {
                if (subscription && subscription.closed === false) {
                    subscription.unsubscribe();
                }
            }));
        }
    }
    /**
     * @return {?}
     */
    pesquisar() {
        this.clickPesquisa.emit(this.searchInput.value);
        this.clickPesquisa.subscribe((/**
         * @return {?}
         */
        () => {
            this.searchInput.focus();
        }));
    }
    /**
     * @return {?}
     */
    toggleFilters() {
        this.dialogRef = this.dialog.open(this.template);
    }
    /**
     * @return {?}
     */
    onNoClick() {
        this.dialogRef.close();
    }
}
FilterComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-filter',
                template: "<div class=\"uikit-filter\">\n  <div class=\"uikit-finder\">\n    <mat-form-field>\n      <input #search=\"matInput\" matInput placeholder=\"Pesquisar\" autofocus/>\n      <button type='button' mat-icon-button class=\"btn-search\" (click)=\"pesquisar()\" [attr.disabled]=\"!disabled\">\n        <mat-icon class=\"fa-2x fas fa-search\"></mat-icon>\n      </button>\n    </mat-form-field>\n\n    <button type=\"button\" (click)=\"toggleFilters()\" #moreFilters mat-flat-button color=\"primary\"\n            class=\"btn-more-filters\" matTooltip=\"Mostrar ou esconder mais op\u00E7\u00F5es de filtros\"\n            aria-label=\"Mostrar ou esconder mais op\u00E7\u00F5es de filtros\"\n            [matBadge]=\"(filterBadge$ | async)\" [matBadgeHidden]=\"(filterBadge$ | async) == 0\"\n            matBadgeColor=\"accent\"\n            matBadgeSize=\"medium\">\n      mais filtros\n    </button>\n  </div>\n</div>\n\n<ng-template #filterDialog>\n  <div>\n    <div class=\"row line\">\n      <div class=\"col-10\">\n        <h1 mat-dialog-title>Filtros</h1>\n      </div>\n\n      <div class=\"col-2\">\n        <div mat-dialog-actions class=\"close-position\">\n          <button class=\"close\" mat-button (click)=\"onNoClick()\"><i class=\"fas fa-times\"></i></button>\n        </div>\n      </div>\n     </div>\n    <div mat-dialog-content>\n      <ng-content></ng-content>\n    </div>\n  </div>\n</ng-template>\n",
                styles: [""]
            }] }
];
/** @nocollapse */
FilterComponent.ctorParameters = () => [
    { type: HotkeysService },
    { type: FilterService },
    { type: MatDialog }
];
FilterComponent.propDecorators = {
    clickPesquisa: [{ type: Output }],
    disabled: [{ type: Input }],
    searchInput: [{ type: ViewChild, args: ['search',] }],
    template: [{ type: ViewChild, args: ['filterDialog',] }],
    filtersForm: [{ type: ContentChild, args: [FormGroupDirective,] }]
};
if (false) {
    /** @type {?} */
    FilterComponent.prototype.clickPesquisa;
    /** @type {?} */
    FilterComponent.prototype.disabled;
    /** @type {?} */
    FilterComponent.prototype.searchInput;
    /** @type {?} */
    FilterComponent.prototype.template;
    /** @type {?} */
    FilterComponent.prototype.filtersForm;
    /** @type {?} */
    FilterComponent.prototype.filterBadge$;
    /** @type {?} */
    FilterComponent.prototype.showFilters$;
    /**
     * @type {?}
     * @protected
     */
    FilterComponent.prototype.subscriptions;
    /** @type {?} */
    FilterComponent.prototype.dialogRef;
    /**
     * @type {?}
     * @protected
     */
    FilterComponent.prototype.hotkeysService;
    /**
     * @type {?}
     * @protected
     */
    FilterComponent.prototype.filterService;
    /** @type {?} */
    FilterComponent.prototype.dialog;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class UikitComponentsModule {
}
UikitComponentsModule.decorators = [
    { type: NgModule, args: [{
                providers: [],
                declarations: [SaveButtonComponent, FilterComponent],
                exports: [
                    SaveButtonComponent,
                    FilterComponent,
                ],
                imports: [
                    CommonModule,
                    MatButtonModule$1,
                    MatIconModule$1,
                    MatMenuModule$1,
                    MatFormFieldModule$1,
                    MatInputModule$1,
                    MatBadgeModule$1,
                    MatDialogModule
                ],
                entryComponents: [SaveButtonComponent, FilterComponent]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class GuidService {
    constructor() {
    }
    /**
     * @return {?}
     */
    generate() {
        /** @type {?} */
        let dt = new Date().getTime();
        /** @type {?} */
        const uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (/**
         * @param {?} c
         * @return {?}
         */
        (c) => {
            // tslint:disable-next-line:no-bitwise
            /** @type {?} */
            const r = (dt + Math.random() * 16) % 16 | 0;
            dt = Math.floor(dt / 16);
            // tslint:disable-next-line:no-bitwise
            return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        }));
        return uuid;
    }
}
GuidService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
GuidService.ctorParameters = () => [];
/** @nocollapse */ GuidService.ngInjectableDef = defineInjectable({ factory: function GuidService_Factory() { return new GuidService(); }, token: GuidService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NoRouteReuseStrategy extends RouteReuseStrategy {
    /**
     * @param {?} route
     * @return {?}
     */
    shouldDetach(route) {
        return false;
    }
    /**
     * @param {?} route
     * @param {?} handle
     * @return {?}
     */
    store(route, handle) {
    }
    /**
     * @param {?} route
     * @return {?}
     */
    shouldAttach(route) {
        return false;
    }
    /**
     * @param {?} route
     * @return {?}
     */
    retrieve(route) {
        return null;
    }
    /**
     * @param {?} future
     * @param {?} curr
     * @return {?}
     */
    shouldReuseRoute(future, curr) {
        return false; // default is true if configuration of current and future route are the same
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @param {?} possivelFiltroComposto
 * @return {?}
 */
function isFilterComposite(possivelFiltroComposto) {
    return ((/** @type {?} */ (possivelFiltroComposto))).filtros !== undefined;
}
/** @enum {string} */
const FiltroCompostoOperador = {
    E: 'and',
    Ou: 'or',
};
/**
 * @record
 */
function FiltroComposto() { }
if (false) {
    /** @type {?} */
    FiltroComposto.prototype.operador;
    /** @type {?} */
    FiltroComposto.prototype.filtros;
}
/**
 * @record
 * @template TCampo
 */
function Filtro() { }
if (false) {
    /** @type {?} */
    Filtro.prototype.campo;
    /** @type {?} */
    Filtro.prototype.operador;
    /** @type {?|undefined} */
    Filtro.prototype.valor;
}
/** @enum {string} */
const FiltroOperador = {
    Igual: 'Igual',
    Diferente: 'Diferente',
    ComecaCom: 'Começa com',
    TerminaCom: 'Termina com',
    Contem: 'Contém',
    NaoContem: 'Não contém',
    MaiorOuIgual: 'Maior ou igual',
    Maior: 'Maior',
    Menor: 'Menor',
    MenorOuIgual: 'Menor ou igual',
    Em: 'Em',
    NaoEm: 'Não em',
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function ParametrosDaRequisicao() { }
if (false) {
    /** @type {?} */
    ParametrosDaRequisicao.prototype.filtros;
    /** @type {?} */
    ParametrosDaRequisicao.prototype.ordenacoes;
    /** @type {?} */
    ParametrosDaRequisicao.prototype.paginacao;
    /** @type {?} */
    ParametrosDaRequisicao.prototype.pesquisa;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function Paginacao() { }
if (false) {
    /**
     * Pagina (NÃO É iniciado em zero)
     *
     * @type {?}
     */
    Paginacao.prototype.pagina;
    /**
     * Quantidade de itens por página
     *
     * @type {?}
     */
    Paginacao.prototype.itensPorPagina;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {number} */
const SearchMode = {
    FullTextSearch: 0,
    Filtro: 1,
};
SearchMode[SearchMode.FullTextSearch] = 'FullTextSearch';
SearchMode[SearchMode.Filtro] = 'Filtro';
/**
 * @record
 */
function Pesquisa() { }
if (false) {
    /** @type {?} */
    Pesquisa.prototype.termo;
    /** @type {?|undefined} */
    Pesquisa.prototype.config;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function Ordenacao() { }
if (false) {
    /** @type {?} */
    Ordenacao.prototype.campo;
    /** @type {?} */
    Ordenacao.prototype.direcao;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 * @template TReturn
 */
function Serializer() { }
if (false) {
    /**
     * @param {?} parametros
     * @return {?}
     */
    Serializer.prototype.serialize = function (parametros) { };
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const SERIALIZER_TOKEN = new InjectionToken('Serializer');
/**
 * @abstract
 */
class BaseSerializerService {
    constructor() {
        this.quantidadeDeFiltrosAlinhadosSuportados = Number.MAX_SAFE_INTEGER;
        this.pesquisaFiltroOperadorPadrao = FiltroOperador.Contem;
        this.pesquisaFiltroCompostoOperadorPadrao = FiltroCompostoOperador.Ou;
    }
    /**
     * @param {?} parametrosDaRequisicao
     * @return {?}
     */
    serializeFiltros(parametrosDaRequisicao) {
        if (!this.suportaFiltros) {
            throw new Error('Este serializador não suporta filtros!');
        }
        /** @type {?} */
        const formatoDaClausula = this.filtroClausulaFormato;
        /** @type {?} */
        const filtrosCompostoSerializado = this.serializeFiltroComposto(parametrosDaRequisicao.filtros);
        if (filtrosCompostoSerializado != null) {
            /** @type {?} */
            const filtrosSerializado = this.format(formatoDaClausula, { filtros: filtrosCompostoSerializado });
            return filtrosSerializado;
        }
        else {
            return null;
        }
    }
    /**
     * @param {?} filtroComposto
     * @param {?=} nivel
     * @return {?}
     */
    serializeFiltroComposto(filtroComposto, nivel = 1) {
        if (nivel > this.quantidadeDeFiltrosAlinhadosSuportados) {
            throw new Error(`A quantidade de filtros alinhados suportados é '${this.quantidadeDeFiltrosAlinhadosSuportados}' e foi alcançado o nível '${nivel}'!`);
        }
        /** @type {?} */
        const filtrosSerializadosArray = filtroComposto.filtros.map((/**
         * @param {?} filtro
         * @return {?}
         */
        filtro => {
            if (isFilterComposite(filtro)) {
                /** @type {?} */
                const formatoDosFiltros = this.filtroFormato;
                /** @type {?} */
                const proximoNivel = nivel + 1;
                /** @type {?} */
                const filtrosCompostoSerializado = this.serializeFiltroComposto(filtro, proximoNivel);
                /** @type {?} */
                const filtrosSerializado = this.format(formatoDosFiltros, { filtro: filtrosCompostoSerializado });
                return filtrosSerializado;
            }
            else {
                return this.serializeFiltro(filtro);
            }
        }));
        /** @type {?} */
        let filtroCompostoOperador = null;
        if (filtrosSerializadosArray.length > 1) {
            filtroCompostoOperador = this.getFiltroCompostoOperadorMapeado(filtroComposto.operador);
        }
        if (filtrosSerializadosArray.length > 0) {
            /** @type {?} */
            const filtrosSerializados = this.tratarArray(filtrosSerializadosArray, filtroCompostoOperador);
            return filtrosSerializados;
        }
        else {
            return null;
        }
    }
    /**
     * @param {?} filtro
     * @return {?}
     */
    serializeFiltro(filtro) {
        /** @type {?} */
        const filtroOperador = this.getFiltroOperadorMapeado(filtro.operador);
        /** @type {?} */
        const campo = this.filtroCampoInterceptor(filtro);
        /** @type {?} */
        const valor = this.filtroValorInterceptor(filtro);
        /** @type {?} */
        const filtroSerializado = this.format(filtroOperador, { campo, valor });
        return filtroSerializado;
    }
    /**
     * @protected
     * @param {?} stringArray
     * @param {?=} join
     * @return {?}
     */
    tratarArray(stringArray, join = '&') {
        /** @type {?} */
        const filtrosSerializados = stringArray.filter((/**
         * @param {?} s
         * @return {?}
         */
        s => s !== '' && s != null)).join(join);
        return filtrosSerializados;
    }
    /**
     * @protected
     * @param {?} filtro
     * @return {?}
     */
    filtroValorInterceptor(filtro) {
        return filtro.valor;
    }
    /**
     * @protected
     * @param {?} filtro
     * @return {?}
     */
    filtroCampoInterceptor(filtro) {
        return filtro.campo;
    }
    /**
     * @protected
     * @param {?} pesquisa
     * @return {?}
     */
    pesquisaTermoInterceptor(pesquisa) {
        return pesquisa.termo;
    }
    /**
     * @protected
     * @return {?}
     */
    get pesquisaModoPadrao() {
        if (this.suportaPesquisa) {
            return SearchMode.FullTextSearch;
        }
        else {
            if (this.suportaFiltros) {
                return SearchMode.Filtro;
            }
        }
    }
    /**
     * @param {?} parametrosDaRequisicao
     * @return {?}
     */
    serializePesquisa(parametrosDaRequisicao) {
        /** @type {?} */
        const termo = this.pesquisaTermoInterceptor(parametrosDaRequisicao.pesquisa);
        if (parametrosDaRequisicao.pesquisa.config == null) {
            parametrosDaRequisicao.pesquisa.config = { modo: this.pesquisaModoPadrao };
        }
        if (parametrosDaRequisicao.pesquisa.config.modo == null) {
            // Se não informar as configurações, deve prevalecer o que o serializador definiu como padrao
            parametrosDaRequisicao.pesquisa.config.modo = this.pesquisaModoPadrao;
        }
        if ((this.suportaFiltros && !this.suportaPesquisa) &&
            (parametrosDaRequisicao.pesquisa.config.modo === SearchMode.Filtro || this.pesquisaClausulaFormato == null)) {
            // Este é o caso quando a pesquisa é realizada por meio de filtro simples
            if (parametrosDaRequisicao.pesquisa.config.filtro.campos == null ||
                parametrosDaRequisicao.pesquisa.config.filtro.campos.length <= 0) {
                throw new Error('Na pesquisa com modo Filtro, os campos devem ser informados!');
            }
            /** @type {?} */
            const operador = parametrosDaRequisicao.pesquisa.config.filtro.operador ?
                parametrosDaRequisicao.pesquisa.config.filtro.operador : this.pesquisaFiltroOperadorPadrao;
            /** @type {?} */
            const filtros = parametrosDaRequisicao.pesquisa.config.filtro.campos.map((/**
             * @param {?} campo
             * @return {?}
             */
            campo => {
                /** @type {?} */
                const filtro = (/** @type {?} */ ({ campo, operador, valor: termo }));
                return filtro;
            }));
            /** @type {?} */
            const filtroComposto = (/** @type {?} */ ({ operador: this.pesquisaFiltroCompostoOperadorPadrao, filtros }));
            parametrosDaRequisicao.filtros.filtros.push(filtroComposto);
            return null;
        }
        else {
            if (!this.suportaPesquisa) {
                throw new Error('Este serializador não suporta pesquisa!');
            }
            /** @type {?} */
            const clausula = this.pesquisaClausulaFormato;
            /** @type {?} */
            const clausulaSerializada = this.format(clausula, { pesquisa: termo });
            return clausulaSerializada;
        }
    }
    /**
     * @param {?} parametros
     * @return {?}
     */
    serialize(parametros) {
        /** @type {?} */
        const serializaveis = [];
        if (parametros.pesquisa) {
            /** @type {?} */
            const serializacaoDaPesquisa = this.serializePesquisa(parametros);
            serializaveis.push(serializacaoDaPesquisa);
        }
        if (parametros.filtros) {
            /** @type {?} */
            const serializacaoDosFiltros = this.serializeFiltros(parametros);
            serializaveis.push(serializacaoDosFiltros);
        }
        if (parametros.paginacao) {
            /** @type {?} */
            const serializacaoDaPaginacao = this.serializePaginacao(parametros);
            serializaveis.push(serializacaoDaPaginacao);
        }
        if (parametros.ordenacoes) {
            /** @type {?} */
            const serializacaoDaOrdenacao = this.serializeOrdenacao(parametros);
            serializaveis.push(serializacaoDaOrdenacao);
        }
        if (this.serializeInterceptor) {
            /** @type {?} */
            const parametroSerializado = this.serializeInterceptor(parametros);
            serializaveis.push(parametroSerializado);
        }
        /** @type {?} */
        const parametrosSerializados = this.tratarArray(serializaveis);
        return parametrosSerializados;
    }
    /**
     * @param {?} parametrosDaRequisicao
     * @return {?}
     */
    serializePaginacao(parametrosDaRequisicao) {
        if (!this.suportaPaginacao) {
            throw new Error('Este serializador não suporta paginacao!');
        }
        /** @type {?} */
        const serializaveis = [];
        /** @type {?} */
        const paginacaoPaginaClausulaFormato = this.paginacaoPaginaClausulaFormato;
        /** @type {?} */
        const paginacaoPaginaParaFormatar = {
            pagina: this.paginacaoPaginaInterceptor(parametrosDaRequisicao.paginacao),
        };
        /** @type {?} */
        const paginacaoPaginaSerializado = this.format(paginacaoPaginaClausulaFormato, paginacaoPaginaParaFormatar);
        serializaveis.push(paginacaoPaginaSerializado);
        /** @type {?} */
        const paginacaoItensPorPaginaClausulaFormato = this.paginacaoItensPorPaginaClausulaFormato;
        /** @type {?} */
        const paginacaoItensPorPaginaParaFormatar = {
            itensPorPagina: this.paginacaoItensPorPaginaInterceptor(parametrosDaRequisicao.paginacao),
        };
        /** @type {?} */
        const paginacaoItensPorPaginaSerializado = this.format(paginacaoItensPorPaginaClausulaFormato, paginacaoItensPorPaginaParaFormatar);
        serializaveis.push(paginacaoItensPorPaginaSerializado);
        /** @type {?} */
        const parametrosSerializados = this.tratarArray(serializaveis);
        return parametrosSerializados;
    }
    /**
     * @protected
     * @param {?} paginacao
     * @return {?}
     */
    paginacaoPaginaInterceptor(paginacao) {
        return paginacao.pagina;
    }
    /**
     * @protected
     * @param {?} paginacao
     * @return {?}
     */
    paginacaoItensPorPaginaInterceptor(paginacao) {
        return paginacao.itensPorPagina;
    }
    /**
     * @param {?} parametrosDaRequisicao
     * @return {?}
     */
    serializeOrdenacao(parametrosDaRequisicao) {
        if (!this.suportaOrdenacao) {
            throw new Error('Este serializador não suporta ordenacao!');
        }
        // Name asc, Description desc
        /** @type {?} */
        const paginacaoArray = parametrosDaRequisicao.ordenacoes.map((/**
         * @param {?} ordenacao
         * @return {?}
         */
        ordenacao => {
            /** @type {?} */
            const ordenacaoFormato = this.ordenacaoFormato;
            // `{campo} {direcao}`;
            /** @type {?} */
            const ordenacaoParaFormatar = (/** @type {?} */ ({
                campo: this.ordenacaoCampoInterceptor(ordenacao),
                direcao: this.ordenacaoDirecaoInterceptor(ordenacao),
            }));
            /** @type {?} */
            const ordenacoesSerializadas = this.format(ordenacaoFormato, ordenacaoParaFormatar);
            return ordenacoesSerializadas;
        }));
        /** @type {?} */
        const ordenacoesSeparador = this.ordenacoesSeparador;
        /** @type {?} */
        const parametrosSerializados = this.tratarArray(paginacaoArray, ordenacoesSeparador);
        /** @type {?} */
        const ordenacaoClausula = this.ordenacaoClausulaFormato;
        // `$orderby={ordenacoes}`;
        /** @type {?} */
        const clausulaFormatada = this.format(ordenacaoClausula, { ordenacoes: parametrosSerializados });
        return clausulaFormatada;
    }
    /**
     * @protected
     * @param {?} ordenacao
     * @return {?}
     */
    ordenacaoCampoInterceptor(ordenacao) {
        return ordenacao.campo;
    }
    /**
     * @protected
     * @param {?} ordenacao
     * @return {?}
     */
    ordenacaoDirecaoInterceptor(ordenacao) {
        return ordenacao.direcao;
    }
    /**
     * @protected
     * @param {?} formato
     * @param {...?} args
     * @return {?}
     */
    format(formato, ...args) {
        if (typeof args[0] !== 'object') {
            return formato.replace(/{\d+}/g, (/**
             * @param {?} m
             * @return {?}
             */
            (m) => {
                /** @type {?} */
                const index = Number(m.replace(/\D/g, ''));
                return (args[index] ? args[index] : m);
            }));
        }
        else {
            /** @type {?} */
            const obj = args[0];
            return formato.replace(/{\w+}/g, (/**
             * @param {?} m
             * @return {?}
             */
            (m) => {
                /** @type {?} */
                const key = m.replace(/{|}/g, '');
                return (obj.hasOwnProperty(key) ? obj[key] : m);
            }));
        }
    }
}
if (false) {
    /**
     * Formato que sera serializado a
     * Tag correspondente a query string
     * enviada na requizição da pesquisa.
     *
     * A palavra chave para substituição pelo filtro é `{filtros}`
     *
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.filtroClausulaFormato;
    /**
     *  Corresponde a tag que sera aplicada na formatação dos filtros.
     *
     *  A palavra chave para substituição pelo filtro é `{filtro}`
     *
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.filtroFormato;
    /**
     * Habilita suporte para modo de pesquisa.
     *
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.suportaPesquisa;
    /**
     * Habilita suporte para modo de pesquisa utilizando a lógica com filtros.
     *
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.suportaFiltros;
    /**
     *  Habilita suporte para modo de pesquisa utilizando a lógica com paginação.
     *
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.suportaPaginacao;
    /**
     * Habilita suporte para modo de pesquisa utilizando a lógica com ordenação.
     *
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.suportaOrdenacao;
    /**
     * Formato que sera serializado a
     * Tag correspondente a query string 'skip'
     * enviada na requisição.
     *
     * A palavra chave para substituição é `{pagina}`
     *
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.paginacaoPaginaClausulaFormato;
    /**
     *  Corresponde a tag que sera aplicada na formatação das clausula dos itens por página.
     *
     *  A palavra chave para substituição é `{itensPorPagina}`
     *
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.paginacaoItensPorPaginaClausulaFormato;
    /**
     * Formato que sera serializado a
     * Tag correspondente a query string
     * enviada na requizição da ordernação da pesquisa.
     *
     * A palavra chave para substituição é `{ordenacoes}`
     *
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.ordenacaoClausulaFormato;
    /**
     *  Corresponde a tag que sera aplicada na formatação das clausula de ordenação.
     *
     *  As palavras chaves para substituição é `{campo}` e `{direcao}`
     *
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.ordenacaoFormato;
    /**
     *  Corresponde ao separador que sera aplicado na serialização das ordenações.
     *
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.ordenacoesSeparador;
    /**
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.quantidadeDeFiltrosAlinhadosSuportados;
    /**
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.pesquisaClausulaFormato;
    /**
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.pesquisaFiltroOperadorPadrao;
    /**
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.pesquisaFiltroCompostoOperadorPadrao;
    /**
     * @abstract
     * @protected
     * @param {?} filtroOperador
     * @return {?}
     */
    BaseSerializerService.prototype.getFiltroOperadorMapeado = function (filtroOperador) { };
    /**
     * @abstract
     * @protected
     * @param {?} FiltroCompostoOperador
     * @return {?}
     */
    BaseSerializerService.prototype.getFiltroCompostoOperadorMapeado = function (FiltroCompostoOperador) { };
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const ODataSerializerServiceConfigToken = new InjectionToken('ODataSerializerServiceConfigToken');
/**
 * @record
 */
function ODataSerializerServiceConfig() { }
if (false) {
    /** @type {?} */
    ODataSerializerServiceConfig.prototype.requestCount;
}
class ODataSerializerService extends BaseSerializerService {
    /**
     * @param {?=} oDataSerializerServiceConfig
     */
    constructor(oDataSerializerServiceConfig) {
        super();
        this.pesquisaFiltroOperadorPadrao = FiltroOperador.Contem;
        this.suportaPesquisa = false;
        this.suportaFiltros = true;
        this.suportaPaginacao = true;
        this.suportaOrdenacao = true;
        this.pesquisaClausulaFormato = `$search={pesquisa}`;
        this.ordenacaoClausulaFormato = `$orderby={ordenacoes}`;
        this.ordenacaoFormato = `{campo} {direcao}`;
        this.ordenacoesSeparador = `, `;
        this.paginacaoPaginaClausulaFormato = `$skip={pagina}`;
        this.paginacaoItensPorPaginaClausulaFormato = `$take={itensPorPagina}`;
        this.filtroClausulaFormato = `$filter={filtros}`;
        this.filtroFormato = `({filtro})`;
        /** @type {?} */
        const defaultParameter = (/** @type {?} */ ({ requestCount: true }));
        this.oDataConfig = Object.assign(defaultParameter, oDataSerializerServiceConfig);
    }
    /**
     * @protected
     * @param {?} parametro
     * @return {?}
     */
    serializeInterceptor(parametro) {
        if (this.oDataConfig.requestCount) {
            return `$count=true`;
        }
    }
    /**
     * @protected
     * @param {?} paginacao
     * @return {?}
     */
    paginacaoPaginaInterceptor(paginacao) {
        /** @type {?} */
        const paginaZeroBased = paginacao.pagina - 1;
        /** @type {?} */
        const pagina = paginaZeroBased * paginacao.itensPorPagina;
        return pagina;
    }
    /**
     * @protected
     * @param {?} filtroOperador
     * @return {?}
     */
    getFiltroOperadorMapeado(filtroOperador) {
        switch (filtroOperador) {
            case FiltroOperador.ComecaCom: {
                return `startswith({campo},{valor})`;
            }
            case FiltroOperador.Diferente: {
                return `{campo} ne {valor}`;
            }
            case FiltroOperador.Contem: {
                return `contains({campo},{valor})`;
            }
            case FiltroOperador.Igual: {
                return `{campo} eq {valor}`;
            }
            case FiltroOperador.Maior: {
                return `{campo} gt {valor}`;
            }
            case FiltroOperador.MaiorOuIgual: {
                return `{campo} ge {valor}`;
            }
            case FiltroOperador.Menor: {
                return `{campo} lt {valor}`;
            }
            case FiltroOperador.MenorOuIgual: {
                return `{campo} le {valor}`;
            }
            case FiltroOperador.Em: {
                return `{campo} in ({valor})`;
            }
            case FiltroOperador.NaoContem: {
                return `contains({campo},{valor}) eq false`;
            }
            case FiltroOperador.TerminaCom: {
                return `endswith({campo},{valor})`;
            }
        }
        throw new Error(`Não foi encontrado um operador equivalente a '${filtroOperador}'!`);
    }
    /**
     * @protected
     * @param {?} filtroCompostoOperador
     * @return {?}
     */
    getFiltroCompostoOperadorMapeado(filtroCompostoOperador) {
        switch (filtroCompostoOperador) {
            case FiltroCompostoOperador.E: {
                return ` and `;
            }
            case FiltroCompostoOperador.Ou: {
                return ` or `;
            }
        }
        throw new Error(`Não foi encontrado um operador equivalente a '${filtroCompostoOperador}'!`);
    }
    /**
     * @protected
     * @param {?} filtro
     * @return {?}
     */
    filtroValorInterceptor(filtro) {
        /** @type {?} */
        const valor = filtro.valor;
        if (valor instanceof Date) {
            return valor.toISOString();
        }
        else if (Array.isArray(valor)) {
            return "'" + valor.join("','") + "'";
        }
        else if (typeof valor === 'string') {
            return `'${valor}'`;
        }
        return valor;
    }
}
ODataSerializerService.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */
ODataSerializerService.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [ODataSerializerServiceConfigToken,] }, { type: Optional }] }
];
/** @nocollapse */ ODataSerializerService.ngInjectableDef = defineInjectable({ factory: function ODataSerializerService_Factory() { return new ODataSerializerService(inject(ODataSerializerServiceConfigToken, 8)); }, token: ODataSerializerService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @protected
     */
    ODataSerializerService.prototype.pesquisaFiltroOperadorPadrao;
    /**
     * @type {?}
     * @protected
     */
    ODataSerializerService.prototype.suportaPesquisa;
    /**
     * @type {?}
     * @protected
     */
    ODataSerializerService.prototype.suportaFiltros;
    /**
     * @type {?}
     * @protected
     */
    ODataSerializerService.prototype.suportaPaginacao;
    /**
     * @type {?}
     * @protected
     */
    ODataSerializerService.prototype.suportaOrdenacao;
    /** @type {?} */
    ODataSerializerService.prototype.pesquisaClausulaFormato;
    /** @type {?} */
    ODataSerializerService.prototype.oDataConfig;
    /** @type {?} */
    ODataSerializerService.prototype.ordenacaoClausulaFormato;
    /** @type {?} */
    ODataSerializerService.prototype.ordenacaoFormato;
    /** @type {?} */
    ODataSerializerService.prototype.ordenacoesSeparador;
    /** @type {?} */
    ODataSerializerService.prototype.paginacaoPaginaClausulaFormato;
    /** @type {?} */
    ODataSerializerService.prototype.paginacaoItensPorPaginaClausulaFormato;
    /** @type {?} */
    ODataSerializerService.prototype.filtroClausulaFormato;
    /** @type {?} */
    ODataSerializerService.prototype.filtroFormato;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class RestSerializerService extends BaseSerializerService {
    constructor() {
        super(...arguments);
        this.pesquisaFiltroOperadorPadrao = FiltroOperador.Contem;
        this.suportaPesquisa = false;
        this.suportaFiltros = true;
        this.suportaPaginacao = false;
        this.suportaOrdenacao = false;
        this.quantidadeDeFiltrosAlinhadosSuportados = 1;
        this.filtroClausulaFormato = '{filtros}';
        this.filtroFormato = '{filtro}';
    }
    /**
     * @protected
     * @param {?} filtroOperador
     * @return {?}
     */
    getFiltroOperadorMapeado(filtroOperador) {
        if (filtroOperador === FiltroOperador.Igual) {
            return '{campo}={valor}';
        }
        else {
            // tslint:disable-next-line:max-line-length
            throw new Error('Rest puro não suporta filtro que não seja o igual! Implemente seu próprio serializador para customizar como gerar a requisição devidamente!');
        }
    }
    /**
     * @protected
     * @param {?} filtroCompostoOperador
     * @return {?}
     */
    getFiltroCompostoOperadorMapeado(filtroCompostoOperador) {
        if (filtroCompostoOperador === FiltroCompostoOperador.E) {
            return '&';
        }
        else {
            // tslint:disable-next-line:max-line-length
            throw new Error('Rest puro não suporta filtros que utilizam "ou" como lógica! Implemente seu próprio serializador para customizar como gerar a requisição devidamente!');
        }
    }
}
RestSerializerService.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */ RestSerializerService.ngInjectableDef = defineInjectable({ factory: function RestSerializerService_Factory() { return new RestSerializerService(); }, token: RestSerializerService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @protected
     */
    RestSerializerService.prototype.pesquisaFiltroOperadorPadrao;
    /**
     * @type {?}
     * @protected
     */
    RestSerializerService.prototype.suportaPesquisa;
    /**
     * @type {?}
     * @protected
     */
    RestSerializerService.prototype.suportaFiltros;
    /**
     * @type {?}
     * @protected
     */
    RestSerializerService.prototype.suportaPaginacao;
    /**
     * @type {?}
     * @protected
     */
    RestSerializerService.prototype.suportaOrdenacao;
    /**
     * @type {?}
     * @protected
     */
    RestSerializerService.prototype.quantidadeDeFiltrosAlinhadosSuportados;
    /**
     * @type {?}
     * @protected
     */
    RestSerializerService.prototype.filtroClausulaFormato;
    /**
     * @type {?}
     * @protected
     */
    RestSerializerService.prototype.filtroFormato;
    /**
     * @type {?}
     * @protected
     */
    RestSerializerService.prototype.paginacaoPaginaClausulaFormato;
    /**
     * @type {?}
     * @protected
     */
    RestSerializerService.prototype.paginacaoItensPorPaginaClausulaFormato;
    /**
     * @type {?}
     * @protected
     */
    RestSerializerService.prototype.ordenacaoClausulaFormato;
    /**
     * @type {?}
     * @protected
     */
    RestSerializerService.prototype.ordenacaoFormato;
    /**
     * @type {?}
     * @protected
     */
    RestSerializerService.prototype.ordenacoesSeparador;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const PjeDataSerializerServiceConfigToken = new InjectionToken('PjeDataSerializerServiceConfigToken');
/**
 * @record
 */
function PjeDataSerializerServiceConfig() { }
if (false) {
    /** @type {?} */
    PjeDataSerializerServiceConfig.prototype.requestCount;
}
class PjeSerializerService extends BaseSerializerService {
    /**
     * @param {?=} oDataSerializerServiceConfig
     */
    constructor(oDataSerializerServiceConfig) {
        super();
        this.suportaPesquisa = false;
        this.suportaFiltros = true;
        this.suportaPaginacao = true;
        this.suportaOrdenacao = true;
        this.ordenacaoClausulaFormato = `order={ordenacoes}`;
        this.ordenacaoFormato = `{"{campo}":"{direcao}"}`;
        this.ordenacoesSeparador = `, `;
        this.paginacaoPaginaClausulaFormato = `page={"page":{pagina}, `;
        this.paginacaoItensPorPaginaClausulaFormato = `"size":{itensPorPagina}}`;
        this.filtroClausulaFormato = `filter={{filtros}}`;
        this.filtroFormato = `{filtro}`;
        this.pesquisaClausulaFormato = `simpleFilter={pesquisa}`;
        this.pesquisaFiltroCompostoOperadorPadrao = FiltroCompostoOperador.E;
        /** @type {?} */
        const defaultParameter = (/** @type {?} */ ({ requestCount: true }));
        this.dataConfig = Object.assign(defaultParameter, oDataSerializerServiceConfig);
    }
    /**
     * @protected
     * @param {?} paginacao
     * @return {?}
     */
    paginacaoPaginaInterceptor(paginacao) {
        /** @type {?} */
        const paginaZeroBased = paginacao.pagina - 1;
        return paginaZeroBased * paginacao.itensPorPagina;
    }
    /**
     * @protected
     * @param {?} filtroOperador
     * @return {?}
     */
    getFiltroOperadorMapeado(filtroOperador) {
        switch (filtroOperador) {
            case FiltroOperador.Igual: {
                return `"{campo}": {"eq": "{valor}"}`;
            }
            case FiltroOperador.Menor: {
                return `"{campo}": {"lt": "{valor}"}`;
            }
            case FiltroOperador.Maior: {
                return `"{campo}": {"gt": "{valor}"}`;
            }
            case FiltroOperador.MenorOuIgual: {
                return `"{campo}": {"le": "{valor}"}`;
            }
            case FiltroOperador.MaiorOuIgual: {
                return `"{campo}": {"ge": "{valor}"}`;
            }
            case FiltroOperador.Em: {
                return `"{campo}": {"in": ({valor})}`;
            }
            case FiltroOperador.NaoEm: {
                return `"{campo}": {"not in": ({valor})}`;
            }
            case FiltroOperador.ComecaCom: {
                return `"{campo}": {"starts-with": "{valor}"}`;
            }
            case FiltroOperador.TerminaCom: {
                return `"{campo}": {"ends-with": "{valor}"}`;
            }
            case FiltroOperador.Contem: {
                return `"{campo}": {"contains": "{valor}"}`;
            }
            case FiltroOperador.NaoContem: {
                return `"{campo}": {"not-contains": "{valor}"}`;
            }
        }
        throw new Error(`Não foi encontrado um operador equivalente a '${filtroOperador}'!`);
    }
    /**
     * @param {?} parametrosDaRequisicao
     * @return {?}
     */
    serializePaginacao(parametrosDaRequisicao) {
        if (!this.suportaPaginacao) {
            throw new Error('Este serializador não suporta paginacao!');
        }
        /** @type {?} */
        const serializaveis = [];
        /** @type {?} */
        const paginacaoPaginaClausulaFormato = this.paginacaoPaginaClausulaFormato;
        /** @type {?} */
        const paginacaoPaginaParaFormatar = {
            pagina: this.paginacaoPaginaInterceptor(parametrosDaRequisicao.paginacao),
        };
        /** @type {?} */
        const paginacaoPaginaSerializado = this.format(paginacaoPaginaClausulaFormato, paginacaoPaginaParaFormatar);
        serializaveis.push(paginacaoPaginaSerializado);
        /** @type {?} */
        const paginacaoItensPorPaginaClausulaFormato = this.paginacaoItensPorPaginaClausulaFormato;
        /** @type {?} */
        const paginacaoItensPorPaginaParaFormatar = {
            itensPorPagina: this.paginacaoItensPorPaginaInterceptor(parametrosDaRequisicao.paginacao),
        };
        /** @type {?} */
        const paginacaoItensPorPaginaSerializado = this.format(paginacaoItensPorPaginaClausulaFormato, paginacaoItensPorPaginaParaFormatar);
        serializaveis.push(paginacaoItensPorPaginaSerializado);
        return this.tratarArray(serializaveis, '');
    }
    /**
     * @protected
     * @param {?} filtroCompostoOperador
     * @return {?}
     */
    getFiltroCompostoOperadorMapeado(filtroCompostoOperador) {
        if (filtroCompostoOperador === FiltroCompostoOperador.E) {
            return ',';
        }
        else {
            // tslint:disable-next-line:max-line-length
            throw new Error('Rest puro não suporta filtros que utilizam "ou" como lógica! Implemente seu próprio serializador para customizar como gerar a requisição devidamente!');
        }
    }
    /**
     * @protected
     * @param {?} filtro
     * @return {?}
     */
    filtroValorInterceptor(filtro) {
        /** @type {?} */
        const valor = filtro.valor;
        if (valor instanceof Date) {
            return valor.toISOString();
        }
        else if (Array.isArray(valor)) {
            return `'${valor.join('\',\'')}'`;
        }
        else if (typeof valor === 'string') {
            return `'${valor}'`;
        }
        return valor;
    }
}
PjeSerializerService.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */
PjeSerializerService.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [PjeDataSerializerServiceConfigToken,] }, { type: Optional }] }
];
/** @nocollapse */ PjeSerializerService.ngInjectableDef = defineInjectable({ factory: function PjeSerializerService_Factory() { return new PjeSerializerService(inject(PjeDataSerializerServiceConfigToken, 8)); }, token: PjeSerializerService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @protected
     */
    PjeSerializerService.prototype.suportaPesquisa;
    /**
     * @type {?}
     * @protected
     */
    PjeSerializerService.prototype.suportaFiltros;
    /**
     * @type {?}
     * @protected
     */
    PjeSerializerService.prototype.suportaPaginacao;
    /**
     * @type {?}
     * @protected
     */
    PjeSerializerService.prototype.suportaOrdenacao;
    /**
     * @type {?}
     * @protected
     */
    PjeSerializerService.prototype.ordenacaoClausulaFormato;
    /**
     * @type {?}
     * @protected
     */
    PjeSerializerService.prototype.ordenacaoFormato;
    /**
     * @type {?}
     * @protected
     */
    PjeSerializerService.prototype.ordenacoesSeparador;
    /**
     * @type {?}
     * @protected
     */
    PjeSerializerService.prototype.paginacaoPaginaClausulaFormato;
    /**
     * @type {?}
     * @protected
     */
    PjeSerializerService.prototype.paginacaoItensPorPaginaClausulaFormato;
    /**
     * @type {?}
     * @protected
     */
    PjeSerializerService.prototype.filtroClausulaFormato;
    /**
     * @type {?}
     * @protected
     */
    PjeSerializerService.prototype.filtroFormato;
    /**
     * @type {?}
     * @protected
     */
    PjeSerializerService.prototype.pesquisaClausulaFormato;
    /**
     * @type {?}
     * @protected
     */
    PjeSerializerService.prototype.pesquisaFiltroCompostoOperadorPadrao;
    /** @type {?} */
    PjeSerializerService.prototype.dataConfig;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @template T
 * @param {?} callback
 * @return {?}
 */
function prepare(callback) {
    return (/**
     * @param {?} source
     * @return {?}
     */
    (source) => defer((/**
     * @return {?}
     */
    () => {
        callback();
        return source;
    })));
}
/**
 * @template T
 * @param {?} indicator
 * @return {?}
 */
function indicate(indicator) {
    return (/**
     * @param {?} source
     * @return {?}
     */
    (source) => source.pipe(prepare((/**
     * @return {?}
     */
    () => indicator.next(true))), finalize((/**
     * @return {?}
     */
    () => indicator.next(false)))));
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { AuthModule, AuthService, AuthenticationGuardService, AuthenticationService, AuthorizationConfigToken, AuthorizationGuardService, AuthorizationService, AutoFocusDirective, AutoUnsubscribe, BaseSerializerService, BreadcrumbService, ButtonDialog, DialogResult, DialogService, FAVORITOS_SERVICE_TOKEN, FavoritosService, FilterService, FiltroCompostoOperador, FiltroOperador, GuidService, IAuthenticationManagerToken, IAuthenticationServiceToken, IAuthorizationManagerToken, InitOidcAuthenticationService, KeycloakAuthenticationManager, LOG_CONSUMER_SERVICE, LayoutComponent, LayoutService, LayoutType, LogConsoleConsumerService, LogConsumer, LogConsumersService, LogLevel, LogModule, LogService, MenuItemComponent, NoRouteReuseStrategy, NotFoundModule, NotificacaoComponent, ODataSerializerService, ODataSerializerServiceConfigToken, OIDC_CONFIG, OidcAuthModule, OidcConfigDepHolder, OidcRoutingModule, OperationsManagerService, OperationsService, OperationsSnackBar, PjeDataSerializerServiceConfigToken, PjeSerializerService, PreviousRouteService, ProviderAuthenticationService, RestSerializerService, RouteAuthorizationGuardService, SEARCH_TOKEN, SERIALIZER_TOKEN, SaveButtonComponent, SaveButtonSelection, SearchAbstractService, SearchKeepService, SearchMode, SimpleDialogComponent, ToastAction, ToastComponent, ToastService, UikitComponentsModule, UikitModule, UikitSharedModule, UmaAuthorizationManager, UnauthorizedModule, UpdateComponent, UpdateInfoService, UpdateModule, UpdateService, UserAuthorizationManager, UserService, Wso2AuthenticationManager, XacmlAuthorizationManager, indicate, isFilterComposite, prepare, LayoutModule as ɵa, FavNavsService as ɵb, MenuSearchComponent as ɵba, IaComponent as ɵbb, breadcrumbServiceFactory as ɵbc, BreadcrumbModule as ɵbd, BreadcrumbComponent$1 as ɵbe, LoadingDirective as ɵbf, SpinnerDirective as ɵbg, SkeletonDirective as ɵbh, HighlightPipe as ɵbi, ToastModule as ɵbj, OfflineInterceptor as ɵbk, MatPaginatorIntlPtBr as ɵbl, SearchQuery as ɵbm, SearchStore as ɵbn, GuardService as ɵbo, OidcAuthHttpInterceptor as ɵbp, CallbackComponent as ɵbq, SilentRefreshComponent as ɵbr, ProtectedRouteComponent as ɵbs, LoginIframeComponent as ɵbt, NotFoundComponent as ɵbu, NotFoundRoutingModule as ɵbv, UnauthorizedComponent as ɵbw, UnauthorizedRoutingModule as ɵbx, FilterComponent as ɵby, FavNavsStore as ɵc, FavNavsQuery as ɵd, UikitRrippleService as ɵe, NavQuery as ɵf, NavStore as ɵg, NavService as ɵh, HeaderComponent as ɵi, NavComponent as ɵj, MenuComponent as ɵk, MenuSearchService as ɵl, FavnavComponent as ɵm, SearchComponent as ɵn, searchAnimation as ɵo, HighlightComponent as ɵp, SearchService as ɵq, SearchHistoricoService as ɵr, LocalRepository as ɵs, LocalStorageService as ɵt, NotificationComponent as ɵu, SysteminfoComponent as ɵv, AccessibilityComponent as ɵw, UserinfoComponent as ɵx, BreadcrumbComponent as ɵz };
//# sourceMappingURL=cnj-uikit.js.map
