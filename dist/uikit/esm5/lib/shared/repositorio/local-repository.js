/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { LocalStorageService } from './localstorage.service';
import * as i0 from "@angular/core";
import * as i1 from "./localstorage.service";
var LocalRepository = /** @class */ (function () {
    function LocalRepository(localStorageService) {
        this.localStorageService = localStorageService;
    }
    /**
     * @template T
     * @param {?} key
     * @param {?} data
     * @return {?}
     */
    LocalRepository.prototype.salvarItem = /**
     * @template T
     * @param {?} key
     * @param {?} data
     * @return {?}
     */
    function (key, data) {
        this.localStorageService.set(key, data);
    };
    /**
     * @template T
     * @param {?} key
     * @return {?}
     */
    LocalRepository.prototype.obterItem = /**
     * @template T
     * @param {?} key
     * @return {?}
     */
    function (key) {
        return (/** @type {?} */ (this.localStorageService.get(key)));
    };
    /**
     * @param {?} key
     * @return {?}
     */
    LocalRepository.prototype.deletar = /**
     * @param {?} key
     * @return {?}
     */
    function (key) {
        this.localStorageService.delete(key);
    };
    LocalRepository.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    LocalRepository.ctorParameters = function () { return [
        { type: LocalStorageService }
    ]; };
    /** @nocollapse */ LocalRepository.ngInjectableDef = i0.defineInjectable({ factory: function LocalRepository_Factory() { return new LocalRepository(i0.inject(i1.LocalStorageService)); }, token: LocalRepository, providedIn: "root" });
    return LocalRepository;
}());
export { LocalRepository };
if (false) {
    /**
     * @type {?}
     * @private
     */
    LocalRepository.prototype.localStorageService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9jYWwtcmVwb3NpdG9yeS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL3JlcG9zaXRvcmlvL2xvY2FsLXJlcG9zaXRvcnkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUNBLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFDLG1CQUFtQixFQUFDLE1BQU0sd0JBQXdCLENBQUM7OztBQUUzRDtJQUtFLHlCQUFvQixtQkFBd0M7UUFBeEMsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtJQUM1RCxDQUFDOzs7Ozs7O0lBRUQsb0NBQVU7Ozs7OztJQUFWLFVBQWMsR0FBVyxFQUFFLElBQU87UUFDaEMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDMUMsQ0FBQzs7Ozs7O0lBRUQsbUNBQVM7Ozs7O0lBQVQsVUFBYSxHQUFRO1FBQ25CLE9BQU8sbUJBQUEsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFBSyxDQUFDO0lBQ2hELENBQUM7Ozs7O0lBRUQsaUNBQU87Ozs7SUFBUCxVQUFRLEdBQVE7UUFDZCxJQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7O2dCQWxCRixVQUFVLFNBQUM7b0JBQ1YsVUFBVSxFQUFFLE1BQU07aUJBQ25COzs7O2dCQUpPLG1CQUFtQjs7OzBCQUYzQjtDQXVCQyxBQW5CRCxJQW1CQztTQWhCWSxlQUFlOzs7Ozs7SUFFZCw4Q0FBZ0QiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0lMb2NhbFJlcG9zaXRvcnl9IGZyb20gJy4vaWxvY2FsLXJlcG9zaXRvcnknO1xuaW1wb3J0IHtJbmplY3RhYmxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7TG9jYWxTdG9yYWdlU2VydmljZX0gZnJvbSAnLi9sb2NhbHN0b3JhZ2Uuc2VydmljZSc7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIExvY2FsUmVwb3NpdG9yeSBpbXBsZW1lbnRzIElMb2NhbFJlcG9zaXRvcnkge1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgbG9jYWxTdG9yYWdlU2VydmljZTogTG9jYWxTdG9yYWdlU2VydmljZSkge1xuICB9XG5cbiAgc2FsdmFySXRlbTxUPihrZXk6IHN0cmluZywgZGF0YTogVCkge1xuICAgIHRoaXMubG9jYWxTdG9yYWdlU2VydmljZS5zZXQoa2V5LCBkYXRhKTtcbiAgfVxuXG4gIG9idGVySXRlbTxUPihrZXk6IGFueSk6IFQge1xuICAgIHJldHVybiB0aGlzLmxvY2FsU3RvcmFnZVNlcnZpY2UuZ2V0KGtleSkgYXMgVDtcbiAgfVxuXG4gIGRlbGV0YXIoa2V5OiBhbnkpOiB2b2lkIHtcbiAgICB0aGlzLmxvY2FsU3RvcmFnZVNlcnZpY2UuZGVsZXRlKGtleSk7XG4gIH1cbn1cbiJdfQ==