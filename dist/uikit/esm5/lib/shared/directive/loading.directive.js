/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, Input, ElementRef, Renderer2, Host, Optional, Self, ViewContainerRef, ComponentFactoryResolver, } from '@angular/core';
import { MatTable } from "@angular/material/table";
import { SkeletonService } from "./skeleton.service";
import { SpinnerService } from "./spinner.service";
var LoadingDirective = /** @class */ (function () {
    function LoadingDirective(el, renderer, viewContainerRef, componentFactoryResolver, matTable) {
        this.el = el;
        this.renderer = renderer;
        this.viewContainerRef = viewContainerRef;
        this.componentFactoryResolver = componentFactoryResolver;
        this.matTable = matTable;
        if (matTable) {
            this.skeletonService = new SkeletonService(el, renderer);
        }
        else {
            this.spinnerService = new SpinnerService(el, renderer, viewContainerRef, componentFactoryResolver);
        }
    }
    Object.defineProperty(LoadingDirective.prototype, "isLoading", {
        set: /**
         * @param {?} condition
         * @return {?}
         */
        function (condition) {
            if (condition) {
                this.show();
            }
            else {
                this.hide();
            }
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    LoadingDirective.prototype.show = /**
     * @return {?}
     */
    function () {
        if (this.matTable) {
            this.skeletonService.show();
        }
        else {
            this.spinnerService.show();
        }
    };
    /**
     * @return {?}
     */
    LoadingDirective.prototype.hide = /**
     * @return {?}
     */
    function () {
        if (this.matTable) {
            this.skeletonService.hide();
        }
        else {
            this.spinnerService.hide();
        }
    };
    LoadingDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[uikitLoading]'
                },] }
    ];
    /** @nocollapse */
    LoadingDirective.ctorParameters = function () { return [
        { type: ElementRef },
        { type: Renderer2 },
        { type: ViewContainerRef },
        { type: ComponentFactoryResolver },
        { type: MatTable, decorators: [{ type: Host }, { type: Self }, { type: Optional }] }
    ]; };
    LoadingDirective.propDecorators = {
        isLoading: [{ type: Input, args: ['uikitLoading',] }]
    };
    return LoadingDirective;
}());
export { LoadingDirective };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    LoadingDirective.prototype.skeletonService;
    /**
     * @type {?}
     * @protected
     */
    LoadingDirective.prototype.spinnerService;
    /**
     * @type {?}
     * @protected
     */
    LoadingDirective.prototype.el;
    /**
     * @type {?}
     * @protected
     */
    LoadingDirective.prototype.renderer;
    /**
     * @type {?}
     * @protected
     */
    LoadingDirective.prototype.viewContainerRef;
    /**
     * @type {?}
     * @protected
     */
    LoadingDirective.prototype.componentFactoryResolver;
    /** @type {?} */
    LoadingDirective.prototype.matTable;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9hZGluZy5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9kaXJlY3RpdmUvbG9hZGluZy5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFDTCxTQUFTLEVBQ1QsS0FBSyxFQUNMLFVBQVUsRUFDVixTQUFTLEVBQ1QsSUFBSSxFQUNKLFFBQVEsRUFDUixJQUFJLEVBQUUsZ0JBQWdCLEVBQUUsd0JBQXdCLEdBQ2pELE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFBQyxRQUFRLEVBQUMsTUFBTSx5QkFBeUIsQ0FBQztBQUNqRCxPQUFPLEVBQUMsZUFBZSxFQUFDLE1BQU0sb0JBQW9CLENBQUM7QUFDbkQsT0FBTyxFQUFDLGNBQWMsRUFBQyxNQUFNLG1CQUFtQixDQUFDO0FBRWpEO0lBZUUsMEJBQ1ksRUFBYyxFQUNkLFFBQW1CLEVBQ25CLGdCQUFrQyxFQUNsQyx3QkFBa0QsRUFDekIsUUFBdUI7UUFKaEQsT0FBRSxHQUFGLEVBQUUsQ0FBWTtRQUNkLGFBQVEsR0FBUixRQUFRLENBQVc7UUFDbkIscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUNsQyw2QkFBd0IsR0FBeEIsd0JBQXdCLENBQTBCO1FBQ3pCLGFBQVEsR0FBUixRQUFRLENBQWU7UUFFMUQsSUFBSSxRQUFRLEVBQUU7WUFDWixJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksZUFBZSxDQUFDLEVBQUUsRUFBRSxRQUFRLENBQUMsQ0FBQztTQUMxRDthQUFNO1lBQ0wsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLGNBQWMsQ0FBQyxFQUFFLEVBQUUsUUFBUSxFQUFFLGdCQUFnQixFQUFFLHdCQUF3QixDQUFDLENBQUM7U0FDcEc7SUFDSCxDQUFDO0lBdkJELHNCQUEyQix1Q0FBUzs7Ozs7UUFBcEMsVUFBcUMsU0FBUztZQUM1QyxJQUFJLFNBQVMsRUFBRTtnQkFDYixJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7YUFDYjtpQkFBTTtnQkFDTCxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7YUFDYjtRQUNILENBQUM7OztPQUFBOzs7O0lBbUJELCtCQUFJOzs7SUFBSjtRQUNFLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNqQixJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksRUFBRSxDQUFDO1NBQzdCO2FBQU07WUFDTCxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxDQUFDO1NBQzVCO0lBQ0gsQ0FBQzs7OztJQUVELCtCQUFJOzs7SUFBSjtRQUNFLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNqQixJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksRUFBRSxDQUFDO1NBQzdCO2FBQU07WUFDTCxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxDQUFDO1NBQzVCO0lBQ0gsQ0FBQzs7Z0JBM0NGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsZ0JBQWdCO2lCQUMzQjs7OztnQkFaQyxVQUFVO2dCQUNWLFNBQVM7Z0JBR0gsZ0JBQWdCO2dCQUFFLHdCQUF3QjtnQkFFMUMsUUFBUSx1QkF3QlgsSUFBSSxZQUFJLElBQUksWUFBSSxRQUFROzs7NEJBaEIxQixLQUFLLFNBQUMsY0FBYzs7SUF3Q3ZCLHVCQUFDO0NBQUEsQUE1Q0QsSUE0Q0M7U0F6Q1ksZ0JBQWdCOzs7Ozs7SUFTM0IsMkNBQTJDOzs7OztJQUMzQywwQ0FBeUM7Ozs7O0lBR3ZDLDhCQUF3Qjs7Ozs7SUFDeEIsb0NBQTZCOzs7OztJQUM3Qiw0Q0FBNEM7Ozs7O0lBQzVDLG9EQUE0RDs7SUFDNUQsb0NBQTBEIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcbiAgRGlyZWN0aXZlLFxuICBJbnB1dCxcbiAgRWxlbWVudFJlZixcbiAgUmVuZGVyZXIyLFxuICBIb3N0LFxuICBPcHRpb25hbCxcbiAgU2VsZiwgVmlld0NvbnRhaW5lclJlZiwgQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyLFxufSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7TWF0VGFibGV9IGZyb20gXCJAYW5ndWxhci9tYXRlcmlhbC90YWJsZVwiO1xuaW1wb3J0IHtTa2VsZXRvblNlcnZpY2V9IGZyb20gXCIuL3NrZWxldG9uLnNlcnZpY2VcIjtcbmltcG9ydCB7U3Bpbm5lclNlcnZpY2V9IGZyb20gXCIuL3NwaW5uZXIuc2VydmljZVwiO1xuXG5ARGlyZWN0aXZlKHtcbiAgc2VsZWN0b3I6ICdbdWlraXRMb2FkaW5nXSdcbn0pXG5leHBvcnQgY2xhc3MgTG9hZGluZ0RpcmVjdGl2ZSB7XG4gIEBJbnB1dCgndWlraXRMb2FkaW5nJykgc2V0IGlzTG9hZGluZyhjb25kaXRpb24pIHtcbiAgICBpZiAoY29uZGl0aW9uKSB7XG4gICAgICB0aGlzLnNob3coKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5oaWRlKCk7XG4gICAgfVxuICB9XG5cbiAgcHJvdGVjdGVkIHNrZWxldG9uU2VydmljZTogU2tlbGV0b25TZXJ2aWNlO1xuICBwcm90ZWN0ZWQgc3Bpbm5lclNlcnZpY2U6IFNwaW5uZXJTZXJ2aWNlO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByb3RlY3RlZCBlbDogRWxlbWVudFJlZixcbiAgICBwcm90ZWN0ZWQgcmVuZGVyZXI6IFJlbmRlcmVyMixcbiAgICBwcm90ZWN0ZWQgdmlld0NvbnRhaW5lclJlZjogVmlld0NvbnRhaW5lclJlZixcbiAgICBwcm90ZWN0ZWQgY29tcG9uZW50RmFjdG9yeVJlc29sdmVyOiBDb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIsXG4gICAgQEhvc3QoKSBAU2VsZigpIEBPcHRpb25hbCgpIHB1YmxpYyBtYXRUYWJsZTogTWF0VGFibGU8YW55PlxuICApIHtcbiAgICBpZiAobWF0VGFibGUpIHtcbiAgICAgIHRoaXMuc2tlbGV0b25TZXJ2aWNlID0gbmV3IFNrZWxldG9uU2VydmljZShlbCwgcmVuZGVyZXIpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnNwaW5uZXJTZXJ2aWNlID0gbmV3IFNwaW5uZXJTZXJ2aWNlKGVsLCByZW5kZXJlciwgdmlld0NvbnRhaW5lclJlZiwgY29tcG9uZW50RmFjdG9yeVJlc29sdmVyKTtcbiAgICB9XG4gIH1cblxuICBzaG93KCkge1xuICAgIGlmICh0aGlzLm1hdFRhYmxlKSB7XG4gICAgICB0aGlzLnNrZWxldG9uU2VydmljZS5zaG93KCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuc3Bpbm5lclNlcnZpY2Uuc2hvdygpO1xuICAgIH1cbiAgfVxuXG4gIGhpZGUoKSB7XG4gICAgaWYgKHRoaXMubWF0VGFibGUpIHtcbiAgICAgIHRoaXMuc2tlbGV0b25TZXJ2aWNlLmhpZGUoKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5zcGlubmVyU2VydmljZS5oaWRlKCk7XG4gICAgfVxuICB9XG59XG4iXX0=