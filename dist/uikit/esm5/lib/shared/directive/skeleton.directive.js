/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, Input, ElementRef, Renderer2, } from '@angular/core';
import { SkeletonService } from "./skeleton.service";
var SkeletonDirective = /** @class */ (function () {
    function SkeletonDirective(el, renderer) {
        this.el = el;
        this.renderer = renderer;
        this.skeletonService = new SkeletonService(el, renderer);
    }
    Object.defineProperty(SkeletonDirective.prototype, "isLoading", {
        set: /**
         * @param {?} condition
         * @return {?}
         */
        function (condition) {
            if (condition) {
                this.skeletonService.show();
            }
            else {
                this.skeletonService.hide();
            }
        },
        enumerable: true,
        configurable: true
    });
    SkeletonDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[uikitSkeleton]'
                },] }
    ];
    /** @nocollapse */
    SkeletonDirective.ctorParameters = function () { return [
        { type: ElementRef },
        { type: Renderer2 }
    ]; };
    SkeletonDirective.propDecorators = {
        isLoading: [{ type: Input, args: ['uikitSkeleton',] }]
    };
    return SkeletonDirective;
}());
export { SkeletonDirective };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    SkeletonDirective.prototype.skeletonService;
    /**
     * @type {?}
     * @protected
     */
    SkeletonDirective.prototype.el;
    /**
     * @type {?}
     * @protected
     */
    SkeletonDirective.prototype.renderer;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2tlbGV0b24uZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvZGlyZWN0aXZlL3NrZWxldG9uLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUNMLFNBQVMsRUFDVCxLQUFLLEVBQ0wsVUFBVSxFQUNWLFNBQVMsR0FDVixNQUFNLGVBQWUsQ0FBQztBQUN2QixPQUFPLEVBQUMsZUFBZSxFQUFDLE1BQU0sb0JBQW9CLENBQUM7QUFFbkQ7SUFjRSwyQkFBc0IsRUFBYyxFQUNkLFFBQW1CO1FBRG5CLE9BQUUsR0FBRixFQUFFLENBQVk7UUFDZCxhQUFRLEdBQVIsUUFBUSxDQUFXO1FBQ3ZDLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxlQUFlLENBQUMsRUFBRSxFQUFFLFFBQVEsQ0FBQyxDQUFDO0lBQzNELENBQUM7SUFiRCxzQkFBNEIsd0NBQVM7Ozs7O1FBQXJDLFVBQXNDLFNBQVM7WUFDN0MsSUFBSSxTQUFTLEVBQUU7Z0JBQ2IsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLEVBQUUsQ0FBQzthQUM3QjtpQkFBTTtnQkFDTCxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksRUFBRSxDQUFDO2FBQzdCO1FBQ0gsQ0FBQzs7O09BQUE7O2dCQVZGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsaUJBQWlCO2lCQUM1Qjs7OztnQkFQQyxVQUFVO2dCQUNWLFNBQVM7Ozs0QkFRUixLQUFLLFNBQUMsZUFBZTs7SUFjeEIsd0JBQUM7Q0FBQSxBQWxCRCxJQWtCQztTQWZZLGlCQUFpQjs7Ozs7O0lBUzVCLDRDQUEwQzs7Ozs7SUFFOUIsK0JBQXdCOzs7OztJQUN4QixxQ0FBNkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xuICBEaXJlY3RpdmUsXG4gIElucHV0LFxuICBFbGVtZW50UmVmLFxuICBSZW5kZXJlcjIsXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtTa2VsZXRvblNlcnZpY2V9IGZyb20gXCIuL3NrZWxldG9uLnNlcnZpY2VcIjtcblxuQERpcmVjdGl2ZSh7XG4gIHNlbGVjdG9yOiAnW3Vpa2l0U2tlbGV0b25dJ1xufSlcbmV4cG9ydCBjbGFzcyBTa2VsZXRvbkRpcmVjdGl2ZSB7XG4gIEBJbnB1dCgndWlraXRTa2VsZXRvbicpIHNldCBpc0xvYWRpbmcoY29uZGl0aW9uKSB7XG4gICAgaWYgKGNvbmRpdGlvbikge1xuICAgICAgdGhpcy5za2VsZXRvblNlcnZpY2Uuc2hvdygpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnNrZWxldG9uU2VydmljZS5oaWRlKCk7XG4gICAgfVxuICB9XG5cbiAgcHJvdGVjdGVkIHNrZWxldG9uU2VydmljZTogU2tlbGV0b25TZXJ2aWNlXG5cbiAgY29uc3RydWN0b3IocHJvdGVjdGVkIGVsOiBFbGVtZW50UmVmLFxuICAgICAgICAgICAgICBwcm90ZWN0ZWQgcmVuZGVyZXI6IFJlbmRlcmVyMiwpIHtcbiAgICB0aGlzLnNrZWxldG9uU2VydmljZSA9IG5ldyBTa2VsZXRvblNlcnZpY2UoZWwsIHJlbmRlcmVyKTtcbiAgfVxufVxuIl19