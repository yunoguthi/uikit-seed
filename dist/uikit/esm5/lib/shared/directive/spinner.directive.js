/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, Input, ElementRef, Renderer2, ViewContainerRef, ComponentFactoryResolver, } from '@angular/core';
import { SpinnerService } from "./spinner.service";
var SpinnerDirective = /** @class */ (function () {
    function SpinnerDirective(el, renderer, viewContainerRef, componentFactoryResolver) {
        this.el = el;
        this.renderer = renderer;
        this.viewContainerRef = viewContainerRef;
        this.componentFactoryResolver = componentFactoryResolver;
        this.spinnerService = new SpinnerService(el, renderer, viewContainerRef, componentFactoryResolver);
    }
    Object.defineProperty(SpinnerDirective.prototype, "isLoading", {
        set: /**
         * @param {?} condition
         * @return {?}
         */
        function (condition) {
            if (condition) {
                this.spinnerService.show();
            }
            else {
                this.spinnerService.hide();
            }
        },
        enumerable: true,
        configurable: true
    });
    SpinnerDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[uikitSpinner]'
                },] }
    ];
    /** @nocollapse */
    SpinnerDirective.ctorParameters = function () { return [
        { type: ElementRef },
        { type: Renderer2 },
        { type: ViewContainerRef },
        { type: ComponentFactoryResolver }
    ]; };
    SpinnerDirective.propDecorators = {
        isLoading: [{ type: Input, args: ['uikitSpinner',] }]
    };
    return SpinnerDirective;
}());
export { SpinnerDirective };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    SpinnerDirective.prototype.spinnerService;
    /**
     * @type {?}
     * @protected
     */
    SpinnerDirective.prototype.el;
    /**
     * @type {?}
     * @protected
     */
    SpinnerDirective.prototype.renderer;
    /**
     * @type {?}
     * @protected
     */
    SpinnerDirective.prototype.viewContainerRef;
    /**
     * @type {?}
     * @protected
     */
    SpinnerDirective.prototype.componentFactoryResolver;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3Bpbm5lci5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9kaXJlY3RpdmUvc3Bpbm5lci5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFDTCxTQUFTLEVBQ1QsS0FBSyxFQUNMLFVBQVUsRUFDVixTQUFTLEVBQ1QsZ0JBQWdCLEVBQ2hCLHdCQUF3QixHQUN6QixNQUFNLGVBQWUsQ0FBQztBQUN2QixPQUFPLEVBQUMsY0FBYyxFQUFDLE1BQU0sbUJBQW1CLENBQUM7QUFFakQ7SUFjRSwwQkFDWSxFQUFjLEVBQ2QsUUFBbUIsRUFDbkIsZ0JBQWtDLEVBQ2xDLHdCQUFrRDtRQUhsRCxPQUFFLEdBQUYsRUFBRSxDQUFZO1FBQ2QsYUFBUSxHQUFSLFFBQVEsQ0FBVztRQUNuQixxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBQ2xDLDZCQUF3QixHQUF4Qix3QkFBd0IsQ0FBMEI7UUFFNUQsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLGNBQWMsQ0FBQyxFQUFFLEVBQUUsUUFBUSxFQUFFLGdCQUFnQixFQUFFLHdCQUF3QixDQUFDLENBQUM7SUFDckcsQ0FBQztJQWpCRCxzQkFBMkIsdUNBQVM7Ozs7O1FBQXBDLFVBQXFDLFNBQVM7WUFDNUMsSUFBSSxTQUFTLEVBQUU7Z0JBQ2IsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsQ0FBQzthQUM1QjtpQkFBTTtnQkFDTCxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxDQUFDO2FBQzVCO1FBQ0gsQ0FBQzs7O09BQUE7O2dCQVZGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsZ0JBQWdCO2lCQUMzQjs7OztnQkFUQyxVQUFVO2dCQUNWLFNBQVM7Z0JBQ1QsZ0JBQWdCO2dCQUNoQix3QkFBd0I7Ozs0QkFRdkIsS0FBSyxTQUFDLGNBQWM7O0lBa0J2Qix1QkFBQztDQUFBLEFBdEJELElBc0JDO1NBbkJZLGdCQUFnQjs7Ozs7O0lBUzNCLDBDQUF5Qzs7Ozs7SUFHdkMsOEJBQXdCOzs7OztJQUN4QixvQ0FBNkI7Ozs7O0lBQzdCLDRDQUE0Qzs7Ozs7SUFDNUMsb0RBQTREIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcbiAgRGlyZWN0aXZlLFxuICBJbnB1dCxcbiAgRWxlbWVudFJlZixcbiAgUmVuZGVyZXIyLFxuICBWaWV3Q29udGFpbmVyUmVmLFxuICBDb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIsXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtTcGlubmVyU2VydmljZX0gZnJvbSBcIi4vc3Bpbm5lci5zZXJ2aWNlXCI7XG5cbkBEaXJlY3RpdmUoe1xuICBzZWxlY3RvcjogJ1t1aWtpdFNwaW5uZXJdJ1xufSlcbmV4cG9ydCBjbGFzcyBTcGlubmVyRGlyZWN0aXZlIHtcbiAgQElucHV0KCd1aWtpdFNwaW5uZXInKSBzZXQgaXNMb2FkaW5nKGNvbmRpdGlvbikge1xuICAgIGlmIChjb25kaXRpb24pIHtcbiAgICAgIHRoaXMuc3Bpbm5lclNlcnZpY2Uuc2hvdygpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnNwaW5uZXJTZXJ2aWNlLmhpZGUoKTtcbiAgICB9XG4gIH1cblxuICBwcm90ZWN0ZWQgc3Bpbm5lclNlcnZpY2U6IFNwaW5uZXJTZXJ2aWNlO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByb3RlY3RlZCBlbDogRWxlbWVudFJlZixcbiAgICBwcm90ZWN0ZWQgcmVuZGVyZXI6IFJlbmRlcmVyMixcbiAgICBwcm90ZWN0ZWQgdmlld0NvbnRhaW5lclJlZjogVmlld0NvbnRhaW5lclJlZixcbiAgICBwcm90ZWN0ZWQgY29tcG9uZW50RmFjdG9yeVJlc29sdmVyOiBDb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIsXG4gICkge1xuICAgIHRoaXMuc3Bpbm5lclNlcnZpY2UgPSBuZXcgU3Bpbm5lclNlcnZpY2UoZWwsIHJlbmRlcmVyLCB2aWV3Q29udGFpbmVyUmVmLCBjb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIpO1xuICB9XG59XG4iXX0=