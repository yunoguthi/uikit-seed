/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { MatSpinner } from "@angular/material/progress-spinner";
var SpinnerService = /** @class */ (function () {
    function SpinnerService(el, renderer, viewContainerRef, componentFactoryResolver) {
        this.el = el;
        this.renderer = renderer;
        this.viewContainerRef = viewContainerRef;
        this.componentFactoryResolver = componentFactoryResolver;
        this.Init();
    }
    /**
     * @return {?}
     */
    SpinnerService.prototype.Init = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var factory = this.componentFactoryResolver.resolveComponentFactory(MatSpinner);
        /** @type {?} */
        var componentRef = this.viewContainerRef.createComponent(factory);
        this.divCenter = this.renderer.createElement('div');
        this.spinner = componentRef.instance;
        this.spinner.strokeWidth = 3;
        this.spinner.diameter = 24;
        this.renderer.addClass(this.divCenter, 'uikit-container-spinner');
        this.renderer.addClass(this.spinner._elementRef.nativeElement, 'uikit-spinner');
        this.renderer.setStyle(this.spinner._elementRef.nativeElement, 'display', 'none');
        /** @type {?} */
        var spanButton = (/** @type {?} */ (this.el.nativeElement.querySelector('.mat-button-wrapper')));
        if (spanButton) {
            this.renderer.setStyle(spanButton, 'display', 'flex');
            this.renderer.setStyle(spanButton, 'align-items', 'center');
            this.renderer.setStyle(spanButton, 'justify-content', 'center');
        }
    };
    /**
     * @return {?}
     */
    SpinnerService.prototype.hide = /**
     * @return {?}
     */
    function () {
        this.renderer.setStyle(this.spinner._elementRef.nativeElement, 'display', 'none');
        this.renderer.removeChild(this.el.nativeElement.firstChild, this.spinner._elementRef.nativeElement);
        this.renderer.removeChild(this.el.nativeElement.firstChild, this.divCenter);
        this.el.nativeElement.disabled = false;
    };
    /**
     * @return {?}
     */
    SpinnerService.prototype.show = /**
     * @return {?}
     */
    function () {
        this.renderer.appendChild(this.el.nativeElement.firstChild, this.divCenter);
        this.renderer.appendChild(this.el.nativeElement.firstChild, this.spinner._elementRef.nativeElement);
        this.renderer.setStyle(this.spinner._elementRef.nativeElement, 'display', 'inherit');
        this.el.nativeElement.disabled = true;
    };
    return SpinnerService;
}());
export { SpinnerService };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    SpinnerService.prototype.spinner;
    /**
     * @type {?}
     * @protected
     */
    SpinnerService.prototype.divCenter;
    /**
     * @type {?}
     * @protected
     */
    SpinnerService.prototype.el;
    /**
     * @type {?}
     * @protected
     */
    SpinnerService.prototype.renderer;
    /**
     * @type {?}
     * @protected
     */
    SpinnerService.prototype.viewContainerRef;
    /**
     * @type {?}
     * @protected
     */
    SpinnerService.prototype.componentFactoryResolver;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3Bpbm5lci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvZGlyZWN0aXZlL3NwaW5uZXIuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQ0EsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLG9DQUFvQyxDQUFDO0FBRTlEO0lBSUUsd0JBQ1ksRUFBYyxFQUNkLFFBQW1CLEVBQ25CLGdCQUFrQyxFQUNsQyx3QkFBa0Q7UUFIbEQsT0FBRSxHQUFGLEVBQUUsQ0FBWTtRQUNkLGFBQVEsR0FBUixRQUFRLENBQVc7UUFDbkIscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUNsQyw2QkFBd0IsR0FBeEIsd0JBQXdCLENBQTBCO1FBRTVELElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUNkLENBQUM7Ozs7SUFFTSw2QkFBSTs7O0lBQVg7O1lBQ1EsT0FBTyxHQUFHLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyx1QkFBdUIsQ0FBQyxVQUFVLENBQUM7O1lBQzNFLFlBQVksR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQztRQUNuRSxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBRXBELElBQUksQ0FBQyxPQUFPLEdBQUcsWUFBWSxDQUFDLFFBQVEsQ0FBQztRQUNyQyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsR0FBRyxDQUFDLENBQUM7UUFDN0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO1FBQzNCLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUseUJBQXlCLENBQUMsQ0FBQztRQUNsRSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsZUFBZSxDQUFDLENBQUM7UUFDaEYsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLFNBQVMsRUFBRSxNQUFNLENBQUMsQ0FBQzs7WUFFNUUsVUFBVSxHQUFHLG1CQUFBLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxxQkFBcUIsQ0FBQyxFQUFtQjtRQUNoRyxJQUFJLFVBQVUsRUFBRTtZQUNkLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSxTQUFTLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDdEQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLGFBQWEsRUFBRSxRQUFRLENBQUMsQ0FBQztZQUM1RCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsaUJBQWlCLEVBQUUsUUFBUSxDQUFDLENBQUM7U0FDakU7SUFDSCxDQUFDOzs7O0lBRU0sNkJBQUk7OztJQUFYO1FBQ0UsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLFNBQVMsRUFBRSxNQUFNLENBQUMsQ0FBQztRQUNsRixJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDcEcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUM1RSxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO0lBQ3pDLENBQUM7Ozs7SUFFTSw2QkFBSTs7O0lBQVg7UUFDRSxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQzVFLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNwRyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsU0FBUyxFQUFFLFNBQVMsQ0FBQyxDQUFDO1FBQ3JGLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7SUFDeEMsQ0FBQztJQUNILHFCQUFDO0FBQUQsQ0FBQyxBQTlDRCxJQThDQzs7Ozs7OztJQTdDQyxpQ0FBOEI7Ozs7O0lBQzlCLG1DQUF5Qjs7Ozs7SUFHdkIsNEJBQXdCOzs7OztJQUN4QixrQ0FBNkI7Ozs7O0lBQzdCLDBDQUE0Qzs7Ozs7SUFDNUMsa0RBQTREIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIsIEVsZW1lbnRSZWYsIFJlbmRlcmVyMiwgVmlld0NvbnRhaW5lclJlZn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge01hdFNwaW5uZXJ9IGZyb20gXCJAYW5ndWxhci9tYXRlcmlhbC9wcm9ncmVzcy1zcGlubmVyXCI7XG5cbmV4cG9ydCBjbGFzcyBTcGlubmVyU2VydmljZSB7XG4gIHByb3RlY3RlZCBzcGlubmVyOiBNYXRTcGlubmVyO1xuICBwcm90ZWN0ZWQgZGl2Q2VudGVyOiBhbnk7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJvdGVjdGVkIGVsOiBFbGVtZW50UmVmLFxuICAgIHByb3RlY3RlZCByZW5kZXJlcjogUmVuZGVyZXIyLFxuICAgIHByb3RlY3RlZCB2aWV3Q29udGFpbmVyUmVmOiBWaWV3Q29udGFpbmVyUmVmLFxuICAgIHByb3RlY3RlZCBjb21wb25lbnRGYWN0b3J5UmVzb2x2ZXI6IENvbXBvbmVudEZhY3RvcnlSZXNvbHZlcixcbiAgKSB7XG4gICAgdGhpcy5Jbml0KCk7XG4gIH1cblxuICBwdWJsaWMgSW5pdCgpIHtcbiAgICBjb25zdCBmYWN0b3J5ID0gdGhpcy5jb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIucmVzb2x2ZUNvbXBvbmVudEZhY3RvcnkoTWF0U3Bpbm5lcik7XG4gICAgY29uc3QgY29tcG9uZW50UmVmID0gdGhpcy52aWV3Q29udGFpbmVyUmVmLmNyZWF0ZUNvbXBvbmVudChmYWN0b3J5KTtcbiAgICB0aGlzLmRpdkNlbnRlciA9IHRoaXMucmVuZGVyZXIuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG5cbiAgICB0aGlzLnNwaW5uZXIgPSBjb21wb25lbnRSZWYuaW5zdGFuY2U7XG4gICAgdGhpcy5zcGlubmVyLnN0cm9rZVdpZHRoID0gMztcbiAgICB0aGlzLnNwaW5uZXIuZGlhbWV0ZXIgPSAyNDtcbiAgICB0aGlzLnJlbmRlcmVyLmFkZENsYXNzKHRoaXMuZGl2Q2VudGVyLCAndWlraXQtY29udGFpbmVyLXNwaW5uZXInKTtcbiAgICB0aGlzLnJlbmRlcmVyLmFkZENsYXNzKHRoaXMuc3Bpbm5lci5fZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LCAndWlraXQtc3Bpbm5lcicpO1xuICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUodGhpcy5zcGlubmVyLl9lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQsICdkaXNwbGF5JywgJ25vbmUnKTtcblxuICAgIGNvbnN0IHNwYW5CdXR0b24gPSB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQucXVlcnlTZWxlY3RvcignLm1hdC1idXR0b24td3JhcHBlcicpIGFzIEhUTUxTcGFuRWxlbWVudDtcbiAgICBpZiAoc3BhbkJ1dHRvbikge1xuICAgICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZShzcGFuQnV0dG9uLCAnZGlzcGxheScsICdmbGV4Jyk7XG4gICAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKHNwYW5CdXR0b24sICdhbGlnbi1pdGVtcycsICdjZW50ZXInKTtcbiAgICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUoc3BhbkJ1dHRvbiwgJ2p1c3RpZnktY29udGVudCcsICdjZW50ZXInKTtcbiAgICB9XG4gIH1cblxuICBwdWJsaWMgaGlkZSgpOiB2b2lkIHtcbiAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKHRoaXMuc3Bpbm5lci5fZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LCAnZGlzcGxheScsICdub25lJyk7XG4gICAgdGhpcy5yZW5kZXJlci5yZW1vdmVDaGlsZCh0aGlzLmVsLm5hdGl2ZUVsZW1lbnQuZmlyc3RDaGlsZCwgdGhpcy5zcGlubmVyLl9lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQpO1xuICAgIHRoaXMucmVuZGVyZXIucmVtb3ZlQ2hpbGQodGhpcy5lbC5uYXRpdmVFbGVtZW50LmZpcnN0Q2hpbGQsIHRoaXMuZGl2Q2VudGVyKTtcbiAgICB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQuZGlzYWJsZWQgPSBmYWxzZTtcbiAgfVxuXG4gIHB1YmxpYyBzaG93KCk6IHZvaWQge1xuICAgIHRoaXMucmVuZGVyZXIuYXBwZW5kQ2hpbGQodGhpcy5lbC5uYXRpdmVFbGVtZW50LmZpcnN0Q2hpbGQsIHRoaXMuZGl2Q2VudGVyKTtcbiAgICB0aGlzLnJlbmRlcmVyLmFwcGVuZENoaWxkKHRoaXMuZWwubmF0aXZlRWxlbWVudC5maXJzdENoaWxkLCB0aGlzLnNwaW5uZXIuX2VsZW1lbnRSZWYubmF0aXZlRWxlbWVudCk7XG4gICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0aGlzLnNwaW5uZXIuX2VsZW1lbnRSZWYubmF0aXZlRWxlbWVudCwgJ2Rpc3BsYXknLCAnaW5oZXJpdCcpO1xuICAgIHRoaXMuZWwubmF0aXZlRWxlbWVudC5kaXNhYmxlZCA9IHRydWU7XG4gIH1cbn1cbiJdfQ==