/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
var HighlightPipe = /** @class */ (function () {
    function HighlightPipe() {
    }
    /**
     * @param {?} value
     * @param {?} term
     * @return {?}
     */
    HighlightPipe.prototype.transform = /**
     * @param {?} value
     * @param {?} term
     * @return {?}
     */
    function (value, term) {
        if (!value) {
            return '';
        }
        /** @type {?} */
        var regex = this.createRegex(term);
        if (!regex) {
            return value;
        }
        // tslint:disable-next-line:only-arrow-functions
        value = value.replace(regex, (/**
         * @param {?} matched
         * @param {?} group1
         * @return {?}
         */
        function (matched, group1) {
            return '<strong>' + group1 + '</strong>';
        }));
        return value;
    };
    /**
     * @param {?} input
     * @return {?}
     */
    HighlightPipe.prototype.createRegex = /**
     * @param {?} input
     * @return {?}
     */
    function (input) {
        input = input || '';
        input = this.createReplacements(input);
        input = input.replace(/^[^A-z\u00C0-\u00ff]+|[^A-z\u00C0-\u00ff]+$/g, '');
        input = input.replace(/^\||\|$/g, '');
        if (input) {
            /** @type {?} */
            var re = '(' + input + ')';
            return new RegExp(re, 'i');
        }
        return null;
    };
    /**
     * @param {?} str
     * @return {?}
     */
    HighlightPipe.prototype.createReplacements = /**
     * @param {?} str
     * @return {?}
     */
    function (str) {
        /** @type {?} */
        var replacements = [
            '[aàáâãäå]',
            '(æ|oe)',
            '[cç]',
            '[eèéêë]',
            '[iìíîï]',
            '[nñ]',
            '[oòóôõö]',
            '[uùúûü]',
            '[yýÿ]'
        ];
        return replacements.reduce((/**
         * @param {?} item
         * @param {?} regexStr
         * @return {?}
         */
        function (item, regexStr) {
            return item.replace(new RegExp(regexStr, 'gi'), regexStr);
        }), str);
    };
    HighlightPipe.decorators = [
        { type: Pipe, args: [{ name: 'highlight' },] }
    ];
    return HighlightPipe;
}());
export { HighlightPipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGlnaGxpZ2h0LnBpcGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9waXBlcy9oaWdobGlnaHQucGlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLElBQUksRUFBZ0IsTUFBTSxlQUFlLENBQUM7QUFFbEQ7SUFBQTtJQXFEQSxDQUFDOzs7Ozs7SUFsREMsaUNBQVM7Ozs7O0lBQVQsVUFBVSxLQUFhLEVBQUUsSUFBWTtRQUVuQyxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQ1YsT0FBTyxFQUFFLENBQUM7U0FDWDs7WUFFSyxLQUFLLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUM7UUFDcEMsSUFBSSxDQUFDLEtBQUssRUFBRTtZQUNWLE9BQU8sS0FBSyxDQUFDO1NBQ2Q7UUFFRCxnREFBZ0Q7UUFDaEQsS0FBSyxHQUFHLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSzs7Ozs7UUFBRSxVQUFVLE9BQU8sRUFBRSxNQUFNO1lBQ3BELE9BQU8sVUFBVSxHQUFHLE1BQU0sR0FBRyxXQUFXLENBQUM7UUFDM0MsQ0FBQyxFQUFDLENBQUM7UUFFSCxPQUFPLEtBQUssQ0FBQztJQUNmLENBQUM7Ozs7O0lBRUQsbUNBQVc7Ozs7SUFBWCxVQUFZLEtBQUs7UUFDZixLQUFLLEdBQUcsS0FBSyxJQUFJLEVBQUUsQ0FBQztRQUNwQixLQUFLLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3ZDLEtBQUssR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLDhDQUE4QyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQzFFLEtBQUssR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLFVBQVUsRUFBRSxFQUFFLENBQUMsQ0FBQztRQUN0QyxJQUFJLEtBQUssRUFBRTs7Z0JBQ0gsRUFBRSxHQUFHLEdBQUcsR0FBRyxLQUFLLEdBQUcsR0FBRztZQUM1QixPQUFPLElBQUksTUFBTSxDQUFDLEVBQUUsRUFBRSxHQUFHLENBQUMsQ0FBQztTQUM1QjtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQzs7Ozs7SUFHRCwwQ0FBa0I7Ozs7SUFBbEIsVUFBbUIsR0FBRzs7WUFDZCxZQUFZLEdBQUc7WUFDbkIsV0FBVztZQUNYLFFBQVE7WUFDUixNQUFNO1lBQ04sU0FBUztZQUNULFNBQVM7WUFDVCxNQUFNO1lBQ04sVUFBVTtZQUNWLFNBQVM7WUFDVCxPQUFPO1NBQ1I7UUFFRCxPQUFPLFlBQVksQ0FBQyxNQUFNOzs7OztRQUFDLFVBQUMsSUFBSSxFQUFFLFFBQVE7WUFDeEMsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksTUFBTSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsRUFBRSxRQUFRLENBQUMsQ0FBQztRQUM1RCxDQUFDLEdBQUUsR0FBRyxDQUFDLENBQUM7SUFFVixDQUFDOztnQkFwREYsSUFBSSxTQUFDLEVBQUMsSUFBSSxFQUFFLFdBQVcsRUFBQzs7SUFxRHpCLG9CQUFDO0NBQUEsQUFyREQsSUFxREM7U0FwRFksYUFBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7UGlwZSwgUGlwZVRyYW5zZm9ybX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBQaXBlKHtuYW1lOiAnaGlnaGxpZ2h0J30pXG5leHBvcnQgY2xhc3MgSGlnaGxpZ2h0UGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm0ge1xuXG4gIHRyYW5zZm9ybSh2YWx1ZTogc3RyaW5nLCB0ZXJtOiBzdHJpbmcpOiBzdHJpbmcge1xuXG4gICAgaWYgKCF2YWx1ZSkge1xuICAgICAgcmV0dXJuICcnO1xuICAgIH1cblxuICAgIGNvbnN0IHJlZ2V4ID0gdGhpcy5jcmVhdGVSZWdleCh0ZXJtKTtcbiAgICBpZiAoIXJlZ2V4KSB7XG4gICAgICByZXR1cm4gdmFsdWU7XG4gICAgfVxuXG4gICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOm9ubHktYXJyb3ctZnVuY3Rpb25zXG4gICAgdmFsdWUgPSB2YWx1ZS5yZXBsYWNlKHJlZ2V4LCBmdW5jdGlvbiAobWF0Y2hlZCwgZ3JvdXAxKSB7XG4gICAgICByZXR1cm4gJzxzdHJvbmc+JyArIGdyb3VwMSArICc8L3N0cm9uZz4nO1xuICAgIH0pO1xuXG4gICAgcmV0dXJuIHZhbHVlO1xuICB9XG5cbiAgY3JlYXRlUmVnZXgoaW5wdXQpIHtcbiAgICBpbnB1dCA9IGlucHV0IHx8ICcnO1xuICAgIGlucHV0ID0gdGhpcy5jcmVhdGVSZXBsYWNlbWVudHMoaW5wdXQpO1xuICAgIGlucHV0ID0gaW5wdXQucmVwbGFjZSgvXlteQS16XFx1MDBDMC1cXHUwMGZmXSt8W15BLXpcXHUwMEMwLVxcdTAwZmZdKyQvZywgJycpO1xuICAgIGlucHV0ID0gaW5wdXQucmVwbGFjZSgvXlxcfHxcXHwkL2csICcnKTtcbiAgICBpZiAoaW5wdXQpIHtcbiAgICAgIGNvbnN0IHJlID0gJygnICsgaW5wdXQgKyAnKSc7XG4gICAgICByZXR1cm4gbmV3IFJlZ0V4cChyZSwgJ2knKTtcbiAgICB9XG4gICAgcmV0dXJuIG51bGw7XG4gIH1cblxuXG4gIGNyZWF0ZVJlcGxhY2VtZW50cyhzdHIpIHtcbiAgICBjb25zdCByZXBsYWNlbWVudHMgPSBbXG4gICAgICAnW2HDoMOhw6LDo8Okw6VdJyxcbiAgICAgICcow6Z8b2UpJyxcbiAgICAgICdbY8OnXScsXG4gICAgICAnW2XDqMOpw6rDq10nLFxuICAgICAgJ1tpw6zDrcOuw69dJyxcbiAgICAgICdbbsOxXScsXG4gICAgICAnW2/DssOzw7TDtcO2XScsXG4gICAgICAnW3XDucO6w7vDvF0nLFxuICAgICAgJ1t5w73Dv10nXG4gICAgXTtcblxuICAgIHJldHVybiByZXBsYWNlbWVudHMucmVkdWNlKChpdGVtLCByZWdleFN0cikgPT4ge1xuICAgICAgcmV0dXJuIGl0ZW0ucmVwbGFjZShuZXcgUmVnRXhwKHJlZ2V4U3RyLCAnZ2knKSwgcmVnZXhTdHIpO1xuICAgIH0sIHN0cik7XG5cbiAgfVxufVxuIl19