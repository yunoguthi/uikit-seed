/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AutoFocusDirective } from '../utils/auto-focus.directive';
import { HighlightPipe } from './pipes/highlight.pipe';
import { LoadingDirective } from './directive/loading.directive';
import { MatSpinner } from '@angular/material';
import { SpinnerDirective } from './directive/spinner.directive';
import { SkeletonDirective } from './directive/skeleton.directive';
var UikitSharedModule = /** @class */ (function () {
    function UikitSharedModule() {
    }
    UikitSharedModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [
                        AutoFocusDirective,
                        LoadingDirective,
                        SpinnerDirective,
                        SkeletonDirective,
                        HighlightPipe
                    ],
                    imports: [
                        CommonModule,
                    ],
                    exports: [
                        AutoFocusDirective,
                        LoadingDirective,
                        SpinnerDirective,
                        SkeletonDirective,
                        HighlightPipe,
                    ],
                    entryComponents: [MatSpinner]
                },] }
    ];
    return UikitSharedModule;
}());
export { UikitSharedModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hhcmVkLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL3NoYXJlZC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxRQUFRLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDdkMsT0FBTyxFQUFDLFlBQVksRUFBQyxNQUFNLGlCQUFpQixDQUFDO0FBQzdDLE9BQU8sRUFBQyxrQkFBa0IsRUFBQyxNQUFNLCtCQUErQixDQUFDO0FBQ2pFLE9BQU8sRUFBQyxhQUFhLEVBQUMsTUFBTSx3QkFBd0IsQ0FBQztBQUNyRCxPQUFPLEVBQUMsZ0JBQWdCLEVBQUMsTUFBTSwrQkFBK0IsQ0FBQztBQUMvRCxPQUFPLEVBQUMsVUFBVSxFQUFDLE1BQU0sbUJBQW1CLENBQUM7QUFDN0MsT0FBTyxFQUFDLGdCQUFnQixFQUFDLE1BQU0sK0JBQStCLENBQUM7QUFDL0QsT0FBTyxFQUFDLGlCQUFpQixFQUFDLE1BQU0sZ0NBQWdDLENBQUM7QUFFakU7SUFBQTtJQXFCQSxDQUFDOztnQkFyQkEsUUFBUSxTQUFDO29CQUNSLFlBQVksRUFBRTt3QkFDWixrQkFBa0I7d0JBQ2xCLGdCQUFnQjt3QkFDaEIsZ0JBQWdCO3dCQUNoQixpQkFBaUI7d0JBQ2pCLGFBQWE7cUJBQ2Q7b0JBQ0QsT0FBTyxFQUFFO3dCQUNQLFlBQVk7cUJBQ2I7b0JBQ0QsT0FBTyxFQUFFO3dCQUNQLGtCQUFrQjt3QkFDbEIsZ0JBQWdCO3dCQUNoQixnQkFBZ0I7d0JBQ2hCLGlCQUFpQjt3QkFDakIsYUFBYTtxQkFDZDtvQkFDRCxlQUFlLEVBQUUsQ0FBQyxVQUFVLENBQUM7aUJBQzlCOztJQUVELHdCQUFDO0NBQUEsQUFyQkQsSUFxQkM7U0FEWSxpQkFBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge05nTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7Q29tbW9uTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHtBdXRvRm9jdXNEaXJlY3RpdmV9IGZyb20gJy4uL3V0aWxzL2F1dG8tZm9jdXMuZGlyZWN0aXZlJztcbmltcG9ydCB7SGlnaGxpZ2h0UGlwZX0gZnJvbSAnLi9waXBlcy9oaWdobGlnaHQucGlwZSc7XG5pbXBvcnQge0xvYWRpbmdEaXJlY3RpdmV9IGZyb20gJy4vZGlyZWN0aXZlL2xvYWRpbmcuZGlyZWN0aXZlJztcbmltcG9ydCB7TWF0U3Bpbm5lcn0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xuaW1wb3J0IHtTcGlubmVyRGlyZWN0aXZlfSBmcm9tICcuL2RpcmVjdGl2ZS9zcGlubmVyLmRpcmVjdGl2ZSc7XG5pbXBvcnQge1NrZWxldG9uRGlyZWN0aXZlfSBmcm9tICcuL2RpcmVjdGl2ZS9za2VsZXRvbi5kaXJlY3RpdmUnO1xuXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtcbiAgICBBdXRvRm9jdXNEaXJlY3RpdmUsXG4gICAgTG9hZGluZ0RpcmVjdGl2ZSxcbiAgICBTcGlubmVyRGlyZWN0aXZlLFxuICAgIFNrZWxldG9uRGlyZWN0aXZlLFxuICAgIEhpZ2hsaWdodFBpcGVcbiAgXSxcbiAgaW1wb3J0czogW1xuICAgIENvbW1vbk1vZHVsZSxcbiAgXSxcbiAgZXhwb3J0czogW1xuICAgIEF1dG9Gb2N1c0RpcmVjdGl2ZSxcbiAgICBMb2FkaW5nRGlyZWN0aXZlLFxuICAgIFNwaW5uZXJEaXJlY3RpdmUsXG4gICAgU2tlbGV0b25EaXJlY3RpdmUsXG4gICAgSGlnaGxpZ2h0UGlwZSxcbiAgXSxcbiAgZW50cnlDb21wb25lbnRzOiBbTWF0U3Bpbm5lcl1cbn0pXG5leHBvcnQgY2xhc3MgVWlraXRTaGFyZWRNb2R1bGUge1xufVxuIl19