/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @param {?=} blackList
 * @return {?}
 */
export function AutoUnsubscribe(blackList) {
    if (blackList === void 0) { blackList = []; }
    return (/**
     * @param {?} constructor
     * @return {?}
     */
    function (constructor) {
        /** @type {?} */
        var original = constructor.prototype.ngOnDestroy;
        constructor.prototype.ngOnDestroy = (/**
         * @return {?}
         */
        function () {
            for (var prop in this) {
                /** @type {?} */
                var property = this[prop];
                if (!blackList.includes(prop)) {
                    if (property && (typeof property.unsubscribe === "function")) {
                        property.unsubscribe();
                    }
                }
            }
            original && typeof original === 'function' && original.apply(this, arguments);
        });
    });
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0by11bnN1YnNjcmliZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2F1dG8tdW5zdWJzY3JpYmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQSxNQUFNLFVBQVUsZUFBZSxDQUFDLFNBQWM7SUFBZCwwQkFBQSxFQUFBLGNBQWM7SUFFNUM7Ozs7SUFBTyxVQUFTLFdBQVc7O1lBQ25CLFFBQVEsR0FBRyxXQUFXLENBQUMsU0FBUyxDQUFDLFdBQVc7UUFFbEQsV0FBVyxDQUFDLFNBQVMsQ0FBQyxXQUFXOzs7UUFBRztZQUNsQyxLQUFLLElBQUksSUFBSSxJQUFJLElBQUksRUFBRTs7b0JBQ2YsUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7Z0JBQzNCLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFO29CQUM3QixJQUFJLFFBQVEsSUFBSSxDQUFDLE9BQU8sUUFBUSxDQUFDLFdBQVcsS0FBSyxVQUFVLENBQUMsRUFBRTt3QkFDNUQsUUFBUSxDQUFDLFdBQVcsRUFBRSxDQUFDO3FCQUN4QjtpQkFDRjthQUNGO1lBQ0QsUUFBUSxJQUFJLE9BQU8sUUFBUSxLQUFLLFVBQVUsSUFBSSxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxTQUFTLENBQUMsQ0FBQztRQUNoRixDQUFDLENBQUEsQ0FBQztJQUNKLENBQUMsRUFBQTtBQUVILENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZnVuY3Rpb24gQXV0b1Vuc3Vic2NyaWJlKGJsYWNrTGlzdCA9IFtdKSB7XG5cbiAgcmV0dXJuIGZ1bmN0aW9uKGNvbnN0cnVjdG9yKSB7XG4gICAgY29uc3Qgb3JpZ2luYWwgPSBjb25zdHJ1Y3Rvci5wcm90b3R5cGUubmdPbkRlc3Ryb3k7XG5cbiAgICBjb25zdHJ1Y3Rvci5wcm90b3R5cGUubmdPbkRlc3Ryb3kgPSBmdW5jdGlvbigpIHtcbiAgICAgIGZvciAobGV0IHByb3AgaW4gdGhpcykge1xuICAgICAgICBjb25zdCBwcm9wZXJ0eSA9IHRoaXNbcHJvcF07XG4gICAgICAgIGlmICghYmxhY2tMaXN0LmluY2x1ZGVzKHByb3ApKSB7XG4gICAgICAgICAgaWYgKHByb3BlcnR5ICYmICh0eXBlb2YgcHJvcGVydHkudW5zdWJzY3JpYmUgPT09IFwiZnVuY3Rpb25cIikpIHtcbiAgICAgICAgICAgIHByb3BlcnR5LnVuc3Vic2NyaWJlKCk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgICBvcmlnaW5hbCAmJiB0eXBlb2Ygb3JpZ2luYWwgPT09ICdmdW5jdGlvbicgJiYgb3JpZ2luYWwuYXBwbHkodGhpcywgYXJndW1lbnRzKTtcbiAgICB9O1xuICB9XG5cbn1cbiJdfQ==