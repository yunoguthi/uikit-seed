/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { catchError, mergeMap } from 'rxjs/operators';
import { Injectable, Optional, Injector } from '@angular/core';
import { throwError, from } from 'rxjs';
import { LogService } from '../../../utils/log/log.service';
import { UserService } from '../authentication/user.service';
import { IAuthenticationServiceToken } from '../authentication/abstraction/provider-authentication-service.token';
var OidcAuthHttpInterceptor = /** @class */ (function () {
    function OidcAuthHttpInterceptor(userService, injector, logService) {
        this.userService = userService;
        this.injector = injector;
        this.logService = logService;
    }
    /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    OidcAuthHttpInterceptor.prototype.intercept = /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    function (req, next) {
        var _this = this;
        /** @type {?} */
        var authReq = req;
        if (!req.headers.has('Authorization')) {
            if (this.userService && this.userService.userValue && this.userService.userValue.authenticated) {
                /** @type {?} */
                var token = ((/** @type {?} */ (this.userService.userValue))).access_token ||
                    ((/** @type {?} */ (this.userService.userValue))).id_token;
                /** @type {?} */
                var httpHeaders = req.headers.set('Authorization', "Bearer " + token);
                if (!req.headers.has('content-type')) {
                    httpHeaders = httpHeaders.set('content-type', 'application/json');
                }
                authReq = req.clone({
                    headers: httpHeaders
                });
                if (this.logService) {
                    this.logService.debug("N\u00E3o possui 'Authorization' header, foi adicionado.");
                }
            }
        }
        /** @type {?} */
        var httpHandle = next
            .handle(authReq)
            .pipe(catchError((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            if (error.status === 401) {
                /** @type {?} */
                var authenticationService = (/** @type {?} */ (_this.injector.get(IAuthenticationServiceToken)));
                if (authenticationService.needRenewUser) {
                    return from(authenticationService.renewUser()).pipe(mergeMap((/**
                     * @return {?}
                     */
                    function () {
                        return _this.intercept(req, next);
                    })));
                }
            }
            if (_this.logService) {
                _this.logService.error(JSON.stringify(error));
            }
            return throwError(error);
        })));
        return httpHandle;
    };
    OidcAuthHttpInterceptor.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    OidcAuthHttpInterceptor.ctorParameters = function () { return [
        { type: UserService },
        { type: Injector },
        { type: LogService, decorators: [{ type: Optional }] }
    ]; };
    return OidcAuthHttpInterceptor;
}());
export { OidcAuthHttpInterceptor };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    OidcAuthHttpInterceptor.prototype.userService;
    /**
     * @type {?}
     * @private
     */
    OidcAuthHttpInterceptor.prototype.injector;
    /**
     * @type {?}
     * @protected
     */
    OidcAuthHttpInterceptor.prototype.logService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC1odHRwLmludGVyY2VwdG9yLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvYXV0aC9hdXRoZW50aWNhdGlvbi1vaWRjL2F1dGgtaHR0cC5pbnRlcmNlcHRvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUN0RCxPQUFPLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBc0MsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRW5HLE9BQU8sRUFBYyxVQUFVLEVBQUUsSUFBSSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBRXBELE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUM1RCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0NBQWdDLENBQUM7QUFFN0QsT0FBTyxFQUFFLDJCQUEyQixFQUFFLE1BQU0scUVBQXFFLENBQUM7QUFHbEg7SUFHRSxpQ0FDWSxXQUF3QixFQUMxQixRQUFrQixFQUNKLFVBQXVCO1FBRm5DLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQzFCLGFBQVEsR0FBUixRQUFRLENBQVU7UUFDSixlQUFVLEdBQVYsVUFBVSxDQUFhO0lBRy9DLENBQUM7Ozs7OztJQUVELDJDQUFTOzs7OztJQUFULFVBQVUsR0FBcUIsRUFBRSxJQUFpQjtRQUFsRCxpQkE4Q0M7O1lBNUNLLE9BQU8sR0FBRyxHQUFHO1FBRWpCLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsRUFBRTtZQUNyQyxJQUFJLElBQUksQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsYUFBYSxFQUFFOztvQkFFeEYsS0FBSyxHQUNULENBQUMsbUJBQUEsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLEVBQWEsQ0FBQyxDQUFDLFlBQVk7b0JBQ3RELENBQUMsbUJBQUEsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLEVBQXFCLENBQUMsQ0FBQyxRQUFROztvQkFFeEQsV0FBVyxHQUFHLEdBQUcsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsRUFBRSxZQUFVLEtBQU8sQ0FBQztnQkFFckUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxFQUFFO29CQUNwQyxXQUFXLEdBQUcsV0FBVyxDQUFDLEdBQUcsQ0FBQyxjQUFjLEVBQUUsa0JBQWtCLENBQUMsQ0FBQztpQkFDbkU7Z0JBRUQsT0FBTyxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUM7b0JBQ2xCLE9BQU8sRUFBRSxXQUFXO2lCQUNyQixDQUFDLENBQUM7Z0JBRUgsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO29CQUNuQixJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyx5REFBb0QsQ0FBQyxDQUFDO2lCQUM3RTthQUNGO1NBQ0Y7O1lBRUssVUFBVSxHQUFHLElBQUk7YUFDcEIsTUFBTSxDQUFDLE9BQU8sQ0FBQzthQUNmLElBQUksQ0FBQyxVQUFVOzs7O1FBQUMsVUFBQyxLQUF3QjtZQUN4QyxJQUFJLEtBQUssQ0FBQyxNQUFNLEtBQUssR0FBRyxFQUFFOztvQkFDbEIscUJBQXFCLEdBQUcsbUJBQUEsS0FBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsMkJBQTJCLENBQUMsRUFBNkI7Z0JBQ3pHLElBQUkscUJBQXFCLENBQUMsYUFBYSxFQUFFO29CQUN2QyxPQUFPLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFROzs7b0JBQUM7d0JBQzNELE9BQU8sS0FBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUM7b0JBQ25DLENBQUMsRUFBQyxDQUFDLENBQUM7aUJBQ0w7YUFDRjtZQUNELElBQUksS0FBSSxDQUFDLFVBQVUsRUFBRTtnQkFDbkIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO2FBQzlDO1lBQ0QsT0FBTyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDM0IsQ0FBQyxFQUFDLENBQUM7UUFFTCxPQUFPLFVBQVUsQ0FBQztJQUVwQixDQUFDOztnQkF6REYsVUFBVTs7OztnQkFMRixXQUFXO2dCQUwrQyxRQUFRO2dCQUlsRSxVQUFVLHVCQVlkLFFBQVE7O0lBcURiLDhCQUFDO0NBQUEsQUEzREQsSUEyREM7U0ExRFksdUJBQXVCOzs7Ozs7SUFHaEMsOENBQWtDOzs7OztJQUNsQywyQ0FBMEI7Ozs7O0lBQzFCLDZDQUE2QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IGNhdGNoRXJyb3IsIG1lcmdlTWFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHsgSW5qZWN0YWJsZSwgT3B0aW9uYWwsIEluamVjdCwgaW5qZWN0LCBSZWZsZWN0aXZlSW5qZWN0b3IsIEluamVjdG9yIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwRXZlbnQsIEh0dHBJbnRlcmNlcHRvciwgSHR0cEhhbmRsZXIsIEh0dHBSZXF1ZXN0LCBIdHRwSGVhZGVycywgSHR0cEVycm9yUmVzcG9uc2UgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlLCB0aHJvd0Vycm9yLCBmcm9tIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBPaWRjQXV0aGVudGljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi9vaWRjLWF1dGhlbnRpY2F0aW9uLnNlcnZpY2UnO1xuaW1wb3J0IHsgTG9nU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL3V0aWxzL2xvZy9sb2cuc2VydmljZSc7XG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gJy4uL2F1dGhlbnRpY2F0aW9uL3VzZXIuc2VydmljZSc7XG5pbXBvcnQgeyBPQXV0aFVzZXIsIE9wZW5JRENvbm5lY3RVc2VyLCBVc2VyIH0gZnJvbSAnLi4vYXV0aGVudGljYXRpb24vdXNlci5tb2RlbCc7XG5pbXBvcnQgeyBJQXV0aGVudGljYXRpb25TZXJ2aWNlVG9rZW4gfSBmcm9tICcuLi9hdXRoZW50aWNhdGlvbi9hYnN0cmFjdGlvbi9wcm92aWRlci1hdXRoZW50aWNhdGlvbi1zZXJ2aWNlLnRva2VuJztcbmltcG9ydCB7IElQcm92aWRlckF1dGhlbnRpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uL2F1dGhlbnRpY2F0aW9uL2Fic3RyYWN0aW9uL3Byb3ZpZGVyLWF1dGhlbnRpY2F0aW9uLnNlcnZpY2UnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgT2lkY0F1dGhIdHRwSW50ZXJjZXB0b3IgaW1wbGVtZW50cyBIdHRwSW50ZXJjZXB0b3Ige1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByb3RlY3RlZCB1c2VyU2VydmljZTogVXNlclNlcnZpY2UsXG4gICAgcHJpdmF0ZSBpbmplY3RvcjogSW5qZWN0b3IsXG4gICAgQE9wdGlvbmFsKCkgcHJvdGVjdGVkIGxvZ1NlcnZpY2U/OiBMb2dTZXJ2aWNlLFxuICApIHtcblxuICB9XG5cbiAgaW50ZXJjZXB0KHJlcTogSHR0cFJlcXVlc3Q8YW55PiwgbmV4dDogSHR0cEhhbmRsZXIpOiBPYnNlcnZhYmxlPEh0dHBFdmVudDxhbnk+PiB7XG5cbiAgICBsZXQgYXV0aFJlcSA9IHJlcTtcblxuICAgIGlmICghcmVxLmhlYWRlcnMuaGFzKCdBdXRob3JpemF0aW9uJykpIHtcbiAgICAgIGlmICh0aGlzLnVzZXJTZXJ2aWNlICYmIHRoaXMudXNlclNlcnZpY2UudXNlclZhbHVlICYmIHRoaXMudXNlclNlcnZpY2UudXNlclZhbHVlLmF1dGhlbnRpY2F0ZWQpIHtcblxuICAgICAgICBjb25zdCB0b2tlbiA9XG4gICAgICAgICAgKHRoaXMudXNlclNlcnZpY2UudXNlclZhbHVlIGFzIE9BdXRoVXNlcikuYWNjZXNzX3Rva2VuIHx8XG4gICAgICAgICAgKHRoaXMudXNlclNlcnZpY2UudXNlclZhbHVlIGFzIE9wZW5JRENvbm5lY3RVc2VyKS5pZF90b2tlbjtcblxuICAgICAgICBsZXQgaHR0cEhlYWRlcnMgPSByZXEuaGVhZGVycy5zZXQoJ0F1dGhvcml6YXRpb24nLCBgQmVhcmVyICR7dG9rZW59YCk7XG5cbiAgICAgICAgaWYgKCFyZXEuaGVhZGVycy5oYXMoJ2NvbnRlbnQtdHlwZScpKSB7XG4gICAgICAgICAgaHR0cEhlYWRlcnMgPSBodHRwSGVhZGVycy5zZXQoJ2NvbnRlbnQtdHlwZScsICdhcHBsaWNhdGlvbi9qc29uJyk7XG4gICAgICAgIH1cblxuICAgICAgICBhdXRoUmVxID0gcmVxLmNsb25lKHtcbiAgICAgICAgICBoZWFkZXJzOiBodHRwSGVhZGVyc1xuICAgICAgICB9KTtcblxuICAgICAgICBpZiAodGhpcy5sb2dTZXJ2aWNlKSB7XG4gICAgICAgICAgdGhpcy5sb2dTZXJ2aWNlLmRlYnVnKGBOw6NvIHBvc3N1aSAnQXV0aG9yaXphdGlvbicgaGVhZGVyLCBmb2kgYWRpY2lvbmFkby5gKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuICAgIGNvbnN0IGh0dHBIYW5kbGUgPSBuZXh0XG4gICAgICAuaGFuZGxlKGF1dGhSZXEpXG4gICAgICAucGlwZShjYXRjaEVycm9yKChlcnJvcjogSHR0cEVycm9yUmVzcG9uc2UpID0+IHtcbiAgICAgICAgaWYgKGVycm9yLnN0YXR1cyA9PT0gNDAxKSB7XG4gICAgICAgICAgY29uc3QgYXV0aGVudGljYXRpb25TZXJ2aWNlID0gdGhpcy5pbmplY3Rvci5nZXQoSUF1dGhlbnRpY2F0aW9uU2VydmljZVRva2VuKSBhcyBPaWRjQXV0aGVudGljYXRpb25TZXJ2aWNlO1xuICAgICAgICAgIGlmIChhdXRoZW50aWNhdGlvblNlcnZpY2UubmVlZFJlbmV3VXNlcikge1xuICAgICAgICAgICAgcmV0dXJuIGZyb20oYXV0aGVudGljYXRpb25TZXJ2aWNlLnJlbmV3VXNlcigpKS5waXBlKG1lcmdlTWFwKCgpID0+IHtcbiAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuaW50ZXJjZXB0KHJlcSwgbmV4dCk7XG4gICAgICAgICAgICB9KSk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIGlmICh0aGlzLmxvZ1NlcnZpY2UpIHtcbiAgICAgICAgICB0aGlzLmxvZ1NlcnZpY2UuZXJyb3IoSlNPTi5zdHJpbmdpZnkoZXJyb3IpKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhyb3dFcnJvcihlcnJvcik7XG4gICAgICB9KSk7XG5cbiAgICByZXR1cm4gaHR0cEhhbmRsZTtcblxuICB9XG5cbn1cbiJdfQ==