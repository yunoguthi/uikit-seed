/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { Router, NavigationStart, ActivatedRoute } from '@angular/router';
import { filter } from 'rxjs/operators';
import { UserManager, WebStorageStateStore } from 'oidc-client';
import { ProviderAuthenticationService } from '../authentication/provider-authentication.service';
import { PlatformLocation, LocationStrategy, } from '@angular/common';
// @dynamic
var OidcAuthenticationService = /** @class */ (function (_super) {
    tslib_1.__extends(OidcAuthenticationService, _super);
    function OidcAuthenticationService(authenticationSettings, router, activatedRoute, platformLocation, locationStrategy) {
        var _this = _super.call(this) || this;
        _this.authenticationSettings = authenticationSettings;
        _this.router = router;
        _this.activatedRoute = activatedRoute;
        _this.platformLocation = platformLocation;
        _this.locationStrategy = locationStrategy;
        _this.userManager = null;
        _this.userValue = null;
        /** @type {?} */
        var userManagerArg = tslib_1.__assign({}, authenticationSettings, { userStore: new WebStorageStateStore({ store: window.localStorage }) });
        _this.userManager = new UserManager(userManagerArg);
        _this.userManager.events.addUserLoaded((/**
         * @param {?} user
         * @return {?}
         */
        function (user) {
            _this.loadOidcUser(user);
        }));
        _this.userManager.events.addAccessTokenExpired((/**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            _this.userManager.signinSilent().catch((/**
             * @param {?} error
             * @return {?}
             */
            function (error) {
                console.error(error);
                _this.userManager.removeUser();
                _super.prototype.logout.call(_this);
            }));
        }));
        _this.userManager.events.addSilentRenewError((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            console.error(error);
            _this.userManager.removeUser();
            _super.prototype.logout.call(_this);
        }));
        _this.userManager.getUser().then((/**
         * @param {?} user
         * @return {?}
         */
        function (user) {
            _this.loadOidcUser(user);
        }));
        if (_this.authenticationSettings.automaticSilentRenew) {
            _this.router.events
                .pipe(filter((/**
             * @param {?} event
             * @return {?}
             */
            function (event) { return event instanceof NavigationStart; })))
                .pipe(filter((/**
             * @param {?} event
             * @return {?}
             */
            function (event) {
                return new RegExp(_this.authenticationSettings.silent_redirect_uri).test(location.href);
            })))
                .subscribe((/**
             * @param {?} event
             * @return {?}
             */
            function (event) {
                _this.userManager.signinSilentCallback().catch((/**
                 * @param {?} err
                 * @return {?}
                 */
                function (err) {
                    console.error(err);
                    _this.userManager.startSilentRenew();
                }));
            }));
        }
        _this.router.events
            .pipe(filter((/**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            return new RegExp(_this.authenticationSettings.redirect_uri).test(location.href);
        })))
            .subscribe((/**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            _this.userManager.signinRedirectCallback();
        }));
        return _this;
    }
    Object.defineProperty(OidcAuthenticationService.prototype, "needRenewUser", {
        get: /**
         * @return {?}
         */
        function () {
            return this.userValue != null && this.userValue.expired === true;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    OidcAuthenticationService.prototype.renewUser = /**
     * @return {?}
     */
    function () {
        var _this = this;
        return this.userManager
            .signinSilent()
            .then((/**
         * @param {?} user
         * @return {?}
         */
        function (user) { return (/**
         * @return {?}
         */
        function () { return _this.loadOidcUser(user); }); }));
    };
    /**
     * @param {?} user
     * @return {?}
     */
    OidcAuthenticationService.prototype.loadOidcUser = /**
     * @param {?} user
     * @return {?}
     */
    function (user) {
        this.userValue = user;
        this.loadProviderUser(user);
    };
    /**
     * @param {?} user
     * @return {?}
     */
    OidcAuthenticationService.prototype.transform = /**
     * @param {?} user
     * @return {?}
     */
    function (user) {
        /** @type {?} */
        var destinationUser = (/** @type {?} */ ({}));
        /** @type {?} */
        var combinedUser = Object.assign(destinationUser, user);
        // O OIDC_Client usa a propriedade profile para colocar claims,
        // portanto devemos fazer o assign deste com o objetivo de manter
        // no usuário também as informações 'não mapeadas'
        if (user.profile) {
            combinedUser = Object.assign(combinedUser, user.profile);
        }
        combinedUser.authenticated = true;
        combinedUser.sub = user.profile.sub || null;
        combinedUser.name = user.profile.name || null;
        combinedUser.preferred_username = user.profile.preferred_username || null;
        combinedUser.given_name = user.profile.given_name || null;
        combinedUser.nickname = user.profile.nickname || null;
        combinedUser.email = user.profile.email || null;
        combinedUser.picture =
            (user.profile.picture && user.profile.picture[0]) || null;
        combinedUser.client_id = user.profile.client_id || null;
        combinedUser.access_token = user.access_token || null;
        combinedUser.id_token = user.id_token || null;
        combinedUser.roles = user.profile.roles || null;
        return combinedUser;
    };
    /**
     * @param {?=} args
     * @return {?}
     */
    OidcAuthenticationService.prototype.login = /**
     * @param {?=} args
     * @return {?}
     */
    function (args) {
        /** @type {?} */
        var baseHref = this.platformLocation.getBaseHrefFromDOM();
        /** @type {?} */
        var routeActiveIsUnauthorized = this.router.isActive(baseHref + 'unauthorized', false);
        if (this.authenticationSettings.login_mode === 'redirect' ||
            (this.authenticationSettings.login_mode === 'iframe' &&
                routeActiveIsUnauthorized)) {
            return this.userManager.signinRedirect(args).catch((/**
             * @param {?} error
             * @return {?}
             */
            function (error) {
                /** @type {?} */
                var erro = (/** @type {?} */ (error));
                if ('Network Error' === erro.message) {
                    /** @type {?} */
                    var message = 'Não foi possível se conectar ao servidor de autenticação!';
                    /** @type {?} */
                    var errorToThrow = new Error(message);
                    errorToThrow.stack = erro.stack;
                    errorToThrow.name = 'Erro de conexão';
                    throw errorToThrow;
                }
                else {
                    throw erro;
                }
                // console.error(error);
            }));
        }
        else if (this.authenticationSettings.login_mode === 'iframe' &&
            routeActiveIsUnauthorized === false) {
            return this.router.navigateByUrl(baseHref + 'login');
        }
        else if (this.authenticationSettings.login_mode === 'popup') {
            return this.userManager.signinPopup(args).catch((/**
             * @param {?} error
             * @return {?}
             */
            function (error) {
                /** @type {?} */
                var erro = (/** @type {?} */ (error));
                if ('Network Error' === erro.message) {
                    /** @type {?} */
                    var message = 'Não foi possível se conectar ao servidor de autenticação!';
                    /** @type {?} */
                    var errorToThrow = new Error(message);
                    errorToThrow.stack = erro.stack;
                    errorToThrow.name = 'Erro de conexão';
                    throw errorToThrow;
                }
                else {
                    throw erro;
                }
                // console.error(error);
            }));
        }
    };
    /**
     * @param {?=} ssoLogout
     * @return {?}
     */
    OidcAuthenticationService.prototype.logout = /**
     * @param {?=} ssoLogout
     * @return {?}
     */
    function (ssoLogout) {
        var _this = this;
        if (ssoLogout === void 0) { ssoLogout = true; }
        if (ssoLogout) {
            /** @type {?} */
            var isOnline = navigator.onLine;
            if (isOnline) {
                return this.userManager.createSignoutRequest().then((/**
                 * @param {?} req
                 * @return {?}
                 */
                function (req) {
                    console.log('Request logout: ', req);
                    /** @type {?} */
                    var ifrm = document.createElement('iframe');
                    ifrm.setAttribute('src', req.url);
                    ifrm.style.width = '640px';
                    ifrm.style.height = '480px';
                    ifrm.style.display = 'none';
                    ifrm.onload = (/**
                     * @return {?}
                     */
                    function () {
                        console.log('IFrame do logout terminou de carregar!');
                        _super.prototype.logout.call(_this).then((/**
                         * @return {?}
                         */
                        function () {
                            _this.userManager.removeUser();
                            ifrm.parentElement.removeChild(ifrm);
                        }));
                    });
                    document.body.appendChild(ifrm);
                }));
                // return this.userManager.signoutRedirect()
                //   .then(() => super.logout())
                //   .catch((error) => {
                //     console.error(error);
                //     super.logout();
                //     throw error;
                //   });
            }
            else {
                /** @type {?} */
                var message = 'Não foi possível se conectar ao servidor de autenticação!';
                /** @type {?} */
                var errorToThrow = new Error(message);
                errorToThrow.name = 'Erro de conexão';
                throw errorToThrow;
            }
        }
        else {
            _super.prototype.logout.call(this);
        }
    };
    OidcAuthenticationService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    OidcAuthenticationService.ctorParameters = function () { return [
        { type: undefined },
        { type: Router },
        { type: ActivatedRoute },
        { type: PlatformLocation },
        { type: LocationStrategy }
    ]; };
    return OidcAuthenticationService;
}(ProviderAuthenticationService));
export { OidcAuthenticationService };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    OidcAuthenticationService.prototype.userManager;
    /** @type {?} */
    OidcAuthenticationService.prototype.userValue;
    /** @type {?} */
    OidcAuthenticationService.prototype.authenticationSettings;
    /**
     * @type {?}
     * @protected
     */
    OidcAuthenticationService.prototype.router;
    /**
     * @type {?}
     * @protected
     */
    OidcAuthenticationService.prototype.activatedRoute;
    /**
     * @type {?}
     * @protected
     */
    OidcAuthenticationService.prototype.platformLocation;
    /**
     * @type {?}
     * @protected
     */
    OidcAuthenticationService.prototype.locationStrategy;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib2lkYy1hdXRoZW50aWNhdGlvbi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvYXV0aC9hdXRoZW50aWNhdGlvbi1vaWRjL29pZGMtYXV0aGVudGljYXRpb24uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUNBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLE1BQU0sRUFBRSxlQUFlLEVBQUUsY0FBYyxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDMUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3hDLE9BQU8sRUFBRSxXQUFXLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFDaEUsT0FBTyxFQUFFLDZCQUE2QixFQUFFLE1BQU0sbURBQW1ELENBQUM7QUFFbEcsT0FBTyxFQUNMLGdCQUFnQixFQUVoQixnQkFBZ0IsR0FDakIsTUFBTSxpQkFBaUIsQ0FBQzs7QUFHekI7SUFDK0MscURBSzlDO0lBZUMsbUNBQ1Msc0JBQ2dCLEVBQ2IsTUFBYyxFQUNkLGNBQThCLEVBQzlCLGdCQUFrQyxFQUNsQyxnQkFBa0M7UUFOOUMsWUFRRSxpQkFBTyxTQXVEUjtRQTlEUSw0QkFBc0IsR0FBdEIsc0JBQXNCLENBQ047UUFDYixZQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2Qsb0JBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLHNCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFDbEMsc0JBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQXBCcEMsaUJBQVcsR0FBcUIsSUFBSSxDQUFDO1FBRXhDLGVBQVMsR0FBYyxJQUFJLENBQUM7O1lBc0IzQixjQUFjLHdCQUNmLHNCQUFzQixJQUN6QixTQUFTLEVBQUUsSUFBSSxvQkFBb0IsQ0FBQyxFQUFFLEtBQUssRUFBRSxNQUFNLENBQUMsWUFBWSxFQUFFLENBQUMsR0FDcEU7UUFDRCxLQUFJLENBQUMsV0FBVyxHQUFHLElBQUksV0FBVyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ25ELEtBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLGFBQWE7Ozs7UUFBQyxVQUFDLElBQUk7WUFDekMsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMxQixDQUFDLEVBQUMsQ0FBQztRQUNILEtBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLHFCQUFxQjs7OztRQUFDLFVBQUMsS0FBSztZQUNsRCxLQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksRUFBRSxDQUFDLEtBQUs7Ozs7WUFBQyxVQUFDLEtBQUs7Z0JBQzFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3JCLEtBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxFQUFFLENBQUM7Z0JBQzlCLGlCQUFNLE1BQU0sWUFBRSxDQUFDO1lBQ2pCLENBQUMsRUFBQyxDQUFDO1FBQ0wsQ0FBQyxFQUFDLENBQUM7UUFDSCxLQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxtQkFBbUI7Ozs7UUFBQyxVQUFDLEtBQUs7WUFDaEQsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNyQixLQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsRUFBRSxDQUFDO1lBQzlCLGlCQUFNLE1BQU0sWUFBRSxDQUFDO1FBQ2pCLENBQUMsRUFBQyxDQUFDO1FBQ0gsS0FBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxJQUFJOzs7O1FBQUMsVUFBQyxJQUFJO1lBQ25DLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDMUIsQ0FBQyxFQUFDLENBQUM7UUFFSCxJQUFJLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxvQkFBb0IsRUFBRTtZQUNwRCxLQUFJLENBQUMsTUFBTSxDQUFDLE1BQU07aUJBQ2YsSUFBSSxDQUFDLE1BQU07Ozs7WUFBQyxVQUFDLEtBQUssSUFBSyxPQUFBLEtBQUssWUFBWSxlQUFlLEVBQWhDLENBQWdDLEVBQUMsQ0FBQztpQkFDekQsSUFBSSxDQUNILE1BQU07Ozs7WUFBQyxVQUFDLEtBQXNCO2dCQUM1QixPQUFBLElBQUksTUFBTSxDQUFDLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLElBQUksQ0FDOUQsUUFBUSxDQUFDLElBQUksQ0FDZDtZQUZELENBRUMsRUFDRixDQUNGO2lCQUNBLFNBQVM7Ozs7WUFBQyxVQUFDLEtBQXNCO2dCQUNoQyxLQUFJLENBQUMsV0FBVyxDQUFDLG9CQUFvQixFQUFFLENBQUMsS0FBSzs7OztnQkFBQyxVQUFDLEdBQUc7b0JBQ2hELE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQ25CLEtBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztnQkFDdEMsQ0FBQyxFQUFDLENBQUM7WUFDTCxDQUFDLEVBQUMsQ0FBQztTQUNOO1FBRUQsS0FBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNO2FBQ2YsSUFBSSxDQUNILE1BQU07Ozs7UUFBQyxVQUFDLEtBQXNCO1lBQzVCLE9BQUEsSUFBSSxNQUFNLENBQUMsS0FBSSxDQUFDLHNCQUFzQixDQUFDLFlBQVksQ0FBQyxDQUFDLElBQUksQ0FDdkQsUUFBUSxDQUFDLElBQUksQ0FDZDtRQUZELENBRUMsRUFDRixDQUNGO2FBQ0EsU0FBUzs7OztRQUFDLFVBQUMsS0FBc0I7WUFDaEMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1FBQzVDLENBQUMsRUFBQyxDQUFDOztJQUNQLENBQUM7SUF6RUQsc0JBQUksb0RBQWE7Ozs7UUFBakI7WUFDRSxPQUFPLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxLQUFLLElBQUksQ0FBQztRQUNuRSxDQUFDOzs7T0FBQTs7OztJQUVELDZDQUFTOzs7SUFBVDtRQUFBLGlCQUlDO1FBSEMsT0FBTyxJQUFJLENBQUMsV0FBVzthQUNwQixZQUFZLEVBQUU7YUFDZCxJQUFJOzs7O1FBQUMsVUFBQyxJQUFJOzs7UUFBSyxjQUFNLE9BQUEsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsRUFBdkIsQ0FBdUIsSUFBQSxFQUFDLENBQUM7SUFDbkQsQ0FBQzs7Ozs7SUFtRU0sZ0RBQVk7Ozs7SUFBbkIsVUFBb0IsSUFBSTtRQUN0QixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUN0QixJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDOUIsQ0FBQzs7Ozs7SUFFTSw2Q0FBUzs7OztJQUFoQixVQUFpQixJQUFlOztZQUN4QixlQUFlLEdBQUcsbUJBQUEsRUFBRSxFQUFxQjs7WUFDM0MsWUFBWSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQztRQUN2RCwrREFBK0Q7UUFDL0QsaUVBQWlFO1FBQ2pFLGtEQUFrRDtRQUNsRCxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDaEIsWUFBWSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztTQUMxRDtRQUVELFlBQVksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1FBRWxDLFlBQVksQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLElBQUksSUFBSSxDQUFDO1FBQzVDLFlBQVksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDO1FBQzlDLFlBQVksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLGtCQUFrQixJQUFJLElBQUksQ0FBQztRQUMxRSxZQUFZLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQztRQUMxRCxZQUFZLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQztRQUN0RCxZQUFZLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQztRQUNoRCxZQUFZLENBQUMsT0FBTztZQUNsQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDO1FBQzVELFlBQVksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDO1FBQ3hELFlBQVksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUM7UUFDdEQsWUFBWSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQztRQUM5QyxZQUFZLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQztRQUVoRCxPQUFPLFlBQVksQ0FBQztJQUN0QixDQUFDOzs7OztJQUVNLHlDQUFLOzs7O0lBQVosVUFBYSxJQUFLOztZQUNWLFFBQVEsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsa0JBQWtCLEVBQUU7O1lBQ3JELHlCQUF5QixHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUNwRCxRQUFRLEdBQUcsY0FBYyxFQUN6QixLQUFLLENBQ047UUFDRCxJQUNFLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxVQUFVLEtBQUssVUFBVTtZQUNyRCxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxVQUFVLEtBQUssUUFBUTtnQkFDbEQseUJBQXlCLENBQUMsRUFDNUI7WUFDQSxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDLEtBQUs7Ozs7WUFBQyxVQUFDLEtBQUs7O29CQUNqRCxJQUFJLEdBQUcsbUJBQUEsS0FBSyxFQUFTO2dCQUMzQixJQUFJLGVBQWUsS0FBSyxJQUFJLENBQUMsT0FBTyxFQUFFOzt3QkFDOUIsT0FBTyxHQUNYLDJEQUEyRDs7d0JBQ3ZELFlBQVksR0FBRyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUM7b0JBQ3ZDLFlBQVksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztvQkFDaEMsWUFBWSxDQUFDLElBQUksR0FBRyxpQkFBaUIsQ0FBQztvQkFDdEMsTUFBTSxZQUFZLENBQUM7aUJBQ3BCO3FCQUFNO29CQUNMLE1BQU0sSUFBSSxDQUFDO2lCQUNaO2dCQUNELHdCQUF3QjtZQUMxQixDQUFDLEVBQUMsQ0FBQztTQUNKO2FBQU0sSUFDTCxJQUFJLENBQUMsc0JBQXNCLENBQUMsVUFBVSxLQUFLLFFBQVE7WUFDbkQseUJBQXlCLEtBQUssS0FBSyxFQUNuQztZQUNBLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQyxDQUFDO1NBQ3REO2FBQU0sSUFBSSxJQUFJLENBQUMsc0JBQXNCLENBQUMsVUFBVSxLQUFLLE9BQU8sRUFBRTtZQUM3RCxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDLEtBQUs7Ozs7WUFBQyxVQUFDLEtBQUs7O29CQUM5QyxJQUFJLEdBQUcsbUJBQUEsS0FBSyxFQUFTO2dCQUMzQixJQUFJLGVBQWUsS0FBSyxJQUFJLENBQUMsT0FBTyxFQUFFOzt3QkFDOUIsT0FBTyxHQUNYLDJEQUEyRDs7d0JBQ3ZELFlBQVksR0FBRyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUM7b0JBQ3ZDLFlBQVksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztvQkFDaEMsWUFBWSxDQUFDLElBQUksR0FBRyxpQkFBaUIsQ0FBQztvQkFDdEMsTUFBTSxZQUFZLENBQUM7aUJBQ3BCO3FCQUFNO29CQUNMLE1BQU0sSUFBSSxDQUFDO2lCQUNaO2dCQUNELHdCQUF3QjtZQUMxQixDQUFDLEVBQUMsQ0FBQztTQUNKO0lBQ0gsQ0FBQzs7Ozs7SUFFTSwwQ0FBTTs7OztJQUFiLFVBQWMsU0FBZ0I7UUFBOUIsaUJBcUNDO1FBckNhLDBCQUFBLEVBQUEsZ0JBQWdCO1FBQzVCLElBQUksU0FBUyxFQUFFOztnQkFDUCxRQUFRLEdBQUcsU0FBUyxDQUFDLE1BQU07WUFDakMsSUFBSSxRQUFRLEVBQUU7Z0JBQ1osT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLG9CQUFvQixFQUFFLENBQUMsSUFBSTs7OztnQkFBQyxVQUFDLEdBQUc7b0JBQ3RELE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLEVBQUUsR0FBRyxDQUFDLENBQUM7O3dCQUMvQixJQUFJLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUM7b0JBQzdDLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDbEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsT0FBTyxDQUFDO29CQUMzQixJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxPQUFPLENBQUM7b0JBQzVCLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztvQkFDNUIsSUFBSSxDQUFDLE1BQU07OztvQkFBRzt3QkFDWixPQUFPLENBQUMsR0FBRyxDQUFDLHdDQUF3QyxDQUFDLENBQUM7d0JBQ3RELGlCQUFNLE1BQU0sWUFBRSxDQUFDLElBQUk7Ozt3QkFBQzs0QkFDbEIsS0FBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLEVBQUUsQ0FBQzs0QkFDOUIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ3ZDLENBQUMsRUFBQyxDQUFDO29CQUNMLENBQUMsQ0FBQSxDQUFDO29CQUNGLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNsQyxDQUFDLEVBQUMsQ0FBQztnQkFDSCw0Q0FBNEM7Z0JBQzVDLGdDQUFnQztnQkFDaEMsd0JBQXdCO2dCQUN4Qiw0QkFBNEI7Z0JBQzVCLHNCQUFzQjtnQkFDdEIsbUJBQW1CO2dCQUNuQixRQUFRO2FBQ1Q7aUJBQU07O29CQUNDLE9BQU8sR0FDWCwyREFBMkQ7O29CQUN2RCxZQUFZLEdBQUcsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDO2dCQUN2QyxZQUFZLENBQUMsSUFBSSxHQUFHLGlCQUFpQixDQUFDO2dCQUN0QyxNQUFNLFlBQVksQ0FBQzthQUNwQjtTQUNGO2FBQU07WUFDTCxpQkFBTSxNQUFNLFdBQUUsQ0FBQztTQUNoQjtJQUNILENBQUM7O2dCQTVNRixVQUFVOzs7OztnQkFaRixNQUFNO2dCQUFtQixjQUFjO2dCQU05QyxnQkFBZ0I7Z0JBRWhCLGdCQUFnQjs7SUFpTmxCLGdDQUFDO0NBQUEsQUE3TUQsQ0FDK0MsNkJBQTZCLEdBNE0zRTtTQTVNWSx5QkFBeUI7Ozs7OztJQU1wQyxnREFBK0M7O0lBRS9DLDhDQUFtQzs7SUFhakMsMkRBQ3VCOzs7OztJQUN2QiwyQ0FBd0I7Ozs7O0lBQ3hCLG1EQUF3Qzs7Ozs7SUFDeEMscURBQTRDOzs7OztJQUM1QyxxREFBNEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBPcGVuSURDb25uZWN0VXNlciB9IGZyb20gJy4vLi4vYXV0aGVudGljYXRpb24vdXNlci5tb2RlbCc7XG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBSb3V0ZXIsIE5hdmlnYXRpb25TdGFydCwgQWN0aXZhdGVkUm91dGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHsgZmlsdGVyIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHsgVXNlck1hbmFnZXIsIFdlYlN0b3JhZ2VTdGF0ZVN0b3JlIH0gZnJvbSAnb2lkYy1jbGllbnQnO1xuaW1wb3J0IHsgUHJvdmlkZXJBdXRoZW50aWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi9hdXRoZW50aWNhdGlvbi9wcm92aWRlci1hdXRoZW50aWNhdGlvbi5zZXJ2aWNlJztcbmltcG9ydCB7IE9wZW5JRENvbm5lY3RTZXR0aW5ncyB9IGZyb20gJy4vb2lkYy1tb2R1bGUnO1xuaW1wb3J0IHtcbiAgUGxhdGZvcm1Mb2NhdGlvbixcbiAgSGFzaExvY2F0aW9uU3RyYXRlZ3ksXG4gIExvY2F0aW9uU3RyYXRlZ3ksXG59IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG4vLyBAZHluYW1pY1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgT2lkY0F1dGhlbnRpY2F0aW9uU2VydmljZSBleHRlbmRzIFByb3ZpZGVyQXV0aGVudGljYXRpb25TZXJ2aWNlPFxuICBPaWRjLlVzZXIsXG4gIE9wZW5JRENvbm5lY3RVc2VyLFxuICB2b2lkLFxuICBib29sZWFuXG4+IHtcbiAgcHJvdGVjdGVkIHVzZXJNYW5hZ2VyOiBPaWRjLlVzZXJNYW5hZ2VyID0gbnVsbDtcblxuICBwdWJsaWMgdXNlclZhbHVlOiBPaWRjLlVzZXIgPSBudWxsO1xuXG4gIGdldCBuZWVkUmVuZXdVc2VyKCkge1xuICAgIHJldHVybiB0aGlzLnVzZXJWYWx1ZSAhPSBudWxsICYmIHRoaXMudXNlclZhbHVlLmV4cGlyZWQgPT09IHRydWU7XG4gIH1cblxuICByZW5ld1VzZXIoKSB7XG4gICAgcmV0dXJuIHRoaXMudXNlck1hbmFnZXJcbiAgICAgIC5zaWduaW5TaWxlbnQoKVxuICAgICAgLnRoZW4oKHVzZXIpID0+ICgpID0+IHRoaXMubG9hZE9pZGNVc2VyKHVzZXIpKTtcbiAgfVxuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHB1YmxpYyBhdXRoZW50aWNhdGlvblNldHRpbmdzOiBPaWRjLlVzZXJNYW5hZ2VyU2V0dGluZ3MgJlxuICAgICAgT3BlbklEQ29ubmVjdFNldHRpbmdzLFxuICAgIHByb3RlY3RlZCByb3V0ZXI6IFJvdXRlcixcbiAgICBwcm90ZWN0ZWQgYWN0aXZhdGVkUm91dGU6IEFjdGl2YXRlZFJvdXRlLFxuICAgIHByb3RlY3RlZCBwbGF0Zm9ybUxvY2F0aW9uOiBQbGF0Zm9ybUxvY2F0aW9uLFxuICAgIHByb3RlY3RlZCBsb2NhdGlvblN0cmF0ZWd5OiBMb2NhdGlvblN0cmF0ZWd5XG4gICkge1xuICAgIHN1cGVyKCk7XG5cbiAgICBjb25zdCB1c2VyTWFuYWdlckFyZyA9IHtcbiAgICAgIC4uLmF1dGhlbnRpY2F0aW9uU2V0dGluZ3MsXG4gICAgICB1c2VyU3RvcmU6IG5ldyBXZWJTdG9yYWdlU3RhdGVTdG9yZSh7IHN0b3JlOiB3aW5kb3cubG9jYWxTdG9yYWdlIH0pLFxuICAgIH07XG4gICAgdGhpcy51c2VyTWFuYWdlciA9IG5ldyBVc2VyTWFuYWdlcih1c2VyTWFuYWdlckFyZyk7XG4gICAgdGhpcy51c2VyTWFuYWdlci5ldmVudHMuYWRkVXNlckxvYWRlZCgodXNlcikgPT4ge1xuICAgICAgdGhpcy5sb2FkT2lkY1VzZXIodXNlcik7XG4gICAgfSk7XG4gICAgdGhpcy51c2VyTWFuYWdlci5ldmVudHMuYWRkQWNjZXNzVG9rZW5FeHBpcmVkKChldmVudCkgPT4ge1xuICAgICAgdGhpcy51c2VyTWFuYWdlci5zaWduaW5TaWxlbnQoKS5jYXRjaCgoZXJyb3IpID0+IHtcbiAgICAgICAgY29uc29sZS5lcnJvcihlcnJvcik7XG4gICAgICAgIHRoaXMudXNlck1hbmFnZXIucmVtb3ZlVXNlcigpO1xuICAgICAgICBzdXBlci5sb2dvdXQoKTtcbiAgICAgIH0pO1xuICAgIH0pO1xuICAgIHRoaXMudXNlck1hbmFnZXIuZXZlbnRzLmFkZFNpbGVudFJlbmV3RXJyb3IoKGVycm9yKSA9PiB7XG4gICAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcbiAgICAgIHRoaXMudXNlck1hbmFnZXIucmVtb3ZlVXNlcigpO1xuICAgICAgc3VwZXIubG9nb3V0KCk7XG4gICAgfSk7XG4gICAgdGhpcy51c2VyTWFuYWdlci5nZXRVc2VyKCkudGhlbigodXNlcikgPT4ge1xuICAgICAgdGhpcy5sb2FkT2lkY1VzZXIodXNlcik7XG4gICAgfSk7XG5cbiAgICBpZiAodGhpcy5hdXRoZW50aWNhdGlvblNldHRpbmdzLmF1dG9tYXRpY1NpbGVudFJlbmV3KSB7XG4gICAgICB0aGlzLnJvdXRlci5ldmVudHNcbiAgICAgICAgLnBpcGUoZmlsdGVyKChldmVudCkgPT4gZXZlbnQgaW5zdGFuY2VvZiBOYXZpZ2F0aW9uU3RhcnQpKVxuICAgICAgICAucGlwZShcbiAgICAgICAgICBmaWx0ZXIoKGV2ZW50OiBOYXZpZ2F0aW9uU3RhcnQpID0+XG4gICAgICAgICAgICBuZXcgUmVnRXhwKHRoaXMuYXV0aGVudGljYXRpb25TZXR0aW5ncy5zaWxlbnRfcmVkaXJlY3RfdXJpKS50ZXN0KFxuICAgICAgICAgICAgICBsb2NhdGlvbi5ocmVmXG4gICAgICAgICAgICApXG4gICAgICAgICAgKVxuICAgICAgICApXG4gICAgICAgIC5zdWJzY3JpYmUoKGV2ZW50OiBOYXZpZ2F0aW9uU3RhcnQpID0+IHtcbiAgICAgICAgICB0aGlzLnVzZXJNYW5hZ2VyLnNpZ25pblNpbGVudENhbGxiYWNrKCkuY2F0Y2goKGVycikgPT4ge1xuICAgICAgICAgICAgY29uc29sZS5lcnJvcihlcnIpO1xuICAgICAgICAgICAgdGhpcy51c2VyTWFuYWdlci5zdGFydFNpbGVudFJlbmV3KCk7XG4gICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIHRoaXMucm91dGVyLmV2ZW50c1xuICAgICAgLnBpcGUoXG4gICAgICAgIGZpbHRlcigoZXZlbnQ6IE5hdmlnYXRpb25TdGFydCkgPT5cbiAgICAgICAgICBuZXcgUmVnRXhwKHRoaXMuYXV0aGVudGljYXRpb25TZXR0aW5ncy5yZWRpcmVjdF91cmkpLnRlc3QoXG4gICAgICAgICAgICBsb2NhdGlvbi5ocmVmXG4gICAgICAgICAgKVxuICAgICAgICApXG4gICAgICApXG4gICAgICAuc3Vic2NyaWJlKChldmVudDogTmF2aWdhdGlvblN0YXJ0KSA9PiB7XG4gICAgICAgIHRoaXMudXNlck1hbmFnZXIuc2lnbmluUmVkaXJlY3RDYWxsYmFjaygpO1xuICAgICAgfSk7XG4gIH1cblxuICBwdWJsaWMgbG9hZE9pZGNVc2VyKHVzZXIpIHtcbiAgICB0aGlzLnVzZXJWYWx1ZSA9IHVzZXI7XG4gICAgdGhpcy5sb2FkUHJvdmlkZXJVc2VyKHVzZXIpO1xuICB9XG5cbiAgcHVibGljIHRyYW5zZm9ybSh1c2VyOiBPaWRjLlVzZXIpOiBPcGVuSURDb25uZWN0VXNlciB7XG4gICAgY29uc3QgZGVzdGluYXRpb25Vc2VyID0ge30gYXMgT3BlbklEQ29ubmVjdFVzZXI7XG4gICAgbGV0IGNvbWJpbmVkVXNlciA9IE9iamVjdC5hc3NpZ24oZGVzdGluYXRpb25Vc2VyLCB1c2VyKTtcbiAgICAvLyBPIE9JRENfQ2xpZW50IHVzYSBhIHByb3ByaWVkYWRlIHByb2ZpbGUgcGFyYSBjb2xvY2FyIGNsYWltcyxcbiAgICAvLyBwb3J0YW50byBkZXZlbW9zIGZhemVyIG8gYXNzaWduIGRlc3RlIGNvbSBvIG9iamV0aXZvIGRlIG1hbnRlclxuICAgIC8vIG5vIHVzdcOhcmlvIHRhbWLDqW0gYXMgaW5mb3JtYcOnw7VlcyAnbsOjbyBtYXBlYWRhcydcbiAgICBpZiAodXNlci5wcm9maWxlKSB7XG4gICAgICBjb21iaW5lZFVzZXIgPSBPYmplY3QuYXNzaWduKGNvbWJpbmVkVXNlciwgdXNlci5wcm9maWxlKTtcbiAgICB9XG5cbiAgICBjb21iaW5lZFVzZXIuYXV0aGVudGljYXRlZCA9IHRydWU7XG5cbiAgICBjb21iaW5lZFVzZXIuc3ViID0gdXNlci5wcm9maWxlLnN1YiB8fCBudWxsO1xuICAgIGNvbWJpbmVkVXNlci5uYW1lID0gdXNlci5wcm9maWxlLm5hbWUgfHwgbnVsbDtcbiAgICBjb21iaW5lZFVzZXIucHJlZmVycmVkX3VzZXJuYW1lID0gdXNlci5wcm9maWxlLnByZWZlcnJlZF91c2VybmFtZSB8fCBudWxsO1xuICAgIGNvbWJpbmVkVXNlci5naXZlbl9uYW1lID0gdXNlci5wcm9maWxlLmdpdmVuX25hbWUgfHwgbnVsbDtcbiAgICBjb21iaW5lZFVzZXIubmlja25hbWUgPSB1c2VyLnByb2ZpbGUubmlja25hbWUgfHwgbnVsbDtcbiAgICBjb21iaW5lZFVzZXIuZW1haWwgPSB1c2VyLnByb2ZpbGUuZW1haWwgfHwgbnVsbDtcbiAgICBjb21iaW5lZFVzZXIucGljdHVyZSA9XG4gICAgICAodXNlci5wcm9maWxlLnBpY3R1cmUgJiYgdXNlci5wcm9maWxlLnBpY3R1cmVbMF0pIHx8IG51bGw7XG4gICAgY29tYmluZWRVc2VyLmNsaWVudF9pZCA9IHVzZXIucHJvZmlsZS5jbGllbnRfaWQgfHwgbnVsbDtcbiAgICBjb21iaW5lZFVzZXIuYWNjZXNzX3Rva2VuID0gdXNlci5hY2Nlc3NfdG9rZW4gfHwgbnVsbDtcbiAgICBjb21iaW5lZFVzZXIuaWRfdG9rZW4gPSB1c2VyLmlkX3Rva2VuIHx8IG51bGw7XG4gICAgY29tYmluZWRVc2VyLnJvbGVzID0gdXNlci5wcm9maWxlLnJvbGVzIHx8IG51bGw7XG5cbiAgICByZXR1cm4gY29tYmluZWRVc2VyO1xuICB9XG5cbiAgcHVibGljIGxvZ2luKGFyZ3M/KSB7XG4gICAgY29uc3QgYmFzZUhyZWYgPSB0aGlzLnBsYXRmb3JtTG9jYXRpb24uZ2V0QmFzZUhyZWZGcm9tRE9NKCk7XG4gICAgY29uc3Qgcm91dGVBY3RpdmVJc1VuYXV0aG9yaXplZCA9IHRoaXMucm91dGVyLmlzQWN0aXZlKFxuICAgICAgYmFzZUhyZWYgKyAndW5hdXRob3JpemVkJyxcbiAgICAgIGZhbHNlXG4gICAgKTtcbiAgICBpZiAoXG4gICAgICB0aGlzLmF1dGhlbnRpY2F0aW9uU2V0dGluZ3MubG9naW5fbW9kZSA9PT0gJ3JlZGlyZWN0JyB8fFxuICAgICAgKHRoaXMuYXV0aGVudGljYXRpb25TZXR0aW5ncy5sb2dpbl9tb2RlID09PSAnaWZyYW1lJyAmJlxuICAgICAgICByb3V0ZUFjdGl2ZUlzVW5hdXRob3JpemVkKVxuICAgICkge1xuICAgICAgcmV0dXJuIHRoaXMudXNlck1hbmFnZXIuc2lnbmluUmVkaXJlY3QoYXJncykuY2F0Y2goKGVycm9yKSA9PiB7XG4gICAgICAgIGNvbnN0IGVycm8gPSBlcnJvciBhcyBFcnJvcjtcbiAgICAgICAgaWYgKCdOZXR3b3JrIEVycm9yJyA9PT0gZXJyby5tZXNzYWdlKSB7XG4gICAgICAgICAgY29uc3QgbWVzc2FnZSA9XG4gICAgICAgICAgICAnTsOjbyBmb2kgcG9zc8OtdmVsIHNlIGNvbmVjdGFyIGFvIHNlcnZpZG9yIGRlIGF1dGVudGljYcOnw6NvISc7XG4gICAgICAgICAgY29uc3QgZXJyb3JUb1Rocm93ID0gbmV3IEVycm9yKG1lc3NhZ2UpO1xuICAgICAgICAgIGVycm9yVG9UaHJvdy5zdGFjayA9IGVycm8uc3RhY2s7XG4gICAgICAgICAgZXJyb3JUb1Rocm93Lm5hbWUgPSAnRXJybyBkZSBjb25leMOjbyc7XG4gICAgICAgICAgdGhyb3cgZXJyb3JUb1Rocm93O1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHRocm93IGVycm87XG4gICAgICAgIH1cbiAgICAgICAgLy8gY29uc29sZS5lcnJvcihlcnJvcik7XG4gICAgICB9KTtcbiAgICB9IGVsc2UgaWYgKFxuICAgICAgdGhpcy5hdXRoZW50aWNhdGlvblNldHRpbmdzLmxvZ2luX21vZGUgPT09ICdpZnJhbWUnICYmXG4gICAgICByb3V0ZUFjdGl2ZUlzVW5hdXRob3JpemVkID09PSBmYWxzZVxuICAgICkge1xuICAgICAgcmV0dXJuIHRoaXMucm91dGVyLm5hdmlnYXRlQnlVcmwoYmFzZUhyZWYgKyAnbG9naW4nKTtcbiAgICB9IGVsc2UgaWYgKHRoaXMuYXV0aGVudGljYXRpb25TZXR0aW5ncy5sb2dpbl9tb2RlID09PSAncG9wdXAnKSB7XG4gICAgICByZXR1cm4gdGhpcy51c2VyTWFuYWdlci5zaWduaW5Qb3B1cChhcmdzKS5jYXRjaCgoZXJyb3IpID0+IHtcbiAgICAgICAgY29uc3QgZXJybyA9IGVycm9yIGFzIEVycm9yO1xuICAgICAgICBpZiAoJ05ldHdvcmsgRXJyb3InID09PSBlcnJvLm1lc3NhZ2UpIHtcbiAgICAgICAgICBjb25zdCBtZXNzYWdlID1cbiAgICAgICAgICAgICdOw6NvIGZvaSBwb3Nzw612ZWwgc2UgY29uZWN0YXIgYW8gc2Vydmlkb3IgZGUgYXV0ZW50aWNhw6fDo28hJztcbiAgICAgICAgICBjb25zdCBlcnJvclRvVGhyb3cgPSBuZXcgRXJyb3IobWVzc2FnZSk7XG4gICAgICAgICAgZXJyb3JUb1Rocm93LnN0YWNrID0gZXJyby5zdGFjaztcbiAgICAgICAgICBlcnJvclRvVGhyb3cubmFtZSA9ICdFcnJvIGRlIGNvbmV4w6NvJztcbiAgICAgICAgICB0aHJvdyBlcnJvclRvVGhyb3c7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgdGhyb3cgZXJybztcbiAgICAgICAgfVxuICAgICAgICAvLyBjb25zb2xlLmVycm9yKGVycm9yKTtcbiAgICAgIH0pO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyBsb2dvdXQoc3NvTG9nb3V0ID0gdHJ1ZSkge1xuICAgIGlmIChzc29Mb2dvdXQpIHtcbiAgICAgIGNvbnN0IGlzT25saW5lID0gbmF2aWdhdG9yLm9uTGluZTtcbiAgICAgIGlmIChpc09ubGluZSkge1xuICAgICAgICByZXR1cm4gdGhpcy51c2VyTWFuYWdlci5jcmVhdGVTaWdub3V0UmVxdWVzdCgpLnRoZW4oKHJlcSkgPT4ge1xuICAgICAgICAgIGNvbnNvbGUubG9nKCdSZXF1ZXN0IGxvZ291dDogJywgcmVxKTtcbiAgICAgICAgICBjb25zdCBpZnJtID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnaWZyYW1lJyk7XG4gICAgICAgICAgaWZybS5zZXRBdHRyaWJ1dGUoJ3NyYycsIHJlcS51cmwpO1xuICAgICAgICAgIGlmcm0uc3R5bGUud2lkdGggPSAnNjQwcHgnO1xuICAgICAgICAgIGlmcm0uc3R5bGUuaGVpZ2h0ID0gJzQ4MHB4JztcbiAgICAgICAgICBpZnJtLnN0eWxlLmRpc3BsYXkgPSAnbm9uZSc7XG4gICAgICAgICAgaWZybS5vbmxvYWQgPSAoKSA9PiB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZygnSUZyYW1lIGRvIGxvZ291dCB0ZXJtaW5vdSBkZSBjYXJyZWdhciEnKTtcbiAgICAgICAgICAgIHN1cGVyLmxvZ291dCgpLnRoZW4oKCkgPT4ge1xuICAgICAgICAgICAgICB0aGlzLnVzZXJNYW5hZ2VyLnJlbW92ZVVzZXIoKTtcbiAgICAgICAgICAgICAgaWZybS5wYXJlbnRFbGVtZW50LnJlbW92ZUNoaWxkKGlmcm0pO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfTtcbiAgICAgICAgICBkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKGlmcm0pO1xuICAgICAgICB9KTtcbiAgICAgICAgLy8gcmV0dXJuIHRoaXMudXNlck1hbmFnZXIuc2lnbm91dFJlZGlyZWN0KClcbiAgICAgICAgLy8gICAudGhlbigoKSA9PiBzdXBlci5sb2dvdXQoKSlcbiAgICAgICAgLy8gICAuY2F0Y2goKGVycm9yKSA9PiB7XG4gICAgICAgIC8vICAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcbiAgICAgICAgLy8gICAgIHN1cGVyLmxvZ291dCgpO1xuICAgICAgICAvLyAgICAgdGhyb3cgZXJyb3I7XG4gICAgICAgIC8vICAgfSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBjb25zdCBtZXNzYWdlID1cbiAgICAgICAgICAnTsOjbyBmb2kgcG9zc8OtdmVsIHNlIGNvbmVjdGFyIGFvIHNlcnZpZG9yIGRlIGF1dGVudGljYcOnw6NvISc7XG4gICAgICAgIGNvbnN0IGVycm9yVG9UaHJvdyA9IG5ldyBFcnJvcihtZXNzYWdlKTtcbiAgICAgICAgZXJyb3JUb1Rocm93Lm5hbWUgPSAnRXJybyBkZSBjb25leMOjbyc7XG4gICAgICAgIHRocm93IGVycm9yVG9UaHJvdztcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgc3VwZXIubG9nb3V0KCk7XG4gICAgfVxuICB9XG59XG4iXX0=