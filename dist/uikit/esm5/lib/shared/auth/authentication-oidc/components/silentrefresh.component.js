/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';
var SilentRefreshComponent = /** @class */ (function () {
    function SilentRefreshComponent() {
    }
    /**
     * @return {?}
     */
    SilentRefreshComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    SilentRefreshComponent.decorators = [
        { type: Component, args: [{
                    selector: 'uikit-silentrefresh',
                    template: "",
                    encapsulation: ViewEncapsulation.ShadowDom,
                    changeDetection: ChangeDetectionStrategy.OnPush
                }] }
    ];
    /** @nocollapse */
    SilentRefreshComponent.ctorParameters = function () { return []; };
    return SilentRefreshComponent;
}());
export { SilentRefreshComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2lsZW50cmVmcmVzaC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9hdXRoL2F1dGhlbnRpY2F0aW9uLW9pZGMvY29tcG9uZW50cy9zaWxlbnRyZWZyZXNoLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxpQkFBaUIsRUFBRSx1QkFBdUIsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUc5RjtJQVNFO0lBQWdCLENBQUM7Ozs7SUFFakIseUNBQVE7OztJQUFSO0lBRUEsQ0FBQzs7Z0JBYkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxxQkFBcUI7b0JBQy9CLFFBQVEsRUFBRSxFQUFFO29CQUVaLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxTQUFTO29CQUMxQyxlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtpQkFDaEQ7Ozs7SUFTRCw2QkFBQztDQUFBLEFBZkQsSUFlQztTQVJZLHNCQUFzQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBWaWV3RW5jYXBzdWxhdGlvbiwgQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICd1aWtpdC1zaWxlbnRyZWZyZXNoJyxcbiAgdGVtcGxhdGU6IGBgLFxuICBzdHlsZXM6IFtdLFxuICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5TaGFkb3dEb20sXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoXG59KVxuZXhwb3J0IGNsYXNzIFNpbGVudFJlZnJlc2hDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgbmdPbkluaXQoKSB7XG5cbiAgfVxuXG59XG4iXX0=