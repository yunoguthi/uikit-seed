/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { UserService } from '../../authentication/user.service';
import { Router } from '@angular/router';
import { PlatformLocation, LocationStrategy } from '@angular/common';
var ProtectedRouteComponent = /** @class */ (function () {
    function ProtectedRouteComponent(userService, router, platformLocation, locationStrategy) {
        this.userService = userService;
        this.router = router;
        this.platformLocation = platformLocation;
        this.locationStrategy = locationStrategy;
    }
    /**
     * @return {?}
     */
    ProtectedRouteComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    ProtectedRouteComponent.decorators = [
        { type: Component, args: [{
                    selector: 'uikit-protected-route',
                    template: ""
                }] }
    ];
    /** @nocollapse */
    ProtectedRouteComponent.ctorParameters = function () { return [
        { type: UserService },
        { type: Router },
        { type: PlatformLocation },
        { type: LocationStrategy }
    ]; };
    return ProtectedRouteComponent;
}());
export { ProtectedRouteComponent };
if (false) {
    /**
     * @type {?}
     * @private
     */
    ProtectedRouteComponent.prototype.userService;
    /**
     * @type {?}
     * @protected
     */
    ProtectedRouteComponent.prototype.router;
    /**
     * @type {?}
     * @private
     */
    ProtectedRouteComponent.prototype.platformLocation;
    /**
     * @type {?}
     * @private
     */
    ProtectedRouteComponent.prototype.locationStrategy;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvdGVjdGVkLXJvdXRlLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2F1dGgvYXV0aGVudGljYXRpb24tb2lkYy9jb21wb25lbnRzL3Byb3RlY3RlZC1yb3V0ZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQXNELE1BQU0sZUFBZSxDQUFDO0FBRTlGLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxtQ0FBbUMsQ0FBQztBQUNoRSxPQUFPLEVBQUUsTUFBTSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDekMsT0FBTyxFQUFFLGdCQUFnQixFQUFFLGdCQUFnQixFQUF3QixNQUFNLGlCQUFpQixDQUFDO0FBRzNGO0lBTUUsaUNBQW9CLFdBQXdCLEVBQVksTUFBYyxFQUFVLGdCQUFrQyxFQUN4RyxnQkFBa0M7UUFEeEIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFBWSxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQVUscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUN4RyxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO0lBQUksQ0FBQzs7OztJQUVqRCwwQ0FBUTs7O0lBQVI7SUFHQSxDQUFDOztnQkFaRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLHVCQUF1QjtvQkFDakMsUUFBUSxFQUFFLEVBQUU7aUJBRWI7Ozs7Z0JBVFEsV0FBVztnQkFDWCxNQUFNO2dCQUNOLGdCQUFnQjtnQkFBRSxnQkFBZ0I7O0lBb0IzQyw4QkFBQztDQUFBLEFBakJELElBaUJDO1NBWlksdUJBQXVCOzs7Ozs7SUFDdEIsOENBQWdDOzs7OztJQUFFLHlDQUF3Qjs7Ozs7SUFBRSxtREFBMEM7Ozs7O0lBQ2hILG1EQUEwQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBWaWV3RW5jYXBzdWxhdGlvbiwgQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEF1dGhlbnRpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uL2F1dGhlbnRpY2F0aW9uL2F1dGhlbnRpY2F0aW9uLnNlcnZpY2UnO1xuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tICcuLi8uLi9hdXRoZW50aWNhdGlvbi91c2VyLnNlcnZpY2UnO1xuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IFBsYXRmb3JtTG9jYXRpb24sIExvY2F0aW9uU3RyYXRlZ3ksIEhhc2hMb2NhdGlvblN0cmF0ZWd5IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcblxuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICd1aWtpdC1wcm90ZWN0ZWQtcm91dGUnLFxuICB0ZW1wbGF0ZTogYGAsXG4gIHN0eWxlczogW10sXG59KVxuZXhwb3J0IGNsYXNzIFByb3RlY3RlZFJvdXRlQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgY29uc3RydWN0b3IocHJpdmF0ZSB1c2VyU2VydmljZTogVXNlclNlcnZpY2UsIHByb3RlY3RlZCByb3V0ZXI6IFJvdXRlciwgcHJpdmF0ZSBwbGF0Zm9ybUxvY2F0aW9uOiBQbGF0Zm9ybUxvY2F0aW9uLFxuICAgIHByaXZhdGUgbG9jYXRpb25TdHJhdGVneTogTG9jYXRpb25TdHJhdGVneSkgeyB9XG5cbiAgbmdPbkluaXQoKSB7XG5cblxuICB9XG5cblxuXG5cbn1cbiJdfQ==