/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CallbackComponent } from './components/callback.component';
import { SilentRefreshComponent } from './components/silentrefresh.component';
import { ProtectedRouteComponent } from './components/protected-route.component';
import { LoginIframeComponent } from './components/login-iframe.component';
import { AuthenticationGuardService } from '../authorization/authentication-guard.service';
var ɵ0 = { allowedTypes: ['noshellnobreadcrumb'], defaultType: 'noshellnobreadcrumb' }, ɵ1 = { allowedTypes: ['noshellnobreadcrumb'], defaultType: 'noshellnobreadcrumb' }, ɵ2 = { login: 'auto', setCallback: false, allowedTypes: ['noshellnobreadcrumb'], defaultType: 'noshellnobreadcrumb' };
/** @type {?} */
var routes = [
    { path: 'callback', component: CallbackComponent, data: ɵ0 },
    { path: 'silentrefresh', component: SilentRefreshComponent, data: ɵ1 },
    { path: 'login', component: LoginIframeComponent },
    { path: 'protected-route', component: ProtectedRouteComponent, canActivate: [AuthenticationGuardService],
        data: ɵ2
    },
];
var OidcRoutingModule = /** @class */ (function () {
    function OidcRoutingModule() {
    }
    OidcRoutingModule.decorators = [
        { type: NgModule, args: [{
                    imports: [RouterModule.forChild(routes)],
                    exports: [RouterModule],
                },] }
    ];
    return OidcRoutingModule;
}());
export { OidcRoutingModule };
export { ɵ0, ɵ1, ɵ2 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib2lkYy1yb3V0ZXIubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvYXV0aC9hdXRoZW50aWNhdGlvbi1vaWRjL29pZGMtcm91dGVyLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQVUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDdkQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0saUNBQWlDLENBQUM7QUFDcEUsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFDOUUsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sd0NBQXdDLENBQUM7QUFDakYsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0scUNBQXFDLENBQUM7QUFDM0UsT0FBTyxFQUFFLDBCQUEwQixFQUFFLE1BQU0sK0NBQStDLENBQUM7U0FJakMsRUFBRSxZQUFZLEVBQUUsQ0FBRSxxQkFBcUIsQ0FBRSxFQUFFLFdBQVcsRUFBRSxxQkFBcUIsRUFBRSxPQUNyRSxFQUFFLFlBQVksRUFBRSxDQUFFLHFCQUFxQixDQUFFLEVBQUUsV0FBVyxFQUFFLHFCQUFxQixFQUFFLE9BR3pJLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFFLFlBQVksRUFBRSxDQUFFLHFCQUFxQixDQUFFLEVBQUUsV0FBVyxFQUFFLHFCQUFxQixFQUFFOztJQUx0SCxNQUFNLEdBQVc7SUFDckIsRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRSxpQkFBaUIsRUFBRSxJQUFJLElBQWlGLEVBQUU7SUFDekksRUFBRSxJQUFJLEVBQUUsZUFBZSxFQUFFLFNBQVMsRUFBRSxzQkFBc0IsRUFBRSxJQUFJLElBQWlGLEVBQUU7SUFDbkosRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLFNBQVMsRUFBRSxvQkFBb0IsRUFBRTtJQUNsRCxFQUFFLElBQUksRUFBRSxpQkFBaUIsRUFBRSxTQUFTLEVBQUUsdUJBQXVCLEVBQUUsV0FBVyxFQUFFLENBQUUsMEJBQTBCLENBQUU7UUFDeEcsSUFBSSxJQUFvSDtLQUN6SDtDQUNGO0FBRUQ7SUFBQTtJQUlpQyxDQUFDOztnQkFKakMsUUFBUSxTQUFDO29CQUNSLE9BQU8sRUFBRSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7b0JBQ3hDLE9BQU8sRUFBRSxDQUFDLFlBQVksQ0FBQztpQkFDeEI7O0lBQ2dDLHdCQUFDO0NBQUEsQUFKbEMsSUFJa0M7U0FBckIsaUJBQWlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFJvdXRlcywgUm91dGVyTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IENhbGxiYWNrQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2NhbGxiYWNrLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBTaWxlbnRSZWZyZXNoQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL3NpbGVudHJlZnJlc2guY29tcG9uZW50JztcbmltcG9ydCB7IFByb3RlY3RlZFJvdXRlQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL3Byb3RlY3RlZC1yb3V0ZS5jb21wb25lbnQnO1xuaW1wb3J0IHsgTG9naW5JZnJhbWVDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvbG9naW4taWZyYW1lLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBBdXRoZW50aWNhdGlvbkd1YXJkU2VydmljZSB9IGZyb20gJy4uL2F1dGhvcml6YXRpb24vYXV0aGVudGljYXRpb24tZ3VhcmQuc2VydmljZSc7XG5cblxuY29uc3Qgcm91dGVzOiBSb3V0ZXMgPSBbXG4gIHsgcGF0aDogJ2NhbGxiYWNrJywgY29tcG9uZW50OiBDYWxsYmFja0NvbXBvbmVudCwgZGF0YTogeyBhbGxvd2VkVHlwZXM6IFsgJ25vc2hlbGxub2JyZWFkY3J1bWInIF0sIGRlZmF1bHRUeXBlOiAnbm9zaGVsbG5vYnJlYWRjcnVtYicgfSB9LFxuICB7IHBhdGg6ICdzaWxlbnRyZWZyZXNoJywgY29tcG9uZW50OiBTaWxlbnRSZWZyZXNoQ29tcG9uZW50LCBkYXRhOiB7IGFsbG93ZWRUeXBlczogWyAnbm9zaGVsbG5vYnJlYWRjcnVtYicgXSwgZGVmYXVsdFR5cGU6ICdub3NoZWxsbm9icmVhZGNydW1iJyB9IH0sXG4gIHsgcGF0aDogJ2xvZ2luJywgY29tcG9uZW50OiBMb2dpbklmcmFtZUNvbXBvbmVudCB9LFxuICB7IHBhdGg6ICdwcm90ZWN0ZWQtcm91dGUnLCBjb21wb25lbnQ6IFByb3RlY3RlZFJvdXRlQ29tcG9uZW50LCBjYW5BY3RpdmF0ZTogWyBBdXRoZW50aWNhdGlvbkd1YXJkU2VydmljZSBdLFxuICAgIGRhdGE6IHsgbG9naW46ICdhdXRvJywgc2V0Q2FsbGJhY2s6IGZhbHNlLCBhbGxvd2VkVHlwZXM6IFsgJ25vc2hlbGxub2JyZWFkY3J1bWInIF0sIGRlZmF1bHRUeXBlOiAnbm9zaGVsbG5vYnJlYWRjcnVtYicgfVxuICB9LFxuXTtcblxuQE5nTW9kdWxlKHtcbiAgaW1wb3J0czogW1JvdXRlck1vZHVsZS5mb3JDaGlsZChyb3V0ZXMpXSxcbiAgZXhwb3J0czogW1JvdXRlck1vZHVsZV0sXG59KVxuZXhwb3J0IGNsYXNzIE9pZGNSb3V0aW5nTW9kdWxlIHsgfVxuIl19