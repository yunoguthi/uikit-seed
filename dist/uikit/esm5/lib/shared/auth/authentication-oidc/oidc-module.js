/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { IAuthenticationServiceToken } from './../authentication/abstraction/provider-authentication-service.token';
import { OidcAuthenticationService } from './oidc-authentication.service';
import { NgModule, InjectionToken, Inject, Injectable } from '@angular/core';
import { OidcRoutingModule } from './oidc-router.module';
import { CallbackComponent } from './components/callback.component';
import { SilentRefreshComponent } from './components/silentrefresh.component';
import { ActivatedRoute, Router } from '@angular/router';
import { PlatformLocation, LocationStrategy, CommonModule } from '@angular/common';
import { LoginIframeComponent } from './components/login-iframe.component';
import { ProtectedRouteComponent } from './components/protected-route.component';
/**
 * @record
 */
export function OpenIDConnectSettings() { }
if (false) {
    /** @type {?} */
    OpenIDConnectSettings.prototype.client_uri;
    /**
     * Modo de como efetuar o login. (Padrão é 'iframe' quando não informado)
     * @type {?|undefined}
     */
    OpenIDConnectSettings.prototype.login_mode;
}
/** @type {?} */
export var OIDC_CONFIG = new InjectionToken("OIDC_CONFIG");
var OidcConfigDepHolder = /** @class */ (function () {
    function OidcConfigDepHolder(openIDConnectSettings) {
        this.openIDConnectSettings = openIDConnectSettings;
    }
    OidcConfigDepHolder.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    OidcConfigDepHolder.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [OIDC_CONFIG,] }] }
    ]; };
    return OidcConfigDepHolder;
}());
export { OidcConfigDepHolder };
if (false) {
    /** @type {?} */
    OidcConfigDepHolder.prototype.openIDConnectSettings;
}
// @Injectable()
// export class AuthManagerDepHolder {
//   constructor(
//     @Inject(IAuthenticationManagerToken) @Optional() public authenticationManager: IAuthenticationManager
//   ) {
//   }
// }
/**
 * @param {?} oidcConfigDepHolder
 * @param {?} router
 * @param {?} platformLocation
 * @param {?} activatedRoute
 * @param {?} locationStrategy
 * @return {?}
 */
export function InitOidcAuthenticationService(oidcConfigDepHolder, router, platformLocation, activatedRoute, locationStrategy) {
    /** @type {?} */
    var clientSettings = oidcConfigDepHolder.openIDConnectSettings;
    clientSettings.login_mode = clientSettings.login_mode ? clientSettings.login_mode : 'iframe';
    /** @type {?} */
    var baseHref = platformLocation.getBaseHrefFromDOM();
    /** @type {?} */
    var origin = window.location.origin;
    /** @type {?} */
    var completeUrlToBaseHref = origin + baseHref;
    /** @type {?} */
    var baseClientUri = clientSettings.client_uri || completeUrlToBaseHref;
    /** @type {?} */
    var defaults = {
        redirect_uri: baseClientUri + "callback",
        post_logout_redirect_uri: "" + baseClientUri,
        response_type: "id_token token",
        scope: "openid profile email roles offline_access",
        silent_redirect_uri: baseClientUri + "silentrefresh",
        automaticSilentRenew: true,
        filterProtocolClaims: false,
        loadUserInfo: true
    };
    /** @type {?} */
    var finalSettings = Object.assign(defaults, clientSettings);
    return new OidcAuthenticationService(finalSettings, router, activatedRoute, platformLocation, locationStrategy);
}
var OidcAuthModule = /** @class */ (function () {
    function OidcAuthModule() {
    }
    OidcAuthModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [CallbackComponent, SilentRefreshComponent, ProtectedRouteComponent, LoginIframeComponent],
                    imports: [OidcRoutingModule, CommonModule],
                    providers: [
                        OidcConfigDepHolder,
                        // { provide: APP_INITIALIZER, useFactory: initializeUser, deps: [], multi: true },
                        {
                            // tslint:disable-next-line:object-literal-shorthand
                            provide: IAuthenticationServiceToken,
                            useFactory: InitOidcAuthenticationService,
                            deps: [
                                OidcConfigDepHolder,
                                // ActivatedRoute,
                                Router,
                                PlatformLocation,
                                ActivatedRoute,
                                LocationStrategy,
                            ]
                        },
                    ]
                },] }
    ];
    return OidcAuthModule;
}());
export { OidcAuthModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib2lkYy1tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9hdXRoL2F1dGhlbnRpY2F0aW9uLW9pZGMvb2lkYy1tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSwyQkFBMkIsRUFBRSxNQUFNLHVFQUF1RSxDQUFDO0FBR3BILE9BQU8sRUFBRSx5QkFBeUIsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBQzFFLE9BQU8sRUFDTCxRQUFRLEVBQXVCLGNBQWMsRUFBaUIsTUFBTSxFQUNwRSxVQUFVLEVBQ1gsTUFBTSxlQUFlLENBQUM7QUFDdkIsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDekQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0saUNBQWlDLENBQUM7QUFDcEUsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFDOUUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUl6RCxPQUFPLEVBQUMsZ0JBQWdCLEVBQUUsZ0JBQWdCLEVBQUUsWUFBWSxFQUFDLE1BQU0saUJBQWlCLENBQUM7QUFDakYsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0scUNBQXFDLENBQUM7QUFDM0UsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sd0NBQXdDLENBQUM7Ozs7QUFHakYsMkNBSUM7OztJQUhDLDJDQUFtQjs7Ozs7SUFFbkIsMkNBQTZDOzs7QUFFL0MsTUFBTSxLQUFPLFdBQVcsR0FBRyxJQUFJLGNBQWMsQ0FBd0IsYUFBYSxDQUFDO0FBR25GO0lBRUUsNkJBQzhCLHFCQUE0QztRQUE1QywwQkFBcUIsR0FBckIscUJBQXFCLENBQXVCO0lBRzFFLENBQUM7O2dCQU5GLFVBQVU7Ozs7Z0RBR04sTUFBTSxTQUFDLFdBQVc7O0lBSXZCLDBCQUFDO0NBQUEsQUFQRCxJQU9DO1NBTlksbUJBQW1COzs7SUFFNUIsb0RBQXdFOzs7Ozs7Ozs7Ozs7Ozs7OztBQWM1RSxNQUFNLFVBQVUsNkJBQTZCLENBQzNDLG1CQUF3QyxFQUFFLE1BQWMsRUFBRSxnQkFBa0MsRUFDNUYsY0FBOEIsRUFBRSxnQkFBa0M7O1FBRTFELGNBQWMsR0FBRyxtQkFBbUIsQ0FBQyxxQkFBcUI7SUFFaEUsY0FBYyxDQUFDLFVBQVUsR0FBRyxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUM7O1FBR3ZGLFFBQVEsR0FBRyxnQkFBZ0IsQ0FBQyxrQkFBa0IsRUFBRTs7UUFDaEQsTUFBTSxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTTs7UUFDL0IscUJBQXFCLEdBQUcsTUFBTSxHQUFHLFFBQVE7O1FBR3pDLGFBQWEsR0FBRyxjQUFjLENBQUMsVUFBVSxJQUFJLHFCQUFxQjs7UUFFbEUsUUFBUSxHQUF3QjtRQUNwQyxZQUFZLEVBQUssYUFBYSxhQUFVO1FBQ3hDLHdCQUF3QixFQUFFLEtBQUcsYUFBZTtRQUM1QyxhQUFhLEVBQUUsZ0JBQWdCO1FBQy9CLEtBQUssRUFBRSwyQ0FBMkM7UUFDbEQsbUJBQW1CLEVBQUssYUFBYSxrQkFBZTtRQUNwRCxvQkFBb0IsRUFBRSxJQUFJO1FBQzFCLG9CQUFvQixFQUFFLEtBQUs7UUFDM0IsWUFBWSxFQUFFLElBQUk7S0FDbkI7O1FBRUssYUFBYSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLGNBQWMsQ0FBQztJQUU3RCxPQUFPLElBQUkseUJBQXlCLENBQUMsYUFBYSxFQUFFLE1BQU0sRUFBRSxjQUFjLEVBQUUsZ0JBQWdCLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztBQUNwSCxDQUFDO0FBRUQ7SUFBQTtJQXlCQSxDQUFDOztnQkF6QkEsUUFBUSxTQUFDO29CQUNSLFlBQVksRUFBRSxDQUFDLGlCQUFpQixFQUFFLHNCQUFzQixFQUFFLHVCQUF1QixFQUFFLG9CQUFvQixDQUFDO29CQUN4RyxPQUFPLEVBQUUsQ0FBQyxpQkFBaUIsRUFBRSxZQUFZLENBQUM7b0JBQzFDLFNBQVMsRUFBRTt3QkFDVCxtQkFBbUI7d0JBQ25CLG1GQUFtRjt3QkFFbkY7OzRCQUVFLE9BQU8sRUFBRSwyQkFBMkI7NEJBQ3BDLFVBQVUsRUFBRSw2QkFBNkI7NEJBQ3pDLElBQUksRUFDRjtnQ0FDRSxtQkFBbUI7Z0NBQ25CLGtCQUFrQjtnQ0FDbEIsTUFBTTtnQ0FDTixnQkFBZ0I7Z0NBQ2hCLGNBQWM7Z0NBQ2QsZ0JBQWdCOzZCQUNqQjt5QkFDSjtxQkFFRjtpQkFDRjs7SUFFRCxxQkFBQztDQUFBLEFBekJELElBeUJDO1NBRFksY0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IElBdXRoZW50aWNhdGlvblNlcnZpY2VUb2tlbiB9IGZyb20gJy4vLi4vYXV0aGVudGljYXRpb24vYWJzdHJhY3Rpb24vcHJvdmlkZXItYXV0aGVudGljYXRpb24tc2VydmljZS50b2tlbic7XG4vLyBpbXBvcnQgeyBPaWRjVXNlclRyYW5zZm9ybWF0aW9uU2VydmljZSB9IGZyb20gYC4vb2lkYy11c2VyLXRyYW5zZm9ybWF0aW9uLnNlcnZpY2VgO1xuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tICcuLy4uL2F1dGhlbnRpY2F0aW9uL3VzZXIuc2VydmljZSc7XG5pbXBvcnQgeyBPaWRjQXV0aGVudGljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi9vaWRjLWF1dGhlbnRpY2F0aW9uLnNlcnZpY2UnO1xuaW1wb3J0IHtcbiAgTmdNb2R1bGUsIE1vZHVsZVdpdGhQcm92aWRlcnMsIEluamVjdGlvblRva2VuLCBWYWx1ZVByb3ZpZGVyLCBJbmplY3QsXG4gIEluamVjdGFibGUsIFR5cGUsIE9wdGlvbmFsLCBmb3J3YXJkUmVmLCBQcm92aWRlciwgQVBQX0lOSVRJQUxJWkVSXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgT2lkY1JvdXRpbmdNb2R1bGUgfSBmcm9tICcuL29pZGMtcm91dGVyLm1vZHVsZSc7XG5pbXBvcnQgeyBDYWxsYmFja0NvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9jYWxsYmFjay5jb21wb25lbnQnO1xuaW1wb3J0IHsgU2lsZW50UmVmcmVzaENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9zaWxlbnRyZWZyZXNoLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBBY3RpdmF0ZWRSb3V0ZSwgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IElBdXRoZW50aWNhdGlvbk1hbmFnZXIgfSBmcm9tICcuLi9hdXRoZW50aWNhdGlvbi9hdXRoZW50aWNhdGlvbi1tYW5hZ2VyJztcbmltcG9ydCB7IElBdXRoZW50aWNhdGlvbk1hbmFnZXJUb2tlbiB9IGZyb20gJy4uL2F1dGhlbnRpY2F0aW9uL2F1dGhlbnRpY2F0aW9uLW1hbmFnZXIudG9rZW4nO1xuaW1wb3J0IHsgVXNlck1hbmFnZXJTZXR0aW5ncywgVXNlck1hbmFnZXIsIFdlYlN0b3JhZ2VTdGF0ZVN0b3JlIH0gZnJvbSAnb2lkYy1jbGllbnQnO1xuaW1wb3J0IHtQbGF0Zm9ybUxvY2F0aW9uLCBMb2NhdGlvblN0cmF0ZWd5LCBDb21tb25Nb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQgeyBMb2dpbklmcmFtZUNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9sb2dpbi1pZnJhbWUuY29tcG9uZW50JztcbmltcG9ydCB7IFByb3RlY3RlZFJvdXRlQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL3Byb3RlY3RlZC1yb3V0ZS5jb21wb25lbnQnO1xuXG4vLyBleHBvcnQgdHlwZSBPcGVuSURDb25uZWN0U2V0dGluZ3MgPSBPaWRjLlVzZXJNYW5hZ2VyU2V0dGluZ3MgJiB7IGNsaWVudF91cmk6IHN0cmluZyB9O1xuZXhwb3J0IGludGVyZmFjZSBPcGVuSURDb25uZWN0U2V0dGluZ3MgZXh0ZW5kcyBVc2VyTWFuYWdlclNldHRpbmdzIHtcbiAgY2xpZW50X3VyaTogc3RyaW5nO1xuICAvKiogTW9kbyBkZSBjb21vIGVmZXR1YXIgbyBsb2dpbi4gKFBhZHLDo28gw6kgJ2lmcmFtZScgcXVhbmRvIG7Do28gaW5mb3JtYWRvKSAqL1xuICBsb2dpbl9tb2RlPzogJ3JlZGlyZWN0JyB8ICdwb3B1cCcgfCAnaWZyYW1lJztcbn1cbmV4cG9ydCBjb25zdCBPSURDX0NPTkZJRyA9IG5ldyBJbmplY3Rpb25Ub2tlbjxPcGVuSURDb25uZWN0U2V0dGluZ3M+KGBPSURDX0NPTkZJR2ApO1xuXG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBPaWRjQ29uZmlnRGVwSG9sZGVyIHtcbiAgY29uc3RydWN0b3IoXG4gICAgQEluamVjdChPSURDX0NPTkZJRykgcHVibGljIG9wZW5JRENvbm5lY3RTZXR0aW5nczogT3BlbklEQ29ubmVjdFNldHRpbmdzXG4gICkge1xuXG4gIH1cbn1cblxuLy8gQEluamVjdGFibGUoKVxuLy8gZXhwb3J0IGNsYXNzIEF1dGhNYW5hZ2VyRGVwSG9sZGVyIHtcbi8vICAgY29uc3RydWN0b3IoXG4vLyAgICAgQEluamVjdChJQXV0aGVudGljYXRpb25NYW5hZ2VyVG9rZW4pIEBPcHRpb25hbCgpIHB1YmxpYyBhdXRoZW50aWNhdGlvbk1hbmFnZXI6IElBdXRoZW50aWNhdGlvbk1hbmFnZXJcbi8vICAgKSB7XG4vLyAgIH1cbi8vIH1cblxuZXhwb3J0IGZ1bmN0aW9uIEluaXRPaWRjQXV0aGVudGljYXRpb25TZXJ2aWNlKFxuICBvaWRjQ29uZmlnRGVwSG9sZGVyOiBPaWRjQ29uZmlnRGVwSG9sZGVyLCByb3V0ZXI6IFJvdXRlciwgcGxhdGZvcm1Mb2NhdGlvbjogUGxhdGZvcm1Mb2NhdGlvbixcbiAgYWN0aXZhdGVkUm91dGU6IEFjdGl2YXRlZFJvdXRlLCBsb2NhdGlvblN0cmF0ZWd5OiBMb2NhdGlvblN0cmF0ZWd5KSB7XG5cbiAgICBjb25zdCBjbGllbnRTZXR0aW5ncyA9IG9pZGNDb25maWdEZXBIb2xkZXIub3BlbklEQ29ubmVjdFNldHRpbmdzO1xuXG4gICAgY2xpZW50U2V0dGluZ3MubG9naW5fbW9kZSA9IGNsaWVudFNldHRpbmdzLmxvZ2luX21vZGUgPyBjbGllbnRTZXR0aW5ncy5sb2dpbl9tb2RlIDogJ2lmcmFtZSc7XG5cblxuICAgIGNvbnN0IGJhc2VIcmVmID0gcGxhdGZvcm1Mb2NhdGlvbi5nZXRCYXNlSHJlZkZyb21ET00oKTtcbiAgICBjb25zdCBvcmlnaW4gPSB3aW5kb3cubG9jYXRpb24ub3JpZ2luO1xuICAgIGNvbnN0IGNvbXBsZXRlVXJsVG9CYXNlSHJlZiA9IG9yaWdpbiArIGJhc2VIcmVmO1xuXG5cbiAgICBjb25zdCBiYXNlQ2xpZW50VXJpID0gY2xpZW50U2V0dGluZ3MuY2xpZW50X3VyaSB8fCBjb21wbGV0ZVVybFRvQmFzZUhyZWY7XG5cbiAgICBjb25zdCBkZWZhdWx0czogVXNlck1hbmFnZXJTZXR0aW5ncyA9IHtcbiAgICAgIHJlZGlyZWN0X3VyaTogYCR7YmFzZUNsaWVudFVyaX1jYWxsYmFja2AsXG4gICAgICBwb3N0X2xvZ291dF9yZWRpcmVjdF91cmk6IGAke2Jhc2VDbGllbnRVcml9YCxcbiAgICAgIHJlc3BvbnNlX3R5cGU6IGBpZF90b2tlbiB0b2tlbmAsXG4gICAgICBzY29wZTogYG9wZW5pZCBwcm9maWxlIGVtYWlsIHJvbGVzIG9mZmxpbmVfYWNjZXNzYCxcbiAgICAgIHNpbGVudF9yZWRpcmVjdF91cmk6IGAke2Jhc2VDbGllbnRVcml9c2lsZW50cmVmcmVzaGAsXG4gICAgICBhdXRvbWF0aWNTaWxlbnRSZW5ldzogdHJ1ZSxcbiAgICAgIGZpbHRlclByb3RvY29sQ2xhaW1zOiBmYWxzZSxcbiAgICAgIGxvYWRVc2VySW5mbzogdHJ1ZVxuICAgIH07XG5cbiAgICBjb25zdCBmaW5hbFNldHRpbmdzID0gT2JqZWN0LmFzc2lnbihkZWZhdWx0cywgY2xpZW50U2V0dGluZ3MpO1xuXG4gICAgcmV0dXJuIG5ldyBPaWRjQXV0aGVudGljYXRpb25TZXJ2aWNlKGZpbmFsU2V0dGluZ3MsIHJvdXRlciwgYWN0aXZhdGVkUm91dGUsIHBsYXRmb3JtTG9jYXRpb24sIGxvY2F0aW9uU3RyYXRlZ3kpO1xufVxuXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtDYWxsYmFja0NvbXBvbmVudCwgU2lsZW50UmVmcmVzaENvbXBvbmVudCwgUHJvdGVjdGVkUm91dGVDb21wb25lbnQsIExvZ2luSWZyYW1lQ29tcG9uZW50XSxcbiAgaW1wb3J0czogW09pZGNSb3V0aW5nTW9kdWxlLCBDb21tb25Nb2R1bGVdLFxuICBwcm92aWRlcnM6IFtcbiAgICBPaWRjQ29uZmlnRGVwSG9sZGVyLFxuICAgIC8vIHsgcHJvdmlkZTogQVBQX0lOSVRJQUxJWkVSLCB1c2VGYWN0b3J5OiBpbml0aWFsaXplVXNlciwgZGVwczogW10sIG11bHRpOiB0cnVlIH0sXG5cbiAgICB7XG4gICAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6b2JqZWN0LWxpdGVyYWwtc2hvcnRoYW5kXG4gICAgICBwcm92aWRlOiBJQXV0aGVudGljYXRpb25TZXJ2aWNlVG9rZW4sXG4gICAgICB1c2VGYWN0b3J5OiBJbml0T2lkY0F1dGhlbnRpY2F0aW9uU2VydmljZSxcbiAgICAgIGRlcHM6XG4gICAgICAgIFtcbiAgICAgICAgICBPaWRjQ29uZmlnRGVwSG9sZGVyLFxuICAgICAgICAgIC8vIEFjdGl2YXRlZFJvdXRlLFxuICAgICAgICAgIFJvdXRlcixcbiAgICAgICAgICBQbGF0Zm9ybUxvY2F0aW9uLFxuICAgICAgICAgIEFjdGl2YXRlZFJvdXRlLFxuICAgICAgICAgIExvY2F0aW9uU3RyYXRlZ3ksXG4gICAgICAgIF1cbiAgICB9LFxuICAgIC8vIE9pZGNVc2VyVHJhbnNmb3JtYXRpb25TZXJ2aWNlXG4gIF1cbn0pXG5leHBvcnQgY2xhc3MgT2lkY0F1dGhNb2R1bGUge1xufVxuXG4iXX0=