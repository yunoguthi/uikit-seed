/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { AuthService } from './auth.service';
import { AuthenticationGuardService } from './authorization/authentication-guard.service';
import { RouteAuthorizationGuardService } from './authorization/route-authorization-guard.service';
import { AuthorizationService } from './authorization/authorization.service';
import { UserService } from './authentication/user.service';
import { NgModule } from '@angular/core';
import { AuthenticationService } from './authentication/authentication.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { OidcAuthHttpInterceptor } from './authentication-oidc/auth-http.interceptor';
var AuthModule = /** @class */ (function () {
    function AuthModule() {
    }
    AuthModule.decorators = [
        { type: NgModule, args: [{
                    providers: [
                        AuthenticationGuardService,
                        AuthenticationService,
                        // ProviderUserTransformationService,
                        UserService,
                        AuthorizationService,
                        // AuthorizationGuardService,
                        RouteAuthorizationGuardService,
                        {
                            provide: HTTP_INTERCEPTORS,
                            useClass: OidcAuthHttpInterceptor,
                            multi: true,
                        },
                        AuthService
                    ]
                },] }
    ];
    return AuthModule;
}());
export { AuthModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9hdXRoL2F1dGgubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDN0MsT0FBTyxFQUFFLDBCQUEwQixFQUFFLE1BQU0sOENBQThDLENBQUM7QUFDMUYsT0FBTyxFQUFFLDhCQUE4QixFQUFFLE1BQU0sbURBQW1ELENBQUM7QUFDbkcsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDN0UsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBQzVELE9BQU8sRUFBRSxRQUFRLEVBQTZCLE1BQU0sZUFBZSxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBRWhGLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBR3pELE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBR3RGO0lBQUE7SUErQ0EsQ0FBQzs7Z0JBL0NBLFFBQVEsU0FBQztvQkFDUixTQUFTLEVBQUU7d0JBQ1QsMEJBQTBCO3dCQUMxQixxQkFBcUI7d0JBQ3JCLHFDQUFxQzt3QkFDckMsV0FBVzt3QkFFWCxvQkFBb0I7d0JBQ3BCLDZCQUE2Qjt3QkFDN0IsOEJBQThCO3dCQUU5Qjs0QkFDRSxPQUFPLEVBQUUsaUJBQWlCOzRCQUMxQixRQUFRLEVBQUUsdUJBQXVCOzRCQUNqQyxLQUFLLEVBQUUsSUFBSTt5QkFDWDt3QkFFRixXQUFXO3FCQUNaO2lCQUNGOztJQTRCRCxpQkFBQztDQUFBLEFBL0NELElBK0NDO1NBMUJZLFVBQVUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBBdXRoU2VydmljZSB9IGZyb20gJy4vYXV0aC5zZXJ2aWNlJztcbmltcG9ydCB7IEF1dGhlbnRpY2F0aW9uR3VhcmRTZXJ2aWNlIH0gZnJvbSAnLi9hdXRob3JpemF0aW9uL2F1dGhlbnRpY2F0aW9uLWd1YXJkLnNlcnZpY2UnO1xuaW1wb3J0IHsgUm91dGVBdXRob3JpemF0aW9uR3VhcmRTZXJ2aWNlIH0gZnJvbSAnLi9hdXRob3JpemF0aW9uL3JvdXRlLWF1dGhvcml6YXRpb24tZ3VhcmQuc2VydmljZSc7XG5pbXBvcnQgeyBBdXRob3JpemF0aW9uU2VydmljZSB9IGZyb20gJy4vYXV0aG9yaXphdGlvbi9hdXRob3JpemF0aW9uLnNlcnZpY2UnO1xuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tICcuL2F1dGhlbnRpY2F0aW9uL3VzZXIuc2VydmljZSc7XG5pbXBvcnQgeyBOZ01vZHVsZSwgVHlwZSwgTW9kdWxlV2l0aFByb3ZpZGVycyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQXV0aGVudGljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi9hdXRoZW50aWNhdGlvbi9hdXRoZW50aWNhdGlvbi5zZXJ2aWNlJztcbmltcG9ydCB7IEF1dGhvcml6YXRpb25HdWFyZFNlcnZpY2UgfSBmcm9tICcuL2F1dGhvcml6YXRpb24vYXV0aG9yaXphdGlvbi1ndWFyZC5zZXJ2aWNlJztcbmltcG9ydCB7IEhUVFBfSU5URVJDRVBUT1JTIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgQXV0aEh0dHBJbnRlcmNlcHRvciB9IGZyb20gJy4vYXV0aC1odHRwLmludGVyY2VwdG9yJztcbmltcG9ydCB7IFByb3ZpZGVyVXNlclRyYW5zZm9ybWF0aW9uU2VydmljZSB9IGZyb20gJy4vYXV0aGVudGljYXRpb24vdXNlci10cmFuc2Zvcm1hdGlvbi5zZXJ2aWNlJztcbmltcG9ydCB7IE9pZGNBdXRoSHR0cEludGVyY2VwdG9yIH0gZnJvbSAnLi9hdXRoZW50aWNhdGlvbi1vaWRjL2F1dGgtaHR0cC5pbnRlcmNlcHRvcic7XG5cblxuQE5nTW9kdWxlKHtcbiAgcHJvdmlkZXJzOiBbXG4gICAgQXV0aGVudGljYXRpb25HdWFyZFNlcnZpY2UsXG4gICAgQXV0aGVudGljYXRpb25TZXJ2aWNlLFxuICAgIC8vIFByb3ZpZGVyVXNlclRyYW5zZm9ybWF0aW9uU2VydmljZSxcbiAgICBVc2VyU2VydmljZSxcblxuICAgIEF1dGhvcml6YXRpb25TZXJ2aWNlLFxuICAgIC8vIEF1dGhvcml6YXRpb25HdWFyZFNlcnZpY2UsXG4gICAgUm91dGVBdXRob3JpemF0aW9uR3VhcmRTZXJ2aWNlLFxuXG4gICAge1xuICAgICAgcHJvdmlkZTogSFRUUF9JTlRFUkNFUFRPUlMsXG4gICAgICB1c2VDbGFzczogT2lkY0F1dGhIdHRwSW50ZXJjZXB0b3IsXG4gICAgICBtdWx0aTogdHJ1ZSxcbiAgICAgfSxcblxuICAgIEF1dGhTZXJ2aWNlXG4gIF1cbn1cbilcbmV4cG9ydCBjbGFzcyBBdXRoTW9kdWxlIHtcbiAgLy8gc3RhdGljIGZvclJvb3QoXG4gIC8vICAgYXV0aGVudGljYXRpb25Qcm92aWRlcnM6IChhbnlbXSB8IFR5cGU8YW55PiB8IE1vZHVsZVdpdGhQcm92aWRlcnMpW11cbiAgLy8gKTogKGFueVtdIHwgVHlwZTxhbnk+IHwgTW9kdWxlV2l0aFByb3ZpZGVycylbXSB7XG4gIC8vICAgY29uc3QgbW9kdWxlQ29uZmlnID0gW1xuICAvLyAgICAgYXV0aGVudGljYXRpb25Qcm92aWRlcnMsXG4gIC8vICAgICB7XG4gIC8vICAgICAgIG5nTW9kdWxlOiBBdXRoTW9kdWxlLFxuICAvLyAgICAgICBwcm92aWRlcnM6IFtcbiAgLy8gICAgICAgICBBdXRoZW50aWNhdGlvbkd1YXJkU2VydmljZSxcbiAgLy8gICAgICAgICBBdXRoZW50aWNhdGlvblNlcnZpY2UsXG4gIC8vICAgICAgICAgSWRlbnRpdHlTZXJ2aWNlLFxuXG4gIC8vICAgICAgICAgQXV0aG9yaXphdGlvblNlcnZpY2UsXG4gIC8vICAgICAgICAgQXV0aG9yaXphdGlvbkd1YXJkU2VydmljZSxcbiAgLy8gICAgICAgICBSb3V0ZUF1dGhvcml6YXRpb25HdWFyZFNlcnZpY2UsXG4gIC8vICAgICAgIF1cbiAgLy8gICAgIH1cbiAgLy8gICBdIGFzIChhbnlbXSB8IFR5cGU8YW55PiB8IE1vZHVsZVdpdGhQcm92aWRlcnMpW107XG5cbiAgLy8gICByZXR1cm4gbW9kdWxlQ29uZmlnO1xuICAvLyAgIC8vIG1vZHVsZUNvbmZpZy5wdXNoKGF1dGhlbnRpY2F0aW9uUHJvdmlkZXJzKTtcbiAgLy8gICAvLyAvL2NvbnN0IHJldG9ybm9BdHJpYnVpZG8gPSBPYmplY3QuYXNzaWduKG1vZHVsZUNvbmZpZywgYXV0aGVudGljYXRpb25Qcm92aWRlcnMpO1xuXG4gIC8vICAgLy8gcmV0dXJuIG1vZHVsZUNvbmZpZztcbiAgLy8gfVxufVxuIl19