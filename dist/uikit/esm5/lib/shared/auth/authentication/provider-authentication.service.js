/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { BehaviorSubject } from 'rxjs';
// tslint:disable-next-line:max-line-length
/**
 * @abstract
 * @template TUserProvider, TUser, TLoginParam, TLogoutParam
 */
var 
// tslint:disable-next-line:max-line-length
/**
 * @abstract
 * @template TUserProvider, TUser, TLoginParam, TLogoutParam
 */
ProviderAuthenticationService = /** @class */ (function () {
    function ProviderAuthenticationService() {
        var _this = this;
        this.userProviderSubject = new BehaviorSubject(null);
        // protected userProvider$ = this.userProviderSubject.asObservable();
        this.userSubject = new BehaviorSubject(null);
        this.user$ = this.userSubject.asObservable();
        this.userProviderSubject.subscribe((/**
         * @param {?} userProvider
         * @return {?}
         */
        function (userProvider) {
            if (userProvider) {
                /** @type {?} */
                var user = _this.transform(userProvider);
                _this.loadUser(user);
            }
            else {
                _this.loadUser(null);
            }
        }));
    }
    /**
     * @param {?=} param
     * @return {?}
     */
    ProviderAuthenticationService.prototype.logout = /**
     * @param {?=} param
     * @return {?}
     */
    function (param) {
        var _this = this;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            _this.loadProviderUser(null);
            resolve();
        }));
    };
    /**
     * @private
     * @param {?} user
     * @return {?}
     */
    ProviderAuthenticationService.prototype.loadUser = /**
     * @private
     * @param {?} user
     * @return {?}
     */
    function (user) {
        this.userSubject.next(user);
    };
    /**
     * @protected
     * @param {?} user
     * @return {?}
     */
    ProviderAuthenticationService.prototype.loadProviderUser = /**
     * @protected
     * @param {?} user
     * @return {?}
     */
    function (user) {
        this.userProviderSubject.next(user);
    };
    return ProviderAuthenticationService;
}());
// tslint:disable-next-line:max-line-length
/**
 * @abstract
 * @template TUserProvider, TUser, TLoginParam, TLogoutParam
 */
export { ProviderAuthenticationService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    ProviderAuthenticationService.prototype.userProviderSubject;
    /**
     * @type {?}
     * @private
     */
    ProviderAuthenticationService.prototype.userSubject;
    /** @type {?} */
    ProviderAuthenticationService.prototype.user$;
    /**
     * @abstract
     * @param {?=} param
     * @return {?}
     */
    ProviderAuthenticationService.prototype.login = function (param) { };
    /**
     * @abstract
     * @protected
     * @param {?} providerUser
     * @return {?}
     */
    ProviderAuthenticationService.prototype.transform = function (providerUser) { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvdmlkZXItYXV0aGVudGljYXRpb24uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2F1dGgvYXV0aGVudGljYXRpb24vcHJvdmlkZXItYXV0aGVudGljYXRpb24uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBR0EsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLE1BQU0sQ0FBQzs7Ozs7O0FBTXZDOzs7Ozs7O0lBZUU7UUFBQSxpQkFTQztRQXRCTyx3QkFBbUIsR0FBbUMsSUFBSSxlQUFlLENBQWdCLElBQUksQ0FBQyxDQUFDOztRQUcvRixnQkFBVyxHQUEyQixJQUFJLGVBQWUsQ0FBUSxJQUFJLENBQUMsQ0FBQztRQUN4RSxVQUFLLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQVU3QyxJQUFJLENBQUMsbUJBQW1CLENBQUMsU0FBUzs7OztRQUFDLFVBQUEsWUFBWTtZQUM3QyxJQUFJLFlBQVksRUFBRTs7b0JBQ1YsSUFBSSxHQUFHLEtBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDO2dCQUN6QyxLQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ3JCO2lCQUFNO2dCQUNMLEtBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDckI7UUFDSCxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBZkQsOENBQU07Ozs7SUFBTixVQUFPLEtBQW9CO1FBQTNCLGlCQUtDO1FBSkMsT0FBTyxJQUFJLE9BQU87Ozs7O1FBQUMsVUFBQyxPQUFPLEVBQUUsTUFBTTtZQUNqQyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDNUIsT0FBTyxFQUFFLENBQUM7UUFDWixDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7OztJQVlPLGdEQUFROzs7OztJQUFoQixVQUFpQixJQUFXO1FBQzFCLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzlCLENBQUM7Ozs7OztJQUNTLHdEQUFnQjs7Ozs7SUFBMUIsVUFBMkIsSUFBbUI7UUFDNUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN0QyxDQUFDO0lBQ0gsb0NBQUM7QUFBRCxDQUFDLEFBaENELElBZ0NDOzs7Ozs7Ozs7Ozs7SUE5QkMsNERBQXVHOzs7OztJQUd2RyxvREFBK0U7O0lBQy9FLDhDQUErQzs7Ozs7O0lBRS9DLHFFQUFtRDs7Ozs7OztJQWlCbkQsZ0ZBQWlFIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tICcuL3VzZXIuc2VydmljZSc7XG5pbXBvcnQgeyBJUHJvdmlkZXJBdXRoZW50aWNhdGlvblNlcnZpY2UgfSBmcm9tICcuL2Fic3RyYWN0aW9uL3Byb3ZpZGVyLWF1dGhlbnRpY2F0aW9uLnNlcnZpY2UnO1xuaW1wb3J0IHsgVXNlciB9IGZyb20gJy4vdXNlci5tb2RlbCc7XG5pbXBvcnQgeyBCZWhhdmlvclN1YmplY3QgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IG1hcCwgdGFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuXG5cblxuLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOm1heC1saW5lLWxlbmd0aFxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIFByb3ZpZGVyQXV0aGVudGljYXRpb25TZXJ2aWNlPFRVc2VyUHJvdmlkZXIgZXh0ZW5kcyBhbnkgPSBhbnksIFRVc2VyIGV4dGVuZHMgVXNlciA9IFVzZXIsIFRMb2dpblBhcmFtIGV4dGVuZHMgYW55ID0gYW55LCBUTG9nb3V0UGFyYW0gZXh0ZW5kcyBhbnkgPSBhbnk+IGltcGxlbWVudHMgSVByb3ZpZGVyQXV0aGVudGljYXRpb25TZXJ2aWNlIHtcblxuICBwcml2YXRlIHVzZXJQcm92aWRlclN1YmplY3Q6IEJlaGF2aW9yU3ViamVjdDxUVXNlclByb3ZpZGVyPiA9IG5ldyBCZWhhdmlvclN1YmplY3Q8VFVzZXJQcm92aWRlcj4obnVsbCk7XG4gIC8vIHByb3RlY3RlZCB1c2VyUHJvdmlkZXIkID0gdGhpcy51c2VyUHJvdmlkZXJTdWJqZWN0LmFzT2JzZXJ2YWJsZSgpO1xuXG4gIHByaXZhdGUgdXNlclN1YmplY3Q6IEJlaGF2aW9yU3ViamVjdDxUVXNlcj4gPSBuZXcgQmVoYXZpb3JTdWJqZWN0PFRVc2VyPihudWxsKTtcbiAgcHVibGljIHVzZXIkID0gdGhpcy51c2VyU3ViamVjdC5hc09ic2VydmFibGUoKTtcblxuICBhYnN0cmFjdCBsb2dpbihwYXJhbT86IFRMb2dpblBhcmFtKTogUHJvbWlzZTx2b2lkPjtcbiAgbG9nb3V0KHBhcmFtPzogVExvZ291dFBhcmFtKTogUHJvbWlzZTx2b2lkPiB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgIHRoaXMubG9hZFByb3ZpZGVyVXNlcihudWxsKTtcbiAgICAgIHJlc29sdmUoKTtcbiAgICB9KTtcbiAgfVxuICBjb25zdHJ1Y3RvcigpIHtcbiAgICB0aGlzLnVzZXJQcm92aWRlclN1YmplY3Quc3Vic2NyaWJlKHVzZXJQcm92aWRlciA9PiB7XG4gICAgICBpZiAodXNlclByb3ZpZGVyKSB7XG4gICAgICAgIGNvbnN0IHVzZXIgPSB0aGlzLnRyYW5zZm9ybSh1c2VyUHJvdmlkZXIpO1xuICAgICAgICB0aGlzLmxvYWRVc2VyKHVzZXIpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5sb2FkVXNlcihudWxsKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuICBwcm90ZWN0ZWQgYWJzdHJhY3QgdHJhbnNmb3JtKHByb3ZpZGVyVXNlcjogVFVzZXJQcm92aWRlcik6IFRVc2VyO1xuICBwcml2YXRlIGxvYWRVc2VyKHVzZXI6IFRVc2VyKSB7XG4gICAgdGhpcy51c2VyU3ViamVjdC5uZXh0KHVzZXIpO1xuICB9XG4gIHByb3RlY3RlZCBsb2FkUHJvdmlkZXJVc2VyKHVzZXI6IFRVc2VyUHJvdmlkZXIpIHtcbiAgICB0aGlzLnVzZXJQcm92aWRlclN1YmplY3QubmV4dCh1c2VyKTtcbiAgfVxufVxuXG5cbi8vIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTptYXgtbGluZS1sZW5ndGhcbi8vIGV4cG9ydCBhYnN0cmFjdCBjbGFzcyBCYXNlUHJvdmlkZXJBdXRoZW50aWNhdGlvblNlcnZpY2U8VExvZ2luUGFyYW1zIGV4dGVuZHMgYW55LCBUTG9nb3V0UGFyYW1zIGV4dGVuZHMgYW55PiBpbXBsZW1lbnRzIElBdXRoZW50aWNhdGlvblNlcnZpY2U8VExvZ2luUGFyYW1zLCBUTG9nb3V0UGFyYW1zPiB7XG4vLyAgIHB1YmxpYyBhYnN0cmFjdCBsb2dpbihwYXJhbTogVExvZ2luUGFyYW1zKTogUHJvbWlzZTxVc2VyPjtcbi8vICAgY29uc3RydWN0b3IocHJvdGVjdGVkIHVzZXJTZXJ2aWNlOiBVc2VyU2VydmljZSkge31cbi8vICAgcHVibGljIGxvZ291dChwYXJhbTogVExvZ291dFBhcmFtcyk6IFByb21pc2U8dm9pZD4ge1xuLy8gICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4vLyAgICAgICB0cnkge1xuLy8gICAgICAgICB0aGlzLnVzZXJTZXJ2aWNlLnVubG9hZCgpO1xuLy8gICAgICAgICByZXNvbHZlKCk7XG4vLyAgICAgICB9IGNhdGNoIChlcnJvcikge1xuLy8gICAgICAgICByZWplY3QoZXJyb3IpO1xuLy8gICAgICAgfVxuLy8gICAgIH0pO1xuLy8gICB9XG4vLyB9XG4iXX0=