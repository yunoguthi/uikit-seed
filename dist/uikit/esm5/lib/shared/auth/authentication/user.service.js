/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
var AnonymousUser = /** @class */ (function () {
    function AnonymousUser() {
        this.sub = null;
        this.authenticated = false;
        this.name = 'Anônimo';
        this.email = null;
    }
    return AnonymousUser;
}());
export { AnonymousUser };
if (false) {
    /** @type {?} */
    AnonymousUser.prototype.sub;
    /** @type {?} */
    AnonymousUser.prototype.authenticated;
    /** @type {?} */
    AnonymousUser.prototype.name;
    /** @type {?} */
    AnonymousUser.prototype.email;
}
/**
 * @template TUser
 */
var UserService = /** @class */ (function () {
    function UserService() {
        this.anonymousUser = new AnonymousUser();
        this.userSubject$ = new BehaviorSubject((/** @type {?} */ (this.anonymousUser)));
        this.user$ = this.userSubject$.asObservable();
    }
    Object.defineProperty(UserService.prototype, "userValue", {
        get: /**
         * @return {?}
         */
        function () {
            return this.userSubject$.getValue();
        },
        enumerable: true,
        configurable: true
    });
    /** Seta o usuário (não deve ser usado na aplicação [somente em testes]).
     * Para login e logout utilize o serviço 'AuthenticationService'
     */
    /**
     * Seta o usuário (não deve ser usado na aplicação [somente em testes]).
     * Para login e logout utilize o serviço 'AuthenticationService'
     * @param {?} user
     * @return {?}
     */
    UserService.prototype.load = /**
     * Seta o usuário (não deve ser usado na aplicação [somente em testes]).
     * Para login e logout utilize o serviço 'AuthenticationService'
     * @param {?} user
     * @return {?}
     */
    function (user) {
        if (!user) {
            throw Error('É obrigatório informar um usuário!');
        }
        this.userSubject$.next(user);
    };
    /** Seta o usuário como o anônimo (não deve ser usado na aplicação [somente em testes]).
     * Para login e logout utilize o serviço 'AuthenticationService'
     */
    /**
     * Seta o usuário como o anônimo (não deve ser usado na aplicação [somente em testes]).
     * Para login e logout utilize o serviço 'AuthenticationService'
     * @return {?}
     */
    UserService.prototype.unload = /**
     * Seta o usuário como o anônimo (não deve ser usado na aplicação [somente em testes]).
     * Para login e logout utilize o serviço 'AuthenticationService'
     * @return {?}
     */
    function () {
        this.userSubject$.next((/** @type {?} */ (this.anonymousUser)));
    };
    UserService.decorators = [
        { type: Injectable }
    ];
    return UserService;
}());
export { UserService };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    UserService.prototype.anonymousUser;
    /**
     * @type {?}
     * @private
     */
    UserService.prototype.userSubject$;
    /** @type {?} */
    UserService.prototype.user$;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvYXV0aC9hdXRoZW50aWNhdGlvbi91c2VyLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUNBLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFFdkMsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUUzQztJQUFBO1FBQ0UsUUFBRyxHQUFXLElBQUksQ0FBQztRQUNuQixrQkFBYSxHQUFHLEtBQUssQ0FBQztRQUN0QixTQUFJLEdBQUcsU0FBUyxDQUFDO1FBQ2pCLFVBQUssR0FBVyxJQUFJLENBQUM7SUFDdkIsQ0FBQztJQUFELG9CQUFDO0FBQUQsQ0FBQyxBQUxELElBS0M7Ozs7SUFKQyw0QkFBbUI7O0lBQ25CLHNDQUFzQjs7SUFDdEIsNkJBQWlCOztJQUNqQiw4QkFBcUI7Ozs7O0FBR3ZCO0lBQUE7UUFFWSxrQkFBYSxHQUFHLElBQUksYUFBYSxFQUFFLENBQUM7UUFFdEMsaUJBQVksR0FBMkIsSUFBSSxlQUFlLENBQVEsbUJBQUEsSUFBSSxDQUFDLGFBQWEsRUFBUyxDQUFDLENBQUM7UUFDaEcsVUFBSyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsWUFBWSxFQUFFLENBQUM7SUFxQmxELENBQUM7SUFwQkMsc0JBQVcsa0NBQVM7Ozs7UUFBcEI7WUFDRSxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDdEMsQ0FBQzs7O09BQUE7SUFFRDs7T0FFRzs7Ozs7OztJQUNJLDBCQUFJOzs7Ozs7SUFBWCxVQUFZLElBQVc7UUFDckIsSUFBSSxDQUFDLElBQUksRUFBRTtZQUNULE1BQU0sS0FBSyxDQUFDLG9DQUFvQyxDQUFDLENBQUM7U0FDbkQ7UUFDRCxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUMvQixDQUFDO0lBRUQ7O09BRUc7Ozs7OztJQUNJLDRCQUFNOzs7OztJQUFiO1FBQ0UsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsbUJBQUEsSUFBSSxDQUFDLGFBQWEsRUFBUyxDQUFDLENBQUM7SUFDdEQsQ0FBQzs7Z0JBekJGLFVBQVU7O0lBMEJYLGtCQUFDO0NBQUEsQUExQkQsSUEwQkM7U0F6QlksV0FBVzs7Ozs7O0lBQ3RCLG9DQUE4Qzs7Ozs7SUFFOUMsbUNBQXVHOztJQUN2Ryw0QkFBZ0QiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBCZWhhdmlvclN1YmplY3QgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IFVzZXIsIFJvbGUgfSBmcm9tICcuL3VzZXIubW9kZWwnO1xuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5leHBvcnQgY2xhc3MgQW5vbnltb3VzVXNlciBpbXBsZW1lbnRzIFVzZXIge1xuICBzdWI6IHN0cmluZyA9IG51bGw7XG4gIGF1dGhlbnRpY2F0ZWQgPSBmYWxzZTtcbiAgbmFtZSA9ICdBbsO0bmltbyc7XG4gIGVtYWlsOiBzdHJpbmcgPSBudWxsO1xufVxuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgVXNlclNlcnZpY2U8VFVzZXIgZXh0ZW5kcyBVc2VyID0gVXNlcj4ge1xuICBwcm90ZWN0ZWQgYW5vbnltb3VzVXNlciA9IG5ldyBBbm9ueW1vdXNVc2VyKCk7XG5cbiAgcHJpdmF0ZSB1c2VyU3ViamVjdCQ6IEJlaGF2aW9yU3ViamVjdDxUVXNlcj4gPSBuZXcgQmVoYXZpb3JTdWJqZWN0PFRVc2VyPih0aGlzLmFub255bW91c1VzZXIgYXMgVFVzZXIpO1xuICBwdWJsaWMgdXNlciQgPSB0aGlzLnVzZXJTdWJqZWN0JC5hc09ic2VydmFibGUoKTtcbiAgcHVibGljIGdldCB1c2VyVmFsdWUoKSB7XG4gICAgcmV0dXJuIHRoaXMudXNlclN1YmplY3QkLmdldFZhbHVlKCk7XG4gIH1cblxuICAvKiogU2V0YSBvIHVzdcOhcmlvIChuw6NvIGRldmUgc2VyIHVzYWRvIG5hIGFwbGljYcOnw6NvIFtzb21lbnRlIGVtIHRlc3Rlc10pLlxuICAgKiBQYXJhIGxvZ2luIGUgbG9nb3V0IHV0aWxpemUgbyBzZXJ2acOnbyAnQXV0aGVudGljYXRpb25TZXJ2aWNlJ1xuICAgKi9cbiAgcHVibGljIGxvYWQodXNlcjogVFVzZXIpIHtcbiAgICBpZiAoIXVzZXIpIHtcbiAgICAgIHRocm93IEVycm9yKCfDiSBvYnJpZ2F0w7NyaW8gaW5mb3JtYXIgdW0gdXN1w6FyaW8hJyk7XG4gICAgfVxuICAgIHRoaXMudXNlclN1YmplY3QkLm5leHQodXNlcik7XG4gIH1cblxuICAvKiogU2V0YSBvIHVzdcOhcmlvIGNvbW8gbyBhbsO0bmltbyAobsOjbyBkZXZlIHNlciB1c2FkbyBuYSBhcGxpY2HDp8OjbyBbc29tZW50ZSBlbSB0ZXN0ZXNdKS5cbiAgICogUGFyYSBsb2dpbiBlIGxvZ291dCB1dGlsaXplIG8gc2VydmnDp28gJ0F1dGhlbnRpY2F0aW9uU2VydmljZSdcbiAgICovXG4gIHB1YmxpYyB1bmxvYWQoKSB7XG4gICAgdGhpcy51c2VyU3ViamVjdCQubmV4dCh0aGlzLmFub255bW91c1VzZXIgYXMgVFVzZXIpO1xuICB9XG59XG4iXX0=