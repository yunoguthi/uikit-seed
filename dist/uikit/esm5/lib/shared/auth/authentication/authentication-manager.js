/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 * @template TFrameworkUser, TApplicationUser
 */
export function IAuthenticationManager() { }
if (false) {
    /**
     * @param {?} user
     * @return {?}
     */
    IAuthenticationManager.prototype.transform = function (user) { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aGVudGljYXRpb24tbWFuYWdlci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2F1dGgvYXV0aGVudGljYXRpb24vYXV0aGVudGljYXRpb24tbWFuYWdlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUVBLDRDQUVDOzs7Ozs7SUFEQyxpRUFBMkQiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBVc2VyIH0gZnJvbSAnLi91c2VyLm1vZGVsJztcblxuZXhwb3J0IGludGVyZmFjZSBJQXV0aGVudGljYXRpb25NYW5hZ2VyPFRGcmFtZXdvcmtVc2VyIGV4dGVuZHMgVXNlciA9IFVzZXIsIFRBcHBsaWNhdGlvblVzZXIgZXh0ZW5kcyBVc2VyID0gVXNlcj4ge1xuICB0cmFuc2Zvcm0odXNlcjogVEZyYW1ld29ya1VzZXIpOiBQcm9taXNlPFRBcHBsaWNhdGlvblVzZXI+O1xufVxuIl19