/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';
import { map } from 'rxjs/operators';
import { AuthorizationConfigToken } from '../../authorization/authorization-config.token';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
import * as i2 from "../../authorization/authorization-config.token";
var KeycloakAuthenticationManager = /** @class */ (function () {
    function KeycloakAuthenticationManager(httpClient, authorizationConfig) {
        this.httpClient = httpClient;
        this.authorizationConfig = authorizationConfig;
    }
    /**
     * @param {?} user
     * @return {?}
     */
    KeycloakAuthenticationManager.prototype.transform = /**
     * @param {?} user
     * @return {?}
     */
    function (user) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                // Faço request para buscar todas as permissões do usuário (resource owner) para um client específico
                // Neste caso uso o padrão User Managed Access para retornar as informações
                return [2 /*return*/, this.httpClient
                        .post("" + this.authorizationConfig.umaConfig.tokenEndpoint, new HttpParams()
                        .set('grant_type', 'urn:ietf:params:oauth:grant-type:uma-ticket')
                        .set('audience', "" + this.authorizationConfig.clientId)
                        .toString(), {
                        headers: new HttpHeaders()
                            .set('Content-Type', 'application/x-www-form-urlencoded')
                            .set('Authorization', "Bearer " + user.access_token)
                    })
                        // tslint:disable-next-line:max-line-length
                        .pipe(map((/**
                     * @param {?} result
                     * @return {?}
                     */
                    function (result) {
                        // Devo então buscar o payload do access_token e incluir (substituindo o access_token anterior) as informações de autorização
                        /** @type {?} */
                        var accessToken = result.access_token.split('.')[1].replace('-', '+').replace('_', '/');
                        /** @type {?} */
                        var accessTokenPayload1 = JSON.parse(atob(accessToken));
                        /** @type {?} */
                        var usuarioFinal1 = (/** @type {?} */ (Object.assign(user, accessTokenPayload1)));
                        usuarioFinal1.access_token = result.access_token;
                        ((/** @type {?} */ (usuarioFinal1))).access_token = result.access_token;
                        return (/** @type {?} */ (usuarioFinal1));
                    })))
                        // tslint:disable-next-line:max-line-length
                        .pipe(map((/**
                     * @param {?} user
                     * @return {?}
                     */
                    function (user) {
                        // Devo agora retornar as informações do usuário no formado esperado (OpenIDConnectUser & UserWithAuthorizations)
                        /** @type {?} */
                        var userToReturn = (/** @type {?} */ (JSON.parse(JSON.stringify(user))));
                        delete userToReturn['authorization'];
                        userToReturn.authorizations = user.authorization.permissions.map((/**
                         * @param {?} permission
                         * @return {?}
                         */
                        function (permission) {
                            return { action: permission.scopes, resource: permission.rsname, resourceId: permission.rsid };
                        }));
                        return userToReturn;
                    })))
                        .toPromise()];
            });
        });
    };
    KeycloakAuthenticationManager.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    KeycloakAuthenticationManager.ctorParameters = function () { return [
        { type: HttpClient },
        { type: undefined, decorators: [{ type: Inject, args: [AuthorizationConfigToken,] }] }
    ]; };
    /** @nocollapse */ KeycloakAuthenticationManager.ngInjectableDef = i0.defineInjectable({ factory: function KeycloakAuthenticationManager_Factory() { return new KeycloakAuthenticationManager(i0.inject(i1.HttpClient), i0.inject(i2.AuthorizationConfigToken)); }, token: KeycloakAuthenticationManager, providedIn: "root" });
    return KeycloakAuthenticationManager;
}());
export { KeycloakAuthenticationManager };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    KeycloakAuthenticationManager.prototype.httpClient;
    /**
     * @type {?}
     * @protected
     */
    KeycloakAuthenticationManager.prototype.authorizationConfig;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoia2V5Y2xvYWstcHJvdmlkZXItYXV0aGVudGljYXRpb24uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2F1dGgvYXV0aGVudGljYXRpb24va2V5Y2xvYWsva2V5Y2xvYWstcHJvdmlkZXItYXV0aGVudGljYXRpb24uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQzNFLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ25ELE9BQU8sRUFBRSxHQUFHLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNyQyxPQUFPLEVBQXVCLHdCQUF3QixFQUFFLE1BQU0sZ0RBQWdELENBQUM7Ozs7QUFJL0c7SUFJRSx1Q0FDWSxVQUFzQixFQUV0QixtQkFBd0M7UUFGeEMsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUV0Qix3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO0lBQ2pELENBQUM7Ozs7O0lBQ1MsaURBQVM7Ozs7SUFBdEIsVUFDRSxJQUF1Qjs7O2dCQUd2QixxR0FBcUc7Z0JBQ3JHLDJFQUEyRTtnQkFDM0Usc0JBQU8sSUFBSSxDQUFDLFVBQVU7eUJBQ25CLElBQUksQ0FBQyxLQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTLENBQUMsYUFBZSxFQUN6RCxJQUFJLFVBQVUsRUFBRTt5QkFDZixHQUFHLENBQUMsWUFBWSxFQUFFLDZDQUE2QyxDQUFDO3lCQUNoRSxHQUFHLENBQUMsVUFBVSxFQUFFLEtBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVUsQ0FBQzt5QkFDdkQsUUFBUSxFQUFFLEVBQ1g7d0JBQ0UsT0FBTyxFQUFFLElBQUksV0FBVyxFQUFFOzZCQUN6QixHQUFHLENBQUMsY0FBYyxFQUFFLG1DQUFtQyxDQUFDOzZCQUN4RCxHQUFHLENBQUMsZUFBZSxFQUFFLFlBQVUsSUFBSSxDQUFDLFlBQWMsQ0FBQztxQkFDckQsQ0FDRjt3QkFDRCwyQ0FBMkM7eUJBQzFDLElBQUksQ0FBQyxHQUFHOzs7O29CQUFDLFVBQUMsTUFBK0k7Ozs0QkFFbEosV0FBVyxHQUFHLE1BQU0sQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUM7OzRCQUNuRixtQkFBbUIsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQzs7NEJBRW5ELGFBQWEsR0FBRyxtQkFBQSxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxtQkFBbUIsQ0FBQyxFQUFxQjt3QkFDbkYsYUFBYSxDQUFDLFlBQVksR0FBRyxNQUFNLENBQUMsWUFBWSxDQUFDO3dCQUNqRCxDQUFDLG1CQUFBLGFBQWEsRUFBTyxDQUFDLENBQUMsWUFBWSxHQUFHLE1BQU0sQ0FBQyxZQUFZLENBQUM7d0JBRTFELE9BQU8sbUJBQUEsYUFBYSxFQUFPLENBQUM7b0JBQzlCLENBQUMsRUFBQyxDQUFDO3dCQUNILDJDQUEyQzt5QkFDMUMsSUFBSSxDQUFDLEdBQUc7Ozs7b0JBQUMsVUFBQyxJQUE0SDs7OzRCQUVqSSxZQUFZLEdBQUcsbUJBQUEsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQ2lEO3dCQUVwRyxPQUFPLFlBQVksQ0FBQyxlQUFlLENBQUMsQ0FBQzt3QkFFckMsWUFBWSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQyxHQUFHOzs7O3dCQUFDLFVBQUEsVUFBVTs0QkFDekUsT0FBTyxFQUFFLE1BQU0sRUFBRSxVQUFVLENBQUMsTUFBTSxFQUFFLFFBQVEsRUFBRSxVQUFVLENBQUMsTUFBTSxFQUFFLFVBQVUsRUFBRSxVQUFVLENBQUMsSUFBSSxFQUFFLENBQUM7d0JBQ2pHLENBQUMsRUFBQyxDQUFDO3dCQUVILE9BQU8sWUFBWSxDQUFDO29CQUN0QixDQUFDLEVBQUMsQ0FBQzt5QkFDRixTQUFTLEVBQUUsRUFBQzs7O0tBRWhCOztnQkF2REYsVUFBVSxTQUFDO29CQUNWLFVBQVUsRUFBRSxNQUFNO2lCQUNuQjs7OztnQkFUUSxVQUFVO2dEQWFkLE1BQU0sU0FBQyx3QkFBd0I7Ozt3Q0FicEM7Q0ErREMsQUF4REQsSUF3REM7U0FyRFksNkJBQTZCOzs7Ozs7SUFFdEMsbURBQWdDOzs7OztJQUNoQyw0REFDa0QiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBIdHRwQ2xpZW50LCBIdHRwUGFyYW1zLCBIdHRwSGVhZGVycyB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IEluamVjdGFibGUsIEluamVjdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgbWFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHsgQXV0aG9yaXphdGlvbkNvbmZpZywgQXV0aG9yaXphdGlvbkNvbmZpZ1Rva2VuIH0gZnJvbSAnLi4vLi4vYXV0aG9yaXphdGlvbi9hdXRob3JpemF0aW9uLWNvbmZpZy50b2tlbic7XG5pbXBvcnQgeyBPcGVuSURDb25uZWN0VXNlciwgVXNlcldpdGhBdXRob3JpemF0aW9ucyB9IGZyb20gJy4uL3VzZXIubW9kZWwnO1xuaW1wb3J0IHsgSUF1dGhlbnRpY2F0aW9uTWFuYWdlciB9IGZyb20gJy4uL2F1dGhlbnRpY2F0aW9uLW1hbmFnZXInO1xuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBLZXljbG9ha0F1dGhlbnRpY2F0aW9uTWFuYWdlciBpbXBsZW1lbnRzIElBdXRoZW50aWNhdGlvbk1hbmFnZXIge1xuICBjb25zdHJ1Y3RvcihcbiAgICBwcm90ZWN0ZWQgaHR0cENsaWVudDogSHR0cENsaWVudCxcbiAgICBASW5qZWN0KEF1dGhvcml6YXRpb25Db25maWdUb2tlbilcbiAgICBwcm90ZWN0ZWQgYXV0aG9yaXphdGlvbkNvbmZpZzogQXV0aG9yaXphdGlvbkNvbmZpZ1xuICApIHt9XG4gIHB1YmxpYyBhc3luYyB0cmFuc2Zvcm0oXG4gICAgdXNlcjogT3BlbklEQ29ubmVjdFVzZXJcbiAgKTogUHJvbWlzZTxPcGVuSURDb25uZWN0VXNlciAmIFVzZXJXaXRoQXV0aG9yaXphdGlvbnM+IHtcblxuICAgIC8vIEZhw6dvIHJlcXVlc3QgcGFyYSBidXNjYXIgdG9kYXMgYXMgcGVybWlzc8O1ZXMgZG8gdXN1w6FyaW8gKHJlc291cmNlIG93bmVyKSBwYXJhIHVtIGNsaWVudCBlc3BlY8OtZmljb1xuICAgIC8vIE5lc3RlIGNhc28gdXNvIG8gcGFkcsOjbyBVc2VyIE1hbmFnZWQgQWNjZXNzIHBhcmEgcmV0b3JuYXIgYXMgaW5mb3JtYcOnw7Vlc1xuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnRcbiAgICAgIC5wb3N0KGAke3RoaXMuYXV0aG9yaXphdGlvbkNvbmZpZy51bWFDb25maWcudG9rZW5FbmRwb2ludH1gLFxuICAgICAgICBuZXcgSHR0cFBhcmFtcygpXG4gICAgICAgIC5zZXQoJ2dyYW50X3R5cGUnLCAndXJuOmlldGY6cGFyYW1zOm9hdXRoOmdyYW50LXR5cGU6dW1hLXRpY2tldCcpXG4gICAgICAgIC5zZXQoJ2F1ZGllbmNlJywgYCR7dGhpcy5hdXRob3JpemF0aW9uQ29uZmlnLmNsaWVudElkfWApXG4gICAgICAgIC50b1N0cmluZygpLFxuICAgICAgICB7XG4gICAgICAgICAgaGVhZGVyczogbmV3IEh0dHBIZWFkZXJzKClcbiAgICAgICAgICAuc2V0KCdDb250ZW50LVR5cGUnLCAnYXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkJylcbiAgICAgICAgICAuc2V0KCdBdXRob3JpemF0aW9uJywgYEJlYXJlciAke3VzZXIuYWNjZXNzX3Rva2VufWApXG4gICAgICAgIH1cbiAgICAgIClcbiAgICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTptYXgtbGluZS1sZW5ndGhcbiAgICAgIC5waXBlKG1hcCgocmVzdWx0OiB7IHVwZ3JhZGVkOiBib29sZWFuOyBhY2Nlc3NfdG9rZW46IHN0cmluZzsgZXhwaXJlc19pbjogbnVtYmVyOyByZWZyZXNoX2V4cGlyZXNfaW46IG51bWJlcjsgcmVmcmVzaF90b2tlbjogc3RyaW5nOyB0b2tlbl90eXBlOiBzdHJpbmc7IH0pID0+IHtcbiAgICAgICAgLy8gRGV2byBlbnTDo28gYnVzY2FyIG8gcGF5bG9hZCBkbyBhY2Nlc3NfdG9rZW4gZSBpbmNsdWlyIChzdWJzdGl0dWluZG8gbyBhY2Nlc3NfdG9rZW4gYW50ZXJpb3IpIGFzIGluZm9ybWHDp8O1ZXMgZGUgYXV0b3JpemHDp8Ojb1xuICAgICAgICBjb25zdCBhY2Nlc3NUb2tlbiA9IHJlc3VsdC5hY2Nlc3NfdG9rZW4uc3BsaXQoJy4nKVsxXS5yZXBsYWNlKCctJywgJysnKS5yZXBsYWNlKCdfJywgJy8nKTtcbiAgICAgICAgY29uc3QgYWNjZXNzVG9rZW5QYXlsb2FkMSA9IEpTT04ucGFyc2UoYXRvYihhY2Nlc3NUb2tlbikpO1xuXG4gICAgICAgIGNvbnN0IHVzdWFyaW9GaW5hbDEgPSBPYmplY3QuYXNzaWduKHVzZXIsIGFjY2Vzc1Rva2VuUGF5bG9hZDEpIGFzIE9wZW5JRENvbm5lY3RVc2VyO1xuICAgICAgICB1c3VhcmlvRmluYWwxLmFjY2Vzc190b2tlbiA9IHJlc3VsdC5hY2Nlc3NfdG9rZW47XG4gICAgICAgICh1c3VhcmlvRmluYWwxIGFzIGFueSkuYWNjZXNzX3Rva2VuID0gcmVzdWx0LmFjY2Vzc190b2tlbjtcblxuICAgICAgICByZXR1cm4gdXN1YXJpb0ZpbmFsMSBhcyBhbnk7XG4gICAgICB9KSlcbiAgICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTptYXgtbGluZS1sZW5ndGhcbiAgICAgIC5waXBlKG1hcCgodXNlcjogT3BlbklEQ29ubmVjdFVzZXIgJiB7IGF1dGhvcml6YXRpb246IHsgcGVybWlzc2lvbnM6IEFycmF5PHsgc2NvcGVzOiBBcnJheTxzdHJpbmc+LCByc2lkOiBzdHJpbmcsIHJzbmFtZTogc3RyaW5nIH0+IH0gfSkgPT4ge1xuICAgICAgICAvLyBEZXZvIGFnb3JhIHJldG9ybmFyIGFzIGluZm9ybWHDp8O1ZXMgZG8gdXN1w6FyaW8gbm8gZm9ybWFkbyBlc3BlcmFkbyAoT3BlbklEQ29ubmVjdFVzZXIgJiBVc2VyV2l0aEF1dGhvcml6YXRpb25zKVxuICAgICAgICBsZXQgdXNlclRvUmV0dXJuID0gSlNPTi5wYXJzZShKU09OLnN0cmluZ2lmeSh1c2VyKSkgYXMgT3BlbklEQ29ubmVjdFVzZXIgJiBVc2VyV2l0aEF1dGhvcml6YXRpb25zICZcbiAgICAgICAgICB7IGF1dGhvcml6YXRpb246IHsgcGVybWlzc2lvbnM6IEFycmF5PHsgc2NvcGVzOiBBcnJheTxzdHJpbmc+LCByc2lkOiBzdHJpbmcsIHJzbmFtZTogc3RyaW5nIH0+IH0gfTtcblxuICAgICAgICBkZWxldGUgdXNlclRvUmV0dXJuWydhdXRob3JpemF0aW9uJ107XG5cbiAgICAgICAgdXNlclRvUmV0dXJuLmF1dGhvcml6YXRpb25zID0gdXNlci5hdXRob3JpemF0aW9uLnBlcm1pc3Npb25zLm1hcChwZXJtaXNzaW9uID0+IHtcbiAgICAgICAgICByZXR1cm4geyBhY3Rpb246IHBlcm1pc3Npb24uc2NvcGVzLCByZXNvdXJjZTogcGVybWlzc2lvbi5yc25hbWUsIHJlc291cmNlSWQ6IHBlcm1pc3Npb24ucnNpZCB9O1xuICAgICAgICB9KTtcblxuICAgICAgICByZXR1cm4gdXNlclRvUmV0dXJuO1xuICAgICAgfSkpXG4gICAgICAudG9Qcm9taXNlKCk7XG5cbiAgfVxufVxuIl19