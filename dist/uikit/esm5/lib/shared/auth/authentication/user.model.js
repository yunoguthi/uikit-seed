/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function User() { }
if (false) {
    /**
     * Identificador único do usuário naquele provedor de identidade
     * @type {?}
     */
    User.prototype.sub;
    /**
     * Endereço do serviço provedor de identidade
     * @type {?|undefined}
     */
    User.prototype.iss;
    /**
     * Informa se o usuário foi autenticado
     * @type {?}
     */
    User.prototype.authenticated;
    /**
     * Url da imagem do usuário
     * @type {?|undefined}
     */
    User.prototype.picture;
    /**
     * Nome do usuário
     * @type {?}
     */
    User.prototype.name;
    /**
     * Nome escolhido pelo usuario no provedor de identidade
     * @type {?|undefined}
     */
    User.prototype.preferred_username;
    /**
     * Nome de exibição do usuário
     * @type {?|undefined}
     */
    User.prototype.given_name;
    /**
     * Apelido do usuário
     * @type {?|undefined}
     */
    User.prototype.nickname;
    /**
     * Email principal do usuário
     * @type {?}
     */
    User.prototype.email;
    /**
     * CPF do usuário
     * @type {?|undefined}
     */
    User.prototype.cpf;
    /**
     * Perfis do usuário para o sistema
     * @type {?|undefined}
     */
    User.prototype.roles;
}
/**
 * @record
 */
export function Role() { }
if (false) {
    /**
     * Identificador do perfil do usuário no sistema
     * @type {?|undefined}
     */
    Role.prototype.id;
    /**
     * Nome do perfil do usuário no sistema
     * @type {?}
     */
    Role.prototype.name;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2F1dGgvYXV0aGVudGljYXRpb24vdXNlci5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUEsMEJBdUJDOzs7Ozs7SUFyQkMsbUJBQVk7Ozs7O0lBRVosbUJBQWE7Ozs7O0lBRWIsNkJBQXVCOzs7OztJQUV2Qix1QkFBaUI7Ozs7O0lBRWpCLG9CQUFhOzs7OztJQUViLGtDQUE0Qjs7Ozs7SUFFNUIsMEJBQW9COzs7OztJQUVwQix3QkFBa0I7Ozs7O0lBRWxCLHFCQUFjOzs7OztJQUVkLG1CQUFhOzs7OztJQUViLHFCQUFvQjs7Ozs7QUFHdEIsMEJBS0M7Ozs7OztJQUhDLGtCQUFZOzs7OztJQUVaLG9CQUFhIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGludGVyZmFjZSBVc2VyIHtcbiAgLyoqIElkZW50aWZpY2Fkb3Igw7puaWNvIGRvIHVzdcOhcmlvIG5hcXVlbGUgcHJvdmVkb3IgZGUgaWRlbnRpZGFkZSAqL1xuICBzdWI6IHN0cmluZztcbiAgLyoqIEVuZGVyZcOnbyBkbyBzZXJ2acOnbyBwcm92ZWRvciBkZSBpZGVudGlkYWRlICovXG4gIGlzcz86IHN0cmluZztcbiAgLyoqIEluZm9ybWEgc2UgbyB1c3XDoXJpbyBmb2kgYXV0ZW50aWNhZG8gKi9cbiAgYXV0aGVudGljYXRlZDogYm9vbGVhbjtcbiAgLyoqIFVybCBkYSBpbWFnZW0gZG8gdXN1w6FyaW8gKi9cbiAgcGljdHVyZT86IHN0cmluZztcbiAgLyoqIE5vbWUgZG8gdXN1w6FyaW8gKi9cbiAgbmFtZTogc3RyaW5nO1xuICAvKiogTm9tZSBlc2NvbGhpZG8gcGVsbyB1c3VhcmlvIG5vIHByb3ZlZG9yIGRlIGlkZW50aWRhZGUgKi9cbiAgcHJlZmVycmVkX3VzZXJuYW1lPzogc3RyaW5nO1xuICAvKiogTm9tZSBkZSBleGliacOnw6NvIGRvIHVzdcOhcmlvICovXG4gIGdpdmVuX25hbWU/OiBzdHJpbmc7XG4gIC8qKiBBcGVsaWRvIGRvIHVzdcOhcmlvICovXG4gIG5pY2tuYW1lPzogc3RyaW5nO1xuICAvKiogRW1haWwgcHJpbmNpcGFsIGRvIHVzdcOhcmlvICovXG4gIGVtYWlsOiBzdHJpbmc7XG4gIC8qKiBDUEYgZG8gdXN1w6FyaW8gKi9cbiAgY3BmPzogc3RyaW5nO1xuICAvKiogUGVyZmlzIGRvIHVzdcOhcmlvIHBhcmEgbyBzaXN0ZW1hICovXG4gIHJvbGVzPzogQXJyYXk8Um9sZT47XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgUm9sZSB7XG4gIC8qKiBJZGVudGlmaWNhZG9yIGRvIHBlcmZpbCBkbyB1c3XDoXJpbyBubyBzaXN0ZW1hICovXG4gIGlkPzogc3RyaW5nO1xuICAvKiogTm9tZSBkbyBwZXJmaWwgZG8gdXN1w6FyaW8gbm8gc2lzdGVtYSAqL1xuICBuYW1lOiBzdHJpbmc7XG59XG5cbmV4cG9ydCB0eXBlIE9BdXRoVXNlciA9IFVzZXIgJiB7XG4gIGFjY2Vzc190b2tlbjogc3RyaW5nO1xuICBjbGllbnRfaWQ6IHN0cmluZztcbn07XG5cbmV4cG9ydCB0eXBlIE9wZW5JRENvbm5lY3RVc2VyID0gT0F1dGhVc2VyICYge1xuICBpZF90b2tlbjogc3RyaW5nO1xufTtcblxuZXhwb3J0IHR5cGUgVXNlcldpdGhBdXRob3JpemF0aW9ucyA9IFVzZXIgJiB7XG4gIGF1dGhvcml6YXRpb25zPzogQXJyYXk8e1xuICAgIGFjdGlvbjogQXJyYXk8c3RyaW5nPjtcbiAgICByZXNvdXJjZTogc3RyaW5nO1xuICAgIGNsaWVudD86IHN0cmluZztcbiAgfT47XG59O1xuIl19