/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';
import { map } from 'rxjs/operators';
import { AuthorizationConfigToken } from '../../authorization/authorization-config.token';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
import * as i2 from "../../authorization/authorization-config.token";
/** @type {?} */
var groupBy = (/**
 * @template T
 * @param {?} items
 * @param {?} keyFunction
 * @return {?}
 */
function (items, keyFunction) {
    /** @type {?} */
    var groups = {};
    items.forEach((/**
     * @param {?} el
     * @return {?}
     */
    function (el) {
        /** @type {?} */
        var key = keyFunction(el);
        if (key in groups == false) {
            groups[key] = [];
        }
        groups[key].push(el);
    }));
    return Object.keys(groups).map((/**
     * @param {?} key
     * @return {?}
     */
    function (key) {
        return {
            key: key,
            values: (/** @type {?} */ (groups[key]))
        };
    }));
});
var ɵ0 = groupBy;
/**
 * @record
 */
function EntitledAttributesDTOs() { }
if (false) {
    /** @type {?} */
    EntitledAttributesDTOs.prototype.resourceName;
    /** @type {?} */
    EntitledAttributesDTOs.prototype.action;
    /** @type {?} */
    EntitledAttributesDTOs.prototype.environment;
    /** @type {?} */
    EntitledAttributesDTOs.prototype.allActions;
    /** @type {?} */
    EntitledAttributesDTOs.prototype.allResources;
    /** @type {?} */
    EntitledAttributesDTOs.prototype.attributeDTOs;
}
/**
 * @record
 */
function IResultEntitlementAllRequest() { }
if (false) {
    /** @type {?} */
    IResultEntitlementAllRequest.prototype.entitledResultSetDTO;
}
var Wso2AuthenticationManager = /** @class */ (function () {
    function Wso2AuthenticationManager(httpClient, authorizationConfig) {
        this.httpClient = httpClient;
        this.authorizationConfig = authorizationConfig;
    }
    /**
     * @param {?} user
     * @return {?}
     */
    Wso2AuthenticationManager.prototype.transform = /**
     * @param {?} user
     * @return {?}
     */
    function (user) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                // Faço request para buscar todas as permissões do usuário (resource owner) para um client específico
                // Neste caso uso o padrão User Managed Access para retornar as informações
                // https://localhost:9443/api/identity/entitlement/decision/entitlements-all
                return [2 /*return*/, this.httpClient
                        .post("" + this.authorizationConfig.xacmlConfig.entitlementsAllEndpoint, {
                        identifier: '',
                        givenAttributes: []
                    }, {
                        headers: new HttpHeaders()
                            .set('Content-Type', 'application/json')
                            .set('Accept', 'application/json')
                            .set('Authorization', "Bearer " + user.access_token)
                    })
                        .pipe(map((/**
                     * @param {?} result
                     * @return {?}
                     */
                    function (result) { return result.entitledResultSetDTO.entitledAttributesDTOs; })))
                        .pipe(map((/**
                     * @param {?} result
                     * @return {?}
                     */
                    function (result) {
                        /** @type {?} */
                        var authorizations = groupBy(result, (
                        // tslint:disable-next-line:max-line-length
                        // tslint:disable-next-line:max-line-length
                        /**
                         * @param {?} item
                         * @return {?}
                         */
                        function (item) { return item.attributeDTOs.find((/**
                         * @param {?} attr
                         * @return {?}
                         */
                        function (attr) { return attr.attributeId === 'urn:oasis:names:tc:xacml:1.0:resource:resource-id'; })).attributeValue; }));
                        /** @type {?} */
                        var permissions = authorizations.map((/**
                         * @param {?} perm
                         * @return {?}
                         */
                        function (perm) { return ({
                            resource: perm.key,
                            action: perm.values.map((/**
                             * @param {?} r
                             * @return {?}
                             */
                            function (r) { return r.attributeDTOs.find((/**
                             * @param {?} a
                             * @return {?}
                             */
                            function (a) { return a.attributeId === 'urn:oasis:names:tc:xacml:1.0:action:action-id'; })).attributeValue; }))
                        }); }));
                        /** @type {?} */
                        var returnUser = (/** @type {?} */ (tslib_1.__assign({}, user)));
                        returnUser.authorizations = permissions;
                        return returnUser;
                    })))
                        .toPromise()];
            });
        });
    };
    Wso2AuthenticationManager.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    Wso2AuthenticationManager.ctorParameters = function () { return [
        { type: HttpClient },
        { type: undefined, decorators: [{ type: Inject, args: [AuthorizationConfigToken,] }] }
    ]; };
    /** @nocollapse */ Wso2AuthenticationManager.ngInjectableDef = i0.defineInjectable({ factory: function Wso2AuthenticationManager_Factory() { return new Wso2AuthenticationManager(i0.inject(i1.HttpClient), i0.inject(i2.AuthorizationConfigToken)); }, token: Wso2AuthenticationManager, providedIn: "root" });
    return Wso2AuthenticationManager;
}());
export { Wso2AuthenticationManager };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    Wso2AuthenticationManager.prototype.httpClient;
    /**
     * @type {?}
     * @protected
     */
    Wso2AuthenticationManager.prototype.authorizationConfig;
}
export { ɵ0 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid3NvMi1wcm92aWRlci1hdXRoZW50aWNhdGlvbi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvYXV0aC9hdXRoZW50aWNhdGlvbi93c28yL3dzbzItcHJvdmlkZXItYXV0aGVudGljYXRpb24uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQWMsV0FBVyxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFFM0UsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDbkQsT0FBTyxFQUFFLEdBQUcsRUFBVyxNQUFNLGdCQUFnQixDQUFDO0FBRTlDLE9BQU8sRUFBdUIsd0JBQXdCLEVBQUUsTUFBTSxnREFBZ0QsQ0FBQzs7Ozs7SUFLekcsT0FBTzs7Ozs7O0FBQUcsVUFBWSxLQUFlLEVBQUUsV0FBOEI7O1FBQ25FLE1BQU0sR0FBRyxFQUFFO0lBQ2pCLEtBQUssQ0FBQyxPQUFPOzs7O0lBQUMsVUFBUyxFQUFFOztZQUNqQixHQUFHLEdBQUcsV0FBVyxDQUFDLEVBQUUsQ0FBQztRQUN6QixJQUFJLEdBQUcsSUFBSSxNQUFNLElBQUksS0FBSyxFQUFFO1lBQ3hCLE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLENBQUM7U0FDcEI7UUFDRCxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ3pCLENBQUMsRUFBQyxDQUFDO0lBQ0gsT0FBTyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEdBQUc7Ozs7SUFBQyxVQUFTLEdBQUc7UUFDdkMsT0FBTztZQUNILEdBQUcsRUFBRSxHQUFHO1lBQ1IsTUFBTSxFQUFFLG1CQUFBLE1BQU0sQ0FBQyxHQUFHLENBQUMsRUFBTztTQUM3QixDQUFDO0lBQ04sQ0FBQyxFQUFDLENBQUM7QUFDTCxDQUFDLENBQUE7Ozs7O0FBVUQscUNBWUc7OztJQVhELDhDQUFtQjs7SUFDakIsd0NBQWE7O0lBQ2IsNkNBQWtCOztJQUNsQiw0Q0FBa0I7O0lBQ2xCLDhDQUFvQjs7SUFDcEIsK0NBS0c7Ozs7O0FBR1AsMkNBSUM7OztJQUhDLDREQUVFOztBQUdKO0lBSUUsbUNBQ1ksVUFBc0IsRUFFdEIsbUJBQXdDO1FBRnhDLGVBQVUsR0FBVixVQUFVLENBQVk7UUFFdEIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtJQUNqRCxDQUFDOzs7OztJQUNTLDZDQUFTOzs7O0lBQXRCLFVBQ0UsSUFBdUI7OztnQkFHdkIscUdBQXFHO2dCQUNyRywyRUFBMkU7Z0JBRTNFLDRFQUE0RTtnQkFDNUUsc0JBQU8sSUFBSSxDQUFDLFVBQVU7eUJBQ25CLElBQUksQ0FBQyxLQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLENBQUMsdUJBQXlCLEVBQ3JFO3dCQUNFLFVBQVUsRUFBRSxFQUFFO3dCQUNkLGVBQWUsRUFBRSxFQUFFO3FCQUNwQixFQUNEO3dCQUNFLE9BQU8sRUFBRSxJQUFJLFdBQVcsRUFBRTs2QkFDekIsR0FBRyxDQUFDLGNBQWMsRUFBRSxrQkFBa0IsQ0FBQzs2QkFDdkMsR0FBRyxDQUFDLFFBQVEsRUFBRSxrQkFBa0IsQ0FBQzs2QkFDakMsR0FBRyxDQUFDLGVBQWUsRUFBRSxZQUFVLElBQUksQ0FBQyxZQUFjLENBQUM7cUJBQ3JELENBQ0Y7eUJBQ0EsSUFBSSxDQUFDLEdBQUc7Ozs7b0JBQUMsVUFBQyxNQUFvQyxJQUFLLE9BQUEsTUFBTSxDQUFDLG9CQUFvQixDQUFDLHNCQUFzQixFQUFsRCxDQUFrRCxFQUFDLENBQUM7eUJBQ3ZHLElBQUksQ0FBQyxHQUFHOzs7O29CQUFDLFVBQUMsTUFBTTs7NEJBQ1QsY0FBYyxHQUFHLE9BQU8sQ0FBQyxNQUFNO3dCQUNuQywyQ0FBMkM7Ozs7Ozt3QkFDM0MsVUFBQyxJQUFJLElBQUssT0FBQSxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUk7Ozs7d0JBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxJQUFJLENBQUMsV0FBVyxLQUFLLG1EQUFtRCxFQUF4RSxDQUF3RSxFQUFDLENBQUMsY0FBYyxFQUF4SCxDQUF3SCxFQUNuSTs7NEJBRUssV0FBVyxHQUFHLGNBQWMsQ0FBQyxHQUFHOzs7O3dCQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsQ0FBQzs0QkFDOUMsUUFBUSxFQUFFLElBQUksQ0FBQyxHQUFHOzRCQUNsQixNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHOzs7OzRCQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxJQUFJOzs7OzRCQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLFdBQVcsS0FBSywrQ0FBK0MsRUFBakUsQ0FBaUUsRUFBQyxDQUFDLGNBQWMsRUFBM0csQ0FBMkcsRUFBQzt5QkFDMUksQ0FBQyxFQUg2QyxDQUc3QyxFQUFDOzs0QkFHRyxVQUFVLEdBQUcsd0NBQUssSUFBSSxHQUFnRDt3QkFDNUUsVUFBVSxDQUFDLGNBQWMsR0FBRyxXQUFXLENBQUM7d0JBR3hDLE9BQU8sVUFBVSxDQUFDO29CQUVwQixDQUFDLEVBQUMsQ0FBQzt5QkFDRixTQUFTLEVBQUUsRUFBQzs7O0tBRWhCOztnQkFwREYsVUFBVSxTQUFDO29CQUNWLFVBQVUsRUFBRSxNQUFNO2lCQUNuQjs7OztnQkF6RFEsVUFBVTtnREE2RGQsTUFBTSxTQUFDLHdCQUF3Qjs7O29DQTdEcEM7Q0E0R0MsQUFyREQsSUFxREM7U0FsRFkseUJBQXlCOzs7Ozs7SUFFbEMsK0NBQWdDOzs7OztJQUNoQyx3REFDa0QiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBIdHRwQ2xpZW50LCBIdHRwUGFyYW1zLCBIdHRwSGVhZGVycyB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcblxuaW1wb3J0IHsgSW5qZWN0YWJsZSwgSW5qZWN0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBtYXAsIGZsYXRNYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQgeyBJQXV0aGVudGljYXRpb25NYW5hZ2VyIH0gZnJvbSAnLi4vYXV0aGVudGljYXRpb24tbWFuYWdlcic7XG5pbXBvcnQgeyBBdXRob3JpemF0aW9uQ29uZmlnLCBBdXRob3JpemF0aW9uQ29uZmlnVG9rZW4gfSBmcm9tICcuLi8uLi9hdXRob3JpemF0aW9uL2F1dGhvcml6YXRpb24tY29uZmlnLnRva2VuJztcbmltcG9ydCB7IE9wZW5JRENvbm5lY3RVc2VyLCBVc2VyV2l0aEF1dGhvcml6YXRpb25zIH0gZnJvbSAnLi4vdXNlci5tb2RlbCc7XG5cblxuXG5jb25zdCBncm91cEJ5ID0gZnVuY3Rpb248VD4oaXRlbXM6IEFycmF5PFQ+LCBrZXlGdW5jdGlvbjogKGl0ZW1zOiBUKSA9PiBhbnkpIHtcbiAgY29uc3QgZ3JvdXBzID0ge307XG4gIGl0ZW1zLmZvckVhY2goZnVuY3Rpb24oZWwpIHtcbiAgICAgIHZhciBrZXkgPSBrZXlGdW5jdGlvbihlbCk7XG4gICAgICBpZiAoa2V5IGluIGdyb3VwcyA9PSBmYWxzZSkge1xuICAgICAgICAgIGdyb3Vwc1trZXldID0gW107XG4gICAgICB9XG4gICAgICBncm91cHNba2V5XS5wdXNoKGVsKTtcbiAgfSk7XG4gIHJldHVybiBPYmplY3Qua2V5cyhncm91cHMpLm1hcChmdW5jdGlvbihrZXkpIHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgICAga2V5OiBrZXksXG4gICAgICAgICAgdmFsdWVzOiBncm91cHNba2V5XSBhcyBUW11cbiAgICAgIH07XG4gIH0pO1xufTtcblxuXG5cblxuXG5cblxuXG5cbmludGVyZmFjZSBFbnRpdGxlZEF0dHJpYnV0ZXNEVE9zIHtcbiAgcmVzb3VyY2VOYW1lOiBudWxsO1xuICAgIGFjdGlvbjogbnVsbDtcbiAgICBlbnZpcm9ubWVudDogbnVsbDtcbiAgICBhbGxBY3Rpb25zOiBmYWxzZTtcbiAgICBhbGxSZXNvdXJjZXM6IGZhbHNlO1xuICAgIGF0dHJpYnV0ZURUT3M6IEFycmF5PHtcbiAgICAgIGF0dHJpYnV0ZVZhbHVlOiBzdHJpbmcsXG4gICAgICBhdHRyaWJ1dGVEYXRhVHlwZTogJ2h0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hI3N0cmluZycsXG4gICAgICBhdHRyaWJ1dGVJZDogJ3VybjpvYXNpczpuYW1lczp0Yzp4YWNtbDoxLjA6cmVzb3VyY2U6cmVzb3VyY2UtaWQnIHwgJ3VybjpvYXNpczpuYW1lczp0Yzp4YWNtbDoxLjA6c3ViamVjdDpzdWJqZWN0LWlkJyB8ICd1cm46b2FzaXM6bmFtZXM6dGM6eGFjbWw6MS4wOmFjdGlvbjphY3Rpb24taWQnLFxuICAgICAgY2F0ZWdvcnk6ICd1cm46b2FzaXM6bmFtZXM6dGM6eGFjbWw6My4wOmF0dHJpYnV0ZS1jYXRlZ29yeTpyZXNvdXJjZScgfCAndXJuOm9hc2lzOm5hbWVzOnRjOnhhY21sOjEuMDpzdWJqZWN0LWNhdGVnb3J5OmFjY2Vzcy1zdWJqZWN0JyB8ICd1cm46b2FzaXM6bmFtZXM6dGM6eGFjbWw6My4wOmF0dHJpYnV0ZS1jYXRlZ29yeTphY3Rpb24nLFxuICAgIH0+O1xuICB9XG5cbmludGVyZmFjZSBJUmVzdWx0RW50aXRsZW1lbnRBbGxSZXF1ZXN0IHtcbiAgZW50aXRsZWRSZXN1bHRTZXREVE86IHtcbiAgICBlbnRpdGxlZEF0dHJpYnV0ZXNEVE9zOiBBcnJheTxFbnRpdGxlZEF0dHJpYnV0ZXNEVE9zPixcbiAgfTtcbn1cblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgV3NvMkF1dGhlbnRpY2F0aW9uTWFuYWdlciBpbXBsZW1lbnRzIElBdXRoZW50aWNhdGlvbk1hbmFnZXIge1xuICBjb25zdHJ1Y3RvcihcbiAgICBwcm90ZWN0ZWQgaHR0cENsaWVudDogSHR0cENsaWVudCxcbiAgICBASW5qZWN0KEF1dGhvcml6YXRpb25Db25maWdUb2tlbilcbiAgICBwcm90ZWN0ZWQgYXV0aG9yaXphdGlvbkNvbmZpZzogQXV0aG9yaXphdGlvbkNvbmZpZ1xuICApIHt9XG4gIHB1YmxpYyBhc3luYyB0cmFuc2Zvcm0oXG4gICAgdXNlcjogT3BlbklEQ29ubmVjdFVzZXJcbiAgKTogUHJvbWlzZTxPcGVuSURDb25uZWN0VXNlciAmIFVzZXJXaXRoQXV0aG9yaXphdGlvbnM+IHtcblxuICAgIC8vIEZhw6dvIHJlcXVlc3QgcGFyYSBidXNjYXIgdG9kYXMgYXMgcGVybWlzc8O1ZXMgZG8gdXN1w6FyaW8gKHJlc291cmNlIG93bmVyKSBwYXJhIHVtIGNsaWVudCBlc3BlY8OtZmljb1xuICAgIC8vIE5lc3RlIGNhc28gdXNvIG8gcGFkcsOjbyBVc2VyIE1hbmFnZWQgQWNjZXNzIHBhcmEgcmV0b3JuYXIgYXMgaW5mb3JtYcOnw7Vlc1xuXG4gICAgLy8gaHR0cHM6Ly9sb2NhbGhvc3Q6OTQ0My9hcGkvaWRlbnRpdHkvZW50aXRsZW1lbnQvZGVjaXNpb24vZW50aXRsZW1lbnRzLWFsbFxuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnRcbiAgICAgIC5wb3N0KGAke3RoaXMuYXV0aG9yaXphdGlvbkNvbmZpZy54YWNtbENvbmZpZy5lbnRpdGxlbWVudHNBbGxFbmRwb2ludH1gLFxuICAgICAgICB7XG4gICAgICAgICAgaWRlbnRpZmllcjogJycsXG4gICAgICAgICAgZ2l2ZW5BdHRyaWJ1dGVzOiBbXVxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgaGVhZGVyczogbmV3IEh0dHBIZWFkZXJzKClcbiAgICAgICAgICAuc2V0KCdDb250ZW50LVR5cGUnLCAnYXBwbGljYXRpb24vanNvbicpXG4gICAgICAgICAgLnNldCgnQWNjZXB0JywgJ2FwcGxpY2F0aW9uL2pzb24nKVxuICAgICAgICAgIC5zZXQoJ0F1dGhvcml6YXRpb24nLCBgQmVhcmVyICR7dXNlci5hY2Nlc3NfdG9rZW59YClcbiAgICAgICAgfVxuICAgICAgKVxuICAgICAgLnBpcGUobWFwKChyZXN1bHQ6IElSZXN1bHRFbnRpdGxlbWVudEFsbFJlcXVlc3QpID0+IHJlc3VsdC5lbnRpdGxlZFJlc3VsdFNldERUTy5lbnRpdGxlZEF0dHJpYnV0ZXNEVE9zKSlcbiAgICAgIC5waXBlKG1hcCgocmVzdWx0KSA9PiB7XG4gICAgICAgIGNvbnN0IGF1dGhvcml6YXRpb25zID0gZ3JvdXBCeShyZXN1bHQsXG4gICAgICAgICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOm1heC1saW5lLWxlbmd0aFxuICAgICAgICAgIChpdGVtKSA9PiBpdGVtLmF0dHJpYnV0ZURUT3MuZmluZChhdHRyID0+IGF0dHIuYXR0cmlidXRlSWQgPT09ICd1cm46b2FzaXM6bmFtZXM6dGM6eGFjbWw6MS4wOnJlc291cmNlOnJlc291cmNlLWlkJykuYXR0cmlidXRlVmFsdWVcbiAgICAgICAgKTtcblxuICAgICAgICBjb25zdCBwZXJtaXNzaW9ucyA9IGF1dGhvcml6YXRpb25zLm1hcChwZXJtID0+ICh7XG4gICAgICAgICAgcmVzb3VyY2U6IHBlcm0ua2V5LFxuICAgICAgICAgIGFjdGlvbjogcGVybS52YWx1ZXMubWFwKHIgPT4gci5hdHRyaWJ1dGVEVE9zLmZpbmQoYSA9PiBhLmF0dHJpYnV0ZUlkID09PSAndXJuOm9hc2lzOm5hbWVzOnRjOnhhY21sOjEuMDphY3Rpb246YWN0aW9uLWlkJykuYXR0cmlidXRlVmFsdWUpXG4gICAgICAgIH0pKTtcblxuXG4gICAgICAgIGNvbnN0IHJldHVyblVzZXIgPSB7IC4uLnVzZXIgfSBhcyBPcGVuSURDb25uZWN0VXNlciAmIFVzZXJXaXRoQXV0aG9yaXphdGlvbnM7XG4gICAgICAgIHJldHVyblVzZXIuYXV0aG9yaXphdGlvbnMgPSBwZXJtaXNzaW9ucztcblxuXG4gICAgICAgIHJldHVybiByZXR1cm5Vc2VyO1xuXG4gICAgICB9KSlcbiAgICAgIC50b1Byb21pc2UoKTtcblxuICB9XG59XG5cblxuIl19