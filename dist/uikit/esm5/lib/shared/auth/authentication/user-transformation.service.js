/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
/**
 * @template SourceUser, DestinationUser
 */
var ProviderUserTransformationService = /** @class */ (function () {
    function ProviderUserTransformationService() {
    }
    /**
     * @param {?} user
     * @return {?}
     */
    ProviderUserTransformationService.prototype.transform = /**
     * @param {?} user
     * @return {?}
     */
    function (user) {
        /** @type {?} */
        var destinationUser = (/** @type {?} */ ({}));
        /** @type {?} */
        var combinedUser = Object.assign(destinationUser, user);
        combinedUser.authenticated = true;
        return combinedUser;
    };
    ProviderUserTransformationService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    ProviderUserTransformationService.ctorParameters = function () { return []; };
    return ProviderUserTransformationService;
}());
export { ProviderUserTransformationService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci10cmFuc2Zvcm1hdGlvbi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvYXV0aC9hdXRoZW50aWNhdGlvbi91c2VyLXRyYW5zZm9ybWF0aW9uLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7Ozs7QUFHM0M7SUFHRTtJQUFnQixDQUFDOzs7OztJQUVWLHFEQUFTOzs7O0lBQWhCLFVBQWlCLElBQWdCOztZQUN6QixlQUFlLEdBQW9CLG1CQUFLLEVBQUUsRUFBQTs7WUFDMUMsWUFBWSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQztRQUN6RCxZQUFZLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztRQUNsQyxPQUFPLFlBQVksQ0FBQztJQUN0QixDQUFDOztnQkFWRixVQUFVOzs7O0lBV1gsd0NBQUM7Q0FBQSxBQVhELElBV0M7U0FWWSxpQ0FBaUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBVc2VyIH0gZnJvbSAnLi91c2VyLm1vZGVsJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIFByb3ZpZGVyVXNlclRyYW5zZm9ybWF0aW9uU2VydmljZTxTb3VyY2VVc2VyLCBEZXN0aW5hdGlvblVzZXIgZXh0ZW5kcyBVc2VyPiB7XG5cbiAgY29uc3RydWN0b3IoKSB7IH1cblxuICBwdWJsaWMgdHJhbnNmb3JtKHVzZXI6IFNvdXJjZVVzZXIpOiBEZXN0aW5hdGlvblVzZXIge1xuICAgIGNvbnN0IGRlc3RpbmF0aW9uVXNlcjogRGVzdGluYXRpb25Vc2VyID0gPGFueT57fTtcbiAgICBjb25zdCBjb21iaW5lZFVzZXIgPSBPYmplY3QuYXNzaWduKGRlc3RpbmF0aW9uVXNlciwgdXNlcik7XG4gICAgY29tYmluZWRVc2VyLmF1dGhlbnRpY2F0ZWQgPSB0cnVlO1xuICAgIHJldHVybiBjb21iaW5lZFVzZXI7XG4gIH1cbn1cblxuIl19