/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { AuthorizationConfigToken } from './../authorization-config.token';
import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { switchMap } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
import * as i2 from "../authorization-config.token";
var UmaAuthorizationManager = /** @class */ (function () {
    function UmaAuthorizationManager(httpClient, authorizationConfig) {
        this.httpClient = httpClient;
        this.authorizationConfig = authorizationConfig;
    }
    /**
     * @param {?} action
     * @param {?} resource
     * @return {?}
     */
    UmaAuthorizationManager.prototype.authorize = /**
     * @param {?} action
     * @param {?} resource
     * @return {?}
     */
    function (action, resource) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                // WSO2 - ${environment.settings.authentication.authority}api/identity/oauth2/uma/permission/v1.0/permission
                // KEYCLOAK - ${environment.settings.authentication.authority}authz/protection/permission
                return [2 /*return*/, this.httpClient.post("" + this.authorizationConfig.umaConfig.permissionEndpoint, [
                        {
                            resource_id: resource,
                            resource_scopes: [action]
                        }
                    ]).pipe(switchMap((/**
                     * @param {?} result
                     * @return {?}
                     */
                    function (result) {
                        // WSO2 - `${environment.settings.authentication.authority}oauth2/token`,
                        // KEYCLOAK - `${environment.settings.authentication.authority}protocol/openid-connect/token`,
                        return _this.httpClient.post("" + _this.authorizationConfig.umaConfig.tokenEndpoint, new HttpParams()
                            .set('grant_type', 'urn:ietf:params:oauth:grant-type:uma-ticket')
                            // .set('audience', client)
                            .set('response_mode', 'decision')
                            .set('ticket', result.ticket)
                            .toString(), {
                            headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
                        });
                    }))).toPromise().then((/**
                     * @param {?} result
                     * @return {?}
                     */
                    function (result) {
                        return result.result;
                    }))];
            });
        });
    };
    UmaAuthorizationManager.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    UmaAuthorizationManager.ctorParameters = function () { return [
        { type: HttpClient },
        { type: undefined, decorators: [{ type: Inject, args: [AuthorizationConfigToken,] }] }
    ]; };
    /** @nocollapse */ UmaAuthorizationManager.ngInjectableDef = i0.defineInjectable({ factory: function UmaAuthorizationManager_Factory() { return new UmaAuthorizationManager(i0.inject(i1.HttpClient), i0.inject(i2.AuthorizationConfigToken)); }, token: UmaAuthorizationManager, providedIn: "root" });
    return UmaAuthorizationManager;
}());
export { UmaAuthorizationManager };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    UmaAuthorizationManager.prototype.httpClient;
    /**
     * @type {?}
     * @protected
     */
    UmaAuthorizationManager.prototype.authorizationConfig;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidW1hLWF1dGhvcml6YXRpb24uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2F1dGgvYXV0aG9yaXphdGlvbi91bWEvdW1hLWF1dGhvcml6YXRpb24uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBdUIsd0JBQXdCLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQUNoRyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNuRCxPQUFPLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFBRSxVQUFVLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUMzRSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7Ozs7QUFLM0M7SUFJRSxpQ0FDWSxVQUFzQixFQUNZLG1CQUF3QztRQUQxRSxlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ1ksd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtJQUV0RixDQUFDOzs7Ozs7SUFDWSwyQ0FBUzs7Ozs7SUFBdEIsVUFBdUIsTUFBYyxFQUFFLFFBQWdCOzs7O2dCQUVyRCw0R0FBNEc7Z0JBQzVHLHlGQUF5RjtnQkFFekYsc0JBQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsU0FBUyxDQUFDLGtCQUFvQixFQUNwRjt3QkFDRTs0QkFDRSxXQUFXLEVBQUUsUUFBUTs0QkFDckIsZUFBZSxFQUFFLENBQUUsTUFBTSxDQUFFO3lCQUM1QjtxQkFDRixDQUNGLENBQUMsSUFBSSxDQUFDLFNBQVM7Ozs7b0JBQUMsVUFBQyxNQUEwQjt3QkFHMUMseUVBQXlFO3dCQUN6RSw4RkFBOEY7d0JBRTlGLE9BQU8sS0FBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBRyxLQUFJLENBQUMsbUJBQW1CLENBQUMsU0FBUyxDQUFDLGFBQWUsRUFDL0UsSUFBSSxVQUFVLEVBQUU7NkJBQ2YsR0FBRyxDQUFDLFlBQVksRUFBRSw2Q0FBNkMsQ0FBQzs0QkFDakUsMkJBQTJCOzZCQUMxQixHQUFHLENBQUMsZUFBZSxFQUFFLFVBQVUsQ0FBQzs2QkFDaEMsR0FBRyxDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDOzZCQUM1QixRQUFRLEVBQUUsRUFDWDs0QkFDRSxPQUFPLEVBQUUsSUFBSSxXQUFXLEVBQUUsQ0FBQyxHQUFHLENBQUMsY0FBYyxFQUFFLG1DQUFtQyxDQUFDO3lCQUNwRixDQUNGLENBQUM7b0JBRUosQ0FBQyxFQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxJQUFJOzs7O29CQUFDLFVBQUMsTUFBMkI7d0JBRS9DLE9BQU8sTUFBTSxDQUFDLE1BQU0sQ0FBQztvQkFFdkIsQ0FBQyxFQUFDLEVBQUM7OztLQUVKOztnQkE3Q0YsVUFBVSxTQUFDO29CQUNWLFVBQVUsRUFBRSxNQUFNO2lCQUNuQjs7OztnQkFSUSxVQUFVO2dEQVlkLE1BQU0sU0FBQyx3QkFBd0I7OztrQ0FkcEM7Q0FzREMsQUE5Q0QsSUE4Q0M7U0EzQ1ksdUJBQXVCOzs7Ozs7SUFFaEMsNkNBQWdDOzs7OztJQUNoQyxzREFBb0YiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBBdXRob3JpemF0aW9uQ29uZmlnLCBBdXRob3JpemF0aW9uQ29uZmlnVG9rZW4gfSBmcm9tICcuLy4uL2F1dGhvcml6YXRpb24tY29uZmlnLnRva2VuJztcbmltcG9ydCB7IEluamVjdGFibGUsIEluamVjdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSHR0cENsaWVudCwgSHR0cEhlYWRlcnMsIEh0dHBQYXJhbXMgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBzd2l0Y2hNYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gJy4uLy4uL2F1dGhlbnRpY2F0aW9uL3VzZXIuc2VydmljZSc7XG5pbXBvcnQgeyBJQXV0aG9yaXphdGlvbk1hbmFnZXIgfSBmcm9tICcuLi9hdXRob3JpemF0aW9uLnNlcnZpY2UnO1xuXG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIFVtYUF1dGhvcml6YXRpb25NYW5hZ2VyIGltcGxlbWVudHMgSUF1dGhvcml6YXRpb25NYW5hZ2VyIHtcbiAgY29uc3RydWN0b3IoXG4gICAgcHJvdGVjdGVkIGh0dHBDbGllbnQ6IEh0dHBDbGllbnQsXG4gICAgQEluamVjdChBdXRob3JpemF0aW9uQ29uZmlnVG9rZW4pIHByb3RlY3RlZCBhdXRob3JpemF0aW9uQ29uZmlnOiBBdXRob3JpemF0aW9uQ29uZmlnLFxuICApIHtcbiAgfVxuICBwdWJsaWMgYXN5bmMgYXV0aG9yaXplKGFjdGlvbjogc3RyaW5nLCByZXNvdXJjZTogc3RyaW5nKTogUHJvbWlzZTxib29sZWFuPiB7XG5cbiAgICAvLyBXU08yIC0gJHtlbnZpcm9ubWVudC5zZXR0aW5ncy5hdXRoZW50aWNhdGlvbi5hdXRob3JpdHl9YXBpL2lkZW50aXR5L29hdXRoMi91bWEvcGVybWlzc2lvbi92MS4wL3Blcm1pc3Npb25cbiAgICAvLyBLRVlDTE9BSyAtICR7ZW52aXJvbm1lbnQuc2V0dGluZ3MuYXV0aGVudGljYXRpb24uYXV0aG9yaXR5fWF1dGh6L3Byb3RlY3Rpb24vcGVybWlzc2lvblxuXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5wb3N0KGAke3RoaXMuYXV0aG9yaXphdGlvbkNvbmZpZy51bWFDb25maWcucGVybWlzc2lvbkVuZHBvaW50fWAsXG4gICAgICBbXG4gICAgICAgIHtcbiAgICAgICAgICByZXNvdXJjZV9pZDogcmVzb3VyY2UsXG4gICAgICAgICAgcmVzb3VyY2Vfc2NvcGVzOiBbIGFjdGlvbiBdXG4gICAgICAgIH1cbiAgICAgIF1cbiAgICApLnBpcGUoc3dpdGNoTWFwKChyZXN1bHQ6IHsgdGlja2V0OiBzdHJpbmcgfSkgPT4ge1xuXG5cbiAgICAgIC8vIFdTTzIgLSBgJHtlbnZpcm9ubWVudC5zZXR0aW5ncy5hdXRoZW50aWNhdGlvbi5hdXRob3JpdHl9b2F1dGgyL3Rva2VuYCxcbiAgICAgIC8vIEtFWUNMT0FLIC0gYCR7ZW52aXJvbm1lbnQuc2V0dGluZ3MuYXV0aGVudGljYXRpb24uYXV0aG9yaXR5fXByb3RvY29sL29wZW5pZC1jb25uZWN0L3Rva2VuYCxcblxuICAgICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5wb3N0KGAke3RoaXMuYXV0aG9yaXphdGlvbkNvbmZpZy51bWFDb25maWcudG9rZW5FbmRwb2ludH1gLFxuICAgICAgICBuZXcgSHR0cFBhcmFtcygpXG4gICAgICAgIC5zZXQoJ2dyYW50X3R5cGUnLCAndXJuOmlldGY6cGFyYW1zOm9hdXRoOmdyYW50LXR5cGU6dW1hLXRpY2tldCcpXG4gICAgICAgIC8vIC5zZXQoJ2F1ZGllbmNlJywgY2xpZW50KVxuICAgICAgICAuc2V0KCdyZXNwb25zZV9tb2RlJywgJ2RlY2lzaW9uJylcbiAgICAgICAgLnNldCgndGlja2V0JywgcmVzdWx0LnRpY2tldClcbiAgICAgICAgLnRvU3RyaW5nKCksXG4gICAgICAgIHtcbiAgICAgICAgICBoZWFkZXJzOiBuZXcgSHR0cEhlYWRlcnMoKS5zZXQoJ0NvbnRlbnQtVHlwZScsICdhcHBsaWNhdGlvbi94LXd3dy1mb3JtLXVybGVuY29kZWQnKVxuICAgICAgICB9XG4gICAgICApO1xuXG4gICAgfSkpLnRvUHJvbWlzZSgpLnRoZW4oKHJlc3VsdDogeyByZXN1bHQ6IGJvb2xlYW4gfSkgPT4ge1xuXG4gICAgICByZXR1cm4gcmVzdWx0LnJlc3VsdDtcblxuICAgIH0pO1xuXG4gIH1cbn1cbiJdfQ==