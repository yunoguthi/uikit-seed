/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @abstract
 */
var /**
 * @abstract
 */
GuardService = /** @class */ (function () {
    function GuardService(router, angularLocation, platformLocation) {
        this.router = router;
        this.angularLocation = angularLocation;
        this.platformLocation = platformLocation;
    }
    Object.defineProperty(GuardService.prototype, "routerStateSnapshot", {
        get: /**
         * @protected
         * @return {?}
         */
        function () {
            return this.router.routerState.snapshot;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GuardService.prototype, "activatedRouteSnapshot", {
        get: /**
         * @protected
         * @return {?}
         */
        function () {
            return this.router.routerState.root.snapshot;
        },
        enumerable: true,
        configurable: true
    });
    // tslint:disable-next-line:max-line-length
    // tslint:disable-next-line:max-line-length
    /**
     * @param {?} childRoute
     * @param {?} state
     * @return {?}
     */
    GuardService.prototype.canActivateChild = 
    // tslint:disable-next-line:max-line-length
    /**
     * @param {?} childRoute
     * @param {?} state
     * @return {?}
     */
    function (childRoute, state) {
        return this.canActivate(childRoute, state);
    };
    /**
     * @return {?}
     */
    GuardService.prototype.allow = /**
     * @return {?}
     */
    function () {
        return true;
    };
    /**
     * @param {?} state
     * @param {?=} login
     * @param {?=} setCallback
     * @return {?}
     */
    GuardService.prototype.deny = /**
     * @param {?} state
     * @param {?=} login
     * @param {?=} setCallback
     * @return {?}
     */
    function (state, login, setCallback) {
        if (login === void 0) { login = 'manual'; }
        if (setCallback === void 0) { setCallback = true; }
        if (setCallback) {
            /** @type {?} */
            var baseHref = this.platformLocation.getBaseHrefFromDOM();
            /** @type {?} */
            var origin_1 = window.location.origin;
            /** @type {?} */
            var completeUrlToBaseHref = origin_1 + baseHref;
            /** @type {?} */
            var angularRouteWithBaseHref = this.angularLocation.prepareExternalUrl(state.url);
            /** @type {?} */
            var angularRouteWithoutBaseHref = angularRouteWithBaseHref;
            if (angularRouteWithBaseHref.startsWith(baseHref)) {
                angularRouteWithoutBaseHref = angularRouteWithBaseHref.replace(baseHref, '');
            }
            /** @type {?} */
            var completeRoute = completeUrlToBaseHref + angularRouteWithoutBaseHref;
            console.log('Setando after callback como: ', completeRoute);
            localStorage.setItem('authentication-callback', completeRoute);
        }
        else {
            localStorage.removeItem('authentication-callback');
        }
        // const uri = this.angularLocation.prepareExternalUrl(state.url);
        // localStorage.setItem(location.host + ':callback', uri);
        /** @type {?} */
        var parametros = { queryParams: { login: login } };
        if (login == 'manual') {
            parametros = undefined;
        }
        this.router.navigate(['unauthorized'], parametros);
        return false;
    };
    return GuardService;
}());
/**
 * @abstract
 */
export { GuardService };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    GuardService.prototype.router;
    /**
     * @type {?}
     * @protected
     */
    GuardService.prototype.angularLocation;
    /**
     * @type {?}
     * @protected
     */
    GuardService.prototype.platformLocation;
    /**
     * @abstract
     * @param {?} route
     * @param {?} state
     * @return {?}
     */
    GuardService.prototype.canActivate = function (route, state) { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ3VhcmQuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2F1dGgvYXV0aG9yaXphdGlvbi9ndWFyZC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFJQTs7OztJQVdFLHNCQUNZLE1BQWMsRUFDZCxlQUF5QixFQUN6QixnQkFBa0M7UUFGbEMsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLG9CQUFlLEdBQWYsZUFBZSxDQUFVO1FBQ3pCLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7SUFFOUMsQ0FBQztJQWJELHNCQUFjLDZDQUFtQjs7Ozs7UUFBakM7WUFDRSxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQztRQUMxQyxDQUFDOzs7T0FBQTtJQUVELHNCQUFjLGdEQUFzQjs7Ozs7UUFBcEM7WUFDRSxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDL0MsQ0FBQzs7O09BQUE7SUFTRCwyQ0FBMkM7Ozs7Ozs7SUFDM0MsdUNBQWdCOzs7Ozs7O0lBQWhCLFVBQWlCLFVBQWtDLEVBQUUsS0FBMEI7UUFDN0UsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUM3QyxDQUFDOzs7O0lBSUQsNEJBQUs7OztJQUFMO1FBQ0UsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDOzs7Ozs7O0lBRUQsMkJBQUk7Ozs7OztJQUFKLFVBQUssS0FBMEIsRUFBRSxLQUFtQyxFQUFFLFdBQWtCO1FBQXZELHNCQUFBLEVBQUEsZ0JBQW1DO1FBQUUsNEJBQUEsRUFBQSxrQkFBa0I7UUFFdEYsSUFBSSxXQUFXLEVBQUU7O2dCQUVULFFBQVEsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsa0JBQWtCLEVBQUU7O2dCQUNyRCxRQUFNLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxNQUFNOztnQkFFL0IscUJBQXFCLEdBQUcsUUFBTSxHQUFHLFFBQVE7O2dCQUd6Qyx3QkFBd0IsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLGtCQUFrQixDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7O2dCQUUvRSwyQkFBMkIsR0FBRyx3QkFBd0I7WUFDMUQsSUFBSSx3QkFBd0IsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ2pELDJCQUEyQixHQUFHLHdCQUF3QixDQUFDLE9BQU8sQ0FBQyxRQUFRLEVBQUUsRUFBRSxDQUFDLENBQUM7YUFDOUU7O2dCQUVLLGFBQWEsR0FBRyxxQkFBcUIsR0FBRywyQkFBMkI7WUFFekUsT0FBTyxDQUFDLEdBQUcsQ0FBQywrQkFBK0IsRUFBRSxhQUFhLENBQUMsQ0FBQztZQUM1RCxZQUFZLENBQUMsT0FBTyxDQUFDLHlCQUF5QixFQUFFLGFBQWEsQ0FBQyxDQUFDO1NBQ2hFO2FBQUk7WUFDSCxZQUFZLENBQUMsVUFBVSxDQUFDLHlCQUF5QixDQUFDLENBQUM7U0FDcEQ7Ozs7WUFJRyxVQUFVLEdBQUcsRUFBRSxXQUFXLEVBQUUsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEVBQUU7UUFDbEQsSUFBSSxLQUFLLElBQUksUUFBUSxFQUFFO1lBQ3JCLFVBQVUsR0FBRyxTQUFTLENBQUM7U0FDeEI7UUFFRCxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxFQUFFLFVBQVUsQ0FBRSxDQUFDO1FBQ3BELE9BQU8sS0FBSyxDQUFDO0lBQ2YsQ0FBQztJQUVILG1CQUFDO0FBQUQsQ0FBQyxBQWpFRCxJQWlFQzs7Ozs7Ozs7OztJQXJERyw4QkFBd0I7Ozs7O0lBQ3hCLHVDQUFtQzs7Ozs7SUFDbkMsd0NBQTRDOzs7Ozs7O0lBUzlDLGlFQUFrSSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENhbkFjdGl2YXRlLCBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LCBSb3V0ZXJTdGF0ZVNuYXBzaG90LCBSb3V0ZXIsIENhbkFjdGl2YXRlQ2hpbGQsIFVybFRyZWUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHsgTG9jYXRpb24sIFBsYXRmb3JtTG9jYXRpb24gfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xuXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgR3VhcmRTZXJ2aWNlIGltcGxlbWVudHMgQ2FuQWN0aXZhdGUsIENhbkFjdGl2YXRlQ2hpbGQge1xuXG5cbiAgcHJvdGVjdGVkIGdldCByb3V0ZXJTdGF0ZVNuYXBzaG90KCkge1xuICAgIHJldHVybiB0aGlzLnJvdXRlci5yb3V0ZXJTdGF0ZS5zbmFwc2hvdDtcbiAgfVxuXG4gIHByb3RlY3RlZCBnZXQgYWN0aXZhdGVkUm91dGVTbmFwc2hvdCgpIHtcbiAgICByZXR1cm4gdGhpcy5yb3V0ZXIucm91dGVyU3RhdGUucm9vdC5zbmFwc2hvdDtcbiAgfVxuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByb3RlY3RlZCByb3V0ZXI6IFJvdXRlcixcbiAgICBwcm90ZWN0ZWQgYW5ndWxhckxvY2F0aW9uOiBMb2NhdGlvbixcbiAgICBwcm90ZWN0ZWQgcGxhdGZvcm1Mb2NhdGlvbjogUGxhdGZvcm1Mb2NhdGlvbixcbiAgKSB7XG4gIH1cblxuICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6bWF4LWxpbmUtbGVuZ3RoXG4gIGNhbkFjdGl2YXRlQ2hpbGQoY2hpbGRSb3V0ZTogQWN0aXZhdGVkUm91dGVTbmFwc2hvdCwgc3RhdGU6IFJvdXRlclN0YXRlU25hcHNob3QpOiBib29sZWFuIHwgVXJsVHJlZSB8IE9ic2VydmFibGU8Ym9vbGVhbiB8IFVybFRyZWU+IHwgUHJvbWlzZTxib29sZWFuIHwgVXJsVHJlZT4ge1xuICAgIHJldHVybiB0aGlzLmNhbkFjdGl2YXRlKGNoaWxkUm91dGUsIHN0YXRlKTtcbiAgfVxuXG4gIGFic3RyYWN0IGNhbkFjdGl2YXRlKHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LCBzdGF0ZTogUm91dGVyU3RhdGVTbmFwc2hvdCk6IGJvb2xlYW4gfCBQcm9taXNlPGJvb2xlYW4+IHwgT2JzZXJ2YWJsZTxib29sZWFuPjtcblxuICBhbGxvdygpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdHJ1ZTtcbiAgfVxuXG4gIGRlbnkoc3RhdGU6IFJvdXRlclN0YXRlU25hcHNob3QsIGxvZ2luOiAnYXV0bycgfCAnbWFudWFsJyA9ICdtYW51YWwnLCBzZXRDYWxsYmFjayA9IHRydWUpOiBib29sZWFuIHtcblxuICAgIGlmIChzZXRDYWxsYmFjaykge1xuXG4gICAgICBjb25zdCBiYXNlSHJlZiA9IHRoaXMucGxhdGZvcm1Mb2NhdGlvbi5nZXRCYXNlSHJlZkZyb21ET00oKTtcbiAgICAgIGNvbnN0IG9yaWdpbiA9IHdpbmRvdy5sb2NhdGlvbi5vcmlnaW47XG5cbiAgICAgIGNvbnN0IGNvbXBsZXRlVXJsVG9CYXNlSHJlZiA9IG9yaWdpbiArIGJhc2VIcmVmO1xuXG5cbiAgICAgIGNvbnN0IGFuZ3VsYXJSb3V0ZVdpdGhCYXNlSHJlZiA9IHRoaXMuYW5ndWxhckxvY2F0aW9uLnByZXBhcmVFeHRlcm5hbFVybChzdGF0ZS51cmwpO1xuXG4gICAgICBsZXQgYW5ndWxhclJvdXRlV2l0aG91dEJhc2VIcmVmID0gYW5ndWxhclJvdXRlV2l0aEJhc2VIcmVmO1xuICAgICAgaWYgKGFuZ3VsYXJSb3V0ZVdpdGhCYXNlSHJlZi5zdGFydHNXaXRoKGJhc2VIcmVmKSkge1xuICAgICAgICBhbmd1bGFyUm91dGVXaXRob3V0QmFzZUhyZWYgPSBhbmd1bGFyUm91dGVXaXRoQmFzZUhyZWYucmVwbGFjZShiYXNlSHJlZiwgJycpO1xuICAgICAgfVxuXG4gICAgICBjb25zdCBjb21wbGV0ZVJvdXRlID0gY29tcGxldGVVcmxUb0Jhc2VIcmVmICsgYW5ndWxhclJvdXRlV2l0aG91dEJhc2VIcmVmO1xuXG4gICAgICBjb25zb2xlLmxvZygnU2V0YW5kbyBhZnRlciBjYWxsYmFjayBjb21vOiAnLCBjb21wbGV0ZVJvdXRlKTtcbiAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdhdXRoZW50aWNhdGlvbi1jYWxsYmFjaycsIGNvbXBsZXRlUm91dGUpO1xuICAgIH1lbHNle1xuICAgICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oJ2F1dGhlbnRpY2F0aW9uLWNhbGxiYWNrJyk7XG4gICAgfVxuXG4gICAgLy8gY29uc3QgdXJpID0gdGhpcy5hbmd1bGFyTG9jYXRpb24ucHJlcGFyZUV4dGVybmFsVXJsKHN0YXRlLnVybCk7XG4gICAgLy8gbG9jYWxTdG9yYWdlLnNldEl0ZW0obG9jYXRpb24uaG9zdCArICc6Y2FsbGJhY2snLCB1cmkpO1xuICAgIGxldCBwYXJhbWV0cm9zID0geyBxdWVyeVBhcmFtczogeyBsb2dpbjogbG9naW4gfSB9O1xuICAgIGlmIChsb2dpbiA9PSAnbWFudWFsJykge1xuICAgICAgcGFyYW1ldHJvcyA9IHVuZGVmaW5lZDtcbiAgICB9XG5cbiAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJ3VuYXV0aG9yaXplZCddLCBwYXJhbWV0cm9zICk7XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG5cbn1cbiJdfQ==