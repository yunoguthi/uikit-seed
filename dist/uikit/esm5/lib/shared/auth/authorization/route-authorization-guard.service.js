/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { AuthorizationGuardService } from './authorization-guard.service';
var RouteAuthorizationGuardService = /** @class */ (function (_super) {
    tslib_1.__extends(RouteAuthorizationGuardService, _super);
    function RouteAuthorizationGuardService() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * @param {?} route
     * @param {?} state
     * @return {?}
     */
    RouteAuthorizationGuardService.prototype.canActivate = /**
     * @param {?} route
     * @param {?} state
     * @return {?}
     */
    function (route, state) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var permissionToCheck, hasPermission;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        permissionToCheck = route.data && (/** @type {?} */ (route.data.authorize));
                        if (!permissionToCheck) return [3 /*break*/, 2];
                        console.log("Verificando autoriza\u00E7\u00E3o para a rota '" + state.url + "'...");
                        return [4 /*yield*/, this.authorizationService.authorize(permissionToCheck.action, permissionToCheck.resource)];
                    case 1:
                        hasPermission = _a.sent();
                        console.log("Autoriza\u00E7\u00E3o para a rota '" + state.url + "' foi '" + (hasPermission ? 'concedida' : 'negada') + "'!");
                        if (!hasPermission) {
                            return [2 /*return*/, this.deny(state)];
                        }
                        else {
                            return [2 /*return*/, this.allow()];
                        }
                        return [3 /*break*/, 3];
                    case 2: throw new Error('Sem permissão para verificar!');
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    RouteAuthorizationGuardService.decorators = [
        { type: Injectable }
    ];
    return RouteAuthorizationGuardService;
}(AuthorizationGuardService));
export { RouteAuthorizationGuardService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicm91dGUtYXV0aG9yaXphdGlvbi1ndWFyZC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvYXV0aC9hdXRob3JpemF0aW9uL3JvdXRlLWF1dGhvcml6YXRpb24tZ3VhcmQuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFHM0MsT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0sK0JBQStCLENBQUM7QUFHMUU7SUFDb0QsMERBQXlCO0lBRDdFOztJQW9CQSxDQUFDOzs7Ozs7SUFsQk8sb0RBQVc7Ozs7O0lBQWpCLFVBQWtCLEtBQTZCLEVBQUUsS0FBMEI7Ozs7Ozt3QkFFbkUsaUJBQWlCLEdBQUcsS0FBSyxDQUFDLElBQUksSUFBSSxtQkFBQSxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBd0M7NkJBRWhHLGlCQUFpQixFQUFqQix3QkFBaUI7d0JBQ25CLE9BQU8sQ0FBQyxHQUFHLENBQUMsb0RBQXdDLEtBQUssQ0FBQyxHQUFHLFNBQU0sQ0FBQyxDQUFDO3dCQUMvQyxxQkFBTSxJQUFJLENBQUMsb0JBQW9CLENBQUMsU0FBUyxDQUFDLGlCQUFpQixDQUFDLE1BQU0sRUFBRSxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsRUFBQTs7d0JBQS9HLGFBQWEsR0FBRyxTQUErRjt3QkFDckgsT0FBTyxDQUFDLEdBQUcsQ0FBQyx3Q0FBNEIsS0FBSyxDQUFDLEdBQUcsZUFBVSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsT0FBSSxDQUFDLENBQUM7d0JBQ3pHLElBQUksQ0FBQyxhQUFhLEVBQUU7NEJBQ2xCLHNCQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUM7eUJBQ3pCOzZCQUFNOzRCQUNMLHNCQUFPLElBQUksQ0FBQyxLQUFLLEVBQUUsRUFBQzt5QkFDckI7OzRCQUVELE1BQU0sSUFBSSxLQUFLLENBQUMsK0JBQStCLENBQUMsQ0FBQzs7Ozs7S0FHcEQ7O2dCQW5CRixVQUFVOztJQW9CWCxxQ0FBQztDQUFBLEFBcEJELENBQ29ELHlCQUF5QixHQW1CNUU7U0FuQlksOEJBQThCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGVTbmFwc2hvdCwgUm91dGVyU3RhdGVTbmFwc2hvdCwgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IEF1dGhvcml6YXRpb25HdWFyZFNlcnZpY2UgfSBmcm9tICcuL2F1dGhvcml6YXRpb24tZ3VhcmQuc2VydmljZSc7XG5cblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIFJvdXRlQXV0aG9yaXphdGlvbkd1YXJkU2VydmljZSBleHRlbmRzIEF1dGhvcml6YXRpb25HdWFyZFNlcnZpY2Uge1xuICBhc3luYyBjYW5BY3RpdmF0ZShyb3V0ZTogQWN0aXZhdGVkUm91dGVTbmFwc2hvdCwgc3RhdGU6IFJvdXRlclN0YXRlU25hcHNob3QpOiBQcm9taXNlPGJvb2xlYW4+IHtcblxuICAgIGNvbnN0IHBlcm1pc3Npb25Ub0NoZWNrID0gcm91dGUuZGF0YSAmJiByb3V0ZS5kYXRhLmF1dGhvcml6ZSBhcyB7IGFjdGlvbjogc3RyaW5nLCByZXNvdXJjZTogc3RyaW5nIH07XG5cbiAgICBpZiAocGVybWlzc2lvblRvQ2hlY2spIHtcbiAgICAgIGNvbnNvbGUubG9nKGBWZXJpZmljYW5kbyBhdXRvcml6YcOnw6NvIHBhcmEgYSByb3RhICcke3N0YXRlLnVybH0nLi4uYCk7XG4gICAgICBjb25zdCBoYXNQZXJtaXNzaW9uID0gYXdhaXQgdGhpcy5hdXRob3JpemF0aW9uU2VydmljZS5hdXRob3JpemUocGVybWlzc2lvblRvQ2hlY2suYWN0aW9uLCBwZXJtaXNzaW9uVG9DaGVjay5yZXNvdXJjZSk7XG4gICAgICBjb25zb2xlLmxvZyhgQXV0b3JpemHDp8OjbyBwYXJhIGEgcm90YSAnJHtzdGF0ZS51cmx9JyBmb2kgJyR7KGhhc1Blcm1pc3Npb24gPyAnY29uY2VkaWRhJyA6ICduZWdhZGEnKX0nIWApO1xuICAgICAgaWYgKCFoYXNQZXJtaXNzaW9uKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmRlbnkoc3RhdGUpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuYWxsb3coKTtcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKCdTZW0gcGVybWlzc8OjbyBwYXJhIHZlcmlmaWNhciEnKTtcbiAgICAgfVxuXG4gIH1cbn1cbiJdfQ==