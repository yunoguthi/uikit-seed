/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { UserService } from '../../authentication/user.service';
import * as i0 from "@angular/core";
import * as i1 from "../../authentication/user.service";
var UserAuthorizationManager = /** @class */ (function () {
    function UserAuthorizationManager(userService) {
        this.userService = userService;
    }
    /**
     * @param {?} action
     * @param {?} resource
     * @return {?}
     */
    UserAuthorizationManager.prototype.authorize = /**
     * @param {?} action
     * @param {?} resource
     * @return {?}
     */
    function (action, resource) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var userWithAuthorizations, usuarioPossuiPermissao;
            return tslib_1.__generator(this, function (_a) {
                userWithAuthorizations = (/** @type {?} */ (this.userService.userValue));
                if (userWithAuthorizations.authenticated) {
                    if (userWithAuthorizations.authorizations != null && userWithAuthorizations.authorizations.length > 0) {
                        usuarioPossuiPermissao = userWithAuthorizations.authorizations.some((/**
                         * @param {?} permissao
                         * @return {?}
                         */
                        function (permissao) {
                            /** @type {?} */
                            var ehMesmoRecurso = permissao.resource === resource;
                            /** @type {?} */
                            var ehMesmaAcao = permissao.action.includes(action);
                            /** @type {?} */
                            var possuiPermissao = ehMesmoRecurso && ehMesmaAcao;
                            return possuiPermissao;
                        }));
                        if (usuarioPossuiPermissao) {
                            return [2 /*return*/, true];
                        }
                    }
                }
                return [2 /*return*/, false];
            });
        });
    };
    UserAuthorizationManager.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    UserAuthorizationManager.ctorParameters = function () { return [
        { type: UserService }
    ]; };
    /** @nocollapse */ UserAuthorizationManager.ngInjectableDef = i0.defineInjectable({ factory: function UserAuthorizationManager_Factory() { return new UserAuthorizationManager(i0.inject(i1.UserService)); }, token: UserAuthorizationManager, providedIn: "root" });
    return UserAuthorizationManager;
}());
export { UserAuthorizationManager };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    UserAuthorizationManager.prototype.userService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci1hdXRob3JpemF0aW9uLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9hdXRoL2F1dGhvcml6YXRpb24vdXNlci91c2VyLWF1dGhvcml6YXRpb24uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUNBLE9BQU8sRUFBRSxVQUFVLEVBQVUsTUFBTSxlQUFlLENBQUM7QUFDbkQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLG1DQUFtQyxDQUFDOzs7QUFHaEU7SUFJRSxrQ0FBc0IsV0FBd0I7UUFBeEIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7SUFDOUMsQ0FBQzs7Ozs7O0lBQ1ksNENBQVM7Ozs7O0lBQXRCLFVBQXVCLE1BQWMsRUFBRSxRQUFnQjs7OztnQkFFL0Msc0JBQXNCLEdBQUcsbUJBQUEsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLEVBQTBCO2dCQUVuRixJQUFJLHNCQUFzQixDQUFDLGFBQWEsRUFBRTtvQkFFeEMsSUFBSSxzQkFBc0IsQ0FBQyxjQUFjLElBQUksSUFBSSxJQUFJLHNCQUFzQixDQUFDLGNBQWMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO3dCQUUvRixzQkFBc0IsR0FBRyxzQkFBc0IsQ0FBQyxjQUFjLENBQUMsSUFBSTs7Ozt3QkFBQyxVQUFBLFNBQVM7O2dDQUMzRSxjQUFjLEdBQUcsU0FBUyxDQUFDLFFBQVEsS0FBSyxRQUFROztnQ0FDaEQsV0FBVyxHQUFHLFNBQVMsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQzs7Z0NBRS9DLGVBQWUsR0FBRyxjQUFjLElBQUksV0FBVzs0QkFDckQsT0FBTyxlQUFlLENBQUM7d0JBQ3pCLENBQUMsRUFBQzt3QkFFRixJQUFJLHNCQUFzQixFQUFFOzRCQUMxQixzQkFBTyxJQUFJLEVBQUM7eUJBQ2I7cUJBRUY7aUJBQ0Y7Z0JBRUQsc0JBQU8sS0FBSyxFQUFDOzs7S0FDZDs7Z0JBOUJGLFVBQVUsU0FBQztvQkFDVixVQUFVLEVBQUUsTUFBTTtpQkFDbkI7Ozs7Z0JBTFEsV0FBVzs7O21DQUZwQjtDQW9DQyxBQS9CRCxJQStCQztTQTVCWSx3QkFBd0I7Ozs7OztJQUN2QiwrQ0FBa0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBVc2VyV2l0aEF1dGhvcml6YXRpb25zIH0gZnJvbSAnLi8uLi8uLi9hdXRoZW50aWNhdGlvbi91c2VyLm1vZGVsJztcbmltcG9ydCB7IEluamVjdGFibGUsIEluamVjdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tICcuLi8uLi9hdXRoZW50aWNhdGlvbi91c2VyLnNlcnZpY2UnO1xuaW1wb3J0IHsgSUF1dGhvcml6YXRpb25NYW5hZ2VyIH0gZnJvbSAnLi4vYXV0aG9yaXphdGlvbi5zZXJ2aWNlJztcblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgVXNlckF1dGhvcml6YXRpb25NYW5hZ2VyIGltcGxlbWVudHMgSUF1dGhvcml6YXRpb25NYW5hZ2VyIHtcbiAgY29uc3RydWN0b3IocHJvdGVjdGVkIHVzZXJTZXJ2aWNlOiBVc2VyU2VydmljZSkge1xuICB9XG4gIHB1YmxpYyBhc3luYyBhdXRob3JpemUoYWN0aW9uOiBzdHJpbmcsIHJlc291cmNlOiBzdHJpbmcpOiBQcm9taXNlPGJvb2xlYW4+IHtcblxuICAgIGNvbnN0IHVzZXJXaXRoQXV0aG9yaXphdGlvbnMgPSB0aGlzLnVzZXJTZXJ2aWNlLnVzZXJWYWx1ZSBhcyBVc2VyV2l0aEF1dGhvcml6YXRpb25zO1xuXG4gICAgaWYgKHVzZXJXaXRoQXV0aG9yaXphdGlvbnMuYXV0aGVudGljYXRlZCkge1xuXG4gICAgICBpZiAodXNlcldpdGhBdXRob3JpemF0aW9ucy5hdXRob3JpemF0aW9ucyAhPSBudWxsICYmIHVzZXJXaXRoQXV0aG9yaXphdGlvbnMuYXV0aG9yaXphdGlvbnMubGVuZ3RoID4gMCkge1xuXG4gICAgICAgIGNvbnN0IHVzdWFyaW9Qb3NzdWlQZXJtaXNzYW8gPSB1c2VyV2l0aEF1dGhvcml6YXRpb25zLmF1dGhvcml6YXRpb25zLnNvbWUocGVybWlzc2FvID0+IHtcbiAgICAgICAgICBjb25zdCBlaE1lc21vUmVjdXJzbyA9IHBlcm1pc3Nhby5yZXNvdXJjZSA9PT0gcmVzb3VyY2U7XG4gICAgICAgICAgY29uc3QgZWhNZXNtYUFjYW8gPSBwZXJtaXNzYW8uYWN0aW9uLmluY2x1ZGVzKGFjdGlvbik7XG5cbiAgICAgICAgICBjb25zdCBwb3NzdWlQZXJtaXNzYW8gPSBlaE1lc21vUmVjdXJzbyAmJiBlaE1lc21hQWNhbztcbiAgICAgICAgICByZXR1cm4gcG9zc3VpUGVybWlzc2FvO1xuICAgICAgICB9KTtcblxuICAgICAgICBpZiAodXN1YXJpb1Bvc3N1aVBlcm1pc3Nhbykge1xuICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICB9XG5cbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cbn1cbiJdfQ==