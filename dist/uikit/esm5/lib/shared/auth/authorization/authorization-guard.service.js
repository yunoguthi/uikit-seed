/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Location, PlatformLocation } from '@angular/common';
import { GuardService } from './guard.service';
import { AuthorizationService } from './authorization.service';
/**
 * @abstract
 */
var AuthorizationGuardService = /** @class */ (function (_super) {
    tslib_1.__extends(AuthorizationGuardService, _super);
    function AuthorizationGuardService(authorizationService, router, angularLocation, platformLocation) {
        var _this = _super.call(this, router, angularLocation, platformLocation) || this;
        _this.authorizationService = authorizationService;
        _this.router = router;
        _this.angularLocation = angularLocation;
        _this.platformLocation = platformLocation;
        return _this;
    }
    AuthorizationGuardService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    AuthorizationGuardService.ctorParameters = function () { return [
        { type: AuthorizationService },
        { type: Router },
        { type: Location },
        { type: PlatformLocation }
    ]; };
    return AuthorizationGuardService;
}(GuardService));
export { AuthorizationGuardService };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    AuthorizationGuardService.prototype.authorizationService;
    /**
     * @type {?}
     * @protected
     */
    AuthorizationGuardService.prototype.router;
    /**
     * @type {?}
     * @protected
     */
    AuthorizationGuardService.prototype.angularLocation;
    /**
     * @type {?}
     * @protected
     */
    AuthorizationGuardService.prototype.platformLocation;
    /**
     * @abstract
     * @param {?} route
     * @param {?} state
     * @return {?}
     */
    AuthorizationGuardService.prototype.canActivate = function (route, state) { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aG9yaXphdGlvbi1ndWFyZC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvYXV0aC9hdXRob3JpemF0aW9uL2F1dGhvcml6YXRpb24tZ3VhcmQuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUNBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUE0RCxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUduRyxPQUFPLEVBQUUsUUFBUSxFQUFFLGdCQUFnQixFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDN0QsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHlCQUF5QixDQUFDOzs7O0FBRy9EO0lBQ3dELHFEQUFZO0lBQ2xFLG1DQUNZLG9CQUEwQyxFQUMxQyxNQUFjLEVBQ2QsZUFBeUIsRUFDekIsZ0JBQWtDO1FBSjlDLFlBTUUsa0JBQU0sTUFBTSxFQUFFLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxTQUNqRDtRQU5XLDBCQUFvQixHQUFwQixvQkFBb0IsQ0FBc0I7UUFDMUMsWUFBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLHFCQUFlLEdBQWYsZUFBZSxDQUFVO1FBQ3pCLHNCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7O0lBRzlDLENBQUM7O2dCQVRGLFVBQVU7Ozs7Z0JBSEYsb0JBQW9CO2dCQUxzQyxNQUFNO2dCQUdoRSxRQUFRO2dCQUFFLGdCQUFnQjs7SUFrQm5DLGdDQUFDO0NBQUEsQUFiRCxDQUN3RCxZQUFZLEdBWW5FO1NBWnFCLHlCQUF5Qjs7Ozs7O0lBRTNDLHlEQUFvRDs7Ozs7SUFDcEQsMkNBQXdCOzs7OztJQUN4QixvREFBbUM7Ozs7O0lBQ25DLHFEQUE0Qzs7Ozs7OztJQUs5Qyw4RUFBd0ciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBBdXRoZW50aWNhdGlvbkd1YXJkU2VydmljZSB9IGZyb20gJy4vYXV0aGVudGljYXRpb24tZ3VhcmQuc2VydmljZSc7XG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDYW5BY3RpdmF0ZSwgQWN0aXZhdGVkUm91dGVTbmFwc2hvdCwgUm91dGVyU3RhdGVTbmFwc2hvdCwgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSAnLi4vYXV0aGVudGljYXRpb24vdXNlci5zZXJ2aWNlJztcbmltcG9ydCB7IExvY2F0aW9uLCBQbGF0Zm9ybUxvY2F0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7IEd1YXJkU2VydmljZSB9IGZyb20gJy4vZ3VhcmQuc2VydmljZSc7XG5pbXBvcnQgeyBBdXRob3JpemF0aW9uU2VydmljZSB9IGZyb20gJy4vYXV0aG9yaXphdGlvbi5zZXJ2aWNlJztcblxuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgQXV0aG9yaXphdGlvbkd1YXJkU2VydmljZSBleHRlbmRzIEd1YXJkU2VydmljZSB7XG4gIGNvbnN0cnVjdG9yKFxuICAgIHByb3RlY3RlZCBhdXRob3JpemF0aW9uU2VydmljZTogQXV0aG9yaXphdGlvblNlcnZpY2UsXG4gICAgcHJvdGVjdGVkIHJvdXRlcjogUm91dGVyLFxuICAgIHByb3RlY3RlZCBhbmd1bGFyTG9jYXRpb246IExvY2F0aW9uLFxuICAgIHByb3RlY3RlZCBwbGF0Zm9ybUxvY2F0aW9uOiBQbGF0Zm9ybUxvY2F0aW9uXG4gICkge1xuICAgIHN1cGVyKHJvdXRlciwgYW5ndWxhckxvY2F0aW9uLCBwbGF0Zm9ybUxvY2F0aW9uKTtcbiAgfVxuXG4gIGFzeW5jIGFic3RyYWN0IGNhbkFjdGl2YXRlKHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LCBzdGF0ZTogUm91dGVyU3RhdGVTbmFwc2hvdCk6IFByb21pc2U8Ym9vbGVhbj47XG5cbn1cbiJdfQ==