/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable, Inject, Optional } from '@angular/core';
import { IAuthorizationManagerToken } from './authorization-service.token';
/**
 * @record
 */
export function IAuthorizationManager() { }
if (false) {
    /**
     * Método responsável por verificar se o usuário possui permissão de executar a ação '{action}' no recurso '{resource}'
     * Attribute Based - https://en.wikipedia.org/wiki/Attribute-based_access_control
     * @param {?} action
     * @param {?} resource
     * @return {?}
     */
    IAuthorizationManager.prototype.authorize = function (action, resource) { };
}
var AuthorizationService = /** @class */ (function () {
    function AuthorizationService(authorizationService) {
        this.authorizationService = authorizationService;
    }
    /**
     * @private
     * @return {?}
     */
    AuthorizationService.prototype.validate = /**
     * @private
     * @return {?}
     */
    function () {
        if (this.authorizationService == null) {
            throw new Error('Deve ser registrado um "AuthorizationManager" (IAuthorizationManagerToken) para que se verifique autorização!');
        }
    };
    /**
     * @param {?} action
     * @param {?} resource
     * @return {?}
     */
    AuthorizationService.prototype.authorize = /**
     * @param {?} action
     * @param {?} resource
     * @return {?}
     */
    function (action, resource) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var hasPermission;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.validate();
                        // if (!client) {
                        //   if ((!this.authorizationConfig) || (!this.authorizationConfig.clientId)) {
                        //     throw Error(`Não é possível verificar autorização sem identificar o 'client'!`);
                        //   }
                        //   client = this.authorizationConfig.clientId;
                        // }
                        console.log("Verificando autoriza\u00E7\u00E3o para executar a a\u00E7\u00E3o '" + action + "' no recurso '" + resource + "'...");
                        return [4 /*yield*/, this.authorizationService.authorize(action, resource)];
                    case 1:
                        hasPermission = _a.sent();
                        console.log("Autoriza\u00E7\u00E3o para executar a a\u00E7\u00E3o '" + action + "' no recurso '" + resource + "' foi '" + (hasPermission ? 'concedida' : 'negada') + "'!");
                        return [2 /*return*/, hasPermission];
                }
            });
        });
    };
    AuthorizationService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    AuthorizationService.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [IAuthorizationManagerToken,] }, { type: Optional }] }
    ]; };
    return AuthorizationService;
}());
export { AuthorizationService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    AuthorizationService.prototype.authorizationService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aG9yaXphdGlvbi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvYXV0aC9hdXRob3JpemF0aW9uL2F1dGhvcml6YXRpb24uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM3RCxPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQzs7OztBQUkzRSwyQ0FNQzs7Ozs7Ozs7O0lBREMsNEVBQThEOztBQUdoRTtJQUVFLDhCQUMwRCxvQkFBNEM7UUFBNUMseUJBQW9CLEdBQXBCLG9CQUFvQixDQUF3QjtJQUd0RyxDQUFDOzs7OztJQUVPLHVDQUFROzs7O0lBQWhCO1FBQ0UsSUFBSSxJQUFJLENBQUMsb0JBQW9CLElBQUksSUFBSSxFQUFFO1lBQ3JDLE1BQU0sSUFBSSxLQUFLLENBQUMsK0dBQStHLENBQUMsQ0FBQztTQUNsSTtJQUNILENBQUM7Ozs7OztJQUVZLHdDQUFTOzs7OztJQUF0QixVQUF1QixNQUFjLEVBQUUsUUFBZ0I7Ozs7Ozt3QkFFckQsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO3dCQUVoQixpQkFBaUI7d0JBQ2pCLCtFQUErRTt3QkFDL0UsdUZBQXVGO3dCQUN2RixNQUFNO3dCQUNOLGdEQUFnRDt3QkFDaEQsSUFBSTt3QkFFSixPQUFPLENBQUMsR0FBRyxDQUFDLHVFQUFpRCxNQUFNLHNCQUFpQixRQUFRLFNBQU0sQ0FBQyxDQUFDO3dCQUM5RSxxQkFBTSxJQUFJLENBQUMsb0JBQW9CLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRSxRQUFRLENBQUMsRUFBQTs7d0JBQTNFLGFBQWEsR0FBRyxTQUEyRDt3QkFDakYsT0FBTyxDQUFDLEdBQUcsQ0FBQywyREFBcUMsTUFBTSxzQkFBaUIsUUFBUSxlQUFVLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxPQUFJLENBQUMsQ0FBQzt3QkFDeEksc0JBQU8sYUFBYSxFQUFDOzs7O0tBQ3RCOztnQkE3QkYsVUFBVTs7OztnREFHTixNQUFNLFNBQUMsMEJBQTBCLGNBQUcsUUFBUTs7SUFtRGpELDJCQUFDO0NBQUEsQUF0REQsSUFzREM7U0FyRFksb0JBQW9COzs7Ozs7SUFFN0Isb0RBQW9HIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSwgSW5qZWN0LCBPcHRpb25hbCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSUF1dGhvcml6YXRpb25NYW5hZ2VyVG9rZW4gfSBmcm9tICcuL2F1dGhvcml6YXRpb24tc2VydmljZS50b2tlbic7XG5pbXBvcnQgeyBBdXRob3JpemF0aW9uQ29uZmlnVG9rZW4sIEF1dGhvcml6YXRpb25Db25maWcgfSBmcm9tICcuL2F1dGhvcml6YXRpb24tY29uZmlnLnRva2VuJztcblxuXG5leHBvcnQgaW50ZXJmYWNlIElBdXRob3JpemF0aW9uTWFuYWdlciB7XG4gIC8qKlxuICAgKiBNw6l0b2RvIHJlc3BvbnPDoXZlbCBwb3IgdmVyaWZpY2FyIHNlIG8gdXN1w6FyaW8gcG9zc3VpIHBlcm1pc3PDo28gZGUgZXhlY3V0YXIgYSBhw6fDo28gJ3thY3Rpb259JyBubyByZWN1cnNvICd7cmVzb3VyY2V9J1xuICAgKiBBdHRyaWJ1dGUgQmFzZWQgLSBodHRwczovL2VuLndpa2lwZWRpYS5vcmcvd2lraS9BdHRyaWJ1dGUtYmFzZWRfYWNjZXNzX2NvbnRyb2xcbiAgICovXG4gIGF1dGhvcml6ZShhY3Rpb246IHN0cmluZywgcmVzb3VyY2U6IHN0cmluZyk6IFByb21pc2U8Ym9vbGVhbj47XG59XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBBdXRob3JpemF0aW9uU2VydmljZSB7XG4gIGNvbnN0cnVjdG9yKFxuICAgIEBJbmplY3QoSUF1dGhvcml6YXRpb25NYW5hZ2VyVG9rZW4pIEBPcHRpb25hbCgpIHByaXZhdGUgYXV0aG9yaXphdGlvblNlcnZpY2U/OiBJQXV0aG9yaXphdGlvbk1hbmFnZXJcbiAgKSB7XG5cbiAgfVxuXG4gIHByaXZhdGUgdmFsaWRhdGUoKSB7XG4gICAgaWYgKHRoaXMuYXV0aG9yaXphdGlvblNlcnZpY2UgPT0gbnVsbCkge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKCdEZXZlIHNlciByZWdpc3RyYWRvIHVtIFwiQXV0aG9yaXphdGlvbk1hbmFnZXJcIiAoSUF1dGhvcml6YXRpb25NYW5hZ2VyVG9rZW4pIHBhcmEgcXVlIHNlIHZlcmlmaXF1ZSBhdXRvcml6YcOnw6NvIScpO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyBhc3luYyBhdXRob3JpemUoYWN0aW9uOiBzdHJpbmcsIHJlc291cmNlOiBzdHJpbmcpOiBQcm9taXNlPGJvb2xlYW4+IHtcblxuICAgIHRoaXMudmFsaWRhdGUoKTtcblxuICAgIC8vIGlmICghY2xpZW50KSB7XG4gICAgLy8gICBpZiAoKCF0aGlzLmF1dGhvcml6YXRpb25Db25maWcpIHx8ICghdGhpcy5hdXRob3JpemF0aW9uQ29uZmlnLmNsaWVudElkKSkge1xuICAgIC8vICAgICB0aHJvdyBFcnJvcihgTsOjbyDDqSBwb3Nzw612ZWwgdmVyaWZpY2FyIGF1dG9yaXphw6fDo28gc2VtIGlkZW50aWZpY2FyIG8gJ2NsaWVudCchYCk7XG4gICAgLy8gICB9XG4gICAgLy8gICBjbGllbnQgPSB0aGlzLmF1dGhvcml6YXRpb25Db25maWcuY2xpZW50SWQ7XG4gICAgLy8gfVxuXG4gICAgY29uc29sZS5sb2coYFZlcmlmaWNhbmRvIGF1dG9yaXphw6fDo28gcGFyYSBleGVjdXRhciBhIGHDp8OjbyAnJHthY3Rpb259JyBubyByZWN1cnNvICcke3Jlc291cmNlfScuLi5gKTtcbiAgICBjb25zdCBoYXNQZXJtaXNzaW9uID0gYXdhaXQgdGhpcy5hdXRob3JpemF0aW9uU2VydmljZS5hdXRob3JpemUoYWN0aW9uLCByZXNvdXJjZSk7XG4gICAgY29uc29sZS5sb2coYEF1dG9yaXphw6fDo28gcGFyYSBleGVjdXRhciBhIGHDp8OjbyAnJHthY3Rpb259JyBubyByZWN1cnNvICcke3Jlc291cmNlfScgZm9pICckeyhoYXNQZXJtaXNzaW9uID8gJ2NvbmNlZGlkYScgOiAnbmVnYWRhJyl9JyFgKTtcbiAgICByZXR1cm4gaGFzUGVybWlzc2lvbjtcbiAgfVxuXG4gIC8vIC8qKiBWZXJpZmljYSBzZSBleGlzdGUgbm8gdXN1w6FyaW8gdW1hIHByb3ByaWVkYWRlIGNvbSBvIG5vbWUgZGEgYGNsYWltTmFtZWAgaW5mb3JtYWRhIGUgY29tIG8gdmFsb3IgZGEgYGNsYWltVmFsdWVgIGluZm9ybWFkby4gKi9cbiAgLy8gcHVibGljIGNoZWNrQ2xhaW1QZXJtaXNzaW9uKGNsYWltTmFtZTogc3RyaW5nLCBjbGFpbVZhbHVlPzogc3RyaW5nKTogYm9vbGVhbiB7XG4gIC8vICAgY29uc3QgdXNlciA9IHRoaXMudXNlclNlcnZpY2UudXNlclZhbHVlO1xuICAvLyAgIGNvbnN0IHVzZXJIYXNDbGFpbSA9IHVzZXIuaGFzT3duUHJvcGVydHkoY2xhaW1OYW1lKTtcbiAgLy8gICBpZiAoIXVzZXJIYXNDbGFpbSkge1xuICAvLyAgICAgcmV0dXJuIGZhbHNlO1xuICAvLyAgIH1cbiAgLy8gICBpZiAoY2xhaW1WYWx1ZSkge1xuICAvLyAgICAgY29uc3QgdXNlckNsYWltVmFsdWUgPSB1c2VyW2NsYWltTmFtZV07XG4gIC8vICAgICBpZiAodXNlckNsYWltVmFsdWUgPT09IGNsYWltVmFsdWUpIHtcbiAgLy8gICAgICAgcmV0dXJuIHRydWU7XG4gIC8vICAgICB9XG4gIC8vICAgfSBlbHNlIHtcbiAgLy8gICAgIHJldHVybiB0cnVlO1xuICAvLyAgIH1cbiAgLy8gICByZXR1cm4gZmFsc2U7XG4gIC8vIH1cblxuICAvLyBwdWJsaWMgaXNJblJvbGUocm9sZTogc3RyaW5nKTogYm9vbGVhbiB7XG4gIC8vICAgY29uc3QgdXNlciA9IHRoaXMudXNlclNlcnZpY2UudXNlclZhbHVlO1xuICAvLyAgIGNvbnN0IGlzSW5Sb2xlID0gdXNlci5yb2xlcy5pbmRleE9mKHJvbGUpID4gLTE7XG4gIC8vICAgcmV0dXJuIGlzSW5Sb2xlO1xuICAvLyB9XG59XG4iXX0=