/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../authentication/user.service';
import { Location, PlatformLocation } from '@angular/common';
import { GuardService } from './guard.service';
import { map } from 'rxjs/operators';
var AuthenticationGuardService = /** @class */ (function (_super) {
    tslib_1.__extends(AuthenticationGuardService, _super);
    function AuthenticationGuardService(identityService, router, angularLocation, platformLocation) {
        var _this = _super.call(this, router, angularLocation, platformLocation) || this;
        _this.identityService = identityService;
        _this.router = router;
        _this.angularLocation = angularLocation;
        _this.platformLocation = platformLocation;
        return _this;
    }
    /**
     * @param {?} route
     * @param {?} state
     * @return {?}
     */
    AuthenticationGuardService.prototype.canActivate = /**
     * @param {?} route
     * @param {?} state
     * @return {?}
     */
    function (route, state) {
        var _this = this;
        console.debug('AuthenticationGuardService!', this.identityService.userValue);
        /** @type {?} */
        var mapped = this.identityService.user$
            .pipe(map((/**
         * @param {?} user
         * @return {?}
         */
        function (user) {
            console.debug('Verificando se usuário está autenticado!', user);
            if (user.authenticated) {
                return _this.allow();
            }
            else {
                /** @type {?} */
                var data = route.data;
                /** @type {?} */
                var login = 'auto';
                if (data && data.login == 'manual') {
                    login = 'manual';
                }
                /** @type {?} */
                var setCallback = true;
                if (data && data.setCallback == false) {
                    setCallback = false;
                }
                return _this.deny(state, login, setCallback);
            }
        })));
        // const ret = map.catch(() => {
        //   return this.deny();
        // });
        return mapped;
    };
    AuthenticationGuardService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    AuthenticationGuardService.ctorParameters = function () { return [
        { type: UserService },
        { type: Router },
        { type: Location },
        { type: PlatformLocation }
    ]; };
    return AuthenticationGuardService;
}(GuardService));
export { AuthenticationGuardService };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    AuthenticationGuardService.prototype.identityService;
    /**
     * @type {?}
     * @protected
     */
    AuthenticationGuardService.prototype.router;
    /**
     * @type {?}
     * @protected
     */
    AuthenticationGuardService.prototype.angularLocation;
    /**
     * @type {?}
     * @protected
     */
    AuthenticationGuardService.prototype.platformLocation;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aGVudGljYXRpb24tZ3VhcmQuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2F1dGgvYXV0aG9yaXphdGlvbi9hdXRoZW50aWNhdGlvbi1ndWFyZC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQTRELE1BQU0sRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRW5HLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUM3RCxPQUFPLEVBQUUsUUFBUSxFQUFFLGdCQUFnQixFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDN0QsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRS9DLE9BQU8sRUFBRSxHQUFHLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUVyQztJQUNnRCxzREFBWTtJQUUxRCxvQ0FDWSxlQUE0QixFQUM1QixNQUFjLEVBQ2QsZUFBeUIsRUFDekIsZ0JBQWtDO1FBSjlDLFlBTUUsa0JBQU0sTUFBTSxFQUFFLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxTQUNqRDtRQU5XLHFCQUFlLEdBQWYsZUFBZSxDQUFhO1FBQzVCLFlBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxxQkFBZSxHQUFmLGVBQWUsQ0FBVTtRQUN6QixzQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCOztJQUc5QyxDQUFDOzs7Ozs7SUFFRCxnREFBVzs7Ozs7SUFBWCxVQUFZLEtBQTZCLEVBQUUsS0FBMEI7UUFBckUsaUJBeUJDO1FBeEJDLE9BQU8sQ0FBQyxLQUFLLENBQUMsNkJBQTZCLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsQ0FBQzs7WUFDdkUsTUFBTSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSzthQUN4QyxJQUFJLENBQUMsR0FBRzs7OztRQUFDLFVBQUEsSUFBSTtZQUNaLE9BQU8sQ0FBQyxLQUFLLENBQUMsMENBQTBDLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDaEUsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO2dCQUN0QixPQUFPLEtBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQzthQUNyQjtpQkFBTTs7b0JBQ0MsSUFBSSxHQUFHLEtBQUssQ0FBQyxJQUFJOztvQkFDbkIsS0FBSyxHQUFzQixNQUFNO2dCQUNyQyxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsS0FBSyxJQUFJLFFBQVEsRUFBRTtvQkFDbEMsS0FBSyxHQUFHLFFBQVEsQ0FBQztpQkFDbEI7O29CQUVHLFdBQVcsR0FBRyxJQUFJO2dCQUN0QixJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsV0FBVyxJQUFJLEtBQUssRUFBRTtvQkFDckMsV0FBVyxHQUFHLEtBQUssQ0FBQztpQkFDckI7Z0JBQ0QsT0FBTyxLQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxLQUFLLEVBQUUsV0FBVyxDQUFDLENBQUM7YUFDN0M7UUFDSCxDQUFDLEVBQUMsQ0FBQztRQUNILGdDQUFnQztRQUNoQyx3QkFBd0I7UUFDeEIsTUFBTTtRQUNOLE9BQU8sTUFBTSxDQUFDO0lBQ2hCLENBQUM7O2dCQXJDRixVQUFVOzs7O2dCQU5GLFdBQVc7Z0JBRitDLE1BQU07Z0JBR2hFLFFBQVE7Z0JBQUUsZ0JBQWdCOztJQTZDbkMsaUNBQUM7Q0FBQSxBQXhDRCxDQUNnRCxZQUFZLEdBdUMzRDtTQXZDWSwwQkFBMEI7Ozs7OztJQUduQyxxREFBc0M7Ozs7O0lBQ3RDLDRDQUF3Qjs7Ozs7SUFDeEIscURBQW1DOzs7OztJQUNuQyxzREFBNEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDYW5BY3RpdmF0ZSwgQWN0aXZhdGVkUm91dGVTbmFwc2hvdCwgUm91dGVyU3RhdGVTbmFwc2hvdCwgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSAnLi4vYXV0aGVudGljYXRpb24vdXNlci5zZXJ2aWNlJztcbmltcG9ydCB7IExvY2F0aW9uLCBQbGF0Zm9ybUxvY2F0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7IEd1YXJkU2VydmljZSB9IGZyb20gJy4vZ3VhcmQuc2VydmljZSc7XG5cbmltcG9ydCB7IG1hcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIEF1dGhlbnRpY2F0aW9uR3VhcmRTZXJ2aWNlIGV4dGVuZHMgR3VhcmRTZXJ2aWNlIHtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcm90ZWN0ZWQgaWRlbnRpdHlTZXJ2aWNlOiBVc2VyU2VydmljZSxcbiAgICBwcm90ZWN0ZWQgcm91dGVyOiBSb3V0ZXIsXG4gICAgcHJvdGVjdGVkIGFuZ3VsYXJMb2NhdGlvbjogTG9jYXRpb24sXG4gICAgcHJvdGVjdGVkIHBsYXRmb3JtTG9jYXRpb246IFBsYXRmb3JtTG9jYXRpb25cbiAgKSB7XG4gICAgc3VwZXIocm91dGVyLCBhbmd1bGFyTG9jYXRpb24sIHBsYXRmb3JtTG9jYXRpb24pO1xuICB9XG5cbiAgY2FuQWN0aXZhdGUocm91dGU6IEFjdGl2YXRlZFJvdXRlU25hcHNob3QsIHN0YXRlOiBSb3V0ZXJTdGF0ZVNuYXBzaG90KTogYm9vbGVhbiB8IFByb21pc2U8Ym9vbGVhbj4gfCBPYnNlcnZhYmxlPGJvb2xlYW4+IHtcbiAgICBjb25zb2xlLmRlYnVnKCdBdXRoZW50aWNhdGlvbkd1YXJkU2VydmljZSEnLCB0aGlzLmlkZW50aXR5U2VydmljZS51c2VyVmFsdWUpO1xuICAgIGNvbnN0IG1hcHBlZCA9IHRoaXMuaWRlbnRpdHlTZXJ2aWNlLnVzZXIkXG4gICAgLnBpcGUobWFwKHVzZXIgPT4ge1xuICAgICAgY29uc29sZS5kZWJ1ZygnVmVyaWZpY2FuZG8gc2UgdXN1w6FyaW8gZXN0w6EgYXV0ZW50aWNhZG8hJywgdXNlcik7XG4gICAgICBpZiAodXNlci5hdXRoZW50aWNhdGVkKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmFsbG93KCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBjb25zdCBkYXRhID0gcm91dGUuZGF0YTtcbiAgICAgICAgbGV0IGxvZ2luOiAnbWFudWFsJyB8ICdhdXRvJyA9ICdhdXRvJztcbiAgICAgICAgaWYgKGRhdGEgJiYgZGF0YS5sb2dpbiA9PSAnbWFudWFsJykge1xuICAgICAgICAgIGxvZ2luID0gJ21hbnVhbCc7XG4gICAgICAgIH1cblxuICAgICAgICBsZXQgc2V0Q2FsbGJhY2sgPSB0cnVlO1xuICAgICAgICBpZiAoZGF0YSAmJiBkYXRhLnNldENhbGxiYWNrID09IGZhbHNlKSB7XG4gICAgICAgICAgc2V0Q2FsbGJhY2sgPSBmYWxzZTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcy5kZW55KHN0YXRlLCBsb2dpbiwgc2V0Q2FsbGJhY2spO1xuICAgICAgfVxuICAgIH0pKTtcbiAgICAvLyBjb25zdCByZXQgPSBtYXAuY2F0Y2goKCkgPT4ge1xuICAgIC8vICAgcmV0dXJuIHRoaXMuZGVueSgpO1xuICAgIC8vIH0pO1xuICAgIHJldHVybiBtYXBwZWQ7XG4gIH1cblxuXG59XG4iXX0=