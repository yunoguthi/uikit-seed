/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthorizationConfigToken } from '../authorization-config.token';
import { UserService } from '../../authentication/user.service';
import * as i0 from "@angular/core";
import * as i1 from "../../authentication/user.service";
import * as i2 from "@angular/common/http";
import * as i3 from "../authorization-config.token";
var XacmlAuthorizationManager = /** @class */ (function () {
    function XacmlAuthorizationManager(userService, httpClient, authorizationConfig) {
        this.userService = userService;
        this.httpClient = httpClient;
        this.authorizationConfig = authorizationConfig;
    }
    /**
     * @param {?} action
     * @param {?} resource
     * @return {?}
     */
    XacmlAuthorizationManager.prototype.authorize = /**
     * @param {?} action
     * @param {?} resource
     * @return {?}
     */
    function (action, resource) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                // https://docs.wso2.com/display/IS570/Entitlement+with+REST+APIs
                // WSO2 - ${environment.settings.authentication.authority}api/identity/entitlement/decision/pdp
                return [2 /*return*/, this.httpClient
                        .post("" + this.authorizationConfig.xacmlConfig.policyDecisionEndpoint, {
                        Request: {
                            AccessSubject: {
                                Attribute: [
                                    {
                                        AttributeId: 'subject-id',
                                        Value: "" + this.userService.userValue.sub,
                                        DataType: 'string',
                                        IncludeInResult: true
                                    }
                                ]
                            },
                            Resource: {
                                Attribute: [
                                    {
                                        AttributeId: 'resource-id',
                                        Value: resource,
                                        DataType: 'string',
                                        IncludeInResult: true
                                    }
                                ]
                            },
                            Action: {
                                Attribute: [
                                    {
                                        AttributeId: 'action-id',
                                        Value: action,
                                        DataType: 'string',
                                        IncludeInResult: true
                                    }
                                ]
                            }
                        }
                    })
                        .toPromise()
                        .then((/**
                     * @param {?} result
                     * @return {?}
                     */
                    function (result) {
                        if (result && result.Response && result.Response[0]) {
                            /** @type {?} */
                            var resposta = result.Response[0];
                            if (resposta.Decision === 'Permit') {
                                return true;
                            }
                        }
                        return false;
                    }))];
            });
        });
    };
    XacmlAuthorizationManager.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    XacmlAuthorizationManager.ctorParameters = function () { return [
        { type: UserService },
        { type: HttpClient },
        { type: undefined, decorators: [{ type: Inject, args: [AuthorizationConfigToken,] }] }
    ]; };
    /** @nocollapse */ XacmlAuthorizationManager.ngInjectableDef = i0.defineInjectable({ factory: function XacmlAuthorizationManager_Factory() { return new XacmlAuthorizationManager(i0.inject(i1.UserService), i0.inject(i2.HttpClient), i0.inject(i3.AuthorizationConfigToken)); }, token: XacmlAuthorizationManager, providedIn: "root" });
    return XacmlAuthorizationManager;
}());
export { XacmlAuthorizationManager };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    XacmlAuthorizationManager.prototype.userService;
    /**
     * @type {?}
     * @protected
     */
    XacmlAuthorizationManager.prototype.httpClient;
    /**
     * @type {?}
     * @protected
     */
    XacmlAuthorizationManager.prototype.authorizationConfig;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoieGFjbWwtYXV0aG9yaXphdGlvbi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvYXV0aC9hdXRob3JpemF0aW9uL3hhY21sL3hhY21sLWF1dGhvcml6YXRpb24uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ25ELE9BQU8sRUFBRSxVQUFVLEVBQTJCLE1BQU0sc0JBQXNCLENBQUM7QUFDM0UsT0FBTyxFQUFFLHdCQUF3QixFQUF1QixNQUFNLCtCQUErQixDQUFDO0FBQzlGLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxtQ0FBbUMsQ0FBQzs7Ozs7QUFHaEU7SUFJRSxtQ0FDWSxXQUF3QixFQUN4QixVQUFzQixFQUNZLG1CQUF3QztRQUYxRSxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ1ksd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtJQUV0RixDQUFDOzs7Ozs7SUFDWSw2Q0FBUzs7Ozs7SUFBdEIsVUFBdUIsTUFBYyxFQUFFLFFBQWdCOzs7Z0JBRXJELGlFQUFpRTtnQkFDakUsK0ZBQStGO2dCQUUvRixzQkFBTyxJQUFJLENBQUMsVUFBVTt5QkFDbkIsSUFBSSxDQUFDLEtBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFdBQVcsQ0FBQyxzQkFBd0IsRUFDcEU7d0JBQ0UsT0FBTyxFQUFFOzRCQUNQLGFBQWEsRUFBRTtnQ0FDYixTQUFTLEVBQUU7b0NBQ1Q7d0NBQ0UsV0FBVyxFQUFFLFlBQVk7d0NBQ3pCLEtBQUssRUFBRSxLQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLEdBQUs7d0NBQzFDLFFBQVEsRUFBRSxRQUFRO3dDQUNsQixlQUFlLEVBQUUsSUFBSTtxQ0FDdEI7aUNBQ0Y7NkJBQ0Y7NEJBQ0QsUUFBUSxFQUFFO2dDQUNSLFNBQVMsRUFBRTtvQ0FDVDt3Q0FDRSxXQUFXLEVBQUUsYUFBYTt3Q0FDMUIsS0FBSyxFQUFFLFFBQVE7d0NBQ2YsUUFBUSxFQUFFLFFBQVE7d0NBQ2xCLGVBQWUsRUFBRSxJQUFJO3FDQUN0QjtpQ0FDRjs2QkFDRjs0QkFDRCxNQUFNLEVBQUU7Z0NBQ04sU0FBUyxFQUFFO29DQUNUO3dDQUNFLFdBQVcsRUFBRSxXQUFXO3dDQUN4QixLQUFLLEVBQUUsTUFBTTt3Q0FDYixRQUFRLEVBQUUsUUFBUTt3Q0FDbEIsZUFBZSxFQUFFLElBQUk7cUNBQ3RCO2lDQUNGOzZCQUNGO3lCQUNGO3FCQUNGLENBQ0Y7eUJBQ0EsU0FBUyxFQUFFO3lCQUNYLElBQUk7Ozs7b0JBQUMsVUFBQyxNQUF1RDt3QkFDNUQsSUFBSSxNQUFNLElBQUksTUFBTSxDQUFDLFFBQVEsSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxFQUFFOztnQ0FDN0MsUUFBUSxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDOzRCQUNuQyxJQUFJLFFBQVEsQ0FBQyxRQUFRLEtBQUssUUFBUSxFQUFFO2dDQUNsQyxPQUFPLElBQUksQ0FBQzs2QkFDYjt5QkFDRjt3QkFDRCxPQUFPLEtBQUssQ0FBQztvQkFDZixDQUFDLEVBQUMsRUFBQzs7O0tBQ047O2dCQTlERixVQUFVLFNBQUM7b0JBQ1YsVUFBVSxFQUFFLE1BQU07aUJBQ25COzs7O2dCQUxRLFdBQVc7Z0JBRlgsVUFBVTtnREFZZCxNQUFNLFNBQUMsd0JBQXdCOzs7b0NBYnBDO0NBcUVDLEFBL0RELElBK0RDO1NBNURZLHlCQUF5Qjs7Ozs7O0lBRWxDLGdEQUFrQzs7Ozs7SUFDbEMsK0NBQWdDOzs7OztJQUNoQyx3REFBb0YiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBJbmplY3QgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEh0dHBDbGllbnQsIEh0dHBIZWFkZXJzLCBIdHRwUGFyYW1zIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgQXV0aG9yaXphdGlvbkNvbmZpZ1Rva2VuLCBBdXRob3JpemF0aW9uQ29uZmlnIH0gZnJvbSAnLi4vYXV0aG9yaXphdGlvbi1jb25maWcudG9rZW4nO1xuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tICcuLi8uLi9hdXRoZW50aWNhdGlvbi91c2VyLnNlcnZpY2UnO1xuaW1wb3J0IHsgSUF1dGhvcml6YXRpb25NYW5hZ2VyIH0gZnJvbSAnLi4vYXV0aG9yaXphdGlvbi5zZXJ2aWNlJztcblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgWGFjbWxBdXRob3JpemF0aW9uTWFuYWdlciBpbXBsZW1lbnRzIElBdXRob3JpemF0aW9uTWFuYWdlciB7XG4gIGNvbnN0cnVjdG9yKFxuICAgIHByb3RlY3RlZCB1c2VyU2VydmljZTogVXNlclNlcnZpY2UsXG4gICAgcHJvdGVjdGVkIGh0dHBDbGllbnQ6IEh0dHBDbGllbnQsXG4gICAgQEluamVjdChBdXRob3JpemF0aW9uQ29uZmlnVG9rZW4pIHByb3RlY3RlZCBhdXRob3JpemF0aW9uQ29uZmlnOiBBdXRob3JpemF0aW9uQ29uZmlnLFxuICApIHtcbiAgfVxuICBwdWJsaWMgYXN5bmMgYXV0aG9yaXplKGFjdGlvbjogc3RyaW5nLCByZXNvdXJjZTogc3RyaW5nKTogUHJvbWlzZTxib29sZWFuPiB7XG5cbiAgICAvLyBodHRwczovL2RvY3Mud3NvMi5jb20vZGlzcGxheS9JUzU3MC9FbnRpdGxlbWVudCt3aXRoK1JFU1QrQVBJc1xuICAgIC8vIFdTTzIgLSAke2Vudmlyb25tZW50LnNldHRpbmdzLmF1dGhlbnRpY2F0aW9uLmF1dGhvcml0eX1hcGkvaWRlbnRpdHkvZW50aXRsZW1lbnQvZGVjaXNpb24vcGRwXG5cbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50XG4gICAgICAucG9zdChgJHt0aGlzLmF1dGhvcml6YXRpb25Db25maWcueGFjbWxDb25maWcucG9saWN5RGVjaXNpb25FbmRwb2ludH1gLFxuICAgICAgICB7XG4gICAgICAgICAgUmVxdWVzdDoge1xuICAgICAgICAgICAgQWNjZXNzU3ViamVjdDoge1xuICAgICAgICAgICAgICBBdHRyaWJ1dGU6IFtcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICBBdHRyaWJ1dGVJZDogJ3N1YmplY3QtaWQnLFxuICAgICAgICAgICAgICAgICAgVmFsdWU6IGAke3RoaXMudXNlclNlcnZpY2UudXNlclZhbHVlLnN1Yn1gLFxuICAgICAgICAgICAgICAgICAgRGF0YVR5cGU6ICdzdHJpbmcnLFxuICAgICAgICAgICAgICAgICAgSW5jbHVkZUluUmVzdWx0OiB0cnVlXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBdXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgUmVzb3VyY2U6IHtcbiAgICAgICAgICAgICAgQXR0cmlidXRlOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgQXR0cmlidXRlSWQ6ICdyZXNvdXJjZS1pZCcsXG4gICAgICAgICAgICAgICAgICBWYWx1ZTogcmVzb3VyY2UsXG4gICAgICAgICAgICAgICAgICBEYXRhVHlwZTogJ3N0cmluZycsXG4gICAgICAgICAgICAgICAgICBJbmNsdWRlSW5SZXN1bHQ6IHRydWVcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIF1cbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBBY3Rpb246IHtcbiAgICAgICAgICAgICAgQXR0cmlidXRlOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgQXR0cmlidXRlSWQ6ICdhY3Rpb24taWQnLFxuICAgICAgICAgICAgICAgICAgVmFsdWU6IGFjdGlvbixcbiAgICAgICAgICAgICAgICAgIERhdGFUeXBlOiAnc3RyaW5nJyxcbiAgICAgICAgICAgICAgICAgIEluY2x1ZGVJblJlc3VsdDogdHJ1ZVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgKVxuICAgICAgLnRvUHJvbWlzZSgpXG4gICAgICAudGhlbigocmVzdWx0OiB7IFJlc3BvbnNlOiB7IERlY2lzaW9uOiAnUGVybWl0JyB8ICdEZW55JyB9W10gfSkgPT4ge1xuICAgICAgICBpZiAocmVzdWx0ICYmIHJlc3VsdC5SZXNwb25zZSAmJiByZXN1bHQuUmVzcG9uc2VbMF0pIHtcbiAgICAgICAgICBjb25zdCByZXNwb3N0YSA9IHJlc3VsdC5SZXNwb25zZVswXTtcbiAgICAgICAgICBpZiAocmVzcG9zdGEuRGVjaXNpb24gPT09ICdQZXJtaXQnKSB7XG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgfSk7XG4gIH1cbn1cbiJdfQ==