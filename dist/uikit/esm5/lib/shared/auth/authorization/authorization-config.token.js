/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { InjectionToken } from '@angular/core';
/** @type {?} */
export var AuthorizationConfigToken = new InjectionToken('AuthorizationConfigToken');
/**
 * @record
 */
export function AuthorizationConfig() { }
if (false) {
    /** @type {?} */
    AuthorizationConfig.prototype.clientId;
    /**
     * Necessário para o UmaAuthorization ('User Managed Access')
     * @type {?|undefined}
     */
    AuthorizationConfig.prototype.umaConfig;
    /**
     * Necessário para o XacmlAuthorization ('eXtensible Access Control Markup Language')
     * @type {?|undefined}
     */
    AuthorizationConfig.prototype.xacmlConfig;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aG9yaXphdGlvbi1jb25maWcudG9rZW4uanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9hdXRoL2F1dGhvcml6YXRpb24vYXV0aG9yaXphdGlvbi1jb25maWcudG9rZW4udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxlQUFlLENBQUM7O0FBRS9DLE1BQU0sS0FBTyx3QkFBd0IsR0FBRyxJQUFJLGNBQWMsQ0FBc0IsMEJBQTBCLENBQUM7Ozs7QUFFM0cseUNBZ0JDOzs7SUFmQyx1Q0FBaUI7Ozs7O0lBRWpCLHdDQUtFOzs7OztJQUVGLDBDQUtFIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0aW9uVG9rZW4gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuZXhwb3J0IGNvbnN0IEF1dGhvcml6YXRpb25Db25maWdUb2tlbiA9IG5ldyBJbmplY3Rpb25Ub2tlbjxBdXRob3JpemF0aW9uQ29uZmlnPignQXV0aG9yaXphdGlvbkNvbmZpZ1Rva2VuJyk7XG5cbmV4cG9ydCBpbnRlcmZhY2UgQXV0aG9yaXphdGlvbkNvbmZpZyB7XG4gIGNsaWVudElkOiBzdHJpbmc7XG4gIC8qKiBOZWNlc3PDoXJpbyBwYXJhIG8gVW1hQXV0aG9yaXphdGlvbiAoJ1VzZXIgTWFuYWdlZCBBY2Nlc3MnKSAqL1xuICB1bWFDb25maWc/OiB7XG4gICAgLyoqIFVzYWRvIHBlbG8gVW1hQXV0aG9yaXphdGlvbk1hbmFnZXIgKi9cbiAgICBwZXJtaXNzaW9uRW5kcG9pbnQ/OiBzdHJpbmc7XG4gICAgLyoqIFVzYWRvIHBlbG8gVW1hQXV0aG9yaXphdGlvbk1hbmFnZXIgZSBLZXljbG9ha0F1dGhlbnRpY2F0aW9uTWFuYWdlciAqL1xuICAgIHRva2VuRW5kcG9pbnQ/OiBzdHJpbmc7XG4gIH07XG4gIC8qKiBOZWNlc3PDoXJpbyBwYXJhIG8gWGFjbWxBdXRob3JpemF0aW9uICgnZVh0ZW5zaWJsZSBBY2Nlc3MgQ29udHJvbCBNYXJrdXAgTGFuZ3VhZ2UnKSAqL1xuICB4YWNtbENvbmZpZz86IHtcbiAgICAvKiogVXNhZG8gcGVsbyBXc28yQXV0aG9yaXphdGlvbk1hbmFnZXIgKi9cbiAgICBwb2xpY3lEZWNpc2lvbkVuZHBvaW50Pzogc3RyaW5nO1xuICAgIC8qKiBVc2FkbyBwZWxvIFdzbzJBdXRoZW50aWNhdGlvbk1hbmFnZXIgKi9cbiAgICBlbnRpdGxlbWVudHNBbGxFbmRwb2ludD86IHN0cmluZztcbiAgfTtcbn1cbiJdfQ==