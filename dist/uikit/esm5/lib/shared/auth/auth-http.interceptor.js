/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { catchError } from 'rxjs/operators';
import { Injectable, Optional } from '@angular/core';
import { throwError } from 'rxjs';
import { UserService } from './authentication/user.service';
import { LogService } from '../../utils/log/log.service';
var AuthHttpInterceptor = /** @class */ (function () {
    function AuthHttpInterceptor(userService, logService) {
        this.userService = userService;
        this.logService = logService;
    }
    /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    AuthHttpInterceptor.prototype.intercept = /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    function (req, next) {
        var _this = this;
        /** @type {?} */
        var authReq = req;
        if (!req.headers.has('Authorization')) {
            if (this.userService && this.userService.userValue && this.userService.userValue.authenticated) {
                /** @type {?} */
                var token = ((/** @type {?} */ (this.userService.userValue))).access_token ||
                    ((/** @type {?} */ (this.userService.userValue))).id_token;
                /** @type {?} */
                var httpHeaders = req.headers.set('Authorization', "Bearer " + token);
                if (!req.headers.has('content-type')) {
                    httpHeaders = httpHeaders.set('content-type', 'application/json');
                }
                authReq = req.clone({
                    headers: httpHeaders
                });
                if (this.logService) {
                    this.logService.debug("N\u00E3o possui 'Authorization' header, foi adicionado.");
                }
            }
        }
        /** @type {?} */
        var httpHandle = next
            .handle(authReq)
            .pipe(catchError((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            if (_this.logService) {
                _this.logService.error(error);
            }
            // return the error to the method that called it
            return throwError(error);
        })));
        return httpHandle;
    };
    AuthHttpInterceptor.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    AuthHttpInterceptor.ctorParameters = function () { return [
        { type: UserService },
        { type: LogService, decorators: [{ type: Optional }] }
    ]; };
    return AuthHttpInterceptor;
}());
export { AuthHttpInterceptor };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    AuthHttpInterceptor.prototype.userService;
    /**
     * @type {?}
     * @protected
     */
    AuthHttpInterceptor.prototype.logService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC1odHRwLmludGVyY2VwdG9yLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvYXV0aC9hdXRoLWh0dHAuaW50ZXJjZXB0b3IudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUM1QyxPQUFPLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUVyRCxPQUFPLEVBQWMsVUFBVSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQzlDLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUU1RCxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFFekQ7SUFHRSw2QkFDWSxXQUF3QixFQUNaLFVBQXNCO1FBRGxDLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQ1osZUFBVSxHQUFWLFVBQVUsQ0FBWTtJQUc5QyxDQUFDOzs7Ozs7SUFFRCx1Q0FBUzs7Ozs7SUFBVCxVQUFVLEdBQXFCLEVBQUUsSUFBaUI7UUFBbEQsaUJBc0NDOztZQXBDSyxPQUFPLEdBQUcsR0FBRztRQUVqQixJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLEVBQUU7WUFDckMsSUFBSSxJQUFJLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLGFBQWEsRUFBRTs7b0JBQ3hGLEtBQUssR0FDVCxDQUFDLG1CQUFBLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxFQUFhLENBQUMsQ0FBQyxZQUFZO29CQUN0RCxDQUFDLG1CQUFBLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxFQUFxQixDQUFDLENBQUMsUUFBUTs7b0JBRXhELFdBQVcsR0FBRyxHQUFHLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLEVBQUUsWUFBVSxLQUFPLENBQUM7Z0JBRXJFLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsRUFBRTtvQkFDcEMsV0FBVyxHQUFHLFdBQVcsQ0FBQyxHQUFHLENBQUMsY0FBYyxFQUFFLGtCQUFrQixDQUFDLENBQUM7aUJBQ25FO2dCQUVELE9BQU8sR0FBRyxHQUFHLENBQUMsS0FBSyxDQUFDO29CQUNsQixPQUFPLEVBQUUsV0FBVztpQkFDckIsQ0FBQyxDQUFDO2dCQUVILElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtvQkFDbkIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMseURBQW9ELENBQUMsQ0FBQztpQkFDN0U7YUFDRjtTQUNGOztZQUVLLFVBQVUsR0FBRyxJQUFJO2FBQ3BCLE1BQU0sQ0FBQyxPQUFPLENBQUM7YUFDZixJQUFJLENBQUMsVUFBVTs7OztRQUFDLFVBQUMsS0FBSztZQUNyQixJQUFJLEtBQUksQ0FBQyxVQUFVLEVBQUU7Z0JBQ25CLEtBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQzlCO1lBQ0QsZ0RBQWdEO1lBQ2hELE9BQU8sVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzNCLENBQUMsRUFBQyxDQUFDO1FBRUwsT0FBTyxVQUFVLENBQUM7SUFFcEIsQ0FBQzs7Z0JBaERGLFVBQVU7Ozs7Z0JBSkYsV0FBVztnQkFFWCxVQUFVLHVCQU9kLFFBQVE7O0lBNENiLDBCQUFDO0NBQUEsQUFqREQsSUFpREM7U0FoRFksbUJBQW1COzs7Ozs7SUFHNUIsMENBQWtDOzs7OztJQUNsQyx5Q0FBNEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBjYXRjaEVycm9yIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHsgSW5qZWN0YWJsZSwgT3B0aW9uYWwgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEh0dHBFdmVudCwgSHR0cEludGVyY2VwdG9yLCBIdHRwSGFuZGxlciwgSHR0cFJlcXVlc3QsIEh0dHBIZWFkZXJzIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgdGhyb3dFcnJvciB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tICcuL2F1dGhlbnRpY2F0aW9uL3VzZXIuc2VydmljZSc7XG5pbXBvcnQgeyBPQXV0aFVzZXIsIE9wZW5JRENvbm5lY3RVc2VyIH0gZnJvbSAnLi9hdXRoZW50aWNhdGlvbi91c2VyLm1vZGVsJztcbmltcG9ydCB7IExvZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi91dGlscy9sb2cvbG9nLnNlcnZpY2UnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgQXV0aEh0dHBJbnRlcmNlcHRvciBpbXBsZW1lbnRzIEh0dHBJbnRlcmNlcHRvciB7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJvdGVjdGVkIHVzZXJTZXJ2aWNlOiBVc2VyU2VydmljZSxcbiAgICBAT3B0aW9uYWwoKSBwcm90ZWN0ZWQgbG9nU2VydmljZTogTG9nU2VydmljZVxuICApIHtcblxuICB9XG5cbiAgaW50ZXJjZXB0KHJlcTogSHR0cFJlcXVlc3Q8YW55PiwgbmV4dDogSHR0cEhhbmRsZXIpOiBPYnNlcnZhYmxlPEh0dHBFdmVudDxhbnk+PiB7XG5cbiAgICBsZXQgYXV0aFJlcSA9IHJlcTtcblxuICAgIGlmICghcmVxLmhlYWRlcnMuaGFzKCdBdXRob3JpemF0aW9uJykpIHtcbiAgICAgIGlmICh0aGlzLnVzZXJTZXJ2aWNlICYmIHRoaXMudXNlclNlcnZpY2UudXNlclZhbHVlICYmIHRoaXMudXNlclNlcnZpY2UudXNlclZhbHVlLmF1dGhlbnRpY2F0ZWQpIHtcbiAgICAgICAgY29uc3QgdG9rZW4gPVxuICAgICAgICAgICh0aGlzLnVzZXJTZXJ2aWNlLnVzZXJWYWx1ZSBhcyBPQXV0aFVzZXIpLmFjY2Vzc190b2tlbiB8fFxuICAgICAgICAgICh0aGlzLnVzZXJTZXJ2aWNlLnVzZXJWYWx1ZSBhcyBPcGVuSURDb25uZWN0VXNlcikuaWRfdG9rZW47XG5cbiAgICAgICAgbGV0IGh0dHBIZWFkZXJzID0gcmVxLmhlYWRlcnMuc2V0KCdBdXRob3JpemF0aW9uJywgYEJlYXJlciAke3Rva2VufWApO1xuXG4gICAgICAgIGlmICghcmVxLmhlYWRlcnMuaGFzKCdjb250ZW50LXR5cGUnKSkge1xuICAgICAgICAgIGh0dHBIZWFkZXJzID0gaHR0cEhlYWRlcnMuc2V0KCdjb250ZW50LXR5cGUnLCAnYXBwbGljYXRpb24vanNvbicpO1xuICAgICAgICB9XG5cbiAgICAgICAgYXV0aFJlcSA9IHJlcS5jbG9uZSh7XG4gICAgICAgICAgaGVhZGVyczogaHR0cEhlYWRlcnNcbiAgICAgICAgfSk7XG5cbiAgICAgICAgaWYgKHRoaXMubG9nU2VydmljZSkge1xuICAgICAgICAgIHRoaXMubG9nU2VydmljZS5kZWJ1ZyhgTsOjbyBwb3NzdWkgJ0F1dGhvcml6YXRpb24nIGhlYWRlciwgZm9pIGFkaWNpb25hZG8uYCk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG5cbiAgICBjb25zdCBodHRwSGFuZGxlID0gbmV4dFxuICAgICAgLmhhbmRsZShhdXRoUmVxKVxuICAgICAgLnBpcGUoY2F0Y2hFcnJvcigoZXJyb3IpID0+IHtcbiAgICAgICAgaWYgKHRoaXMubG9nU2VydmljZSkge1xuICAgICAgICAgIHRoaXMubG9nU2VydmljZS5lcnJvcihlcnJvcik7XG4gICAgICAgIH1cbiAgICAgICAgLy8gcmV0dXJuIHRoZSBlcnJvciB0byB0aGUgbWV0aG9kIHRoYXQgY2FsbGVkIGl0XG4gICAgICAgIHJldHVybiB0aHJvd0Vycm9yKGVycm9yKTtcbiAgICAgIH0pKTtcblxuICAgIHJldHVybiBodHRwSGFuZGxlO1xuXG4gIH1cbn1cbiJdfQ==