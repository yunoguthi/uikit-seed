/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, Optional } from '@angular/core';
import { AuthenticationService } from './authentication/authentication.service';
import { AuthorizationService } from './authorization/authorization.service';
import { UserService } from './authentication/user.service';
var AuthService = /** @class */ (function () {
    function AuthService(user, authentication, authorization) {
        this.user = user;
        this.authentication = authentication;
        this.authorization = authorization;
    }
    AuthService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    AuthService.ctorParameters = function () { return [
        { type: UserService },
        { type: AuthenticationService, decorators: [{ type: Optional }] },
        { type: AuthorizationService, decorators: [{ type: Optional }] }
    ]; };
    return AuthService;
}());
export { AuthService };
if (false) {
    /** @type {?} */
    AuthService.prototype.user;
    /** @type {?} */
    AuthService.prototype.authentication;
    /** @type {?} */
    AuthService.prototype.authorization;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvYXV0aC9hdXRoLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRXJELE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQ2hGLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQzdFLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUU1RDtJQUdFLHFCQUNTLElBQWlCLEVBQ0wsY0FBc0MsRUFDdEMsYUFBb0M7UUFGaEQsU0FBSSxHQUFKLElBQUksQ0FBYTtRQUNMLG1CQUFjLEdBQWQsY0FBYyxDQUF3QjtRQUN0QyxrQkFBYSxHQUFiLGFBQWEsQ0FBdUI7SUFDckQsQ0FBQzs7Z0JBUE4sVUFBVTs7OztnQkFGRixXQUFXO2dCQUZYLHFCQUFxQix1QkFTekIsUUFBUTtnQkFSSixvQkFBb0IsdUJBU3hCLFFBQVE7O0lBSWIsa0JBQUM7Q0FBQSxBQVZELElBVUM7U0FUWSxXQUFXOzs7SUFHcEIsMkJBQXdCOztJQUN4QixxQ0FBeUQ7O0lBQ3pELG9DQUF1RCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIE9wdGlvbmFsIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBBdXRoTW9kdWxlIH0gZnJvbSAnLi9hdXRoLm1vZHVsZSc7XG5pbXBvcnQgeyBBdXRoZW50aWNhdGlvblNlcnZpY2UgfSBmcm9tICcuL2F1dGhlbnRpY2F0aW9uL2F1dGhlbnRpY2F0aW9uLnNlcnZpY2UnO1xuaW1wb3J0IHsgQXV0aG9yaXphdGlvblNlcnZpY2UgfSBmcm9tICcuL2F1dGhvcml6YXRpb24vYXV0aG9yaXphdGlvbi5zZXJ2aWNlJztcbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSAnLi9hdXRoZW50aWNhdGlvbi91c2VyLnNlcnZpY2UnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgQXV0aFNlcnZpY2Uge1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHB1YmxpYyB1c2VyOiBVc2VyU2VydmljZSxcbiAgICBAT3B0aW9uYWwoKSBwdWJsaWMgYXV0aGVudGljYXRpb24/OiBBdXRoZW50aWNhdGlvblNlcnZpY2UsXG4gICAgQE9wdGlvbmFsKCkgcHVibGljIGF1dGhvcml6YXRpb24/OiBBdXRob3JpemF0aW9uU2VydmljZSxcbiAgKSB7IH1cblxuXG59XG4iXX0=