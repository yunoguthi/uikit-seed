/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {number} */
var SearchMode = {
    FullTextSearch: 0,
    Filtro: 1,
};
export { SearchMode };
SearchMode[SearchMode.FullTextSearch] = 'FullTextSearch';
SearchMode[SearchMode.Filtro] = 'Filtro';
/**
 * @record
 */
export function Pesquisa() { }
if (false) {
    /** @type {?} */
    Pesquisa.prototype.termo;
    /** @type {?|undefined} */
    Pesquisa.prototype.config;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGVzcXVpc2EuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9maWx0ZXJzL3Blc3F1aXNhLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztJQUdFLGlCQUFjO0lBQ2QsU0FBTTs7Ozs7Ozs7QUFHUiw4QkFTQzs7O0lBUkMseUJBQWM7O0lBQ2QsMEJBTUUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBGaWx0cm9PcGVyYWRvciB9IGZyb20gJy4vZmlsdGVyJztcblxuZXhwb3J0IGVudW0gU2VhcmNoTW9kZSB7XG4gIEZ1bGxUZXh0U2VhcmNoLFxuICBGaWx0cm8sXG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgUGVzcXVpc2Ege1xuICB0ZXJtbzogc3RyaW5nO1xuICBjb25maWc/OiB7XG4gICAgbW9kbz86IFNlYXJjaE1vZGU7XG4gICAgZmlsdHJvPzoge1xuICAgICAgY2FtcG9zOiBzdHJpbmdbXTtcbiAgICAgIG9wZXJhZG9yPzogRmlsdHJvT3BlcmFkb3I7XG4gICAgfVxuICB9O1xufVxuIl19