/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { InjectionToken } from '@angular/core';
import { FiltroOperador, isFilterComposite, FiltroCompostoOperador } from '../filter';
import { SearchMode } from '../pesquisa';
/** @type {?} */
export var SERIALIZER_TOKEN = new InjectionToken('Serializer');
/**
 * @abstract
 */
var /**
 * @abstract
 */
BaseSerializerService = /** @class */ (function () {
    function BaseSerializerService() {
        this.quantidadeDeFiltrosAlinhadosSuportados = Number.MAX_SAFE_INTEGER;
        this.pesquisaFiltroOperadorPadrao = FiltroOperador.Contem;
        this.pesquisaFiltroCompostoOperadorPadrao = FiltroCompostoOperador.Ou;
    }
    /**
     * @param {?} parametrosDaRequisicao
     * @return {?}
     */
    BaseSerializerService.prototype.serializeFiltros = /**
     * @param {?} parametrosDaRequisicao
     * @return {?}
     */
    function (parametrosDaRequisicao) {
        if (!this.suportaFiltros) {
            throw new Error('Este serializador não suporta filtros!');
        }
        /** @type {?} */
        var formatoDaClausula = this.filtroClausulaFormato;
        /** @type {?} */
        var filtrosCompostoSerializado = this.serializeFiltroComposto(parametrosDaRequisicao.filtros);
        if (filtrosCompostoSerializado != null) {
            /** @type {?} */
            var filtrosSerializado = this.format(formatoDaClausula, { filtros: filtrosCompostoSerializado });
            return filtrosSerializado;
        }
        else {
            return null;
        }
    };
    /**
     * @param {?} filtroComposto
     * @param {?=} nivel
     * @return {?}
     */
    BaseSerializerService.prototype.serializeFiltroComposto = /**
     * @param {?} filtroComposto
     * @param {?=} nivel
     * @return {?}
     */
    function (filtroComposto, nivel) {
        var _this = this;
        if (nivel === void 0) { nivel = 1; }
        if (nivel > this.quantidadeDeFiltrosAlinhadosSuportados) {
            throw new Error("A quantidade de filtros alinhados suportados \u00E9 '" + this.quantidadeDeFiltrosAlinhadosSuportados + "' e foi alcan\u00E7ado o n\u00EDvel '" + nivel + "'!");
        }
        /** @type {?} */
        var filtrosSerializadosArray = filtroComposto.filtros.map((/**
         * @param {?} filtro
         * @return {?}
         */
        function (filtro) {
            if (isFilterComposite(filtro)) {
                /** @type {?} */
                var formatoDosFiltros = _this.filtroFormato;
                /** @type {?} */
                var proximoNivel = nivel + 1;
                /** @type {?} */
                var filtrosCompostoSerializado = _this.serializeFiltroComposto(filtro, proximoNivel);
                /** @type {?} */
                var filtrosSerializado = _this.format(formatoDosFiltros, { filtro: filtrosCompostoSerializado });
                return filtrosSerializado;
            }
            else {
                return _this.serializeFiltro(filtro);
            }
        }));
        /** @type {?} */
        var filtroCompostoOperador = null;
        if (filtrosSerializadosArray.length > 1) {
            filtroCompostoOperador = this.getFiltroCompostoOperadorMapeado(filtroComposto.operador);
        }
        if (filtrosSerializadosArray.length > 0) {
            /** @type {?} */
            var filtrosSerializados = this.tratarArray(filtrosSerializadosArray, filtroCompostoOperador);
            return filtrosSerializados;
        }
        else {
            return null;
        }
    };
    /**
     * @param {?} filtro
     * @return {?}
     */
    BaseSerializerService.prototype.serializeFiltro = /**
     * @param {?} filtro
     * @return {?}
     */
    function (filtro) {
        /** @type {?} */
        var filtroOperador = this.getFiltroOperadorMapeado(filtro.operador);
        /** @type {?} */
        var campo = this.filtroCampoInterceptor(filtro);
        /** @type {?} */
        var valor = this.filtroValorInterceptor(filtro);
        /** @type {?} */
        var filtroSerializado = this.format(filtroOperador, { campo: campo, valor: valor });
        return filtroSerializado;
    };
    /**
     * @protected
     * @param {?} stringArray
     * @param {?=} join
     * @return {?}
     */
    BaseSerializerService.prototype.tratarArray = /**
     * @protected
     * @param {?} stringArray
     * @param {?=} join
     * @return {?}
     */
    function (stringArray, join) {
        if (join === void 0) { join = '&'; }
        /** @type {?} */
        var filtrosSerializados = stringArray.filter((/**
         * @param {?} s
         * @return {?}
         */
        function (s) { return s !== '' && s != null; })).join(join);
        return filtrosSerializados;
    };
    /**
     * @protected
     * @param {?} filtro
     * @return {?}
     */
    BaseSerializerService.prototype.filtroValorInterceptor = /**
     * @protected
     * @param {?} filtro
     * @return {?}
     */
    function (filtro) {
        return filtro.valor;
    };
    /**
     * @protected
     * @param {?} filtro
     * @return {?}
     */
    BaseSerializerService.prototype.filtroCampoInterceptor = /**
     * @protected
     * @param {?} filtro
     * @return {?}
     */
    function (filtro) {
        return filtro.campo;
    };
    /**
     * @protected
     * @param {?} pesquisa
     * @return {?}
     */
    BaseSerializerService.prototype.pesquisaTermoInterceptor = /**
     * @protected
     * @param {?} pesquisa
     * @return {?}
     */
    function (pesquisa) {
        return pesquisa.termo;
    };
    Object.defineProperty(BaseSerializerService.prototype, "pesquisaModoPadrao", {
        get: /**
         * @protected
         * @return {?}
         */
        function () {
            if (this.suportaPesquisa) {
                return SearchMode.FullTextSearch;
            }
            else {
                if (this.suportaFiltros) {
                    return SearchMode.Filtro;
                }
            }
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} parametrosDaRequisicao
     * @return {?}
     */
    BaseSerializerService.prototype.serializePesquisa = /**
     * @param {?} parametrosDaRequisicao
     * @return {?}
     */
    function (parametrosDaRequisicao) {
        /** @type {?} */
        var termo = this.pesquisaTermoInterceptor(parametrosDaRequisicao.pesquisa);
        if (parametrosDaRequisicao.pesquisa.config == null) {
            parametrosDaRequisicao.pesquisa.config = { modo: this.pesquisaModoPadrao };
        }
        if (parametrosDaRequisicao.pesquisa.config.modo == null) {
            // Se não informar as configurações, deve prevalecer o que o serializador definiu como padrao
            parametrosDaRequisicao.pesquisa.config.modo = this.pesquisaModoPadrao;
        }
        if ((this.suportaFiltros && !this.suportaPesquisa) &&
            (parametrosDaRequisicao.pesquisa.config.modo === SearchMode.Filtro || this.pesquisaClausulaFormato == null)) {
            // Este é o caso quando a pesquisa é realizada por meio de filtro simples
            if (parametrosDaRequisicao.pesquisa.config.filtro.campos == null ||
                parametrosDaRequisicao.pesquisa.config.filtro.campos.length <= 0) {
                throw new Error('Na pesquisa com modo Filtro, os campos devem ser informados!');
            }
            /** @type {?} */
            var operador_1 = parametrosDaRequisicao.pesquisa.config.filtro.operador ?
                parametrosDaRequisicao.pesquisa.config.filtro.operador : this.pesquisaFiltroOperadorPadrao;
            /** @type {?} */
            var filtros = parametrosDaRequisicao.pesquisa.config.filtro.campos.map((/**
             * @param {?} campo
             * @return {?}
             */
            function (campo) {
                /** @type {?} */
                var filtro = (/** @type {?} */ ({ campo: campo, operador: operador_1, valor: termo }));
                return filtro;
            }));
            /** @type {?} */
            var filtroComposto = (/** @type {?} */ ({ operador: this.pesquisaFiltroCompostoOperadorPadrao, filtros: filtros }));
            parametrosDaRequisicao.filtros.filtros.push(filtroComposto);
            return null;
        }
        else {
            if (!this.suportaPesquisa) {
                throw new Error('Este serializador não suporta pesquisa!');
            }
            /** @type {?} */
            var clausula = this.pesquisaClausulaFormato;
            /** @type {?} */
            var clausulaSerializada = this.format(clausula, { pesquisa: termo });
            return clausulaSerializada;
        }
    };
    /**
     * @param {?} parametros
     * @return {?}
     */
    BaseSerializerService.prototype.serialize = /**
     * @param {?} parametros
     * @return {?}
     */
    function (parametros) {
        /** @type {?} */
        var serializaveis = [];
        if (parametros.pesquisa) {
            /** @type {?} */
            var serializacaoDaPesquisa = this.serializePesquisa(parametros);
            serializaveis.push(serializacaoDaPesquisa);
        }
        if (parametros.filtros) {
            /** @type {?} */
            var serializacaoDosFiltros = this.serializeFiltros(parametros);
            serializaveis.push(serializacaoDosFiltros);
        }
        if (parametros.paginacao) {
            /** @type {?} */
            var serializacaoDaPaginacao = this.serializePaginacao(parametros);
            serializaveis.push(serializacaoDaPaginacao);
        }
        if (parametros.ordenacoes) {
            /** @type {?} */
            var serializacaoDaOrdenacao = this.serializeOrdenacao(parametros);
            serializaveis.push(serializacaoDaOrdenacao);
        }
        if (this.serializeInterceptor) {
            /** @type {?} */
            var parametroSerializado = this.serializeInterceptor(parametros);
            serializaveis.push(parametroSerializado);
        }
        /** @type {?} */
        var parametrosSerializados = this.tratarArray(serializaveis);
        return parametrosSerializados;
    };
    /**
     * @param {?} parametrosDaRequisicao
     * @return {?}
     */
    BaseSerializerService.prototype.serializePaginacao = /**
     * @param {?} parametrosDaRequisicao
     * @return {?}
     */
    function (parametrosDaRequisicao) {
        if (!this.suportaPaginacao) {
            throw new Error('Este serializador não suporta paginacao!');
        }
        /** @type {?} */
        var serializaveis = [];
        /** @type {?} */
        var paginacaoPaginaClausulaFormato = this.paginacaoPaginaClausulaFormato;
        /** @type {?} */
        var paginacaoPaginaParaFormatar = {
            pagina: this.paginacaoPaginaInterceptor(parametrosDaRequisicao.paginacao),
        };
        /** @type {?} */
        var paginacaoPaginaSerializado = this.format(paginacaoPaginaClausulaFormato, paginacaoPaginaParaFormatar);
        serializaveis.push(paginacaoPaginaSerializado);
        /** @type {?} */
        var paginacaoItensPorPaginaClausulaFormato = this.paginacaoItensPorPaginaClausulaFormato;
        /** @type {?} */
        var paginacaoItensPorPaginaParaFormatar = {
            itensPorPagina: this.paginacaoItensPorPaginaInterceptor(parametrosDaRequisicao.paginacao),
        };
        /** @type {?} */
        var paginacaoItensPorPaginaSerializado = this.format(paginacaoItensPorPaginaClausulaFormato, paginacaoItensPorPaginaParaFormatar);
        serializaveis.push(paginacaoItensPorPaginaSerializado);
        /** @type {?} */
        var parametrosSerializados = this.tratarArray(serializaveis);
        return parametrosSerializados;
    };
    /**
     * @protected
     * @param {?} paginacao
     * @return {?}
     */
    BaseSerializerService.prototype.paginacaoPaginaInterceptor = /**
     * @protected
     * @param {?} paginacao
     * @return {?}
     */
    function (paginacao) {
        return paginacao.pagina;
    };
    /**
     * @protected
     * @param {?} paginacao
     * @return {?}
     */
    BaseSerializerService.prototype.paginacaoItensPorPaginaInterceptor = /**
     * @protected
     * @param {?} paginacao
     * @return {?}
     */
    function (paginacao) {
        return paginacao.itensPorPagina;
    };
    /**
     * @param {?} parametrosDaRequisicao
     * @return {?}
     */
    BaseSerializerService.prototype.serializeOrdenacao = /**
     * @param {?} parametrosDaRequisicao
     * @return {?}
     */
    function (parametrosDaRequisicao) {
        var _this = this;
        if (!this.suportaOrdenacao) {
            throw new Error('Este serializador não suporta ordenacao!');
        }
        // Name asc, Description desc
        /** @type {?} */
        var paginacaoArray = parametrosDaRequisicao.ordenacoes.map((/**
         * @param {?} ordenacao
         * @return {?}
         */
        function (ordenacao) {
            /** @type {?} */
            var ordenacaoFormato = _this.ordenacaoFormato;
            // `{campo} {direcao}`;
            /** @type {?} */
            var ordenacaoParaFormatar = (/** @type {?} */ ({
                campo: _this.ordenacaoCampoInterceptor(ordenacao),
                direcao: _this.ordenacaoDirecaoInterceptor(ordenacao),
            }));
            /** @type {?} */
            var ordenacoesSerializadas = _this.format(ordenacaoFormato, ordenacaoParaFormatar);
            return ordenacoesSerializadas;
        }));
        /** @type {?} */
        var ordenacoesSeparador = this.ordenacoesSeparador;
        /** @type {?} */
        var parametrosSerializados = this.tratarArray(paginacaoArray, ordenacoesSeparador);
        /** @type {?} */
        var ordenacaoClausula = this.ordenacaoClausulaFormato;
        // `$orderby={ordenacoes}`;
        /** @type {?} */
        var clausulaFormatada = this.format(ordenacaoClausula, { ordenacoes: parametrosSerializados });
        return clausulaFormatada;
    };
    /**
     * @protected
     * @param {?} ordenacao
     * @return {?}
     */
    BaseSerializerService.prototype.ordenacaoCampoInterceptor = /**
     * @protected
     * @param {?} ordenacao
     * @return {?}
     */
    function (ordenacao) {
        return ordenacao.campo;
    };
    /**
     * @protected
     * @param {?} ordenacao
     * @return {?}
     */
    BaseSerializerService.prototype.ordenacaoDirecaoInterceptor = /**
     * @protected
     * @param {?} ordenacao
     * @return {?}
     */
    function (ordenacao) {
        return ordenacao.direcao;
    };
    /**
     * @protected
     * @param {?} formato
     * @param {...?} args
     * @return {?}
     */
    BaseSerializerService.prototype.format = /**
     * @protected
     * @param {?} formato
     * @param {...?} args
     * @return {?}
     */
    function (formato) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        if (typeof args[0] !== 'object') {
            return formato.replace(/{\d+}/g, (/**
             * @param {?} m
             * @return {?}
             */
            function (m) {
                /** @type {?} */
                var index = Number(m.replace(/\D/g, ''));
                return (args[index] ? args[index] : m);
            }));
        }
        else {
            /** @type {?} */
            var obj_1 = args[0];
            return formato.replace(/{\w+}/g, (/**
             * @param {?} m
             * @return {?}
             */
            function (m) {
                /** @type {?} */
                var key = m.replace(/{|}/g, '');
                return (obj_1.hasOwnProperty(key) ? obj_1[key] : m);
            }));
        }
    };
    return BaseSerializerService;
}());
/**
 * @abstract
 */
export { BaseSerializerService };
if (false) {
    /**
     * Formato que sera serializado a
     * Tag correspondente a query string
     * enviada na requizição da pesquisa.
     *
     * A palavra chave para substituição pelo filtro é `{filtros}`
     *
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.filtroClausulaFormato;
    /**
     *  Corresponde a tag que sera aplicada na formatação dos filtros.
     *
     *  A palavra chave para substituição pelo filtro é `{filtro}`
     *
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.filtroFormato;
    /**
     * Habilita suporte para modo de pesquisa.
     *
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.suportaPesquisa;
    /**
     * Habilita suporte para modo de pesquisa utilizando a lógica com filtros.
     *
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.suportaFiltros;
    /**
     *  Habilita suporte para modo de pesquisa utilizando a lógica com paginação.
     *
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.suportaPaginacao;
    /**
     * Habilita suporte para modo de pesquisa utilizando a lógica com ordenação.
     *
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.suportaOrdenacao;
    /**
     * Formato que sera serializado a
     * Tag correspondente a query string 'skip'
     * enviada na requisição.
     *
     * A palavra chave para substituição é `{pagina}`
     *
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.paginacaoPaginaClausulaFormato;
    /**
     *  Corresponde a tag que sera aplicada na formatação das clausula dos itens por página.
     *
     *  A palavra chave para substituição é `{itensPorPagina}`
     *
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.paginacaoItensPorPaginaClausulaFormato;
    /**
     * Formato que sera serializado a
     * Tag correspondente a query string
     * enviada na requizição da ordernação da pesquisa.
     *
     * A palavra chave para substituição é `{ordenacoes}`
     *
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.ordenacaoClausulaFormato;
    /**
     *  Corresponde a tag que sera aplicada na formatação das clausula de ordenação.
     *
     *  As palavras chaves para substituição é `{campo}` e `{direcao}`
     *
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.ordenacaoFormato;
    /**
     *  Corresponde ao separador que sera aplicado na serialização das ordenações.
     *
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.ordenacoesSeparador;
    /**
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.quantidadeDeFiltrosAlinhadosSuportados;
    /**
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.pesquisaClausulaFormato;
    /**
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.pesquisaFiltroOperadorPadrao;
    /**
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.pesquisaFiltroCompostoOperadorPadrao;
    /**
     * @abstract
     * @protected
     * @param {?} filtroOperador
     * @return {?}
     */
    BaseSerializerService.prototype.getFiltroOperadorMapeado = function (filtroOperador) { };
    /**
     * @abstract
     * @protected
     * @param {?} FiltroCompostoOperador
     * @return {?}
     */
    BaseSerializerService.prototype.getFiltroCompostoOperadorMapeado = function (FiltroCompostoOperador) { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFzZS1zZXJpYWxpemVyLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9maWx0ZXJzL3NlcmlhbGl6ZXIvYmFzZS1zZXJpYWxpemVyLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxjQUFjLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDN0MsT0FBTyxFQUF5QixjQUFjLEVBQUUsaUJBQWlCLEVBQUUsc0JBQXNCLEVBQUMsTUFBTSxXQUFXLENBQUM7QUFLNUcsT0FBTyxFQUFDLFVBQVUsRUFBVyxNQUFNLGFBQWEsQ0FBQzs7QUFFakQsTUFBTSxLQUFPLGdCQUFnQixHQUFHLElBQUksY0FBYyxDQUFhLFlBQVksQ0FBQzs7OztBQUU1RTs7OztJQUFBO1FBcUVZLDJDQUFzQyxHQUFHLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQztRQXNFakUsaUNBQTRCLEdBQW1CLGNBQWMsQ0FBQyxNQUFNLENBQUM7UUFDckUseUNBQW9DLEdBQTJCLHNCQUFzQixDQUFDLEVBQUUsQ0FBQztJQWlLckcsQ0FBQzs7Ozs7SUF0T1EsZ0RBQWdCOzs7O0lBQXZCLFVBQXdCLHNCQUE4QztRQUNwRSxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRTtZQUN4QixNQUFNLElBQUksS0FBSyxDQUFDLHdDQUF3QyxDQUFDLENBQUM7U0FDM0Q7O1lBQ0ssaUJBQWlCLEdBQUcsSUFBSSxDQUFDLHFCQUFxQjs7WUFDOUMsMEJBQTBCLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDLHNCQUFzQixDQUFDLE9BQU8sQ0FBQztRQUMvRixJQUFJLDBCQUEwQixJQUFJLElBQUksRUFBRTs7Z0JBQ2hDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsaUJBQWlCLEVBQUUsRUFBQyxPQUFPLEVBQUUsMEJBQTBCLEVBQUMsQ0FBQztZQUNoRyxPQUFPLGtCQUFrQixDQUFDO1NBQzNCO2FBQU07WUFDTCxPQUFPLElBQUksQ0FBQztTQUNiO0lBQ0gsQ0FBQzs7Ozs7O0lBRU0sdURBQXVCOzs7OztJQUE5QixVQUErQixjQUE4QixFQUFFLEtBQVM7UUFBeEUsaUJBeUJDO1FBekI4RCxzQkFBQSxFQUFBLFNBQVM7UUFDdEUsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLHNDQUFzQyxFQUFFO1lBQ3ZELE1BQU0sSUFBSSxLQUFLLENBQUMsMERBQW1ELElBQUksQ0FBQyxzQ0FBc0MsNkNBQThCLEtBQUssT0FBSSxDQUFDLENBQUM7U0FDeEo7O1lBQ0ssd0JBQXdCLEdBQUcsY0FBYyxDQUFDLE9BQU8sQ0FBQyxHQUFHOzs7O1FBQUMsVUFBQSxNQUFNO1lBQ2hFLElBQUksaUJBQWlCLENBQUMsTUFBTSxDQUFDLEVBQUU7O29CQUN2QixpQkFBaUIsR0FBRyxLQUFJLENBQUMsYUFBYTs7b0JBQ3RDLFlBQVksR0FBRyxLQUFLLEdBQUcsQ0FBQzs7b0JBQ3hCLDBCQUEwQixHQUFHLEtBQUksQ0FBQyx1QkFBdUIsQ0FBQyxNQUFNLEVBQUUsWUFBWSxDQUFDOztvQkFDL0Usa0JBQWtCLEdBQUcsS0FBSSxDQUFDLE1BQU0sQ0FBQyxpQkFBaUIsRUFBRSxFQUFDLE1BQU0sRUFBRSwwQkFBMEIsRUFBQyxDQUFDO2dCQUMvRixPQUFPLGtCQUFrQixDQUFDO2FBQzNCO2lCQUFNO2dCQUNMLE9BQU8sS0FBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUNyQztRQUNILENBQUMsRUFBQzs7WUFDRSxzQkFBc0IsR0FBRyxJQUFJO1FBQ2pDLElBQUksd0JBQXdCLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUN2QyxzQkFBc0IsR0FBRyxJQUFJLENBQUMsZ0NBQWdDLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1NBQ3pGO1FBQ0QsSUFBSSx3QkFBd0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFOztnQkFDakMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyx3QkFBd0IsRUFBRSxzQkFBc0IsQ0FBQztZQUM5RixPQUFPLG1CQUFtQixDQUFDO1NBQzVCO2FBQU07WUFDTCxPQUFPLElBQUksQ0FBQztTQUNiO0lBQ0gsQ0FBQzs7Ozs7SUFFTSwrQ0FBZTs7OztJQUF0QixVQUF1QixNQUFjOztZQUM3QixjQUFjLEdBQUcsSUFBSSxDQUFDLHdCQUF3QixDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUM7O1lBQy9ELEtBQUssR0FBRyxJQUFJLENBQUMsc0JBQXNCLENBQUMsTUFBTSxDQUFDOztZQUMzQyxLQUFLLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixDQUFDLE1BQU0sQ0FBQzs7WUFDM0MsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxjQUFjLEVBQUUsRUFBQyxLQUFLLE9BQUEsRUFBRSxLQUFLLE9BQUEsRUFBQyxDQUFDO1FBQ3JFLE9BQU8saUJBQWlCLENBQUM7SUFDM0IsQ0FBQzs7Ozs7OztJQUVTLDJDQUFXOzs7Ozs7SUFBckIsVUFBc0IsV0FBcUIsRUFBRSxJQUFVO1FBQVYscUJBQUEsRUFBQSxVQUFVOztZQUMvQyxtQkFBbUIsR0FBRyxXQUFXLENBQUMsTUFBTTs7OztRQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLElBQUksSUFBSSxFQUFyQixDQUFxQixFQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztRQUNyRixPQUFPLG1CQUFtQixDQUFDO0lBQzdCLENBQUM7Ozs7OztJQUVTLHNEQUFzQjs7Ozs7SUFBaEMsVUFBaUMsTUFBYztRQUM3QyxPQUFPLE1BQU0sQ0FBQyxLQUFLLENBQUM7SUFDdEIsQ0FBQzs7Ozs7O0lBRVMsc0RBQXNCOzs7OztJQUFoQyxVQUFpQyxNQUFjO1FBQzdDLE9BQU8sTUFBTSxDQUFDLEtBQUssQ0FBQztJQUN0QixDQUFDOzs7Ozs7SUFJUyx3REFBd0I7Ozs7O0lBQWxDLFVBQW1DLFFBQWtCO1FBQ25ELE9BQU8sUUFBUSxDQUFDLEtBQUssQ0FBQztJQUN4QixDQUFDO0lBS0Qsc0JBQWMscURBQWtCOzs7OztRQUFoQztZQUNFLElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRTtnQkFDeEIsT0FBTyxVQUFVLENBQUMsY0FBYyxDQUFDO2FBQ2xDO2lCQUFNO2dCQUNMLElBQUksSUFBSSxDQUFDLGNBQWMsRUFBRTtvQkFDdkIsT0FBTyxVQUFVLENBQUMsTUFBTSxDQUFDO2lCQUMxQjthQUNGO1FBQ0gsQ0FBQzs7O09BQUE7Ozs7O0lBRU0saURBQWlCOzs7O0lBQXhCLFVBQXlCLHNCQUE4Qzs7WUFDL0QsS0FBSyxHQUFHLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxzQkFBc0IsQ0FBQyxRQUFRLENBQUM7UUFDNUUsSUFBSSxzQkFBc0IsQ0FBQyxRQUFRLENBQUMsTUFBTSxJQUFJLElBQUksRUFBRTtZQUNsRCxzQkFBc0IsQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLEVBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxrQkFBa0IsRUFBQyxDQUFDO1NBQzFFO1FBQ0QsSUFBSSxzQkFBc0IsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLElBQUksSUFBSSxJQUFJLEVBQUU7WUFDdkQsNkZBQTZGO1lBQzdGLHNCQUFzQixDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQztTQUN2RTtRQUNELElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQztZQUNoRCxDQUFDLHNCQUFzQixDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsSUFBSSxLQUFLLFVBQVUsQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLHVCQUF1QixJQUFJLElBQUksQ0FBQyxFQUFFO1lBQzdHLHlFQUF5RTtZQUN6RSxJQUFJLHNCQUFzQixDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sSUFBSSxJQUFJO2dCQUM5RCxzQkFBc0IsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTtnQkFDbEUsTUFBTSxJQUFJLEtBQUssQ0FBQyw4REFBOEQsQ0FBQyxDQUFDO2FBQ2pGOztnQkFDSyxVQUFRLEdBQUcsc0JBQXNCLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3ZFLHNCQUFzQixDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLDRCQUE0Qjs7Z0JBQ3RGLE9BQU8sR0FBRyxzQkFBc0IsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBRzs7OztZQUFDLFVBQUEsS0FBSzs7b0JBQ3RFLE1BQU0sR0FBRyxtQkFBQSxFQUFDLEtBQUssT0FBQSxFQUFFLFFBQVEsWUFBQSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUMsRUFBVTtnQkFDeEQsT0FBTyxNQUFNLENBQUM7WUFDaEIsQ0FBQyxFQUFDOztnQkFDSSxjQUFjLEdBQUcsbUJBQUEsRUFBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLG9DQUFvQyxFQUFFLE9BQU8sU0FBQSxFQUFDLEVBQWtCO1lBQ3ZHLHNCQUFzQixDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBQzVELE9BQU8sSUFBSSxDQUFDO1NBQ2I7YUFBTTtZQUNMLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFFO2dCQUN6QixNQUFNLElBQUksS0FBSyxDQUFDLHlDQUF5QyxDQUFDLENBQUM7YUFDNUQ7O2dCQUNLLFFBQVEsR0FBRyxJQUFJLENBQUMsdUJBQXVCOztnQkFDdkMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsRUFBQyxRQUFRLEVBQUUsS0FBSyxFQUFDLENBQUM7WUFDcEUsT0FBTyxtQkFBbUIsQ0FBQztTQUM1QjtJQUNILENBQUM7Ozs7O0lBRUQseUNBQVM7Ozs7SUFBVCxVQUFVLFVBQWtDOztZQUNwQyxhQUFhLEdBQWEsRUFBRTtRQUVsQyxJQUFJLFVBQVUsQ0FBQyxRQUFRLEVBQUU7O2dCQUNqQixzQkFBc0IsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDO1lBQ2pFLGFBQWEsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsQ0FBQztTQUM1QztRQUVELElBQUksVUFBVSxDQUFDLE9BQU8sRUFBRTs7Z0JBQ2hCLHNCQUFzQixHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLENBQUM7WUFDaEUsYUFBYSxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO1NBQzVDO1FBRUQsSUFBSSxVQUFVLENBQUMsU0FBUyxFQUFFOztnQkFDbEIsdUJBQXVCLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFVBQVUsQ0FBQztZQUNuRSxhQUFhLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLENBQUM7U0FDN0M7UUFFRCxJQUFJLFVBQVUsQ0FBQyxVQUFVLEVBQUU7O2dCQUNuQix1QkFBdUIsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsVUFBVSxDQUFDO1lBQ25FLGFBQWEsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsQ0FBQztTQUM3QztRQUVELElBQUksSUFBSSxDQUFDLG9CQUFvQixFQUFFOztnQkFDdkIsb0JBQW9CLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFVBQVUsQ0FBQztZQUNsRSxhQUFhLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUM7U0FDMUM7O1lBRUssc0JBQXNCLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUM7UUFDOUQsT0FBTyxzQkFBc0IsQ0FBQztJQUNoQyxDQUFDOzs7OztJQUlELGtEQUFrQjs7OztJQUFsQixVQUFtQixzQkFBOEM7UUFDL0QsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtZQUMxQixNQUFNLElBQUksS0FBSyxDQUFDLDBDQUEwQyxDQUFDLENBQUM7U0FDN0Q7O1lBRUssYUFBYSxHQUFhLEVBQUU7O1lBRTVCLDhCQUE4QixHQUFHLElBQUksQ0FBQyw4QkFBOEI7O1lBQ3BFLDJCQUEyQixHQUFHO1lBQ2xDLE1BQU0sRUFBRSxJQUFJLENBQUMsMEJBQTBCLENBQUMsc0JBQXNCLENBQUMsU0FBUyxDQUFDO1NBQzFFOztZQUNLLDBCQUEwQixHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsOEJBQThCLEVBQUUsMkJBQTJCLENBQUM7UUFDM0csYUFBYSxDQUFDLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxDQUFDOztZQUV6QyxzQ0FBc0MsR0FBRyxJQUFJLENBQUMsc0NBQXNDOztZQUNwRixtQ0FBbUMsR0FBRztZQUMxQyxjQUFjLEVBQUUsSUFBSSxDQUFDLGtDQUFrQyxDQUFDLHNCQUFzQixDQUFDLFNBQVMsQ0FBQztTQUMxRjs7WUFDSyxrQ0FBa0MsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLHNDQUFzQyxFQUFFLG1DQUFtQyxDQUFDO1FBQ25JLGFBQWEsQ0FBQyxJQUFJLENBQUMsa0NBQWtDLENBQUMsQ0FBQzs7WUFFakQsc0JBQXNCLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUM7UUFDOUQsT0FBTyxzQkFBc0IsQ0FBQztJQUNoQyxDQUFDOzs7Ozs7SUFFUywwREFBMEI7Ozs7O0lBQXBDLFVBQXFDLFNBQW9CO1FBQ3ZELE9BQU8sU0FBUyxDQUFDLE1BQU0sQ0FBQztJQUMxQixDQUFDOzs7Ozs7SUFFUyxrRUFBa0M7Ozs7O0lBQTVDLFVBQTZDLFNBQW9CO1FBQy9ELE9BQU8sU0FBUyxDQUFDLGNBQWMsQ0FBQztJQUNsQyxDQUFDOzs7OztJQUVELGtEQUFrQjs7OztJQUFsQixVQUFtQixzQkFBOEM7UUFBakUsaUJBc0JDO1FBckJDLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7WUFDMUIsTUFBTSxJQUFJLEtBQUssQ0FBQywwQ0FBMEMsQ0FBQyxDQUFDO1NBQzdEOzs7WUFFSyxjQUFjLEdBQUcsc0JBQXNCLENBQUMsVUFBVSxDQUFDLEdBQUc7Ozs7UUFBQyxVQUFBLFNBQVM7O2dCQUM5RCxnQkFBZ0IsR0FBRyxLQUFJLENBQUMsZ0JBQWdCOzs7Z0JBQ3hDLHFCQUFxQixHQUFHLG1CQUFBO2dCQUM1QixLQUFLLEVBQUUsS0FBSSxDQUFDLHlCQUF5QixDQUFDLFNBQVMsQ0FBQztnQkFDaEQsT0FBTyxFQUFFLEtBQUksQ0FBQywyQkFBMkIsQ0FBQyxTQUFTLENBQUM7YUFDckQsRUFBYTs7Z0JBQ1Isc0JBQXNCLEdBQUcsS0FBSSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsRUFBRSxxQkFBcUIsQ0FBQztZQUNuRixPQUFPLHNCQUFzQixDQUFDO1FBQ2hDLENBQUMsRUFBQzs7WUFFSSxtQkFBbUIsR0FBRyxJQUFJLENBQUMsbUJBQW1COztZQUM5QyxzQkFBc0IsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsRUFBRSxtQkFBbUIsQ0FBQzs7WUFFOUUsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLHdCQUF3Qjs7O1lBQ2pELGlCQUFpQixHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsaUJBQWlCLEVBQUUsRUFBQyxVQUFVLEVBQUUsc0JBQXNCLEVBQUMsQ0FBQztRQUU5RixPQUFPLGlCQUFpQixDQUFDO0lBQzNCLENBQUM7Ozs7OztJQUVTLHlEQUF5Qjs7Ozs7SUFBbkMsVUFBb0MsU0FBb0I7UUFDdEQsT0FBTyxTQUFTLENBQUMsS0FBSyxDQUFDO0lBQ3pCLENBQUM7Ozs7OztJQUVTLDJEQUEyQjs7Ozs7SUFBckMsVUFBc0MsU0FBb0I7UUFDeEQsT0FBTyxTQUFTLENBQUMsT0FBTyxDQUFDO0lBQzNCLENBQUM7Ozs7Ozs7SUFFUyxzQ0FBTTs7Ozs7O0lBQWhCLFVBQWlCLE9BQWU7UUFBRSxjQUFZO2FBQVosVUFBWSxFQUFaLHFCQUFZLEVBQVosSUFBWTtZQUFaLDZCQUFZOztRQUM1QyxJQUFJLE9BQU8sSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLFFBQVEsRUFBRTtZQUMvQixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsUUFBUTs7OztZQUFFLFVBQUMsQ0FBQzs7b0JBQzNCLEtBQUssR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLENBQUM7Z0JBQzFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDekMsQ0FBQyxFQUFDLENBQUM7U0FDSjthQUFNOztnQkFDQyxLQUFHLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUVuQixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsUUFBUTs7OztZQUFFLFVBQUMsQ0FBQzs7b0JBQzNCLEdBQUcsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxFQUFFLENBQUM7Z0JBQ2pDLE9BQU8sQ0FBQyxLQUFHLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2xELENBQUMsRUFBQyxDQUFDO1NBQ0o7SUFDSCxDQUFDO0lBQ0gsNEJBQUM7QUFBRCxDQUFDLEFBN1NELElBNlNDOzs7Ozs7Ozs7Ozs7Ozs7O0lBalNDLHNEQUFpRDs7Ozs7Ozs7O0lBTWpELDhDQUF5Qzs7Ozs7OztJQUt6QyxnREFBNEM7Ozs7Ozs7SUFJNUMsK0NBQTJDOzs7Ozs7O0lBSTNDLGlEQUE2Qzs7Ozs7OztJQUk3QyxpREFBNkM7Ozs7Ozs7Ozs7O0lBUTdDLCtEQUEwRDs7Ozs7Ozs7O0lBTTFELHVFQUFrRTs7Ozs7Ozs7Ozs7SUFRbEUseURBQW9EOzs7Ozs7Ozs7SUFNcEQsaURBQTRDOzs7Ozs7O0lBSTVDLG9EQUErQzs7Ozs7SUFFL0MsdUVBQTJFOzs7OztJQWdFM0Usd0RBQTJDOzs7OztJQU0zQyw2REFBK0U7Ozs7O0lBQy9FLHFFQUFtRzs7Ozs7OztJQTNJbkcseUZBQW9GOzs7Ozs7O0lBRXBGLHlHQUE0RyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SW5qZWN0aW9uVG9rZW59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtGaWx0cm9Db21wb3N0bywgRmlsdHJvLCBGaWx0cm9PcGVyYWRvciwgaXNGaWx0ZXJDb21wb3NpdGUsIEZpbHRyb0NvbXBvc3RvT3BlcmFkb3J9IGZyb20gJy4uL2ZpbHRlcic7XG5pbXBvcnQge1BhcmFtZXRyb3NEYVJlcXVpc2ljYW99IGZyb20gJy4uL3JlcXVlc3QtcGFyYW1ldGVycyc7XG5pbXBvcnQge1NlcmlhbGl6ZXJ9IGZyb20gJy4vc2VyaWFsaXplcic7XG5pbXBvcnQge09yZGVuYWNhb30gZnJvbSAnLi4vc29ydCc7XG5pbXBvcnQge1BhZ2luYWNhb30gZnJvbSAnLi4vcGFnZSc7XG5pbXBvcnQge1NlYXJjaE1vZGUsIFBlc3F1aXNhfSBmcm9tICcuLi9wZXNxdWlzYSc7XG5cbmV4cG9ydCBjb25zdCBTRVJJQUxJWkVSX1RPS0VOID0gbmV3IEluamVjdGlvblRva2VuPFNlcmlhbGl6ZXI+KCdTZXJpYWxpemVyJyk7XG5cbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBCYXNlU2VyaWFsaXplclNlcnZpY2UgaW1wbGVtZW50cyBTZXJpYWxpemVyIHtcbiAgcHJvdGVjdGVkIGFic3RyYWN0IGdldEZpbHRyb09wZXJhZG9yTWFwZWFkbyhmaWx0cm9PcGVyYWRvcjogRmlsdHJvT3BlcmFkb3IpOiBzdHJpbmc7XG5cbiAgcHJvdGVjdGVkIGFic3RyYWN0IGdldEZpbHRyb0NvbXBvc3RvT3BlcmFkb3JNYXBlYWRvKEZpbHRyb0NvbXBvc3RvT3BlcmFkb3I6IEZpbHRyb0NvbXBvc3RvT3BlcmFkb3IpOiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIEZvcm1hdG8gcXVlIHNlcmEgc2VyaWFsaXphZG8gYVxuICAgKiBUYWcgY29ycmVzcG9uZGVudGUgYSBxdWVyeSBzdHJpbmdcbiAgICogZW52aWFkYSBuYSByZXF1aXppw6fDo28gZGEgcGVzcXVpc2EuXG4gICAqXG4gICAqIEEgcGFsYXZyYSBjaGF2ZSBwYXJhIHN1YnN0aXR1acOnw6NvIHBlbG8gZmlsdHJvIMOpIGB7ZmlsdHJvc31gXG4gICAqICovXG4gIHByb3RlY3RlZCBhYnN0cmFjdCBmaWx0cm9DbGF1c3VsYUZvcm1hdG86IHN0cmluZztcbiAgLyoqXG4gICAqICBDb3JyZXNwb25kZSBhIHRhZyBxdWUgc2VyYSBhcGxpY2FkYSBuYSBmb3JtYXRhw6fDo28gZG9zIGZpbHRyb3MuXG4gICAqXG4gICAqICBBIHBhbGF2cmEgY2hhdmUgcGFyYSBzdWJzdGl0dWnDp8OjbyBwZWxvIGZpbHRybyDDqSBge2ZpbHRyb31gXG4gICAqICovXG4gIHByb3RlY3RlZCBhYnN0cmFjdCBmaWx0cm9Gb3JtYXRvOiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIEhhYmlsaXRhIHN1cG9ydGUgcGFyYSBtb2RvIGRlIHBlc3F1aXNhLlxuICAgKiAqL1xuICBwcm90ZWN0ZWQgYWJzdHJhY3Qgc3Vwb3J0YVBlc3F1aXNhOiBib29sZWFuO1xuICAvKipcbiAgICogSGFiaWxpdGEgc3Vwb3J0ZSBwYXJhIG1vZG8gZGUgcGVzcXVpc2EgdXRpbGl6YW5kbyBhIGzDs2dpY2EgY29tIGZpbHRyb3MuXG4gICAqICovXG4gIHByb3RlY3RlZCBhYnN0cmFjdCBzdXBvcnRhRmlsdHJvczogYm9vbGVhbjtcbiAgLyoqXG4gICAqICBIYWJpbGl0YSBzdXBvcnRlIHBhcmEgbW9kbyBkZSBwZXNxdWlzYSB1dGlsaXphbmRvIGEgbMOzZ2ljYSBjb20gcGFnaW5hw6fDo28uXG4gICAqICovXG4gIHByb3RlY3RlZCBhYnN0cmFjdCBzdXBvcnRhUGFnaW5hY2FvOiBib29sZWFuO1xuICAvKipcbiAgICogSGFiaWxpdGEgc3Vwb3J0ZSBwYXJhIG1vZG8gZGUgcGVzcXVpc2EgdXRpbGl6YW5kbyBhIGzDs2dpY2EgY29tIG9yZGVuYcOnw6NvLlxuICAgKiAqL1xuICBwcm90ZWN0ZWQgYWJzdHJhY3Qgc3Vwb3J0YU9yZGVuYWNhbzogYm9vbGVhbjtcbiAgLyoqXG4gICAqIEZvcm1hdG8gcXVlIHNlcmEgc2VyaWFsaXphZG8gYVxuICAgKiBUYWcgY29ycmVzcG9uZGVudGUgYSBxdWVyeSBzdHJpbmcgJ3NraXAnXG4gICAqIGVudmlhZGEgbmEgcmVxdWlzacOnw6NvLlxuICAgKlxuICAgKiBBIHBhbGF2cmEgY2hhdmUgcGFyYSBzdWJzdGl0dWnDp8OjbyDDqSBge3BhZ2luYX1gXG4gICAqICovXG4gIHByb3RlY3RlZCBhYnN0cmFjdCBwYWdpbmFjYW9QYWdpbmFDbGF1c3VsYUZvcm1hdG86IHN0cmluZztcbiAgLyoqXG4gICAqICBDb3JyZXNwb25kZSBhIHRhZyBxdWUgc2VyYSBhcGxpY2FkYSBuYSBmb3JtYXRhw6fDo28gZGFzIGNsYXVzdWxhIGRvcyBpdGVucyBwb3IgcMOhZ2luYS5cbiAgICpcbiAgICogIEEgcGFsYXZyYSBjaGF2ZSBwYXJhIHN1YnN0aXR1acOnw6NvIMOpIGB7aXRlbnNQb3JQYWdpbmF9YFxuICAgKiAqL1xuICBwcm90ZWN0ZWQgYWJzdHJhY3QgcGFnaW5hY2FvSXRlbnNQb3JQYWdpbmFDbGF1c3VsYUZvcm1hdG86IHN0cmluZztcbiAgLyoqXG4gICAqIEZvcm1hdG8gcXVlIHNlcmEgc2VyaWFsaXphZG8gYVxuICAgKiBUYWcgY29ycmVzcG9uZGVudGUgYSBxdWVyeSBzdHJpbmdcbiAgICogZW52aWFkYSBuYSByZXF1aXppw6fDo28gZGEgb3JkZXJuYcOnw6NvIGRhIHBlc3F1aXNhLlxuICAgKlxuICAgKiBBIHBhbGF2cmEgY2hhdmUgcGFyYSBzdWJzdGl0dWnDp8OjbyDDqSBge29yZGVuYWNvZXN9YFxuICAgKiAqL1xuICBwcm90ZWN0ZWQgYWJzdHJhY3Qgb3JkZW5hY2FvQ2xhdXN1bGFGb3JtYXRvOiBzdHJpbmc7XG4gIC8qKlxuICAgKiAgQ29ycmVzcG9uZGUgYSB0YWcgcXVlIHNlcmEgYXBsaWNhZGEgbmEgZm9ybWF0YcOnw6NvIGRhcyBjbGF1c3VsYSBkZSBvcmRlbmHDp8Ojby5cbiAgICpcbiAgICogIEFzIHBhbGF2cmFzIGNoYXZlcyBwYXJhIHN1YnN0aXR1acOnw6NvIMOpIGB7Y2FtcG99YCBlIGB7ZGlyZWNhb31gXG4gICAqICovXG4gIHByb3RlY3RlZCBhYnN0cmFjdCBvcmRlbmFjYW9Gb3JtYXRvOiBzdHJpbmc7XG4gIC8qKlxuICAgKiAgQ29ycmVzcG9uZGUgYW8gc2VwYXJhZG9yIHF1ZSBzZXJhIGFwbGljYWRvIG5hIHNlcmlhbGl6YcOnw6NvIGRhcyBvcmRlbmHDp8O1ZXMuXG4gICAqICovXG4gIHByb3RlY3RlZCBhYnN0cmFjdCBvcmRlbmFjb2VzU2VwYXJhZG9yOiBzdHJpbmc7XG5cbiAgcHJvdGVjdGVkIHF1YW50aWRhZGVEZUZpbHRyb3NBbGluaGFkb3NTdXBvcnRhZG9zID0gTnVtYmVyLk1BWF9TQUZFX0lOVEVHRVI7XG5cbiAgcHVibGljIHNlcmlhbGl6ZUZpbHRyb3MocGFyYW1ldHJvc0RhUmVxdWlzaWNhbzogUGFyYW1ldHJvc0RhUmVxdWlzaWNhbyk6IHN0cmluZyB7XG4gICAgaWYgKCF0aGlzLnN1cG9ydGFGaWx0cm9zKSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ0VzdGUgc2VyaWFsaXphZG9yIG7Do28gc3Vwb3J0YSBmaWx0cm9zIScpO1xuICAgIH1cbiAgICBjb25zdCBmb3JtYXRvRGFDbGF1c3VsYSA9IHRoaXMuZmlsdHJvQ2xhdXN1bGFGb3JtYXRvO1xuICAgIGNvbnN0IGZpbHRyb3NDb21wb3N0b1NlcmlhbGl6YWRvID0gdGhpcy5zZXJpYWxpemVGaWx0cm9Db21wb3N0byhwYXJhbWV0cm9zRGFSZXF1aXNpY2FvLmZpbHRyb3MpO1xuICAgIGlmIChmaWx0cm9zQ29tcG9zdG9TZXJpYWxpemFkbyAhPSBudWxsKSB7XG4gICAgICBjb25zdCBmaWx0cm9zU2VyaWFsaXphZG8gPSB0aGlzLmZvcm1hdChmb3JtYXRvRGFDbGF1c3VsYSwge2ZpbHRyb3M6IGZpbHRyb3NDb21wb3N0b1NlcmlhbGl6YWRvfSk7XG4gICAgICByZXR1cm4gZmlsdHJvc1NlcmlhbGl6YWRvO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG4gIH1cblxuICBwdWJsaWMgc2VyaWFsaXplRmlsdHJvQ29tcG9zdG8oZmlsdHJvQ29tcG9zdG86IEZpbHRyb0NvbXBvc3RvLCBuaXZlbCA9IDEpOiBzdHJpbmcge1xuICAgIGlmIChuaXZlbCA+IHRoaXMucXVhbnRpZGFkZURlRmlsdHJvc0FsaW5oYWRvc1N1cG9ydGFkb3MpIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcihgQSBxdWFudGlkYWRlIGRlIGZpbHRyb3MgYWxpbmhhZG9zIHN1cG9ydGFkb3Mgw6kgJyR7dGhpcy5xdWFudGlkYWRlRGVGaWx0cm9zQWxpbmhhZG9zU3Vwb3J0YWRvc30nIGUgZm9pIGFsY2Fuw6dhZG8gbyBuw612ZWwgJyR7bml2ZWx9JyFgKTtcbiAgICB9XG4gICAgY29uc3QgZmlsdHJvc1NlcmlhbGl6YWRvc0FycmF5ID0gZmlsdHJvQ29tcG9zdG8uZmlsdHJvcy5tYXAoZmlsdHJvID0+IHtcbiAgICAgIGlmIChpc0ZpbHRlckNvbXBvc2l0ZShmaWx0cm8pKSB7XG4gICAgICAgIGNvbnN0IGZvcm1hdG9Eb3NGaWx0cm9zID0gdGhpcy5maWx0cm9Gb3JtYXRvO1xuICAgICAgICBjb25zdCBwcm94aW1vTml2ZWwgPSBuaXZlbCArIDE7XG4gICAgICAgIGNvbnN0IGZpbHRyb3NDb21wb3N0b1NlcmlhbGl6YWRvID0gdGhpcy5zZXJpYWxpemVGaWx0cm9Db21wb3N0byhmaWx0cm8sIHByb3hpbW9OaXZlbCk7XG4gICAgICAgIGNvbnN0IGZpbHRyb3NTZXJpYWxpemFkbyA9IHRoaXMuZm9ybWF0KGZvcm1hdG9Eb3NGaWx0cm9zLCB7ZmlsdHJvOiBmaWx0cm9zQ29tcG9zdG9TZXJpYWxpemFkb30pO1xuICAgICAgICByZXR1cm4gZmlsdHJvc1NlcmlhbGl6YWRvO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc2VyaWFsaXplRmlsdHJvKGZpbHRybyk7XG4gICAgICB9XG4gICAgfSk7XG4gICAgbGV0IGZpbHRyb0NvbXBvc3RvT3BlcmFkb3IgPSBudWxsO1xuICAgIGlmIChmaWx0cm9zU2VyaWFsaXphZG9zQXJyYXkubGVuZ3RoID4gMSkge1xuICAgICAgZmlsdHJvQ29tcG9zdG9PcGVyYWRvciA9IHRoaXMuZ2V0RmlsdHJvQ29tcG9zdG9PcGVyYWRvck1hcGVhZG8oZmlsdHJvQ29tcG9zdG8ub3BlcmFkb3IpO1xuICAgIH1cbiAgICBpZiAoZmlsdHJvc1NlcmlhbGl6YWRvc0FycmF5Lmxlbmd0aCA+IDApIHtcbiAgICAgIGNvbnN0IGZpbHRyb3NTZXJpYWxpemFkb3MgPSB0aGlzLnRyYXRhckFycmF5KGZpbHRyb3NTZXJpYWxpemFkb3NBcnJheSwgZmlsdHJvQ29tcG9zdG9PcGVyYWRvcik7XG4gICAgICByZXR1cm4gZmlsdHJvc1NlcmlhbGl6YWRvcztcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuICB9XG5cbiAgcHVibGljIHNlcmlhbGl6ZUZpbHRybyhmaWx0cm86IEZpbHRybyk6IHN0cmluZyB7XG4gICAgY29uc3QgZmlsdHJvT3BlcmFkb3IgPSB0aGlzLmdldEZpbHRyb09wZXJhZG9yTWFwZWFkbyhmaWx0cm8ub3BlcmFkb3IpO1xuICAgIGNvbnN0IGNhbXBvID0gdGhpcy5maWx0cm9DYW1wb0ludGVyY2VwdG9yKGZpbHRybyk7XG4gICAgY29uc3QgdmFsb3IgPSB0aGlzLmZpbHRyb1ZhbG9ySW50ZXJjZXB0b3IoZmlsdHJvKTtcbiAgICBjb25zdCBmaWx0cm9TZXJpYWxpemFkbyA9IHRoaXMuZm9ybWF0KGZpbHRyb09wZXJhZG9yLCB7Y2FtcG8sIHZhbG9yfSk7XG4gICAgcmV0dXJuIGZpbHRyb1NlcmlhbGl6YWRvO1xuICB9XG5cbiAgcHJvdGVjdGVkIHRyYXRhckFycmF5KHN0cmluZ0FycmF5OiBzdHJpbmdbXSwgam9pbiA9ICcmJykge1xuICAgIGNvbnN0IGZpbHRyb3NTZXJpYWxpemFkb3MgPSBzdHJpbmdBcnJheS5maWx0ZXIocyA9PiBzICE9PSAnJyAmJiBzICE9IG51bGwpLmpvaW4oam9pbik7XG4gICAgcmV0dXJuIGZpbHRyb3NTZXJpYWxpemFkb3M7XG4gIH1cblxuICBwcm90ZWN0ZWQgZmlsdHJvVmFsb3JJbnRlcmNlcHRvcihmaWx0cm86IEZpbHRybyk6IHN0cmluZyB7XG4gICAgcmV0dXJuIGZpbHRyby52YWxvcjtcbiAgfVxuXG4gIHByb3RlY3RlZCBmaWx0cm9DYW1wb0ludGVyY2VwdG9yKGZpbHRybzogRmlsdHJvKTogc3RyaW5nIHtcbiAgICByZXR1cm4gZmlsdHJvLmNhbXBvO1xuICB9XG5cbiAgcHJvdGVjdGVkIHBlc3F1aXNhQ2xhdXN1bGFGb3JtYXRvPzogc3RyaW5nO1xuXG4gIHByb3RlY3RlZCBwZXNxdWlzYVRlcm1vSW50ZXJjZXB0b3IocGVzcXVpc2E6IFBlc3F1aXNhKTogc3RyaW5nIHtcbiAgICByZXR1cm4gcGVzcXVpc2EudGVybW87XG4gIH1cblxuICBwcm90ZWN0ZWQgcGVzcXVpc2FGaWx0cm9PcGVyYWRvclBhZHJhbzogRmlsdHJvT3BlcmFkb3IgPSBGaWx0cm9PcGVyYWRvci5Db250ZW07XG4gIHByb3RlY3RlZCBwZXNxdWlzYUZpbHRyb0NvbXBvc3RvT3BlcmFkb3JQYWRyYW86IEZpbHRyb0NvbXBvc3RvT3BlcmFkb3IgPSBGaWx0cm9Db21wb3N0b09wZXJhZG9yLk91O1xuXG4gIHByb3RlY3RlZCBnZXQgcGVzcXVpc2FNb2RvUGFkcmFvKCk6IFNlYXJjaE1vZGUge1xuICAgIGlmICh0aGlzLnN1cG9ydGFQZXNxdWlzYSkge1xuICAgICAgcmV0dXJuIFNlYXJjaE1vZGUuRnVsbFRleHRTZWFyY2g7XG4gICAgfSBlbHNlIHtcbiAgICAgIGlmICh0aGlzLnN1cG9ydGFGaWx0cm9zKSB7XG4gICAgICAgIHJldHVybiBTZWFyY2hNb2RlLkZpbHRybztcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBwdWJsaWMgc2VyaWFsaXplUGVzcXVpc2EocGFyYW1ldHJvc0RhUmVxdWlzaWNhbzogUGFyYW1ldHJvc0RhUmVxdWlzaWNhbyk6IHN0cmluZyB7XG4gICAgY29uc3QgdGVybW8gPSB0aGlzLnBlc3F1aXNhVGVybW9JbnRlcmNlcHRvcihwYXJhbWV0cm9zRGFSZXF1aXNpY2FvLnBlc3F1aXNhKTtcbiAgICBpZiAocGFyYW1ldHJvc0RhUmVxdWlzaWNhby5wZXNxdWlzYS5jb25maWcgPT0gbnVsbCkge1xuICAgICAgcGFyYW1ldHJvc0RhUmVxdWlzaWNhby5wZXNxdWlzYS5jb25maWcgPSB7bW9kbzogdGhpcy5wZXNxdWlzYU1vZG9QYWRyYW99O1xuICAgIH1cbiAgICBpZiAocGFyYW1ldHJvc0RhUmVxdWlzaWNhby5wZXNxdWlzYS5jb25maWcubW9kbyA9PSBudWxsKSB7XG4gICAgICAvLyBTZSBuw6NvIGluZm9ybWFyIGFzIGNvbmZpZ3VyYcOnw7VlcywgZGV2ZSBwcmV2YWxlY2VyIG8gcXVlIG8gc2VyaWFsaXphZG9yIGRlZmluaXUgY29tbyBwYWRyYW9cbiAgICAgIHBhcmFtZXRyb3NEYVJlcXVpc2ljYW8ucGVzcXVpc2EuY29uZmlnLm1vZG8gPSB0aGlzLnBlc3F1aXNhTW9kb1BhZHJhbztcbiAgICB9XG4gICAgaWYgKCh0aGlzLnN1cG9ydGFGaWx0cm9zICYmICF0aGlzLnN1cG9ydGFQZXNxdWlzYSkgJiZcbiAgICAgIChwYXJhbWV0cm9zRGFSZXF1aXNpY2FvLnBlc3F1aXNhLmNvbmZpZy5tb2RvID09PSBTZWFyY2hNb2RlLkZpbHRybyB8fCB0aGlzLnBlc3F1aXNhQ2xhdXN1bGFGb3JtYXRvID09IG51bGwpKSB7XG4gICAgICAvLyBFc3RlIMOpIG8gY2FzbyBxdWFuZG8gYSBwZXNxdWlzYSDDqSByZWFsaXphZGEgcG9yIG1laW8gZGUgZmlsdHJvIHNpbXBsZXNcbiAgICAgIGlmIChwYXJhbWV0cm9zRGFSZXF1aXNpY2FvLnBlc3F1aXNhLmNvbmZpZy5maWx0cm8uY2FtcG9zID09IG51bGwgfHxcbiAgICAgICAgcGFyYW1ldHJvc0RhUmVxdWlzaWNhby5wZXNxdWlzYS5jb25maWcuZmlsdHJvLmNhbXBvcy5sZW5ndGggPD0gMCkge1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ05hIHBlc3F1aXNhIGNvbSBtb2RvIEZpbHRybywgb3MgY2FtcG9zIGRldmVtIHNlciBpbmZvcm1hZG9zIScpO1xuICAgICAgfVxuICAgICAgY29uc3Qgb3BlcmFkb3IgPSBwYXJhbWV0cm9zRGFSZXF1aXNpY2FvLnBlc3F1aXNhLmNvbmZpZy5maWx0cm8ub3BlcmFkb3IgP1xuICAgICAgICBwYXJhbWV0cm9zRGFSZXF1aXNpY2FvLnBlc3F1aXNhLmNvbmZpZy5maWx0cm8ub3BlcmFkb3IgOiB0aGlzLnBlc3F1aXNhRmlsdHJvT3BlcmFkb3JQYWRyYW87XG4gICAgICBjb25zdCBmaWx0cm9zID0gcGFyYW1ldHJvc0RhUmVxdWlzaWNhby5wZXNxdWlzYS5jb25maWcuZmlsdHJvLmNhbXBvcy5tYXAoY2FtcG8gPT4ge1xuICAgICAgICBjb25zdCBmaWx0cm8gPSB7Y2FtcG8sIG9wZXJhZG9yLCB2YWxvcjogdGVybW99IGFzIEZpbHRybztcbiAgICAgICAgcmV0dXJuIGZpbHRybztcbiAgICAgIH0pO1xuICAgICAgY29uc3QgZmlsdHJvQ29tcG9zdG8gPSB7b3BlcmFkb3I6IHRoaXMucGVzcXVpc2FGaWx0cm9Db21wb3N0b09wZXJhZG9yUGFkcmFvLCBmaWx0cm9zfSBhcyBGaWx0cm9Db21wb3N0bztcbiAgICAgIHBhcmFtZXRyb3NEYVJlcXVpc2ljYW8uZmlsdHJvcy5maWx0cm9zLnB1c2goZmlsdHJvQ29tcG9zdG8pO1xuICAgICAgcmV0dXJuIG51bGw7XG4gICAgfSBlbHNlIHtcbiAgICAgIGlmICghdGhpcy5zdXBvcnRhUGVzcXVpc2EpIHtcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdFc3RlIHNlcmlhbGl6YWRvciBuw6NvIHN1cG9ydGEgcGVzcXVpc2EhJyk7XG4gICAgICB9XG4gICAgICBjb25zdCBjbGF1c3VsYSA9IHRoaXMucGVzcXVpc2FDbGF1c3VsYUZvcm1hdG87XG4gICAgICBjb25zdCBjbGF1c3VsYVNlcmlhbGl6YWRhID0gdGhpcy5mb3JtYXQoY2xhdXN1bGEsIHtwZXNxdWlzYTogdGVybW99KTtcbiAgICAgIHJldHVybiBjbGF1c3VsYVNlcmlhbGl6YWRhO1xuICAgIH1cbiAgfVxuXG4gIHNlcmlhbGl6ZShwYXJhbWV0cm9zOiBQYXJhbWV0cm9zRGFSZXF1aXNpY2FvKTogc3RyaW5nIHtcbiAgICBjb25zdCBzZXJpYWxpemF2ZWlzOiBzdHJpbmdbXSA9IFtdO1xuXG4gICAgaWYgKHBhcmFtZXRyb3MucGVzcXVpc2EpIHtcbiAgICAgIGNvbnN0IHNlcmlhbGl6YWNhb0RhUGVzcXVpc2EgPSB0aGlzLnNlcmlhbGl6ZVBlc3F1aXNhKHBhcmFtZXRyb3MpO1xuICAgICAgc2VyaWFsaXphdmVpcy5wdXNoKHNlcmlhbGl6YWNhb0RhUGVzcXVpc2EpO1xuICAgIH1cblxuICAgIGlmIChwYXJhbWV0cm9zLmZpbHRyb3MpIHtcbiAgICAgIGNvbnN0IHNlcmlhbGl6YWNhb0Rvc0ZpbHRyb3MgPSB0aGlzLnNlcmlhbGl6ZUZpbHRyb3MocGFyYW1ldHJvcyk7XG4gICAgICBzZXJpYWxpemF2ZWlzLnB1c2goc2VyaWFsaXphY2FvRG9zRmlsdHJvcyk7XG4gICAgfVxuXG4gICAgaWYgKHBhcmFtZXRyb3MucGFnaW5hY2FvKSB7XG4gICAgICBjb25zdCBzZXJpYWxpemFjYW9EYVBhZ2luYWNhbyA9IHRoaXMuc2VyaWFsaXplUGFnaW5hY2FvKHBhcmFtZXRyb3MpO1xuICAgICAgc2VyaWFsaXphdmVpcy5wdXNoKHNlcmlhbGl6YWNhb0RhUGFnaW5hY2FvKTtcbiAgICB9XG5cbiAgICBpZiAocGFyYW1ldHJvcy5vcmRlbmFjb2VzKSB7XG4gICAgICBjb25zdCBzZXJpYWxpemFjYW9EYU9yZGVuYWNhbyA9IHRoaXMuc2VyaWFsaXplT3JkZW5hY2FvKHBhcmFtZXRyb3MpO1xuICAgICAgc2VyaWFsaXphdmVpcy5wdXNoKHNlcmlhbGl6YWNhb0RhT3JkZW5hY2FvKTtcbiAgICB9XG5cbiAgICBpZiAodGhpcy5zZXJpYWxpemVJbnRlcmNlcHRvcikge1xuICAgICAgY29uc3QgcGFyYW1ldHJvU2VyaWFsaXphZG8gPSB0aGlzLnNlcmlhbGl6ZUludGVyY2VwdG9yKHBhcmFtZXRyb3MpO1xuICAgICAgc2VyaWFsaXphdmVpcy5wdXNoKHBhcmFtZXRyb1NlcmlhbGl6YWRvKTtcbiAgICB9XG5cbiAgICBjb25zdCBwYXJhbWV0cm9zU2VyaWFsaXphZG9zID0gdGhpcy50cmF0YXJBcnJheShzZXJpYWxpemF2ZWlzKTtcbiAgICByZXR1cm4gcGFyYW1ldHJvc1NlcmlhbGl6YWRvcztcbiAgfVxuXG4gIHByb3RlY3RlZCBzZXJpYWxpemVJbnRlcmNlcHRvcj8ocGFyYW1ldHJvczogUGFyYW1ldHJvc0RhUmVxdWlzaWNhbyk6IHN0cmluZztcblxuICBzZXJpYWxpemVQYWdpbmFjYW8ocGFyYW1ldHJvc0RhUmVxdWlzaWNhbzogUGFyYW1ldHJvc0RhUmVxdWlzaWNhbyk6IHN0cmluZyB7XG4gICAgaWYgKCF0aGlzLnN1cG9ydGFQYWdpbmFjYW8pIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcignRXN0ZSBzZXJpYWxpemFkb3IgbsOjbyBzdXBvcnRhIHBhZ2luYWNhbyEnKTtcbiAgICB9XG5cbiAgICBjb25zdCBzZXJpYWxpemF2ZWlzOiBzdHJpbmdbXSA9IFtdO1xuXG4gICAgY29uc3QgcGFnaW5hY2FvUGFnaW5hQ2xhdXN1bGFGb3JtYXRvID0gdGhpcy5wYWdpbmFjYW9QYWdpbmFDbGF1c3VsYUZvcm1hdG87XG4gICAgY29uc3QgcGFnaW5hY2FvUGFnaW5hUGFyYUZvcm1hdGFyID0ge1xuICAgICAgcGFnaW5hOiB0aGlzLnBhZ2luYWNhb1BhZ2luYUludGVyY2VwdG9yKHBhcmFtZXRyb3NEYVJlcXVpc2ljYW8ucGFnaW5hY2FvKSxcbiAgICB9O1xuICAgIGNvbnN0IHBhZ2luYWNhb1BhZ2luYVNlcmlhbGl6YWRvID0gdGhpcy5mb3JtYXQocGFnaW5hY2FvUGFnaW5hQ2xhdXN1bGFGb3JtYXRvLCBwYWdpbmFjYW9QYWdpbmFQYXJhRm9ybWF0YXIpO1xuICAgIHNlcmlhbGl6YXZlaXMucHVzaChwYWdpbmFjYW9QYWdpbmFTZXJpYWxpemFkbyk7XG5cbiAgICBjb25zdCBwYWdpbmFjYW9JdGVuc1BvclBhZ2luYUNsYXVzdWxhRm9ybWF0byA9IHRoaXMucGFnaW5hY2FvSXRlbnNQb3JQYWdpbmFDbGF1c3VsYUZvcm1hdG87XG4gICAgY29uc3QgcGFnaW5hY2FvSXRlbnNQb3JQYWdpbmFQYXJhRm9ybWF0YXIgPSB7XG4gICAgICBpdGVuc1BvclBhZ2luYTogdGhpcy5wYWdpbmFjYW9JdGVuc1BvclBhZ2luYUludGVyY2VwdG9yKHBhcmFtZXRyb3NEYVJlcXVpc2ljYW8ucGFnaW5hY2FvKSxcbiAgICB9O1xuICAgIGNvbnN0IHBhZ2luYWNhb0l0ZW5zUG9yUGFnaW5hU2VyaWFsaXphZG8gPSB0aGlzLmZvcm1hdChwYWdpbmFjYW9JdGVuc1BvclBhZ2luYUNsYXVzdWxhRm9ybWF0bywgcGFnaW5hY2FvSXRlbnNQb3JQYWdpbmFQYXJhRm9ybWF0YXIpO1xuICAgIHNlcmlhbGl6YXZlaXMucHVzaChwYWdpbmFjYW9JdGVuc1BvclBhZ2luYVNlcmlhbGl6YWRvKTtcblxuICAgIGNvbnN0IHBhcmFtZXRyb3NTZXJpYWxpemFkb3MgPSB0aGlzLnRyYXRhckFycmF5KHNlcmlhbGl6YXZlaXMpO1xuICAgIHJldHVybiBwYXJhbWV0cm9zU2VyaWFsaXphZG9zO1xuICB9XG5cbiAgcHJvdGVjdGVkIHBhZ2luYWNhb1BhZ2luYUludGVyY2VwdG9yKHBhZ2luYWNhbzogUGFnaW5hY2FvKSB7XG4gICAgcmV0dXJuIHBhZ2luYWNhby5wYWdpbmE7XG4gIH1cblxuICBwcm90ZWN0ZWQgcGFnaW5hY2FvSXRlbnNQb3JQYWdpbmFJbnRlcmNlcHRvcihwYWdpbmFjYW86IFBhZ2luYWNhbykge1xuICAgIHJldHVybiBwYWdpbmFjYW8uaXRlbnNQb3JQYWdpbmE7XG4gIH1cblxuICBzZXJpYWxpemVPcmRlbmFjYW8ocGFyYW1ldHJvc0RhUmVxdWlzaWNhbzogUGFyYW1ldHJvc0RhUmVxdWlzaWNhbyk6IHN0cmluZyB7XG4gICAgaWYgKCF0aGlzLnN1cG9ydGFPcmRlbmFjYW8pIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcignRXN0ZSBzZXJpYWxpemFkb3IgbsOjbyBzdXBvcnRhIG9yZGVuYWNhbyEnKTtcbiAgICB9XG4gICAgLy8gTmFtZSBhc2MsIERlc2NyaXB0aW9uIGRlc2NcbiAgICBjb25zdCBwYWdpbmFjYW9BcnJheSA9IHBhcmFtZXRyb3NEYVJlcXVpc2ljYW8ub3JkZW5hY29lcy5tYXAob3JkZW5hY2FvID0+IHtcbiAgICAgIGNvbnN0IG9yZGVuYWNhb0Zvcm1hdG8gPSB0aGlzLm9yZGVuYWNhb0Zvcm1hdG87IC8vIGB7Y2FtcG99IHtkaXJlY2FvfWA7XG4gICAgICBjb25zdCBvcmRlbmFjYW9QYXJhRm9ybWF0YXIgPSB7XG4gICAgICAgIGNhbXBvOiB0aGlzLm9yZGVuYWNhb0NhbXBvSW50ZXJjZXB0b3Iob3JkZW5hY2FvKSxcbiAgICAgICAgZGlyZWNhbzogdGhpcy5vcmRlbmFjYW9EaXJlY2FvSW50ZXJjZXB0b3Iob3JkZW5hY2FvKSxcbiAgICAgIH0gYXMgT3JkZW5hY2FvO1xuICAgICAgY29uc3Qgb3JkZW5hY29lc1NlcmlhbGl6YWRhcyA9IHRoaXMuZm9ybWF0KG9yZGVuYWNhb0Zvcm1hdG8sIG9yZGVuYWNhb1BhcmFGb3JtYXRhcik7XG4gICAgICByZXR1cm4gb3JkZW5hY29lc1NlcmlhbGl6YWRhcztcbiAgICB9KTtcblxuICAgIGNvbnN0IG9yZGVuYWNvZXNTZXBhcmFkb3IgPSB0aGlzLm9yZGVuYWNvZXNTZXBhcmFkb3I7XG4gICAgY29uc3QgcGFyYW1ldHJvc1NlcmlhbGl6YWRvcyA9IHRoaXMudHJhdGFyQXJyYXkocGFnaW5hY2FvQXJyYXksIG9yZGVuYWNvZXNTZXBhcmFkb3IpO1xuXG4gICAgY29uc3Qgb3JkZW5hY2FvQ2xhdXN1bGEgPSB0aGlzLm9yZGVuYWNhb0NsYXVzdWxhRm9ybWF0bzsgLy8gYCRvcmRlcmJ5PXtvcmRlbmFjb2VzfWA7XG4gICAgY29uc3QgY2xhdXN1bGFGb3JtYXRhZGEgPSB0aGlzLmZvcm1hdChvcmRlbmFjYW9DbGF1c3VsYSwge29yZGVuYWNvZXM6IHBhcmFtZXRyb3NTZXJpYWxpemFkb3N9KTtcblxuICAgIHJldHVybiBjbGF1c3VsYUZvcm1hdGFkYTtcbiAgfVxuXG4gIHByb3RlY3RlZCBvcmRlbmFjYW9DYW1wb0ludGVyY2VwdG9yKG9yZGVuYWNhbzogT3JkZW5hY2FvKSB7XG4gICAgcmV0dXJuIG9yZGVuYWNhby5jYW1wbztcbiAgfVxuXG4gIHByb3RlY3RlZCBvcmRlbmFjYW9EaXJlY2FvSW50ZXJjZXB0b3Iob3JkZW5hY2FvOiBPcmRlbmFjYW8pIHtcbiAgICByZXR1cm4gb3JkZW5hY2FvLmRpcmVjYW87XG4gIH1cblxuICBwcm90ZWN0ZWQgZm9ybWF0KGZvcm1hdG86IHN0cmluZywgLi4uYXJnczogYW55KSB7XG4gICAgaWYgKHR5cGVvZiBhcmdzWzBdICE9PSAnb2JqZWN0Jykge1xuICAgICAgcmV0dXJuIGZvcm1hdG8ucmVwbGFjZSgve1xcZCt9L2csIChtKSA9PiB7XG4gICAgICAgIGNvbnN0IGluZGV4ID0gTnVtYmVyKG0ucmVwbGFjZSgvXFxEL2csICcnKSk7XG4gICAgICAgIHJldHVybiAoYXJnc1tpbmRleF0gPyBhcmdzW2luZGV4XSA6IG0pO1xuICAgICAgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGNvbnN0IG9iaiA9IGFyZ3NbMF07XG5cbiAgICAgIHJldHVybiBmb3JtYXRvLnJlcGxhY2UoL3tcXHcrfS9nLCAobSkgPT4ge1xuICAgICAgICBjb25zdCBrZXkgPSBtLnJlcGxhY2UoL3t8fS9nLCAnJyk7XG4gICAgICAgIHJldHVybiAob2JqLmhhc093blByb3BlcnR5KGtleSkgPyBvYmpba2V5XSA6IG0pO1xuICAgICAgfSk7XG4gICAgfVxuICB9XG59XG5cblxuXG5cblxuIl19