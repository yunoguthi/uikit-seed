/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { BaseSerializerService } from '../base-serializer.service';
import { Inject, Injectable, InjectionToken, Optional } from '@angular/core';
import { FiltroCompostoOperador, FiltroOperador } from '../../filter';
import * as i0 from "@angular/core";
/** @type {?} */
export var PjeDataSerializerServiceConfigToken = new InjectionToken('PjeDataSerializerServiceConfigToken');
/**
 * @record
 */
export function PjeDataSerializerServiceConfig() { }
if (false) {
    /** @type {?} */
    PjeDataSerializerServiceConfig.prototype.requestCount;
}
var PjeSerializerService = /** @class */ (function (_super) {
    tslib_1.__extends(PjeSerializerService, _super);
    function PjeSerializerService(oDataSerializerServiceConfig) {
        var _this = _super.call(this) || this;
        _this.suportaPesquisa = false;
        _this.suportaFiltros = true;
        _this.suportaPaginacao = true;
        _this.suportaOrdenacao = true;
        _this.ordenacaoClausulaFormato = "order={ordenacoes}";
        _this.ordenacaoFormato = "{\"{campo}\":\"{direcao}\"}";
        _this.ordenacoesSeparador = ", ";
        _this.paginacaoPaginaClausulaFormato = "page={\"page\":{pagina}, ";
        _this.paginacaoItensPorPaginaClausulaFormato = "\"size\":{itensPorPagina}}";
        _this.filtroClausulaFormato = "filter={{filtros}}";
        _this.filtroFormato = "{filtro}";
        _this.pesquisaClausulaFormato = "simpleFilter={pesquisa}";
        _this.pesquisaFiltroCompostoOperadorPadrao = FiltroCompostoOperador.E;
        /** @type {?} */
        var defaultParameter = (/** @type {?} */ ({ requestCount: true }));
        _this.dataConfig = Object.assign(defaultParameter, oDataSerializerServiceConfig);
        return _this;
    }
    /**
     * @protected
     * @param {?} paginacao
     * @return {?}
     */
    PjeSerializerService.prototype.paginacaoPaginaInterceptor = /**
     * @protected
     * @param {?} paginacao
     * @return {?}
     */
    function (paginacao) {
        /** @type {?} */
        var paginaZeroBased = paginacao.pagina - 1;
        return paginaZeroBased * paginacao.itensPorPagina;
    };
    /**
     * @protected
     * @param {?} filtroOperador
     * @return {?}
     */
    PjeSerializerService.prototype.getFiltroOperadorMapeado = /**
     * @protected
     * @param {?} filtroOperador
     * @return {?}
     */
    function (filtroOperador) {
        switch (filtroOperador) {
            case FiltroOperador.Igual: {
                return "\"{campo}\": {\"eq\": \"{valor}\"}";
            }
            case FiltroOperador.Menor: {
                return "\"{campo}\": {\"lt\": \"{valor}\"}";
            }
            case FiltroOperador.Maior: {
                return "\"{campo}\": {\"gt\": \"{valor}\"}";
            }
            case FiltroOperador.MenorOuIgual: {
                return "\"{campo}\": {\"le\": \"{valor}\"}";
            }
            case FiltroOperador.MaiorOuIgual: {
                return "\"{campo}\": {\"ge\": \"{valor}\"}";
            }
            case FiltroOperador.Em: {
                return "\"{campo}\": {\"in\": ({valor})}";
            }
            case FiltroOperador.NaoEm: {
                return "\"{campo}\": {\"not in\": ({valor})}";
            }
            case FiltroOperador.ComecaCom: {
                return "\"{campo}\": {\"starts-with\": \"{valor}\"}";
            }
            case FiltroOperador.TerminaCom: {
                return "\"{campo}\": {\"ends-with\": \"{valor}\"}";
            }
            case FiltroOperador.Contem: {
                return "\"{campo}\": {\"contains\": \"{valor}\"}";
            }
            case FiltroOperador.NaoContem: {
                return "\"{campo}\": {\"not-contains\": \"{valor}\"}";
            }
        }
        throw new Error("N\u00E3o foi encontrado um operador equivalente a '" + filtroOperador + "'!");
    };
    /**
     * @param {?} parametrosDaRequisicao
     * @return {?}
     */
    PjeSerializerService.prototype.serializePaginacao = /**
     * @param {?} parametrosDaRequisicao
     * @return {?}
     */
    function (parametrosDaRequisicao) {
        if (!this.suportaPaginacao) {
            throw new Error('Este serializador não suporta paginacao!');
        }
        /** @type {?} */
        var serializaveis = [];
        /** @type {?} */
        var paginacaoPaginaClausulaFormato = this.paginacaoPaginaClausulaFormato;
        /** @type {?} */
        var paginacaoPaginaParaFormatar = {
            pagina: this.paginacaoPaginaInterceptor(parametrosDaRequisicao.paginacao),
        };
        /** @type {?} */
        var paginacaoPaginaSerializado = this.format(paginacaoPaginaClausulaFormato, paginacaoPaginaParaFormatar);
        serializaveis.push(paginacaoPaginaSerializado);
        /** @type {?} */
        var paginacaoItensPorPaginaClausulaFormato = this.paginacaoItensPorPaginaClausulaFormato;
        /** @type {?} */
        var paginacaoItensPorPaginaParaFormatar = {
            itensPorPagina: this.paginacaoItensPorPaginaInterceptor(parametrosDaRequisicao.paginacao),
        };
        /** @type {?} */
        var paginacaoItensPorPaginaSerializado = this.format(paginacaoItensPorPaginaClausulaFormato, paginacaoItensPorPaginaParaFormatar);
        serializaveis.push(paginacaoItensPorPaginaSerializado);
        return this.tratarArray(serializaveis, '');
    };
    /**
     * @protected
     * @param {?} filtroCompostoOperador
     * @return {?}
     */
    PjeSerializerService.prototype.getFiltroCompostoOperadorMapeado = /**
     * @protected
     * @param {?} filtroCompostoOperador
     * @return {?}
     */
    function (filtroCompostoOperador) {
        if (filtroCompostoOperador === FiltroCompostoOperador.E) {
            return ',';
        }
        else {
            // tslint:disable-next-line:max-line-length
            throw new Error('Rest puro não suporta filtros que utilizam "ou" como lógica! Implemente seu próprio serializador para customizar como gerar a requisição devidamente!');
        }
    };
    /**
     * @protected
     * @param {?} filtro
     * @return {?}
     */
    PjeSerializerService.prototype.filtroValorInterceptor = /**
     * @protected
     * @param {?} filtro
     * @return {?}
     */
    function (filtro) {
        /** @type {?} */
        var valor = filtro.valor;
        if (valor instanceof Date) {
            return valor.toISOString();
        }
        else if (Array.isArray(valor)) {
            return "'" + valor.join('\',\'') + "'";
        }
        else if (typeof valor === 'string') {
            return "'" + valor + "'";
        }
        return valor;
    };
    PjeSerializerService.decorators = [
        { type: Injectable, args: [{ providedIn: 'root' },] }
    ];
    /** @nocollapse */
    PjeSerializerService.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [PjeDataSerializerServiceConfigToken,] }, { type: Optional }] }
    ]; };
    /** @nocollapse */ PjeSerializerService.ngInjectableDef = i0.defineInjectable({ factory: function PjeSerializerService_Factory() { return new PjeSerializerService(i0.inject(PjeDataSerializerServiceConfigToken, 8)); }, token: PjeSerializerService, providedIn: "root" });
    return PjeSerializerService;
}(BaseSerializerService));
export { PjeSerializerService };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    PjeSerializerService.prototype.suportaPesquisa;
    /**
     * @type {?}
     * @protected
     */
    PjeSerializerService.prototype.suportaFiltros;
    /**
     * @type {?}
     * @protected
     */
    PjeSerializerService.prototype.suportaPaginacao;
    /**
     * @type {?}
     * @protected
     */
    PjeSerializerService.prototype.suportaOrdenacao;
    /**
     * @type {?}
     * @protected
     */
    PjeSerializerService.prototype.ordenacaoClausulaFormato;
    /**
     * @type {?}
     * @protected
     */
    PjeSerializerService.prototype.ordenacaoFormato;
    /**
     * @type {?}
     * @protected
     */
    PjeSerializerService.prototype.ordenacoesSeparador;
    /**
     * @type {?}
     * @protected
     */
    PjeSerializerService.prototype.paginacaoPaginaClausulaFormato;
    /**
     * @type {?}
     * @protected
     */
    PjeSerializerService.prototype.paginacaoItensPorPaginaClausulaFormato;
    /**
     * @type {?}
     * @protected
     */
    PjeSerializerService.prototype.filtroClausulaFormato;
    /**
     * @type {?}
     * @protected
     */
    PjeSerializerService.prototype.filtroFormato;
    /**
     * @type {?}
     * @protected
     */
    PjeSerializerService.prototype.pesquisaClausulaFormato;
    /**
     * @type {?}
     * @protected
     */
    PjeSerializerService.prototype.pesquisaFiltroCompostoOperadorPadrao;
    /** @type {?} */
    PjeSerializerService.prototype.dataConfig;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGplLXNlcmlhbGl6ZXIuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2ZpbHRlcnMvc2VyaWFsaXplci9yZXN0L3BqZS1zZXJpYWxpemVyLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUMscUJBQXFCLEVBQUMsTUFBTSw0QkFBNEIsQ0FBQztBQUNqRSxPQUFPLEVBQUMsTUFBTSxFQUFFLFVBQVUsRUFBRSxjQUFjLEVBQUUsUUFBUSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQzNFLE9BQU8sRUFBUyxzQkFBc0IsRUFBRSxjQUFjLEVBQUMsTUFBTSxjQUFjLENBQUM7OztBQUk1RSxNQUFNLEtBQU8sbUNBQW1DLEdBQzlDLElBQUksY0FBYyxDQUFpQyxxQ0FBcUMsQ0FBQzs7OztBQUUzRixvREFFQzs7O0lBREMsc0RBQXNCOztBQUd4QjtJQUMwQyxnREFBcUI7SUFpQjdELDhCQUFxRSw0QkFBNkQ7UUFBbEksWUFDRSxpQkFBTyxTQUdSO1FBcEJTLHFCQUFlLEdBQUcsS0FBSyxDQUFDO1FBQ3hCLG9CQUFjLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLHNCQUFnQixHQUFHLElBQUksQ0FBQztRQUN4QixzQkFBZ0IsR0FBRyxJQUFJLENBQUM7UUFDeEIsOEJBQXdCLEdBQUcsb0JBQW9CLENBQUM7UUFDaEQsc0JBQWdCLEdBQUcsNkJBQXlCLENBQUM7UUFDN0MseUJBQW1CLEdBQUcsSUFBSSxDQUFDO1FBQzNCLG9DQUE4QixHQUFHLDJCQUF5QixDQUFDO1FBQzNELDRDQUFzQyxHQUFHLDRCQUEwQixDQUFDO1FBQ3BFLDJCQUFxQixHQUFHLG9CQUFvQixDQUFDO1FBQzdDLG1CQUFhLEdBQUcsVUFBVSxDQUFDO1FBQzNCLDZCQUF1QixHQUFHLHlCQUF5QixDQUFDO1FBQ3BELDBDQUFvQyxHQUFHLHNCQUFzQixDQUFDLENBQUMsQ0FBQzs7WUFNbEUsZ0JBQWdCLEdBQUcsbUJBQUEsRUFBQyxZQUFZLEVBQUUsSUFBSSxFQUFDLEVBQWtDO1FBQy9FLEtBQUksQ0FBQyxVQUFVLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsRUFBRSw0QkFBNEIsQ0FBQyxDQUFDOztJQUNsRixDQUFDOzs7Ozs7SUFFUyx5REFBMEI7Ozs7O0lBQXBDLFVBQXFDLFNBQW9COztZQUNqRCxlQUFlLEdBQUcsU0FBUyxDQUFDLE1BQU0sR0FBRyxDQUFDO1FBQzVDLE9BQU8sZUFBZSxHQUFHLFNBQVMsQ0FBQyxjQUFjLENBQUM7SUFDcEQsQ0FBQzs7Ozs7O0lBRVMsdURBQXdCOzs7OztJQUFsQyxVQUFtQyxjQUE4QjtRQUMvRCxRQUFRLGNBQWMsRUFBRTtZQUN0QixLQUFLLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDekIsT0FBTyxvQ0FBOEIsQ0FBQzthQUN2QztZQUNELEtBQUssY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUN6QixPQUFPLG9DQUE4QixDQUFDO2FBQ3ZDO1lBQ0QsS0FBSyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3pCLE9BQU8sb0NBQThCLENBQUM7YUFDdkM7WUFDRCxLQUFLLGNBQWMsQ0FBQyxZQUFZLENBQUMsQ0FBQztnQkFDaEMsT0FBTyxvQ0FBOEIsQ0FBQzthQUN2QztZQUNELEtBQUssY0FBYyxDQUFDLFlBQVksQ0FBQyxDQUFDO2dCQUNoQyxPQUFPLG9DQUE4QixDQUFDO2FBQ3ZDO1lBQ0QsS0FBSyxjQUFjLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ3RCLE9BQU8sa0NBQThCLENBQUM7YUFDdkM7WUFDRCxLQUFLLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDekIsT0FBTyxzQ0FBa0MsQ0FBQzthQUMzQztZQUNELEtBQUssY0FBYyxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUM3QixPQUFPLDZDQUF1QyxDQUFDO2FBQ2hEO1lBQ0QsS0FBSyxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQzlCLE9BQU8sMkNBQXFDLENBQUM7YUFDOUM7WUFDRCxLQUFLLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDMUIsT0FBTywwQ0FBb0MsQ0FBQzthQUM3QztZQUNELEtBQUssY0FBYyxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUM3QixPQUFPLDhDQUF3QyxDQUFDO2FBQ2pEO1NBQ0Y7UUFDRCxNQUFNLElBQUksS0FBSyxDQUFDLHdEQUFpRCxjQUFjLE9BQUksQ0FBQyxDQUFDO0lBQ3ZGLENBQUM7Ozs7O0lBRUQsaURBQWtCOzs7O0lBQWxCLFVBQW1CLHNCQUE4QztRQUMvRCxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFFO1lBQzFCLE1BQU0sSUFBSSxLQUFLLENBQUMsMENBQTBDLENBQUMsQ0FBQztTQUM3RDs7WUFFSyxhQUFhLEdBQWEsRUFBRTs7WUFFNUIsOEJBQThCLEdBQUcsSUFBSSxDQUFDLDhCQUE4Qjs7WUFDcEUsMkJBQTJCLEdBQUc7WUFDbEMsTUFBTSxFQUFFLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxzQkFBc0IsQ0FBQyxTQUFTLENBQUM7U0FDMUU7O1lBQ0ssMEJBQTBCLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyw4QkFBOEIsRUFBRSwyQkFBMkIsQ0FBQztRQUMzRyxhQUFhLENBQUMsSUFBSSxDQUFDLDBCQUEwQixDQUFDLENBQUM7O1lBRXpDLHNDQUFzQyxHQUFHLElBQUksQ0FBQyxzQ0FBc0M7O1lBQ3BGLG1DQUFtQyxHQUFHO1lBQzFDLGNBQWMsRUFBRSxJQUFJLENBQUMsa0NBQWtDLENBQUMsc0JBQXNCLENBQUMsU0FBUyxDQUFDO1NBQzFGOztZQUNLLGtDQUFrQyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsc0NBQXNDLEVBQUUsbUNBQW1DLENBQUM7UUFDbkksYUFBYSxDQUFDLElBQUksQ0FBQyxrQ0FBa0MsQ0FBQyxDQUFDO1FBRXZELE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFDN0MsQ0FBQzs7Ozs7O0lBRVMsK0RBQWdDOzs7OztJQUExQyxVQUEyQyxzQkFBOEM7UUFDdkYsSUFBSSxzQkFBc0IsS0FBSyxzQkFBc0IsQ0FBQyxDQUFDLEVBQUU7WUFDdkQsT0FBTyxHQUFHLENBQUM7U0FDWjthQUFNO1lBQ0wsMkNBQTJDO1lBQzNDLE1BQU0sSUFBSSxLQUFLLENBQUMsdUpBQXVKLENBQUMsQ0FBQztTQUMxSztJQUNILENBQUM7Ozs7OztJQUVTLHFEQUFzQjs7Ozs7SUFBaEMsVUFBaUMsTUFBYzs7WUFDdkMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxLQUFLO1FBQzFCLElBQUksS0FBSyxZQUFZLElBQUksRUFBRTtZQUN6QixPQUFPLEtBQUssQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUM1QjthQUFNLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUMvQixPQUFPLE1BQUksS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBRyxDQUFDO1NBQ25DO2FBQU0sSUFBSSxPQUFPLEtBQUssS0FBSyxRQUFRLEVBQUU7WUFDcEMsT0FBTyxNQUFJLEtBQUssTUFBRyxDQUFDO1NBQ3JCO1FBQ0QsT0FBTyxLQUFLLENBQUM7SUFDZixDQUFDOztnQkEvR0YsVUFBVSxTQUFDLEVBQUMsVUFBVSxFQUFFLE1BQU0sRUFBQzs7OztnREFrQmpCLE1BQU0sU0FBQyxtQ0FBbUMsY0FBRyxRQUFROzs7K0JBL0JwRTtDQTZIQyxBQWhIRCxDQUMwQyxxQkFBcUIsR0ErRzlEO1NBL0dZLG9CQUFvQjs7Ozs7O0lBQy9CLCtDQUFrQzs7Ozs7SUFDbEMsOENBQWdDOzs7OztJQUNoQyxnREFBa0M7Ozs7O0lBQ2xDLGdEQUFrQzs7Ozs7SUFDbEMsd0RBQTBEOzs7OztJQUMxRCxnREFBdUQ7Ozs7O0lBQ3ZELG1EQUFxQzs7Ozs7SUFDckMsOERBQXFFOzs7OztJQUNyRSxzRUFBOEU7Ozs7O0lBQzlFLHFEQUF1RDs7Ozs7SUFDdkQsNkNBQXFDOzs7OztJQUNyQyx1REFBOEQ7Ozs7O0lBQzlELG9FQUEwRTs7SUFFMUUsMENBQWtEIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtCYXNlU2VyaWFsaXplclNlcnZpY2V9IGZyb20gJy4uL2Jhc2Utc2VyaWFsaXplci5zZXJ2aWNlJztcbmltcG9ydCB7SW5qZWN0LCBJbmplY3RhYmxlLCBJbmplY3Rpb25Ub2tlbiwgT3B0aW9uYWx9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtGaWx0cm8sIEZpbHRyb0NvbXBvc3RvT3BlcmFkb3IsIEZpbHRyb09wZXJhZG9yfSBmcm9tICcuLi8uLi9maWx0ZXInO1xuaW1wb3J0IHtQYXJhbWV0cm9zRGFSZXF1aXNpY2FvfSBmcm9tICcuLi8uLi9yZXF1ZXN0LXBhcmFtZXRlcnMnO1xuaW1wb3J0IHtQYWdpbmFjYW99IGZyb20gJy4uLy4uL3BhZ2UnO1xuXG5leHBvcnQgY29uc3QgUGplRGF0YVNlcmlhbGl6ZXJTZXJ2aWNlQ29uZmlnVG9rZW4gPVxuICBuZXcgSW5qZWN0aW9uVG9rZW48UGplRGF0YVNlcmlhbGl6ZXJTZXJ2aWNlQ29uZmlnPignUGplRGF0YVNlcmlhbGl6ZXJTZXJ2aWNlQ29uZmlnVG9rZW4nKTtcblxuZXhwb3J0IGludGVyZmFjZSBQamVEYXRhU2VyaWFsaXplclNlcnZpY2VDb25maWcge1xuICByZXF1ZXN0Q291bnQ6IGJvb2xlYW47XG59XG5cbkBJbmplY3RhYmxlKHtwcm92aWRlZEluOiAncm9vdCd9KVxuZXhwb3J0IGNsYXNzIFBqZVNlcmlhbGl6ZXJTZXJ2aWNlIGV4dGVuZHMgQmFzZVNlcmlhbGl6ZXJTZXJ2aWNlIHtcbiAgcHJvdGVjdGVkIHN1cG9ydGFQZXNxdWlzYSA9IGZhbHNlO1xuICBwcm90ZWN0ZWQgc3Vwb3J0YUZpbHRyb3MgPSB0cnVlO1xuICBwcm90ZWN0ZWQgc3Vwb3J0YVBhZ2luYWNhbyA9IHRydWU7XG4gIHByb3RlY3RlZCBzdXBvcnRhT3JkZW5hY2FvID0gdHJ1ZTtcbiAgcHJvdGVjdGVkIG9yZGVuYWNhb0NsYXVzdWxhRm9ybWF0byA9IGBvcmRlcj17b3JkZW5hY29lc31gO1xuICBwcm90ZWN0ZWQgb3JkZW5hY2FvRm9ybWF0byA9IGB7XCJ7Y2FtcG99XCI6XCJ7ZGlyZWNhb31cIn1gO1xuICBwcm90ZWN0ZWQgb3JkZW5hY29lc1NlcGFyYWRvciA9IGAsIGA7XG4gIHByb3RlY3RlZCBwYWdpbmFjYW9QYWdpbmFDbGF1c3VsYUZvcm1hdG8gPSBgcGFnZT17XCJwYWdlXCI6e3BhZ2luYX0sIGA7XG4gIHByb3RlY3RlZCBwYWdpbmFjYW9JdGVuc1BvclBhZ2luYUNsYXVzdWxhRm9ybWF0byA9IGBcInNpemVcIjp7aXRlbnNQb3JQYWdpbmF9fWA7XG4gIHByb3RlY3RlZCBmaWx0cm9DbGF1c3VsYUZvcm1hdG8gPSBgZmlsdGVyPXt7ZmlsdHJvc319YDtcbiAgcHJvdGVjdGVkIGZpbHRyb0Zvcm1hdG8gPSBge2ZpbHRyb31gO1xuICBwcm90ZWN0ZWQgcGVzcXVpc2FDbGF1c3VsYUZvcm1hdG8gPSBgc2ltcGxlRmlsdGVyPXtwZXNxdWlzYX1gO1xuICBwcm90ZWN0ZWQgcGVzcXVpc2FGaWx0cm9Db21wb3N0b09wZXJhZG9yUGFkcmFvID0gRmlsdHJvQ29tcG9zdG9PcGVyYWRvci5FO1xuXG4gIHB1YmxpYyBkYXRhQ29uZmlnOiBQamVEYXRhU2VyaWFsaXplclNlcnZpY2VDb25maWc7XG5cbiAgY29uc3RydWN0b3IoQEluamVjdChQamVEYXRhU2VyaWFsaXplclNlcnZpY2VDb25maWdUb2tlbikgQE9wdGlvbmFsKCkgb0RhdGFTZXJpYWxpemVyU2VydmljZUNvbmZpZz86IFBqZURhdGFTZXJpYWxpemVyU2VydmljZUNvbmZpZykge1xuICAgIHN1cGVyKCk7XG4gICAgY29uc3QgZGVmYXVsdFBhcmFtZXRlciA9IHtyZXF1ZXN0Q291bnQ6IHRydWV9IGFzIFBqZURhdGFTZXJpYWxpemVyU2VydmljZUNvbmZpZztcbiAgICB0aGlzLmRhdGFDb25maWcgPSBPYmplY3QuYXNzaWduKGRlZmF1bHRQYXJhbWV0ZXIsIG9EYXRhU2VyaWFsaXplclNlcnZpY2VDb25maWcpO1xuICB9XG5cbiAgcHJvdGVjdGVkIHBhZ2luYWNhb1BhZ2luYUludGVyY2VwdG9yKHBhZ2luYWNhbzogUGFnaW5hY2FvKSB7XG4gICAgY29uc3QgcGFnaW5hWmVyb0Jhc2VkID0gcGFnaW5hY2FvLnBhZ2luYSAtIDE7XG4gICAgcmV0dXJuIHBhZ2luYVplcm9CYXNlZCAqIHBhZ2luYWNhby5pdGVuc1BvclBhZ2luYTtcbiAgfVxuXG4gIHByb3RlY3RlZCBnZXRGaWx0cm9PcGVyYWRvck1hcGVhZG8oZmlsdHJvT3BlcmFkb3I6IEZpbHRyb09wZXJhZG9yKTogc3RyaW5nIHtcbiAgICBzd2l0Y2ggKGZpbHRyb09wZXJhZG9yKSB7XG4gICAgICBjYXNlIEZpbHRyb09wZXJhZG9yLklndWFsOiB7XG4gICAgICAgIHJldHVybiBgXCJ7Y2FtcG99XCI6IHtcImVxXCI6IFwie3ZhbG9yfVwifWA7XG4gICAgICB9XG4gICAgICBjYXNlIEZpbHRyb09wZXJhZG9yLk1lbm9yOiB7XG4gICAgICAgIHJldHVybiBgXCJ7Y2FtcG99XCI6IHtcImx0XCI6IFwie3ZhbG9yfVwifWA7XG4gICAgICB9XG4gICAgICBjYXNlIEZpbHRyb09wZXJhZG9yLk1haW9yOiB7XG4gICAgICAgIHJldHVybiBgXCJ7Y2FtcG99XCI6IHtcImd0XCI6IFwie3ZhbG9yfVwifWA7XG4gICAgICB9XG4gICAgICBjYXNlIEZpbHRyb09wZXJhZG9yLk1lbm9yT3VJZ3VhbDoge1xuICAgICAgICByZXR1cm4gYFwie2NhbXBvfVwiOiB7XCJsZVwiOiBcInt2YWxvcn1cIn1gO1xuICAgICAgfVxuICAgICAgY2FzZSBGaWx0cm9PcGVyYWRvci5NYWlvck91SWd1YWw6IHtcbiAgICAgICAgcmV0dXJuIGBcIntjYW1wb31cIjoge1wiZ2VcIjogXCJ7dmFsb3J9XCJ9YDtcbiAgICAgIH1cbiAgICAgIGNhc2UgRmlsdHJvT3BlcmFkb3IuRW06IHtcbiAgICAgICAgcmV0dXJuIGBcIntjYW1wb31cIjoge1wiaW5cIjogKHt2YWxvcn0pfWA7XG4gICAgICB9XG4gICAgICBjYXNlIEZpbHRyb09wZXJhZG9yLk5hb0VtOiB7XG4gICAgICAgIHJldHVybiBgXCJ7Y2FtcG99XCI6IHtcIm5vdCBpblwiOiAoe3ZhbG9yfSl9YDtcbiAgICAgIH1cbiAgICAgIGNhc2UgRmlsdHJvT3BlcmFkb3IuQ29tZWNhQ29tOiB7XG4gICAgICAgIHJldHVybiBgXCJ7Y2FtcG99XCI6IHtcInN0YXJ0cy13aXRoXCI6IFwie3ZhbG9yfVwifWA7XG4gICAgICB9XG4gICAgICBjYXNlIEZpbHRyb09wZXJhZG9yLlRlcm1pbmFDb206IHtcbiAgICAgICAgcmV0dXJuIGBcIntjYW1wb31cIjoge1wiZW5kcy13aXRoXCI6IFwie3ZhbG9yfVwifWA7XG4gICAgICB9XG4gICAgICBjYXNlIEZpbHRyb09wZXJhZG9yLkNvbnRlbToge1xuICAgICAgICByZXR1cm4gYFwie2NhbXBvfVwiOiB7XCJjb250YWluc1wiOiBcInt2YWxvcn1cIn1gO1xuICAgICAgfVxuICAgICAgY2FzZSBGaWx0cm9PcGVyYWRvci5OYW9Db250ZW06IHtcbiAgICAgICAgcmV0dXJuIGBcIntjYW1wb31cIjoge1wibm90LWNvbnRhaW5zXCI6IFwie3ZhbG9yfVwifWA7XG4gICAgICB9XG4gICAgfVxuICAgIHRocm93IG5ldyBFcnJvcihgTsOjbyBmb2kgZW5jb250cmFkbyB1bSBvcGVyYWRvciBlcXVpdmFsZW50ZSBhICcke2ZpbHRyb09wZXJhZG9yfSchYCk7XG4gIH1cblxuICBzZXJpYWxpemVQYWdpbmFjYW8ocGFyYW1ldHJvc0RhUmVxdWlzaWNhbzogUGFyYW1ldHJvc0RhUmVxdWlzaWNhbyk6IHN0cmluZyB7XG4gICAgaWYgKCF0aGlzLnN1cG9ydGFQYWdpbmFjYW8pIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcignRXN0ZSBzZXJpYWxpemFkb3IgbsOjbyBzdXBvcnRhIHBhZ2luYWNhbyEnKTtcbiAgICB9XG5cbiAgICBjb25zdCBzZXJpYWxpemF2ZWlzOiBzdHJpbmdbXSA9IFtdO1xuXG4gICAgY29uc3QgcGFnaW5hY2FvUGFnaW5hQ2xhdXN1bGFGb3JtYXRvID0gdGhpcy5wYWdpbmFjYW9QYWdpbmFDbGF1c3VsYUZvcm1hdG87XG4gICAgY29uc3QgcGFnaW5hY2FvUGFnaW5hUGFyYUZvcm1hdGFyID0ge1xuICAgICAgcGFnaW5hOiB0aGlzLnBhZ2luYWNhb1BhZ2luYUludGVyY2VwdG9yKHBhcmFtZXRyb3NEYVJlcXVpc2ljYW8ucGFnaW5hY2FvKSxcbiAgICB9O1xuICAgIGNvbnN0IHBhZ2luYWNhb1BhZ2luYVNlcmlhbGl6YWRvID0gdGhpcy5mb3JtYXQocGFnaW5hY2FvUGFnaW5hQ2xhdXN1bGFGb3JtYXRvLCBwYWdpbmFjYW9QYWdpbmFQYXJhRm9ybWF0YXIpO1xuICAgIHNlcmlhbGl6YXZlaXMucHVzaChwYWdpbmFjYW9QYWdpbmFTZXJpYWxpemFkbyk7XG5cbiAgICBjb25zdCBwYWdpbmFjYW9JdGVuc1BvclBhZ2luYUNsYXVzdWxhRm9ybWF0byA9IHRoaXMucGFnaW5hY2FvSXRlbnNQb3JQYWdpbmFDbGF1c3VsYUZvcm1hdG87XG4gICAgY29uc3QgcGFnaW5hY2FvSXRlbnNQb3JQYWdpbmFQYXJhRm9ybWF0YXIgPSB7XG4gICAgICBpdGVuc1BvclBhZ2luYTogdGhpcy5wYWdpbmFjYW9JdGVuc1BvclBhZ2luYUludGVyY2VwdG9yKHBhcmFtZXRyb3NEYVJlcXVpc2ljYW8ucGFnaW5hY2FvKSxcbiAgICB9O1xuICAgIGNvbnN0IHBhZ2luYWNhb0l0ZW5zUG9yUGFnaW5hU2VyaWFsaXphZG8gPSB0aGlzLmZvcm1hdChwYWdpbmFjYW9JdGVuc1BvclBhZ2luYUNsYXVzdWxhRm9ybWF0bywgcGFnaW5hY2FvSXRlbnNQb3JQYWdpbmFQYXJhRm9ybWF0YXIpO1xuICAgIHNlcmlhbGl6YXZlaXMucHVzaChwYWdpbmFjYW9JdGVuc1BvclBhZ2luYVNlcmlhbGl6YWRvKTtcblxuICAgIHJldHVybiB0aGlzLnRyYXRhckFycmF5KHNlcmlhbGl6YXZlaXMsICcnKTtcbiAgfVxuXG4gIHByb3RlY3RlZCBnZXRGaWx0cm9Db21wb3N0b09wZXJhZG9yTWFwZWFkbyhmaWx0cm9Db21wb3N0b09wZXJhZG9yOiBGaWx0cm9Db21wb3N0b09wZXJhZG9yKTogc3RyaW5nIHtcbiAgICBpZiAoZmlsdHJvQ29tcG9zdG9PcGVyYWRvciA9PT0gRmlsdHJvQ29tcG9zdG9PcGVyYWRvci5FKSB7XG4gICAgICByZXR1cm4gJywnO1xuICAgIH0gZWxzZSB7XG4gICAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6bWF4LWxpbmUtbGVuZ3RoXG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ1Jlc3QgcHVybyBuw6NvIHN1cG9ydGEgZmlsdHJvcyBxdWUgdXRpbGl6YW0gXCJvdVwiIGNvbW8gbMOzZ2ljYSEgSW1wbGVtZW50ZSBzZXUgcHLDs3ByaW8gc2VyaWFsaXphZG9yIHBhcmEgY3VzdG9taXphciBjb21vIGdlcmFyIGEgcmVxdWlzacOnw6NvIGRldmlkYW1lbnRlIScpO1xuICAgIH1cbiAgfVxuXG4gIHByb3RlY3RlZCBmaWx0cm9WYWxvckludGVyY2VwdG9yKGZpbHRybzogRmlsdHJvKTogc3RyaW5nIHtcbiAgICBjb25zdCB2YWxvciA9IGZpbHRyby52YWxvcjtcbiAgICBpZiAodmFsb3IgaW5zdGFuY2VvZiBEYXRlKSB7XG4gICAgICByZXR1cm4gdmFsb3IudG9JU09TdHJpbmcoKTtcbiAgICB9IGVsc2UgaWYgKEFycmF5LmlzQXJyYXkodmFsb3IpKSB7XG4gICAgICByZXR1cm4gYCcke3ZhbG9yLmpvaW4oJ1xcJyxcXCcnKX0nYDtcbiAgICB9IGVsc2UgaWYgKHR5cGVvZiB2YWxvciA9PT0gJ3N0cmluZycpIHtcbiAgICAgIHJldHVybiBgJyR7dmFsb3J9J2A7XG4gICAgfVxuICAgIHJldHVybiB2YWxvcjtcbiAgfVxufVxuIl19