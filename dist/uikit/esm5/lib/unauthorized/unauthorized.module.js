/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UnauthorizedRoutingModule } from './unauthorized-routing.module';
import { UnauthorizedComponent } from './unauthorized.component';
import { MatButtonModule } from '@angular/material';
var UnauthorizedModule = /** @class */ (function () {
    function UnauthorizedModule() {
    }
    UnauthorizedModule.decorators = [
        { type: NgModule, args: [{
                    entryComponents: [],
                    declarations: [UnauthorizedComponent],
                    imports: [
                        CommonModule,
                        UnauthorizedRoutingModule,
                        MatButtonModule
                    ],
                },] }
    ];
    return UnauthorizedModule;
}());
export { UnauthorizedModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidW5hdXRob3JpemVkLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvdW5hdXRob3JpemVkL3VuYXV0aG9yaXplZC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRS9DLE9BQU8sRUFBRSx5QkFBeUIsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBQzFFLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ2pFLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUVwRDtJQUFBO0lBU2tDLENBQUM7O2dCQVRsQyxRQUFRLFNBQUM7b0JBQ1IsZUFBZSxFQUFFLEVBQUU7b0JBQ25CLFlBQVksRUFBRSxDQUFDLHFCQUFxQixDQUFDO29CQUNyQyxPQUFPLEVBQUU7d0JBQ1AsWUFBWTt3QkFDWix5QkFBeUI7d0JBQ3pCLGVBQWU7cUJBQ2hCO2lCQUNGOztJQUNpQyx5QkFBQztDQUFBLEFBVG5DLElBU21DO1NBQXRCLGtCQUFrQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuXG5pbXBvcnQgeyBVbmF1dGhvcml6ZWRSb3V0aW5nTW9kdWxlIH0gZnJvbSAnLi91bmF1dGhvcml6ZWQtcm91dGluZy5tb2R1bGUnO1xuaW1wb3J0IHsgVW5hdXRob3JpemVkQ29tcG9uZW50IH0gZnJvbSAnLi91bmF1dGhvcml6ZWQuY29tcG9uZW50JztcbmltcG9ydCB7IE1hdEJ1dHRvbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcblxuQE5nTW9kdWxlKHtcbiAgZW50cnlDb21wb25lbnRzOiBbXSxcbiAgZGVjbGFyYXRpb25zOiBbVW5hdXRob3JpemVkQ29tcG9uZW50XSxcbiAgaW1wb3J0czogW1xuICAgIENvbW1vbk1vZHVsZSxcbiAgICBVbmF1dGhvcml6ZWRSb3V0aW5nTW9kdWxlLFxuICAgIE1hdEJ1dHRvbk1vZHVsZVxuICBdLFxufSlcbmV4cG9ydCBjbGFzcyBVbmF1dGhvcml6ZWRNb2R1bGUgeyB9XG4iXX0=