/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UnauthorizedComponent } from './unauthorized.component';
var ɵ0 = {
    allowedTypes: ['normal', 'noshell', 'noshellnobreadcrumb'],
    defaultType: 'normal'
};
/** @type {?} */
var routes = [
    {
        path: 'unauthorized',
        component: UnauthorizedComponent,
        data: ɵ0
    }
];
var UnauthorizedRoutingModule = /** @class */ (function () {
    function UnauthorizedRoutingModule() {
    }
    UnauthorizedRoutingModule.decorators = [
        { type: NgModule, args: [{
                    imports: [RouterModule.forChild(routes)],
                    exports: [RouterModule]
                },] }
    ];
    return UnauthorizedRoutingModule;
}());
export { UnauthorizedRoutingModule };
export { ɵ0 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidW5hdXRob3JpemVkLXJvdXRpbmcubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi91bmF1dGhvcml6ZWQvdW5hdXRob3JpemVkLXJvdXRpbmcubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBVSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUN2RCxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztTQU12RDtJQUNKLFlBQVksRUFBRSxDQUFDLFFBQVEsRUFBRSxTQUFTLEVBQUUscUJBQXFCLENBQUM7SUFDMUQsV0FBVyxFQUFFLFFBQVE7Q0FDdEI7O0lBUEMsTUFBTSxHQUFXO0lBQ3JCO1FBQ0UsSUFBSSxFQUFFLGNBQWM7UUFDcEIsU0FBUyxFQUFFLHFCQUFxQjtRQUNoQyxJQUFJLElBR0g7S0FDRjtDQUNGO0FBRUQ7SUFBQTtJQUl3QyxDQUFDOztnQkFKeEMsUUFBUSxTQUFDO29CQUNSLE9BQU8sRUFBRSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7b0JBQ3hDLE9BQU8sRUFBRSxDQUFDLFlBQVksQ0FBQztpQkFDeEI7O0lBQ3VDLGdDQUFDO0NBQUEsQUFKekMsSUFJeUM7U0FBNUIseUJBQXlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFJvdXRlcywgUm91dGVyTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IFVuYXV0aG9yaXplZENvbXBvbmVudCB9IGZyb20gJy4vdW5hdXRob3JpemVkLmNvbXBvbmVudCc7XG5cbmNvbnN0IHJvdXRlczogUm91dGVzID0gW1xuICB7XG4gICAgcGF0aDogJ3VuYXV0aG9yaXplZCcsXG4gICAgY29tcG9uZW50OiBVbmF1dGhvcml6ZWRDb21wb25lbnQsXG4gICAgZGF0YToge1xuICAgICAgYWxsb3dlZFR5cGVzOiBbJ25vcm1hbCcsICdub3NoZWxsJywgJ25vc2hlbGxub2JyZWFkY3J1bWInXSxcbiAgICAgIGRlZmF1bHRUeXBlOiAnbm9ybWFsJ1xuICAgIH1cbiAgfVxuXTtcblxuQE5nTW9kdWxlKHtcbiAgaW1wb3J0czogW1JvdXRlck1vZHVsZS5mb3JDaGlsZChyb3V0ZXMpXSxcbiAgZXhwb3J0czogW1JvdXRlck1vZHVsZV1cbn0pXG5leHBvcnQgY2xhc3MgVW5hdXRob3JpemVkUm91dGluZ01vZHVsZSB7fVxuIl19