/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, ViewChild, Renderer2, Output, EventEmitter } from '@angular/core';
import { Location } from '@angular/common';
import { MatButton } from '@angular/material';
/** @enum {string} */
var SaveButtonSelection = {
    Salvar: 'Salvar',
    SalvarVoltar: 'Salvar e voltar',
    SalvarNovo: 'Salvar e novo',
};
export { SaveButtonSelection };
var SaveButtonComponent = /** @class */ (function () {
    function SaveButtonComponent(renderer, ngLocation) {
        this.renderer = renderer;
        this.ngLocation = ngLocation;
        // @ViewChild(MatButton) saveButton: MatButton;
        // @ViewChild('uikitSubmitButton', { read: ElementRef }) protected submitButton: ElementRef;
        // @ViewChild('uikitResetButton', { read: ElementRef }) protected resetButton: ElementRef;
        /**
         * Use the parent form to save (using submit), new (reset) and back (history)
         */
        // @Input() automaticTrigger = false;
        this.save = new EventEmitter();
        this.selection = SaveButtonSelection.Salvar;
        this.disabled = false;
    }
    /**
     * @return {?}
     */
    SaveButtonComponent.prototype.ngAfterViewInit = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @return {?}
     */
    SaveButtonComponent.prototype.saveAction = /**
     * @return {?}
     */
    function () {
        console.log(SaveButtonSelection.Salvar);
        this.selection = SaveButtonSelection.Salvar;
        // if (this.automaticTrigger) {
        //   this.action();
        // }
        this.save.emit(SaveButtonSelection.Salvar);
    };
    /**
     * @return {?}
     */
    SaveButtonComponent.prototype.saveBackAction = /**
     * @return {?}
     */
    function () {
        console.log(SaveButtonSelection.SalvarVoltar);
        this.selection = SaveButtonSelection.SalvarVoltar;
        // if (this.automaticTrigger) {
        //   this.action();
        // }
        this.save.emit(SaveButtonSelection.SalvarVoltar);
    };
    /**
     * @return {?}
     */
    SaveButtonComponent.prototype.saveNewAction = /**
     * @return {?}
     */
    function () {
        console.log(SaveButtonSelection.SalvarNovo);
        this.selection = SaveButtonSelection.SalvarNovo;
        // if (this.automaticTrigger) {
        //   this.action();
        // }
        this.save.emit(SaveButtonSelection.SalvarNovo);
    };
    // submit() {
    //   const saveButtonElement = this.saveButton.nativeElement as HTMLButtonElement;
    //   this.renderer.setAttribute(saveButtonElement, `type`, `submit`);
    //   saveButtonElement.click();
    //   if (this.selection === SaveButtonSelection.SalvarNovo) {
    //   }
    // }
    // submit() {
    //   const saveButtonElement = this.saveButton.nativeElement as HTMLButtonElement;
    //   this.renderer.setAttribute(saveButtonElement, `type`, `submit`);
    //   saveButtonElement.click();
    //   if (this.selection === SaveButtonSelection.SalvarNovo) {
    //   }
    // }
    /**
     * @return {?}
     */
    SaveButtonComponent.prototype.action = 
    // submit() {
    //   const saveButtonElement = this.saveButton.nativeElement as HTMLButtonElement;
    //   this.renderer.setAttribute(saveButtonElement, `type`, `submit`);
    //   saveButtonElement.click();
    //   if (this.selection === SaveButtonSelection.SalvarNovo) {
    //   }
    // }
    /**
     * @return {?}
     */
    function () {
        if (this.selection === SaveButtonSelection.Salvar) {
            return this.saveAction();
        }
        else if (this.selection === SaveButtonSelection.SalvarNovo) {
            return this.saveNewAction();
        }
        else if (this.selection === SaveButtonSelection.SalvarVoltar) {
            return this.saveBackAction();
        }
    };
    SaveButtonComponent.decorators = [
        { type: Component, args: [{
                    selector: 'uikit-save-button',
                    template: "<div class=\"btn-group\">\n  <button type=\"button\" (click)=\"action()\" [disabled]=\"disabled\" mat-fab color=\"primary\">\n    <mat-icon class=\"fas fa-save\"></mat-icon> {{ selection }}\n  </button>\n  <div [hidden]=\"true\">\n    <button mat-button #uikitSubmitButton type=\"submit\"></button>\n    <button mat-button #uikitResetButton type=\"reset\"></button>\n  </div>\n  <mat-menu #menu=\"matMenu\">\n    <button mat-menu-item [disabled]=\"disabled\" (click)=\"saveAction()\">Salvar</button>\n    <button mat-menu-item [disabled]=\"disabled\" (click)=\"saveBackAction()\">Salvar e Voltar</button>\n    <button mat-menu-item [disabled]=\"disabled\" (click)=\"saveNewAction()\">Salvar e Novo</button>\n  </mat-menu>\n\n  <button type=\"button\" [disabled]=\"disabled\" [matMenuTriggerFor]=\"menu\" mat-fab color=\"primary\">\n    <mat-icon class=\"fas fa-caret-down\"></mat-icon>\n  </button>\n</div>\n",
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    SaveButtonComponent.ctorParameters = function () { return [
        { type: Renderer2 },
        { type: Location }
    ]; };
    SaveButtonComponent.propDecorators = {
        save: [{ type: Output }],
        submitButton: [{ type: ViewChild, args: ['uikitSubmitButton',] }],
        resetButton: [{ type: ViewChild, args: ['uikitResetButton',] }],
        selection: [{ type: Input }],
        disabled: [{ type: Input }]
    };
    return SaveButtonComponent;
}());
export { SaveButtonComponent };
if (false) {
    /**
     * Use the parent form to save (using submit), new (reset) and back (history)
     * @type {?}
     */
    SaveButtonComponent.prototype.save;
    /**
     * @type {?}
     * @protected
     */
    SaveButtonComponent.prototype.submitButton;
    /**
     * @type {?}
     * @protected
     */
    SaveButtonComponent.prototype.resetButton;
    /** @type {?} */
    SaveButtonComponent.prototype.selection;
    /** @type {?} */
    SaveButtonComponent.prototype.disabled;
    /**
     * @type {?}
     * @protected
     */
    SaveButtonComponent.prototype.renderer;
    /**
     * @type {?}
     * @protected
     */
    SaveButtonComponent.prototype.ngLocation;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2F2ZS1idXR0b24uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL3NhdmUtYnV0dG9uL3NhdmUtYnV0dG9uLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsU0FBUyxFQUE2QixTQUFTLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNoSSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDM0MsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLG1CQUFtQixDQUFDOzs7SUFHNUMsUUFBUyxRQUFRO0lBQ2pCLGNBQWUsaUJBQWlCO0lBQ2hDLFlBQWEsZUFBZTs7O0FBRzlCO0lBMEJFLDZCQUNZLFFBQW1CLEVBQ25CLFVBQW9CO1FBRHBCLGFBQVEsR0FBUixRQUFRLENBQVc7UUFDbkIsZUFBVSxHQUFWLFVBQVUsQ0FBVTs7Ozs7Ozs7UUFkdEIsU0FBSSxHQUFHLElBQUksWUFBWSxFQUF1QixDQUFDO1FBUXpDLGNBQVMsR0FBd0IsbUJBQW1CLENBQUMsTUFBTSxDQUFDO1FBRTVELGFBQVEsR0FBRyxLQUFLLENBQUM7SUFLN0IsQ0FBQzs7OztJQUVMLDZDQUFlOzs7SUFBZjtJQUlBLENBQUM7Ozs7SUFFRCx3Q0FBVTs7O0lBQVY7UUFDRSxPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3hDLElBQUksQ0FBQyxTQUFTLEdBQUcsbUJBQW1CLENBQUMsTUFBTSxDQUFDO1FBQzVDLCtCQUErQjtRQUMvQixtQkFBbUI7UUFDbkIsSUFBSTtRQUNKLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzdDLENBQUM7Ozs7SUFFRCw0Q0FBYzs7O0lBQWQ7UUFDRSxPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQzlDLElBQUksQ0FBQyxTQUFTLEdBQUcsbUJBQW1CLENBQUMsWUFBWSxDQUFDO1FBQ2xELCtCQUErQjtRQUMvQixtQkFBbUI7UUFDbkIsSUFBSTtRQUNKLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQ25ELENBQUM7Ozs7SUFFRCwyQ0FBYTs7O0lBQWI7UUFDRSxPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQzVDLElBQUksQ0FBQyxTQUFTLEdBQUcsbUJBQW1CLENBQUMsVUFBVSxDQUFDO1FBQ2hELCtCQUErQjtRQUMvQixtQkFBbUI7UUFDbkIsSUFBSTtRQUNKLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ2pELENBQUM7SUFFRCxhQUFhO0lBQ2Isa0ZBQWtGO0lBQ2xGLHFFQUFxRTtJQUNyRSwrQkFBK0I7SUFDL0IsNkRBQTZEO0lBRTdELE1BQU07SUFDTixJQUFJOzs7Ozs7Ozs7OztJQUVKLG9DQUFNOzs7Ozs7Ozs7OztJQUFOO1FBQ0UsSUFBSSxJQUFJLENBQUMsU0FBUyxLQUFLLG1CQUFtQixDQUFDLE1BQU0sRUFBRTtZQUNqRCxPQUFPLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztTQUMxQjthQUFNLElBQUksSUFBSSxDQUFDLFNBQVMsS0FBSyxtQkFBbUIsQ0FBQyxVQUFVLEVBQUU7WUFDNUQsT0FBTyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7U0FDN0I7YUFBTSxJQUFJLElBQUksQ0FBQyxTQUFTLEtBQUssbUJBQW1CLENBQUMsWUFBWSxFQUFFO1lBQzlELE9BQU8sSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1NBQzlCO0lBQ0gsQ0FBQzs7Z0JBakZGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsbUJBQW1CO29CQUM3Qix3NUJBQTJDOztpQkFFNUM7Ozs7Z0JBZHdFLFNBQVM7Z0JBQ3pFLFFBQVE7Ozt1QkF1QmQsTUFBTTsrQkFLTixTQUFTLFNBQUMsbUJBQW1COzhCQUM3QixTQUFTLFNBQUMsa0JBQWtCOzRCQUU1QixLQUFLOzJCQUVMLEtBQUs7O0lBd0VSLDBCQUFDO0NBQUEsQUFoR0QsSUFnR0M7U0EzRlksbUJBQW1COzs7Ozs7SUFTOUIsbUNBQXlEOzs7OztJQUt6RCwyQ0FBa0U7Ozs7O0lBQ2xFLDBDQUFnRTs7SUFFaEUsd0NBQTRFOztJQUU1RSx1Q0FBaUM7Ozs7O0lBRy9CLHVDQUE2Qjs7Ozs7SUFDN0IseUNBQThCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBWaWV3Q2hpbGQsIEFmdGVyVmlld0luaXQsIEVsZW1lbnRSZWYsIFJlbmRlcmVyMiwgT3V0cHV0LCBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IExvY2F0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7IE1hdEJ1dHRvbiB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcblxuZXhwb3J0IGVudW0gU2F2ZUJ1dHRvblNlbGVjdGlvbiB7XG4gIFNhbHZhciA9ICdTYWx2YXInLFxuICBTYWx2YXJWb2x0YXIgPSAnU2FsdmFyIGUgdm9sdGFyJyxcbiAgU2FsdmFyTm92byA9ICdTYWx2YXIgZSBub3ZvJyxcbn1cblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAndWlraXQtc2F2ZS1idXR0b24nLFxuICB0ZW1wbGF0ZVVybDogJy4vc2F2ZS1idXR0b24uY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9zYXZlLWJ1dHRvbi5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIFNhdmVCdXR0b25Db21wb25lbnQgaW1wbGVtZW50cyBBZnRlclZpZXdJbml0IHtcblxuICAvLyBAVmlld0NoaWxkKE1hdEJ1dHRvbikgc2F2ZUJ1dHRvbjogTWF0QnV0dG9uO1xuICAvLyBAVmlld0NoaWxkKCd1aWtpdFN1Ym1pdEJ1dHRvbicsIHsgcmVhZDogRWxlbWVudFJlZiB9KSBwcm90ZWN0ZWQgc3VibWl0QnV0dG9uOiBFbGVtZW50UmVmO1xuICAvLyBAVmlld0NoaWxkKCd1aWtpdFJlc2V0QnV0dG9uJywgeyByZWFkOiBFbGVtZW50UmVmIH0pIHByb3RlY3RlZCByZXNldEJ1dHRvbjogRWxlbWVudFJlZjtcblxuICAvKiogVXNlIHRoZSBwYXJlbnQgZm9ybSB0byBzYXZlICh1c2luZyBzdWJtaXQpLCBuZXcgKHJlc2V0KSBhbmQgYmFjayAoaGlzdG9yeSkgKi9cbiAgLy8gQElucHV0KCkgYXV0b21hdGljVHJpZ2dlciA9IGZhbHNlO1xuXG4gIEBPdXRwdXQoKSBzYXZlID0gbmV3IEV2ZW50RW1pdHRlcjxTYXZlQnV0dG9uU2VsZWN0aW9uPigpO1xuICAvLyBAT3V0cHV0KCkgY2xpY2tBdFNhdmUgPSBuZXcgRXZlbnRFbWl0dGVyPFNhdmVCdXR0b25TZWxlY3Rpb24+KCk7XG4gIC8vIEBPdXRwdXQoKSBjbGlja0F0U2F2ZUFuZEJhY2sgPSBuZXcgRXZlbnRFbWl0dGVyPFNhdmVCdXR0b25TZWxlY3Rpb24+KCk7XG4gIC8vIEBPdXRwdXQoKSBjbGlja0F0U2F2ZUFuZE5ldyA9IG5ldyBFdmVudEVtaXR0ZXI8U2F2ZUJ1dHRvblNlbGVjdGlvbj4oKTtcblxuICBAVmlld0NoaWxkKCd1aWtpdFN1Ym1pdEJ1dHRvbicpIHByb3RlY3RlZCBzdWJtaXRCdXR0b246IE1hdEJ1dHRvbjtcbiAgQFZpZXdDaGlsZCgndWlraXRSZXNldEJ1dHRvbicpIHByb3RlY3RlZCByZXNldEJ1dHRvbjogTWF0QnV0dG9uO1xuXG4gIEBJbnB1dCgpIHB1YmxpYyBzZWxlY3Rpb246IFNhdmVCdXR0b25TZWxlY3Rpb24gPSBTYXZlQnV0dG9uU2VsZWN0aW9uLlNhbHZhcjtcblxuICBASW5wdXQoKSBwdWJsaWMgZGlzYWJsZWQgPSBmYWxzZTtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcm90ZWN0ZWQgcmVuZGVyZXI6IFJlbmRlcmVyMixcbiAgICBwcm90ZWN0ZWQgbmdMb2NhdGlvbjogTG9jYXRpb24sXG4gICkgeyB9XG5cbiAgbmdBZnRlclZpZXdJbml0KCkge1xuXG5cblxuICB9XG5cbiAgc2F2ZUFjdGlvbigpIHtcbiAgICBjb25zb2xlLmxvZyhTYXZlQnV0dG9uU2VsZWN0aW9uLlNhbHZhcik7XG4gICAgdGhpcy5zZWxlY3Rpb24gPSBTYXZlQnV0dG9uU2VsZWN0aW9uLlNhbHZhcjtcbiAgICAvLyBpZiAodGhpcy5hdXRvbWF0aWNUcmlnZ2VyKSB7XG4gICAgLy8gICB0aGlzLmFjdGlvbigpO1xuICAgIC8vIH1cbiAgICB0aGlzLnNhdmUuZW1pdChTYXZlQnV0dG9uU2VsZWN0aW9uLlNhbHZhcik7XG4gIH1cblxuICBzYXZlQmFja0FjdGlvbigpIHtcbiAgICBjb25zb2xlLmxvZyhTYXZlQnV0dG9uU2VsZWN0aW9uLlNhbHZhclZvbHRhcik7XG4gICAgdGhpcy5zZWxlY3Rpb24gPSBTYXZlQnV0dG9uU2VsZWN0aW9uLlNhbHZhclZvbHRhcjtcbiAgICAvLyBpZiAodGhpcy5hdXRvbWF0aWNUcmlnZ2VyKSB7XG4gICAgLy8gICB0aGlzLmFjdGlvbigpO1xuICAgIC8vIH1cbiAgICB0aGlzLnNhdmUuZW1pdChTYXZlQnV0dG9uU2VsZWN0aW9uLlNhbHZhclZvbHRhcik7XG4gIH1cblxuICBzYXZlTmV3QWN0aW9uKCkge1xuICAgIGNvbnNvbGUubG9nKFNhdmVCdXR0b25TZWxlY3Rpb24uU2FsdmFyTm92byk7XG4gICAgdGhpcy5zZWxlY3Rpb24gPSBTYXZlQnV0dG9uU2VsZWN0aW9uLlNhbHZhck5vdm87XG4gICAgLy8gaWYgKHRoaXMuYXV0b21hdGljVHJpZ2dlcikge1xuICAgIC8vICAgdGhpcy5hY3Rpb24oKTtcbiAgICAvLyB9XG4gICAgdGhpcy5zYXZlLmVtaXQoU2F2ZUJ1dHRvblNlbGVjdGlvbi5TYWx2YXJOb3ZvKTtcbiAgfVxuXG4gIC8vIHN1Ym1pdCgpIHtcbiAgLy8gICBjb25zdCBzYXZlQnV0dG9uRWxlbWVudCA9IHRoaXMuc2F2ZUJ1dHRvbi5uYXRpdmVFbGVtZW50IGFzIEhUTUxCdXR0b25FbGVtZW50O1xuICAvLyAgIHRoaXMucmVuZGVyZXIuc2V0QXR0cmlidXRlKHNhdmVCdXR0b25FbGVtZW50LCBgdHlwZWAsIGBzdWJtaXRgKTtcbiAgLy8gICBzYXZlQnV0dG9uRWxlbWVudC5jbGljaygpO1xuICAvLyAgIGlmICh0aGlzLnNlbGVjdGlvbiA9PT0gU2F2ZUJ1dHRvblNlbGVjdGlvbi5TYWx2YXJOb3ZvKSB7XG5cbiAgLy8gICB9XG4gIC8vIH1cblxuICBhY3Rpb24oKSB7XG4gICAgaWYgKHRoaXMuc2VsZWN0aW9uID09PSBTYXZlQnV0dG9uU2VsZWN0aW9uLlNhbHZhcikge1xuICAgICAgcmV0dXJuIHRoaXMuc2F2ZUFjdGlvbigpO1xuICAgIH0gZWxzZSBpZiAodGhpcy5zZWxlY3Rpb24gPT09IFNhdmVCdXR0b25TZWxlY3Rpb24uU2FsdmFyTm92bykge1xuICAgICAgcmV0dXJuIHRoaXMuc2F2ZU5ld0FjdGlvbigpO1xuICAgIH0gZWxzZSBpZiAodGhpcy5zZWxlY3Rpb24gPT09IFNhdmVCdXR0b25TZWxlY3Rpb24uU2FsdmFyVm9sdGFyKSB7XG4gICAgICByZXR1cm4gdGhpcy5zYXZlQmFja0FjdGlvbigpO1xuICAgIH1cbiAgfVxuXG4gIC8vIGFjdGlvbigpIHtcbiAgLy8gICBjb25zdCBzdWJtaXRCdXR0b25FbGVtZW50UmVmID0gdGhpcy5zdWJtaXRCdXR0b24uX2VsZW1lbnRSZWY7XG4gIC8vICAgY29uc3Qgc3VibWl0QnV0dG9uRWxlbWVudCA9IHN1Ym1pdEJ1dHRvbkVsZW1lbnRSZWYubmF0aXZlRWxlbWVudCBhcyBIVE1MQnV0dG9uRWxlbWVudDtcbiAgLy8gICBzdWJtaXRCdXR0b25FbGVtZW50LmNsaWNrKCk7XG4gIC8vICAgaWYgKHRoaXMuc2VsZWN0aW9uID09PSBTYXZlQnV0dG9uU2VsZWN0aW9uLlNhbHZhck5vdm8pIHtcbiAgLy8gICAgIGNvbnN0IHJlc2V0QnV0dG9uRWxlbWVudFJlZiA9IHRoaXMucmVzZXRCdXR0b24uX2VsZW1lbnRSZWY7XG4gIC8vICAgICBjb25zdCByZXNldEJ1dHRvbkVsZW1lbnQgPSByZXNldEJ1dHRvbkVsZW1lbnRSZWYubmF0aXZlRWxlbWVudCBhcyBIVE1MQnV0dG9uRWxlbWVudDtcbiAgLy8gICAgIHJlc2V0QnV0dG9uRWxlbWVudC5jbGljaygpO1xuICAvLyAgIH0gZWxzZSBpZiAodGhpcy5zZWxlY3Rpb24gPT09IFNhdmVCdXR0b25TZWxlY3Rpb24uU2FsdmFyVm9sdGFyKSB7XG4gIC8vICAgICB0aGlzLm5nTG9jYXRpb24uYmFjaygpO1xuICAvLyAgIH1cbiAgLy8gfVxuXG59XG4iXX0=