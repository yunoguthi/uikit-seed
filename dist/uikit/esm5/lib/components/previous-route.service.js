/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { filter } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
var PreviousRouteService = /** @class */ (function () {
    function PreviousRouteService(router) {
        var _this = this;
        this.router = router;
        this.history = [];
        this.rotasAcessadas = [];
        this.removerRotaAcessada = (/**
         * @return {?}
         */
        function () { return _this.rotasAcessadas.splice(_this.rotasAcessadas.length - 2, 2); });
        console.log('constructor PreviousRouteService');
        this.desabilitarBotaoVoltar = new BehaviorSubject(true);
        this.router.events
            .pipe(filter((/**
         * @param {?} event
         * @return {?}
         */
        function (event) { return event instanceof NavigationEnd; })))
            .subscribe((/**
         * @param {?} __0
         * @return {?}
         */
        function (_a) {
            var urlAfterRedirects = _a.urlAfterRedirects;
            _this.history = tslib_1.__spread(_this.history, [urlAfterRedirects]);
            _this.desabilitarBotaoVoltar.next(_this.rotasAcessadas.length === 0);
            _this.rotasAcessadas = tslib_1.__spread(_this.rotasAcessadas, [urlAfterRedirects]);
        }));
        // this.router.events
        //   .pipe(filter((event: RouterEvent) => event instanceof NavigationEnd))
        //   .subscribe((event: NavigationEnd) => {
        //     console.log(`prev -> ${this.currentUrl}`);
        //     console.log(`curr -> ${event.urlAfterRedirects}`);
        //     this.previousUrl.next(this.currentUrl);
        //     this.currentUrl = event.urlAfterRedirects;
        //   });
        // this.previousUrl$.subscribe((previousUrl) => {
        //   this.previousUrl = previousUrl;
        // });
        // this.currentUrl$.subscribe((currentUrl) => {
        //   this.currentUrl = currentUrl;
        // });
        // this.currentUrlSubject.next(this.router.url);
        // router.events.subscribe(event => {
        //   if (event instanceof NavigationEnd) {
        //     this.previousUrlSubject.next(this.currentUrlSubject.getValue());
        //     this.currentUrlSubject.next(event.url);
        //   }
        // });
    }
    /**
     * @return {?}
     */
    PreviousRouteService.prototype.getHistory = /**
     * @return {?}
     */
    function () {
        return this.history;
    };
    /**
     * @return {?}
     */
    PreviousRouteService.prototype.getPreviousUrl = /**
     * @return {?}
     */
    function () {
        return this.history[this.history.length - 2];
    };
    PreviousRouteService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    PreviousRouteService.ctorParameters = function () { return [
        { type: Router }
    ]; };
    /** @nocollapse */ PreviousRouteService.ngInjectableDef = i0.defineInjectable({ factory: function PreviousRouteService_Factory() { return new PreviousRouteService(i0.inject(i1.Router)); }, token: PreviousRouteService, providedIn: "root" });
    return PreviousRouteService;
}());
export { PreviousRouteService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    PreviousRouteService.prototype.history;
    /** @type {?} */
    PreviousRouteService.prototype.desabilitarBotaoVoltar;
    /**
     * @type {?}
     * @private
     */
    PreviousRouteService.prototype.rotasAcessadas;
    /** @type {?} */
    PreviousRouteService.prototype.removerRotaAcessada;
    /**
     * @type {?}
     * @private
     */
    PreviousRouteService.prototype.router;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJldmlvdXMtcm91dGUuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9wcmV2aW91cy1yb3V0ZS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQWtCLE1BQU0sRUFBZSxhQUFhLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUVwRixPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ3ZDLE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQzs7O0FBRXhDO0lBa0JFLDhCQUFvQixNQUFjO1FBQWxDLGlCQW9DQztRQXBDbUIsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQWIxQixZQUFPLEdBQUcsRUFBRSxDQUFDO1FBV2IsbUJBQWMsR0FBRyxFQUFFLENBQUM7UUFnRHJCLHdCQUFtQjs7O1FBQUcsY0FBTSxPQUFBLEtBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLEtBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBN0QsQ0FBNkQsRUFBQztRQTdDL0YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxrQ0FBa0MsQ0FBQyxDQUFDO1FBRWhELElBQUksQ0FBQyxzQkFBc0IsR0FBRyxJQUFJLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUV4RCxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU07YUFDZixJQUFJLENBQUMsTUFBTTs7OztRQUFDLFVBQUEsS0FBSyxJQUFJLE9BQUEsS0FBSyxZQUFZLGFBQWEsRUFBOUIsQ0FBOEIsRUFBQyxDQUFDO2FBQ3JELFNBQVM7Ozs7UUFBQyxVQUFDLEVBQW9DO2dCQUFsQyx3Q0FBaUI7WUFDN0IsS0FBSSxDQUFDLE9BQU8sb0JBQVEsS0FBSSxDQUFDLE9BQU8sR0FBRSxpQkFBaUIsRUFBRSxDQUFDO1lBRXRELEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDbkUsS0FBSSxDQUFDLGNBQWMsb0JBQVEsS0FBSSxDQUFDLGNBQWMsR0FBRSxpQkFBaUIsRUFBRSxDQUFDO1FBQ3RFLENBQUMsRUFBQyxDQUFDO1FBRUwscUJBQXFCO1FBQ3JCLDBFQUEwRTtRQUMxRSwyQ0FBMkM7UUFDM0MsaURBQWlEO1FBQ2pELHlEQUF5RDtRQUN6RCw4Q0FBOEM7UUFDOUMsaURBQWlEO1FBQ2pELFFBQVE7UUFFUixpREFBaUQ7UUFDakQsb0NBQW9DO1FBQ3BDLE1BQU07UUFDTiwrQ0FBK0M7UUFDL0Msa0NBQWtDO1FBQ2xDLE1BQU07UUFDTixnREFBZ0Q7UUFDaEQscUNBQXFDO1FBQ3JDLDBDQUEwQztRQUMxQyx1RUFBdUU7UUFDdkUsOENBQThDO1FBQzlDLE1BQU07UUFDTixNQUFNO0lBQ1IsQ0FBQzs7OztJQUVNLHlDQUFVOzs7SUFBakI7UUFDRSxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUM7SUFDdEIsQ0FBQzs7OztJQUVNLDZDQUFjOzs7SUFBckI7UUFDRSxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUM7SUFDL0MsQ0FBQzs7Z0JBOURGLFVBQVUsU0FBQztvQkFDVixVQUFVLEVBQUUsTUFBTTtpQkFDbkI7Ozs7Z0JBUHdCLE1BQU07OzsrQkFEL0I7Q0ErRUMsQUF6RUQsSUF5RUM7U0F0RVksb0JBQW9COzs7Ozs7SUFFL0IsdUNBQXFCOztJQVVyQixzREFBd0Q7Ozs7O0lBQ3hELDhDQUE0Qjs7SUFnRDVCLG1EQUFpRzs7Ozs7SUE5Q3JGLHNDQUFzQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SW5qZWN0YWJsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge05hdmlnYXRpb25TdGFydCwgUm91dGVyLCBSb3V0ZXJFdmVudCwgTmF2aWdhdGlvbkVuZH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7UGxhdGZvcm1Mb2NhdGlvbn0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7IEJlaGF2aW9yU3ViamVjdCB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgZmlsdGVyIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBQcmV2aW91c1JvdXRlU2VydmljZSB7XG5cbiAgcHJpdmF0ZSBoaXN0b3J5ID0gW107XG5cbiAgLy8gcHJvdGVjdGVkIHByZXZpb3VzVXJsU3ViamVjdCA9IG5ldyBCZWhhdmlvclN1YmplY3Q8c3RyaW5nPihudWxsKTtcbiAgLy8gcHVibGljIHByZXZpb3VzVXJsJCA9IHRoaXMucHJldmlvdXNVcmxTdWJqZWN0LmFzT2JzZXJ2YWJsZSgpLnBpcGUoZGlzdGluY3RVbnRpbENoYW5nZWQoKSk7XG4gIC8vIHB1YmxpYyBwcmV2aW91c1VybCA9IG51bGw7XG5cbiAgLy8gcHJvdGVjdGVkIGN1cnJlbnRVcmxTdWJqZWN0ID0gbmV3IEJlaGF2aW9yU3ViamVjdDxzdHJpbmc+KG51bGwpO1xuICAvLyBwdWJsaWMgY3VycmVudFVybCQgPSB0aGlzLmN1cnJlbnRVcmxTdWJqZWN0LmFzT2JzZXJ2YWJsZSgpLnBpcGUoZGlzdGluY3RVbnRpbENoYW5nZWQoKSk7XG4gIC8vIHB1YmxpYyBjdXJyZW50VXJsID0gbnVsbDtcblxuICBwdWJsaWMgZGVzYWJpbGl0YXJCb3Rhb1ZvbHRhcjogQmVoYXZpb3JTdWJqZWN0PGJvb2xlYW4+O1xuICBwcml2YXRlIHJvdGFzQWNlc3NhZGFzID0gW107XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSByb3V0ZXI6IFJvdXRlcikge1xuICAgIGNvbnNvbGUubG9nKCdjb25zdHJ1Y3RvciBQcmV2aW91c1JvdXRlU2VydmljZScpO1xuXG4gICAgdGhpcy5kZXNhYmlsaXRhckJvdGFvVm9sdGFyID0gbmV3IEJlaGF2aW9yU3ViamVjdCh0cnVlKTtcblxuICAgIHRoaXMucm91dGVyLmV2ZW50c1xuICAgICAgLnBpcGUoZmlsdGVyKGV2ZW50ID0+IGV2ZW50IGluc3RhbmNlb2YgTmF2aWdhdGlvbkVuZCkpXG4gICAgICAuc3Vic2NyaWJlKCh7IHVybEFmdGVyUmVkaXJlY3RzIH06IE5hdmlnYXRpb25FbmQpID0+IHtcbiAgICAgICAgdGhpcy5oaXN0b3J5ID0gWyAuLi50aGlzLmhpc3RvcnksIHVybEFmdGVyUmVkaXJlY3RzIF07XG5cbiAgICAgICAgdGhpcy5kZXNhYmlsaXRhckJvdGFvVm9sdGFyLm5leHQodGhpcy5yb3Rhc0FjZXNzYWRhcy5sZW5ndGggPT09IDApO1xuICAgICAgICB0aGlzLnJvdGFzQWNlc3NhZGFzID0gWyAuLi50aGlzLnJvdGFzQWNlc3NhZGFzLCB1cmxBZnRlclJlZGlyZWN0cyBdO1xuICAgICAgfSk7XG5cbiAgICAvLyB0aGlzLnJvdXRlci5ldmVudHNcbiAgICAvLyAgIC5waXBlKGZpbHRlcigoZXZlbnQ6IFJvdXRlckV2ZW50KSA9PiBldmVudCBpbnN0YW5jZW9mIE5hdmlnYXRpb25FbmQpKVxuICAgIC8vICAgLnN1YnNjcmliZSgoZXZlbnQ6IE5hdmlnYXRpb25FbmQpID0+IHtcbiAgICAvLyAgICAgY29uc29sZS5sb2coYHByZXYgLT4gJHt0aGlzLmN1cnJlbnRVcmx9YCk7XG4gICAgLy8gICAgIGNvbnNvbGUubG9nKGBjdXJyIC0+ICR7ZXZlbnQudXJsQWZ0ZXJSZWRpcmVjdHN9YCk7XG4gICAgLy8gICAgIHRoaXMucHJldmlvdXNVcmwubmV4dCh0aGlzLmN1cnJlbnRVcmwpO1xuICAgIC8vICAgICB0aGlzLmN1cnJlbnRVcmwgPSBldmVudC51cmxBZnRlclJlZGlyZWN0cztcbiAgICAvLyAgIH0pO1xuXG4gICAgLy8gdGhpcy5wcmV2aW91c1VybCQuc3Vic2NyaWJlKChwcmV2aW91c1VybCkgPT4ge1xuICAgIC8vICAgdGhpcy5wcmV2aW91c1VybCA9IHByZXZpb3VzVXJsO1xuICAgIC8vIH0pO1xuICAgIC8vIHRoaXMuY3VycmVudFVybCQuc3Vic2NyaWJlKChjdXJyZW50VXJsKSA9PiB7XG4gICAgLy8gICB0aGlzLmN1cnJlbnRVcmwgPSBjdXJyZW50VXJsO1xuICAgIC8vIH0pO1xuICAgIC8vIHRoaXMuY3VycmVudFVybFN1YmplY3QubmV4dCh0aGlzLnJvdXRlci51cmwpO1xuICAgIC8vIHJvdXRlci5ldmVudHMuc3Vic2NyaWJlKGV2ZW50ID0+IHtcbiAgICAvLyAgIGlmIChldmVudCBpbnN0YW5jZW9mIE5hdmlnYXRpb25FbmQpIHtcbiAgICAvLyAgICAgdGhpcy5wcmV2aW91c1VybFN1YmplY3QubmV4dCh0aGlzLmN1cnJlbnRVcmxTdWJqZWN0LmdldFZhbHVlKCkpO1xuICAgIC8vICAgICB0aGlzLmN1cnJlbnRVcmxTdWJqZWN0Lm5leHQoZXZlbnQudXJsKTtcbiAgICAvLyAgIH1cbiAgICAvLyB9KTtcbiAgfVxuXG4gIHB1YmxpYyBnZXRIaXN0b3J5KCk6IHN0cmluZ1tdIHtcbiAgICByZXR1cm4gdGhpcy5oaXN0b3J5O1xuICB9XG5cbiAgcHVibGljIGdldFByZXZpb3VzVXJsKCk6IHN0cmluZyB7XG4gICAgcmV0dXJuIHRoaXMuaGlzdG9yeVt0aGlzLmhpc3RvcnkubGVuZ3RoIC0gMl07XG4gIH1cblxuICBwdWJsaWMgcmVtb3ZlclJvdGFBY2Vzc2FkYSA9ICgpID0+IHRoaXMucm90YXNBY2Vzc2FkYXMuc3BsaWNlKHRoaXMucm90YXNBY2Vzc2FkYXMubGVuZ3RoIC0gMiwgMik7XG5cbiAgLy8gcHVibGljIGdldCBnZXRQcmV2aW91c1VybCgpIHtcbiAgLy8gICByZXR1cm4gdGhpcy5wcmV2aW91c1VybDtcbiAgLy8gfVxuXG4gIC8vIHB1YmxpYyBnZXQgZ2V0Q3VycmVudFVybCgpIHtcbiAgLy8gICByZXR1cm4gdGhpcy5jdXJyZW50VXJsO1xuICAvLyB9XG59XG4iXX0=