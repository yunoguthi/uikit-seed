/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { ContentChild, ViewChild, EventEmitter, Output, Component, Input, TemplateRef } from '@angular/core';
import { FormGroupDirective } from '@angular/forms';
import { MatDialog, MatInput } from '@angular/material';
import { Hotkey, HotkeysService } from 'angular2-hotkeys';
import { FilterService } from './filter.service';
var FilterComponent = /** @class */ (function () {
    function FilterComponent(hotkeysService, filterService, dialog) {
        this.hotkeysService = hotkeysService;
        this.filterService = filterService;
        this.dialog = dialog;
        this.clickPesquisa = new EventEmitter();
        this.filterBadge$ = this.filterService.filterBadge$;
        this.showFilters$ = this.filterService.showFilters$;
        this.subscriptions = [];
    }
    /**
     * @return {?}
     */
    FilterComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @return {?}
     */
    FilterComponent.prototype.ngAfterViewInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.hotkeysService.add(new Hotkey('alt+3', (/**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            _this.searchInput.focus();
            return false;
        })));
        this.hotkeysService.add(new Hotkey('ctrl+alt+3', (/**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            _this.toggleFilters();
            return false;
        })));
    };
    /**
     * @return {?}
     */
    FilterComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        if (this.subscriptions) {
            this.subscriptions.forEach((/**
             * @param {?} subscription
             * @return {?}
             */
            function (subscription) {
                if (subscription && subscription.closed === false) {
                    subscription.unsubscribe();
                }
            }));
        }
    };
    /**
     * @return {?}
     */
    FilterComponent.prototype.pesquisar = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.clickPesquisa.emit(this.searchInput.value);
        this.clickPesquisa.subscribe((/**
         * @return {?}
         */
        function () {
            _this.searchInput.focus();
        }));
    };
    /**
     * @return {?}
     */
    FilterComponent.prototype.toggleFilters = /**
     * @return {?}
     */
    function () {
        this.dialogRef = this.dialog.open(this.template);
    };
    /**
     * @return {?}
     */
    FilterComponent.prototype.onNoClick = /**
     * @return {?}
     */
    function () {
        this.dialogRef.close();
    };
    FilterComponent.decorators = [
        { type: Component, args: [{
                    selector: 'uikit-filter',
                    template: "<div class=\"uikit-filter\">\n  <div class=\"uikit-finder\">\n    <mat-form-field>\n      <input #search=\"matInput\" matInput placeholder=\"Pesquisar\" autofocus/>\n      <button type='button' mat-icon-button class=\"btn-search\" (click)=\"pesquisar()\" [attr.disabled]=\"!disabled\">\n        <mat-icon class=\"fa-2x fas fa-search\"></mat-icon>\n      </button>\n    </mat-form-field>\n\n    <button type=\"button\" (click)=\"toggleFilters()\" #moreFilters mat-flat-button color=\"primary\"\n            class=\"btn-more-filters\" matTooltip=\"Mostrar ou esconder mais op\u00E7\u00F5es de filtros\"\n            aria-label=\"Mostrar ou esconder mais op\u00E7\u00F5es de filtros\"\n            [matBadge]=\"(filterBadge$ | async)\" [matBadgeHidden]=\"(filterBadge$ | async) == 0\"\n            matBadgeColor=\"accent\"\n            matBadgeSize=\"medium\">\n      mais filtros\n    </button>\n  </div>\n</div>\n\n<ng-template #filterDialog>\n  <div>\n    <div class=\"row line\">\n      <div class=\"col-10\">\n        <h1 mat-dialog-title>Filtros</h1>\n      </div>\n\n      <div class=\"col-2\">\n        <div mat-dialog-actions class=\"close-position\">\n          <button class=\"close\" mat-button (click)=\"onNoClick()\"><i class=\"fas fa-times\"></i></button>\n        </div>\n      </div>\n     </div>\n    <div mat-dialog-content>\n      <ng-content></ng-content>\n    </div>\n  </div>\n</ng-template>\n",
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    FilterComponent.ctorParameters = function () { return [
        { type: HotkeysService },
        { type: FilterService },
        { type: MatDialog }
    ]; };
    FilterComponent.propDecorators = {
        clickPesquisa: [{ type: Output }],
        disabled: [{ type: Input }],
        searchInput: [{ type: ViewChild, args: ['search',] }],
        template: [{ type: ViewChild, args: ['filterDialog',] }],
        filtersForm: [{ type: ContentChild, args: [FormGroupDirective,] }]
    };
    return FilterComponent;
}());
export { FilterComponent };
if (false) {
    /** @type {?} */
    FilterComponent.prototype.clickPesquisa;
    /** @type {?} */
    FilterComponent.prototype.disabled;
    /** @type {?} */
    FilterComponent.prototype.searchInput;
    /** @type {?} */
    FilterComponent.prototype.template;
    /** @type {?} */
    FilterComponent.prototype.filtersForm;
    /** @type {?} */
    FilterComponent.prototype.filterBadge$;
    /** @type {?} */
    FilterComponent.prototype.showFilters$;
    /**
     * @type {?}
     * @protected
     */
    FilterComponent.prototype.subscriptions;
    /** @type {?} */
    FilterComponent.prototype.dialogRef;
    /**
     * @type {?}
     * @protected
     */
    FilterComponent.prototype.hotkeysService;
    /**
     * @type {?}
     * @protected
     */
    FilterComponent.prototype.filterService;
    /** @type {?} */
    FilterComponent.prototype.dialog;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsdGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9maWx0ZXIvZmlsdGVyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUlMLFlBQVksRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLFdBQVcsRUFDN0UsTUFBTSxlQUFlLENBQUM7QUFFdkIsT0FBTyxFQUFDLGtCQUFrQixFQUFDLE1BQU0sZ0JBQWdCLENBQUM7QUFDbEQsT0FBTyxFQUFDLFNBQVMsRUFBZ0IsUUFBUSxFQUFDLE1BQU0sbUJBQW1CLENBQUM7QUFDcEUsT0FBTyxFQUFDLE1BQU0sRUFBRSxjQUFjLEVBQUMsTUFBTSxrQkFBa0IsQ0FBQztBQUN4RCxPQUFPLEVBQUMsYUFBYSxFQUFDLE1BQU0sa0JBQWtCLENBQUM7QUFFL0M7SUFpQkUseUJBQXNCLGNBQThCLEVBQzlCLGFBQTRCLEVBQy9CLE1BQWlCO1FBRmQsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBQy9CLFdBQU0sR0FBTixNQUFNLENBQVc7UUFaMUIsa0JBQWEsR0FBc0IsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUt6RCxpQkFBWSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDO1FBQy9DLGlCQUFZLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUM7UUFDNUMsa0JBQWEsR0FBbUIsRUFBRSxDQUFDO0lBTTdDLENBQUM7Ozs7SUFFRCxrQ0FBUTs7O0lBQVI7SUFDQSxDQUFDOzs7O0lBRUQseUNBQWU7OztJQUFmO1FBQUEsaUJBVUM7UUFUQyxJQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxPQUFPOzs7O1FBQUUsVUFBQyxLQUFvQjtZQUMvRCxLQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ3pCLE9BQU8sS0FBSyxDQUFDO1FBQ2YsQ0FBQyxFQUFDLENBQUMsQ0FBQztRQUVKLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLElBQUksTUFBTSxDQUFDLFlBQVk7Ozs7UUFBRSxVQUFDLEtBQW9CO1lBQ3BFLEtBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztZQUNyQixPQUFPLEtBQUssQ0FBQztRQUNmLENBQUMsRUFBQyxDQUFDLENBQUM7SUFDTixDQUFDOzs7O0lBRUQscUNBQVc7OztJQUFYO1FBQ0UsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQ3RCLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTzs7OztZQUFDLFVBQUEsWUFBWTtnQkFDckMsSUFBSSxZQUFZLElBQUksWUFBWSxDQUFDLE1BQU0sS0FBSyxLQUFLLEVBQUU7b0JBQ2pELFlBQVksQ0FBQyxXQUFXLEVBQUUsQ0FBQztpQkFDNUI7WUFDSCxDQUFDLEVBQUMsQ0FBQztTQUNKO0lBQ0gsQ0FBQzs7OztJQUVELG1DQUFTOzs7SUFBVDtRQUFBLGlCQUtDO1FBSkMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNoRCxJQUFJLENBQUMsYUFBYSxDQUFDLFNBQVM7OztRQUFDO1lBQzNCLEtBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDM0IsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDOzs7O0lBRUQsdUNBQWE7OztJQUFiO1FBQ0UsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDbkQsQ0FBQzs7OztJQUVELG1DQUFTOzs7SUFBVDtRQUNFLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDekIsQ0FBQzs7Z0JBNURGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsY0FBYztvQkFDeEIsaTVDQUFzQzs7aUJBRXZDOzs7O2dCQVBlLGNBQWM7Z0JBQ3RCLGFBQWE7Z0JBRmIsU0FBUzs7O2dDQVdkLE1BQU07MkJBQ04sS0FBSzs4QkFDTCxTQUFTLFNBQUMsUUFBUTsyQkFDbEIsU0FBUyxTQUFDLGNBQWM7OEJBQ3hCLFlBQVksU0FBQyxrQkFBa0I7O0lBa0RsQyxzQkFBQztDQUFBLEFBN0RELElBNkRDO1NBeERZLGVBQWU7OztJQUUxQix3Q0FBZ0U7O0lBQ2hFLG1DQUEyQjs7SUFDM0Isc0NBQTJDOztJQUMzQyxtQ0FBcUQ7O0lBQ3JELHNDQUFrRTs7SUFDbEUsdUNBQXNEOztJQUN0RCx1Q0FBc0Q7Ozs7O0lBQ3RELHdDQUE2Qzs7SUFDN0Msb0NBQTZCOzs7OztJQUVqQix5Q0FBd0M7Ozs7O0lBQ3hDLHdDQUFzQzs7SUFDdEMsaUNBQXdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcbiAgT25Jbml0LFxuICBPbkRlc3Ryb3ksXG4gIEFmdGVyVmlld0luaXQsXG4gIENvbnRlbnRDaGlsZCwgVmlld0NoaWxkLCBFdmVudEVtaXR0ZXIsIE91dHB1dCwgQ29tcG9uZW50LCBJbnB1dCwgVGVtcGxhdGVSZWZcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge1N1YnNjcmlwdGlvbn0gZnJvbSAncnhqcyc7XG5pbXBvcnQge0Zvcm1Hcm91cERpcmVjdGl2ZX0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHtNYXREaWFsb2csIE1hdERpYWxvZ1JlZiwgTWF0SW5wdXR9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcbmltcG9ydCB7SG90a2V5LCBIb3RrZXlzU2VydmljZX0gZnJvbSAnYW5ndWxhcjItaG90a2V5cyc7XG5pbXBvcnQge0ZpbHRlclNlcnZpY2V9IGZyb20gJy4vZmlsdGVyLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICd1aWtpdC1maWx0ZXInLFxuICB0ZW1wbGF0ZVVybDogJy4vZmlsdGVyLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vZmlsdGVyLmNvbXBvbmVudC5zY3NzJ10sXG59KVxuZXhwb3J0IGNsYXNzIEZpbHRlckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgQWZ0ZXJWaWV3SW5pdCwgT25EZXN0cm95IHtcblxuICBAT3V0cHV0KCkgY2xpY2tQZXNxdWlzYTogRXZlbnRFbWl0dGVyPGFueT4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIEBJbnB1dCgpIGRpc2FibGVkOiBib29sZWFuO1xuICBAVmlld0NoaWxkKCdzZWFyY2gnKSBzZWFyY2hJbnB1dDogTWF0SW5wdXQ7XG4gIEBWaWV3Q2hpbGQoJ2ZpbHRlckRpYWxvZycpIHRlbXBsYXRlOiBUZW1wbGF0ZVJlZjx7fT47XG4gIEBDb250ZW50Q2hpbGQoRm9ybUdyb3VwRGlyZWN0aXZlKSBmaWx0ZXJzRm9ybTogRm9ybUdyb3VwRGlyZWN0aXZlO1xuICBwdWJsaWMgZmlsdGVyQmFkZ2UkID0gdGhpcy5maWx0ZXJTZXJ2aWNlLmZpbHRlckJhZGdlJDtcbiAgcHVibGljIHNob3dGaWx0ZXJzJCA9IHRoaXMuZmlsdGVyU2VydmljZS5zaG93RmlsdGVycyQ7XG4gIHByb3RlY3RlZCBzdWJzY3JpcHRpb25zOiBTdWJzY3JpcHRpb25bXSA9IFtdO1xuICBkaWFsb2dSZWY6IE1hdERpYWxvZ1JlZjxhbnk+O1xuXG4gIGNvbnN0cnVjdG9yKHByb3RlY3RlZCBob3RrZXlzU2VydmljZTogSG90a2V5c1NlcnZpY2UsXG4gICAgICAgICAgICAgIHByb3RlY3RlZCBmaWx0ZXJTZXJ2aWNlOiBGaWx0ZXJTZXJ2aWNlLFxuICAgICAgICAgICAgICBwdWJsaWMgZGlhbG9nOiBNYXREaWFsb2cpIHtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICB9XG5cbiAgbmdBZnRlclZpZXdJbml0KCk6IHZvaWQge1xuICAgIHRoaXMuaG90a2V5c1NlcnZpY2UuYWRkKG5ldyBIb3RrZXkoJ2FsdCszJywgKGV2ZW50OiBLZXlib2FyZEV2ZW50KTogYm9vbGVhbiA9PiB7XG4gICAgICB0aGlzLnNlYXJjaElucHV0LmZvY3VzKCk7XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfSkpO1xuXG4gICAgdGhpcy5ob3RrZXlzU2VydmljZS5hZGQobmV3IEhvdGtleSgnY3RybCthbHQrMycsIChldmVudDogS2V5Ym9hcmRFdmVudCk6IGJvb2xlYW4gPT4ge1xuICAgICAgdGhpcy50b2dnbGVGaWx0ZXJzKCk7XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfSkpO1xuICB9XG5cbiAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XG4gICAgaWYgKHRoaXMuc3Vic2NyaXB0aW9ucykge1xuICAgICAgdGhpcy5zdWJzY3JpcHRpb25zLmZvckVhY2goc3Vic2NyaXB0aW9uID0+IHtcbiAgICAgICAgaWYgKHN1YnNjcmlwdGlvbiAmJiBzdWJzY3JpcHRpb24uY2xvc2VkID09PSBmYWxzZSkge1xuICAgICAgICAgIHN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9XG4gIH1cblxuICBwZXNxdWlzYXIoKSB7XG4gICAgdGhpcy5jbGlja1Blc3F1aXNhLmVtaXQodGhpcy5zZWFyY2hJbnB1dC52YWx1ZSk7XG4gICAgdGhpcy5jbGlja1Blc3F1aXNhLnN1YnNjcmliZSgoKSA9PiB7XG4gICAgICB0aGlzLnNlYXJjaElucHV0LmZvY3VzKCk7XG4gICAgfSk7XG4gIH1cblxuICB0b2dnbGVGaWx0ZXJzKCkge1xuICAgIHRoaXMuZGlhbG9nUmVmID0gdGhpcy5kaWFsb2cub3Blbih0aGlzLnRlbXBsYXRlKTtcbiAgfVxuXG4gIG9uTm9DbGljaygpOiB2b2lkIHtcbiAgICB0aGlzLmRpYWxvZ1JlZi5jbG9zZSgpO1xuICB9XG59XG4iXX0=