/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import * as i0 from "@angular/core";
var FilterService = /** @class */ (function () {
    function FilterService() {
        this.filterBadgeSubject = new BehaviorSubject(0);
        this.filterBadge$ = this.filterBadgeSubject.asObservable();
        this.showFiltersSubject = new BehaviorSubject(false);
        this.showFilters$ = this.showFiltersSubject.asObservable();
    }
    /**
     * @param {?} count
     * @return {?}
     */
    FilterService.prototype.setCountFilter = /**
     * @param {?} count
     * @return {?}
     */
    function (count) {
        this.filterBadgeSubject.next(count);
    };
    /**
     * @return {?}
     */
    FilterService.prototype.setShowFilters = /**
     * @return {?}
     */
    function () {
        this.showFiltersSubject.next(!this.showFiltersSubject.getValue());
    };
    FilterService.decorators = [
        { type: Injectable, args: [{ providedIn: 'root' },] }
    ];
    /** @nocollapse */
    FilterService.ctorParameters = function () { return []; };
    /** @nocollapse */ FilterService.ngInjectableDef = i0.defineInjectable({ factory: function FilterService_Factory() { return new FilterService(); }, token: FilterService, providedIn: "root" });
    return FilterService;
}());
export { FilterService };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    FilterService.prototype.filterBadgeSubject;
    /** @type {?} */
    FilterService.prototype.filterBadge$;
    /**
     * @type {?}
     * @protected
     */
    FilterService.prototype.showFiltersSubject;
    /** @type {?} */
    FilterService.prototype.showFilters$;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsdGVyLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvZmlsdGVyL2ZpbHRlci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsVUFBVSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBQyxlQUFlLEVBQUMsTUFBTSxNQUFNLENBQUM7O0FBRXJDO0lBUUU7UUFMVSx1QkFBa0IsR0FBRyxJQUFJLGVBQWUsQ0FBUyxDQUFDLENBQUMsQ0FBQztRQUN2RCxpQkFBWSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUNuRCx1QkFBa0IsR0FBRyxJQUFJLGVBQWUsQ0FBVSxLQUFLLENBQUMsQ0FBQztRQUM1RCxpQkFBWSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUc3RCxDQUFDOzs7OztJQUVELHNDQUFjOzs7O0lBQWQsVUFBZSxLQUFhO1FBQzFCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDdEMsQ0FBQzs7OztJQUVELHNDQUFjOzs7SUFBZDtRQUNFLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztJQUNwRSxDQUFDOztnQkFqQkYsVUFBVSxTQUFDLEVBQUMsVUFBVSxFQUFFLE1BQU0sRUFBQzs7Ozs7d0JBSGhDO0NBcUJDLEFBbEJELElBa0JDO1NBakJZLGFBQWE7Ozs7OztJQUV4QiwyQ0FBOEQ7O0lBQzlELHFDQUE2RDs7Ozs7SUFDN0QsMkNBQW1FOztJQUNuRSxxQ0FBNkQiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0luamVjdGFibGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtCZWhhdmlvclN1YmplY3R9IGZyb20gJ3J4anMnO1xuXG5ASW5qZWN0YWJsZSh7cHJvdmlkZWRJbjogJ3Jvb3QnfSlcbmV4cG9ydCBjbGFzcyBGaWx0ZXJTZXJ2aWNlIHtcblxuICBwcm90ZWN0ZWQgZmlsdGVyQmFkZ2VTdWJqZWN0ID0gbmV3IEJlaGF2aW9yU3ViamVjdDxudW1iZXI+KDApO1xuICBwdWJsaWMgZmlsdGVyQmFkZ2UkID0gdGhpcy5maWx0ZXJCYWRnZVN1YmplY3QuYXNPYnNlcnZhYmxlKCk7XG4gIHByb3RlY3RlZCBzaG93RmlsdGVyc1N1YmplY3QgPSBuZXcgQmVoYXZpb3JTdWJqZWN0PGJvb2xlYW4+KGZhbHNlKTtcbiAgcHVibGljIHNob3dGaWx0ZXJzJCA9IHRoaXMuc2hvd0ZpbHRlcnNTdWJqZWN0LmFzT2JzZXJ2YWJsZSgpO1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICB9XG5cbiAgc2V0Q291bnRGaWx0ZXIoY291bnQ6IG51bWJlcikge1xuICAgIHRoaXMuZmlsdGVyQmFkZ2VTdWJqZWN0Lm5leHQoY291bnQpO1xuICB9XG5cbiAgc2V0U2hvd0ZpbHRlcnMoKSB7XG4gICAgdGhpcy5zaG93RmlsdGVyc1N1YmplY3QubmV4dCghdGhpcy5zaG93RmlsdGVyc1N1YmplY3QuZ2V0VmFsdWUoKSk7XG4gIH1cbn1cbiJdfQ==