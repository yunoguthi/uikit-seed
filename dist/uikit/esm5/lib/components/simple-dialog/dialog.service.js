/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { SimpleDialogComponent } from './simple-dialog.component';
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/dialog";
var DialogService = /** @class */ (function () {
    function DialogService(dialog) {
        this.dialog = dialog;
    }
    /**
     * @param {?} description
     * @param {?=} items
     * @param {?=} title
     * @param {?=} width
     * @return {?}
     */
    DialogService.prototype.show = /**
     * @param {?} description
     * @param {?=} items
     * @param {?=} title
     * @param {?=} width
     * @return {?}
     */
    function (description, items, title, width) {
        if (title === void 0) { title = 'Confirmação'; }
        if (width === void 0) { width = '450px'; }
        /** @type {?} */
        var dialogRef = this.dialog.open(SimpleDialogComponent, {
            width: '450px',
            data: {
                title: title,
                description: description,
                items: items
            }
        });
        return dialogRef.afterClosed();
    };
    DialogService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    DialogService.ctorParameters = function () { return [
        { type: MatDialog }
    ]; };
    /** @nocollapse */ DialogService.ngInjectableDef = i0.defineInjectable({ factory: function DialogService_Factory() { return new DialogService(i0.inject(i1.MatDialog)); }, token: DialogService, providedIn: "root" });
    return DialogService;
}());
export { DialogService };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    DialogService.prototype.dialog;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGlhbG9nLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvc2ltcGxlLWRpYWxvZy9kaWFsb2cuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDOUMsT0FBTyxFQUFFLHFCQUFxQixFQUErQixNQUFNLDJCQUEyQixDQUFDOzs7QUFHL0Y7SUFLRSx1QkFBc0IsTUFBaUI7UUFBakIsV0FBTSxHQUFOLE1BQU0sQ0FBVztJQUFJLENBQUM7Ozs7Ozs7O0lBRTVDLDRCQUFJOzs7Ozs7O0lBQUosVUFBSyxXQUFtQixFQUFFLEtBQWEsRUFBRSxLQUE2QixFQUFFLEtBQXVCO1FBQXRELHNCQUFBLEVBQUEscUJBQTZCO1FBQUUsc0JBQUEsRUFBQSxlQUF1Qjs7WUFDdkYsU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFxRCxxQkFBcUIsRUFBRTtZQUM1RyxLQUFLLEVBQUUsT0FBTztZQUNkLElBQUksRUFBRTtnQkFDSixLQUFLLEVBQUUsS0FBSztnQkFDWixXQUFXLEVBQUUsV0FBVztnQkFDeEIsS0FBSyxFQUFFLEtBQUs7YUFDYjtTQUNGLENBQUM7UUFFRixPQUFPLFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUVqQyxDQUFDOztnQkFuQkYsVUFBVSxTQUFDO29CQUNWLFVBQVUsRUFBRSxNQUFNO2lCQUNuQjs7OztnQkFOUSxTQUFTOzs7d0JBRGxCO0NBeUJDLEFBcEJELElBb0JDO1NBakJZLGFBQWE7Ozs7OztJQUVaLCtCQUEyQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE1hdERpYWxvZyB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcbmltcG9ydCB7IFNpbXBsZURpYWxvZ0NvbXBvbmVudCwgRGlhbG9nUmVzdWx0LCBEaWFsb2dPcHRpb25zIH0gZnJvbSAnLi9zaW1wbGUtZGlhbG9nLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBJRCB9IGZyb20gJ0BkYXRvcmFtYS9ha2l0YSc7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIERpYWxvZ1NlcnZpY2Uge1xuXG4gIGNvbnN0cnVjdG9yKHByb3RlY3RlZCBkaWFsb2c6IE1hdERpYWxvZykgeyB9XG5cbiAgc2hvdyhkZXNjcmlwdGlvbjogc3RyaW5nLCBpdGVtcz86IGFueVtdLCB0aXRsZTogc3RyaW5nID0gJ0NvbmZpcm1hw6fDo28nLCB3aWR0aDogc3RyaW5nID0gJzQ1MHB4Jykge1xuICAgIGNvbnN0IGRpYWxvZ1JlZiA9IHRoaXMuZGlhbG9nLm9wZW48U2ltcGxlRGlhbG9nQ29tcG9uZW50LCBEaWFsb2dPcHRpb25zLCBEaWFsb2dSZXN1bHQ+KFNpbXBsZURpYWxvZ0NvbXBvbmVudCwge1xuICAgICAgd2lkdGg6ICc0NTBweCcsXG4gICAgICBkYXRhOiB7XG4gICAgICAgIHRpdGxlOiB0aXRsZSxcbiAgICAgICAgZGVzY3JpcHRpb246IGRlc2NyaXB0aW9uLFxuICAgICAgICBpdGVtczogaXRlbXNcbiAgICAgIH1cbiAgICB9KTtcblxuICAgIHJldHVybiBkaWFsb2dSZWYuYWZ0ZXJDbG9zZWQoKTtcblxuICB9XG59XG4iXX0=