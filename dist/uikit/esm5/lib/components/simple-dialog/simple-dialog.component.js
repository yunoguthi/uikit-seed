/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
/** @enum {string} */
var DialogResult = {
    Confirmed: 'Confirmado',
    Cancelled: 'Cancelado',
};
export { DialogResult };
/** @enum {number} */
var ButtonDialog = {
    YesNo: 0,
    OkCancel: 1,
};
export { ButtonDialog };
ButtonDialog[ButtonDialog.YesNo] = 'YesNo';
ButtonDialog[ButtonDialog.OkCancel] = 'OkCancel';
/**
 * @record
 */
export function DialogOptions() { }
if (false) {
    /** @type {?} */
    DialogOptions.prototype.title;
    /** @type {?} */
    DialogOptions.prototype.description;
    /** @type {?|undefined} */
    DialogOptions.prototype.items;
    /** @type {?|undefined} */
    DialogOptions.prototype.button;
}
// @dynamic
var SimpleDialogComponent = /** @class */ (function () {
    function SimpleDialogComponent(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.cancelButtonText = 'Cancelar';
        this.confirmButtonText = 'Ok';
        if (data.button === ButtonDialog.YesNo) {
            this.confirmButtonText = 'Sim';
            this.cancelButtonText = 'Não';
        }
        if (data.items && data.items.length > 0) {
            this.columns = Object.keys(data.items[0]);
        }
    }
    /**
     * @return {?}
     */
    SimpleDialogComponent.prototype.onCancelClick = /**
     * @return {?}
     */
    function () {
        this.dialogRef.close(DialogResult.Cancelled);
    };
    /**
     * @return {?}
     */
    SimpleDialogComponent.prototype.onConfirmClick = /**
     * @return {?}
     */
    function () {
        this.dialogRef.close(DialogResult.Confirmed);
    };
    /**
     * @return {?}
     */
    SimpleDialogComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    SimpleDialogComponent.decorators = [
        { type: Component, args: [{
                    selector: 'uikit-simple-dialog',
                    template: "<h3 class=\"mat-dialog-title\">{{ data.title }}</h3>\n<div class=\"mat-dialog-content mat-typography\">\n  <p>{{ data.description }}</p>\n  <mat-list dense *ngIf=\"data.items != null && data.items.length > 0\">\n\n\n    <table mat-table #table [dataSource]=\"data.items\" mdSort>\n      <ng-container *ngFor=\"let col of columns\" matColumnDef={{col}}>\n        <th mat-header-cell *matHeaderCellDef md-sort-header> {{ col }}</th>\n        <td mat-cell *matCellDef=\"let row\"> {{row[col]}}</td>\n      </ng-container>\n      <tr mat-header-row *matHeaderRowDef=\"columns\"></tr>\n      <tr mat-row *matRowDef=\"let row; columns: columns;\"></tr>\n    </table>\n\n\n  </mat-list>\n</div>\n<div class=\"mat-dialog-actions\" align=\"end\">\n  <button mat-button (click)=\"onCancelClick()\">{{ cancelButtonText }}</button>\n  <button mat-button (click)=\"onConfirmClick()\" cdkFocusInitial>{{ confirmButtonText }}</button>\n</div>\n",
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    SimpleDialogComponent.ctorParameters = function () { return [
        { type: MatDialogRef },
        { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] }
    ]; };
    return SimpleDialogComponent;
}());
export { SimpleDialogComponent };
if (false) {
    /** @type {?} */
    SimpleDialogComponent.prototype.columns;
    /** @type {?} */
    SimpleDialogComponent.prototype.cancelButtonText;
    /** @type {?} */
    SimpleDialogComponent.prototype.confirmButtonText;
    /** @type {?} */
    SimpleDialogComponent.prototype.dialogRef;
    /** @type {?} */
    SimpleDialogComponent.prototype.data;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2ltcGxlLWRpYWxvZy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvc2ltcGxlLWRpYWxvZy9zaW1wbGUtZGlhbG9nLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDMUQsT0FBTyxFQUFFLFlBQVksRUFBRSxlQUFlLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQzs7O0lBSXZFLFdBQVksWUFBWTtJQUN4QixXQUFZLFdBQVc7Ozs7O0lBSXZCLFFBQUs7SUFDTCxXQUFROzs7Ozs7OztBQUdWLG1DQUtDOzs7SUFKQyw4QkFBYzs7SUFDZCxvQ0FBb0I7O0lBQ3BCLDhCQUFjOztJQUNkLCtCQUFzQjs7O0FBSXhCO0lBWUUsK0JBQ1MsU0FBMEMsRUFDakIsSUFBbUI7UUFENUMsY0FBUyxHQUFULFNBQVMsQ0FBaUM7UUFDakIsU0FBSSxHQUFKLElBQUksQ0FBZTtRQUxyRCxxQkFBZ0IsR0FBRyxVQUFVLENBQUM7UUFDOUIsc0JBQWlCLEdBQUcsSUFBSSxDQUFDO1FBTXZCLElBQUksSUFBSSxDQUFDLE1BQU0sS0FBSyxZQUFZLENBQUMsS0FBSyxFQUFFO1lBQ3RDLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUM7WUFDL0IsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQztTQUMvQjtRQUNELElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDdkMsSUFBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUMzQztJQUNILENBQUM7Ozs7SUFFRCw2Q0FBYTs7O0lBQWI7UUFDRSxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDL0MsQ0FBQzs7OztJQUVELDhDQUFjOzs7SUFBZDtRQUNFLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUMvQyxDQUFDOzs7O0lBR0Qsd0NBQVE7OztJQUFSO0lBRUEsQ0FBQzs7Z0JBcENGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUscUJBQXFCO29CQUMvQiw2NkJBQTZDOztpQkFFOUM7Ozs7Z0JBekJRLFlBQVk7Z0RBbUNoQixNQUFNLFNBQUMsZUFBZTs7SUF3QjNCLDRCQUFDO0NBQUEsQUF0Q0QsSUFzQ0M7U0FqQ1kscUJBQXFCOzs7SUFFaEMsd0NBQWtCOztJQUVsQixpREFBOEI7O0lBQzlCLGtEQUF5Qjs7SUFHdkIsMENBQWlEOztJQUNqRCxxQ0FBbUQiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5qZWN0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBNYXREaWFsb2dSZWYsIE1BVF9ESUFMT0dfREFUQSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2RpYWxvZyc7XG5pbXBvcnQgeyBJRCB9IGZyb20gJ0BkYXRvcmFtYS9ha2l0YSc7XG5cbmV4cG9ydCBlbnVtIERpYWxvZ1Jlc3VsdCB7XG4gIENvbmZpcm1lZCA9ICdDb25maXJtYWRvJyxcbiAgQ2FuY2VsbGVkID0gJ0NhbmNlbGFkbydcbn1cblxuZXhwb3J0IGVudW0gQnV0dG9uRGlhbG9nIHtcbiAgWWVzTm8sXG4gIE9rQ2FuY2VsXG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgRGlhbG9nT3B0aW9ucyB7XG4gIHRpdGxlOiBzdHJpbmc7XG4gIGRlc2NyaXB0aW9uOiBzdHJpbmc7XG4gIGl0ZW1zPzogYW55W107XG4gIGJ1dHRvbj86IEJ1dHRvbkRpYWxvZztcbn1cblxuLy8gQGR5bmFtaWNcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ3Vpa2l0LXNpbXBsZS1kaWFsb2cnLFxuICB0ZW1wbGF0ZVVybDogJy4vc2ltcGxlLWRpYWxvZy5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL3NpbXBsZS1kaWFsb2cuY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBTaW1wbGVEaWFsb2dDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIGNvbHVtbnM6IHN0cmluZ1tdO1xuXG4gIGNhbmNlbEJ1dHRvblRleHQgPSAnQ2FuY2VsYXInO1xuICBjb25maXJtQnV0dG9uVGV4dCA9ICdPayc7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHVibGljIGRpYWxvZ1JlZjogTWF0RGlhbG9nUmVmPGFueSwgRGlhbG9nUmVzdWx0PixcbiAgICBASW5qZWN0KE1BVF9ESUFMT0dfREFUQSkgcHVibGljIGRhdGE6IERpYWxvZ09wdGlvbnNcbiAgKSB7XG4gICAgaWYgKGRhdGEuYnV0dG9uID09PSBCdXR0b25EaWFsb2cuWWVzTm8pIHtcbiAgICAgIHRoaXMuY29uZmlybUJ1dHRvblRleHQgPSAnU2ltJztcbiAgICAgIHRoaXMuY2FuY2VsQnV0dG9uVGV4dCA9ICdOw6NvJztcbiAgICB9XG4gICAgaWYgKGRhdGEuaXRlbXMgJiYgZGF0YS5pdGVtcy5sZW5ndGggPiAwKSB7XG4gICAgICB0aGlzLmNvbHVtbnMgPSBPYmplY3Qua2V5cyhkYXRhLml0ZW1zWzBdKTtcbiAgICB9XG4gIH1cblxuICBvbkNhbmNlbENsaWNrKCk6IHZvaWQge1xuICAgIHRoaXMuZGlhbG9nUmVmLmNsb3NlKERpYWxvZ1Jlc3VsdC5DYW5jZWxsZWQpO1xuICB9XG5cbiAgb25Db25maXJtQ2xpY2soKTogdm9pZCB7XG4gICAgdGhpcy5kaWFsb2dSZWYuY2xvc2UoRGlhbG9nUmVzdWx0LkNvbmZpcm1lZCk7XG4gIH1cblxuXG4gIG5nT25Jbml0KCkge1xuXG4gIH1cblxufVxuIl19