/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { fromEvent, throwError } from 'rxjs';
import { mapTo, retryWhen, switchMap } from 'rxjs/operators';
var OfflineInterceptor = /** @class */ (function () {
    function OfflineInterceptor() {
        this.onlineChanges$ = fromEvent(window, 'online').pipe(mapTo(true));
    }
    Object.defineProperty(OfflineInterceptor.prototype, "isOnline", {
        get: /**
         * @return {?}
         */
        function () {
            return navigator.onLine;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    OfflineInterceptor.prototype.intercept = /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    function (req, next) {
        var _this = this;
        return next.handle(req)
            .pipe(retryWhen((/**
         * @param {?} errors
         * @return {?}
         */
        function (errors) {
            if (_this.isOnline) {
                return errors.pipe(switchMap((/**
                 * @param {?} err
                 * @return {?}
                 */
                function (err) { return throwError(err); })));
            }
            else if (req.method === 'GET') {
                // Não vamos fazer retry se não for GET (imutabilidade)
                return _this.onlineChanges$;
            }
        })));
    };
    OfflineInterceptor.decorators = [
        { type: Injectable }
    ];
    return OfflineInterceptor;
}());
export { OfflineInterceptor };
if (false) {
    /**
     * @type {?}
     * @private
     */
    OfflineInterceptor.prototype.onlineChanges$;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib2ZmbGluZS1pbnRlcmNlcHRvci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvb2ZmbGluZS9vZmZsaW5lLWludGVyY2VwdG9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFjLE1BQU0sTUFBTSxDQUFDO0FBQ3pELE9BQU8sRUFBRSxLQUFLLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBVSxNQUFNLGdCQUFnQixDQUFDO0FBR3JFO0lBQUE7UUFFVSxtQkFBYyxHQUFHLFNBQVMsQ0FBQyxNQUFNLEVBQUUsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO0lBbUJ6RSxDQUFDO0lBakJDLHNCQUFJLHdDQUFROzs7O1FBQVo7WUFDRSxPQUFPLFNBQVMsQ0FBQyxNQUFNLENBQUM7UUFDMUIsQ0FBQzs7O09BQUE7Ozs7OztJQUVELHNDQUFTOzs7OztJQUFULFVBQVUsR0FBcUIsRUFBRSxJQUFpQjtRQUFsRCxpQkFZQztRQVhDLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUM7YUFDdEIsSUFBSSxDQUNILFNBQVM7Ozs7UUFBQyxVQUFBLE1BQU07WUFDZCxJQUFJLEtBQUksQ0FBQyxRQUFRLEVBQUU7Z0JBQ2pCLE9BQU8sTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTOzs7O2dCQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsVUFBVSxDQUFDLEdBQUcsQ0FBQyxFQUFmLENBQWUsRUFBQyxDQUFDLENBQUM7YUFDdkQ7aUJBQU0sSUFBSSxHQUFHLENBQUMsTUFBTSxLQUFLLEtBQUssRUFBRTtnQkFDL0IsdURBQXVEO2dCQUN2RCxPQUFPLEtBQUksQ0FBQyxjQUFjLENBQUM7YUFDNUI7UUFDSCxDQUFDLEVBQUMsQ0FDSCxDQUFDO0lBQ0osQ0FBQzs7Z0JBcEJGLFVBQVU7O0lBcUJYLHlCQUFDO0NBQUEsQUFyQkQsSUFxQkM7U0FwQlksa0JBQWtCOzs7Ozs7SUFDN0IsNENBQXVFIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgZnJvbUV2ZW50LCB0aHJvd0Vycm9yLCBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBtYXBUbywgcmV0cnlXaGVuLCBzd2l0Y2hNYXAsIGZpbHRlciB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcbmltcG9ydCB7IEh0dHBJbnRlcmNlcHRvciwgSHR0cFJlcXVlc3QsIEh0dHBIYW5kbGVyLCBIdHRwRXZlbnQgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBPZmZsaW5lSW50ZXJjZXB0b3IgaW1wbGVtZW50cyBIdHRwSW50ZXJjZXB0b3Ige1xuICBwcml2YXRlIG9ubGluZUNoYW5nZXMkID0gZnJvbUV2ZW50KHdpbmRvdywgJ29ubGluZScpLnBpcGUobWFwVG8odHJ1ZSkpO1xuXG4gIGdldCBpc09ubGluZSgpIHtcbiAgICByZXR1cm4gbmF2aWdhdG9yLm9uTGluZTtcbiAgfVxuXG4gIGludGVyY2VwdChyZXE6IEh0dHBSZXF1ZXN0PGFueT4sIG5leHQ6IEh0dHBIYW5kbGVyKTogT2JzZXJ2YWJsZTxIdHRwRXZlbnQ8YW55Pj4ge1xuICAgIHJldHVybiBuZXh0LmhhbmRsZShyZXEpXG4gICAgLnBpcGUoXG4gICAgICByZXRyeVdoZW4oZXJyb3JzID0+IHtcbiAgICAgICAgaWYgKHRoaXMuaXNPbmxpbmUpIHtcbiAgICAgICAgICByZXR1cm4gZXJyb3JzLnBpcGUoc3dpdGNoTWFwKGVyciA9PiB0aHJvd0Vycm9yKGVycikpKTtcbiAgICAgICAgfSBlbHNlIGlmIChyZXEubWV0aG9kID09PSAnR0VUJykge1xuICAgICAgICAgIC8vIE7Do28gdmFtb3MgZmF6ZXIgcmV0cnkgc2UgbsOjbyBmb3IgR0VUIChpbXV0YWJpbGlkYWRlKVxuICAgICAgICAgIHJldHVybiB0aGlzLm9ubGluZUNoYW5nZXMkO1xuICAgICAgICB9XG4gICAgICB9KVxuICAgICk7XG4gIH1cbn1cbiJdfQ==