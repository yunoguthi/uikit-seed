/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Location } from '@angular/common';
var NotFoundComponent = /** @class */ (function () {
    function NotFoundComponent(ngLocation) {
        this.ngLocation = ngLocation;
        this.imageSrc = require('@cnj/uikit/lib/theme/uikit/images/error_404.png');
    }
    /**
     * @return {?}
     */
    NotFoundComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () { };
    /**
     * @return {?}
     */
    NotFoundComponent.prototype.goBack = /**
     * @return {?}
     */
    function () {
        this.ngLocation.back();
    };
    NotFoundComponent.decorators = [
        { type: Component, args: [{
                    selector: 'uikit-not-found',
                    template: "<div class=\"container-fluid\">\n  <div class=\"row\">\n    <div class=\"col\">\n      <div class=\"error\">\n        <img src=\"{{ imageSrc }}\" />\n        <h3><b>Oops!</b> P\u00E1gina n\u00E3o encontrada</h3>\n        <button mat-raised-button color=\"primary\" (click)=\"goBack()\">Clique aqui para voltar</button>\n      </div>\n    </div>\n  </div>\n</div>\n",
                    changeDetection: ChangeDetectionStrategy.OnPush,
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    NotFoundComponent.ctorParameters = function () { return [
        { type: Location }
    ]; };
    return NotFoundComponent;
}());
export { NotFoundComponent };
if (false) {
    /** @type {?} */
    NotFoundComponent.prototype.imageSrc;
    /**
     * @type {?}
     * @private
     */
    NotFoundComponent.prototype.ngLocation;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90LWZvdW5kLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvbm90LWZvdW5kL25vdC1mb3VuZC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFDTCxTQUFTLEVBR1QsdUJBQXVCLEVBQ3hCLE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUkzQztJQVFFLDJCQUNVLFVBQW9CO1FBQXBCLGVBQVUsR0FBVixVQUFVLENBQVU7UUFGOUIsYUFBUSxHQUFHLE9BQU8sQ0FBQyxpREFBaUQsQ0FBQyxDQUFDO0lBR2xFLENBQUM7Ozs7SUFFTCxvQ0FBUTs7O0lBQVIsY0FBWSxDQUFDOzs7O0lBRU4sa0NBQU07OztJQUFiO1FBQ0UsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUN6QixDQUFDOztnQkFoQkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxpQkFBaUI7b0JBQzNCLHdYQUF5QztvQkFFekMsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07O2lCQUNoRDs7OztnQkFUUSxRQUFROztJQXNCakIsd0JBQUM7Q0FBQSxBQWxCRCxJQWtCQztTQVpZLGlCQUFpQjs7O0lBQzVCLHFDQUFzRTs7Ozs7SUFFcEUsdUNBQTRCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcbiAgQ29tcG9uZW50LFxuICBPbkluaXQsXG4gIFZpZXdFbmNhcHN1bGF0aW9uLFxuICBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneVxufSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IExvY2F0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcblxuZGVjbGFyZSBmdW5jdGlvbiByZXF1aXJlKHBhdGg6IHN0cmluZyk7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ3Vpa2l0LW5vdC1mb3VuZCcsXG4gIHRlbXBsYXRlVXJsOiAnLi9ub3QtZm91bmQuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9ub3QtZm91bmQuY29tcG9uZW50LnNjc3MnXSxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2hcbn0pXG5leHBvcnQgY2xhc3MgTm90Rm91bmRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBpbWFnZVNyYyA9IHJlcXVpcmUoJ0BjbmovdWlraXQvbGliL3RoZW1lL3Vpa2l0L2ltYWdlcy9lcnJvcl80MDQucG5nJyk7XG4gIGNvbnN0cnVjdG9yKFxuICAgIHByaXZhdGUgbmdMb2NhdGlvbjogTG9jYXRpb25cbiAgKSB7IH1cblxuICBuZ09uSW5pdCgpIHt9XG5cbiAgcHVibGljIGdvQmFjaygpIHtcbiAgICB0aGlzLm5nTG9jYXRpb24uYmFjaygpO1xuICB9XG5cbn1cbiJdfQ==