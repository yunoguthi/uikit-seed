/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { MatPaginatorIntl } from '@angular/material';
var MatPaginatorIntlPtBr = /** @class */ (function (_super) {
    tslib_1.__extends(MatPaginatorIntlPtBr, _super);
    function MatPaginatorIntlPtBr() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.itemsPerPageLabel = 'Items por página';
        _this.nextPageLabel = 'Próxima página';
        _this.previousPageLabel = 'Página anterior';
        _this.getRangeLabel = (/**
         * @param {?} page
         * @param {?} pageSize
         * @param {?} length
         * @return {?}
         */
        function (page, pageSize, length) {
            if (length === 0 || pageSize === 0) {
                return '0 de ' + length;
            }
            length = Math.max(length, 0);
            /** @type {?} */
            var startIndex = page * pageSize;
            /** @type {?} */
            var endIndex = startIndex < length ?
                Math.min(startIndex + pageSize, length) :
                startIndex + pageSize;
            return startIndex + 1 + ' - ' + endIndex + ' de  ' + length;
        });
        return _this;
    }
    return MatPaginatorIntlPtBr;
}(MatPaginatorIntl));
export { MatPaginatorIntlPtBr };
if (false) {
    /** @type {?} */
    MatPaginatorIntlPtBr.prototype.itemsPerPageLabel;
    /** @type {?} */
    MatPaginatorIntlPtBr.prototype.nextPageLabel;
    /** @type {?} */
    MatPaginatorIntlPtBr.prototype.previousPageLabel;
    /** @type {?} */
    MatPaginatorIntlPtBr.prototype.getRangeLabel;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWF0LXBhZ2luYXRvci1wdGJyLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi91dGlscy9tYXQtcGFnaW5hdG9yLXB0YnIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUMsZ0JBQWdCLEVBQUMsTUFBTSxtQkFBbUIsQ0FBQztBQUNuRDtJQUEwQyxnREFBZ0I7SUFBMUQ7UUFBQSxxRUFnQkM7UUFmQyx1QkFBaUIsR0FBRyxrQkFBa0IsQ0FBQztRQUN2QyxtQkFBYSxHQUFPLGdCQUFnQixDQUFDO1FBQ3JDLHVCQUFpQixHQUFHLGlCQUFpQixDQUFDO1FBRXRDLG1CQUFhOzs7Ozs7UUFBRyxVQUFDLElBQUksRUFBRSxRQUFRLEVBQUUsTUFBTTtZQUNyQyxJQUFJLE1BQU0sS0FBSyxDQUFDLElBQUksUUFBUSxLQUFLLENBQUMsRUFBRTtnQkFDbEMsT0FBTyxPQUFPLEdBQUcsTUFBTSxDQUFDO2FBQ3pCO1lBQ0QsTUFBTSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQyxDQUFDOztnQkFDdkIsVUFBVSxHQUFHLElBQUksR0FBRyxRQUFROztnQkFDNUIsUUFBUSxHQUFHLFVBQVUsR0FBRyxNQUFNLENBQUMsQ0FBQztnQkFDcEMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFVLEdBQUcsUUFBUSxFQUFFLE1BQU0sQ0FBQyxDQUFDLENBQUM7Z0JBQ3pDLFVBQVUsR0FBRyxRQUFRO1lBQ3ZCLE9BQU8sVUFBVSxHQUFHLENBQUMsR0FBRyxLQUFLLEdBQUcsUUFBUSxHQUFHLE9BQU8sR0FBRyxNQUFNLENBQUM7UUFDOUQsQ0FBQyxFQUFDOztJQUNKLENBQUM7SUFBRCwyQkFBQztBQUFELENBQUMsQUFoQkQsQ0FBMEMsZ0JBQWdCLEdBZ0J6RDs7OztJQWZDLGlEQUF1Qzs7SUFDdkMsNkNBQXFDOztJQUNyQyxpREFBc0M7O0lBRXRDLDZDQVVFIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtNYXRQYWdpbmF0b3JJbnRsfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XG5leHBvcnQgY2xhc3MgTWF0UGFnaW5hdG9ySW50bFB0QnIgZXh0ZW5kcyBNYXRQYWdpbmF0b3JJbnRsIHtcbiAgaXRlbXNQZXJQYWdlTGFiZWwgPSAnSXRlbXMgcG9yIHDDoWdpbmEnO1xuICBuZXh0UGFnZUxhYmVsICAgICA9ICdQcsOzeGltYSBww6FnaW5hJztcbiAgcHJldmlvdXNQYWdlTGFiZWwgPSAnUMOhZ2luYSBhbnRlcmlvcic7XG5cbiAgZ2V0UmFuZ2VMYWJlbCA9IChwYWdlLCBwYWdlU2l6ZSwgbGVuZ3RoKSA9PiB7XG4gICAgaWYgKGxlbmd0aCA9PT0gMCB8fCBwYWdlU2l6ZSA9PT0gMCkge1xuICAgICAgcmV0dXJuICcwIGRlICcgKyBsZW5ndGg7XG4gICAgfVxuICAgIGxlbmd0aCA9IE1hdGgubWF4KGxlbmd0aCwgMCk7XG4gICAgY29uc3Qgc3RhcnRJbmRleCA9IHBhZ2UgKiBwYWdlU2l6ZTtcbiAgICBjb25zdCBlbmRJbmRleCA9IHN0YXJ0SW5kZXggPCBsZW5ndGggP1xuICAgICAgTWF0aC5taW4oc3RhcnRJbmRleCArIHBhZ2VTaXplLCBsZW5ndGgpIDpcbiAgICAgIHN0YXJ0SW5kZXggKyBwYWdlU2l6ZTtcbiAgICByZXR1cm4gc3RhcnRJbmRleCArIDEgKyAnIC0gJyArIGVuZEluZGV4ICsgJyBkZSAgJyArIGxlbmd0aDtcbiAgfTtcbn1cbiJdfQ==