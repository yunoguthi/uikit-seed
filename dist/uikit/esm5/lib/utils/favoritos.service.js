/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, InjectionToken } from '@angular/core';
/** @type {?} */
export var FAVORITOS_SERVICE_TOKEN = new InjectionToken('FavoritosService');
/**
 * @abstract
 */
var FavoritosService = /** @class */ (function () {
    function FavoritosService() {
    }
    FavoritosService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    FavoritosService.ctorParameters = function () { return []; };
    return FavoritosService;
}());
export { FavoritosService };
if (false) {
    /**
     * Salva os favoritos do usuário
     * @abstract
     * @param {?} itens Itens da lista de favoritos
     * @return {?}
     */
    FavoritosService.prototype.salvar = function (itens) { };
    /**
     * Retorna os favoritos do usuário
     * @abstract
     * @return {?}
     */
    FavoritosService.prototype.buscar = function () { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmF2b3JpdG9zLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL3V0aWxzL2Zhdm9yaXRvcy5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLGNBQWMsRUFBRSxNQUFNLGVBQWUsQ0FBQzs7QUFJM0QsTUFBTSxLQUFPLHVCQUF1QixHQUFHLElBQUksY0FBYyxDQUFtQixrQkFBa0IsQ0FBQzs7OztBQUUvRjtJQUVFO0lBQWdCLENBQUM7O2dCQUZsQixVQUFVOzs7O0lBYVgsdUJBQUM7Q0FBQSxBQWJELElBYUM7U0FacUIsZ0JBQWdCOzs7Ozs7OztJQU1wQyx5REFBcUQ7Ozs7OztJQUtyRCxvREFBMEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBJbmplY3Rpb25Ub2tlbiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTWVudUl0ZW0gfSBmcm9tICcuLi9sYXlvdXQvbmF2L21lbnUvbWVudS1pdGVtL21lbnUtaXRlbS5tb2RlbCc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5cbmV4cG9ydCBjb25zdCBGQVZPUklUT1NfU0VSVklDRV9UT0tFTiA9IG5ldyBJbmplY3Rpb25Ub2tlbjxGYXZvcml0b3NTZXJ2aWNlPignRmF2b3JpdG9zU2VydmljZScpO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgRmF2b3JpdG9zU2VydmljZSB7XG4gIGNvbnN0cnVjdG9yKCkgeyB9XG4gIC8qKlxuICAgKiBTYWx2YSBvcyBmYXZvcml0b3MgZG8gdXN1w6FyaW9cbiAgICogQHBhcmFtIGl0ZW5zIEl0ZW5zIGRhIGxpc3RhIGRlIGZhdm9yaXRvc1xuICAgKi9cbiAgYWJzdHJhY3Qgc2FsdmFyKGl0ZW5zOiBNZW51SXRlbVtdKTogT2JzZXJ2YWJsZTx2b2lkPjtcblxuICAvKipcbiAgICogUmV0b3JuYSBvcyBmYXZvcml0b3MgZG8gdXN1w6FyaW9cbiAgICovXG4gIGFic3RyYWN0IGJ1c2NhcigpOiBPYnNlcnZhYmxlPE1lbnVJdGVtW10+O1xufVxuIl19