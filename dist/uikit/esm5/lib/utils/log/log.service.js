/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { BehaviorSubject, Subscription } from 'rxjs';
import { Injectable, Inject, InjectionToken, Optional } from '@angular/core';
/** @enum {number} */
var LogLevel = {
    Debug: 0,
    Info: 1,
    Warn: 2,
    Error: 3,
};
export { LogLevel };
LogLevel[LogLevel.Debug] = 'Debug';
LogLevel[LogLevel.Info] = 'Info';
LogLevel[LogLevel.Warn] = 'Warn';
LogLevel[LogLevel.Error] = 'Error';
/**
 * @record
 */
export function Log() { }
if (false) {
    /** @type {?} */
    Log.prototype.message;
    /** @type {?} */
    Log.prototype.level;
}
var LogService = /** @class */ (function () {
    function LogService() {
        this.loggerSubject$ = new BehaviorSubject({ message: 'Log iniciado!', level: LogLevel.Debug });
        this.logger$ = this.loggerSubject$.asObservable();
    }
    /**
     * @private
     * @param {...?} infos
     * @return {?}
     */
    LogService.prototype.infosToMessage = /**
     * @private
     * @param {...?} infos
     * @return {?}
     */
    function () {
        var infos = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            infos[_i] = arguments[_i];
        }
        return infos.join(' ');
    };
    /**
     * @protected
     * @param {?=} level
     * @param {...?} infos
     * @return {?}
     */
    LogService.prototype.log = /**
     * @protected
     * @param {?=} level
     * @param {...?} infos
     * @return {?}
     */
    function (level) {
        if (level === void 0) { level = LogLevel.Debug; }
        var infos = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            infos[_i - 1] = arguments[_i];
        }
        /** @type {?} */
        var message = this.infosToMessage.apply(this, tslib_1.__spread(infos));
        this.loggerSubject$.next({ message: message, level: level });
    };
    /**
     * @param {...?} data
     * @return {?}
     */
    LogService.prototype.debug = /**
     * @param {...?} data
     * @return {?}
     */
    function () {
        var data = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            data[_i] = arguments[_i];
        }
        this.log.apply(this, tslib_1.__spread([LogLevel.Debug], data));
    };
    /**
     * @param {...?} data
     * @return {?}
     */
    LogService.prototype.info = /**
     * @param {...?} data
     * @return {?}
     */
    function () {
        var data = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            data[_i] = arguments[_i];
        }
        this.log.apply(this, tslib_1.__spread([LogLevel.Info], data));
    };
    /**
     * @param {...?} data
     * @return {?}
     */
    LogService.prototype.warn = /**
     * @param {...?} data
     * @return {?}
     */
    function () {
        var data = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            data[_i] = arguments[_i];
        }
        this.log.apply(this, tslib_1.__spread([LogLevel.Warn], data));
    };
    /**
     * @param {...?} data
     * @return {?}
     */
    LogService.prototype.error = /**
     * @param {...?} data
     * @return {?}
     */
    function () {
        var data = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            data[_i] = arguments[_i];
        }
        this.log.apply(this, tslib_1.__spread([LogLevel.Error], data));
    };
    LogService.decorators = [
        { type: Injectable }
    ];
    return LogService;
}());
export { LogService };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    LogService.prototype.loggerSubject$;
    /** @type {?} */
    LogService.prototype.logger$;
}
/** @type {?} */
export var LOG_CONSUMER_SERVICE = new InjectionToken('LOG_CONSUMER_SERVICE');
var LogConsumersService = /** @class */ (function () {
    function LogConsumersService(logService, services) {
        this.logService = logService;
        if (services && services.length > 0) {
            services.forEach((/**
             * @param {?} service
             * @return {?}
             */
            function (service) { return service.configure(logService); }));
        }
    }
    LogConsumersService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    LogConsumersService.ctorParameters = function () { return [
        { type: LogService },
        { type: Array, decorators: [{ type: Inject, args: [LOG_CONSUMER_SERVICE,] }, { type: Optional }] }
    ]; };
    return LogConsumersService;
}());
export { LogConsumersService };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    LogConsumersService.prototype.logService;
}
/**
 * @abstract
 */
var /**
 * @abstract
 */
LogConsumer = /** @class */ (function () {
    function LogConsumer() {
        this.logSubscription = new Subscription();
        this.minimalConsumerLevel = LogLevel.Debug;
    }
    /**
     * @protected
     * @param {?} log
     * @return {?}
     */
    LogConsumer.prototype.consume = /**
     * @protected
     * @param {?} log
     * @return {?}
     */
    function (log) {
        if (log.level >= this.minimalConsumerLevel) {
            if (log.level > 0) {
                this.consumeDebug(log.message);
            }
            else if (log.level > 1) {
                this.consumeInfo(log.message);
            }
            else if (log.level > 2) {
                this.consumeWarn(log.message);
            }
            else if (log.level > 3) {
                this.consumeError(log.message);
            }
        }
    };
    /**
     * @param {?} logService
     * @return {?}
     */
    LogConsumer.prototype.configure = /**
     * @param {?} logService
     * @return {?}
     */
    function (logService) {
        var _this = this;
        this.logSubscription.add(logService.logger$.subscribe((/**
         * @param {?} log
         * @return {?}
         */
        function (log) { return _this.consume(log); })));
    };
    /**
     * @return {?}
     */
    LogConsumer.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        if (this.logSubscription && !this.logSubscription.closed) {
            this.logSubscription.unsubscribe();
        }
    };
    return LogConsumer;
}());
/**
 * @abstract
 */
export { LogConsumer };
if (false) {
    /**
     * @type {?}
     * @private
     */
    LogConsumer.prototype.logSubscription;
    /** @type {?} */
    LogConsumer.prototype.minimalConsumerLevel;
    /**
     * @abstract
     * @protected
     * @param {?} message
     * @return {?}
     */
    LogConsumer.prototype.consumeDebug = function (message) { };
    /**
     * @abstract
     * @protected
     * @param {?} message
     * @return {?}
     */
    LogConsumer.prototype.consumeInfo = function (message) { };
    /**
     * @abstract
     * @protected
     * @param {?} message
     * @return {?}
     */
    LogConsumer.prototype.consumeWarn = function (message) { };
    /**
     * @abstract
     * @protected
     * @param {?} message
     * @return {?}
     */
    LogConsumer.prototype.consumeError = function (message) { };
}
var LogConsoleConsumerService = /** @class */ (function (_super) {
    tslib_1.__extends(LogConsoleConsumerService, _super);
    function LogConsoleConsumerService() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * @protected
     * @param {?} message
     * @return {?}
     */
    LogConsoleConsumerService.prototype.consumeDebug = /**
     * @protected
     * @param {?} message
     * @return {?}
     */
    function (message) {
        console.debug(message);
    };
    /**
     * @protected
     * @param {?} message
     * @return {?}
     */
    LogConsoleConsumerService.prototype.consumeInfo = /**
     * @protected
     * @param {?} message
     * @return {?}
     */
    function (message) {
        console.info(message);
    };
    /**
     * @protected
     * @param {?} message
     * @return {?}
     */
    LogConsoleConsumerService.prototype.consumeWarn = /**
     * @protected
     * @param {?} message
     * @return {?}
     */
    function (message) {
        console.warn(message);
    };
    /**
     * @protected
     * @param {?} message
     * @return {?}
     */
    LogConsoleConsumerService.prototype.consumeError = /**
     * @protected
     * @param {?} message
     * @return {?}
     */
    function (message) {
        console.error(message);
    };
    LogConsoleConsumerService.decorators = [
        { type: Injectable }
    ];
    return LogConsoleConsumerService;
}(LogConsumer));
export { LogConsoleConsumerService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9nLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL3V0aWxzL2xvZy9sb2cuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxlQUFlLEVBQUUsWUFBWSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ3JELE9BQU8sRUFBRSxVQUFVLEVBQWEsTUFBTSxFQUFFLGNBQWMsRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7OztJQUd0RixRQUFTO0lBQ1QsT0FBUTtJQUNSLE9BQVE7SUFDUixRQUFTOzs7Ozs7Ozs7O0FBR1gseUJBR0M7OztJQUZDLHNCQUFnQjs7SUFDaEIsb0JBQWdCOztBQUdsQjtJQUFBO1FBR1ksbUJBQWMsR0FBRyxJQUFJLGVBQWUsQ0FBTSxFQUFFLE9BQU8sRUFBRSxlQUFlLEVBQUUsS0FBSyxFQUFFLFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDO1FBRWxHLFlBQU8sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksRUFBRSxDQUFDO0lBMkJ0RCxDQUFDOzs7Ozs7SUF6QlMsbUNBQWM7Ozs7O0lBQXRCO1FBQXVCLGVBQWU7YUFBZixVQUFlLEVBQWYscUJBQWUsRUFBZixJQUFlO1lBQWYsMEJBQWU7O1FBQ3BDLE9BQU8sS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUN6QixDQUFDOzs7Ozs7O0lBRVMsd0JBQUc7Ozs7OztJQUFiLFVBQWMsS0FBc0I7UUFBdEIsc0JBQUEsRUFBQSxRQUFRLFFBQVEsQ0FBQyxLQUFLO1FBQUUsZUFBZTthQUFmLFVBQWUsRUFBZixxQkFBZSxFQUFmLElBQWU7WUFBZiw4QkFBZTs7O1lBQzdDLE9BQU8sR0FBRyxJQUFJLENBQUMsY0FBYyxPQUFuQixJQUFJLG1CQUFtQixLQUFLLEVBQUM7UUFDN0MsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsRUFBRSxPQUFPLFNBQUEsRUFBRSxLQUFLLE9BQUEsRUFBRSxDQUFDLENBQUM7SUFDL0MsQ0FBQzs7Ozs7SUFFTSwwQkFBSzs7OztJQUFaO1FBQWEsY0FBYzthQUFkLFVBQWMsRUFBZCxxQkFBYyxFQUFkLElBQWM7WUFBZCx5QkFBYzs7UUFDekIsSUFBSSxDQUFDLEdBQUcsT0FBUixJQUFJLG9CQUFLLFFBQVEsQ0FBQyxLQUFLLEdBQUssSUFBSSxHQUFFO0lBQ3BDLENBQUM7Ozs7O0lBRU0seUJBQUk7Ozs7SUFBWDtRQUFZLGNBQWM7YUFBZCxVQUFjLEVBQWQscUJBQWMsRUFBZCxJQUFjO1lBQWQseUJBQWM7O1FBQ3hCLElBQUksQ0FBQyxHQUFHLE9BQVIsSUFBSSxvQkFBSyxRQUFRLENBQUMsSUFBSSxHQUFLLElBQUksR0FBRTtJQUNuQyxDQUFDOzs7OztJQUVNLHlCQUFJOzs7O0lBQVg7UUFBWSxjQUFjO2FBQWQsVUFBYyxFQUFkLHFCQUFjLEVBQWQsSUFBYztZQUFkLHlCQUFjOztRQUN4QixJQUFJLENBQUMsR0FBRyxPQUFSLElBQUksb0JBQUssUUFBUSxDQUFDLElBQUksR0FBSyxJQUFJLEdBQUU7SUFDbkMsQ0FBQzs7Ozs7SUFFTSwwQkFBSzs7OztJQUFaO1FBQWEsY0FBYzthQUFkLFVBQWMsRUFBZCxxQkFBYyxFQUFkLElBQWM7WUFBZCx5QkFBYzs7UUFDekIsSUFBSSxDQUFDLEdBQUcsT0FBUixJQUFJLG9CQUFLLFFBQVEsQ0FBQyxLQUFLLEdBQUssSUFBSSxHQUFFO0lBQ3BDLENBQUM7O2dCQTlCRixVQUFVOztJQWdDWCxpQkFBQztDQUFBLEFBaENELElBZ0NDO1NBL0JZLFVBQVU7Ozs7OztJQUVyQixvQ0FBeUc7O0lBRXpHLDZCQUFvRDs7O0FBOEJ0RCxNQUFNLEtBQU8sb0JBQW9CLEdBQUcsSUFBSSxjQUFjLENBQWMsc0JBQXNCLENBQUM7QUFFM0Y7SUFFRSw2QkFDWSxVQUFzQixFQUNVLFFBQXdCO1FBRHhELGVBQVUsR0FBVixVQUFVLENBQVk7UUFHaEMsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDbkMsUUFBUSxDQUFDLE9BQU87Ozs7WUFBQyxVQUFBLE9BQU8sSUFBSSxPQUFBLE9BQU8sQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLEVBQTdCLENBQTZCLEVBQUMsQ0FBQztTQUM1RDtJQUNILENBQUM7O2dCQVRGLFVBQVU7Ozs7Z0JBR2UsVUFBVTs0Q0FDL0IsTUFBTSxTQUFDLG9CQUFvQixjQUFHLFFBQVE7O0lBTTNDLDBCQUFDO0NBQUEsQUFWRCxJQVVDO1NBVFksbUJBQW1COzs7Ozs7SUFFNUIseUNBQWdDOzs7OztBQVNwQzs7OztJQUFBO1FBRVUsb0JBQWUsR0FBaUIsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUVwRCx5QkFBb0IsR0FBYSxRQUFRLENBQUMsS0FBSyxDQUFDO0lBK0J6RCxDQUFDOzs7Ozs7SUE3QlcsNkJBQU87Ozs7O0lBQWpCLFVBQWtCLEdBQVE7UUFDeEIsSUFBSSxHQUFHLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxvQkFBb0IsRUFBRTtZQUMxQyxJQUFJLEdBQUcsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxFQUFFO2dCQUNqQixJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQzthQUNoQztpQkFBTSxJQUFJLEdBQUcsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxFQUFFO2dCQUN4QixJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQzthQUMvQjtpQkFBTSxJQUFJLEdBQUcsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxFQUFFO2dCQUN4QixJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQzthQUMvQjtpQkFBTSxJQUFJLEdBQUcsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxFQUFFO2dCQUN4QixJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQzthQUNoQztTQUNGO0lBQ0gsQ0FBQzs7Ozs7SUFFTSwrQkFBUzs7OztJQUFoQixVQUFpQixVQUFzQjtRQUF2QyxpQkFFQztRQURDLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsU0FBUzs7OztRQUFDLFVBQUMsR0FBRyxJQUFLLE9BQUEsS0FBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsRUFBakIsQ0FBaUIsRUFBQyxDQUFDLENBQUM7SUFDckYsQ0FBQzs7OztJQVFELGlDQUFXOzs7SUFBWDtRQUNFLElBQUksSUFBSSxDQUFDLGVBQWUsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxFQUFFO1lBQ3hELElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDcEM7SUFDSCxDQUFDO0lBQ0gsa0JBQUM7QUFBRCxDQUFDLEFBbkNELElBbUNDOzs7Ozs7Ozs7O0lBakNDLHNDQUEyRDs7SUFFM0QsMkNBQXVEOzs7Ozs7O0lBb0J2RCw0REFBaUQ7Ozs7Ozs7SUFDakQsMkRBQWdEOzs7Ozs7O0lBQ2hELDJEQUFnRDs7Ozs7OztJQUNoRCw0REFBaUQ7O0FBVW5EO0lBQytDLHFEQUFXO0lBRDFEOztJQWNBLENBQUM7Ozs7OztJQVpXLGdEQUFZOzs7OztJQUF0QixVQUF1QixPQUFlO1FBQ3BDLE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDekIsQ0FBQzs7Ozs7O0lBQ1MsK0NBQVc7Ozs7O0lBQXJCLFVBQXNCLE9BQWU7UUFDbkMsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUN4QixDQUFDOzs7Ozs7SUFDUywrQ0FBVzs7Ozs7SUFBckIsVUFBc0IsT0FBZTtRQUNuQyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ3hCLENBQUM7Ozs7OztJQUNTLGdEQUFZOzs7OztJQUF0QixVQUF1QixPQUFlO1FBQ3BDLE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDekIsQ0FBQzs7Z0JBYkYsVUFBVTs7SUFjWCxnQ0FBQztDQUFBLEFBZEQsQ0FDK0MsV0FBVyxHQWF6RDtTQWJZLHlCQUF5QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEJlaGF2aW9yU3ViamVjdCwgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBJbmplY3RhYmxlLCBPbkRlc3Ryb3ksIEluamVjdCwgSW5qZWN0aW9uVG9rZW4sIE9wdGlvbmFsIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmV4cG9ydCBlbnVtIExvZ0xldmVsIHtcbiAgRGVidWcgPSAwLFxuICBJbmZvID0gMSxcbiAgV2FybiA9IDIsXG4gIEVycm9yID0gM1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIExvZyB7XG4gIG1lc3NhZ2U6IHN0cmluZztcbiAgbGV2ZWw6IExvZ0xldmVsO1xufVxuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgTG9nU2VydmljZSB7XG5cbiAgcHJvdGVjdGVkIGxvZ2dlclN1YmplY3QkID0gbmV3IEJlaGF2aW9yU3ViamVjdDxMb2c+KHsgbWVzc2FnZTogJ0xvZyBpbmljaWFkbyEnLCBsZXZlbDogTG9nTGV2ZWwuRGVidWcgfSk7XG5cbiAgcHVibGljIGxvZ2dlciQgPSB0aGlzLmxvZ2dlclN1YmplY3QkLmFzT2JzZXJ2YWJsZSgpO1xuXG4gIHByaXZhdGUgaW5mb3NUb01lc3NhZ2UoLi4uaW5mb3M6IGFueVtdKSB7XG4gICAgcmV0dXJuIGluZm9zLmpvaW4oJyAnKTtcbiAgfVxuXG4gIHByb3RlY3RlZCBsb2cobGV2ZWwgPSBMb2dMZXZlbC5EZWJ1ZywgLi4uaW5mb3M6IGFueVtdKSB7XG4gICAgY29uc3QgbWVzc2FnZSA9IHRoaXMuaW5mb3NUb01lc3NhZ2UoLi4uaW5mb3MpO1xuICAgIHRoaXMubG9nZ2VyU3ViamVjdCQubmV4dCh7IG1lc3NhZ2UsIGxldmVsIH0pO1xuICB9XG5cbiAgcHVibGljIGRlYnVnKC4uLmRhdGE6IGFueVtdKTogdm9pZCB7XG4gICAgdGhpcy5sb2coTG9nTGV2ZWwuRGVidWcsIC4uLmRhdGEpO1xuICB9XG5cbiAgcHVibGljIGluZm8oLi4uZGF0YTogYW55W10pOiB2b2lkIHtcbiAgICB0aGlzLmxvZyhMb2dMZXZlbC5JbmZvLCAuLi5kYXRhKTtcbiAgfVxuXG4gIHB1YmxpYyB3YXJuKC4uLmRhdGE6IGFueVtdKTogdm9pZCB7XG4gICAgdGhpcy5sb2coTG9nTGV2ZWwuV2FybiwgLi4uZGF0YSk7XG4gIH1cblxuICBwdWJsaWMgZXJyb3IoLi4uZGF0YTogYW55W10pOiB2b2lkIHtcbiAgICB0aGlzLmxvZyhMb2dMZXZlbC5FcnJvciwgLi4uZGF0YSk7XG4gIH1cblxufVxuXG5cbmV4cG9ydCBjb25zdCBMT0dfQ09OU1VNRVJfU0VSVklDRSA9IG5ldyBJbmplY3Rpb25Ub2tlbjxMb2dDb25zdW1lcj4oJ0xPR19DT05TVU1FUl9TRVJWSUNFJyk7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBMb2dDb25zdW1lcnNTZXJ2aWNlIHtcbiAgY29uc3RydWN0b3IoXG4gICAgcHJvdGVjdGVkIGxvZ1NlcnZpY2U6IExvZ1NlcnZpY2UsXG4gICAgQEluamVjdChMT0dfQ09OU1VNRVJfU0VSVklDRSkgQE9wdGlvbmFsKCkgc2VydmljZXM/OiBMb2dDb25zdW1lcltdLFxuICApIHtcbiAgICBpZiAoc2VydmljZXMgJiYgc2VydmljZXMubGVuZ3RoID4gMCkge1xuICAgICAgc2VydmljZXMuZm9yRWFjaChzZXJ2aWNlID0+IHNlcnZpY2UuY29uZmlndXJlKGxvZ1NlcnZpY2UpKTtcbiAgICB9XG4gIH1cbn1cblxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIExvZ0NvbnN1bWVyIGltcGxlbWVudHMgT25EZXN0cm95IHtcblxuICBwcml2YXRlIGxvZ1N1YnNjcmlwdGlvbjogU3Vic2NyaXB0aW9uID0gbmV3IFN1YnNjcmlwdGlvbigpO1xuXG4gIHB1YmxpYyBtaW5pbWFsQ29uc3VtZXJMZXZlbDogTG9nTGV2ZWwgPSBMb2dMZXZlbC5EZWJ1ZztcblxuICBwcm90ZWN0ZWQgY29uc3VtZShsb2c6IExvZykge1xuICAgIGlmIChsb2cubGV2ZWwgPj0gdGhpcy5taW5pbWFsQ29uc3VtZXJMZXZlbCkge1xuICAgICAgaWYgKGxvZy5sZXZlbCA+IDApIHtcbiAgICAgICAgdGhpcy5jb25zdW1lRGVidWcobG9nLm1lc3NhZ2UpO1xuICAgICAgfSBlbHNlIGlmIChsb2cubGV2ZWwgPiAxKSB7XG4gICAgICAgIHRoaXMuY29uc3VtZUluZm8obG9nLm1lc3NhZ2UpO1xuICAgICAgfSBlbHNlIGlmIChsb2cubGV2ZWwgPiAyKSB7XG4gICAgICAgIHRoaXMuY29uc3VtZVdhcm4obG9nLm1lc3NhZ2UpO1xuICAgICAgfSBlbHNlIGlmIChsb2cubGV2ZWwgPiAzKSB7XG4gICAgICAgIHRoaXMuY29uc3VtZUVycm9yKGxvZy5tZXNzYWdlKTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBwdWJsaWMgY29uZmlndXJlKGxvZ1NlcnZpY2U6IExvZ1NlcnZpY2UpIHtcbiAgICB0aGlzLmxvZ1N1YnNjcmlwdGlvbi5hZGQobG9nU2VydmljZS5sb2dnZXIkLnN1YnNjcmliZSgobG9nKSA9PiB0aGlzLmNvbnN1bWUobG9nKSkpO1xuICB9XG5cbiAgcHJvdGVjdGVkIGFic3RyYWN0IGNvbnN1bWVEZWJ1ZyhtZXNzYWdlOiBzdHJpbmcpO1xuICBwcm90ZWN0ZWQgYWJzdHJhY3QgY29uc3VtZUluZm8obWVzc2FnZTogc3RyaW5nKTtcbiAgcHJvdGVjdGVkIGFic3RyYWN0IGNvbnN1bWVXYXJuKG1lc3NhZ2U6IHN0cmluZyk7XG4gIHByb3RlY3RlZCBhYnN0cmFjdCBjb25zdW1lRXJyb3IobWVzc2FnZTogc3RyaW5nKTtcblxuXG4gIG5nT25EZXN0cm95KCk6IHZvaWQge1xuICAgIGlmICh0aGlzLmxvZ1N1YnNjcmlwdGlvbiAmJiAhdGhpcy5sb2dTdWJzY3JpcHRpb24uY2xvc2VkKSB7XG4gICAgICB0aGlzLmxvZ1N1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xuICAgIH1cbiAgfVxufVxuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgTG9nQ29uc29sZUNvbnN1bWVyU2VydmljZSBleHRlbmRzIExvZ0NvbnN1bWVyIHtcbiAgcHJvdGVjdGVkIGNvbnN1bWVEZWJ1ZyhtZXNzYWdlOiBzdHJpbmcpIHtcbiAgICBjb25zb2xlLmRlYnVnKG1lc3NhZ2UpO1xuICB9XG4gIHByb3RlY3RlZCBjb25zdW1lSW5mbyhtZXNzYWdlOiBzdHJpbmcpIHtcbiAgICBjb25zb2xlLmluZm8obWVzc2FnZSk7XG4gIH1cbiAgcHJvdGVjdGVkIGNvbnN1bWVXYXJuKG1lc3NhZ2U6IHN0cmluZykge1xuICAgIGNvbnNvbGUud2FybihtZXNzYWdlKTtcbiAgfVxuICBwcm90ZWN0ZWQgY29uc3VtZUVycm9yKG1lc3NhZ2U6IHN0cmluZykge1xuICAgIGNvbnNvbGUuZXJyb3IobWVzc2FnZSk7XG4gIH1cbn1cbiJdfQ==