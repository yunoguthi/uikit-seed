/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule, SkipSelf, Optional } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LogService, LogConsumersService } from './log.service';
var LogModule = /** @class */ (function () {
    function LogModule(logConsumersService, parentModule) {
        this.logConsumersService = logConsumersService;
        if (parentModule) {
            throw new Error('LogModule is already loaded. Import it in the AppModule only');
        }
    }
    LogModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [],
                    imports: [
                        CommonModule
                    ],
                    providers: [
                        LogService,
                        LogConsumersService
                    ],
                    exports: []
                },] }
    ];
    /** @nocollapse */
    LogModule.ctorParameters = function () { return [
        { type: LogConsumersService },
        { type: LogModule, decorators: [{ type: Optional }, { type: SkipSelf }] }
    ]; };
    return LogModule;
}());
export { LogModule };
if (false) {
    /** @type {?} */
    LogModule.prototype.logConsumersService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9nLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvdXRpbHMvbG9nL2xvZy5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM3RCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDL0MsT0FBTyxFQUFFLFVBQVUsRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUVoRTtJQWdCRSxtQkFDUyxtQkFBd0MsRUFDdkIsWUFBd0I7UUFEekMsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUc3QyxJQUFJLFlBQVksRUFBRTtZQUNoQixNQUFNLElBQUksS0FBSyxDQUNiLDhEQUE4RCxDQUFDLENBQUM7U0FDbkU7SUFDTCxDQUFDOztnQkF4QkYsUUFBUSxTQUFDO29CQUNSLFlBQVksRUFBRSxFQUViO29CQUNELE9BQU8sRUFBRTt3QkFDUCxZQUFZO3FCQUNiO29CQUNELFNBQVMsRUFBRTt3QkFDVCxVQUFVO3dCQUNWLG1CQUFtQjtxQkFDcEI7b0JBQ0QsT0FBTyxFQUFFLEVBRVI7aUJBQ0Y7Ozs7Z0JBaEJvQixtQkFBbUI7Z0JBb0JHLFNBQVMsdUJBQS9DLFFBQVEsWUFBSSxRQUFROztJQU96QixnQkFBQztDQUFBLEFBekJELElBeUJDO1NBVlksU0FBUzs7O0lBRWxCLHdDQUErQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlLCBTa2lwU2VsZiwgT3B0aW9uYWwgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQgeyBMb2dTZXJ2aWNlLCBMb2dDb25zdW1lcnNTZXJ2aWNlIH0gZnJvbSAnLi9sb2cuc2VydmljZSc7XG5cbkBOZ01vZHVsZSh7XG4gIGRlY2xhcmF0aW9uczogW1xuXG4gIF0sXG4gIGltcG9ydHM6IFtcbiAgICBDb21tb25Nb2R1bGVcbiAgXSxcbiAgcHJvdmlkZXJzOiBbXG4gICAgTG9nU2VydmljZSxcbiAgICBMb2dDb25zdW1lcnNTZXJ2aWNlXG4gIF0sXG4gIGV4cG9ydHM6IFtcblxuICBdXG59KVxuZXhwb3J0IGNsYXNzIExvZ01vZHVsZSB7XG4gIGNvbnN0cnVjdG9yKFxuICAgIHB1YmxpYyBsb2dDb25zdW1lcnNTZXJ2aWNlOiBMb2dDb25zdW1lcnNTZXJ2aWNlLFxuICAgIEBPcHRpb25hbCgpIEBTa2lwU2VsZigpIHBhcmVudE1vZHVsZT86IExvZ01vZHVsZSxcbiAgICApIHtcbiAgICAgIGlmIChwYXJlbnRNb2R1bGUpIHtcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKFxuICAgICAgICAgICdMb2dNb2R1bGUgaXMgYWxyZWFkeSBsb2FkZWQuIEltcG9ydCBpdCBpbiB0aGUgQXBwTW9kdWxlIG9ubHknKTtcbiAgICAgIH1cbiAgfVxufVxuIl19