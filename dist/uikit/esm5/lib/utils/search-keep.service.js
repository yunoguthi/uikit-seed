/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { SearchQuery } from '../layout/header/search/state/search.query';
import { SearchStore } from '../layout/header/search/state/search.store';
import * as _ from 'lodash';
import { map } from 'rxjs/operators';
import { LayoutService } from '../layout/layout.service';
import { guid } from '@datorama/akita';
import * as Fuse from 'fuse.js';
var SearchKeepService = /** @class */ (function () {
    function SearchKeepService(layoutService, searchQuery, searchStore) {
        this.layoutService = layoutService;
        this.searchQuery = searchQuery;
        this.searchStore = searchStore;
        this.searchGroups = this.layoutService.menuItemsFlattened$;
        this.searchGroupsAll = this.layoutService.menuItems$;
    }
    /*
  * Método responsável por adicionar um novo grupo na pesquisas.
  * */
    /*
    * Método responsável por adicionar um novo grupo na pesquisas.
    * */
    /**
     * @param {?} groups
     * @return {?}
     */
    SearchKeepService.prototype.addGroup = /*
    * Método responsável por adicionar um novo grupo na pesquisas.
    * */
    /**
     * @param {?} groups
     * @return {?}
     */
    function (groups) {
        /** @type {?} */
        var itensGroups = this.searchQuery.getAll();
        if (_.head(itensGroups) !== undefined) {
            this.searchStore.adicionarGrupoPesquisa(itensGroups, groups);
        }
        else {
            this.searchStore.addGroupBase(groups);
        }
    };
    /*
      * Método responsável por remover um grupo da pesquisa.
      * */
    /*
        * Método responsável por remover um grupo da pesquisa.
        * */
    /**
     * @param {?} group
     * @return {?}
     */
    SearchKeepService.prototype.removeGroup = /*
        * Método responsável por remover um grupo da pesquisa.
        * */
    /**
     * @param {?} group
     * @return {?}
     */
    function (group) {
        this.searchStore.removerGrupoPesquisa(group.title);
    };
    /*
     * Método responsável por realizar a pesquisa dos grupo na pesquisa.
     * */
    /*
       * Método responsável por realizar a pesquisa dos grupo na pesquisa.
       * */
    /**
     * @param {?} term
     * @return {?}
     */
    SearchKeepService.prototype.searchGroup = /*
       * Método responsável por realizar a pesquisa dos grupo na pesquisa.
       * */
    /**
     * @param {?} term
     * @return {?}
     */
    function (term) {
        return this._filterGroup(term);
    };
    /**
     * @private
     * @param {?} value
     * @return {?}
     */
    SearchKeepService.prototype._filterGroup = /**
     * @private
     * @param {?} value
     * @return {?}
     */
    function (value) {
        var _this = this;
        /** @type {?} */
        var options = {
            keys: ['title', 'tags',
                'children.title', 'children.tags',
                'children.children.title', 'children.children.title.tags',
                'children.children.children.title', 'children.children.children.title.tags']
        };
        return this.searchGroupsAll.pipe(map((/**
         * @param {?} listMenuItem
         * @return {?}
         */
        function (listMenuItem) { return _this.applyFilter(listMenuItem, value, options); })), map((/**
         * @param {?} itens
         * @return {?}
         */
        function (itens) { return _this.groupReturnFilter(itens); })));
    };
    /**
     * @private
     * @param {?} listMenuItem
     * @param {?} value
     * @param {?} options
     * @return {?}
     */
    SearchKeepService.prototype.applyFilter = /**
     * @private
     * @param {?} listMenuItem
     * @param {?} value
     * @param {?} options
     * @return {?}
     */
    function (listMenuItem, value, options) {
        /** @type {?} */
        var fuse = new Fuse(listMenuItem, options);
        return fuse.search(value);
    };
    /**
     * @private
     * @param {?} itens
     * @return {?}
     */
    SearchKeepService.prototype.groupReturnFilter = /**
     * @private
     * @param {?} itens
     * @return {?}
     */
    function (itens) {
        /** @type {?} */
        var functionMapItem = (/**
         * @param {?} group
         * @return {?}
         */
        function (group) {
            return (/** @type {?} */ ({
                id: group.id ? group.id : guid(),
                title: group.title,
                icon: group.icon,
                link: group.link,
                tags: group.tags
            }));
        });
        /** @type {?} */
        var functionMapGroup = (/**
         * @param {?} group
         * @return {?}
         */
        function (group) {
            /** @type {?} */
            var groupMenu;
            if (group.link) {
                groupMenu = functionMapItem(group);
            }
            else {
                groupMenu = (/** @type {?} */ ({
                    id: group.id ? group.id : guid(),
                    title: group.title,
                    icon: group.icon,
                    isHeader: true,
                }));
                if (group.children && group.children.length > 1) {
                    /** @type {?} */
                    var itemFilhos = group.children
                        .filter((/**
                     * @param {?} b
                     * @return {?}
                     */
                    function (b) { return b !== group; }))
                        .map((/**
                     * @param {?} itemIt
                     * @return {?}
                     */
                    function (itemIt) {
                        if (itemIt.children && itemIt.children.length > 1) {
                            return functionMapGroup(itemIt);
                        }
                        else {
                            return functionMapItem(itemIt);
                        }
                    }));
                    if ((/** @type {?} */ (itemFilhos))) {
                        groupMenu.items = (/** @type {?} */ (itemFilhos));
                    }
                    else {
                        groupMenu.items = (/** @type {?} */ (itemFilhos));
                    }
                }
            }
            return groupMenu;
        });
        return (/** @type {?} */ ({
            id: guid(),
            title: 'Menu',
            isHeader: true,
            items: itens.map(functionMapGroup)
        }));
    };
    SearchKeepService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    SearchKeepService.ctorParameters = function () { return [
        { type: LayoutService },
        { type: SearchQuery },
        { type: SearchStore }
    ]; };
    return SearchKeepService;
}());
export { SearchKeepService };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    SearchKeepService.prototype.searchGroups;
    /**
     * @type {?}
     * @protected
     */
    SearchKeepService.prototype.searchGroupsAll;
    /**
     * @type {?}
     * @protected
     */
    SearchKeepService.prototype.layoutService;
    /**
     * @type {?}
     * @private
     */
    SearchKeepService.prototype.searchQuery;
    /**
     * @type {?}
     * @private
     */
    SearchKeepService.prototype.searchStore;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLWtlZXAuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvdXRpbHMvc2VhcmNoLWtlZXAuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUV6QyxPQUFPLEVBQUMsV0FBVyxFQUFDLE1BQU0sNENBQTRDLENBQUM7QUFDdkUsT0FBTyxFQUFDLFdBQVcsRUFBQyxNQUFNLDRDQUE0QyxDQUFDO0FBQ3ZFLE9BQU8sS0FBSyxDQUFDLE1BQU0sUUFBUSxDQUFDO0FBRzVCLE9BQU8sRUFBQyxHQUFHLEVBQUMsTUFBTSxnQkFBZ0IsQ0FBQztBQUVuQyxPQUFPLEVBQUMsYUFBYSxFQUFDLE1BQU0sMEJBQTBCLENBQUM7QUFDdkQsT0FBTyxFQUFDLElBQUksRUFBQyxNQUFNLGlCQUFpQixDQUFDO0FBQ3JDLE9BQU8sS0FBSyxJQUFJLE1BQU0sU0FBUyxDQUFDO0FBR2hDO0lBTUUsMkJBQXNCLGFBQTRCLEVBQzlCLFdBQXdCLEVBQ3hCLFdBQXdCO1FBRnRCLGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBQzlCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQ3hCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBTGxDLGlCQUFZLEdBQTJCLElBQUksQ0FBQyxhQUFhLENBQUMsbUJBQW1CLENBQUM7UUFDOUUsb0JBQWUsR0FBMkIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUM7SUFLbEYsQ0FBQztJQUVEOztNQUVFOzs7Ozs7OztJQUNGLG9DQUFROzs7Ozs7O0lBQVIsVUFBUyxNQUF1Qjs7WUFDeEIsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFO1FBQzdDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxTQUFTLEVBQUU7WUFDckMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxzQkFBc0IsQ0FBQyxXQUFXLEVBQUUsTUFBTSxDQUFDLENBQUM7U0FDOUQ7YUFBTTtZQUNMLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1NBQ3ZDO0lBQ0gsQ0FBQztJQUVEOztVQUVNOzs7Ozs7OztJQUNOLHVDQUFXOzs7Ozs7O0lBQVgsVUFBWSxLQUFvQjtRQUM5QixJQUFJLENBQUMsV0FBVyxDQUFDLG9CQUFvQixDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNyRCxDQUFDO0lBRUQ7O1NBRUs7Ozs7Ozs7O0lBQ0wsdUNBQVc7Ozs7Ozs7SUFBWCxVQUFZLElBQVk7UUFDdEIsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ2pDLENBQUM7Ozs7OztJQUVPLHdDQUFZOzs7OztJQUFwQixVQUFxQixLQUFhO1FBQWxDLGlCQVdDOztZQVZPLE9BQU8sR0FBRztZQUNkLElBQUksRUFBRSxDQUFDLE9BQU8sRUFBRSxNQUFNO2dCQUNwQixnQkFBZ0IsRUFBRSxlQUFlO2dCQUNqQyx5QkFBeUIsRUFBRSw4QkFBOEI7Z0JBQ3pELGtDQUFrQyxFQUFFLHVDQUF1QyxDQUFDO1NBQy9FO1FBRUQsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FDOUIsR0FBRzs7OztRQUFDLFVBQUEsWUFBWSxJQUFJLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLEVBQUUsS0FBSyxFQUFFLE9BQU8sQ0FBQyxFQUE5QyxDQUE4QyxFQUFDLEVBQ25FLEdBQUc7Ozs7UUFBQyxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsRUFBN0IsQ0FBNkIsRUFBQyxDQUFDLENBQUM7SUFDakQsQ0FBQzs7Ozs7Ozs7SUFFTyx1Q0FBVzs7Ozs7OztJQUFuQixVQUFvQixZQUF3QixFQUFFLEtBQWEsRUFBRSxPQUEyQjs7WUFDaEYsSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLFlBQVksRUFBRSxPQUFPLENBQUM7UUFDNUMsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzVCLENBQUM7Ozs7OztJQUVPLDZDQUFpQjs7Ozs7SUFBekIsVUFBMEIsS0FBaUI7O1lBRW5DLGVBQWU7Ozs7UUFBRyxVQUFDLEtBQWU7WUFDdEMsT0FBTyxtQkFBQTtnQkFDTCxFQUFFLEVBQUUsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFO2dCQUNoQyxLQUFLLEVBQUUsS0FBSyxDQUFDLEtBQUs7Z0JBQ2xCLElBQUksRUFBRSxLQUFLLENBQUMsSUFBSTtnQkFDaEIsSUFBSSxFQUFFLEtBQUssQ0FBQyxJQUFJO2dCQUNoQixJQUFJLEVBQUUsS0FBSyxDQUFDLElBQUk7YUFDakIsRUFBZ0IsQ0FBQztRQUNwQixDQUFDLENBQUE7O1lBRUssZ0JBQWdCOzs7O1FBQUcsVUFBQyxLQUFlOztnQkFFbkMsU0FBdUM7WUFFM0MsSUFBSSxLQUFLLENBQUMsSUFBSSxFQUFFO2dCQUNkLFNBQVMsR0FBRyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDcEM7aUJBQU07Z0JBQ0wsU0FBUyxHQUFHLG1CQUFBO29CQUNWLEVBQUUsRUFBRSxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUU7b0JBQ2hDLEtBQUssRUFBRSxLQUFLLENBQUMsS0FBSztvQkFDbEIsSUFBSSxFQUFFLEtBQUssQ0FBQyxJQUFJO29CQUNoQixRQUFRLEVBQUUsSUFBSTtpQkFDZixFQUFpQixDQUFDO2dCQUVuQixJQUFJLEtBQUssQ0FBQyxRQUFRLElBQUksS0FBSyxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFOzt3QkFFekMsVUFBVSxHQUFHLEtBQUssQ0FBQyxRQUFRO3lCQUM5QixNQUFNOzs7O29CQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxLQUFLLEtBQUssRUFBWCxDQUFXLEVBQUM7eUJBQ3hCLEdBQUc7Ozs7b0JBQUMsVUFBQyxNQUFNO3dCQUNWLElBQUksTUFBTSxDQUFDLFFBQVEsSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7NEJBQ2pELE9BQU8sZ0JBQWdCLENBQUMsTUFBTSxDQUFDLENBQUM7eUJBQ2pDOzZCQUFNOzRCQUNMLE9BQU8sZUFBZSxDQUFDLE1BQU0sQ0FBQyxDQUFDO3lCQUNoQztvQkFDSCxDQUFDLEVBQUM7b0JBRUosSUFBSSxtQkFBQSxVQUFVLEVBQWtCLEVBQUU7d0JBQ2hDLFNBQVMsQ0FBQyxLQUFLLEdBQUcsbUJBQUEsVUFBVSxFQUFrQixDQUFDO3FCQUNoRDt5QkFBTTt3QkFDTCxTQUFTLENBQUMsS0FBSyxHQUFHLG1CQUFBLFVBQVUsRUFBbUIsQ0FBQztxQkFDakQ7aUJBQ0Y7YUFDRjtZQUVELE9BQU8sU0FBUyxDQUFDO1FBQ25CLENBQUMsQ0FBQTtRQUVELE9BQU8sbUJBQUE7WUFDTCxFQUFFLEVBQUUsSUFBSSxFQUFFO1lBQ1YsS0FBSyxFQUFFLE1BQU07WUFDYixRQUFRLEVBQUUsSUFBSTtZQUNkLEtBQUssRUFBRSxLQUFLLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDO1NBQ25DLEVBQWlCLENBQUM7SUFDckIsQ0FBQzs7Z0JBOUdGLFVBQVU7Ozs7Z0JBTEgsYUFBYTtnQkFQYixXQUFXO2dCQUNYLFdBQVc7O0lBMEhuQix3QkFBQztDQUFBLEFBL0dELElBK0dDO1NBOUdZLGlCQUFpQjs7Ozs7O0lBRTVCLHlDQUF3Rjs7Ozs7SUFDeEYsNENBQWtGOzs7OztJQUV0RSwwQ0FBc0M7Ozs7O0lBQ3RDLHdDQUFnQzs7Ozs7SUFDaEMsd0NBQWdDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtJbmplY3RhYmxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7R3J1cG9QZXNxdWlzYSwgSXRlbVBlc3F1aXNhfSBmcm9tICcuLi9sYXlvdXQvaGVhZGVyL3NlYXJjaC9zZWFyY2gubW9kZWwnO1xuaW1wb3J0IHtTZWFyY2hRdWVyeX0gZnJvbSAnLi4vbGF5b3V0L2hlYWRlci9zZWFyY2gvc3RhdGUvc2VhcmNoLnF1ZXJ5JztcbmltcG9ydCB7U2VhcmNoU3RvcmV9IGZyb20gJy4uL2xheW91dC9oZWFkZXIvc2VhcmNoL3N0YXRlL3NlYXJjaC5zdG9yZSc7XG5pbXBvcnQgKiBhcyBfIGZyb20gJ2xvZGFzaCc7XG5pbXBvcnQge1NlYXJjaEFic3RyYWN0U2VydmljZX0gZnJvbSAnLi9hYnN0cmFjdC9zZWFyY2gtYWJzdHJhY3Quc2VydmljZSc7XG5pbXBvcnQge09ic2VydmFibGV9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHttYXB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcbmltcG9ydCB7TWVudUl0ZW19IGZyb20gJy4uL2xheW91dC9uYXYvbWVudS9tZW51LWl0ZW0vbWVudS1pdGVtLm1vZGVsJztcbmltcG9ydCB7TGF5b3V0U2VydmljZX0gZnJvbSAnLi4vbGF5b3V0L2xheW91dC5zZXJ2aWNlJztcbmltcG9ydCB7Z3VpZH0gZnJvbSAnQGRhdG9yYW1hL2FraXRhJztcbmltcG9ydCAqIGFzIEZ1c2UgZnJvbSAnZnVzZS5qcyc7XG5cblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIFNlYXJjaEtlZXBTZXJ2aWNlIGltcGxlbWVudHMgU2VhcmNoQWJzdHJhY3RTZXJ2aWNlIHtcblxuICBwcm90ZWN0ZWQgc2VhcmNoR3JvdXBzOiBPYnNlcnZhYmxlPE1lbnVJdGVtW10+ID0gdGhpcy5sYXlvdXRTZXJ2aWNlLm1lbnVJdGVtc0ZsYXR0ZW5lZCQ7XG4gIHByb3RlY3RlZCBzZWFyY2hHcm91cHNBbGw6IE9ic2VydmFibGU8TWVudUl0ZW1bXT4gPSB0aGlzLmxheW91dFNlcnZpY2UubWVudUl0ZW1zJDtcblxuICBjb25zdHJ1Y3Rvcihwcm90ZWN0ZWQgbGF5b3V0U2VydmljZTogTGF5b3V0U2VydmljZSxcbiAgICAgICAgICAgICAgcHJpdmF0ZSBzZWFyY2hRdWVyeTogU2VhcmNoUXVlcnksXG4gICAgICAgICAgICAgIHByaXZhdGUgc2VhcmNoU3RvcmU6IFNlYXJjaFN0b3JlKSB7XG4gIH1cblxuICAvKlxuKiBNw6l0b2RvIHJlc3BvbnPDoXZlbCBwb3IgYWRpY2lvbmFyIHVtIG5vdm8gZ3J1cG8gbmEgcGVzcXVpc2FzLlxuKiAqL1xuICBhZGRHcm91cChncm91cHM6IEdydXBvUGVzcXVpc2FbXSkge1xuICAgIGNvbnN0IGl0ZW5zR3JvdXBzID0gdGhpcy5zZWFyY2hRdWVyeS5nZXRBbGwoKTtcbiAgICBpZiAoXy5oZWFkKGl0ZW5zR3JvdXBzKSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICB0aGlzLnNlYXJjaFN0b3JlLmFkaWNpb25hckdydXBvUGVzcXVpc2EoaXRlbnNHcm91cHMsIGdyb3Vwcyk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuc2VhcmNoU3RvcmUuYWRkR3JvdXBCYXNlKGdyb3Vwcyk7XG4gICAgfVxuICB9XG5cbiAgLypcbiAgICAqIE3DqXRvZG8gcmVzcG9uc8OhdmVsIHBvciByZW1vdmVyIHVtIGdydXBvIGRhIHBlc3F1aXNhLlxuICAgICogKi9cbiAgcmVtb3ZlR3JvdXAoZ3JvdXA6IEdydXBvUGVzcXVpc2EpIHtcbiAgICB0aGlzLnNlYXJjaFN0b3JlLnJlbW92ZXJHcnVwb1Blc3F1aXNhKGdyb3VwLnRpdGxlKTtcbiAgfVxuXG4gIC8qXG4gICAqIE3DqXRvZG8gcmVzcG9uc8OhdmVsIHBvciByZWFsaXphciBhIHBlc3F1aXNhIGRvcyBncnVwbyBuYSBwZXNxdWlzYS5cbiAgICogKi9cbiAgc2VhcmNoR3JvdXAodGVybTogc3RyaW5nKTogT2JzZXJ2YWJsZTxHcnVwb1Blc3F1aXNhIHwgSXRlbVBlc3F1aXNhPiB7XG4gICAgcmV0dXJuIHRoaXMuX2ZpbHRlckdyb3VwKHRlcm0pO1xuICB9XG5cbiAgcHJpdmF0ZSBfZmlsdGVyR3JvdXAodmFsdWU6IHN0cmluZyk6IE9ic2VydmFibGU8R3J1cG9QZXNxdWlzYSB8IEl0ZW1QZXNxdWlzYT4ge1xuICAgIGNvbnN0IG9wdGlvbnMgPSB7XG4gICAgICBrZXlzOiBbJ3RpdGxlJywgJ3RhZ3MnLFxuICAgICAgICAnY2hpbGRyZW4udGl0bGUnLCAnY2hpbGRyZW4udGFncycsXG4gICAgICAgICdjaGlsZHJlbi5jaGlsZHJlbi50aXRsZScsICdjaGlsZHJlbi5jaGlsZHJlbi50aXRsZS50YWdzJyxcbiAgICAgICAgJ2NoaWxkcmVuLmNoaWxkcmVuLmNoaWxkcmVuLnRpdGxlJywgJ2NoaWxkcmVuLmNoaWxkcmVuLmNoaWxkcmVuLnRpdGxlLnRhZ3MnXVxuICAgIH07XG5cbiAgICByZXR1cm4gdGhpcy5zZWFyY2hHcm91cHNBbGwucGlwZShcbiAgICAgIG1hcChsaXN0TWVudUl0ZW0gPT4gdGhpcy5hcHBseUZpbHRlcihsaXN0TWVudUl0ZW0sIHZhbHVlLCBvcHRpb25zKSksXG4gICAgICBtYXAoaXRlbnMgPT4gdGhpcy5ncm91cFJldHVybkZpbHRlcihpdGVucykpKTtcbiAgfVxuXG4gIHByaXZhdGUgYXBwbHlGaWx0ZXIobGlzdE1lbnVJdGVtOiBNZW51SXRlbVtdLCB2YWx1ZTogc3RyaW5nLCBvcHRpb25zOiB7IGtleXM6IHN0cmluZ1tdIH0pIHtcbiAgICBjb25zdCBmdXNlID0gbmV3IEZ1c2UobGlzdE1lbnVJdGVtLCBvcHRpb25zKTtcbiAgICByZXR1cm4gZnVzZS5zZWFyY2godmFsdWUpO1xuICB9XG5cbiAgcHJpdmF0ZSBncm91cFJldHVybkZpbHRlcihpdGVuczogTWVudUl0ZW1bXSk6IEdydXBvUGVzcXVpc2EgfCBJdGVtUGVzcXVpc2Ege1xuXG4gICAgY29uc3QgZnVuY3Rpb25NYXBJdGVtID0gKGdyb3VwOiBNZW51SXRlbSkgPT4ge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgaWQ6IGdyb3VwLmlkID8gZ3JvdXAuaWQgOiBndWlkKCksXG4gICAgICAgIHRpdGxlOiBncm91cC50aXRsZSxcbiAgICAgICAgaWNvbjogZ3JvdXAuaWNvbixcbiAgICAgICAgbGluazogZ3JvdXAubGluayxcbiAgICAgICAgdGFnczogZ3JvdXAudGFnc1xuICAgICAgfSBhcyBJdGVtUGVzcXVpc2E7XG4gICAgfTtcblxuICAgIGNvbnN0IGZ1bmN0aW9uTWFwR3JvdXAgPSAoZ3JvdXA6IE1lbnVJdGVtKSA9PiB7XG5cbiAgICAgIGxldCBncm91cE1lbnU6IEdydXBvUGVzcXVpc2EgfCBJdGVtUGVzcXVpc2E7XG5cbiAgICAgIGlmIChncm91cC5saW5rKSB7XG4gICAgICAgIGdyb3VwTWVudSA9IGZ1bmN0aW9uTWFwSXRlbShncm91cCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBncm91cE1lbnUgPSB7XG4gICAgICAgICAgaWQ6IGdyb3VwLmlkID8gZ3JvdXAuaWQgOiBndWlkKCksXG4gICAgICAgICAgdGl0bGU6IGdyb3VwLnRpdGxlLFxuICAgICAgICAgIGljb246IGdyb3VwLmljb24sXG4gICAgICAgICAgaXNIZWFkZXI6IHRydWUsXG4gICAgICAgIH0gYXMgR3J1cG9QZXNxdWlzYTtcblxuICAgICAgICBpZiAoZ3JvdXAuY2hpbGRyZW4gJiYgZ3JvdXAuY2hpbGRyZW4ubGVuZ3RoID4gMSkge1xuXG4gICAgICAgICAgY29uc3QgaXRlbUZpbGhvcyA9IGdyb3VwLmNoaWxkcmVuXG4gICAgICAgICAgICAuZmlsdGVyKGIgPT4gYiAhPT0gZ3JvdXApXG4gICAgICAgICAgICAubWFwKChpdGVtSXQpID0+IHtcbiAgICAgICAgICAgICAgaWYgKGl0ZW1JdC5jaGlsZHJlbiAmJiBpdGVtSXQuY2hpbGRyZW4ubGVuZ3RoID4gMSkge1xuICAgICAgICAgICAgICAgIHJldHVybiBmdW5jdGlvbk1hcEdyb3VwKGl0ZW1JdCk7XG4gICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZ1bmN0aW9uTWFwSXRlbShpdGVtSXQpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgIGlmIChpdGVtRmlsaG9zIGFzIEl0ZW1QZXNxdWlzYVtdKSB7XG4gICAgICAgICAgICBncm91cE1lbnUuaXRlbXMgPSBpdGVtRmlsaG9zIGFzIEl0ZW1QZXNxdWlzYVtdO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBncm91cE1lbnUuaXRlbXMgPSBpdGVtRmlsaG9zIGFzIEdydXBvUGVzcXVpc2FbXTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgcmV0dXJuIGdyb3VwTWVudTtcbiAgICB9O1xuXG4gICAgcmV0dXJuIHtcbiAgICAgIGlkOiBndWlkKCksXG4gICAgICB0aXRsZTogJ01lbnUnLFxuICAgICAgaXNIZWFkZXI6IHRydWUsXG4gICAgICBpdGVtczogaXRlbnMubWFwKGZ1bmN0aW9uTWFwR3JvdXApXG4gICAgfSBhcyBHcnVwb1Blc3F1aXNhO1xuICB9XG59XG4iXX0=