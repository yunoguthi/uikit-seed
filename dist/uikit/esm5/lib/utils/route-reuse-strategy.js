/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { RouteReuseStrategy } from '@angular/router';
var NoRouteReuseStrategy = /** @class */ (function (_super) {
    tslib_1.__extends(NoRouteReuseStrategy, _super);
    function NoRouteReuseStrategy() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * @param {?} route
     * @return {?}
     */
    NoRouteReuseStrategy.prototype.shouldDetach = /**
     * @param {?} route
     * @return {?}
     */
    function (route) {
        return false;
    };
    /**
     * @param {?} route
     * @param {?} handle
     * @return {?}
     */
    NoRouteReuseStrategy.prototype.store = /**
     * @param {?} route
     * @param {?} handle
     * @return {?}
     */
    function (route, handle) {
    };
    /**
     * @param {?} route
     * @return {?}
     */
    NoRouteReuseStrategy.prototype.shouldAttach = /**
     * @param {?} route
     * @return {?}
     */
    function (route) {
        return false;
    };
    /**
     * @param {?} route
     * @return {?}
     */
    NoRouteReuseStrategy.prototype.retrieve = /**
     * @param {?} route
     * @return {?}
     */
    function (route) {
        return null;
    };
    /**
     * @param {?} future
     * @param {?} curr
     * @return {?}
     */
    NoRouteReuseStrategy.prototype.shouldReuseRoute = /**
     * @param {?} future
     * @param {?} curr
     * @return {?}
     */
    function (future, curr) {
        return false; // default is true if configuration of current and future route are the same
    };
    return NoRouteReuseStrategy;
}(RouteReuseStrategy));
export { NoRouteReuseStrategy };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicm91dGUtcmV1c2Utc3RyYXRlZ3kuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL3V0aWxzL3JvdXRlLXJldXNlLXN0cmF0ZWd5LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLGtCQUFrQixFQUEwQixNQUFNLGlCQUFpQixDQUFDO0FBRTdFO0lBQTBDLGdEQUFrQjtJQUE1RDs7SUFnQkEsQ0FBQzs7Ozs7SUFmQywyQ0FBWTs7OztJQUFaLFVBQWEsS0FBNkI7UUFDeEMsT0FBTyxLQUFLLENBQUM7SUFDZixDQUFDOzs7Ozs7SUFDRCxvQ0FBSzs7Ozs7SUFBTCxVQUFNLEtBQTZCLEVBQUUsTUFBVTtJQUUvQyxDQUFDOzs7OztJQUNELDJDQUFZOzs7O0lBQVosVUFBYSxLQUE2QjtRQUN4QyxPQUFPLEtBQUssQ0FBQztJQUNmLENBQUM7Ozs7O0lBQ0QsdUNBQVE7Ozs7SUFBUixVQUFTLEtBQTZCO1FBQ3BDLE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQzs7Ozs7O0lBQ0QsK0NBQWdCOzs7OztJQUFoQixVQUFpQixNQUE4QixFQUFFLElBQTRCO1FBQzNFLE9BQU8sS0FBSyxDQUFDLENBQUMsNEVBQTRFO0lBQzVGLENBQUM7SUFDSCwyQkFBQztBQUFELENBQUMsQUFoQkQsQ0FBMEMsa0JBQWtCLEdBZ0IzRCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFJvdXRlUmV1c2VTdHJhdGVneSwgQWN0aXZhdGVkUm91dGVTbmFwc2hvdCB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5cbmV4cG9ydCBjbGFzcyBOb1JvdXRlUmV1c2VTdHJhdGVneSBleHRlbmRzIFJvdXRlUmV1c2VTdHJhdGVneSB7XG4gIHNob3VsZERldGFjaChyb3V0ZTogQWN0aXZhdGVkUm91dGVTbmFwc2hvdCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuICBzdG9yZShyb3V0ZTogQWN0aXZhdGVkUm91dGVTbmFwc2hvdCwgaGFuZGxlOiB7fSk6IHZvaWQge1xuXG4gIH1cbiAgc2hvdWxkQXR0YWNoKHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90KTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG4gIHJldHJpZXZlKHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90KToge30ge1xuICAgIHJldHVybiBudWxsO1xuICB9XG4gIHNob3VsZFJldXNlUm91dGUoZnV0dXJlOiBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LCBjdXJyOiBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90KTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIGZhbHNlOyAvLyBkZWZhdWx0IGlzIHRydWUgaWYgY29uZmlndXJhdGlvbiBvZiBjdXJyZW50IGFuZCBmdXR1cmUgcm91dGUgYXJlIHRoZSBzYW1lXG4gIH1cbn1cbiJdfQ==