/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
var UikitRrippleService = /** @class */ (function () {
    function UikitRrippleService() {
    }
    /**
     * @return {?}
     */
    UikitRrippleService.prototype.init = /**
     * @return {?}
     */
    function () {
        document.addEventListener('mousedown', this.rippleMouseDown, false);
    };
    /**
     * @param {?} e
     * @return {?}
     */
    UikitRrippleService.prototype.rippleMouseDown = /**
     * @param {?} e
     * @return {?}
     */
    function (e) {
        /** @type {?} */
        var parentNode = 'parentNode';
        /** @type {?} */
        var isVisible = (/**
         * @param {?} el
         * @return {?}
         */
        function (el) {
            return !!(el.offsetWidth || el.offsetHeight);
        });
        /** @type {?} */
        var selectorMatches = (/**
         * @param {?} el
         * @param {?} selector
         * @return {?}
         */
        function (el, selector) {
            /** @type {?} */
            var matches = 'matches';
            /** @type {?} */
            var webkitMatchesSelector = 'webkitMatchesSelector';
            /** @type {?} */
            var mozMatchesSelector = 'mozMatchesSelector';
            /** @type {?} */
            var msMatchesSelector = 'msMatchesSelector';
            /** @type {?} */
            var p = Element.prototype;
            /** @type {?} */
            var f = p[matches] || p[webkitMatchesSelector] || p[mozMatchesSelector] || p[msMatchesSelector] || (/**
             * @param {?} s
             * @return {?}
             */
            function (s) {
                return [].indexOf.call(document.querySelectorAll(s), this) !== -1;
            });
            return f.call(el, selector);
        });
        /** @type {?} */
        var addClass = (/**
         * @param {?} element
         * @param {?} className
         * @return {?}
         */
        function (element, className) {
            if (element.classList) {
                element.classList.add(className);
            }
            else {
                element.className += ' ' + className;
            }
        });
        /** @type {?} */
        var hasClass = (/**
         * @param {?} element
         * @param {?} className
         * @return {?}
         */
        function (element, className) {
            if (element.classList) {
                return element.classList.contains(className);
            }
            else {
                return new RegExp('(^| )' + className + '( |$)', 'gi').test(element.className);
            }
        });
        /** @type {?} */
        var removeClass = (/**
         * @param {?} element
         * @param {?} className
         * @return {?}
         */
        function (element, className) {
            if (element.classList) {
                element.classList.remove(className);
            }
            else {
                element.className = element.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
            }
        });
        /** @type {?} */
        var getOffset = (/**
         * @param {?} el
         * @return {?}
         */
        function (el) {
            /** @type {?} */
            var rect = el.getBoundingClientRect();
            return {
                top: rect.top + (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0),
                left: rect.left + (window.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft || 0),
            };
        });
        /** @type {?} */
        var rippleEffect = (/**
         * @param {?} element
         * @param {?} e
         * @return {?}
         */
        function (element, e) {
            if (element.querySelector('.ink') === null) {
                /** @type {?} */
                var inkEl = document.createElement('span');
                addClass(inkEl, 'ink');
                if (hasClass(element, 'ripplelink') && element.querySelector('span')) {
                    element.querySelector('span').insertAdjacentHTML('afterend', '<span class=\'ink\'></span>');
                }
                else {
                    element.appendChild(inkEl);
                }
            }
            /** @type {?} */
            var ink = element.querySelector('.ink');
            removeClass(ink, 'ripple-animate');
            if (!ink.offsetHeight && !ink.offsetWidth) {
                /** @type {?} */
                var d = Math.max(element.offsetWidth, element.offsetHeight);
                ink.style.height = d + 'px';
                ink.style.width = d + 'px';
            }
            /** @type {?} */
            var x = e.pageX - getOffset(element).left - (ink.offsetWidth / 2);
            /** @type {?} */
            var y = e.pageY - getOffset(element).top - (ink.offsetHeight / 2);
            ink.style.top = y + 'px';
            ink.style.left = x + 'px';
            ink.style.pointerEvents = 'none';
            addClass(ink, 'ripple-animate');
        });
        for (var target = e.target; target && target !== this; target = target[parentNode]) {
            if (!isVisible(target)) {
                continue;
            }
            if (selectorMatches(target, '.ripplelink, .ui-button, .ui-listbox-item, .ui-multiselect-item, .ui-fieldset-toggler')) {
                /** @type {?} */
                var element = target;
                rippleEffect(element, e);
                break;
            }
        }
    };
    UikitRrippleService.decorators = [
        { type: Injectable, args: [{ providedIn: 'root' },] }
    ];
    /** @nocollapse */
    UikitRrippleService.ctorParameters = function () { return []; };
    /** @nocollapse */ UikitRrippleService.ngInjectableDef = i0.defineInjectable({ factory: function UikitRrippleService_Factory() { return new UikitRrippleService(); }, token: UikitRrippleService, providedIn: "root" });
    return UikitRrippleService;
}());
export { UikitRrippleService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWlraXQtcmlwcGxlLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL3V0aWxzL3Vpa2l0LXJpcHBsZS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsVUFBVSxFQUFTLE1BQU0sZUFBZSxDQUFDOztBQUVqRDtJQUVFO0lBQ0EsQ0FBQzs7OztJQUVELGtDQUFJOzs7SUFBSjtRQUNFLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLGVBQWUsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUN0RSxDQUFDOzs7OztJQUVELDZDQUFlOzs7O0lBQWYsVUFBZ0IsQ0FBQzs7WUFDVCxVQUFVLEdBQUcsWUFBWTs7WUFFekIsU0FBUzs7OztRQUFHLFVBQUMsRUFBRTtZQUNuQixPQUFPLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxXQUFXLElBQUksRUFBRSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQy9DLENBQUMsQ0FBQTs7WUFFSyxlQUFlOzs7OztRQUFHLFVBQUMsRUFBRSxFQUFFLFFBQVE7O2dCQUM3QixPQUFPLEdBQUcsU0FBUzs7Z0JBQ25CLHFCQUFxQixHQUFHLHVCQUF1Qjs7Z0JBQy9DLGtCQUFrQixHQUFHLG9CQUFvQjs7Z0JBQ3pDLGlCQUFpQixHQUFHLG1CQUFtQjs7Z0JBQ3ZDLENBQUMsR0FBRyxPQUFPLENBQUMsU0FBUzs7Z0JBQ3JCLENBQUMsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxDQUFDLGlCQUFpQixDQUFDOzs7O1lBQUksVUFBVSxDQUFDO2dCQUM5RyxPQUFPLEVBQUUsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUNwRSxDQUFDLENBQUE7WUFDRCxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBQzlCLENBQUMsQ0FBQTs7WUFFSyxRQUFROzs7OztRQUFHLFVBQUMsT0FBTyxFQUFFLFNBQVM7WUFDbEMsSUFBSSxPQUFPLENBQUMsU0FBUyxFQUFFO2dCQUNyQixPQUFPLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUNsQztpQkFBTTtnQkFDTCxPQUFPLENBQUMsU0FBUyxJQUFJLEdBQUcsR0FBRyxTQUFTLENBQUM7YUFDdEM7UUFDSCxDQUFDLENBQUE7O1lBRUssUUFBUTs7Ozs7UUFBRyxVQUFDLE9BQU8sRUFBRSxTQUFTO1lBQ2xDLElBQUksT0FBTyxDQUFDLFNBQVMsRUFBRTtnQkFDckIsT0FBTyxPQUFPLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUM5QztpQkFBTTtnQkFDTCxPQUFPLElBQUksTUFBTSxDQUFDLE9BQU8sR0FBRyxTQUFTLEdBQUcsT0FBTyxFQUFFLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUM7YUFDaEY7UUFDSCxDQUFDLENBQUE7O1lBRUssV0FBVzs7Ozs7UUFBRyxVQUFDLE9BQU8sRUFBRSxTQUFTO1lBQ3JDLElBQUksT0FBTyxDQUFDLFNBQVMsRUFBRTtnQkFDckIsT0FBTyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUM7YUFDckM7aUJBQU07Z0JBQ0wsT0FBTyxDQUFDLFNBQVMsR0FBRyxPQUFPLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxJQUFJLE1BQU0sQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsU0FBUyxFQUFFLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO2FBQzlIO1FBQ0gsQ0FBQyxDQUFBOztZQUVLLFNBQVM7Ozs7UUFBRyxVQUFDLEVBQUU7O2dCQUNiLElBQUksR0FBRyxFQUFFLENBQUMscUJBQXFCLEVBQUU7WUFFdkMsT0FBTztnQkFDTCxHQUFHLEVBQUUsSUFBSSxDQUFDLEdBQUcsR0FBRyxDQUFDLE1BQU0sQ0FBQyxXQUFXLElBQUksUUFBUSxDQUFDLGVBQWUsQ0FBQyxTQUFTLElBQUksUUFBUSxDQUFDLElBQUksQ0FBQyxTQUFTLElBQUksQ0FBQyxDQUFDO2dCQUMxRyxJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUksR0FBRyxDQUFDLE1BQU0sQ0FBQyxXQUFXLElBQUksUUFBUSxDQUFDLGVBQWUsQ0FBQyxVQUFVLElBQUksUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFVLElBQUksQ0FBQyxDQUFDO2FBQy9HLENBQUM7UUFDSixDQUFDLENBQUE7O1lBRUssWUFBWTs7Ozs7UUFBRyxVQUFDLE9BQU8sRUFBRSxDQUFDO1lBQzlCLElBQUksT0FBTyxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsS0FBSyxJQUFJLEVBQUU7O29CQUNwQyxLQUFLLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUM7Z0JBQzVDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLENBQUM7Z0JBRXZCLElBQUksUUFBUSxDQUFDLE9BQU8sRUFBRSxZQUFZLENBQUMsSUFBSSxPQUFPLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxFQUFFO29CQUNwRSxPQUFPLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDLGtCQUFrQixDQUFDLFVBQVUsRUFBRSw2QkFBNkIsQ0FBQyxDQUFDO2lCQUM3RjtxQkFBTTtvQkFDTCxPQUFPLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUM1QjthQUNGOztnQkFFSyxHQUFHLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUM7WUFDekMsV0FBVyxDQUFDLEdBQUcsRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDO1lBRW5DLElBQUksQ0FBQyxHQUFHLENBQUMsWUFBWSxJQUFJLENBQUMsR0FBRyxDQUFDLFdBQVcsRUFBRTs7b0JBQ25DLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsT0FBTyxDQUFDLFlBQVksQ0FBQztnQkFDN0QsR0FBRyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQztnQkFDNUIsR0FBRyxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQzthQUM1Qjs7Z0JBRUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLEdBQUcsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksR0FBRyxDQUFDLEdBQUcsQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFDOztnQkFDN0QsQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLEdBQUcsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDLEdBQUcsQ0FBQyxZQUFZLEdBQUcsQ0FBQyxDQUFDO1lBRW5FLEdBQUcsQ0FBQyxLQUFLLENBQUMsR0FBRyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUM7WUFDekIsR0FBRyxDQUFDLEtBQUssQ0FBQyxJQUFJLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQztZQUMxQixHQUFHLENBQUMsS0FBSyxDQUFDLGFBQWEsR0FBRyxNQUFNLENBQUM7WUFDakMsUUFBUSxDQUFDLEdBQUcsRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDO1FBQ2xDLENBQUMsQ0FBQTtRQUVELEtBQUssSUFBSSxNQUFNLEdBQUcsQ0FBQyxDQUFDLE1BQU0sRUFBRSxNQUFNLElBQUksTUFBTSxLQUFLLElBQUksRUFBRSxNQUFNLEdBQUcsTUFBTSxDQUFDLFVBQVUsQ0FBQyxFQUFFO1lBQ2xGLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLEVBQUU7Z0JBQ3RCLFNBQVM7YUFDVjtZQUVELElBQUksZUFBZSxDQUFDLE1BQU0sRUFBRSx1RkFBdUYsQ0FBQyxFQUFFOztvQkFDOUcsT0FBTyxHQUFHLE1BQU07Z0JBQ3RCLFlBQVksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ3pCLE1BQU07YUFDUDtTQUNGO0lBQ0gsQ0FBQzs7Z0JBdEdGLFVBQVUsU0FBQyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUU7Ozs7OzhCQUZsQztDQXlHQyxBQXZHRCxJQXVHQztTQXRHWSxtQkFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0luamVjdGFibGUsIE9uSW5pdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBJbmplY3RhYmxlKHsgcHJvdmlkZWRJbjogJ3Jvb3QnIH0pXG5leHBvcnQgY2xhc3MgVWlraXRScmlwcGxlU2VydmljZSB7XG4gIGNvbnN0cnVjdG9yKCkge1xuICB9XG5cbiAgaW5pdCgpIHtcbiAgICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdtb3VzZWRvd24nLCB0aGlzLnJpcHBsZU1vdXNlRG93biwgZmFsc2UpO1xuICB9XG5cbiAgcmlwcGxlTW91c2VEb3duKGUpIHtcbiAgICBjb25zdCBwYXJlbnROb2RlID0gJ3BhcmVudE5vZGUnO1xuXG4gICAgY29uc3QgaXNWaXNpYmxlID0gKGVsKSA9PiB7XG4gICAgICByZXR1cm4gISEoZWwub2Zmc2V0V2lkdGggfHwgZWwub2Zmc2V0SGVpZ2h0KTtcbiAgICB9XG5cbiAgICBjb25zdCBzZWxlY3Rvck1hdGNoZXMgPSAoZWwsIHNlbGVjdG9yKSA9PiB7XG4gICAgICBjb25zdCBtYXRjaGVzID0gJ21hdGNoZXMnO1xuICAgICAgY29uc3Qgd2Via2l0TWF0Y2hlc1NlbGVjdG9yID0gJ3dlYmtpdE1hdGNoZXNTZWxlY3Rvcic7XG4gICAgICBjb25zdCBtb3pNYXRjaGVzU2VsZWN0b3IgPSAnbW96TWF0Y2hlc1NlbGVjdG9yJztcbiAgICAgIGNvbnN0IG1zTWF0Y2hlc1NlbGVjdG9yID0gJ21zTWF0Y2hlc1NlbGVjdG9yJztcbiAgICAgIGNvbnN0IHAgPSBFbGVtZW50LnByb3RvdHlwZTtcbiAgICAgIGNvbnN0IGYgPSBwW21hdGNoZXNdIHx8IHBbd2Via2l0TWF0Y2hlc1NlbGVjdG9yXSB8fCBwW21vek1hdGNoZXNTZWxlY3Rvcl0gfHwgcFttc01hdGNoZXNTZWxlY3Rvcl0gfHwgZnVuY3Rpb24gKHMpIHtcbiAgICAgICAgcmV0dXJuIFtdLmluZGV4T2YuY2FsbChkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKHMpLCB0aGlzKSAhPT0gLTE7XG4gICAgICB9O1xuICAgICAgcmV0dXJuIGYuY2FsbChlbCwgc2VsZWN0b3IpO1xuICAgIH1cblxuICAgIGNvbnN0IGFkZENsYXNzID0gKGVsZW1lbnQsIGNsYXNzTmFtZSkgPT4ge1xuICAgICAgaWYgKGVsZW1lbnQuY2xhc3NMaXN0KSB7XG4gICAgICAgIGVsZW1lbnQuY2xhc3NMaXN0LmFkZChjbGFzc05hbWUpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgZWxlbWVudC5jbGFzc05hbWUgKz0gJyAnICsgY2xhc3NOYW1lO1xuICAgICAgfVxuICAgIH1cblxuICAgIGNvbnN0IGhhc0NsYXNzID0gKGVsZW1lbnQsIGNsYXNzTmFtZSkgPT4ge1xuICAgICAgaWYgKGVsZW1lbnQuY2xhc3NMaXN0KSB7XG4gICAgICAgIHJldHVybiBlbGVtZW50LmNsYXNzTGlzdC5jb250YWlucyhjbGFzc05hbWUpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuIG5ldyBSZWdFeHAoJyhefCApJyArIGNsYXNzTmFtZSArICcoIHwkKScsICdnaScpLnRlc3QoZWxlbWVudC5jbGFzc05hbWUpO1xuICAgICAgfVxuICAgIH1cblxuICAgIGNvbnN0IHJlbW92ZUNsYXNzID0gKGVsZW1lbnQsIGNsYXNzTmFtZSkgPT4ge1xuICAgICAgaWYgKGVsZW1lbnQuY2xhc3NMaXN0KSB7XG4gICAgICAgIGVsZW1lbnQuY2xhc3NMaXN0LnJlbW92ZShjbGFzc05hbWUpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgZWxlbWVudC5jbGFzc05hbWUgPSBlbGVtZW50LmNsYXNzTmFtZS5yZXBsYWNlKG5ldyBSZWdFeHAoJyhefFxcXFxiKScgKyBjbGFzc05hbWUuc3BsaXQoJyAnKS5qb2luKCd8JykgKyAnKFxcXFxifCQpJywgJ2dpJyksICcgJyk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgY29uc3QgZ2V0T2Zmc2V0ID0gKGVsKSA9PiB7XG4gICAgICBjb25zdCByZWN0ID0gZWwuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XG5cbiAgICAgIHJldHVybiB7XG4gICAgICAgIHRvcDogcmVjdC50b3AgKyAod2luZG93LnBhZ2VZT2Zmc2V0IHx8IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5zY3JvbGxUb3AgfHwgZG9jdW1lbnQuYm9keS5zY3JvbGxUb3AgfHwgMCksXG4gICAgICAgIGxlZnQ6IHJlY3QubGVmdCArICh3aW5kb3cucGFnZVhPZmZzZXQgfHwgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnNjcm9sbExlZnQgfHwgZG9jdW1lbnQuYm9keS5zY3JvbGxMZWZ0IHx8IDApLFxuICAgICAgfTtcbiAgICB9XG5cbiAgICBjb25zdCByaXBwbGVFZmZlY3QgPSAoZWxlbWVudCwgZSkgPT4ge1xuICAgICAgaWYgKGVsZW1lbnQucXVlcnlTZWxlY3RvcignLmluaycpID09PSBudWxsKSB7XG4gICAgICAgIGNvbnN0IGlua0VsID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnc3BhbicpO1xuICAgICAgICBhZGRDbGFzcyhpbmtFbCwgJ2luaycpO1xuXG4gICAgICAgIGlmIChoYXNDbGFzcyhlbGVtZW50LCAncmlwcGxlbGluaycpICYmIGVsZW1lbnQucXVlcnlTZWxlY3Rvcignc3BhbicpKSB7XG4gICAgICAgICAgZWxlbWVudC5xdWVyeVNlbGVjdG9yKCdzcGFuJykuaW5zZXJ0QWRqYWNlbnRIVE1MKCdhZnRlcmVuZCcsICc8c3BhbiBjbGFzcz1cXCdpbmtcXCc+PC9zcGFuPicpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGVsZW1lbnQuYXBwZW5kQ2hpbGQoaW5rRWwpO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGNvbnN0IGluayA9IGVsZW1lbnQucXVlcnlTZWxlY3RvcignLmluaycpO1xuICAgICAgcmVtb3ZlQ2xhc3MoaW5rLCAncmlwcGxlLWFuaW1hdGUnKTtcblxuICAgICAgaWYgKCFpbmsub2Zmc2V0SGVpZ2h0ICYmICFpbmsub2Zmc2V0V2lkdGgpIHtcbiAgICAgICAgY29uc3QgZCA9IE1hdGgubWF4KGVsZW1lbnQub2Zmc2V0V2lkdGgsIGVsZW1lbnQub2Zmc2V0SGVpZ2h0KTtcbiAgICAgICAgaW5rLnN0eWxlLmhlaWdodCA9IGQgKyAncHgnO1xuICAgICAgICBpbmsuc3R5bGUud2lkdGggPSBkICsgJ3B4JztcbiAgICAgIH1cblxuICAgICAgY29uc3QgeCA9IGUucGFnZVggLSBnZXRPZmZzZXQoZWxlbWVudCkubGVmdCAtIChpbmsub2Zmc2V0V2lkdGggLyAyKTtcbiAgICAgIGNvbnN0IHkgPSBlLnBhZ2VZIC0gZ2V0T2Zmc2V0KGVsZW1lbnQpLnRvcCAtIChpbmsub2Zmc2V0SGVpZ2h0IC8gMik7XG5cbiAgICAgIGluay5zdHlsZS50b3AgPSB5ICsgJ3B4JztcbiAgICAgIGluay5zdHlsZS5sZWZ0ID0geCArICdweCc7XG4gICAgICBpbmsuc3R5bGUucG9pbnRlckV2ZW50cyA9ICdub25lJztcbiAgICAgIGFkZENsYXNzKGluaywgJ3JpcHBsZS1hbmltYXRlJyk7XG4gICAgfVxuXG4gICAgZm9yIChsZXQgdGFyZ2V0ID0gZS50YXJnZXQ7IHRhcmdldCAmJiB0YXJnZXQgIT09IHRoaXM7IHRhcmdldCA9IHRhcmdldFtwYXJlbnROb2RlXSkge1xuICAgICAgaWYgKCFpc1Zpc2libGUodGFyZ2V0KSkge1xuICAgICAgICBjb250aW51ZTtcbiAgICAgIH1cblxuICAgICAgaWYgKHNlbGVjdG9yTWF0Y2hlcyh0YXJnZXQsICcucmlwcGxlbGluaywgLnVpLWJ1dHRvbiwgLnVpLWxpc3Rib3gtaXRlbSwgLnVpLW11bHRpc2VsZWN0LWl0ZW0sIC51aS1maWVsZHNldC10b2dnbGVyJykpIHtcbiAgICAgICAgY29uc3QgZWxlbWVudCA9IHRhcmdldDtcbiAgICAgICAgcmlwcGxlRWZmZWN0KGVsZW1lbnQsIGUpO1xuICAgICAgICBicmVhaztcbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cbiJdfQ==