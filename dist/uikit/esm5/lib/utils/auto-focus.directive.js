/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, ElementRef, Input } from '@angular/core';
var AutoFocusDirective = /** @class */ (function () {
    function AutoFocusDirective(el) {
        this.el = el;
    }
    /**
     * @return {?}
     */
    AutoFocusDirective.prototype.ngAfterContentInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        setTimeout((/**
         * @return {?}
         */
        function () {
            _this.hideKeyboard(_this.el.nativeElement);
        }), 500);
    };
    /**
     * @param {?} el
     * @return {?}
     */
    AutoFocusDirective.prototype.hideKeyboard = /**
     * @param {?} el
     * @return {?}
     */
    function (el) {
        /** @type {?} */
        var agent = window.navigator.userAgent.toLowerCase();
        /** @type {?} */
        var att = document.createAttribute('readonly');
        el.setAttributeNode(att);
        setTimeout((/**
         * @return {?}
         */
        function () {
            el.blur();
            /** @type {?} */
            var isSafari = (agent.indexOf('safari') !== -1) && (!(agent.indexOf('chrome') > -1));
            if (!isSafari) {
                el.focus();
            }
            el.removeAttribute('readonly');
        }), 100);
    };
    AutoFocusDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[autofocus]'
                },] }
    ];
    /** @nocollapse */
    AutoFocusDirective.ctorParameters = function () { return [
        { type: ElementRef }
    ]; };
    AutoFocusDirective.propDecorators = {
        appAutoFocus: [{ type: Input }]
    };
    return AutoFocusDirective;
}());
export { AutoFocusDirective };
if (false) {
    /** @type {?} */
    AutoFocusDirective.prototype.appAutoFocus;
    /**
     * @type {?}
     * @private
     */
    AutoFocusDirective.prototype.el;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0by1mb2N1cy5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL3V0aWxzL2F1dG8tZm9jdXMuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQW1CLFNBQVMsRUFBRSxVQUFVLEVBQUUsS0FBSyxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBRTdFO0lBT0UsNEJBQTJCLEVBQWM7UUFBZCxPQUFFLEdBQUYsRUFBRSxDQUFZO0lBRXpDLENBQUM7Ozs7SUFFTSwrQ0FBa0I7OztJQUF6QjtRQUFBLGlCQU1DO1FBSkMsVUFBVTs7O1FBQUM7WUFDVCxLQUFJLENBQUMsWUFBWSxDQUFDLEtBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDM0MsQ0FBQyxHQUFFLEdBQUcsQ0FBQyxDQUFDO0lBRVYsQ0FBQzs7Ozs7SUFFRCx5Q0FBWTs7OztJQUFaLFVBQWEsRUFBTzs7WUFDWixLQUFLLEdBQUcsTUFBTSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFOztZQUNoRCxHQUFHLEdBQUcsUUFBUSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUM7UUFDaEQsRUFBRSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3pCLFVBQVU7OztRQUFDO1lBQ1QsRUFBRSxDQUFDLElBQUksRUFBRSxDQUFDOztnQkFDSixRQUFRLEdBQUcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3RGLElBQUksQ0FBQyxRQUFRLEVBQUU7Z0JBQ2IsRUFBRSxDQUFDLEtBQUssRUFBRSxDQUFDO2FBQ1o7WUFDRCxFQUFFLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ2pDLENBQUMsR0FBRSxHQUFHLENBQUMsQ0FBQztJQUNWLENBQUM7O2dCQS9CRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLGFBQWE7aUJBQ3hCOzs7O2dCQUpvQyxVQUFVOzs7K0JBTzVDLEtBQUs7O0lBMkJSLHlCQUFDO0NBQUEsQUFoQ0QsSUFnQ0M7U0E3Qlksa0JBQWtCOzs7SUFFN0IsMENBQXNDOzs7OztJQUVuQixnQ0FBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0FmdGVyQ29udGVudEluaXQsIERpcmVjdGl2ZSwgRWxlbWVudFJlZiwgSW5wdXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5ARGlyZWN0aXZlKHtcbiAgc2VsZWN0b3I6ICdbYXV0b2ZvY3VzXSdcbn0pXG5leHBvcnQgY2xhc3MgQXV0b0ZvY3VzRGlyZWN0aXZlIGltcGxlbWVudHMgQWZ0ZXJDb250ZW50SW5pdCB7XG5cbiAgQElucHV0KCkgcHVibGljIGFwcEF1dG9Gb2N1czogYm9vbGVhbjtcblxuICBwdWJsaWMgY29uc3RydWN0b3IocHJpdmF0ZSBlbDogRWxlbWVudFJlZikge1xuXG4gIH1cblxuICBwdWJsaWMgbmdBZnRlckNvbnRlbnRJbml0KCkge1xuXG4gICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICB0aGlzLmhpZGVLZXlib2FyZCh0aGlzLmVsLm5hdGl2ZUVsZW1lbnQpO1xuICAgIH0sIDUwMCk7XG5cbiAgfVxuXG4gIGhpZGVLZXlib2FyZChlbDogYW55KSB7XG4gICAgY29uc3QgYWdlbnQgPSB3aW5kb3cubmF2aWdhdG9yLnVzZXJBZ2VudC50b0xvd2VyQ2FzZSgpO1xuICAgIGNvbnN0IGF0dCA9IGRvY3VtZW50LmNyZWF0ZUF0dHJpYnV0ZSgncmVhZG9ubHknKTtcbiAgICBlbC5zZXRBdHRyaWJ1dGVOb2RlKGF0dCk7XG4gICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICBlbC5ibHVyKCk7XG4gICAgICBjb25zdCBpc1NhZmFyaSA9IChhZ2VudC5pbmRleE9mKCdzYWZhcmknKSAhPT0gLTEpICYmICghKGFnZW50LmluZGV4T2YoJ2Nocm9tZScpID4gLTEpKTtcbiAgICAgIGlmICghaXNTYWZhcmkpIHtcbiAgICAgICAgZWwuZm9jdXMoKTtcbiAgICAgIH1cbiAgICAgIGVsLnJlbW92ZUF0dHJpYnV0ZSgncmVhZG9ubHknKTtcbiAgICB9LCAxMDApO1xuICB9XG59XG4iXX0=