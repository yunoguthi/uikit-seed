/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { OperationsService } from './operations.service';
import { timer } from 'rxjs';
import { OperationsComponent } from './operations.component';
import { MatSnackBar } from '@angular/material';
var OperationsManagerService = /** @class */ (function () {
    function OperationsManagerService(operationsService, snackBar) {
        var _this = this;
        this.operationsService = operationsService;
        this.snackBar = snackBar;
        // Atualiza a variavel conforme o observable atualizar de valor
        this.operationsService.isLoading$.subscribe((/**
         * @param {?} isLoading
         * @return {?}
         */
        function (isLoading) {
            if (isLoading) {
                // Verifico se o snackbar (de operações) já está sendo exibida
                // para evitar abrir outra (ocasionando em um fecha/abre desnecessário)
                if (_this.isLoadSnackbarActive()) {
                    /** @type {?} */
                    var loadingMaterialDialog = _this.snackBar.openFromComponent(OperationsComponent, {
                        verticalPosition: 'bottom',
                        horizontalPosition: 'center',
                        panelClass: 'operation-dialog',
                        announcementMessage: 'Processando...',
                    });
                }
            }
            else {
                timer(2500).subscribe((/**
                 * @param {?} loading
                 * @return {?}
                 */
                function (loading) {
                    if (!_this.operationsService.isLoading) {
                        _this.close();
                    }
                }));
            }
        }));
    }
    /**
     * @private
     * @return {?}
     */
    OperationsManagerService.prototype.isLoadSnackbarActive = /**
     * @private
     * @return {?}
     */
    function () {
        return !(this.snackBar._openedSnackBarRef &&
            this.snackBar._openedSnackBarRef.instance instanceof OperationsComponent);
    };
    /**
     * @return {?}
     */
    OperationsManagerService.prototype.close = /**
     * @return {?}
     */
    function () {
        this.snackBar.dismiss();
    };
    OperationsManagerService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    OperationsManagerService.ctorParameters = function () { return [
        { type: OperationsService },
        { type: MatSnackBar }
    ]; };
    return OperationsManagerService;
}());
export { OperationsManagerService };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    OperationsManagerService.prototype.operationsService;
    /**
     * @type {?}
     * @protected
     */
    OperationsManagerService.prototype.snackBar;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3BlcmF0aW9ucy1tYW5hZ2VyLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL29wZXJhdGlvbnMvb3BlcmF0aW9ucy1tYW5hZ2VyLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDekQsT0FBTyxFQUFFLEtBQUssRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUM3QixPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUM3RCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFFaEQ7SUFPRSxrQ0FBc0IsaUJBQW9DLEVBQVksUUFBcUI7UUFBM0YsaUJBMkJDO1FBM0JxQixzQkFBaUIsR0FBakIsaUJBQWlCLENBQW1CO1FBQVksYUFBUSxHQUFSLFFBQVEsQ0FBYTtRQUV6RiwrREFBK0Q7UUFDL0QsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxTQUFTOzs7O1FBQUMsVUFBQyxTQUFTO1lBQ3BELElBQUksU0FBUyxFQUFFO2dCQUNiLDhEQUE4RDtnQkFDOUQsdUVBQXVFO2dCQUN2RSxJQUFJLEtBQUksQ0FBQyxvQkFBb0IsRUFBRSxFQUFFOzt3QkFFekIscUJBQXFCLEdBQUcsS0FBSSxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxtQkFBbUIsRUFDL0U7d0JBQ0UsZ0JBQWdCLEVBQUUsUUFBUTt3QkFDMUIsa0JBQWtCLEVBQUUsUUFBUTt3QkFDNUIsVUFBVSxFQUFFLGtCQUFrQjt3QkFDOUIsbUJBQW1CLEVBQUUsZ0JBQWdCO3FCQUN0QyxDQUNGO2lCQUNGO2FBQ0Y7aUJBQU07Z0JBQ0wsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLFNBQVM7Ozs7Z0JBQUMsVUFBQyxPQUFPO29CQUM1QixJQUFJLENBQUMsS0FBSSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsRUFBRTt3QkFDckMsS0FBSSxDQUFDLEtBQUssRUFBRSxDQUFDO3FCQUNkO2dCQUNILENBQUMsRUFBQyxDQUFDO2FBQ0o7UUFDSCxDQUFDLEVBQUMsQ0FBQztJQUVMLENBQUM7Ozs7O0lBaENPLHVEQUFvQjs7OztJQUE1QjtRQUNFLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsa0JBQWtCO1lBQ3ZDLElBQUksQ0FBQyxRQUFRLENBQUMsa0JBQWtCLENBQUMsUUFBUSxZQUFZLG1CQUFtQixDQUFDLENBQUM7SUFDOUUsQ0FBQzs7OztJQStCTSx3Q0FBSzs7O0lBQVo7UUFDRSxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQzFCLENBQUM7O2dCQXRDRixVQUFVOzs7O2dCQUxGLGlCQUFpQjtnQkFHakIsV0FBVzs7SUF5Q3BCLCtCQUFDO0NBQUEsQUF2Q0QsSUF1Q0M7U0F0Q1ksd0JBQXdCOzs7Ozs7SUFNdkIscURBQThDOzs7OztJQUFFLDRDQUErQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE9wZXJhdGlvbnNTZXJ2aWNlIH0gZnJvbSAnLi9vcGVyYXRpb25zLnNlcnZpY2UnO1xuaW1wb3J0IHsgdGltZXIgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IE9wZXJhdGlvbnNDb21wb25lbnQgfSBmcm9tICcuL29wZXJhdGlvbnMuY29tcG9uZW50JztcbmltcG9ydCB7IE1hdFNuYWNrQmFyIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgT3BlcmF0aW9uc01hbmFnZXJTZXJ2aWNlIHtcbiAgcHJpdmF0ZSBpc0xvYWRTbmFja2JhckFjdGl2ZSgpIHtcbiAgICByZXR1cm4gISh0aGlzLnNuYWNrQmFyLl9vcGVuZWRTbmFja0JhclJlZiAmJlxuICAgICAgdGhpcy5zbmFja0Jhci5fb3BlbmVkU25hY2tCYXJSZWYuaW5zdGFuY2UgaW5zdGFuY2VvZiBPcGVyYXRpb25zQ29tcG9uZW50KTtcbiAgfVxuXG4gIGNvbnN0cnVjdG9yKHByb3RlY3RlZCBvcGVyYXRpb25zU2VydmljZTogT3BlcmF0aW9uc1NlcnZpY2UsIHByb3RlY3RlZCBzbmFja0JhcjogTWF0U25hY2tCYXIpIHtcblxuICAgIC8vIEF0dWFsaXphIGEgdmFyaWF2ZWwgY29uZm9ybWUgbyBvYnNlcnZhYmxlIGF0dWFsaXphciBkZSB2YWxvclxuICAgIHRoaXMub3BlcmF0aW9uc1NlcnZpY2UuaXNMb2FkaW5nJC5zdWJzY3JpYmUoKGlzTG9hZGluZykgPT4ge1xuICAgICAgaWYgKGlzTG9hZGluZykge1xuICAgICAgICAvLyBWZXJpZmljbyBzZSBvIHNuYWNrYmFyIChkZSBvcGVyYcOnw7VlcykgasOhIGVzdMOhIHNlbmRvIGV4aWJpZGFcbiAgICAgICAgLy8gcGFyYSBldml0YXIgYWJyaXIgb3V0cmEgKG9jYXNpb25hbmRvIGVtIHVtIGZlY2hhL2FicmUgZGVzbmVjZXNzw6FyaW8pXG4gICAgICAgIGlmICh0aGlzLmlzTG9hZFNuYWNrYmFyQWN0aXZlKCkpIHtcblxuICAgICAgICAgIGNvbnN0IGxvYWRpbmdNYXRlcmlhbERpYWxvZyA9IHRoaXMuc25hY2tCYXIub3BlbkZyb21Db21wb25lbnQoT3BlcmF0aW9uc0NvbXBvbmVudCxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgdmVydGljYWxQb3NpdGlvbjogJ2JvdHRvbScsXG4gICAgICAgICAgICAgIGhvcml6b250YWxQb3NpdGlvbjogJ2NlbnRlcicsXG4gICAgICAgICAgICAgIHBhbmVsQ2xhc3M6ICdvcGVyYXRpb24tZGlhbG9nJyxcbiAgICAgICAgICAgICAgYW5ub3VuY2VtZW50TWVzc2FnZTogJ1Byb2Nlc3NhbmRvLi4uJyxcbiAgICAgICAgICAgIH1cbiAgICAgICAgICApO1xuICAgICAgICB9XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aW1lcigyNTAwKS5zdWJzY3JpYmUoKGxvYWRpbmcpID0+IHtcbiAgICAgICAgICBpZiAoIXRoaXMub3BlcmF0aW9uc1NlcnZpY2UuaXNMb2FkaW5nKSB7XG4gICAgICAgICAgICB0aGlzLmNsb3NlKCk7XG4gICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9KTtcblxuICB9XG5cbiAgcHVibGljIGNsb3NlKCkge1xuICAgIHRoaXMuc25hY2tCYXIuZGlzbWlzcygpO1xuICB9XG59XG4iXX0=