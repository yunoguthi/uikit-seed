/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { BehaviorSubject, combineLatest, throwError } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';
/** @enum {string} */
var OperationState = {
    Idle: 'idle',
    Processing: 'processing',
    Processed: 'processed',
    Cancelling: 'cancelling',
    Cancelled: 'cancelled',
    Undoing: 'undoing',
    Undone: 'undone',
    Error: 'error',
};
export { OperationState };
var OperationsSnackBar = /** @class */ (function () {
    function OperationsSnackBar(description, allowToCancel) {
        if (allowToCancel === void 0) { allowToCancel = false; }
        var _this = this;
        this.description = description;
        this.allowToCancel = allowToCancel;
        this.operationStateSubject$ = new BehaviorSubject(OperationState.Idle);
        this.operationState$ = this.operationStateSubject$.asObservable();
        this.operationState = OperationState.Processing;
        this.error = null;
        this.operation$ = null;
        this.undoOperation = null;
        this.isUndoableSubject$ = new BehaviorSubject(false);
        this.isUndoable$ = this.isUndoableSubject$.asObservable();
        this.isUndoable = false;
        this.canUndoSubject$ = new BehaviorSubject(false);
        this.canUndo$ = this.canUndoSubject$.asObservable();
        this.canUndo = false;
        this.cancelOperation = null;
        this.isCancellableSubject$ = new BehaviorSubject(false);
        this.isCancellable$ = this.isCancellableSubject$.asObservable();
        this.isCancellable = false;
        this.canCancelSubject$ = new BehaviorSubject(false);
        this.canCancel$ = this.canCancelSubject$.asObservable();
        this.canCancel = false;
        // Mantem atualizado as variáveis 'não observável'
        this.operationState$.subscribe((/**
         * @param {?} state
         * @return {?}
         */
        function (state) { return _this.operationState = state; }));
        this.isUndoable$.subscribe((/**
         * @param {?} isUndoable
         * @return {?}
         */
        function (isUndoable) { return _this.isUndoable = isUndoable; }));
        this.isCancellable$.subscribe((/**
         * @param {?} isCancellable
         * @return {?}
         */
        function (isCancellable) { return _this.isCancellable = isCancellable; }));
        combineLatest(this.operationState$, this.isCancellable$).subscribe((/**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            /** @type {?} */
            var state = value[0];
            /** @type {?} */
            var isCancellable = value[1];
            if (state === OperationState.Processing && _this.allowToCancel) {
                _this.canCancel = isCancellable;
            }
            else {
                _this.canCancel = false;
            }
            _this.canCancelSubject$.next(_this.canCancel);
        }));
        combineLatest(this.operationState$, this.isUndoable$).subscribe((/**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            /** @type {?} */
            var state = value[0];
            /** @type {?} */
            var isUndoable = value[1];
            if (state === OperationState.Processed) {
                _this.canUndo = isUndoable;
            }
            else {
                _this.canUndo = false;
            }
            _this.canUndoSubject$.next(_this.canUndo);
        }));
    }
    /**
     * @param {?} subscription
     * @return {?}
     */
    OperationsSnackBar.prototype.setCancelOperation = /**
     * @param {?} subscription
     * @return {?}
     */
    function (subscription) {
        if (!subscription) {
            throw Error('Não é possível setar uma operação de cancelar com a subscrição nula (null) ou indefinida (undefined)!');
        }
        if (subscription && subscription.closed === false) {
            this.cancelOperation = subscription;
            this.isCancellableSubject$.next(true);
        }
    };
    /**
     * @param {?} undoFunction
     * @return {?}
     */
    OperationsSnackBar.prototype.setUndoOperation = /**
     * @param {?} undoFunction
     * @return {?}
     */
    function (undoFunction) {
        if (!undoFunction) {
            throw Error('Não é possível setar uma operação de desfazer com a operação nula (null) ou indefinida (undefined)!');
        }
        else {
            this.undoOperation = undoFunction;
            this.isUndoableSubject$.next(true);
        }
    };
    /**
     * @param {?} operation
     * @return {?}
     */
    OperationsSnackBar.prototype.setOperation = /**
     * @param {?} operation
     * @return {?}
     */
    function (operation) {
        var _this = this;
        /** @type {?} */
        var operationObservavel = operation
            .pipe(catchError((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            return _this.setError(error);
        })), finalize((/**
         * @return {?}
         */
        function () {
            _this.setAsProcessed();
        })));
        this.operation$ = operationObservavel;
    };
    /**
     * @return {?}
     */
    OperationsSnackBar.prototype.setAsProcessed = /**
     * @return {?}
     */
    function () {
        this.setState(OperationState.Processed);
    };
    /**
     * @param {?} error
     * @return {?}
     */
    OperationsSnackBar.prototype.setError = /**
     * @param {?} error
     * @return {?}
     */
    function (error) {
        this.operationStateSubject$.next(OperationState.Error);
        this.error = error;
        return throwError(error);
    };
    /**
     * @protected
     * @param {?} operationState
     * @return {?}
     */
    OperationsSnackBar.prototype.setState = /**
     * @protected
     * @param {?} operationState
     * @return {?}
     */
    function (operationState) {
        this.operationStateSubject$.next(operationState);
    };
    /**
     * @return {?}
     */
    OperationsSnackBar.prototype.setAsStarted = /**
     * @return {?}
     */
    function () {
        this.setState(OperationState.Processing);
    };
    /**
     * @return {?}
     */
    OperationsSnackBar.prototype.undo = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (!this.isUndoableSubject$.getValue()) {
            throw Error('Não é possível desfazer esta operação!');
        }
        /** @type {?} */
        var retorno = null;
        this.operationStateSubject$.next(OperationState.Undoing);
        /** @type {?} */
        var undoOperation = this.undoOperation;
        /** @type {?} */
        var observable = (/** @type {?} */ (undoOperation));
        /** @type {?} */
        var isObservable = observable.pipe;
        if (isObservable) {
            retorno = observable.pipe(catchError((/**
             * @param {?} error
             * @return {?}
             */
            function (error) { return _this.setError(error); })))
                .subscribe((/**
             * @return {?}
             */
            function () { return _this.operationStateSubject$.next(OperationState.Undone); }));
        }
        /** @type {?} */
        var promise = (/** @type {?} */ (undoOperation));
        /** @type {?} */
        var isPromise = promise.then;
        if (isPromise) {
            retorno = promise.then((/**
             * @return {?}
             */
            function () { return _this.operationStateSubject$.next(OperationState.Undone); }), (/**
             * @param {?} error
             * @return {?}
             */
            function (error) { return _this.setError(error); }));
        }
        /** @type {?} */
        var undoLambda = (/** @type {?} */ (undoOperation));
        /** @type {?} */
        var isUndoLambda = undoLambda.call;
        if ((!isObservable) && (!isPromise) && isUndoLambda) {
            try {
                retorno = undoLambda();
                this.operationStateSubject$.next(OperationState.Undone);
            }
            catch (error) {
                this.setError(error);
                throw error;
            }
        }
        return retorno;
    };
    /**
     * @return {?}
     */
    OperationsSnackBar.prototype.cancel = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (!this.canCancel) {
            throw Error('Não é possível cancelar esta operação!');
        }
        this.operationStateSubject$.next(OperationState.Cancelling);
        if (this.cancelOperation && this.cancelOperation.closed === false) {
            this.cancelOperation.add((/**
             * @return {?}
             */
            function () {
                _this.operationStateSubject$.next(OperationState.Cancelled);
                _this.isCancellableSubject$.next(false);
            }));
            this.cancelOperation.unsubscribe();
        }
    };
    return OperationsSnackBar;
}());
export { OperationsSnackBar };
if (false) {
    /**
     * @type {?}
     * @private
     */
    OperationsSnackBar.prototype.operationStateSubject$;
    /** @type {?} */
    OperationsSnackBar.prototype.operationState$;
    /** @type {?} */
    OperationsSnackBar.prototype.operationState;
    /** @type {?} */
    OperationsSnackBar.prototype.error;
    /** @type {?} */
    OperationsSnackBar.prototype.operation$;
    /**
     * @type {?}
     * @protected
     */
    OperationsSnackBar.prototype.undoOperation;
    /**
     * @type {?}
     * @private
     */
    OperationsSnackBar.prototype.isUndoableSubject$;
    /**
     * @type {?}
     * @protected
     */
    OperationsSnackBar.prototype.isUndoable$;
    /**
     * @type {?}
     * @protected
     */
    OperationsSnackBar.prototype.isUndoable;
    /**
     * @type {?}
     * @private
     */
    OperationsSnackBar.prototype.canUndoSubject$;
    /** @type {?} */
    OperationsSnackBar.prototype.canUndo$;
    /** @type {?} */
    OperationsSnackBar.prototype.canUndo;
    /**
     * @type {?}
     * @protected
     */
    OperationsSnackBar.prototype.cancelOperation;
    /**
     * @type {?}
     * @private
     */
    OperationsSnackBar.prototype.isCancellableSubject$;
    /**
     * @type {?}
     * @protected
     */
    OperationsSnackBar.prototype.isCancellable$;
    /**
     * @type {?}
     * @protected
     */
    OperationsSnackBar.prototype.isCancellable;
    /**
     * @type {?}
     * @private
     */
    OperationsSnackBar.prototype.canCancelSubject$;
    /** @type {?} */
    OperationsSnackBar.prototype.canCancel$;
    /** @type {?} */
    OperationsSnackBar.prototype.canCancel;
    /** @type {?} */
    OperationsSnackBar.prototype.description;
    /** @type {?} */
    OperationsSnackBar.prototype.allowToCancel;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3BlcmF0aW9ucy5zbmFja2Jhci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvb3BlcmF0aW9ucy9vcGVyYXRpb25zLnNuYWNrYmFyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQWdCLGVBQWUsRUFBYyxhQUFhLEVBQUUsVUFBVSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQzVGLE9BQU8sRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7OztJQUdwRCxNQUFPLE1BQU07SUFDYixZQUFhLFlBQVk7SUFDekIsV0FBWSxXQUFXO0lBQ3ZCLFlBQWEsWUFBWTtJQUN6QixXQUFZLFdBQVc7SUFDdkIsU0FBVSxTQUFTO0lBQ25CLFFBQVMsUUFBUTtJQUNqQixPQUFRLE9BQU87OztBQUdqQjtJQTZCRSw0QkFDUyxXQUFtQixFQUNuQixhQUFxQjtRQUFyQiw4QkFBQSxFQUFBLHFCQUFxQjtRQUY5QixpQkE4QkM7UUE3QlEsZ0JBQVcsR0FBWCxXQUFXLENBQVE7UUFDbkIsa0JBQWEsR0FBYixhQUFhLENBQVE7UUE3QnRCLDJCQUFzQixHQUFHLElBQUksZUFBZSxDQUFpQixjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDbkYsb0JBQWUsR0FBRyxJQUFJLENBQUMsc0JBQXNCLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDN0QsbUJBQWMsR0FBRyxjQUFjLENBQUMsVUFBVSxDQUFDO1FBRTNDLFVBQUssR0FBVSxJQUFJLENBQUM7UUFFcEIsZUFBVSxHQUFvQixJQUFJLENBQUM7UUFFaEMsa0JBQWEsR0FBcUQsSUFBSSxDQUFDO1FBQ3pFLHVCQUFrQixHQUFHLElBQUksZUFBZSxDQUFVLEtBQUssQ0FBQyxDQUFDO1FBQ3ZELGdCQUFXLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3JELGVBQVUsR0FBRyxLQUFLLENBQUM7UUFFckIsb0JBQWUsR0FBRyxJQUFJLGVBQWUsQ0FBVSxLQUFLLENBQUMsQ0FBQztRQUN2RCxhQUFRLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUMvQyxZQUFPLEdBQUcsS0FBSyxDQUFDO1FBR2Isb0JBQWUsR0FBaUIsSUFBSSxDQUFDO1FBQ3ZDLDBCQUFxQixHQUFHLElBQUksZUFBZSxDQUFVLEtBQUssQ0FBQyxDQUFDO1FBQzFELG1CQUFjLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFlBQVksRUFBRSxDQUFDO1FBQzNELGtCQUFhLEdBQUcsS0FBSyxDQUFDO1FBRXhCLHNCQUFpQixHQUFHLElBQUksZUFBZSxDQUFVLEtBQUssQ0FBQyxDQUFDO1FBQ3pELGVBQVUsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDbkQsY0FBUyxHQUFHLEtBQUssQ0FBQztRQU12QixrREFBa0Q7UUFDbEQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTOzs7O1FBQUMsVUFBQyxLQUFLLElBQUssT0FBQSxLQUFJLENBQUMsY0FBYyxHQUFHLEtBQUssRUFBM0IsQ0FBMkIsRUFBQyxDQUFDO1FBQ3ZFLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUzs7OztRQUFDLFVBQUEsVUFBVSxJQUFJLE9BQUEsS0FBSSxDQUFDLFVBQVUsR0FBRyxVQUFVLEVBQTVCLENBQTRCLEVBQUMsQ0FBQztRQUN2RSxJQUFJLENBQUMsY0FBYyxDQUFDLFNBQVM7Ozs7UUFBQyxVQUFBLGFBQWEsSUFBSSxPQUFBLEtBQUksQ0FBQyxhQUFhLEdBQUcsYUFBYSxFQUFsQyxDQUFrQyxFQUFDLENBQUM7UUFFbkYsYUFBYSxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLFNBQVM7Ozs7UUFBQyxVQUFDLEtBQUs7O2dCQUNqRSxLQUFLLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQzs7Z0JBQ2hCLGFBQWEsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQzlCLElBQUksS0FBSyxLQUFLLGNBQWMsQ0FBQyxVQUFVLElBQUksS0FBSSxDQUFDLGFBQWEsRUFBRTtnQkFDN0QsS0FBSSxDQUFDLFNBQVMsR0FBRyxhQUFhLENBQUM7YUFDaEM7aUJBQU07Z0JBQ0wsS0FBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7YUFDeEI7WUFDRCxLQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUM5QyxDQUFDLEVBQUMsQ0FBQztRQUVILGFBQWEsQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxTQUFTOzs7O1FBQUMsVUFBQyxLQUFLOztnQkFDOUQsS0FBSyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUM7O2dCQUNoQixVQUFVLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUMzQixJQUFJLEtBQUssS0FBSyxjQUFjLENBQUMsU0FBUyxFQUFFO2dCQUN0QyxLQUFJLENBQUMsT0FBTyxHQUFHLFVBQVUsQ0FBQzthQUMzQjtpQkFBTTtnQkFDTCxLQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQzthQUN0QjtZQUNELEtBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUMxQyxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRU0sK0NBQWtCOzs7O0lBQXpCLFVBQTBCLFlBQTBCO1FBQ2xELElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDakIsTUFBTSxLQUFLLENBQUMsdUdBQXVHLENBQUMsQ0FBQztTQUN0SDtRQUNELElBQUksWUFBWSxJQUFJLFlBQVksQ0FBQyxNQUFNLEtBQUssS0FBSyxFQUFFO1lBQ2pELElBQUksQ0FBQyxlQUFlLEdBQUcsWUFBWSxDQUFDO1lBQ3BDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDdkM7SUFDSCxDQUFDOzs7OztJQUVNLDZDQUFnQjs7OztJQUF2QixVQUF3QixZQUE4RDtRQUNwRixJQUFJLENBQUMsWUFBWSxFQUFFO1lBQ2pCLE1BQU0sS0FBSyxDQUFDLHFHQUFxRyxDQUFDLENBQUM7U0FDcEg7YUFBTTtZQUNMLElBQUksQ0FBQyxhQUFhLEdBQUcsWUFBWSxDQUFDO1lBQ2xDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDcEM7SUFDSCxDQUFDOzs7OztJQUVNLHlDQUFZOzs7O0lBQW5CLFVBQW9CLFNBQTBCO1FBQTlDLGlCQVlDOztZQVhPLG1CQUFtQixHQUFHLFNBQVM7YUFDbEMsSUFBSSxDQUNILFVBQVU7Ozs7UUFBQyxVQUFDLEtBQUs7WUFDZixPQUFPLEtBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDOUIsQ0FBQyxFQUFDLEVBQ0YsUUFBUTs7O1FBQUM7WUFDUCxLQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDeEIsQ0FBQyxFQUFDLENBQ0g7UUFFSCxJQUFJLENBQUMsVUFBVSxHQUFHLG1CQUFtQixDQUFDO0lBQ3hDLENBQUM7Ozs7SUFFTSwyQ0FBYzs7O0lBQXJCO1FBQ0UsSUFBSSxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDMUMsQ0FBQzs7Ozs7SUFFTSxxQ0FBUTs7OztJQUFmLFVBQWdCLEtBQVk7UUFDMUIsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDdkQsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7UUFDbkIsT0FBTyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDM0IsQ0FBQzs7Ozs7O0lBRVMscUNBQVE7Ozs7O0lBQWxCLFVBQW1CLGNBQThCO1FBQy9DLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7SUFDbkQsQ0FBQzs7OztJQUVELHlDQUFZOzs7SUFBWjtRQUNFLElBQUksQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQzNDLENBQUM7Ozs7SUFFTSxpQ0FBSTs7O0lBQVg7UUFBQSxpQkFxQ0M7UUFwQ0MsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLEVBQUUsRUFBRTtZQUN2QyxNQUFNLEtBQUssQ0FBQyx3Q0FBd0MsQ0FBQyxDQUFDO1NBQ3ZEOztZQUNHLE9BQU8sR0FBRyxJQUFJO1FBQ2xCLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxDQUFDOztZQUVuRCxhQUFhLEdBQUcsSUFBSSxDQUFDLGFBQWE7O1lBQ2xDLFVBQVUsR0FBRyxtQkFBQSxhQUFhLEVBQW1COztZQUM3QyxZQUFZLEdBQUcsVUFBVSxDQUFDLElBQUk7UUFDcEMsSUFBSSxZQUFZLEVBQUU7WUFDaEIsT0FBTyxHQUFHLFVBQVUsQ0FBQyxJQUFJLENBQUMsVUFBVTs7OztZQUFDLFVBQUEsS0FBSyxJQUFJLE9BQUEsS0FBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsRUFBcEIsQ0FBb0IsRUFBQyxDQUFDO2lCQUNqRSxTQUFTOzs7WUFBQyxjQUFNLE9BQUEsS0FBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLEVBQXZELENBQXVELEVBQUMsQ0FBQztTQUM3RTs7WUFFSyxPQUFPLEdBQUcsbUJBQUEsYUFBYSxFQUFvQjs7WUFDM0MsU0FBUyxHQUFHLE9BQU8sQ0FBQyxJQUFJO1FBQzlCLElBQUksU0FBUyxFQUFFO1lBQ2IsT0FBTyxHQUFHLE9BQU8sQ0FBQyxJQUFJOzs7WUFDcEIsY0FBTSxPQUFBLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxFQUF2RCxDQUF1RDs7OztZQUM3RCxVQUFDLEtBQUssSUFBSyxPQUFBLEtBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEVBQXBCLENBQW9CLEVBQ2hDLENBQUM7U0FDSDs7WUFFSyxVQUFVLEdBQUcsbUJBQUEsYUFBYSxFQUFlOztZQUN6QyxZQUFZLEdBQUcsVUFBVSxDQUFDLElBQUk7UUFDcEMsSUFBSSxDQUFDLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLFlBQVksRUFBRTtZQUNuRCxJQUFJO2dCQUNGLE9BQU8sR0FBRyxVQUFVLEVBQUUsQ0FBQztnQkFDdkIsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDekQ7WUFBQyxPQUFPLEtBQUssRUFBRTtnQkFDZCxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNyQixNQUFNLEtBQUssQ0FBQzthQUNiO1NBQ0Y7UUFFRCxPQUFPLE9BQU8sQ0FBQztJQUNqQixDQUFDOzs7O0lBR00sbUNBQU07OztJQUFiO1FBQUEsaUJBWUM7UUFYQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNuQixNQUFNLEtBQUssQ0FBQyx3Q0FBd0MsQ0FBQyxDQUFDO1NBQ3ZEO1FBQ0QsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDNUQsSUFBSSxJQUFJLENBQUMsZUFBZSxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxLQUFLLEtBQUssRUFBRTtZQUNqRSxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUc7OztZQUFDO2dCQUN2QixLQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDM0QsS0FBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN6QyxDQUFDLEVBQUMsQ0FBQztZQUNILElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDcEM7SUFDSCxDQUFDO0lBQ0gseUJBQUM7QUFBRCxDQUFDLEFBcktELElBcUtDOzs7Ozs7O0lBbktDLG9EQUEwRjs7SUFDMUYsNkNBQW9FOztJQUNwRSw0Q0FBa0Q7O0lBRWxELG1DQUEyQjs7SUFFM0Isd0NBQTBDOzs7OztJQUUxQywyQ0FBaUY7Ozs7O0lBQ2pGLGdEQUFpRTs7Ozs7SUFDakUseUNBQStEOzs7OztJQUMvRCx3Q0FBNkI7Ozs7O0lBRTdCLDZDQUE4RDs7SUFDOUQsc0NBQXNEOztJQUN0RCxxQ0FBdUI7Ozs7O0lBR3ZCLDZDQUErQzs7Ozs7SUFDL0MsbURBQW9FOzs7OztJQUNwRSw0Q0FBcUU7Ozs7O0lBQ3JFLDJDQUFnQzs7Ozs7SUFFaEMsK0NBQWdFOztJQUNoRSx3Q0FBMEQ7O0lBQzFELHVDQUF5Qjs7SUFHdkIseUNBQTBCOztJQUMxQiwyQ0FBNEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBTdWJzY3JpcHRpb24sIEJlaGF2aW9yU3ViamVjdCwgT2JzZXJ2YWJsZSwgY29tYmluZUxhdGVzdCwgdGhyb3dFcnJvciB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgY2F0Y2hFcnJvciwgZmluYWxpemUgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5cbmV4cG9ydCBlbnVtIE9wZXJhdGlvblN0YXRlIHtcbiAgSWRsZSA9ICdpZGxlJyxcbiAgUHJvY2Vzc2luZyA9ICdwcm9jZXNzaW5nJyxcbiAgUHJvY2Vzc2VkID0gJ3Byb2Nlc3NlZCcsXG4gIENhbmNlbGxpbmcgPSAnY2FuY2VsbGluZycsXG4gIENhbmNlbGxlZCA9ICdjYW5jZWxsZWQnLFxuICBVbmRvaW5nID0gJ3VuZG9pbmcnLFxuICBVbmRvbmUgPSAndW5kb25lJyxcbiAgRXJyb3IgPSAnZXJyb3InXG59XG5cbmV4cG9ydCBjbGFzcyBPcGVyYXRpb25zU25hY2tCYXIge1xuXG4gIHByaXZhdGUgb3BlcmF0aW9uU3RhdGVTdWJqZWN0JCA9IG5ldyBCZWhhdmlvclN1YmplY3Q8T3BlcmF0aW9uU3RhdGU+KE9wZXJhdGlvblN0YXRlLklkbGUpO1xuICBwdWJsaWMgb3BlcmF0aW9uU3RhdGUkID0gdGhpcy5vcGVyYXRpb25TdGF0ZVN1YmplY3QkLmFzT2JzZXJ2YWJsZSgpO1xuICBwdWJsaWMgb3BlcmF0aW9uU3RhdGUgPSBPcGVyYXRpb25TdGF0ZS5Qcm9jZXNzaW5nO1xuXG4gIHB1YmxpYyBlcnJvcjogRXJyb3IgPSBudWxsO1xuXG4gIHB1YmxpYyBvcGVyYXRpb24kOiBPYnNlcnZhYmxlPGFueT4gPSBudWxsO1xuXG4gIHByb3RlY3RlZCB1bmRvT3BlcmF0aW9uOiAoKCkgPT4gYW55KSB8IE9ic2VydmFibGU8YW55PiB8IFByb21pc2VMaWtlPGFueT4gPSBudWxsO1xuICBwcml2YXRlIGlzVW5kb2FibGVTdWJqZWN0JCA9IG5ldyBCZWhhdmlvclN1YmplY3Q8Ym9vbGVhbj4oZmFsc2UpO1xuICBwcm90ZWN0ZWQgaXNVbmRvYWJsZSQgPSB0aGlzLmlzVW5kb2FibGVTdWJqZWN0JC5hc09ic2VydmFibGUoKTtcbiAgcHJvdGVjdGVkIGlzVW5kb2FibGUgPSBmYWxzZTtcblxuICBwcml2YXRlIGNhblVuZG9TdWJqZWN0JCA9IG5ldyBCZWhhdmlvclN1YmplY3Q8Ym9vbGVhbj4oZmFsc2UpO1xuICBwdWJsaWMgY2FuVW5kbyQgPSB0aGlzLmNhblVuZG9TdWJqZWN0JC5hc09ic2VydmFibGUoKTtcbiAgcHVibGljIGNhblVuZG8gPSBmYWxzZTtcblxuXG4gIHByb3RlY3RlZCBjYW5jZWxPcGVyYXRpb246IFN1YnNjcmlwdGlvbiA9IG51bGw7XG4gIHByaXZhdGUgaXNDYW5jZWxsYWJsZVN1YmplY3QkID0gbmV3IEJlaGF2aW9yU3ViamVjdDxib29sZWFuPihmYWxzZSk7XG4gIHByb3RlY3RlZCBpc0NhbmNlbGxhYmxlJCA9IHRoaXMuaXNDYW5jZWxsYWJsZVN1YmplY3QkLmFzT2JzZXJ2YWJsZSgpO1xuICBwcm90ZWN0ZWQgaXNDYW5jZWxsYWJsZSA9IGZhbHNlO1xuXG4gIHByaXZhdGUgY2FuQ2FuY2VsU3ViamVjdCQgPSBuZXcgQmVoYXZpb3JTdWJqZWN0PGJvb2xlYW4+KGZhbHNlKTtcbiAgcHVibGljIGNhbkNhbmNlbCQgPSB0aGlzLmNhbkNhbmNlbFN1YmplY3QkLmFzT2JzZXJ2YWJsZSgpO1xuICBwdWJsaWMgY2FuQ2FuY2VsID0gZmFsc2U7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHVibGljIGRlc2NyaXB0aW9uOiBzdHJpbmcsXG4gICAgcHVibGljIGFsbG93VG9DYW5jZWwgPSBmYWxzZVxuICApIHtcbiAgICAvLyBNYW50ZW0gYXR1YWxpemFkbyBhcyB2YXJpw6F2ZWlzICduw6NvIG9ic2VydsOhdmVsJ1xuICAgIHRoaXMub3BlcmF0aW9uU3RhdGUkLnN1YnNjcmliZSgoc3RhdGUpID0+IHRoaXMub3BlcmF0aW9uU3RhdGUgPSBzdGF0ZSk7XG4gICAgdGhpcy5pc1VuZG9hYmxlJC5zdWJzY3JpYmUoaXNVbmRvYWJsZSA9PiB0aGlzLmlzVW5kb2FibGUgPSBpc1VuZG9hYmxlKTtcbiAgICB0aGlzLmlzQ2FuY2VsbGFibGUkLnN1YnNjcmliZShpc0NhbmNlbGxhYmxlID0+IHRoaXMuaXNDYW5jZWxsYWJsZSA9IGlzQ2FuY2VsbGFibGUpO1xuXG4gICAgY29tYmluZUxhdGVzdCh0aGlzLm9wZXJhdGlvblN0YXRlJCwgdGhpcy5pc0NhbmNlbGxhYmxlJCkuc3Vic2NyaWJlKCh2YWx1ZSkgPT4ge1xuICAgICAgY29uc3Qgc3RhdGUgPSB2YWx1ZVswXTtcbiAgICAgIGNvbnN0IGlzQ2FuY2VsbGFibGUgPSB2YWx1ZVsxXTtcbiAgICAgIGlmIChzdGF0ZSA9PT0gT3BlcmF0aW9uU3RhdGUuUHJvY2Vzc2luZyAmJiB0aGlzLmFsbG93VG9DYW5jZWwpIHtcbiAgICAgICAgdGhpcy5jYW5DYW5jZWwgPSBpc0NhbmNlbGxhYmxlO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5jYW5DYW5jZWwgPSBmYWxzZTtcbiAgICAgIH1cbiAgICAgIHRoaXMuY2FuQ2FuY2VsU3ViamVjdCQubmV4dCh0aGlzLmNhbkNhbmNlbCk7XG4gICAgfSk7XG5cbiAgICBjb21iaW5lTGF0ZXN0KHRoaXMub3BlcmF0aW9uU3RhdGUkLCB0aGlzLmlzVW5kb2FibGUkKS5zdWJzY3JpYmUoKHZhbHVlKSA9PiB7XG4gICAgICBjb25zdCBzdGF0ZSA9IHZhbHVlWzBdO1xuICAgICAgY29uc3QgaXNVbmRvYWJsZSA9IHZhbHVlWzFdO1xuICAgICAgaWYgKHN0YXRlID09PSBPcGVyYXRpb25TdGF0ZS5Qcm9jZXNzZWQpIHtcbiAgICAgICAgdGhpcy5jYW5VbmRvID0gaXNVbmRvYWJsZTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMuY2FuVW5kbyA9IGZhbHNlO1xuICAgICAgfVxuICAgICAgdGhpcy5jYW5VbmRvU3ViamVjdCQubmV4dCh0aGlzLmNhblVuZG8pO1xuICAgIH0pO1xuICB9XG5cbiAgcHVibGljIHNldENhbmNlbE9wZXJhdGlvbihzdWJzY3JpcHRpb246IFN1YnNjcmlwdGlvbikge1xuICAgIGlmICghc3Vic2NyaXB0aW9uKSB7XG4gICAgICB0aHJvdyBFcnJvcignTsOjbyDDqSBwb3Nzw612ZWwgc2V0YXIgdW1hIG9wZXJhw6fDo28gZGUgY2FuY2VsYXIgY29tIGEgc3Vic2NyacOnw6NvIG51bGEgKG51bGwpIG91IGluZGVmaW5pZGEgKHVuZGVmaW5lZCkhJyk7XG4gICAgfVxuICAgIGlmIChzdWJzY3JpcHRpb24gJiYgc3Vic2NyaXB0aW9uLmNsb3NlZCA9PT0gZmFsc2UpIHtcbiAgICAgIHRoaXMuY2FuY2VsT3BlcmF0aW9uID0gc3Vic2NyaXB0aW9uO1xuICAgICAgdGhpcy5pc0NhbmNlbGxhYmxlU3ViamVjdCQubmV4dCh0cnVlKTtcbiAgICB9XG4gIH1cblxuICBwdWJsaWMgc2V0VW5kb09wZXJhdGlvbih1bmRvRnVuY3Rpb246ICgoKSA9PiBhbnkpIHwgT2JzZXJ2YWJsZTxhbnk+IHwgUHJvbWlzZUxpa2U8YW55Pikge1xuICAgIGlmICghdW5kb0Z1bmN0aW9uKSB7XG4gICAgICB0aHJvdyBFcnJvcignTsOjbyDDqSBwb3Nzw612ZWwgc2V0YXIgdW1hIG9wZXJhw6fDo28gZGUgZGVzZmF6ZXIgY29tIGEgb3BlcmHDp8OjbyBudWxhIChudWxsKSBvdSBpbmRlZmluaWRhICh1bmRlZmluZWQpIScpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnVuZG9PcGVyYXRpb24gPSB1bmRvRnVuY3Rpb247XG4gICAgICB0aGlzLmlzVW5kb2FibGVTdWJqZWN0JC5uZXh0KHRydWUpO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyBzZXRPcGVyYXRpb24ob3BlcmF0aW9uOiBPYnNlcnZhYmxlPGFueT4pIHtcbiAgICBjb25zdCBvcGVyYXRpb25PYnNlcnZhdmVsID0gb3BlcmF0aW9uXG4gICAgICAucGlwZShcbiAgICAgICAgY2F0Y2hFcnJvcigoZXJyb3IpID0+IHtcbiAgICAgICAgICByZXR1cm4gdGhpcy5zZXRFcnJvcihlcnJvcik7XG4gICAgICAgIH0pLFxuICAgICAgICBmaW5hbGl6ZSgoKSA9PiB7XG4gICAgICAgICAgdGhpcy5zZXRBc1Byb2Nlc3NlZCgpO1xuICAgICAgICB9KSxcbiAgICAgICk7XG5cbiAgICB0aGlzLm9wZXJhdGlvbiQgPSBvcGVyYXRpb25PYnNlcnZhdmVsO1xuICB9XG5cbiAgcHVibGljIHNldEFzUHJvY2Vzc2VkKCk6IHZvaWQge1xuICAgIHRoaXMuc2V0U3RhdGUoT3BlcmF0aW9uU3RhdGUuUHJvY2Vzc2VkKTtcbiAgfVxuXG4gIHB1YmxpYyBzZXRFcnJvcihlcnJvcjogRXJyb3IpIHtcbiAgICB0aGlzLm9wZXJhdGlvblN0YXRlU3ViamVjdCQubmV4dChPcGVyYXRpb25TdGF0ZS5FcnJvcik7XG4gICAgdGhpcy5lcnJvciA9IGVycm9yO1xuICAgIHJldHVybiB0aHJvd0Vycm9yKGVycm9yKTtcbiAgfVxuXG4gIHByb3RlY3RlZCBzZXRTdGF0ZShvcGVyYXRpb25TdGF0ZTogT3BlcmF0aW9uU3RhdGUpIHtcbiAgICB0aGlzLm9wZXJhdGlvblN0YXRlU3ViamVjdCQubmV4dChvcGVyYXRpb25TdGF0ZSk7XG4gIH1cblxuICBzZXRBc1N0YXJ0ZWQoKSB7XG4gICAgdGhpcy5zZXRTdGF0ZShPcGVyYXRpb25TdGF0ZS5Qcm9jZXNzaW5nKTtcbiAgfVxuXG4gIHB1YmxpYyB1bmRvKCkge1xuICAgIGlmICghdGhpcy5pc1VuZG9hYmxlU3ViamVjdCQuZ2V0VmFsdWUoKSkge1xuICAgICAgdGhyb3cgRXJyb3IoJ07Do28gw6kgcG9zc8OtdmVsIGRlc2ZhemVyIGVzdGEgb3BlcmHDp8OjbyEnKTtcbiAgICB9XG4gICAgbGV0IHJldG9ybm8gPSBudWxsO1xuICAgIHRoaXMub3BlcmF0aW9uU3RhdGVTdWJqZWN0JC5uZXh0KE9wZXJhdGlvblN0YXRlLlVuZG9pbmcpO1xuXG4gICAgY29uc3QgdW5kb09wZXJhdGlvbiA9IHRoaXMudW5kb09wZXJhdGlvbjtcbiAgICBjb25zdCBvYnNlcnZhYmxlID0gdW5kb09wZXJhdGlvbiBhcyBPYnNlcnZhYmxlPGFueT47XG4gICAgY29uc3QgaXNPYnNlcnZhYmxlID0gb2JzZXJ2YWJsZS5waXBlO1xuICAgIGlmIChpc09ic2VydmFibGUpIHtcbiAgICAgIHJldG9ybm8gPSBvYnNlcnZhYmxlLnBpcGUoY2F0Y2hFcnJvcihlcnJvciA9PiB0aGlzLnNldEVycm9yKGVycm9yKSkpXG4gICAgICAgIC5zdWJzY3JpYmUoKCkgPT4gdGhpcy5vcGVyYXRpb25TdGF0ZVN1YmplY3QkLm5leHQoT3BlcmF0aW9uU3RhdGUuVW5kb25lKSk7XG4gICAgfVxuXG4gICAgY29uc3QgcHJvbWlzZSA9IHVuZG9PcGVyYXRpb24gYXMgUHJvbWlzZUxpa2U8YW55PjtcbiAgICBjb25zdCBpc1Byb21pc2UgPSBwcm9taXNlLnRoZW47XG4gICAgaWYgKGlzUHJvbWlzZSkge1xuICAgICAgcmV0b3JubyA9IHByb21pc2UudGhlbihcbiAgICAgICAgKCkgPT4gdGhpcy5vcGVyYXRpb25TdGF0ZVN1YmplY3QkLm5leHQoT3BlcmF0aW9uU3RhdGUuVW5kb25lKSxcbiAgICAgICAgKGVycm9yKSA9PiB0aGlzLnNldEVycm9yKGVycm9yKVxuICAgICAgKTtcbiAgICB9XG5cbiAgICBjb25zdCB1bmRvTGFtYmRhID0gdW5kb09wZXJhdGlvbiBhcyAoKCkgPT4gYW55KTtcbiAgICBjb25zdCBpc1VuZG9MYW1iZGEgPSB1bmRvTGFtYmRhLmNhbGw7XG4gICAgaWYgKCghaXNPYnNlcnZhYmxlKSAmJiAoIWlzUHJvbWlzZSkgJiYgaXNVbmRvTGFtYmRhKSB7XG4gICAgICB0cnkge1xuICAgICAgICByZXRvcm5vID0gdW5kb0xhbWJkYSgpO1xuICAgICAgICB0aGlzLm9wZXJhdGlvblN0YXRlU3ViamVjdCQubmV4dChPcGVyYXRpb25TdGF0ZS5VbmRvbmUpO1xuICAgICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgICAgdGhpcy5zZXRFcnJvcihlcnJvcik7XG4gICAgICAgIHRocm93IGVycm9yO1xuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiByZXRvcm5vO1xuICB9XG5cblxuICBwdWJsaWMgY2FuY2VsKCkge1xuICAgIGlmICghdGhpcy5jYW5DYW5jZWwpIHtcbiAgICAgIHRocm93IEVycm9yKCdOw6NvIMOpIHBvc3PDrXZlbCBjYW5jZWxhciBlc3RhIG9wZXJhw6fDo28hJyk7XG4gICAgfVxuICAgIHRoaXMub3BlcmF0aW9uU3RhdGVTdWJqZWN0JC5uZXh0KE9wZXJhdGlvblN0YXRlLkNhbmNlbGxpbmcpO1xuICAgIGlmICh0aGlzLmNhbmNlbE9wZXJhdGlvbiAmJiB0aGlzLmNhbmNlbE9wZXJhdGlvbi5jbG9zZWQgPT09IGZhbHNlKSB7XG4gICAgICB0aGlzLmNhbmNlbE9wZXJhdGlvbi5hZGQoKCkgPT4ge1xuICAgICAgICB0aGlzLm9wZXJhdGlvblN0YXRlU3ViamVjdCQubmV4dChPcGVyYXRpb25TdGF0ZS5DYW5jZWxsZWQpO1xuICAgICAgICB0aGlzLmlzQ2FuY2VsbGFibGVTdWJqZWN0JC5uZXh0KGZhbHNlKTtcbiAgICAgIH0pO1xuICAgICAgdGhpcy5jYW5jZWxPcGVyYXRpb24udW5zdWJzY3JpYmUoKTtcbiAgICB9XG4gIH1cbn1cbiJdfQ==