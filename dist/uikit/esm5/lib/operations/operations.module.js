/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OperationsComponent } from './operations.component';
import { MatProgressSpinnerModule, MatSnackBarModule, MatListModule, MatExpansionModule, MatButtonModule, MatIconModule, MatTooltipModule } from '@angular/material';
import { OperationsService } from './operations.service';
import { OperationsManagerService } from './operations-manager.service';
var OperationsModule = /** @class */ (function () {
    function OperationsModule(operationsManagerService) {
        this.operationsManagerService = operationsManagerService;
    }
    OperationsModule.decorators = [
        { type: NgModule, args: [{
                    entryComponents: [OperationsComponent],
                    declarations: [OperationsComponent],
                    imports: [
                        CommonModule,
                        MatProgressSpinnerModule,
                        MatSnackBarModule,
                        MatListModule,
                        MatExpansionModule,
                        MatButtonModule,
                        MatIconModule,
                        MatTooltipModule
                    ],
                    exports: [OperationsComponent],
                    providers: [OperationsService, OperationsManagerService]
                },] }
    ];
    /** @nocollapse */
    OperationsModule.ctorParameters = function () { return [
        { type: OperationsManagerService }
    ]; };
    return OperationsModule;
}());
export { OperationsModule };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    OperationsModule.prototype.operationsManagerService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3BlcmF0aW9ucy5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL29wZXJhdGlvbnMvb3BlcmF0aW9ucy5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQzdELE9BQU8sRUFDTCx3QkFBd0IsRUFBRSxpQkFBaUIsRUFBRSxhQUFhLEVBQzFELGtCQUFrQixFQUFFLGVBQWUsRUFBRSxhQUFhLEVBQUUsZ0JBQWdCLEVBQ3JFLE1BQU0sbUJBQW1CLENBQUM7QUFDM0IsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDekQsT0FBTyxFQUFFLHdCQUF3QixFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFFeEU7SUFpQkUsMEJBQXNCLHdCQUFrRDtRQUFsRCw2QkFBd0IsR0FBeEIsd0JBQXdCLENBQTBCO0lBQ3hFLENBQUM7O2dCQWxCRixRQUFRLFNBQUM7b0JBQ1IsZUFBZSxFQUFFLENBQUMsbUJBQW1CLENBQUM7b0JBQ3RDLFlBQVksRUFBRSxDQUFDLG1CQUFtQixDQUFDO29CQUNuQyxPQUFPLEVBQUU7d0JBQ1AsWUFBWTt3QkFDWix3QkFBd0I7d0JBQ3hCLGlCQUFpQjt3QkFDakIsYUFBYTt3QkFDYixrQkFBa0I7d0JBQ2xCLGVBQWU7d0JBQ2YsYUFBYTt3QkFDYixnQkFBZ0I7cUJBQ2pCO29CQUNELE9BQU8sRUFBRSxDQUFDLG1CQUFtQixDQUFDO29CQUM5QixTQUFTLEVBQUUsQ0FBQyxpQkFBaUIsRUFBRSx3QkFBd0IsQ0FBQztpQkFDekQ7Ozs7Z0JBakJRLHdCQUF3Qjs7SUFxQmpDLHVCQUFDO0NBQUEsQUFuQkQsSUFtQkM7U0FIWSxnQkFBZ0I7Ozs7OztJQUNmLG9EQUE0RCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHsgT3BlcmF0aW9uc0NvbXBvbmVudCB9IGZyb20gJy4vb3BlcmF0aW9ucy5jb21wb25lbnQnO1xuaW1wb3J0IHtcbiAgTWF0UHJvZ3Jlc3NTcGlubmVyTW9kdWxlLCBNYXRTbmFja0Jhck1vZHVsZSwgTWF0TGlzdE1vZHVsZSxcbiAgTWF0RXhwYW5zaW9uTW9kdWxlLCBNYXRCdXR0b25Nb2R1bGUsIE1hdEljb25Nb2R1bGUsIE1hdFRvb2x0aXBNb2R1bGVcbn0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xuaW1wb3J0IHsgT3BlcmF0aW9uc1NlcnZpY2UgfSBmcm9tICcuL29wZXJhdGlvbnMuc2VydmljZSc7XG5pbXBvcnQgeyBPcGVyYXRpb25zTWFuYWdlclNlcnZpY2UgfSBmcm9tICcuL29wZXJhdGlvbnMtbWFuYWdlci5zZXJ2aWNlJztcblxuQE5nTW9kdWxlKHtcbiAgZW50cnlDb21wb25lbnRzOiBbT3BlcmF0aW9uc0NvbXBvbmVudF0sXG4gIGRlY2xhcmF0aW9uczogW09wZXJhdGlvbnNDb21wb25lbnRdLFxuICBpbXBvcnRzOiBbXG4gICAgQ29tbW9uTW9kdWxlLFxuICAgIE1hdFByb2dyZXNzU3Bpbm5lck1vZHVsZSxcbiAgICBNYXRTbmFja0Jhck1vZHVsZSxcbiAgICBNYXRMaXN0TW9kdWxlLFxuICAgIE1hdEV4cGFuc2lvbk1vZHVsZSxcbiAgICBNYXRCdXR0b25Nb2R1bGUsXG4gICAgTWF0SWNvbk1vZHVsZSxcbiAgICBNYXRUb29sdGlwTW9kdWxlXG4gIF0sXG4gIGV4cG9ydHM6IFtPcGVyYXRpb25zQ29tcG9uZW50XSxcbiAgcHJvdmlkZXJzOiBbT3BlcmF0aW9uc1NlcnZpY2UsIE9wZXJhdGlvbnNNYW5hZ2VyU2VydmljZV1cbn0pXG5leHBvcnQgY2xhc3MgT3BlcmF0aW9uc01vZHVsZSB7XG4gIGNvbnN0cnVjdG9yKHByb3RlY3RlZCBvcGVyYXRpb25zTWFuYWdlclNlcnZpY2U6IE9wZXJhdGlvbnNNYW5hZ2VyU2VydmljZSkge1xuICB9XG59XG4iXX0=