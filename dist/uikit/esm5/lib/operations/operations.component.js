/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ViewEncapsulation } from '@angular/core';
import { OperationsService } from './operations.service';
import { trigger, style, state, transition, animate } from '@angular/animations';
import { OperationsManagerService } from './operations-manager.service';
var OperationsComponent = /** @class */ (function () {
    function OperationsComponent(operationsService, operationsManagerService) {
        this.operationsService = operationsService;
        this.operationsManagerService = operationsManagerService;
    }
    /**
     * @return {?}
     */
    OperationsComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.operations$ = this.operationsService.operations$;
        this.isLoading$ = this.operationsService.isLoading$;
    };
    /**
     * @return {?}
     */
    OperationsComponent.prototype.close = /**
     * @return {?}
     */
    function () {
        this.operationsManagerService.close();
    };
    OperationsComponent.decorators = [
        { type: Component, args: [{
                    selector: 'uikit-operations',
                    template: "<mat-expansion-panel [expanded]=\"(operations$ | async).length == 1\">\n\n  <mat-expansion-panel-header *ngIf=\"(operations$ | async).length > 1\">\n    <mat-panel-title>\n      <div *ngIf=\"(isLoading$ | async); else notLoading\">\n        Processando {{ (operations$ | async).length }} opera\u00E7\u00F5es...\n      </div>\n      <ng-template #notLoading>\n        Finalizado\n      </ng-template>\n    </mat-panel-title>\n\n  </mat-expansion-panel-header>\n\n  <mat-list>\n    <mat-list-item *ngFor=\"let operation of (operations$ | async)\" [@fadeInOut]>\n      <h4 matLine>{{ operation.description }}</h4>\n\n      <mat-progress-spinner\n        *ngIf=\"\n        (operation.operationState$ | async) === 'processing' ||\n        (operation.operationState$ | async) === 'cancelling' ||\n        (operation.operationState$ | async) === 'undoing'\n        \"\n        [diameter]=\"20\" mode=\"indeterminate\" color=\"primary\"></mat-progress-spinner>\n\n      <div *ngIf=\"(operation.operationState$ | async) === 'processed'\">Finalizado</div>\n      <div *ngIf=\"(operation.operationState$ | async) === 'cancelling'\">Cancelando</div>\n      <div *ngIf=\"(operation.operationState$ | async) === 'cancelled'\">Cancelado</div>\n      <div *ngIf=\"(operation.operationState$ | async) === 'undoing'\">Desfazendo</div>\n      <div *ngIf=\"(operation.operationState$ | async) === 'undone'\">Desfeito</div>\n      <div *ngIf=\"(operation.operationState$ | async) === 'error'\" matTooltip=\"{{ operation.error.message }}\">Erro</div>\n\n        <button *ngIf=\"operation.canUndo$ | async\"\n        (click)=\"operation.undo()\" mat-icon-button><mat-icon class=\"fas fa-undo\"></mat-icon></button>\n\n        <button *ngIf=\"operation.canCancel$ | async\"\n        (click)=\"operation.cancel()\" mat-icon-button><mat-icon class=\"fas fa-times-circle\"></mat-icon></button>\n\n    </mat-list-item>\n  </mat-list>\n\n</mat-expansion-panel>\n",
                    encapsulation: ViewEncapsulation.None,
                    animations: [
                        trigger('fadeInOut', [
                            state('void', style({
                                opacity: 0
                            })),
                            transition('void => *', animate(250)),
                            transition('* => void', animate(500)),
                        ]),
                    ],
                    styles: [".operation-dialog{background:0 0;padding:0;min-height:auto!important}.operation-dialog .mat-expansion-panel .mat-expansion-panel-header{background:rgba(0,0,0,.9)!important;border-radius:0;padding:0 15px}.operation-dialog .mat-expansion-panel .mat-expansion-panel-header .mat-expansion-indicator:after,.operation-dialog .mat-expansion-panel .mat-expansion-panel-header .mat-expansion-panel-header-title{color:#fff}.operation-dialog .mat-expansion-panel .mat-expansion-panel-body{padding:0 15px!important;background:rgba(0,0,0,.85)!important}.operation-dialog .mat-expansion-panel .mat-expansion-panel-body .mat-list{padding:0!important}.operation-dialog .mat-expansion-panel .mat-expansion-panel-body .mat-list .mat-list-item{min-height:initial!important;padding:10px 0!important;color:#fff!important;border-bottom:1px solid rgba(255,255,255,.1)}.operation-dialog .mat-expansion-panel .mat-expansion-panel-body .mat-list .mat-list-item:last-child{border:none}.operation-dialog .mat-list-item-content{padding:0!important}"]
                }] }
    ];
    /** @nocollapse */
    OperationsComponent.ctorParameters = function () { return [
        { type: OperationsService },
        { type: OperationsManagerService }
    ]; };
    return OperationsComponent;
}());
export { OperationsComponent };
if (false) {
    /** @type {?} */
    OperationsComponent.prototype.operations$;
    /** @type {?} */
    OperationsComponent.prototype.isLoading$;
    /**
     * @type {?}
     * @protected
     */
    OperationsComponent.prototype.operationsService;
    /**
     * @type {?}
     * @protected
     */
    OperationsComponent.prototype.operationsManagerService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3BlcmF0aW9ucy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL29wZXJhdGlvbnMvb3BlcmF0aW9ucy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsaUJBQWlCLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDckUsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFHekQsT0FBTyxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxPQUFPLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUNqRixPQUFPLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQUV4RTtJQW9CRSw2QkFDWSxpQkFBb0MsRUFDcEMsd0JBQWtEO1FBRGxELHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBbUI7UUFDcEMsNkJBQXdCLEdBQXhCLHdCQUF3QixDQUEwQjtJQUMxRCxDQUFDOzs7O0lBRUwsc0NBQVE7OztJQUFSO1FBQ0UsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsV0FBVyxDQUFDO1FBQ3RELElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFVBQVUsQ0FBQztJQUN0RCxDQUFDOzs7O0lBRUQsbUNBQUs7OztJQUFMO1FBQ0UsSUFBSSxDQUFDLHdCQUF3QixDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ3hDLENBQUM7O2dCQWhDRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLGtCQUFrQjtvQkFDNUIseTVEQUEwQztvQkFFMUMsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7b0JBQ3JDLFVBQVUsRUFBRTt3QkFDVixPQUFPLENBQUMsV0FBVyxFQUFFOzRCQUNuQixLQUFLLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQztnQ0FDbEIsT0FBTyxFQUFFLENBQUM7NkJBQ1gsQ0FBQyxDQUFDOzRCQUNILFVBQVUsQ0FBQyxXQUFXLEVBQUUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDOzRCQUNyQyxVQUFVLENBQUMsV0FBVyxFQUFFLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQzt5QkFDdEMsQ0FBQztxQkFDSDs7aUJBQ0Y7Ozs7Z0JBcEJRLGlCQUFpQjtnQkFJakIsd0JBQXdCOztJQW1DakMsMEJBQUM7Q0FBQSxBQWpDRCxJQWlDQztTQWxCWSxtQkFBbUI7OztJQUU5QiwwQ0FBcUQ7O0lBQ3JELHlDQUF1Qzs7Ozs7SUFHckMsZ0RBQThDOzs7OztJQUM5Qyx1REFBNEQiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgVmlld0VuY2Fwc3VsYXRpb24gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE9wZXJhdGlvbnNTZXJ2aWNlIH0gZnJvbSAnLi9vcGVyYXRpb25zLnNlcnZpY2UnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgT3BlcmF0aW9uc1NuYWNrQmFyIH0gZnJvbSAnLi9vcGVyYXRpb25zLnNuYWNrYmFyJztcbmltcG9ydCB7IHRyaWdnZXIsIHN0eWxlLCBzdGF0ZSwgdHJhbnNpdGlvbiwgYW5pbWF0ZSB9IGZyb20gJ0Bhbmd1bGFyL2FuaW1hdGlvbnMnO1xuaW1wb3J0IHsgT3BlcmF0aW9uc01hbmFnZXJTZXJ2aWNlIH0gZnJvbSAnLi9vcGVyYXRpb25zLW1hbmFnZXIuc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ3Vpa2l0LW9wZXJhdGlvbnMnLFxuICB0ZW1wbGF0ZVVybDogJy4vb3BlcmF0aW9ucy5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL29wZXJhdGlvbnMuY29tcG9uZW50LnNjc3MnXSxcbiAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZSxcbiAgYW5pbWF0aW9uczogW1xuICAgIHRyaWdnZXIoJ2ZhZGVJbk91dCcsIFtcbiAgICAgIHN0YXRlKCd2b2lkJywgc3R5bGUoe1xuICAgICAgICBvcGFjaXR5OiAwXG4gICAgICB9KSksXG4gICAgICB0cmFuc2l0aW9uKCd2b2lkID0+IConLCBhbmltYXRlKDI1MCkpLFxuICAgICAgdHJhbnNpdGlvbignKiA9PiB2b2lkJywgYW5pbWF0ZSg1MDApKSxcbiAgICBdKSxcbiAgXVxufSlcbmV4cG9ydCBjbGFzcyBPcGVyYXRpb25zQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBwdWJsaWMgb3BlcmF0aW9ucyQ6IE9ic2VydmFibGU8T3BlcmF0aW9uc1NuYWNrQmFyW10+O1xuICBwdWJsaWMgaXNMb2FkaW5nJDogT2JzZXJ2YWJsZTxib29sZWFuPjtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcm90ZWN0ZWQgb3BlcmF0aW9uc1NlcnZpY2U6IE9wZXJhdGlvbnNTZXJ2aWNlLFxuICAgIHByb3RlY3RlZCBvcGVyYXRpb25zTWFuYWdlclNlcnZpY2U6IE9wZXJhdGlvbnNNYW5hZ2VyU2VydmljZVxuICApIHsgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIHRoaXMub3BlcmF0aW9ucyQgPSB0aGlzLm9wZXJhdGlvbnNTZXJ2aWNlLm9wZXJhdGlvbnMkO1xuICAgIHRoaXMuaXNMb2FkaW5nJCA9IHRoaXMub3BlcmF0aW9uc1NlcnZpY2UuaXNMb2FkaW5nJDtcbiAgfVxuXG4gIGNsb3NlKCkge1xuICAgIHRoaXMub3BlcmF0aW9uc01hbmFnZXJTZXJ2aWNlLmNsb3NlKCk7XG4gIH1cbn1cbiJdfQ==