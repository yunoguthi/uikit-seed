/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, Optional } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { SwUpdate } from '@angular/service-worker';
import { LogService } from '../utils/log/log.service';
import { map } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "@angular/service-worker";
import * as i2 from "../utils/log/log.service";
var UpdateInfoService = /** @class */ (function () {
    function UpdateInfoService(swUpdate, logService) {
        var _this = this;
        this.swUpdate = swUpdate;
        this.logService = logService;
        this.hasUpdateSubject$ = new BehaviorSubject(false);
        this.hasUpdate$ = this.hasUpdateSubject$.asObservable();
        this.installPromptEventSubject$ = new BehaviorSubject(null);
        this.installPromptEvent$ = this.installPromptEventSubject$.asObservable();
        this.hasInstallOption$ = this.installPromptEvent$.pipe(map((/**
         * @param {?} r
         * @return {?}
         */
        function (r) { return r != null; })));
        window.addEventListener('beforeinstallprompt', (/**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            _this.logService.debug('beforeinstallprompt fired!');
            // Prevent Chrome <= 67 from automatically showing the prompt
            // event.preventDefault();
            // Stash the event so it can be triggered later.
            _this.installPromptEventSubject$.next(event);
            // Update the install UI to notify the user app can be installed
            // (doing that by the btnInstall that is displayed if there a installPromptEvent instance)
            //(<any>document.querySelector('#btnInstall')).disabled = false;
        }));
    }
    /**
     * @return {?}
     */
    UpdateInfoService.prototype.setUpdateAsAvailable = /**
     * @return {?}
     */
    function () {
        this.logService.debug("Atualiza\u00E7\u00E3o encontrada!");
        this.hasUpdateSubject$.next(true);
    };
    /**
     * @return {?}
     */
    UpdateInfoService.prototype.install = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.logService.debug('install fired!');
        /** @type {?} */
        var installPromptEvent = this.installPromptEventSubject$.getValue();
        if (installPromptEvent) {
            this.logService.debug('installPromptEvent');
            installPromptEvent.prompt();
            // Wait for the user to respond to the prompt
            installPromptEvent.userChoice.then((/**
             * @param {?} choice
             * @return {?}
             */
            function (choice) {
                if (choice.outcome === 'accepted') {
                    _this.logService.debug('User accepted the A2HS prompt');
                }
                else {
                    _this.logService.debug('User dismissed the A2HS prompt');
                }
                // Clear the saved prompt since it can't be used again
                _this.installPromptEventSubject$.next(null);
            }));
        }
    };
    UpdateInfoService.decorators = [
        { type: Injectable, args: [{ providedIn: 'root' },] }
    ];
    /** @nocollapse */
    UpdateInfoService.ctorParameters = function () { return [
        { type: SwUpdate, decorators: [{ type: Optional }] },
        { type: LogService }
    ]; };
    /** @nocollapse */ UpdateInfoService.ngInjectableDef = i0.defineInjectable({ factory: function UpdateInfoService_Factory() { return new UpdateInfoService(i0.inject(i1.SwUpdate, 8), i0.inject(i2.LogService)); }, token: UpdateInfoService, providedIn: "root" });
    return UpdateInfoService;
}());
export { UpdateInfoService };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    UpdateInfoService.prototype.hasUpdateSubject$;
    /** @type {?} */
    UpdateInfoService.prototype.hasUpdate$;
    /**
     * @type {?}
     * @protected
     */
    UpdateInfoService.prototype.installPromptEventSubject$;
    /**
     * @type {?}
     * @protected
     */
    UpdateInfoService.prototype.installPromptEvent$;
    /** @type {?} */
    UpdateInfoService.prototype.hasInstallOption$;
    /**
     * @type {?}
     * @protected
     */
    UpdateInfoService.prototype.swUpdate;
    /**
     * @type {?}
     * @protected
     */
    UpdateInfoService.prototype.logService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXBkYXRlLWluZm8uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvdXBkYXRlL3VwZGF0ZS1pbmZvLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3JELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDdkMsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQ25ELE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUN0RCxPQUFPLEVBQUUsR0FBRyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7Ozs7QUFFckM7SUFXRSwyQkFDd0IsUUFBa0IsRUFDOUIsVUFBc0I7UUFGbEMsaUJBb0JDO1FBbkJ1QixhQUFRLEdBQVIsUUFBUSxDQUFVO1FBQzlCLGVBQVUsR0FBVixVQUFVLENBQVk7UUFWeEIsc0JBQWlCLEdBQUcsSUFBSSxlQUFlLENBQVUsS0FBSyxDQUFDLENBQUM7UUFDM0QsZUFBVSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUVoRCwrQkFBMEIsR0FBRyxJQUFJLGVBQWUsQ0FBTSxJQUFJLENBQUMsQ0FBQztRQUM1RCx3QkFBbUIsR0FBRyxJQUFJLENBQUMsMEJBQTBCLENBQUMsWUFBWSxFQUFFLENBQUM7UUFFeEUsc0JBQWlCLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxHQUFHOzs7O1FBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLElBQUksSUFBSSxFQUFULENBQVMsRUFBQyxDQUFDLENBQUM7UUFRNUUsTUFBTSxDQUFDLGdCQUFnQixDQUFDLHFCQUFxQjs7OztRQUFFLFVBQUMsS0FBSztZQUNuRCxLQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyw0QkFBNEIsQ0FBQyxDQUFDO1lBQ3BELDZEQUE2RDtZQUM3RCwwQkFBMEI7WUFHMUIsZ0RBQWdEO1lBQ2hELEtBQUksQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFFNUMsZ0VBQWdFO1lBQ2hFLDBGQUEwRjtZQUMxRixnRUFBZ0U7UUFDbEUsQ0FBQyxFQUFDLENBQUM7SUFFTCxDQUFDOzs7O0lBRU0sZ0RBQW9COzs7SUFBM0I7UUFDRSxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxtQ0FBeUIsQ0FBQyxDQUFDO1FBQ2pELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDcEMsQ0FBQzs7OztJQUdNLG1DQUFPOzs7SUFBZDtRQUFBLGlCQWlCQztRQWhCQyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDOztZQUNsQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsMEJBQTBCLENBQUMsUUFBUSxFQUFFO1FBQ3JFLElBQUksa0JBQWtCLEVBQUU7WUFDdEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsb0JBQW9CLENBQUMsQ0FBQztZQUM1QyxrQkFBa0IsQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUM1Qiw2Q0FBNkM7WUFDN0Msa0JBQWtCLENBQUMsVUFBVSxDQUFDLElBQUk7Ozs7WUFBQyxVQUFDLE1BQU07Z0JBQ3hDLElBQUksTUFBTSxDQUFDLE9BQU8sS0FBSyxVQUFVLEVBQUU7b0JBQ2pDLEtBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLCtCQUErQixDQUFDLENBQUM7aUJBQ3hEO3FCQUFNO29CQUNMLEtBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLGdDQUFnQyxDQUFDLENBQUM7aUJBQ3pEO2dCQUNELHNEQUFzRDtnQkFDdEQsS0FBSSxDQUFDLDBCQUEwQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM3QyxDQUFDLEVBQUMsQ0FBQztTQUNKO0lBQ0gsQ0FBQzs7Z0JBeERGLFVBQVUsU0FBQyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUU7Ozs7Z0JBSnpCLFFBQVEsdUJBZ0JaLFFBQVE7Z0JBZkosVUFBVTs7OzRCQUhuQjtDQWlFQyxBQTNERCxJQTJEQztTQTFEWSxpQkFBaUI7Ozs7OztJQUU1Qiw4Q0FBa0U7O0lBQ2xFLHVDQUEwRDs7Ozs7SUFFMUQsdURBQXNFOzs7OztJQUN0RSxnREFBK0U7O0lBRS9FLDhDQUE4RTs7Ozs7SUFHNUUscUNBQXdDOzs7OztJQUN4Qyx1Q0FBZ0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBPcHRpb25hbCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQmVoYXZpb3JTdWJqZWN0IH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBTd1VwZGF0ZSB9IGZyb20gJ0Bhbmd1bGFyL3NlcnZpY2Utd29ya2VyJztcbmltcG9ydCB7IExvZ1NlcnZpY2UgfSBmcm9tICcuLi91dGlscy9sb2cvbG9nLnNlcnZpY2UnO1xuaW1wb3J0IHsgbWFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuXG5ASW5qZWN0YWJsZSh7IHByb3ZpZGVkSW46ICdyb290JyB9KVxuZXhwb3J0IGNsYXNzIFVwZGF0ZUluZm9TZXJ2aWNlIHtcblxuICBwcm90ZWN0ZWQgaGFzVXBkYXRlU3ViamVjdCQgPSBuZXcgQmVoYXZpb3JTdWJqZWN0PGJvb2xlYW4+KGZhbHNlKTtcbiAgcHVibGljIGhhc1VwZGF0ZSQgPSB0aGlzLmhhc1VwZGF0ZVN1YmplY3QkLmFzT2JzZXJ2YWJsZSgpO1xuXG4gIHByb3RlY3RlZCBpbnN0YWxsUHJvbXB0RXZlbnRTdWJqZWN0JCA9IG5ldyBCZWhhdmlvclN1YmplY3Q8YW55PihudWxsKTtcbiAgcHJvdGVjdGVkIGluc3RhbGxQcm9tcHRFdmVudCQgPSB0aGlzLmluc3RhbGxQcm9tcHRFdmVudFN1YmplY3QkLmFzT2JzZXJ2YWJsZSgpO1xuXG4gIHB1YmxpYyBoYXNJbnN0YWxsT3B0aW9uJCA9IHRoaXMuaW5zdGFsbFByb21wdEV2ZW50JC5waXBlKG1hcChyID0+IHIgIT0gbnVsbCkpO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIEBPcHRpb25hbCgpIHByb3RlY3RlZCBzd1VwZGF0ZTogU3dVcGRhdGUsXG4gICAgcHJvdGVjdGVkIGxvZ1NlcnZpY2U6IExvZ1NlcnZpY2UpIHtcblxuXG5cbiAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcignYmVmb3JlaW5zdGFsbHByb21wdCcsIChldmVudCkgPT4ge1xuICAgICAgdGhpcy5sb2dTZXJ2aWNlLmRlYnVnKCdiZWZvcmVpbnN0YWxscHJvbXB0IGZpcmVkIScpO1xuICAgICAgLy8gUHJldmVudCBDaHJvbWUgPD0gNjcgZnJvbSBhdXRvbWF0aWNhbGx5IHNob3dpbmcgdGhlIHByb21wdFxuICAgICAgLy8gZXZlbnQucHJldmVudERlZmF1bHQoKTtcblxuXG4gICAgICAvLyBTdGFzaCB0aGUgZXZlbnQgc28gaXQgY2FuIGJlIHRyaWdnZXJlZCBsYXRlci5cbiAgICAgIHRoaXMuaW5zdGFsbFByb21wdEV2ZW50U3ViamVjdCQubmV4dChldmVudCk7XG5cbiAgICAgIC8vIFVwZGF0ZSB0aGUgaW5zdGFsbCBVSSB0byBub3RpZnkgdGhlIHVzZXIgYXBwIGNhbiBiZSBpbnN0YWxsZWRcbiAgICAgIC8vIChkb2luZyB0aGF0IGJ5IHRoZSBidG5JbnN0YWxsIHRoYXQgaXMgZGlzcGxheWVkIGlmIHRoZXJlIGEgaW5zdGFsbFByb21wdEV2ZW50IGluc3RhbmNlKVxuICAgICAgLy8oPGFueT5kb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjYnRuSW5zdGFsbCcpKS5kaXNhYmxlZCA9IGZhbHNlO1xuICAgIH0pO1xuXG4gIH1cblxuICBwdWJsaWMgc2V0VXBkYXRlQXNBdmFpbGFibGUoKSB7XG4gICAgdGhpcy5sb2dTZXJ2aWNlLmRlYnVnKGBBdHVhbGl6YcOnw6NvIGVuY29udHJhZGEhYCk7XG4gICAgdGhpcy5oYXNVcGRhdGVTdWJqZWN0JC5uZXh0KHRydWUpO1xuICB9XG5cblxuICBwdWJsaWMgaW5zdGFsbCgpIHtcbiAgICB0aGlzLmxvZ1NlcnZpY2UuZGVidWcoJ2luc3RhbGwgZmlyZWQhJyk7XG4gICAgY29uc3QgaW5zdGFsbFByb21wdEV2ZW50ID0gdGhpcy5pbnN0YWxsUHJvbXB0RXZlbnRTdWJqZWN0JC5nZXRWYWx1ZSgpO1xuICAgIGlmIChpbnN0YWxsUHJvbXB0RXZlbnQpIHtcbiAgICAgIHRoaXMubG9nU2VydmljZS5kZWJ1ZygnaW5zdGFsbFByb21wdEV2ZW50Jyk7XG4gICAgICBpbnN0YWxsUHJvbXB0RXZlbnQucHJvbXB0KCk7XG4gICAgICAvLyBXYWl0IGZvciB0aGUgdXNlciB0byByZXNwb25kIHRvIHRoZSBwcm9tcHRcbiAgICAgIGluc3RhbGxQcm9tcHRFdmVudC51c2VyQ2hvaWNlLnRoZW4oKGNob2ljZSkgPT4ge1xuICAgICAgICBpZiAoY2hvaWNlLm91dGNvbWUgPT09ICdhY2NlcHRlZCcpIHtcbiAgICAgICAgICB0aGlzLmxvZ1NlcnZpY2UuZGVidWcoJ1VzZXIgYWNjZXB0ZWQgdGhlIEEySFMgcHJvbXB0Jyk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgdGhpcy5sb2dTZXJ2aWNlLmRlYnVnKCdVc2VyIGRpc21pc3NlZCB0aGUgQTJIUyBwcm9tcHQnKTtcbiAgICAgICAgfVxuICAgICAgICAvLyBDbGVhciB0aGUgc2F2ZWQgcHJvbXB0IHNpbmNlIGl0IGNhbid0IGJlIHVzZWQgYWdhaW5cbiAgICAgICAgdGhpcy5pbnN0YWxsUHJvbXB0RXZlbnRTdWJqZWN0JC5uZXh0KG51bGwpO1xuICAgICAgfSk7XG4gICAgfVxuICB9XG5cblxufVxuIl19