/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ChangeDetectionStrategy } from '@angular/core';
import { UpdateInfoService } from './update-info.service';
var UpdateComponent = /** @class */ (function () {
    function UpdateComponent(updateInfoService) {
        this.updateInfoService = updateInfoService;
        this.hasUpdate$ = this.updateInfoService.hasUpdate$;
    }
    UpdateComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-update',
                    template: "\n  <mat-progress-spinner [color]=\"'primary'\" [mode]=\"'indeterminate'\"></mat-progress-spinner>\n  <div *ngIf=\"!(hasUpdate$ | async);else hasUpdate\">\n    Carregando...\n  </div>\n  <ng-template #hasUpdate>\n    Atualizando...\n  </ng-template>\n  ",
                    changeDetection: ChangeDetectionStrategy.OnPush
                }] }
    ];
    /** @nocollapse */
    UpdateComponent.ctorParameters = function () { return [
        { type: UpdateInfoService }
    ]; };
    return UpdateComponent;
}());
export { UpdateComponent };
if (false) {
    /** @type {?} */
    UpdateComponent.prototype.hasUpdate$;
    /**
     * @type {?}
     * @protected
     */
    UpdateComponent.prototype.updateInfoService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXBkYXRlLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvdXBkYXRlL3VwZGF0ZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsdUJBQXVCLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0UsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFFMUQ7SUFrQkUseUJBQXNCLGlCQUFvQztRQUFwQyxzQkFBaUIsR0FBakIsaUJBQWlCLENBQW1CO1FBRm5ELGVBQVUsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDO0lBRVEsQ0FBQzs7Z0JBbEJoRSxTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLFlBQVk7b0JBQ3RCLFFBQVEsRUFBRSwrUEFRVDtvQkFFRCxlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtpQkFDaEQ7Ozs7Z0JBZlEsaUJBQWlCOztJQXNCMUIsc0JBQUM7Q0FBQSxBQXBCRCxJQW9CQztTQU5ZLGVBQWU7OztJQUUxQixxQ0FBc0Q7Ozs7O0lBRTFDLDRDQUE4QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgVXBkYXRlSW5mb1NlcnZpY2UgfSBmcm9tICcuL3VwZGF0ZS1pbmZvLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdhcHAtdXBkYXRlJyxcbiAgdGVtcGxhdGU6IGBcbiAgPG1hdC1wcm9ncmVzcy1zcGlubmVyIFtjb2xvcl09XCIncHJpbWFyeSdcIiBbbW9kZV09XCInaW5kZXRlcm1pbmF0ZSdcIj48L21hdC1wcm9ncmVzcy1zcGlubmVyPlxuICA8ZGl2ICpuZ0lmPVwiIShoYXNVcGRhdGUkIHwgYXN5bmMpO2Vsc2UgaGFzVXBkYXRlXCI+XG4gICAgQ2FycmVnYW5kby4uLlxuICA8L2Rpdj5cbiAgPG5nLXRlbXBsYXRlICNoYXNVcGRhdGU+XG4gICAgQXR1YWxpemFuZG8uLi5cbiAgPC9uZy10ZW1wbGF0ZT5cbiAgYCxcbiAgc3R5bGVzOiBbXSxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2hcbn0pXG5leHBvcnQgY2xhc3MgVXBkYXRlQ29tcG9uZW50IHtcblxuICBwdWJsaWMgaGFzVXBkYXRlJCA9IHRoaXMudXBkYXRlSW5mb1NlcnZpY2UuaGFzVXBkYXRlJDtcblxuICBjb25zdHJ1Y3Rvcihwcm90ZWN0ZWQgdXBkYXRlSW5mb1NlcnZpY2U6IFVwZGF0ZUluZm9TZXJ2aWNlKSB7IH1cblxufVxuIl19