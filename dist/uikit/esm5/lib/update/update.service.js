/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, Optional, ApplicationRef, NgZone } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { MatDialog } from '@angular/material';
import { UpdateComponent } from './update.component';
import { first, concat } from 'rxjs/operators';
import { interval, Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { UpdateInfoService } from './update-info.service';
import { LogService } from '../utils/log/log.service';
import { resetStores } from '@datorama/akita';
import { ToastService } from '../layout/toast/toast.service';
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/dialog";
import * as i2 from "../layout/toast/toast.service";
import * as i3 from "@angular/router";
import * as i4 from "@angular/service-worker";
import * as i5 from "../utils/log/log.service";
import * as i6 from "./update-info.service";
var UpdateService = /** @class */ (function () {
    function UpdateService(matDialog, toastService, appRef, ngZone, router, swUpdate, logService, updateInfoService) {
        var _this = this;
        this.matDialog = matDialog;
        this.toastService = toastService;
        this.appRef = appRef;
        this.ngZone = ngZone;
        this.router = router;
        this.swUpdate = swUpdate;
        this.logService = logService;
        this.updateInfoService = updateInfoService;
        this.primeiroAcesso = true;
        this.hasUpdate = false;
        this.updateSubscription = Subscription.EMPTY;
        this.matDialogRef = null;
        if (this.swUpdate && this.swUpdate.isEnabled) {
            navigator.serviceWorker.getRegistrations().then((/**
             * @param {?} registrations
             * @return {?}
             */
            function (registrations) {
                /** @type {?} */
                var possuiSwRegistrado = registrations.length > 0;
                if (possuiSwRegistrado) {
                    // Mostra dialogo para carregando atualizações
                    _this.matDialogRef = _this.matDialog.open(UpdateComponent, (/** @type {?} */ ({ disableClose: true, hasBackdrop: true })));
                }
            }));
            this.updateSubscription = this.updateInfoService.hasUpdate$.subscribe((/**
             * @param {?} hasUpdate
             * @return {?}
             */
            function (hasUpdate) { return _this.hasUpdate = hasUpdate; }));
            this.swUpdate.available.subscribe((/**
             * @param {?} event
             * @return {?}
             */
            function (event) {
                _this.updateInfoService.setUpdateAsAvailable();
                _this.logService.info("Current version is: " + JSON.stringify(event.current));
                _this.logService.info("Available version is: " + JSON.stringify(event.available));
                if (_this.primeiroAcesso) {
                    _this.update();
                }
                else {
                    if (_this.matDialogRef) {
                        _this.matDialogRef.close();
                    }
                }
            }));
            this.swUpdate.activated.subscribe((/**
             * @param {?} event
             * @return {?}
             */
            function (event) {
                _this.logService.info("Old version was: " + JSON.stringify(event.previous));
                _this.logService.info("New version is: " + JSON.stringify(event.current));
                localStorage.setItem('updated', 'true');
                _this.logService.debug("Atualizando...");
                try {
                    resetStores();
                }
                catch (error) {
                    _this.logService.error(error);
                }
                document.location.reload(true);
            }));
            /** @type {?} */
            var appIsStable$ = appRef.isStable.pipe(first((/**
             * @param {?} isStable
             * @return {?}
             */
            function (isStable) { return isStable === true; })));
            /** @type {?} */
            var everyTime$ = interval(1 * 60 * 5000);
            /** @type {?} */
            var everyTimeOnceAppIsStable$ = appIsStable$.pipe(concat(everyTime$));
            everyTimeOnceAppIsStable$.subscribe((/**
             * @return {?}
             */
            function () { return _this.checkUpdate(); }));
            this.checkUpdate();
        }
        if (localStorage.getItem('updated') === 'true') {
            this.toastService.success('Atualização realizada com sucesso!');
            localStorage.removeItem('updated');
        }
    }
    /**
     * @return {?}
     */
    UpdateService.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        if (this.updateSubscription && this.updateSubscription.closed) {
            this.updateSubscription.unsubscribe();
        }
    };
    /**
     * @return {?}
     */
    UpdateService.prototype.update = /**
     * @return {?}
     */
    function () {
        if (this.matDialogRef == null) {
            this.matDialogRef = this.matDialog.open(UpdateComponent, (/** @type {?} */ ({ disableClose: true, hasBackdrop: true })));
        }
        this.swUpdate.activateUpdate();
    };
    /**
     * @private
     * @return {?}
     */
    UpdateService.prototype.checkUpdate = /**
     * @private
     * @return {?}
     */
    function () {
        var _this = this;
        this.logService.debug("Verificando por atualiza\u00E7\u00E3o...");
        this.swUpdate.checkForUpdate()
            .then((/**
         * @return {?}
         */
        function () {
            _this.logService.debug('checkForUpdate - then');
            _this.ngZone.run((/**
             * @return {?}
             */
            function () {
                _this.primeiroAcesso = false;
                if (!_this.hasUpdate) {
                    _this.logService.debug("this.matDialogRef");
                    if (_this.matDialogRef) {
                        _this.logService.debug("this.matDialogRef.close()");
                        _this.matDialogRef.close();
                    }
                }
            }));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            _this.logService.debug('checkForUpdate - error');
            _this.logService.debug(error);
            _this.ngZone.run((/**
             * @return {?}
             */
            function () {
                /** @type {?} */
                var erro = null;
                if (error.message) {
                    erro = error.message;
                }
                else {
                    erro = error;
                }
                _this.logService.error(erro);
                if (_this.matDialogRef) {
                    _this.matDialogRef.close();
                }
            }));
        }));
    };
    UpdateService.decorators = [
        { type: Injectable, args: [{ providedIn: 'root' },] }
    ];
    /** @nocollapse */
    UpdateService.ctorParameters = function () { return [
        { type: MatDialog },
        { type: ToastService },
        { type: ApplicationRef },
        { type: NgZone },
        { type: Router },
        { type: SwUpdate, decorators: [{ type: Optional }] },
        { type: LogService, decorators: [{ type: Optional }] },
        { type: UpdateInfoService }
    ]; };
    /** @nocollapse */ UpdateService.ngInjectableDef = i0.defineInjectable({ factory: function UpdateService_Factory() { return new UpdateService(i0.inject(i1.MatDialog), i0.inject(i2.ToastService), i0.inject(i0.ApplicationRef), i0.inject(i0.NgZone), i0.inject(i3.Router), i0.inject(i4.SwUpdate, 8), i0.inject(i5.LogService, 8), i0.inject(i6.UpdateInfoService)); }, token: UpdateService, providedIn: "root" });
    return UpdateService;
}());
export { UpdateService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    UpdateService.prototype.primeiroAcesso;
    /**
     * @type {?}
     * @private
     */
    UpdateService.prototype.hasUpdate;
    /**
     * @type {?}
     * @private
     */
    UpdateService.prototype.updateSubscription;
    /**
     * @type {?}
     * @private
     */
    UpdateService.prototype.matDialogRef;
    /**
     * @type {?}
     * @protected
     */
    UpdateService.prototype.matDialog;
    /**
     * @type {?}
     * @protected
     */
    UpdateService.prototype.toastService;
    /**
     * @type {?}
     * @protected
     */
    UpdateService.prototype.appRef;
    /**
     * @type {?}
     * @protected
     */
    UpdateService.prototype.ngZone;
    /**
     * @type {?}
     * @protected
     */
    UpdateService.prototype.router;
    /**
     * @type {?}
     * @protected
     */
    UpdateService.prototype.swUpdate;
    /**
     * @type {?}
     * @protected
     */
    UpdateService.prototype.logService;
    /**
     * @type {?}
     * @protected
     */
    UpdateService.prototype.updateInfoService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXBkYXRlLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL3VwZGF0ZS91cGRhdGUuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsY0FBYyxFQUE0QixNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDdkcsT0FBTyxFQUFFLFFBQVEsRUFBOEMsTUFBTSx5QkFBeUIsQ0FBQztBQUMvRixPQUFPLEVBQUUsU0FBUyxFQUFpQyxNQUFNLG1CQUFtQixDQUFDO0FBQzdFLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUNyRCxPQUFPLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBVSxNQUFNLGdCQUFnQixDQUFDO0FBQ3ZELE9BQU8sRUFBaUIsUUFBUSxFQUFFLFlBQVksRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUM3RCxPQUFPLEVBQUUsTUFBTSxFQUFrQyxNQUFNLGlCQUFpQixDQUFDO0FBQ3pFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQzFELE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUN0RCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDOUMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLCtCQUErQixDQUFDOzs7Ozs7OztBQUU3RDtJQVNFLHVCQUNZLFNBQW9CLEVBQ3BCLFlBQTBCLEVBQzFCLE1BQXNCLEVBQ3RCLE1BQWMsRUFDZCxNQUFjLEVBQ0YsUUFBa0IsRUFDbEIsVUFBc0IsRUFDbEMsaUJBQW9DO1FBUmhELGlCQWtFQztRQWpFVyxjQUFTLEdBQVQsU0FBUyxDQUFXO1FBQ3BCLGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBQzFCLFdBQU0sR0FBTixNQUFNLENBQWdCO1FBQ3RCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ0YsYUFBUSxHQUFSLFFBQVEsQ0FBVTtRQUNsQixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ2xDLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBbUI7UUFkeEMsbUJBQWMsR0FBRyxJQUFJLENBQUM7UUFDdEIsY0FBUyxHQUFHLEtBQUssQ0FBQztRQUNsQix1QkFBa0IsR0FBRyxZQUFZLENBQUMsS0FBSyxDQUFDO1FBRXhDLGlCQUFZLEdBQWtDLElBQUksQ0FBQztRQWF6RCxJQUFJLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUU7WUFFNUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLElBQUk7Ozs7WUFBQyxVQUFBLGFBQWE7O29CQUNyRCxrQkFBa0IsR0FBRyxhQUFhLENBQUMsTUFBTSxHQUFHLENBQUM7Z0JBQ25ELElBQUksa0JBQWtCLEVBQUU7b0JBQ3RCLDhDQUE4QztvQkFDOUMsS0FBSSxDQUFDLFlBQVksR0FBRyxLQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQ3JELG1CQUFBLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxXQUFXLEVBQUUsSUFBSSxFQUFFLEVBQW9DLENBQUMsQ0FBQztpQkFDbEY7WUFDSCxDQUFDLEVBQUMsQ0FBQztZQUVILElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDLFNBQVM7Ozs7WUFBQyxVQUFBLFNBQVMsSUFBSSxPQUFBLEtBQUksQ0FBQyxTQUFTLEdBQUcsU0FBUyxFQUExQixDQUEwQixFQUFDLENBQUM7WUFFL0csSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsU0FBUzs7OztZQUFDLFVBQUMsS0FBMkI7Z0JBQzVELEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO2dCQUM5QyxLQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyx5QkFBdUIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFHLENBQUMsQ0FBQztnQkFDN0UsS0FBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsMkJBQXlCLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBRyxDQUFDLENBQUM7Z0JBRWpGLElBQUksS0FBSSxDQUFDLGNBQWMsRUFBRTtvQkFDdkIsS0FBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO2lCQUNmO3FCQUFNO29CQUNMLElBQUksS0FBSSxDQUFDLFlBQVksRUFBRTt3QkFDckIsS0FBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQztxQkFDM0I7aUJBQ0Y7WUFDSCxDQUFDLEVBQUMsQ0FBQztZQUVILElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLFNBQVM7Ozs7WUFBQyxVQUFDLEtBQTJCO2dCQUM1RCxLQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxzQkFBb0IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFHLENBQUMsQ0FBQztnQkFDM0UsS0FBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMscUJBQW1CLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBRyxDQUFDLENBQUM7Z0JBRXpFLFlBQVksQ0FBQyxPQUFPLENBQUMsU0FBUyxFQUFFLE1BQU0sQ0FBQyxDQUFDO2dCQUN4QyxLQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO2dCQUV4QyxJQUFJO29CQUNGLFdBQVcsRUFBRSxDQUFDO2lCQUNmO2dCQUFDLE9BQU8sS0FBSyxFQUFFO29CQUNkLEtBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUM5QjtnQkFDRCxRQUFRLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNqQyxDQUFDLEVBQUMsQ0FBQzs7Z0JBRUcsWUFBWSxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUs7Ozs7WUFBQyxVQUFBLFFBQVEsSUFBSSxPQUFBLFFBQVEsS0FBSyxJQUFJLEVBQWpCLENBQWlCLEVBQUMsQ0FBQzs7Z0JBQ3pFLFVBQVUsR0FBRyxRQUFRLENBQUMsQ0FBQyxHQUFHLEVBQUUsR0FBRyxJQUFJLENBQUM7O2dCQUNwQyx5QkFBeUIsR0FBRyxZQUFZLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUV2RSx5QkFBeUIsQ0FBQyxTQUFTOzs7WUFBQyxjQUFNLE9BQUEsS0FBSSxDQUFDLFdBQVcsRUFBRSxFQUFsQixDQUFrQixFQUFDLENBQUM7WUFFOUQsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQ3BCO1FBRUQsSUFBSSxZQUFZLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxLQUFLLE1BQU0sRUFBRTtZQUM5QyxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxvQ0FBb0MsQ0FBQyxDQUFDO1lBQ2hFLFlBQVksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLENBQUM7U0FDcEM7SUFDSCxDQUFDOzs7O0lBRUQsbUNBQVc7OztJQUFYO1FBQ0UsSUFBSSxJQUFJLENBQUMsa0JBQWtCLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLE1BQU0sRUFBRTtZQUM3RCxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDdkM7SUFDSCxDQUFDOzs7O0lBRU0sOEJBQU07OztJQUFiO1FBQ0UsSUFBSSxJQUFJLENBQUMsWUFBWSxJQUFJLElBQUksRUFBRTtZQUM3QixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFDckQsbUJBQUEsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUUsRUFBb0MsQ0FBQyxDQUFDO1NBQ2xGO1FBRUQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxjQUFjLEVBQUUsQ0FBQztJQUNqQyxDQUFDOzs7OztJQUVPLG1DQUFXOzs7O0lBQW5CO1FBQUEsaUJBK0JDO1FBOUJDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLDBDQUFnQyxDQUFDLENBQUM7UUFDeEQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxjQUFjLEVBQUU7YUFDM0IsSUFBSTs7O1FBQUM7WUFDSixLQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDO1lBQy9DLEtBQUksQ0FBQyxNQUFNLENBQUMsR0FBRzs7O1lBQUM7Z0JBQ2QsS0FBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7Z0JBQzVCLElBQUksQ0FBQyxLQUFJLENBQUMsU0FBUyxFQUFFO29CQUNuQixLQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO29CQUMzQyxJQUFJLEtBQUksQ0FBQyxZQUFZLEVBQUU7d0JBQ3JCLEtBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLDJCQUEyQixDQUFDLENBQUM7d0JBQ25ELEtBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7cUJBQzNCO2lCQUNGO1lBQ0gsQ0FBQyxFQUFDLENBQUM7UUFDTCxDQUFDLEVBQUMsQ0FBQyxLQUFLOzs7O1FBQUMsVUFBQSxLQUFLO1lBQ1osS0FBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsd0JBQXdCLENBQUMsQ0FBQztZQUNoRCxLQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUM3QixLQUFJLENBQUMsTUFBTSxDQUFDLEdBQUc7OztZQUFDOztvQkFDVixJQUFJLEdBQUcsSUFBSTtnQkFDZixJQUFJLEtBQUssQ0FBQyxPQUFPLEVBQUU7b0JBQ2pCLElBQUksR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDO2lCQUN0QjtxQkFBTTtvQkFDTCxJQUFJLEdBQUcsS0FBSyxDQUFDO2lCQUNkO2dCQUNELEtBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUM1QixJQUFJLEtBQUksQ0FBQyxZQUFZLEVBQUU7b0JBQ3JCLEtBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7aUJBQzNCO1lBQ0gsQ0FBQyxFQUFDLENBQUM7UUFDTCxDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7O2dCQTNIRixVQUFVLFNBQUMsRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFOzs7O2dCQVZ6QixTQUFTO2dCQVFULFlBQVk7Z0JBVlUsY0FBYztnQkFBNEIsTUFBTTtnQkFNdEUsTUFBTTtnQkFMTixRQUFRLHVCQTBCWixRQUFRO2dCQW5CSixVQUFVLHVCQW9CZCxRQUFRO2dCQXJCSixpQkFBaUI7Ozt3QkFQMUI7Q0F3SUMsQUE1SEQsSUE0SEM7U0EzSFksYUFBYTs7Ozs7O0lBRXhCLHVDQUE4Qjs7Ozs7SUFDOUIsa0NBQTBCOzs7OztJQUMxQiwyQ0FBZ0Q7Ozs7O0lBRWhELHFDQUEyRDs7Ozs7SUFHekQsa0NBQThCOzs7OztJQUM5QixxQ0FBb0M7Ozs7O0lBQ3BDLCtCQUFnQzs7Ozs7SUFDaEMsK0JBQXdCOzs7OztJQUN4QiwrQkFBd0I7Ozs7O0lBQ3hCLGlDQUF3Qzs7Ozs7SUFDeEMsbUNBQTRDOzs7OztJQUM1QywwQ0FBOEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBPcHRpb25hbCwgQXBwbGljYXRpb25SZWYsIE9uRGVzdHJveSwgQWZ0ZXJWaWV3SW5pdCwgTmdab25lIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBTd1VwZGF0ZSwgVXBkYXRlQXZhaWxhYmxlRXZlbnQsIFVwZGF0ZUFjdGl2YXRlZEV2ZW50IH0gZnJvbSAnQGFuZ3VsYXIvc2VydmljZS13b3JrZXInO1xuaW1wb3J0IHsgTWF0RGlhbG9nLCBNYXREaWFsb2dDb25maWcsIE1hdERpYWxvZ1JlZiB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcbmltcG9ydCB7IFVwZGF0ZUNvbXBvbmVudCB9IGZyb20gJy4vdXBkYXRlLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBmaXJzdCwgY29uY2F0LCBmaWx0ZXIgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQgeyBjb21iaW5lTGF0ZXN0LCBpbnRlcnZhbCwgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBSb3V0ZXIsIE5hdmlnYXRpb25TdGFydCwgTmF2aWdhdGlvbkVuZCB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQgeyBVcGRhdGVJbmZvU2VydmljZSB9IGZyb20gJy4vdXBkYXRlLWluZm8uc2VydmljZSc7XG5pbXBvcnQgeyBMb2dTZXJ2aWNlIH0gZnJvbSAnLi4vdXRpbHMvbG9nL2xvZy5zZXJ2aWNlJztcbmltcG9ydCB7IHJlc2V0U3RvcmVzIH0gZnJvbSAnQGRhdG9yYW1hL2FraXRhJztcbmltcG9ydCB7IFRvYXN0U2VydmljZSB9IGZyb20gJy4uL2xheW91dC90b2FzdC90b2FzdC5zZXJ2aWNlJztcblxuQEluamVjdGFibGUoeyBwcm92aWRlZEluOiAncm9vdCcgfSlcbmV4cG9ydCBjbGFzcyBVcGRhdGVTZXJ2aWNlIGltcGxlbWVudHMgT25EZXN0cm95IHtcblxuICBwcml2YXRlIHByaW1laXJvQWNlc3NvID0gdHJ1ZTtcbiAgcHJpdmF0ZSBoYXNVcGRhdGUgPSBmYWxzZTtcbiAgcHJpdmF0ZSB1cGRhdGVTdWJzY3JpcHRpb24gPSBTdWJzY3JpcHRpb24uRU1QVFk7XG5cbiAgcHJpdmF0ZSBtYXREaWFsb2dSZWY6IE1hdERpYWxvZ1JlZjxVcGRhdGVDb21wb25lbnQ+ID0gbnVsbDtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcm90ZWN0ZWQgbWF0RGlhbG9nOiBNYXREaWFsb2csXG4gICAgcHJvdGVjdGVkIHRvYXN0U2VydmljZTogVG9hc3RTZXJ2aWNlLFxuICAgIHByb3RlY3RlZCBhcHBSZWY6IEFwcGxpY2F0aW9uUmVmLFxuICAgIHByb3RlY3RlZCBuZ1pvbmU6IE5nWm9uZSxcbiAgICBwcm90ZWN0ZWQgcm91dGVyOiBSb3V0ZXIsXG4gICAgQE9wdGlvbmFsKCkgcHJvdGVjdGVkIHN3VXBkYXRlOiBTd1VwZGF0ZSxcbiAgICBAT3B0aW9uYWwoKSBwcm90ZWN0ZWQgbG9nU2VydmljZTogTG9nU2VydmljZSxcbiAgICBwcm90ZWN0ZWQgdXBkYXRlSW5mb1NlcnZpY2U6IFVwZGF0ZUluZm9TZXJ2aWNlXG4gICkge1xuXG4gICAgaWYgKHRoaXMuc3dVcGRhdGUgJiYgdGhpcy5zd1VwZGF0ZS5pc0VuYWJsZWQpIHtcblxuICAgICAgbmF2aWdhdG9yLnNlcnZpY2VXb3JrZXIuZ2V0UmVnaXN0cmF0aW9ucygpLnRoZW4ocmVnaXN0cmF0aW9ucyA9PiB7XG4gICAgICAgIGNvbnN0IHBvc3N1aVN3UmVnaXN0cmFkbyA9IHJlZ2lzdHJhdGlvbnMubGVuZ3RoID4gMDtcbiAgICAgICAgaWYgKHBvc3N1aVN3UmVnaXN0cmFkbykge1xuICAgICAgICAgIC8vIE1vc3RyYSBkaWFsb2dvIHBhcmEgY2FycmVnYW5kbyBhdHVhbGl6YcOnw7Vlc1xuICAgICAgICAgIHRoaXMubWF0RGlhbG9nUmVmID0gdGhpcy5tYXREaWFsb2cub3BlbihVcGRhdGVDb21wb25lbnQsXG4gICAgICAgICAgICB7IGRpc2FibGVDbG9zZTogdHJ1ZSwgaGFzQmFja2Ryb3A6IHRydWUgfSBhcyBNYXREaWFsb2dDb25maWc8VXBkYXRlQ29tcG9uZW50Pik7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuXG4gICAgICB0aGlzLnVwZGF0ZVN1YnNjcmlwdGlvbiA9IHRoaXMudXBkYXRlSW5mb1NlcnZpY2UuaGFzVXBkYXRlJC5zdWJzY3JpYmUoaGFzVXBkYXRlID0+IHRoaXMuaGFzVXBkYXRlID0gaGFzVXBkYXRlKTtcblxuICAgICAgdGhpcy5zd1VwZGF0ZS5hdmFpbGFibGUuc3Vic2NyaWJlKChldmVudDogVXBkYXRlQXZhaWxhYmxlRXZlbnQpID0+IHtcbiAgICAgICAgdGhpcy51cGRhdGVJbmZvU2VydmljZS5zZXRVcGRhdGVBc0F2YWlsYWJsZSgpO1xuICAgICAgICB0aGlzLmxvZ1NlcnZpY2UuaW5mbyhgQ3VycmVudCB2ZXJzaW9uIGlzOiAke0pTT04uc3RyaW5naWZ5KGV2ZW50LmN1cnJlbnQpfWApO1xuICAgICAgICB0aGlzLmxvZ1NlcnZpY2UuaW5mbyhgQXZhaWxhYmxlIHZlcnNpb24gaXM6ICR7SlNPTi5zdHJpbmdpZnkoZXZlbnQuYXZhaWxhYmxlKX1gKTtcblxuICAgICAgICBpZiAodGhpcy5wcmltZWlyb0FjZXNzbykge1xuICAgICAgICAgIHRoaXMudXBkYXRlKCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgaWYgKHRoaXMubWF0RGlhbG9nUmVmKSB7XG4gICAgICAgICAgICB0aGlzLm1hdERpYWxvZ1JlZi5jbG9zZSgpO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfSk7XG5cbiAgICAgIHRoaXMuc3dVcGRhdGUuYWN0aXZhdGVkLnN1YnNjcmliZSgoZXZlbnQ6IFVwZGF0ZUFjdGl2YXRlZEV2ZW50KSA9PiB7XG4gICAgICAgIHRoaXMubG9nU2VydmljZS5pbmZvKGBPbGQgdmVyc2lvbiB3YXM6ICR7SlNPTi5zdHJpbmdpZnkoZXZlbnQucHJldmlvdXMpfWApO1xuICAgICAgICB0aGlzLmxvZ1NlcnZpY2UuaW5mbyhgTmV3IHZlcnNpb24gaXM6ICR7SlNPTi5zdHJpbmdpZnkoZXZlbnQuY3VycmVudCl9YCk7XG5cbiAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oJ3VwZGF0ZWQnLCAndHJ1ZScpO1xuICAgICAgICB0aGlzLmxvZ1NlcnZpY2UuZGVidWcoYEF0dWFsaXphbmRvLi4uYCk7XG5cbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICByZXNldFN0b3JlcygpO1xuICAgICAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgICAgIHRoaXMubG9nU2VydmljZS5lcnJvcihlcnJvcik7XG4gICAgICAgIH1cbiAgICAgICAgZG9jdW1lbnQubG9jYXRpb24ucmVsb2FkKHRydWUpO1xuICAgICAgfSk7XG5cbiAgICAgIGNvbnN0IGFwcElzU3RhYmxlJCA9IGFwcFJlZi5pc1N0YWJsZS5waXBlKGZpcnN0KGlzU3RhYmxlID0+IGlzU3RhYmxlID09PSB0cnVlKSk7XG4gICAgICBjb25zdCBldmVyeVRpbWUkID0gaW50ZXJ2YWwoMSAqIDYwICogNTAwMCk7XG4gICAgICBjb25zdCBldmVyeVRpbWVPbmNlQXBwSXNTdGFibGUkID0gYXBwSXNTdGFibGUkLnBpcGUoY29uY2F0KGV2ZXJ5VGltZSQpKTtcblxuICAgICAgZXZlcnlUaW1lT25jZUFwcElzU3RhYmxlJC5zdWJzY3JpYmUoKCkgPT4gdGhpcy5jaGVja1VwZGF0ZSgpKTtcblxuICAgICAgdGhpcy5jaGVja1VwZGF0ZSgpO1xuICAgIH1cblxuICAgIGlmIChsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgndXBkYXRlZCcpID09PSAndHJ1ZScpIHtcbiAgICAgIHRoaXMudG9hc3RTZXJ2aWNlLnN1Y2Nlc3MoJ0F0dWFsaXphw6fDo28gcmVhbGl6YWRhIGNvbSBzdWNlc3NvIScpO1xuICAgICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oJ3VwZGF0ZWQnKTtcbiAgICB9XG4gIH1cblxuICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcbiAgICBpZiAodGhpcy51cGRhdGVTdWJzY3JpcHRpb24gJiYgdGhpcy51cGRhdGVTdWJzY3JpcHRpb24uY2xvc2VkKSB7XG4gICAgICB0aGlzLnVwZGF0ZVN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyB1cGRhdGUoKSB7XG4gICAgaWYgKHRoaXMubWF0RGlhbG9nUmVmID09IG51bGwpIHtcbiAgICAgIHRoaXMubWF0RGlhbG9nUmVmID0gdGhpcy5tYXREaWFsb2cub3BlbihVcGRhdGVDb21wb25lbnQsXG4gICAgICAgIHsgZGlzYWJsZUNsb3NlOiB0cnVlLCBoYXNCYWNrZHJvcDogdHJ1ZSB9IGFzIE1hdERpYWxvZ0NvbmZpZzxVcGRhdGVDb21wb25lbnQ+KTtcbiAgICB9XG5cbiAgICB0aGlzLnN3VXBkYXRlLmFjdGl2YXRlVXBkYXRlKCk7XG4gIH1cblxuICBwcml2YXRlIGNoZWNrVXBkYXRlKCkge1xuICAgIHRoaXMubG9nU2VydmljZS5kZWJ1ZyhgVmVyaWZpY2FuZG8gcG9yIGF0dWFsaXphw6fDo28uLi5gKTtcbiAgICB0aGlzLnN3VXBkYXRlLmNoZWNrRm9yVXBkYXRlKClcbiAgICAgIC50aGVuKCgpID0+IHtcbiAgICAgICAgdGhpcy5sb2dTZXJ2aWNlLmRlYnVnKCdjaGVja0ZvclVwZGF0ZSAtIHRoZW4nKTtcbiAgICAgICAgdGhpcy5uZ1pvbmUucnVuKCgpID0+IHtcbiAgICAgICAgICB0aGlzLnByaW1laXJvQWNlc3NvID0gZmFsc2U7XG4gICAgICAgICAgaWYgKCF0aGlzLmhhc1VwZGF0ZSkge1xuICAgICAgICAgICAgdGhpcy5sb2dTZXJ2aWNlLmRlYnVnKGB0aGlzLm1hdERpYWxvZ1JlZmApO1xuICAgICAgICAgICAgaWYgKHRoaXMubWF0RGlhbG9nUmVmKSB7XG4gICAgICAgICAgICAgIHRoaXMubG9nU2VydmljZS5kZWJ1ZyhgdGhpcy5tYXREaWFsb2dSZWYuY2xvc2UoKWApO1xuICAgICAgICAgICAgICB0aGlzLm1hdERpYWxvZ1JlZi5jbG9zZSgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIHRoaXMubG9nU2VydmljZS5kZWJ1ZygnY2hlY2tGb3JVcGRhdGUgLSBlcnJvcicpO1xuICAgICAgICB0aGlzLmxvZ1NlcnZpY2UuZGVidWcoZXJyb3IpO1xuICAgICAgICB0aGlzLm5nWm9uZS5ydW4oKCkgPT4ge1xuICAgICAgICAgIGxldCBlcnJvID0gbnVsbDtcbiAgICAgICAgICBpZiAoZXJyb3IubWVzc2FnZSkge1xuICAgICAgICAgICAgZXJybyA9IGVycm9yLm1lc3NhZ2U7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGVycm8gPSBlcnJvcjtcbiAgICAgICAgICB9XG4gICAgICAgICAgdGhpcy5sb2dTZXJ2aWNlLmVycm9yKGVycm8pO1xuICAgICAgICAgIGlmICh0aGlzLm1hdERpYWxvZ1JlZikge1xuICAgICAgICAgICAgdGhpcy5tYXREaWFsb2dSZWYuY2xvc2UoKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgfSk7XG4gIH1cbn1cbiJdfQ==