/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatDialogModule, MatProgressSpinnerModule } from '@angular/material';
import { UpdateComponent } from './update.component';
import { UpdateService } from './update.service';
import { UpdateInfoService } from './update-info.service';
var UpdateModule = /** @class */ (function () {
    function UpdateModule(updateService, updateInfoService) {
        this.updateService = updateService;
        this.updateInfoService = updateInfoService;
    }
    UpdateModule.decorators = [
        { type: NgModule, args: [{
                    entryComponents: [UpdateComponent],
                    declarations: [UpdateComponent],
                    imports: [
                        CommonModule,
                        MatDialogModule,
                        MatProgressSpinnerModule
                    ],
                    exports: [UpdateComponent]
                },] }
    ];
    /** @nocollapse */
    UpdateModule.ctorParameters = function () { return [
        { type: UpdateService },
        { type: UpdateInfoService }
    ]; };
    return UpdateModule;
}());
export { UpdateModule };
if (false) {
    /** @type {?} */
    UpdateModule.prototype.updateService;
    /** @type {?} */
    UpdateModule.prototype.updateInfoService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXBkYXRlLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvdXBkYXRlL3VwZGF0ZS5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxlQUFlLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUM5RSxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFDckQsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBQ2pELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBRzFEO0lBV0Usc0JBQW1CLGFBQTRCLEVBQ3RDLGlCQUFvQztRQUQxQixrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUN0QyxzQkFBaUIsR0FBakIsaUJBQWlCLENBQW1CO0lBRTdDLENBQUM7O2dCQWRGLFFBQVEsU0FBQztvQkFDUixlQUFlLEVBQUUsQ0FBQyxlQUFlLENBQUM7b0JBQ2xDLFlBQVksRUFBRSxDQUFDLGVBQWUsQ0FBQztvQkFDL0IsT0FBTyxFQUFFO3dCQUNQLFlBQVk7d0JBQ1osZUFBZTt3QkFDZix3QkFBd0I7cUJBQ3pCO29CQUNELE9BQU8sRUFBRSxDQUFDLGVBQWUsQ0FBQztpQkFDM0I7Ozs7Z0JBYlEsYUFBYTtnQkFDYixpQkFBaUI7O0lBa0IxQixtQkFBQztDQUFBLEFBZkQsSUFlQztTQUxZLFlBQVk7OztJQUNYLHFDQUFtQzs7SUFDN0MseUNBQTJDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQgeyBNYXREaWFsb2dNb2R1bGUsIE1hdFByb2dyZXNzU3Bpbm5lck1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcbmltcG9ydCB7IFVwZGF0ZUNvbXBvbmVudCB9IGZyb20gJy4vdXBkYXRlLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBVcGRhdGVTZXJ2aWNlIH0gZnJvbSAnLi91cGRhdGUuc2VydmljZSc7XG5pbXBvcnQgeyBVcGRhdGVJbmZvU2VydmljZSB9IGZyb20gJy4vdXBkYXRlLWluZm8uc2VydmljZSc7XG5cblxuQE5nTW9kdWxlKHtcbiAgZW50cnlDb21wb25lbnRzOiBbVXBkYXRlQ29tcG9uZW50XSxcbiAgZGVjbGFyYXRpb25zOiBbVXBkYXRlQ29tcG9uZW50XSxcbiAgaW1wb3J0czogW1xuICAgIENvbW1vbk1vZHVsZSxcbiAgICBNYXREaWFsb2dNb2R1bGUsXG4gICAgTWF0UHJvZ3Jlc3NTcGlubmVyTW9kdWxlXG4gIF0sXG4gIGV4cG9ydHM6IFtVcGRhdGVDb21wb25lbnRdXG59KVxuZXhwb3J0IGNsYXNzIFVwZGF0ZU1vZHVsZSB7XG4gIGNvbnN0cnVjdG9yKHB1YmxpYyB1cGRhdGVTZXJ2aWNlOiBVcGRhdGVTZXJ2aWNlLFxuICAgIHB1YmxpYyB1cGRhdGVJbmZvU2VydmljZTogVXBkYXRlSW5mb1NlcnZpY2UpIHtcblxuICB9XG59XG4iXX0=