/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutComponent } from './layout.component';
import { HeaderComponent } from './header/header.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatBadgeModule } from '@angular/material/badge';
import { MatTooltipModule } from '@angular/material/tooltip';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatListModule } from '@angular/material/list';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { NavComponent } from './nav/nav.component';
import { MenuComponent } from './nav/menu/menu.component';
import { FavnavComponent } from './nav/favnav/favnav.component';
import { BreadcrumbComponent } from './nav/breadcrumb/breadcrumb.component';
import { SearchComponent } from './header/search/search.component';
import { NotificationComponent } from './header/notification/notification.component';
import { SysteminfoComponent } from './header/systeminfo/systeminfo.component';
import { AccessibilityComponent } from './header/accessibility/accessibility.component';
import { UserinfoComponent } from './header/userinfo/userinfo.component';
import { MatChipsModule } from '@angular/material/chips';
import { MatSliderModule } from '@angular/material/slider';
import { LayoutService } from './layout.service';
import { FavNavsService } from './nav/favnav/state/favnavs.service';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { CdkStepperModule } from '@angular/cdk/stepper';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatTabsModule } from '@angular/material/tabs';
import { MatSelectModule } from '@angular/material/select';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MenuItemComponent } from './nav/menu/menu-item/menu-item.component';
import { RouterModule } from '@angular/router';
import { HotkeyModule } from 'angular2-hotkeys';
import { MatSnackBarModule, MatTableModule, MatTreeModule } from '@angular/material';
import { NotificacaoComponent } from './header/notification/notificacao/notificacao.component';
import { SimpleDialogComponent } from '../components/simple-dialog/simple-dialog.component';
import { DialogService } from '../components/simple-dialog/dialog.service';
import { IaComponent } from './ia/ia.component';
import { BreadcrumbModule } from './nav/breadcrumb/breadcrumbs/breadcrumb.module';
import { UikitSharedModule } from '../shared/shared.module';
import { ToastModule } from './toast/toast.module';
import { PreviousRouteService } from '../components/previous-route.service';
import { HighlightComponent } from './nav/highlight/highlight.component';
import { MenuSearchComponent } from "./nav/menu-search/menu-search.component";
import { UikitRrippleService } from "../utils/uikit-ripple.service";
var LayoutModule = /** @class */ (function () {
    function LayoutModule(previousRouteService) {
        this.previousRouteService = previousRouteService;
    }
    LayoutModule.decorators = [
        { type: NgModule, args: [{
                    providers: [
                        LayoutService,
                        FavNavsService,
                        DialogService,
                        PreviousRouteService,
                        UikitRrippleService
                    ],
                    declarations: [
                        LayoutComponent,
                        HeaderComponent,
                        NavComponent,
                        MenuComponent,
                        FavnavComponent,
                        SearchComponent,
                        HighlightComponent,
                        NotificationComponent,
                        SysteminfoComponent,
                        AccessibilityComponent,
                        UserinfoComponent,
                        BreadcrumbComponent,
                        MenuItemComponent,
                        NotificacaoComponent,
                        SimpleDialogComponent,
                        BreadcrumbComponent,
                        MenuSearchComponent,
                        IaComponent
                    ],
                    imports: [
                        CommonModule,
                        RouterModule,
                        FormsModule,
                        ReactiveFormsModule,
                        HotkeyModule.forRoot(),
                        MatSidenavModule,
                        MatSnackBarModule,
                        MatToolbarModule,
                        MatButtonModule,
                        MatMenuModule,
                        MatIconModule,
                        MatBadgeModule,
                        MatTooltipModule,
                        DragDropModule,
                        MatExpansionModule,
                        MatListModule,
                        MatAutocompleteModule,
                        MatFormFieldModule,
                        MatInputModule,
                        ScrollingModule,
                        MatChipsModule,
                        MatSliderModule,
                        CdkStepperModule,
                        MatSlideToggleModule,
                        MatTabsModule,
                        MatSelectModule,
                        MatPaginatorModule,
                        MatTableModule,
                        MatTreeModule,
                        BreadcrumbModule,
                        UikitSharedModule,
                        ToastModule,
                    ],
                    exports: [
                        MatIconModule,
                        MatButtonModule,
                        MatSnackBarModule,
                        LayoutComponent,
                        HeaderComponent,
                        NavComponent,
                        MenuComponent,
                        FavnavComponent,
                        MatListModule,
                        SearchComponent,
                        NotificationComponent,
                        SysteminfoComponent,
                        AccessibilityComponent,
                        UserinfoComponent,
                        MenuItemComponent,
                        HotkeyModule,
                        NotificacaoComponent,
                        SimpleDialogComponent,
                        BreadcrumbModule,
                        MenuSearchComponent,
                        ToastModule
                    ],
                    entryComponents: [
                        LayoutComponent,
                        HeaderComponent,
                        NavComponent,
                        MenuComponent,
                        FavnavComponent,
                        SearchComponent,
                        NotificationComponent,
                        SysteminfoComponent,
                        AccessibilityComponent,
                        UserinfoComponent,
                        MenuItemComponent,
                        NotificacaoComponent,
                        SimpleDialogComponent,
                        BreadcrumbComponent,
                        IaComponent
                    ]
                },] }
    ];
    /** @nocollapse */
    LayoutModule.ctorParameters = function () { return [
        { type: PreviousRouteService }
    ]; };
    return LayoutModule;
}());
export { LayoutModule };
if (false) {
    /**
     * @type {?}
     * @private
     */
    LayoutModule.prototype.previousRouteService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGF5b3V0Lm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvbGF5b3V0L2xheW91dC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxRQUFRLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDdkMsT0FBTyxFQUFDLFlBQVksRUFBQyxNQUFNLGlCQUFpQixDQUFDO0FBQzdDLE9BQU8sRUFBQyxXQUFXLEVBQUUsbUJBQW1CLEVBQUMsTUFBTSxnQkFBZ0IsQ0FBQztBQUVoRSxPQUFPLEVBQUMsZUFBZSxFQUFDLE1BQU0sb0JBQW9CLENBQUM7QUFDbkQsT0FBTyxFQUFDLGVBQWUsRUFBQyxNQUFNLDJCQUEyQixDQUFDO0FBQzFELE9BQU8sRUFBQyxnQkFBZ0IsRUFBQyxNQUFNLDJCQUEyQixDQUFDO0FBQzNELE9BQU8sRUFBQyxlQUFlLEVBQUMsTUFBTSwwQkFBMEIsQ0FBQztBQUN6RCxPQUFPLEVBQUMsZ0JBQWdCLEVBQUMsTUFBTSwyQkFBMkIsQ0FBQztBQUMzRCxPQUFPLEVBQUMsYUFBYSxFQUFDLE1BQU0sd0JBQXdCLENBQUM7QUFDckQsT0FBTyxFQUFDLGFBQWEsRUFBQyxNQUFNLHdCQUF3QixDQUFDO0FBQ3JELE9BQU8sRUFBQyxjQUFjLEVBQUMsTUFBTSx5QkFBeUIsQ0FBQztBQUN2RCxPQUFPLEVBQUMsZ0JBQWdCLEVBQUMsTUFBTSwyQkFBMkIsQ0FBQztBQUMzRCxPQUFPLEVBQUMsY0FBYyxFQUFDLE1BQU0sd0JBQXdCLENBQUM7QUFDdEQsT0FBTyxFQUFDLGtCQUFrQixFQUFDLE1BQU0sNkJBQTZCLENBQUM7QUFDL0QsT0FBTyxFQUFDLGFBQWEsRUFBQyxNQUFNLHdCQUF3QixDQUFDO0FBQ3JELE9BQU8sRUFBQyxxQkFBcUIsRUFBQyxNQUFNLGdDQUFnQyxDQUFDO0FBQ3JFLE9BQU8sRUFBQyxrQkFBa0IsRUFBQyxNQUFNLDhCQUE4QixDQUFDO0FBQ2hFLE9BQU8sRUFBQyxjQUFjLEVBQUMsTUFBTSx5QkFBeUIsQ0FBQztBQUV2RCxPQUFPLEVBQUMsWUFBWSxFQUFDLE1BQU0scUJBQXFCLENBQUM7QUFDakQsT0FBTyxFQUFDLGFBQWEsRUFBQyxNQUFNLDJCQUEyQixDQUFDO0FBQ3hELE9BQU8sRUFBQyxlQUFlLEVBQUMsTUFBTSwrQkFBK0IsQ0FBQztBQUM5RCxPQUFPLEVBQUMsbUJBQW1CLEVBQUMsTUFBTSx1Q0FBdUMsQ0FBQztBQUMxRSxPQUFPLEVBQUMsZUFBZSxFQUFDLE1BQU0sa0NBQWtDLENBQUM7QUFDakUsT0FBTyxFQUFDLHFCQUFxQixFQUFDLE1BQU0sOENBQThDLENBQUM7QUFDbkYsT0FBTyxFQUFDLG1CQUFtQixFQUFDLE1BQU0sMENBQTBDLENBQUM7QUFDN0UsT0FBTyxFQUFDLHNCQUFzQixFQUFDLE1BQU0sZ0RBQWdELENBQUM7QUFDdEYsT0FBTyxFQUFDLGlCQUFpQixFQUFDLE1BQU0sc0NBQXNDLENBQUM7QUFDdkUsT0FBTyxFQUFDLGNBQWMsRUFBQyxNQUFNLHlCQUF5QixDQUFDO0FBRXZELE9BQU8sRUFBQyxlQUFlLEVBQUMsTUFBTSwwQkFBMEIsQ0FBQztBQUV6RCxPQUFPLEVBQUMsYUFBYSxFQUFDLE1BQU0sa0JBQWtCLENBQUM7QUFDL0MsT0FBTyxFQUFDLGNBQWMsRUFBQyxNQUFNLG9DQUFvQyxDQUFDO0FBRWxFLE9BQU8sRUFBQyxlQUFlLEVBQUMsTUFBTSx3QkFBd0IsQ0FBQztBQUN2RCxPQUFPLEVBQUMsZ0JBQWdCLEVBQUMsTUFBTSxzQkFBc0IsQ0FBQztBQUN0RCxPQUFPLEVBQUMsb0JBQW9CLEVBQUMsTUFBTSxnQ0FBZ0MsQ0FBQztBQUNwRSxPQUFPLEVBQUMsYUFBYSxFQUFDLE1BQU0sd0JBQXdCLENBQUM7QUFDckQsT0FBTyxFQUFDLGVBQWUsRUFBQyxNQUFNLDBCQUEwQixDQUFDO0FBQ3pELE9BQU8sRUFBQyxrQkFBa0IsRUFBQyxNQUFNLDZCQUE2QixDQUFDO0FBQy9ELE9BQU8sRUFBQyxpQkFBaUIsRUFBQyxNQUFNLDBDQUEwQyxDQUFDO0FBQzNFLE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUU3QyxPQUFPLEVBQUMsWUFBWSxFQUFDLE1BQU0sa0JBQWtCLENBQUM7QUFDOUMsT0FBTyxFQUFDLGlCQUFpQixFQUFFLGNBQWMsRUFBRSxhQUFhLEVBQUMsTUFBTSxtQkFBbUIsQ0FBQztBQUNuRixPQUFPLEVBQUMsb0JBQW9CLEVBQUMsTUFBTSx5REFBeUQsQ0FBQztBQUM3RixPQUFPLEVBQUMscUJBQXFCLEVBQUMsTUFBTSxxREFBcUQsQ0FBQztBQUMxRixPQUFPLEVBQUMsYUFBYSxFQUFDLE1BQU0sNENBQTRDLENBQUM7QUFDekUsT0FBTyxFQUFDLFdBQVcsRUFBQyxNQUFNLG1CQUFtQixDQUFDO0FBQzlDLE9BQU8sRUFBQyxnQkFBZ0IsRUFBQyxNQUFNLGdEQUFnRCxDQUFDO0FBQ2hGLE9BQU8sRUFBQyxpQkFBaUIsRUFBQyxNQUFNLHlCQUF5QixDQUFDO0FBRTFELE9BQU8sRUFBQyxXQUFXLEVBQUMsTUFBTSxzQkFBc0IsQ0FBQztBQUNqRCxPQUFPLEVBQUMsb0JBQW9CLEVBQUMsTUFBTSxzQ0FBc0MsQ0FBQztBQUMxRSxPQUFPLEVBQUMsa0JBQWtCLEVBQUMsTUFBTSxxQ0FBcUMsQ0FBQztBQUN2RSxPQUFPLEVBQUMsbUJBQW1CLEVBQUMsTUFBTSx5Q0FBeUMsQ0FBQztBQUM1RSxPQUFPLEVBQUMsbUJBQW1CLEVBQUMsTUFBTSwrQkFBK0IsQ0FBQztBQUVsRTtJQXdHRSxzQkFBb0Isb0JBQTBDO1FBQTFDLHlCQUFvQixHQUFwQixvQkFBb0IsQ0FBc0I7SUFDOUQsQ0FBQzs7Z0JBekdGLFFBQVEsU0FBQztvQkFDUixTQUFTLEVBQUU7d0JBQ1QsYUFBYTt3QkFDYixjQUFjO3dCQUNkLGFBQWE7d0JBQ2Isb0JBQW9CO3dCQUNwQixtQkFBbUI7cUJBQ3BCO29CQUNELFlBQVksRUFBRTt3QkFDWixlQUFlO3dCQUNmLGVBQWU7d0JBQ2YsWUFBWTt3QkFDWixhQUFhO3dCQUNiLGVBQWU7d0JBQ2YsZUFBZTt3QkFDZixrQkFBa0I7d0JBQ2xCLHFCQUFxQjt3QkFDckIsbUJBQW1CO3dCQUNuQixzQkFBc0I7d0JBQ3RCLGlCQUFpQjt3QkFDakIsbUJBQW1CO3dCQUNuQixpQkFBaUI7d0JBQ2pCLG9CQUFvQjt3QkFDcEIscUJBQXFCO3dCQUNyQixtQkFBbUI7d0JBQ25CLG1CQUFtQjt3QkFDbkIsV0FBVztxQkFDWjtvQkFDRCxPQUFPLEVBQUU7d0JBQ1AsWUFBWTt3QkFDWixZQUFZO3dCQUNaLFdBQVc7d0JBQ1gsbUJBQW1CO3dCQUNuQixZQUFZLENBQUMsT0FBTyxFQUFFO3dCQUN0QixnQkFBZ0I7d0JBQ2hCLGlCQUFpQjt3QkFDakIsZ0JBQWdCO3dCQUNoQixlQUFlO3dCQUNmLGFBQWE7d0JBQ2IsYUFBYTt3QkFDYixjQUFjO3dCQUNkLGdCQUFnQjt3QkFDaEIsY0FBYzt3QkFDZCxrQkFBa0I7d0JBQ2xCLGFBQWE7d0JBQ2IscUJBQXFCO3dCQUNyQixrQkFBa0I7d0JBQ2xCLGNBQWM7d0JBQ2QsZUFBZTt3QkFDZixjQUFjO3dCQUNkLGVBQWU7d0JBQ2YsZ0JBQWdCO3dCQUNoQixvQkFBb0I7d0JBQ3BCLGFBQWE7d0JBQ2IsZUFBZTt3QkFDZixrQkFBa0I7d0JBQ2xCLGNBQWM7d0JBQ2QsYUFBYTt3QkFDYixnQkFBZ0I7d0JBQ2hCLGlCQUFpQjt3QkFDakIsV0FBVztxQkFDWjtvQkFDRCxPQUFPLEVBQUU7d0JBQ1AsYUFBYTt3QkFDYixlQUFlO3dCQUNmLGlCQUFpQjt3QkFDakIsZUFBZTt3QkFDZixlQUFlO3dCQUNmLFlBQVk7d0JBQ1osYUFBYTt3QkFDYixlQUFlO3dCQUNmLGFBQWE7d0JBQ2IsZUFBZTt3QkFDZixxQkFBcUI7d0JBQ3JCLG1CQUFtQjt3QkFDbkIsc0JBQXNCO3dCQUN0QixpQkFBaUI7d0JBQ2pCLGlCQUFpQjt3QkFDakIsWUFBWTt3QkFDWixvQkFBb0I7d0JBQ3BCLHFCQUFxQjt3QkFDckIsZ0JBQWdCO3dCQUNoQixtQkFBbUI7d0JBQ25CLFdBQVc7cUJBQ1o7b0JBQ0QsZUFBZSxFQUFFO3dCQUNmLGVBQWU7d0JBQ2YsZUFBZTt3QkFDZixZQUFZO3dCQUNaLGFBQWE7d0JBQ2IsZUFBZTt3QkFDZixlQUFlO3dCQUNmLHFCQUFxQjt3QkFDckIsbUJBQW1CO3dCQUNuQixzQkFBc0I7d0JBQ3RCLGlCQUFpQjt3QkFDakIsaUJBQWlCO3dCQUNqQixvQkFBb0I7d0JBQ3BCLHFCQUFxQjt3QkFDckIsbUJBQW1CO3dCQUNuQixXQUFXO3FCQUNaO2lCQUNGOzs7O2dCQTNHTyxvQkFBb0I7O0lBK0c1QixtQkFBQztDQUFBLEFBMUdELElBMEdDO1NBSFksWUFBWTs7Ozs7O0lBQ1gsNENBQWtEIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtOZ01vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0NvbW1vbk1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7Rm9ybXNNb2R1bGUsIFJlYWN0aXZlRm9ybXNNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcblxuaW1wb3J0IHtMYXlvdXRDb21wb25lbnR9IGZyb20gJy4vbGF5b3V0LmNvbXBvbmVudCc7XG5pbXBvcnQge0hlYWRlckNvbXBvbmVudH0gZnJvbSAnLi9oZWFkZXIvaGVhZGVyLmNvbXBvbmVudCc7XG5pbXBvcnQge01hdFNpZGVuYXZNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3NpZGVuYXYnO1xuaW1wb3J0IHtNYXRCdXR0b25Nb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2J1dHRvbic7XG5pbXBvcnQge01hdFRvb2xiYXJNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3Rvb2xiYXInO1xuaW1wb3J0IHtNYXRNZW51TW9kdWxlfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9tZW51JztcbmltcG9ydCB7TWF0SWNvbk1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvaWNvbic7XG5pbXBvcnQge01hdEJhZGdlTW9kdWxlfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9iYWRnZSc7XG5pbXBvcnQge01hdFRvb2x0aXBNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3Rvb2x0aXAnO1xuaW1wb3J0IHtEcmFnRHJvcE1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY2RrL2RyYWctZHJvcCc7XG5pbXBvcnQge01hdEV4cGFuc2lvbk1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvZXhwYW5zaW9uJztcbmltcG9ydCB7TWF0TGlzdE1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvbGlzdCc7XG5pbXBvcnQge01hdEF1dG9jb21wbGV0ZU1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvYXV0b2NvbXBsZXRlJztcbmltcG9ydCB7TWF0Rm9ybUZpZWxkTW9kdWxlfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9mb3JtLWZpZWxkJztcbmltcG9ydCB7TWF0SW5wdXRNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2lucHV0JztcblxuaW1wb3J0IHtOYXZDb21wb25lbnR9IGZyb20gJy4vbmF2L25hdi5jb21wb25lbnQnO1xuaW1wb3J0IHtNZW51Q29tcG9uZW50fSBmcm9tICcuL25hdi9tZW51L21lbnUuY29tcG9uZW50JztcbmltcG9ydCB7RmF2bmF2Q29tcG9uZW50fSBmcm9tICcuL25hdi9mYXZuYXYvZmF2bmF2LmNvbXBvbmVudCc7XG5pbXBvcnQge0JyZWFkY3J1bWJDb21wb25lbnR9IGZyb20gJy4vbmF2L2JyZWFkY3J1bWIvYnJlYWRjcnVtYi5jb21wb25lbnQnO1xuaW1wb3J0IHtTZWFyY2hDb21wb25lbnR9IGZyb20gJy4vaGVhZGVyL3NlYXJjaC9zZWFyY2guY29tcG9uZW50JztcbmltcG9ydCB7Tm90aWZpY2F0aW9uQ29tcG9uZW50fSBmcm9tICcuL2hlYWRlci9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLmNvbXBvbmVudCc7XG5pbXBvcnQge1N5c3RlbWluZm9Db21wb25lbnR9IGZyb20gJy4vaGVhZGVyL3N5c3RlbWluZm8vc3lzdGVtaW5mby5jb21wb25lbnQnO1xuaW1wb3J0IHtBY2Nlc3NpYmlsaXR5Q29tcG9uZW50fSBmcm9tICcuL2hlYWRlci9hY2Nlc3NpYmlsaXR5L2FjY2Vzc2liaWxpdHkuY29tcG9uZW50JztcbmltcG9ydCB7VXNlcmluZm9Db21wb25lbnR9IGZyb20gJy4vaGVhZGVyL3VzZXJpbmZvL3VzZXJpbmZvLmNvbXBvbmVudCc7XG5pbXBvcnQge01hdENoaXBzTW9kdWxlfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9jaGlwcyc7XG5cbmltcG9ydCB7TWF0U2xpZGVyTW9kdWxlfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9zbGlkZXInO1xuXG5pbXBvcnQge0xheW91dFNlcnZpY2V9IGZyb20gJy4vbGF5b3V0LnNlcnZpY2UnO1xuaW1wb3J0IHtGYXZOYXZzU2VydmljZX0gZnJvbSAnLi9uYXYvZmF2bmF2L3N0YXRlL2Zhdm5hdnMuc2VydmljZSc7XG5cbmltcG9ydCB7U2Nyb2xsaW5nTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jZGsvc2Nyb2xsaW5nJztcbmltcG9ydCB7Q2RrU3RlcHBlck1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY2RrL3N0ZXBwZXInO1xuaW1wb3J0IHtNYXRTbGlkZVRvZ2dsZU1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvc2xpZGUtdG9nZ2xlJztcbmltcG9ydCB7TWF0VGFic01vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvdGFicyc7XG5pbXBvcnQge01hdFNlbGVjdE1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvc2VsZWN0JztcbmltcG9ydCB7TWF0UGFnaW5hdG9yTW9kdWxlfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9wYWdpbmF0b3InO1xuaW1wb3J0IHtNZW51SXRlbUNvbXBvbmVudH0gZnJvbSAnLi9uYXYvbWVudS9tZW51LWl0ZW0vbWVudS1pdGVtLmNvbXBvbmVudCc7XG5pbXBvcnQge1JvdXRlck1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcblxuaW1wb3J0IHtIb3RrZXlNb2R1bGV9IGZyb20gJ2FuZ3VsYXIyLWhvdGtleXMnO1xuaW1wb3J0IHtNYXRTbmFja0Jhck1vZHVsZSwgTWF0VGFibGVNb2R1bGUsIE1hdFRyZWVNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcbmltcG9ydCB7Tm90aWZpY2FjYW9Db21wb25lbnR9IGZyb20gJy4vaGVhZGVyL25vdGlmaWNhdGlvbi9ub3RpZmljYWNhby9ub3RpZmljYWNhby5jb21wb25lbnQnO1xuaW1wb3J0IHtTaW1wbGVEaWFsb2dDb21wb25lbnR9IGZyb20gJy4uL2NvbXBvbmVudHMvc2ltcGxlLWRpYWxvZy9zaW1wbGUtZGlhbG9nLmNvbXBvbmVudCc7XG5pbXBvcnQge0RpYWxvZ1NlcnZpY2V9IGZyb20gJy4uL2NvbXBvbmVudHMvc2ltcGxlLWRpYWxvZy9kaWFsb2cuc2VydmljZSc7XG5pbXBvcnQge0lhQ29tcG9uZW50fSBmcm9tICcuL2lhL2lhLmNvbXBvbmVudCc7XG5pbXBvcnQge0JyZWFkY3J1bWJNb2R1bGV9IGZyb20gJy4vbmF2L2JyZWFkY3J1bWIvYnJlYWRjcnVtYnMvYnJlYWRjcnVtYi5tb2R1bGUnO1xuaW1wb3J0IHtVaWtpdFNoYXJlZE1vZHVsZX0gZnJvbSAnLi4vc2hhcmVkL3NoYXJlZC5tb2R1bGUnO1xuXG5pbXBvcnQge1RvYXN0TW9kdWxlfSBmcm9tICcuL3RvYXN0L3RvYXN0Lm1vZHVsZSc7XG5pbXBvcnQge1ByZXZpb3VzUm91dGVTZXJ2aWNlfSBmcm9tICcuLi9jb21wb25lbnRzL3ByZXZpb3VzLXJvdXRlLnNlcnZpY2UnO1xuaW1wb3J0IHtIaWdobGlnaHRDb21wb25lbnR9IGZyb20gJy4vbmF2L2hpZ2hsaWdodC9oaWdobGlnaHQuY29tcG9uZW50JztcbmltcG9ydCB7TWVudVNlYXJjaENvbXBvbmVudH0gZnJvbSBcIi4vbmF2L21lbnUtc2VhcmNoL21lbnUtc2VhcmNoLmNvbXBvbmVudFwiO1xuaW1wb3J0IHtVaWtpdFJyaXBwbGVTZXJ2aWNlfSBmcm9tIFwiLi4vdXRpbHMvdWlraXQtcmlwcGxlLnNlcnZpY2VcIjtcblxuQE5nTW9kdWxlKHtcbiAgcHJvdmlkZXJzOiBbXG4gICAgTGF5b3V0U2VydmljZSxcbiAgICBGYXZOYXZzU2VydmljZSxcbiAgICBEaWFsb2dTZXJ2aWNlLFxuICAgIFByZXZpb3VzUm91dGVTZXJ2aWNlLFxuICAgIFVpa2l0UnJpcHBsZVNlcnZpY2VcbiAgXSxcbiAgZGVjbGFyYXRpb25zOiBbXG4gICAgTGF5b3V0Q29tcG9uZW50LFxuICAgIEhlYWRlckNvbXBvbmVudCxcbiAgICBOYXZDb21wb25lbnQsXG4gICAgTWVudUNvbXBvbmVudCxcbiAgICBGYXZuYXZDb21wb25lbnQsXG4gICAgU2VhcmNoQ29tcG9uZW50LFxuICAgIEhpZ2hsaWdodENvbXBvbmVudCxcbiAgICBOb3RpZmljYXRpb25Db21wb25lbnQsXG4gICAgU3lzdGVtaW5mb0NvbXBvbmVudCxcbiAgICBBY2Nlc3NpYmlsaXR5Q29tcG9uZW50LFxuICAgIFVzZXJpbmZvQ29tcG9uZW50LFxuICAgIEJyZWFkY3J1bWJDb21wb25lbnQsXG4gICAgTWVudUl0ZW1Db21wb25lbnQsXG4gICAgTm90aWZpY2FjYW9Db21wb25lbnQsXG4gICAgU2ltcGxlRGlhbG9nQ29tcG9uZW50LFxuICAgIEJyZWFkY3J1bWJDb21wb25lbnQsXG4gICAgTWVudVNlYXJjaENvbXBvbmVudCxcbiAgICBJYUNvbXBvbmVudFxuICBdLFxuICBpbXBvcnRzOiBbXG4gICAgQ29tbW9uTW9kdWxlLFxuICAgIFJvdXRlck1vZHVsZSxcbiAgICBGb3Jtc01vZHVsZSxcbiAgICBSZWFjdGl2ZUZvcm1zTW9kdWxlLFxuICAgIEhvdGtleU1vZHVsZS5mb3JSb290KCksXG4gICAgTWF0U2lkZW5hdk1vZHVsZSxcbiAgICBNYXRTbmFja0Jhck1vZHVsZSxcbiAgICBNYXRUb29sYmFyTW9kdWxlLFxuICAgIE1hdEJ1dHRvbk1vZHVsZSxcbiAgICBNYXRNZW51TW9kdWxlLFxuICAgIE1hdEljb25Nb2R1bGUsXG4gICAgTWF0QmFkZ2VNb2R1bGUsXG4gICAgTWF0VG9vbHRpcE1vZHVsZSxcbiAgICBEcmFnRHJvcE1vZHVsZSxcbiAgICBNYXRFeHBhbnNpb25Nb2R1bGUsXG4gICAgTWF0TGlzdE1vZHVsZSxcbiAgICBNYXRBdXRvY29tcGxldGVNb2R1bGUsXG4gICAgTWF0Rm9ybUZpZWxkTW9kdWxlLFxuICAgIE1hdElucHV0TW9kdWxlLFxuICAgIFNjcm9sbGluZ01vZHVsZSxcbiAgICBNYXRDaGlwc01vZHVsZSxcbiAgICBNYXRTbGlkZXJNb2R1bGUsXG4gICAgQ2RrU3RlcHBlck1vZHVsZSxcbiAgICBNYXRTbGlkZVRvZ2dsZU1vZHVsZSxcbiAgICBNYXRUYWJzTW9kdWxlLFxuICAgIE1hdFNlbGVjdE1vZHVsZSxcbiAgICBNYXRQYWdpbmF0b3JNb2R1bGUsXG4gICAgTWF0VGFibGVNb2R1bGUsXG4gICAgTWF0VHJlZU1vZHVsZSxcbiAgICBCcmVhZGNydW1iTW9kdWxlLFxuICAgIFVpa2l0U2hhcmVkTW9kdWxlLFxuICAgIFRvYXN0TW9kdWxlLFxuICBdLFxuICBleHBvcnRzOiBbXG4gICAgTWF0SWNvbk1vZHVsZSxcbiAgICBNYXRCdXR0b25Nb2R1bGUsXG4gICAgTWF0U25hY2tCYXJNb2R1bGUsXG4gICAgTGF5b3V0Q29tcG9uZW50LFxuICAgIEhlYWRlckNvbXBvbmVudCxcbiAgICBOYXZDb21wb25lbnQsXG4gICAgTWVudUNvbXBvbmVudCxcbiAgICBGYXZuYXZDb21wb25lbnQsXG4gICAgTWF0TGlzdE1vZHVsZSxcbiAgICBTZWFyY2hDb21wb25lbnQsXG4gICAgTm90aWZpY2F0aW9uQ29tcG9uZW50LFxuICAgIFN5c3RlbWluZm9Db21wb25lbnQsXG4gICAgQWNjZXNzaWJpbGl0eUNvbXBvbmVudCxcbiAgICBVc2VyaW5mb0NvbXBvbmVudCxcbiAgICBNZW51SXRlbUNvbXBvbmVudCxcbiAgICBIb3RrZXlNb2R1bGUsXG4gICAgTm90aWZpY2FjYW9Db21wb25lbnQsXG4gICAgU2ltcGxlRGlhbG9nQ29tcG9uZW50LFxuICAgIEJyZWFkY3J1bWJNb2R1bGUsXG4gICAgTWVudVNlYXJjaENvbXBvbmVudCxcbiAgICBUb2FzdE1vZHVsZVxuICBdLFxuICBlbnRyeUNvbXBvbmVudHM6IFtcbiAgICBMYXlvdXRDb21wb25lbnQsXG4gICAgSGVhZGVyQ29tcG9uZW50LFxuICAgIE5hdkNvbXBvbmVudCxcbiAgICBNZW51Q29tcG9uZW50LFxuICAgIEZhdm5hdkNvbXBvbmVudCxcbiAgICBTZWFyY2hDb21wb25lbnQsXG4gICAgTm90aWZpY2F0aW9uQ29tcG9uZW50LFxuICAgIFN5c3RlbWluZm9Db21wb25lbnQsXG4gICAgQWNjZXNzaWJpbGl0eUNvbXBvbmVudCxcbiAgICBVc2VyaW5mb0NvbXBvbmVudCxcbiAgICBNZW51SXRlbUNvbXBvbmVudCxcbiAgICBOb3RpZmljYWNhb0NvbXBvbmVudCxcbiAgICBTaW1wbGVEaWFsb2dDb21wb25lbnQsXG4gICAgQnJlYWRjcnVtYkNvbXBvbmVudCxcbiAgICBJYUNvbXBvbmVudFxuICBdXG59KVxuZXhwb3J0IGNsYXNzIExheW91dE1vZHVsZSB7XG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgcHJldmlvdXNSb3V0ZVNlcnZpY2U6IFByZXZpb3VzUm91dGVTZXJ2aWNlKSB7XG4gIH1cbn1cbiJdfQ==