/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ChangeDetectionStrategy } from '@angular/core';
/**
 * @record
 */
export function Section() { }
if (false) {
    /** @type {?} */
    Section.prototype.name;
    /** @type {?} */
    Section.prototype.date;
    /** @type {?} */
    Section.prototype.description;
    /** @type {?} */
    Section.prototype.read;
}
var SysteminfoComponent = /** @class */ (function () {
    function SysteminfoComponent() {
        this.folders = [
            {
                name: '2.0.2',
                date: '04/02/2019',
                description: 'Nesta versão reunimos grandes demandas de melhoria do sistema.',
                read: true
            },
            {
                name: '2.0.1.1',
                date: '27/11/2018',
                description: 'Para esta versão foi feito um trabalho detalhado de correção, uniformização e melhoria da funcionalidade de redistribuição de processos, muitos tribunais estavam reportando e encaminhando códigos pontuais de correção da redistribuição. A equipe do CNJ fez um trabalho detalhado de revisão e readequação da rotina, simplificando o código, melhorando a usabilidade e padronizando as operações. Com esse trabalho foram atendidas outras 30 demandas de 12 tribunais diferentes.',
                read: false
            }
        ];
    }
    /**
     * @return {?}
     */
    SysteminfoComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () { };
    SysteminfoComponent.decorators = [
        { type: Component, args: [{
                    selector: 'uikit-systeminfo',
                    template: "<mat-menu #systemInfo=\"matMenu\">\n  <mat-list class=\"dropdown-list list-reader\" (click)=\"$event.stopPropagation()\">\n    <h3 mat-subheader>\n      Informa\u00E7\u00F5es do sistema\n    </h3>\n    <ng-content></ng-content>\n    <!-- <mat-nav-list>\n      <a mat-list-item *ngFor=\"let folder of folders\" [class.is-read]=\"folder.read\">\n        <div>\n          <h4>\n            {{ folder.name }}\n          </h4>\n          <span class=\"date\">{{ folder.date }}</span>\n          <p>\n            {{ folder.description }}\n          </p>\n        </div>\n        <mat-icon matListIcon class=\"fa-lg far\" [class.fa-eye]=\"!folder.read\" [class.fa-eye-slash]=\"folder.read\"></mat-icon>\n\n        <mat-divider></mat-divider>\n      </a>\n    </mat-nav-list> -->\n  </mat-list>\n\n  <!-- <mat-action-list>\n      <a  mat-list-item routerLink=\".\">ver todos</a>\n    </mat-action-list> -->\n</mat-menu>\n\n<button\n  mat-icon-button\n  tabindex=\"4\"\n  color=\"primary\"\n  [matMenuTriggerFor]=\"systemInfo\"\n  matTooltip=\"Informa\u00E7\u00F5es do sistema\"\n  aria-label=\"Informa\u00E7\u00F5es do sistema\"\n>\n  <mat-icon class=\"fa-2x fas fa-info-circle\"></mat-icon>\n</button>\n",
                    changeDetection: ChangeDetectionStrategy.OnPush,
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    SysteminfoComponent.ctorParameters = function () { return []; };
    return SysteminfoComponent;
}());
export { SysteminfoComponent };
if (false) {
    /** @type {?} */
    SysteminfoComponent.prototype.folders;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3lzdGVtaW5mby5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL2xheW91dC9oZWFkZXIvc3lzdGVtaW5mby9zeXN0ZW1pbmZvLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSx1QkFBdUIsRUFBRSxNQUFNLGVBQWUsQ0FBQzs7OztBQUUzRSw2QkFLQzs7O0lBSkMsdUJBQWE7O0lBQ2IsdUJBQWE7O0lBQ2IsOEJBQW9COztJQUNwQix1QkFBYzs7QUFHaEI7SUF3QkU7UUFqQkEsWUFBTyxHQUFjO1lBQ25CO2dCQUNFLElBQUksRUFBRSxPQUFPO2dCQUNiLElBQUksRUFBRSxZQUFZO2dCQUNsQixXQUFXLEVBQ1QsZ0VBQWdFO2dCQUNsRSxJQUFJLEVBQUUsSUFBSTthQUNYO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLFNBQVM7Z0JBQ2YsSUFBSSxFQUFFLFlBQVk7Z0JBQ2xCLFdBQVcsRUFDVCwwZEFBMGQ7Z0JBQzVkLElBQUksRUFBRSxLQUFLO2FBQ1o7U0FDRixDQUFDO0lBRWEsQ0FBQzs7OztJQUVoQixzQ0FBUTs7O0lBQVIsY0FBWSxDQUFDOztnQkExQmQsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxrQkFBa0I7b0JBQzVCLHdyQ0FBMEM7b0JBRTFDLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNOztpQkFDaEQ7Ozs7SUFzQkQsMEJBQUM7Q0FBQSxBQTNCRCxJQTJCQztTQXJCWSxtQkFBbUI7OztJQUM5QixzQ0FlRSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5leHBvcnQgaW50ZXJmYWNlIFNlY3Rpb24ge1xuICBuYW1lOiBzdHJpbmc7XG4gIGRhdGU6IHN0cmluZztcbiAgZGVzY3JpcHRpb246IHN0cmluZztcbiAgcmVhZDogYm9vbGVhbjtcbn1cblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAndWlraXQtc3lzdGVtaW5mbycsXG4gIHRlbXBsYXRlVXJsOiAnLi9zeXN0ZW1pbmZvLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vc3lzdGVtaW5mby5jb21wb25lbnQuc2NzcyddLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaFxufSlcbmV4cG9ydCBjbGFzcyBTeXN0ZW1pbmZvQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgZm9sZGVyczogU2VjdGlvbltdID0gW1xuICAgIHtcbiAgICAgIG5hbWU6ICcyLjAuMicsXG4gICAgICBkYXRlOiAnMDQvMDIvMjAxOScsXG4gICAgICBkZXNjcmlwdGlvbjpcbiAgICAgICAgJ05lc3RhIHZlcnPDo28gcmV1bmltb3MgZ3JhbmRlcyBkZW1hbmRhcyBkZSBtZWxob3JpYSBkbyBzaXN0ZW1hLicsXG4gICAgICByZWFkOiB0cnVlXG4gICAgfSxcbiAgICB7XG4gICAgICBuYW1lOiAnMi4wLjEuMScsXG4gICAgICBkYXRlOiAnMjcvMTEvMjAxOCcsXG4gICAgICBkZXNjcmlwdGlvbjpcbiAgICAgICAgJ1BhcmEgZXN0YSB2ZXJzw6NvIGZvaSBmZWl0byB1bSB0cmFiYWxobyBkZXRhbGhhZG8gZGUgY29ycmXDp8OjbywgdW5pZm9ybWl6YcOnw6NvIGUgbWVsaG9yaWEgZGEgZnVuY2lvbmFsaWRhZGUgZGUgcmVkaXN0cmlidWnDp8OjbyBkZSBwcm9jZXNzb3MsIG11aXRvcyB0cmlidW5haXMgZXN0YXZhbSByZXBvcnRhbmRvIGUgZW5jYW1pbmhhbmRvIGPDs2RpZ29zIHBvbnR1YWlzIGRlIGNvcnJlw6fDo28gZGEgcmVkaXN0cmlidWnDp8Ojby4gQSBlcXVpcGUgZG8gQ05KIGZleiB1bSB0cmFiYWxobyBkZXRhbGhhZG8gZGUgcmV2aXPDo28gZSByZWFkZXF1YcOnw6NvIGRhIHJvdGluYSwgc2ltcGxpZmljYW5kbyBvIGPDs2RpZ28sIG1lbGhvcmFuZG8gYSB1c2FiaWxpZGFkZSBlIHBhZHJvbml6YW5kbyBhcyBvcGVyYcOnw7Vlcy4gQ29tIGVzc2UgdHJhYmFsaG8gZm9yYW0gYXRlbmRpZGFzIG91dHJhcyAzMCBkZW1hbmRhcyBkZSAxMiB0cmlidW5haXMgZGlmZXJlbnRlcy4nLFxuICAgICAgcmVhZDogZmFsc2VcbiAgICB9XG4gIF07XG5cbiAgY29uc3RydWN0b3IoKSB7fVxuXG4gIG5nT25Jbml0KCkge31cbn1cbiJdfQ==