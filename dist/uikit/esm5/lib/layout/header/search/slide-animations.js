/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { animate, state, style, transition, trigger } from '@angular/animations';
/**
 * @return {?}
 */
export function searchAnimation() {
    return trigger('searchAnimation', [
        state('true', style({ transform: 'translateX(0%)' })),
        state('false', style({ transform: 'translateX(100%)' })),
        transition('false => true', animate('0.2s ease-in-out', style({ transform: 'translateX(0%)' }))),
        transition('true => false', animate('0.2s ease-in-out', style({ transform: 'translateX(100%)' })))
    ]);
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2xpZGUtYW5pbWF0aW9ucy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvbGF5b3V0L2hlYWRlci9zZWFyY2gvc2xpZGUtYW5pbWF0aW9ucy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLE9BQU8sRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxPQUFPLEVBQUMsTUFBTSxxQkFBcUIsQ0FBQzs7OztBQUUvRSxNQUFNLFVBQVUsZUFBZTtJQUM3QixPQUFPLE9BQU8sQ0FBQyxpQkFBaUIsRUFBRTtRQUNoQyxLQUFLLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxFQUFDLFNBQVMsRUFBRSxnQkFBZ0IsRUFBQyxDQUFDLENBQUM7UUFDbkQsS0FBSyxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsRUFBQyxTQUFTLEVBQUUsa0JBQWtCLEVBQUMsQ0FBQyxDQUFDO1FBQ3RELFVBQVUsQ0FBQyxlQUFlLEVBQUUsT0FBTyxDQUFDLGtCQUFrQixFQUFFLEtBQUssQ0FBQyxFQUFDLFNBQVMsRUFBRSxnQkFBZ0IsRUFBQyxDQUFDLENBQUMsQ0FBQztRQUM5RixVQUFVLENBQUMsZUFBZSxFQUFFLE9BQU8sQ0FBQyxrQkFBa0IsRUFBRSxLQUFLLENBQUMsRUFBQyxTQUFTLEVBQUUsa0JBQWtCLEVBQUMsQ0FBQyxDQUFDLENBQUM7S0FDakcsQ0FBQyxDQUFDO0FBQ0wsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7YW5pbWF0ZSwgc3RhdGUsIHN0eWxlLCB0cmFuc2l0aW9uLCB0cmlnZ2VyfSBmcm9tICdAYW5ndWxhci9hbmltYXRpb25zJztcblxuZXhwb3J0IGZ1bmN0aW9uIHNlYXJjaEFuaW1hdGlvbigpIHtcbiAgcmV0dXJuIHRyaWdnZXIoJ3NlYXJjaEFuaW1hdGlvbicsIFtcbiAgICBzdGF0ZSgndHJ1ZScsIHN0eWxlKHt0cmFuc2Zvcm06ICd0cmFuc2xhdGVYKDAlKSd9KSksXG4gICAgc3RhdGUoJ2ZhbHNlJywgc3R5bGUoe3RyYW5zZm9ybTogJ3RyYW5zbGF0ZVgoMTAwJSknfSkpLFxuICAgIHRyYW5zaXRpb24oJ2ZhbHNlID0+IHRydWUnLCBhbmltYXRlKCcwLjJzIGVhc2UtaW4tb3V0Jywgc3R5bGUoe3RyYW5zZm9ybTogJ3RyYW5zbGF0ZVgoMCUpJ30pKSksXG4gICAgdHJhbnNpdGlvbigndHJ1ZSA9PiBmYWxzZScsIGFuaW1hdGUoJzAuMnMgZWFzZS1pbi1vdXQnLCBzdHlsZSh7dHJhbnNmb3JtOiAndHJhbnNsYXRlWCgxMDAlKSd9KSkpXG4gIF0pO1xufVxuIl19