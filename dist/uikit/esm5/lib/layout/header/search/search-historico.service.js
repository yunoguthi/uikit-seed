/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { LocalRepository } from '../../../shared/repositorio/local-repository';
import { of } from 'rxjs';
import * as _ from 'lodash';
import * as i0 from "@angular/core";
import * as i1 from "../../../shared/repositorio/local-repository";
var SearchHistoricoService = /** @class */ (function () {
    function SearchHistoricoService(repositorio) {
        this.repositorio = repositorio;
    }
    /**
     * @return {?}
     */
    SearchHistoricoService.prototype.obterHistorico = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var listaHistorico;
        /** @type {?} */
        var key = 'historicoPesquisa';
        /** @type {?} */
        var item = this.repositorio.obterItem(key);
        if (item === false) {
            this.repositorio.salvarItem(key, []);
        }
        else {
            listaHistorico = item;
        }
        return listaHistorico;
    };
    /**
     * @param {?} historico
     * @return {?}
     */
    SearchHistoricoService.prototype.salvarHistorico = /**
     * @param {?} historico
     * @return {?}
     */
    function (historico) {
        /** @type {?} */
        var listaHistorico;
        /** @type {?} */
        var key = 'historicoPesquisa';
        /** @type {?} */
        var itemAddHistorage = this.repositorio.obterItem(key);
        /** @type {?} */
        var itemRetorno = _.xorWith([historico], itemAddHistorage, this.isEqual);
        if (itemRetorno.length > 3) {
            listaHistorico = _.take(itemRetorno, 3);
        }
        else {
            listaHistorico = itemRetorno;
        }
        this.repositorio.salvarItem(key, listaHistorico);
        return of(null);
    };
    /**
     * @private
     * @param {?} p1
     * @param {?} p2
     * @return {?}
     */
    SearchHistoricoService.prototype.isEqual = /**
     * @private
     * @param {?} p1
     * @param {?} p2
     * @return {?}
     */
    function (p1, p2) {
        return _.isEqual({ x: p1.title, y: p1.title }, { x: p2.title, y: p2.title });
    };
    SearchHistoricoService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    SearchHistoricoService.ctorParameters = function () { return [
        { type: LocalRepository }
    ]; };
    /** @nocollapse */ SearchHistoricoService.ngInjectableDef = i0.defineInjectable({ factory: function SearchHistoricoService_Factory() { return new SearchHistoricoService(i0.inject(i1.LocalRepository)); }, token: SearchHistoricoService, providedIn: "root" });
    return SearchHistoricoService;
}());
export { SearchHistoricoService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    SearchHistoricoService.prototype.repositorio;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLWhpc3Rvcmljby5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9sYXlvdXQvaGVhZGVyL3NlYXJjaC9zZWFyY2gtaGlzdG9yaWNvLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFDLGVBQWUsRUFBQyxNQUFNLDhDQUE4QyxDQUFDO0FBRTdFLE9BQU8sRUFBbUIsRUFBRSxFQUFDLE1BQU0sTUFBTSxDQUFDO0FBQzFDLE9BQU8sS0FBSyxDQUFDLE1BQU0sUUFBUSxDQUFDOzs7QUFFNUI7SUFPRSxnQ0FBWSxXQUE0QjtRQUN0QyxJQUFJLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQztJQUNqQyxDQUFDOzs7O0lBRUQsK0NBQWM7OztJQUFkOztZQUNNLGNBQWM7O1lBQ1osR0FBRyxHQUFHLG1CQUFtQjs7WUFDekIsSUFBSSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFNLEdBQUcsQ0FBQztRQUNqRCxJQUFJLElBQUksS0FBSyxLQUFLLEVBQUU7WUFDbEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1NBQ3RDO2FBQU07WUFDTCxjQUFjLEdBQUcsSUFBSSxDQUFDO1NBQ3ZCO1FBQ0QsT0FBTyxjQUFjLENBQUM7SUFDeEIsQ0FBQzs7Ozs7SUFFRCxnREFBZTs7OztJQUFmLFVBQWdCLFNBQWM7O1lBQ3hCLGNBQWM7O1lBQ1osR0FBRyxHQUFHLG1CQUFtQjs7WUFDekIsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQU0sR0FBRyxDQUFDOztZQUN2RCxXQUFXLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxFQUFFLGdCQUFnQixFQUFFLElBQUksQ0FBQyxPQUFPLENBQUM7UUFFMUUsSUFBSSxXQUFXLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUMxQixjQUFjLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDekM7YUFBTTtZQUNMLGNBQWMsR0FBRyxXQUFXLENBQUM7U0FDOUI7UUFDRCxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxHQUFHLEVBQUUsY0FBYyxDQUFDLENBQUM7UUFDakQsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDbEIsQ0FBQzs7Ozs7OztJQUVPLHdDQUFPOzs7Ozs7SUFBZixVQUFnQixFQUFPLEVBQUUsRUFBTztRQUM5QixPQUFPLENBQUMsQ0FBQyxPQUFPLENBQUMsRUFBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLEtBQUssRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLEtBQUssRUFBQyxFQUFFLEVBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxLQUFLLEVBQUMsQ0FBQyxDQUFDO0lBQzNFLENBQUM7O2dCQXhDRixVQUFVLFNBQUM7b0JBQ1YsVUFBVSxFQUFFLE1BQU07aUJBQ25COzs7O2dCQVBPLGVBQWU7OztpQ0FEdkI7Q0ErQ0MsQUF6Q0QsSUF5Q0M7U0F0Q1ksc0JBQXNCOzs7Ozs7SUFFakMsNkNBQStDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtJbmplY3RhYmxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7TG9jYWxSZXBvc2l0b3J5fSBmcm9tICcuLi8uLi8uLi9zaGFyZWQvcmVwb3NpdG9yaW8vbG9jYWwtcmVwb3NpdG9yeSc7XG5pbXBvcnQge0lMb2NhbFJlcG9zaXRvcnl9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9yZXBvc2l0b3Jpby9pbG9jYWwtcmVwb3NpdG9yeSc7XG5pbXBvcnQge2Zyb20sIE9ic2VydmFibGUsIG9mfSBmcm9tICdyeGpzJztcbmltcG9ydCAqIGFzIF8gZnJvbSAnbG9kYXNoJztcblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgU2VhcmNoSGlzdG9yaWNvU2VydmljZSB7XG5cbiAgcHJpdmF0ZSByZWFkb25seSByZXBvc2l0b3JpbzogSUxvY2FsUmVwb3NpdG9yeTtcblxuICBjb25zdHJ1Y3RvcihyZXBvc2l0b3JpbzogTG9jYWxSZXBvc2l0b3J5KSB7XG4gICAgdGhpcy5yZXBvc2l0b3JpbyA9IHJlcG9zaXRvcmlvO1xuICB9XG5cbiAgb2J0ZXJIaXN0b3JpY28oKSB7XG4gICAgbGV0IGxpc3RhSGlzdG9yaWNvO1xuICAgIGNvbnN0IGtleSA9ICdoaXN0b3JpY29QZXNxdWlzYSc7XG4gICAgY29uc3QgaXRlbSA9IHRoaXMucmVwb3NpdG9yaW8ub2J0ZXJJdGVtPGFueT4oa2V5KTtcbiAgICBpZiAoaXRlbSA9PT0gZmFsc2UpIHtcbiAgICAgIHRoaXMucmVwb3NpdG9yaW8uc2FsdmFySXRlbShrZXksIFtdKTtcbiAgICB9IGVsc2Uge1xuICAgICAgbGlzdGFIaXN0b3JpY28gPSBpdGVtO1xuICAgIH1cbiAgICByZXR1cm4gbGlzdGFIaXN0b3JpY287XG4gIH1cblxuICBzYWx2YXJIaXN0b3JpY28oaGlzdG9yaWNvOiBhbnkpOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIGxldCBsaXN0YUhpc3RvcmljbztcbiAgICBjb25zdCBrZXkgPSAnaGlzdG9yaWNvUGVzcXVpc2EnO1xuICAgIGNvbnN0IGl0ZW1BZGRIaXN0b3JhZ2UgPSB0aGlzLnJlcG9zaXRvcmlvLm9idGVySXRlbTxhbnk+KGtleSk7XG4gICAgY29uc3QgaXRlbVJldG9ybm8gPSBfLnhvcldpdGgoW2hpc3Rvcmljb10sIGl0ZW1BZGRIaXN0b3JhZ2UsIHRoaXMuaXNFcXVhbCk7XG5cbiAgICBpZiAoaXRlbVJldG9ybm8ubGVuZ3RoID4gMykge1xuICAgICAgbGlzdGFIaXN0b3JpY28gPSBfLnRha2UoaXRlbVJldG9ybm8sIDMpO1xuICAgIH0gZWxzZSB7XG4gICAgICBsaXN0YUhpc3RvcmljbyA9IGl0ZW1SZXRvcm5vO1xuICAgIH1cbiAgICB0aGlzLnJlcG9zaXRvcmlvLnNhbHZhckl0ZW0oa2V5LCBsaXN0YUhpc3Rvcmljbyk7XG4gICAgcmV0dXJuIG9mKG51bGwpO1xuICB9XG5cbiAgcHJpdmF0ZSBpc0VxdWFsKHAxOiBhbnksIHAyOiBhbnkpIHtcbiAgICByZXR1cm4gXy5pc0VxdWFsKHt4OiBwMS50aXRsZSwgeTogcDEudGl0bGV9LCB7eDogcDIudGl0bGUsIHk6IHAyLnRpdGxlfSk7XG4gIH1cbn1cbiJdfQ==