/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as _ from 'lodash';
/**
 * @record
 */
export function ItemPesquisa() { }
if (false) {
    /** @type {?} */
    ItemPesquisa.prototype.id;
    /** @type {?} */
    ItemPesquisa.prototype.title;
    /** @type {?|undefined} */
    ItemPesquisa.prototype.icon;
    /** @type {?} */
    ItemPesquisa.prototype.tags;
    /** @type {?} */
    ItemPesquisa.prototype.link;
}
/**
 * @record
 */
export function GrupoPesquisa() { }
if (false) {
    /** @type {?} */
    GrupoPesquisa.prototype.id;
    /** @type {?} */
    GrupoPesquisa.prototype.title;
    /** @type {?|undefined} */
    GrupoPesquisa.prototype.icon;
    /** @type {?|undefined} */
    GrupoPesquisa.prototype.isHeader;
    /** @type {?|undefined} */
    GrupoPesquisa.prototype.isAviso;
    /** @type {?} */
    GrupoPesquisa.prototype.items;
}
/**
 * @return {?}
 */
export function criarNovoGrupo() {
    return (/** @type {?} */ ({
        title: '',
        items: []
    }));
}
/**
 * @param {?} itemParam
 * @return {?}
 */
export function popularItens(itemParam) {
    return _.map(itemParam, (/**
     * @param {?} item
     * @return {?}
     */
    function (item) {
        return (/** @type {?} */ ({
            id: item.id,
            title: item.title,
            tags: item.tags,
            link: item.link
        }));
    }));
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9sYXlvdXQvaGVhZGVyL3NlYXJjaC9zZWFyY2gubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sS0FBSyxDQUFDLE1BQU0sUUFBUSxDQUFDOzs7O0FBRzVCLGtDQU1DOzs7SUFMQywwQkFBTzs7SUFDUCw2QkFBYzs7SUFDZCw0QkFBYzs7SUFDZCw0QkFBZTs7SUFDZiw0QkFBYTs7Ozs7QUFHZixtQ0FPQzs7O0lBTkMsMkJBQU87O0lBQ1AsOEJBQWM7O0lBQ2QsNkJBQWM7O0lBQ2QsaUNBQW1COztJQUNuQixnQ0FBa0I7O0lBQ2xCLDhCQUF3Qzs7Ozs7QUFHMUMsTUFBTSxVQUFVLGNBQWM7SUFDNUIsT0FBTyxtQkFBQTtRQUNMLEtBQUssRUFBRSxFQUFFO1FBQ1QsS0FBSyxFQUFFLEVBQUU7S0FDVixFQUFpQixDQUFDO0FBQ3JCLENBQUM7Ozs7O0FBRUQsTUFBTSxVQUFVLFlBQVksQ0FBQyxTQUFnQjtJQUMzQyxPQUFPLENBQUMsQ0FBQyxHQUFHLENBQUMsU0FBUzs7OztJQUFFLFVBQUMsSUFBSTtRQUMzQixPQUFPLG1CQUFBO1lBQ0wsRUFBRSxFQUFFLElBQUksQ0FBQyxFQUFFO1lBQ1gsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1lBQ2pCLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSTtZQUNmLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSTtTQUNoQixFQUFnQixDQUFDO0lBQ3BCLENBQUMsRUFBQyxDQUFDO0FBQ0wsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIF8gZnJvbSAnbG9kYXNoJztcbmltcG9ydCB7SUR9IGZyb20gJ0BkYXRvcmFtYS9ha2l0YSc7XG5cbmV4cG9ydCBpbnRlcmZhY2UgSXRlbVBlc3F1aXNhIHtcbiAgaWQ6IElEO1xuICB0aXRsZTogc3RyaW5nO1xuICBpY29uPzogc3RyaW5nO1xuICB0YWdzOiBzdHJpbmdbXTtcbiAgbGluazogc3RyaW5nO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIEdydXBvUGVzcXVpc2Ege1xuICBpZDogSUQ7XG4gIHRpdGxlOiBzdHJpbmc7XG4gIGljb24/OiBzdHJpbmc7XG4gIGlzSGVhZGVyPzogYm9vbGVhbjtcbiAgaXNBdmlzbz86IGJvb2xlYW47XG4gIGl0ZW1zOiBJdGVtUGVzcXVpc2FbXSB8IEdydXBvUGVzcXVpc2FbXTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGNyaWFyTm92b0dydXBvKCkge1xuICByZXR1cm4ge1xuICAgIHRpdGxlOiAnJyxcbiAgICBpdGVtczogW11cbiAgfSBhcyBHcnVwb1Blc3F1aXNhO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gcG9wdWxhckl0ZW5zKGl0ZW1QYXJhbTogYW55W10pIHtcbiAgcmV0dXJuIF8ubWFwKGl0ZW1QYXJhbSwgKGl0ZW0pID0+IHtcbiAgICByZXR1cm4ge1xuICAgICAgaWQ6IGl0ZW0uaWQsXG4gICAgICB0aXRsZTogaXRlbS50aXRsZSxcbiAgICAgIHRhZ3M6IGl0ZW0udGFncyxcbiAgICAgIGxpbms6IGl0ZW0ubGlua1xuICAgIH0gYXMgSXRlbVBlc3F1aXNhO1xuICB9KTtcbn1cbiJdfQ==