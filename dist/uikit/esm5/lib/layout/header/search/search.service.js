/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Inject, Injectable, Optional } from '@angular/core';
import { combineLatest, of } from 'rxjs';
import { SEARCH_TOKEN } from '../../../utils/abstract/search-abstract.service';
import * as i0 from "@angular/core";
import * as i1 from "../../../utils/abstract/search-abstract.service";
var SearchService = /** @class */ (function () {
    function SearchService(services) {
        this.services = services;
    }
    /**
     * @param {?} term
     * @return {?}
     */
    SearchService.prototype.buscar = /**
     * @param {?} term
     * @return {?}
     */
    function (term) {
        if (!this.services) {
            console.error('Não existe serviço de busca configurado! Ler documentação para mais informações, de como utilizar.');
            return of();
        }
        /** @type {?} */
        var searchObservables = this.services.map((/**
         * @param {?} searchService
         * @return {?}
         */
        function (searchService) { return searchService.searchGroup(term); }));
        /** @type {?} */
        var combinedObservable = combineLatest(searchObservables);
        return combinedObservable;
    };
    /**
     * @param {?} group
     * @return {?}
     */
    SearchService.prototype.adicionarGrupoPesquisa = /**
     * @param {?} group
     * @return {?}
     */
    function (group) {
        this.services.map((/**
         * @param {?} groupSearch
         * @return {?}
         */
        function (groupSearch) { return groupSearch.addGroup(group); }));
    };
    SearchService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    SearchService.ctorParameters = function () { return [
        { type: Array, decorators: [{ type: Optional }, { type: Inject, args: [SEARCH_TOKEN,] }] }
    ]; };
    /** @nocollapse */ SearchService.ngInjectableDef = i0.defineInjectable({ factory: function SearchService_Factory() { return new SearchService(i0.inject(i1.SEARCH_TOKEN, 8)); }, token: SearchService, providedIn: "root" });
    return SearchService;
}());
export { SearchService };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    SearchService.prototype.services;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL2xheW91dC9oZWFkZXIvc2VhcmNoL3NlYXJjaC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDN0QsT0FBTyxFQUFFLGFBQWEsRUFBYyxFQUFFLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFFckQsT0FBTyxFQUFFLFlBQVksRUFBeUIsTUFBTSxpREFBaUQsQ0FBQzs7O0FBRXRHO0lBS0UsdUJBQXdELFFBQWlDO1FBQWpDLGFBQVEsR0FBUixRQUFRLENBQXlCO0lBRXpGLENBQUM7Ozs7O0lBRUQsOEJBQU07Ozs7SUFBTixVQUFPLElBQVk7UUFFakIsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDbEIsT0FBTyxDQUFDLEtBQUssQ0FBQyxvR0FBb0csQ0FBQyxDQUFDO1lBQ3BILE9BQU8sRUFBRSxFQUFFLENBQUM7U0FDYjs7WUFFSyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUc7Ozs7UUFBQyxVQUFBLGFBQWEsSUFBSSxPQUFBLGFBQWEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEVBQS9CLENBQStCLEVBQUM7O1lBQ3ZGLGtCQUFrQixHQUFHLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQztRQUMzRCxPQUFPLGtCQUFrQixDQUFDO0lBQzVCLENBQUM7Ozs7O0lBRUQsOENBQXNCOzs7O0lBQXRCLFVBQXVCLEtBQXNCO1FBQzNDLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRzs7OztRQUFDLFVBQUEsV0FBVyxJQUFJLE9BQUEsV0FBVyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsRUFBM0IsQ0FBMkIsRUFBQyxDQUFDO0lBQ2hFLENBQUM7O2dCQXZCRixVQUFVLFNBQUM7b0JBQ1YsVUFBVSxFQUFFLE1BQU07aUJBQ25COzs7OzRDQUdjLFFBQVEsWUFBSSxNQUFNLFNBQUMsWUFBWTs7O3dCQVY5QztDQTZCQyxBQXhCRCxJQXdCQztTQXJCWSxhQUFhOzs7Ozs7SUFFWixpQ0FBNkUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3QsIEluamVjdGFibGUsIE9wdGlvbmFsIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBjb21iaW5lTGF0ZXN0LCBPYnNlcnZhYmxlLCBvZiB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgR3J1cG9QZXNxdWlzYSwgSXRlbVBlc3F1aXNhIH0gZnJvbSAnLi9zZWFyY2gubW9kZWwnO1xuaW1wb3J0IHsgU0VBUkNIX1RPS0VOLCBTZWFyY2hBYnN0cmFjdFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi91dGlscy9hYnN0cmFjdC9zZWFyY2gtYWJzdHJhY3Quc2VydmljZSc7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIFNlYXJjaFNlcnZpY2Uge1xuXG4gIGNvbnN0cnVjdG9yKEBPcHRpb25hbCgpIEBJbmplY3QoU0VBUkNIX1RPS0VOKSBwcm90ZWN0ZWQgc2VydmljZXM6IFNlYXJjaEFic3RyYWN0U2VydmljZVtdKSB7XG5cbiAgfVxuXG4gIGJ1c2Nhcih0ZXJtOiBzdHJpbmcpOiBPYnNlcnZhYmxlPChHcnVwb1Blc3F1aXNhIHwgSXRlbVBlc3F1aXNhKVtdPiB7XG5cbiAgICBpZiAoIXRoaXMuc2VydmljZXMpIHtcbiAgICAgIGNvbnNvbGUuZXJyb3IoJ07Do28gZXhpc3RlIHNlcnZpw6dvIGRlIGJ1c2NhIGNvbmZpZ3VyYWRvISBMZXIgZG9jdW1lbnRhw6fDo28gcGFyYSBtYWlzIGluZm9ybWHDp8O1ZXMsIGRlIGNvbW8gdXRpbGl6YXIuJyk7XG4gICAgICByZXR1cm4gb2YoKTtcbiAgICB9XG5cbiAgICBjb25zdCBzZWFyY2hPYnNlcnZhYmxlcyA9IHRoaXMuc2VydmljZXMubWFwKHNlYXJjaFNlcnZpY2UgPT4gc2VhcmNoU2VydmljZS5zZWFyY2hHcm91cCh0ZXJtKSk7XG4gICAgY29uc3QgY29tYmluZWRPYnNlcnZhYmxlID0gY29tYmluZUxhdGVzdChzZWFyY2hPYnNlcnZhYmxlcyk7XG4gICAgcmV0dXJuIGNvbWJpbmVkT2JzZXJ2YWJsZTtcbiAgfVxuXG4gIGFkaWNpb25hckdydXBvUGVzcXVpc2EoZ3JvdXA6IEdydXBvUGVzcXVpc2FbXSkge1xuICAgIHRoaXMuc2VydmljZXMubWFwKGdyb3VwU2VhcmNoID0+IGdyb3VwU2VhcmNoLmFkZEdyb3VwKGdyb3VwKSk7XG4gIH1cbn1cbiJdfQ==