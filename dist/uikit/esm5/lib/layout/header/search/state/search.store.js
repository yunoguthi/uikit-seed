/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { EntityStore, StoreConfig, transaction } from '@datorama/akita';
import * as i0 from "@angular/core";
/**
 * @record
 */
export function SearchState() { }
var SearchStore = /** @class */ (function (_super) {
    tslib_1.__extends(SearchStore, _super);
    function SearchStore() {
        return _super.call(this) || this;
    }
    /**
     * @param {?} valueJaSetado
     * @param {?} groupItem
     * @return {?}
     */
    SearchStore.prototype.adicionarGrupoPesquisa = /**
     * @param {?} valueJaSetado
     * @param {?} groupItem
     * @return {?}
     */
    function (valueJaSetado, groupItem) {
        // valueJaSetado.push(groupItem);
        /** @type {?} */
        var item = tslib_1.__spread(valueJaSetado, groupItem);
        this.set(item);
    };
    /**
     * @param {?} groupItem
     * @return {?}
     */
    SearchStore.prototype.addGroupBase = /**
     * @param {?} groupItem
     * @return {?}
     */
    function (groupItem) {
        this.set(groupItem);
    };
    /**
     * @param {?} title
     * @return {?}
     */
    SearchStore.prototype.removerGrupoPesquisa = /**
     * @param {?} title
     * @return {?}
     */
    function (title) {
        this.remove(title);
    };
    SearchStore.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    SearchStore.ctorParameters = function () { return []; };
    /** @nocollapse */ SearchStore.ngInjectableDef = i0.defineInjectable({ factory: function SearchStore_Factory() { return new SearchStore(); }, token: SearchStore, providedIn: "root" });
    tslib_1.__decorate([
        transaction(),
        tslib_1.__metadata("design:type", Function),
        tslib_1.__metadata("design:paramtypes", [Object, Object]),
        tslib_1.__metadata("design:returntype", void 0)
    ], SearchStore.prototype, "adicionarGrupoPesquisa", null);
    tslib_1.__decorate([
        transaction(),
        tslib_1.__metadata("design:type", Function),
        tslib_1.__metadata("design:paramtypes", [Object]),
        tslib_1.__metadata("design:returntype", void 0)
    ], SearchStore.prototype, "addGroupBase", null);
    tslib_1.__decorate([
        transaction(),
        tslib_1.__metadata("design:type", Function),
        tslib_1.__metadata("design:paramtypes", [String]),
        tslib_1.__metadata("design:returntype", void 0)
    ], SearchStore.prototype, "removerGrupoPesquisa", null);
    SearchStore = tslib_1.__decorate([
        StoreConfig({
            name: 'search'
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], SearchStore);
    return SearchStore;
}(EntityStore));
export { SearchStore };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLnN0b3JlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9sYXlvdXQvaGVhZGVyL3NlYXJjaC9zdGF0ZS9zZWFyY2guc3RvcmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFDQSxPQUFPLEVBQUMsVUFBVSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBYyxXQUFXLEVBQUUsV0FBVyxFQUFFLFdBQVcsRUFBQyxNQUFNLGlCQUFpQixDQUFDOzs7OztBQUVuRixpQ0FDQzs7SUFRZ0MsdUNBQXVDO0lBQ3RFO2VBQ0UsaUJBQU87SUFDVCxDQUFDOzs7Ozs7SUFHRCw0Q0FBc0I7Ozs7O0lBQXRCLFVBQXVCLGFBQWEsRUFBRSxTQUFTOzs7WUFFdkMsSUFBSSxvQkFBTyxhQUFhLEVBQUssU0FBUyxDQUFDO1FBQzdDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDakIsQ0FBQzs7Ozs7SUFHRCxrQ0FBWTs7OztJQUFaLFVBQWEsU0FBUztRQUNwQixJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQ3RCLENBQUM7Ozs7O0lBR0QsMENBQW9COzs7O0lBQXBCLFVBQXFCLEtBQWE7UUFDaEMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNyQixDQUFDOztnQkExQkYsVUFBVSxTQUFDO29CQUNWLFVBQVUsRUFBRSxNQUFNO2lCQUNuQjs7Ozs7SUFVQztRQURDLFdBQVcsRUFBRTs7Ozs2REFLYjtJQUdEO1FBREMsV0FBVyxFQUFFOzs7O21EQUdiO0lBR0Q7UUFEQyxXQUFXLEVBQUU7Ozs7MkRBR2I7SUFwQlUsV0FBVztRQUh2QixXQUFXLENBQUM7WUFDWCxJQUFJLEVBQUUsUUFBUTtTQUNmLENBQUM7O09BQ1csV0FBVyxDQXFCdkI7c0JBbENEO0NBa0NDLENBckJnQyxXQUFXLEdBcUIzQztTQXJCWSxXQUFXIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtHcnVwb1Blc3F1aXNhfSBmcm9tICcuLi9zZWFyY2gubW9kZWwnO1xuaW1wb3J0IHtJbmplY3RhYmxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7RW50aXR5U3RhdGUsIEVudGl0eVN0b3JlLCBTdG9yZUNvbmZpZywgdHJhbnNhY3Rpb259IGZyb20gJ0BkYXRvcmFtYS9ha2l0YSc7XG5cbmV4cG9ydCBpbnRlcmZhY2UgU2VhcmNoU3RhdGUgZXh0ZW5kcyBFbnRpdHlTdGF0ZTxHcnVwb1Blc3F1aXNhPiB7XG59XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuQFN0b3JlQ29uZmlnKHtcbiAgbmFtZTogJ3NlYXJjaCdcbn0pXG5leHBvcnQgY2xhc3MgU2VhcmNoU3RvcmUgZXh0ZW5kcyBFbnRpdHlTdG9yZTxTZWFyY2hTdGF0ZSwgR3J1cG9QZXNxdWlzYT4ge1xuICBjb25zdHJ1Y3RvcigpIHtcbiAgICBzdXBlcigpO1xuICB9XG5cbiAgQHRyYW5zYWN0aW9uKClcbiAgYWRpY2lvbmFyR3J1cG9QZXNxdWlzYSh2YWx1ZUphU2V0YWRvLCBncm91cEl0ZW0pIHtcbiAgICAvLyB2YWx1ZUphU2V0YWRvLnB1c2goZ3JvdXBJdGVtKTtcbiAgICBjb25zdCBpdGVtID0gWy4uLnZhbHVlSmFTZXRhZG8sIC4uLmdyb3VwSXRlbV07XG4gICAgdGhpcy5zZXQoaXRlbSk7XG4gIH1cblxuICBAdHJhbnNhY3Rpb24oKVxuICBhZGRHcm91cEJhc2UoZ3JvdXBJdGVtKSB7XG4gICAgdGhpcy5zZXQoZ3JvdXBJdGVtKTtcbiAgfVxuXG4gIEB0cmFuc2FjdGlvbigpXG4gIHJlbW92ZXJHcnVwb1Blc3F1aXNhKHRpdGxlOiBzdHJpbmcpIHtcbiAgICB0aGlzLnJlbW92ZSh0aXRsZSk7XG4gIH1cbn1cbiJdfQ==