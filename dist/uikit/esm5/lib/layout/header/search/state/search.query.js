/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { SearchStore } from './search.store';
import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import * as i0 from "@angular/core";
import * as i1 from "./search.store";
var SearchQuery = /** @class */ (function (_super) {
    tslib_1.__extends(SearchQuery, _super);
    function SearchQuery(store) {
        var _this = _super.call(this, store) || this;
        _this.store = store;
        return _this;
    }
    SearchQuery.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    SearchQuery.ctorParameters = function () { return [
        { type: SearchStore }
    ]; };
    /** @nocollapse */ SearchQuery.ngInjectableDef = i0.defineInjectable({ factory: function SearchQuery_Factory() { return new SearchQuery(i0.inject(i1.SearchStore)); }, token: SearchQuery, providedIn: "root" });
    return SearchQuery;
}(QueryEntity));
export { SearchQuery };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    SearchQuery.prototype.store;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLnF1ZXJ5LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9sYXlvdXQvaGVhZGVyL3NlYXJjaC9zdGF0ZS9zZWFyY2gucXVlcnkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQWMsV0FBVyxFQUFDLE1BQU0sZ0JBQWdCLENBQUM7QUFHeEQsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUMsV0FBVyxFQUFDLE1BQU0saUJBQWlCLENBQUM7OztBQUU1QztJQUdpQyx1Q0FBdUM7SUFDdEUscUJBQXNCLEtBQWtCO1FBQXhDLFlBQ0Usa0JBQU0sS0FBSyxDQUFDLFNBQ2I7UUFGcUIsV0FBSyxHQUFMLEtBQUssQ0FBYTs7SUFFeEMsQ0FBQzs7Z0JBTkYsVUFBVSxTQUFDO29CQUNWLFVBQVUsRUFBRSxNQUFNO2lCQUNuQjs7OztnQkFSb0IsV0FBVzs7O3NCQUFoQztDQTZCQyxBQXZCRCxDQUdpQyxXQUFXLEdBb0IzQztTQXBCWSxXQUFXOzs7Ozs7SUFDViw0QkFBNEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1NlYXJjaFN0YXRlLCBTZWFyY2hTdG9yZX0gZnJvbSAnLi9zZWFyY2guc3RvcmUnO1xuaW1wb3J0IHtHcnVwb1Blc3F1aXNhfSBmcm9tICcuLi9zZWFyY2gubW9kZWwnO1xuaW1wb3J0IHttYXB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcbmltcG9ydCB7SW5qZWN0YWJsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge1F1ZXJ5RW50aXR5fSBmcm9tICdAZGF0b3JhbWEvYWtpdGEnO1xuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBTZWFyY2hRdWVyeSBleHRlbmRzIFF1ZXJ5RW50aXR5PFNlYXJjaFN0YXRlLCBHcnVwb1Blc3F1aXNhPiB7XG4gIGNvbnN0cnVjdG9yKHByb3RlY3RlZCBzdG9yZTogU2VhcmNoU3RvcmUpIHtcbiAgICBzdXBlcihzdG9yZSk7XG4gIH1cblxuICAvKnNlYXJjaEdyb3VwRGF0YSQgPSB0aGlzLnNlbGVjdEFsbCgpLnBpcGUoXG4gICAgbWFwKHRoaXMuZ2V0R3J1cG9QZXNxdWlzYURhdGEuYmluZCh0aGlzKSlcbiAgKTtcblxuXG4gIGdldEdydXBvUGVzcXVpc2FEYXRhKHN0dWRlbnRzOiBBcnJheTxHcnVwb1Blc3F1aXNhPik6IHsgW2tleTogc3RyaW5nXTogQXJyYXk8R3J1cG9QZXNxdWlzYT4gfSB7XG4gICAgcmV0dXJuIHN0dWRlbnRzLnJlZHVjZSgoXG4gICAgICB7IHRpdGxlOiBuQXJyYXksIGl0ZW1zOiBxQXJyYXl9LFxuICAgICAgeyB0aXRsZSwgaXRlbXMgfSkgPT4ge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgdGl0bGU6IFsuLi5uQXJyYXksIHRpdGxlXSxcbiAgICAgICAgaXRlbXM6IFsuLi5xQXJyYXksIGl0ZW1zXVxuICAgICAgfTtcbiAgICB9LCB7IHRpdGxlOiBbXSwgaXRlbXM6IFtdfSk7XG4gIH0qL1xufVxuIl19