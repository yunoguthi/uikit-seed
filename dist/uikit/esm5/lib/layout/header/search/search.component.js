/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { ChangeDetectionStrategy, Component, ContentChildren, EventEmitter, Input, Output, QueryList, ViewChild, ViewChildren, Renderer2 } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MenuItemComponent } from '../../nav/menu/menu-item/menu-item.component';
import { SearchService } from './search.service';
import { Hotkey, HotkeysService } from 'angular2-hotkeys';
import { MatInput, MatButton } from '@angular/material';
import { Router } from '@angular/router';
import { searchAnimation } from './slide-animations';
import { SearchHistoricoService } from './search-historico.service';
import * as _ from 'lodash';
import { guid } from '@datorama/akita';
import { timer, Subscription } from 'rxjs';
import { ActiveDescendantKeyManager } from '@angular/cdk/a11y';
import { ENTER, ESCAPE, UP_ARROW, DOWN_ARROW } from '@angular/cdk/keycodes';
import { HighlightComponent } from '../../nav/highlight/highlight.component';
var SearchComponent = /** @class */ (function () {
    function SearchComponent(fb, searchService, hotkeysService, router, searchHistorico, renderer) {
        this.fb = fb;
        this.searchService = searchService;
        this.hotkeysService = hotkeysService;
        this.router = router;
        this.searchHistorico = searchHistorico;
        this.renderer = renderer;
        this.adicionarGrupo = new EventEmitter();
        this.removerGrupo = new EventEmitter();
        this.placeholderText = 'Pesquisa (Ctrl + Alt + 3)';
        this.stateForm = this.fb.group({
            searchGroups: '',
        });
        this.searchGroups = [];
        this.subscription = new Subscription();
        this.openSearch = false;
        this.isAnimating = false;
    }
    Object.defineProperty(SearchComponent.prototype, "hasResults", {
        get: /**
         * @return {?}
         */
        function () {
            return this.openSearch && this.searchGroupOptions.length;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    SearchComponent.prototype.ngAfterViewInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (document.body.getBoundingClientRect().width <= 970) {
            this.placeholderText = 'Pesquisa';
        }
        this.hotkeysService.add(new Hotkey('ctrl+alt+3', (/**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            _this.searchOpen.focus();
            return false; // Prevent bubbling
        })));
        timer(1000).subscribe((/**
         * @return {?}
         */
        function () {
            _this.searchOpen.focus();
        }));
        this.keyManager = new ActiveDescendantKeyManager(this.items).withWrap().withTypeAhead();
        this.renderer.setStyle(this.buttonClose._elementRef.nativeElement, 'display', 'none');
    };
    /**
     * @return {?}
     */
    SearchComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.stateForm.get('searchGroups').valueChanges.subscribe((/**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            _this.itemPesquisado = value;
            _this.searchGroupOptions = [];
            _this.searchService.buscar(value)
                .subscribe((/**
             * @param {?} itensMenuPesquisa
             * @return {?}
             */
            function (itensMenuPesquisa) {
                if (itensMenuPesquisa && itensMenuPesquisa.length > 0) {
                    _.map(itensMenuPesquisa, (/**
                     * @param {?} item
                     * @return {?}
                     */
                    function (item) { return _this.searchGroupOptions.push(item); }));
                }
                else {
                    _this.searchGroupOptions.push((/** @type {?} */ ({
                        id: guid(),
                        isAviso: true,
                        title: "N\u00E3o encontramos '" + value + "'"
                    })));
                }
            }));
        }));
        this.searchGroupOptions = this.searchHistorico.obterHistorico();
    };
    /**
     * @param {?} event
     * @param {?} searchContainer
     * @return {?}
     */
    SearchComponent.prototype.openToSearch = /**
     * @param {?} event
     * @param {?} searchContainer
     * @return {?}
     */
    function (event, searchContainer) {
        if (!this.searchOpened
            && event
            && event.code === 'KeyA'
            && event.key.length > 0) {
            this.searchOpened = true;
            this.open(searchContainer);
        }
    };
    /**
     * @param {?} searchContainer
     * @return {?}
     */
    SearchComponent.prototype.onBlur = /**
     * @param {?} searchContainer
     * @return {?}
     */
    function (searchContainer) {
        var _this = this;
        this.subscription.add(timer(150).subscribe((/**
         * @return {?}
         */
        function () {
            if (!_this.searchOpen.focused) {
                _this.close();
                _this.renderer.removeAttribute(searchContainer, 'opened');
                _this.renderer.setStyle(_this.buttonClose._elementRef.nativeElement, 'display', 'none');
            }
        })));
    };
    /**
     * @return {?}
     */
    SearchComponent.prototype.closeToSearch = /**
     * @return {?}
     */
    function () {
        if (!this.searchOpen.focused) {
            this.close();
        }
    };
    /**
     * @param {?} searchContainer
     * @return {?}
     */
    SearchComponent.prototype.open = /**
     * @param {?} searchContainer
     * @return {?}
     */
    function (searchContainer) {
        this.searchOpen.focus();
        this.renderer.setAttribute(searchContainer, 'opened', '');
        this.renderer.removeStyle(this.buttonClose._elementRef.nativeElement, 'display');
    };
    /**
     * @return {?}
     */
    SearchComponent.prototype.close = /**
     * @return {?}
     */
    function () {
        this.searchOpened = false;
        ((/** @type {?} */ (document.activeElement))).blur();
        this.stateForm.controls.searchGroups.setValue('');
    };
    /**
     * @return {?}
     */
    SearchComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this.subscription.unsubscribe();
    };
    /**
     * @param {?} searchContainer
     * @return {?}
     */
    SearchComponent.prototype.toggle = /**
     * @param {?} searchContainer
     * @return {?}
     */
    function (searchContainer) {
        this.searchOpened = (!this.searchOpened);
        if (this.searchOpened) {
            this.open(searchContainer);
        }
        this.stateForm.controls.searchGroups.setValue('');
    };
    /**
     * @param {?} item
     * @param {?} group
     * @return {?}
     */
    SearchComponent.prototype.goToAddHistoric = /**
     * @param {?} item
     * @param {?} group
     * @return {?}
     */
    function (item, group) {
        this.searchHistorico.salvarHistorico(group);
        this.router.navigate([item.link[0]]);
        this.closeToSearch();
    };
    Object.defineProperty(SearchComponent.prototype, "heightSize", {
        get: /**
         * @return {?}
         */
        function () {
            return null;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} event
     * @return {?}
     */
    SearchComponent.prototype.onKeyup = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        switch (event.keyCode) {
            case ENTER:
                /** @type {?} */
                var item = this.keyManager.activeItem.item;
                this.goToAddHistoric(item, item);
                break;
            case ESCAPE:
                this.close();
                break;
            case UP_ARROW:
            case DOWN_ARROW:
                this.keyManager.onKeydown(event);
                this.activeItem(this.keyManager.activeItem.item, event.keyCode === UP_ARROW);
                break;
            default:
                this.keyManager.setFirstItemActive();
                this.activeItem(this.keyManager.activeItem.item, false);
                break;
        }
    };
    /**
     * @param {?} item
     * @param {?} up
     * @return {?}
     */
    SearchComponent.prototype.activeItem = /**
     * @param {?} item
     * @param {?} up
     * @return {?}
     */
    function (item, up) {
        if ((/** @type {?} */ (item)) && ((/** @type {?} */ (item))).items && ((/** @type {?} */ (item))).items.length > 0) {
            if (up) {
                this.keyManager.setPreviousItemActive();
            }
            else {
                this.keyManager.setNextItemActive();
            }
            this.activeItem(this.keyManager.activeItem.item, up);
        }
    };
    /**
     * @param {?} item
     * @return {?}
     */
    SearchComponent.prototype.onMouseoverItem = /**
     * @param {?} item
     * @return {?}
     */
    function (item) {
        this.keyManager.setActiveItem(item);
    };
    SearchComponent.decorators = [
        { type: Component, args: [{
                    selector: 'uikit-search',
                    template: "<form [formGroup]=\"stateForm\" [class.is-opened]=\"searchOpened\" autocomplete=\"off\" >\n  <div search-container #searchContainer [attr.animating]=\"isAnimating ? '' : null\"\n    [attr.with-results]=\"hasResults ? '' : null\">\n\n    <button mat-icon-button class=\"btn-back\" (click)=\"toggle(searchContainer)\">\n      <i class='fa fa-search icon-search' search-icon></i>\n    </button>\n    <input tabindex=\"3\" matInput (keypress)=\"openToSearch($event,searchContainer)\" #searchOpen=\"matInput\"\n      placeholder=\"Pesquisa (Ctrl + Alt + 3)\" placeholder=\"{{placeholderText}}\" formControlName=\"searchGroups\"\n      (keyup)=\"onKeyup($event)\" (blur)=\"onBlur(searchContainer)\" />\n    <button mat-icon-button class=\"icon-sobre\" #buttonClose=\"matButton\" (click)=\"searchGroupOptions = []; toggle(searchContainer)\">\n      <i class='fa fa-times icon-search' search-icon></i>\n    </button>\n\n    <div resultados-container [ngStyle]='{ height: heightSize }'>\n      <div *ngFor='let groupMenu of searchGroupOptions' [attr.header]='groupMenu.isHeader && !groupMenu.isAviso'>\n        <div group-header *ngIf='!groupMenu.isAviso'>\n          <span>{{groupMenu.title}}</span>\n        </div>\n        <ng-template [ngTemplateOutlet]=\"conteudoMenu\" [ngTemplateOutletContext]=\"{ $implicit: groupMenu }\">\n        </ng-template>\n\n      </div>\n    </div>\n  </div>\n</form>\n\n<ng-template #conteudoMenu let-list>\n  <ul name=\"listaMenus\">\n    <uikit-highlight #searchitem *ngFor='let item of list.items' [attr.header]='item.isHeader && !item.isAviso' [item]=\"item\">\n      <div class=\"search-item-title\">\n        <div group-header *ngIf='item.isHeader && !item.isAviso'>\n          <span>\n            <i class=\"first-letter\" [attr.data-first]=\"item.title.substr(0, 1)\" *ngIf='!item.icon'> </i>\n            <i class='{{item.icon}}' *ngIf='item.icon'></i>\n            {{item.title}}\n          </span>\n        </div>\n\n        <a resultado (mouseover)=\"onMouseoverItem(searchitem)\" (click)='goToAddHistoric(item, item);$event.preventDefault()' href='#'\n          *ngIf='!item.isHeader && !item.isAviso'>\n          <span innerHTML='{{item.title | highlight: itemPesquisado}}'></span>\n        </a>\n\n        <ul *ngIf=\"item.items?.length > 0\">\n          <ng-container *ngTemplateOutlet=\"conteudoMenu; context:{ $implicit: item }\"></ng-container>\n        </ul>\n\n        <div aviso *ngIf='item.isAviso'>\n          {{item.title}}\n        </div>\n      </div>\n    </uikit-highlight>\n  </ul>\n</ng-template>\n\n<div hidden>\n  <ng-content select=\"[search]\"></ng-content>\n</div>\n",
                    animations: [searchAnimation()],
                    changeDetection: ChangeDetectionStrategy.OnPush,
                    styles: ["@media screen and (max-width:970px){.btn-back{right:0!important;top:5px}[search-container]{position:relative!important;-webkit-transition:.1s ease-in-out!important;transition:.1s ease-in-out!important;right:0!important}[search-container] [search-container]{width:40px}[search-container] i[search-icon]{-webkit-transition:.1s ease-in-out,none;transition:.1s ease-in-out,none;width:40px;height:40px;line-height:40px;top:0;right:0;z-index:2;font-size:15px;color:#004bcb;text-align:center}[search-container] input{min-width:300px!important;max-width:970px!important;width:100%!important;color:#004bcb;position:fixed;top:0}[search-container]:not([opened]) input{margin-top:-100%!important}[search-container]:not([opened]) i{border-left:0!important}[search-container][opened]{right:0!important}[search-container][opened] input{min-width:300px!important;max-width:970px!important;width:100%!important;border-radius:0!important;height:63px!important;left:0;right:0;position:fixed;background:#f5f5f5;border-bottom:1px solid #dce4ec;top:0}[search-container][opened] [resultados-container]{left:0;margin-left:0!important;width:100%!important;min-width:300px!important}[search-container][opened] .fa-search{display:none}[search-container][opened] .icon-sobre{right:20px;position:fixed;color:#004bcb}}[search-container]{display:block;border-radius:0;z-index:1099;-webkit-user-select:none;right:170px;top:0}[search-container] input::-webkit-input-placeholder{color:#004bcb}[search-container] input:-moz-placeholder{color:#004bcb}[search-container] input::-moz-placeholder{color:#004bcb}[search-container] input:-ms-input-placeholder{color:#004bcb}[search-container] input{font-size:13px;color:#004bcb;padding:0 10px 0 40px;background:#ecf2fe;border-radius:5px;box-sizing:border-box;line-height:34px;font-weight:400;outline:0;border:none;border-bottom:2px solid #004bcb;width:250px;margin-bottom:3px;height:45px;-webkit-transition:.1s ease-in-out,none,.15s linear;transition:.1s ease-in-out,none,.15s linear}[search-container] i[search-icon]{-webkit-transition:.1s ease-in-out,none;transition:.1s ease-in-out,none;width:40px;height:40px;line-height:40px;right:0;position:absolute;top:0;z-index:2;font-size:15px;color:#004bcb;text-align:center}[search-container] .btn-back{right:-40px;top:5px}[search-container] button[clear-button]{top:7px;height:56px;width:39px;line-height:54px;position:absolute;right:0;z-index:3;border:none;visibility:hidden;background:0 0!important}[search-container] button[clear-button]:hover{box-shadow:inset 0 -56px 0 #003795}[search-container] button[clear-button] i{font-size:15px;font-weight:100}[search-container] [resultados-container]{visibility:hidden;display:none;height:0;position:fixed;padding-bottom:0}[search-container] div,[search-container] ul{padding:0 10px 1px;margin:0;list-style:none}[search-container] div div,[search-container] div li,[search-container] ul div,[search-container] ul li{line-height:1}[search-container] div div:not([header]):first-child::before,[search-container] div div[header]+div::before,[search-container] div div[header]+li::before,[search-container] div li:not([header]):first-child::before,[search-container] div li[header]+div::before,[search-container] div li[header]+li::before,[search-container] ul div:not([header]):first-child::before,[search-container] ul div[header]+div::before,[search-container] ul div[header]+li::before,[search-container] ul li:not([header]):first-child::before,[search-container] ul li[header]+div::before,[search-container] ul li[header]+li::before{height:15px!important;top:0}[search-container] div div i,[search-container] div li i,[search-container] ul div i,[search-container] ul li i{color:#aaa;margin-left:-4px}[search-container] div div a[resultado],[search-container] div li a[resultado],[search-container] ul div a[resultado],[search-container] ul li a[resultado]{padding-left:22px;padding-right:22px;display:block;text-decoration:none}[search-container] div div:not([header]) a,[search-container] div li:not([header]) a,[search-container] ul div:not([header]) a,[search-container] ul li:not([header]) a{display:block}[search-container] div div:not([header])::before,[search-container] div li:not([header])::before,[search-container] ul div:not([header])::before,[search-container] ul li:not([header])::before{clear:both;content:'';border-left:1px dotted #788896;border-bottom:1px dotted #788896;display:block;float:left;margin:0 5px;height:25px;width:15px;position:relative;top:-11px}[search-container] div div:not([header]) span,[search-container] div li:not([header]) span,[search-container] ul div:not([header]) span,[search-container] ul li:not([header]) span{display:block;text-decoration:none;line-height:24px;color:#fff;outline:0;font-size:14px!important;cursor:pointer;-ms-user-select:none;-moz-user-select:none;-webkit-user-select:none;font-weight:400;padding:0 10px 0 1px;-webkit-transition:.1s ease-in-out;transition:.1s ease-in-out}[search-container] div div:not([header]) span strong,[search-container] div li:not([header]) span strong,[search-container] ul div:not([header]) span strong,[search-container] ul li:not([header]) span strong{font-size:14px!important;text-decoration:underline;font-weight:400!important}[search-container] div div[header],[search-container] div li[header],[search-container] ul div[header],[search-container] ul li[header]{margin-top:8px}[search-container] div div[header]>[group-header],[search-container] div li[header]>[group-header],[search-container] ul div[header]>[group-header],[search-container] ul li[header]>[group-header]{padding-left:1px;padding-bottom:1px}[search-container] div div[header]>[group-header] span,[search-container] div li[header]>[group-header] span,[search-container] ul div[header]>[group-header] span,[search-container] ul li[header]>[group-header] span{list-style-type:none;outline:0;padding:0;text-transform:uppercase;cursor:pointer;-ms-user-select:none;-moz-user-select:none;-webkit-user-select:none;font-weight:500;font-size:13px!important;padding-left:10px!important;color:rgba(255,255,255,.5);margin-top:-4px}[search-container] div div[header]>[group-header] span.zmdi,[search-container] div li[header]>[group-header] span.zmdi,[search-container] ul div[header]>[group-header] span.zmdi,[search-container] ul li[header]>[group-header] span.zmdi{position:relative;left:-3px}[search-container] div div[header]>[group-header] .first-letter,[search-container] div li[header]>[group-header] .first-letter,[search-container] ul div[header]>[group-header] .first-letter,[search-container] ul li[header]>[group-header] .first-letter{color:rgba(128,128,128,.5);width:23px;height:23px;padding-top:7px;display:block;text-align:center;float:left;-webkit-transition:.1s linear,none;transition:.1s linear,none;z-index:2;position:relative;font-style:normal;font-weight:700;margin-left:-14px;font-size:13px;background:#002e7c}[search-container] div div[header]>[group-header] .first-letter:before,[search-container] div li[header]>[group-header] .first-letter:before,[search-container] ul div[header]>[group-header] .first-letter:before,[search-container] ul li[header]>[group-header] .first-letter:before{content:attr(data-first);position:absolute;margin-left:-5px;margin-top:-3px}[search-container] div div [aviso],[search-container] div li [aviso],[search-container] ul div [aviso],[search-container] ul li [aviso]{font-size:14px!important;padding:7px 0}[search-container][opened]{margin-top:0}[search-container][opened] button[clear-button]{top:2px!important;visibility:visible}[search-container][opened] button[clear-button] i{color:#002e7c}[search-container][opened] [resultados-container]{background:#fff;border-right:1px solid #dce4ec;border-left:1px solid #dce4ec;border-bottom:1px solid #dce4ec;margin-top:5px;margin-left:40px;min-width:600px;visibility:visible;display:block;overflow:auto;height:auto;max-height:calc(100vh - 250px)}[search-container][opened] input{color:#004bcb;padding:15px 15px 15px 40px;width:600px;height:45px;line-height:34px;top:0;margin-top:0;margin-bottom:3px}[search-container][opened] input::-webkit-input-placeholder{color:#004bcb}[search-container][opened] input:-moz-placeholder{color:#004bcb}[search-container][opened] input::-moz-placeholder{color:#004bcb}[search-container][opened] input:-ms-input-placeholder{color:#004bcb}[search-container][opened] i.icon-search{top:-15px;height:67px;line-height:70px;color:#007bff}[search-container][opened] i.icon-search:hover{background:0 0}[search-container][opened] .icon-sobre{margin-left:-40px}@media all and (max-width:1024px){.is-horizontal-menu [search-container]{margin-left:6px;right:10px!important}.is-horizontal-menu [search-container] i{border:none!important;width:30px!important;font-size:13px!important}.is-horizontal-menu [search-container] input{border:none;background:#ecf2fe;width:30px;height:30px;text-align:center;border-radius:20px;position:relative;vertical-align:middle;line-height:30px;padding:0 30px 0 0;margin:3px 0 0;cursor:pointer}.is-horizontal-menu [search-container][opened]{width:auto}.is-horizontal-menu [search-container][opened] button[clear-button]{width:29px;padding:0}.is-horizontal-menu [search-container][opened] button[clear-button] i{top:-2px!important;position:relative;right:1px}.is-horizontal-menu [search-container][opened] input{width:600px!important;border-radius:0;text-align:left;padding-left:15px;padding-right:35px;color:#004bcb;cursor:auto!important}.is-horizontal-menu [search-container][opened] input::-webkit-input-placeholder{color:#004bcb}.is-horizontal-menu [search-container][opened] input:-moz-placeholder{color:#004bcb}.is-horizontal-menu [search-container][opened] input::-moz-placeholder{color:#004bcb}.is-horizontal-menu [search-container][opened] input:-ms-input-placeholder{color:#004bcb}}.btn-primary,[search-container] button[clear-button]{color:#fff;background-color:#007bff;border-color:#007bff}.btn-primary:hover,[search-container] button:hover[clear-button]{color:#fff;background-color:#0069d9;border-color:#0062cc}.btn-primary.focus,.btn-primary:focus,[search-container] button.focus[clear-button],[search-container] button:focus[clear-button]{box-shadow:0 0 0 .2rem rgba(0,123,255,.5)}.btn-primary.disabled,.btn-primary:disabled,[search-container] button.disabled[clear-button],[search-container] button:disabled[clear-button]{background-color:#007bff;border-color:#007bff}.btn-primary:not([disabled]):not(.disabled).active,.btn-primary:not([disabled]):not(.disabled):active,.show>.btn-primary.dropdown-toggle,[search-container] .show>button.dropdown-toggle[clear-button],[search-container] button:not([disabled]):not(.disabled).active[clear-button],[search-container] button:not([disabled]):not(.disabled):active[clear-button]{color:#fff;background-color:#0062cc;border-color:#005cbf;box-shadow:0 0 0 .2rem rgba(0,123,255,.5)}"]
                }] }
    ];
    /** @nocollapse */
    SearchComponent.ctorParameters = function () { return [
        { type: FormBuilder },
        { type: SearchService },
        { type: HotkeysService },
        { type: Router },
        { type: SearchHistoricoService },
        { type: Renderer2 }
    ]; };
    SearchComponent.propDecorators = {
        grupoPesquisado: [{ type: Input }],
        adicionarGrupo: [{ type: Output }],
        removerGrupo: [{ type: Output }],
        placeholderText: [{ type: Input }],
        children: [{ type: ContentChildren, args: [MenuItemComponent, { descendants: false },] }],
        items: [{ type: ViewChildren, args: [HighlightComponent,] }],
        searchInput: [{ type: ViewChild, args: ['search',] }],
        searchOpen: [{ type: ViewChild, args: ['searchOpen',] }],
        buttonClose: [{ type: ViewChild, args: ['buttonClose',] }]
    };
    return SearchComponent;
}());
export { SearchComponent };
if (false) {
    /** @type {?} */
    SearchComponent.prototype.grupoPesquisado;
    /** @type {?} */
    SearchComponent.prototype.adicionarGrupo;
    /** @type {?} */
    SearchComponent.prototype.removerGrupo;
    /** @type {?} */
    SearchComponent.prototype.placeholderText;
    /** @type {?} */
    SearchComponent.prototype.children;
    /** @type {?} */
    SearchComponent.prototype.items;
    /**
     * @type {?}
     * @private
     */
    SearchComponent.prototype.keyManager;
    /** @type {?} */
    SearchComponent.prototype.stateForm;
    /** @type {?} */
    SearchComponent.prototype.searchOpened;
    /** @type {?} */
    SearchComponent.prototype.searchGroups;
    /** @type {?} */
    SearchComponent.prototype.subscription;
    /** @type {?} */
    SearchComponent.prototype.searchGroupOptions;
    /** @type {?} */
    SearchComponent.prototype.highlight;
    /** @type {?} */
    SearchComponent.prototype.openSearch;
    /** @type {?} */
    SearchComponent.prototype.itemPesquisado;
    /** @type {?} */
    SearchComponent.prototype.searchInput;
    /** @type {?} */
    SearchComponent.prototype.searchOpen;
    /** @type {?} */
    SearchComponent.prototype.buttonClose;
    /** @type {?} */
    SearchComponent.prototype.isAnimating;
    /**
     * @type {?}
     * @private
     */
    SearchComponent.prototype.fb;
    /**
     * @type {?}
     * @protected
     */
    SearchComponent.prototype.searchService;
    /** @type {?} */
    SearchComponent.prototype.hotkeysService;
    /**
     * @type {?}
     * @protected
     */
    SearchComponent.prototype.router;
    /**
     * @type {?}
     * @private
     */
    SearchComponent.prototype.searchHistorico;
    /**
     * @type {?}
     * @protected
     */
    SearchComponent.prototype.renderer;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvbGF5b3V0L2hlYWRlci9zZWFyY2gvc2VhcmNoLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUVMLHVCQUF1QixFQUN2QixTQUFTLEVBQ1QsZUFBZSxFQUNmLFlBQVksRUFDWixLQUFLLEVBRUwsTUFBTSxFQUNOLFNBQVMsRUFDVCxTQUFTLEVBQ1QsWUFBWSxFQUNaLFNBQVMsRUFFVixNQUFNLGVBQWUsQ0FBQztBQUN2QixPQUFPLEVBQUMsV0FBVyxFQUFZLE1BQU0sZ0JBQWdCLENBQUM7QUFDdEQsT0FBTyxFQUFDLGlCQUFpQixFQUFDLE1BQU0sOENBQThDLENBQUM7QUFDL0UsT0FBTyxFQUFDLGFBQWEsRUFBQyxNQUFNLGtCQUFrQixDQUFDO0FBQy9DLE9BQU8sRUFBQyxNQUFNLEVBQUUsY0FBYyxFQUFDLE1BQU0sa0JBQWtCLENBQUM7QUFDeEQsT0FBTyxFQUFDLFFBQVEsRUFBRSxTQUFTLEVBQUMsTUFBTSxtQkFBbUIsQ0FBQztBQUN0RCxPQUFPLEVBQUMsTUFBTSxFQUFDLE1BQU0saUJBQWlCLENBQUM7QUFFdkMsT0FBTyxFQUFDLGVBQWUsRUFBQyxNQUFNLG9CQUFvQixDQUFDO0FBQ25ELE9BQU8sRUFBQyxzQkFBc0IsRUFBQyxNQUFNLDRCQUE0QixDQUFDO0FBQ2xFLE9BQU8sS0FBSyxDQUFDLE1BQU0sUUFBUSxDQUFDO0FBQzVCLE9BQU8sRUFBQyxJQUFJLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUNyQyxPQUFPLEVBQUMsS0FBSyxFQUFFLFlBQVksRUFBQyxNQUFNLE1BQU0sQ0FBQztBQUN6QyxPQUFPLEVBQUMsMEJBQTBCLEVBQUMsTUFBTSxtQkFBbUIsQ0FBQztBQUM3RCxPQUFPLEVBQUMsS0FBSyxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUUsVUFBVSxFQUFDLE1BQU0sdUJBQXVCLENBQUM7QUFDMUUsT0FBTyxFQUFDLGtCQUFrQixFQUFDLE1BQU0seUNBQXlDLENBQUM7QUFFM0U7SUF5Q0UseUJBQ1UsRUFBZSxFQUNiLGFBQTRCLEVBQy9CLGNBQThCLEVBQzNCLE1BQWMsRUFDaEIsZUFBdUMsRUFDckMsUUFBbUI7UUFMckIsT0FBRSxHQUFGLEVBQUUsQ0FBYTtRQUNiLGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBQy9CLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUMzQixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2hCLG9CQUFlLEdBQWYsZUFBZSxDQUF3QjtRQUNyQyxhQUFRLEdBQVIsUUFBUSxDQUFXO1FBdENyQixtQkFBYyxHQUFHLElBQUksWUFBWSxFQUFpQixDQUFDO1FBQ25ELGlCQUFZLEdBQUcsSUFBSSxZQUFZLEVBQWlCLENBQUM7UUFHM0Qsb0JBQWUsR0FBRywyQkFBMkIsQ0FBQztRQU05QyxjQUFTLEdBQWMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7WUFDbkMsWUFBWSxFQUFFLEVBQUU7U0FDakIsQ0FBQyxDQUFDO1FBR0gsaUJBQVksR0FBb0IsRUFBRSxDQUFDO1FBQ25DLGlCQUFZLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUlsQyxlQUFVLEdBQUcsS0FBSyxDQUFDO1FBTVosZ0JBQVcsR0FBRyxLQUFLLENBQUM7SUFjM0IsQ0FBQztJQVpELHNCQUFJLHVDQUFVOzs7O1FBQWQ7WUFDRSxPQUFPLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLE1BQU0sQ0FBQztRQUMzRCxDQUFDOzs7T0FBQTs7OztJQVlELHlDQUFlOzs7SUFBZjtRQUFBLGlCQWlCQztRQWZDLElBQUksUUFBUSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxDQUFDLEtBQUssSUFBSSxHQUFHLEVBQUU7WUFDdEQsSUFBSSxDQUFDLGVBQWUsR0FBRyxVQUFVLENBQUM7U0FDbkM7UUFFRCxJQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxZQUFZOzs7O1FBQUUsVUFBQyxLQUFvQjtZQUNwRSxLQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ3hCLE9BQU8sS0FBSyxDQUFDLENBQUMsbUJBQW1CO1FBQ25DLENBQUMsRUFBQyxDQUFDLENBQUM7UUFFSixLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsU0FBUzs7O1FBQUM7WUFDcEIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUMxQixDQUFDLEVBQUMsQ0FBQztRQUVILElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSwwQkFBMEIsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDeEYsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLFNBQVMsRUFBRSxNQUFNLENBQUMsQ0FBQztJQUN4RixDQUFDOzs7O0lBRUQsa0NBQVE7OztJQUFSO1FBQUEsaUJBc0JDO1FBckJDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxTQUFTOzs7O1FBQUMsVUFBQSxLQUFLO1lBQzdELEtBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO1lBQzVCLEtBQUksQ0FBQyxrQkFBa0IsR0FBRyxFQUFFLENBQUM7WUFDN0IsS0FBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDO2lCQUM3QixTQUFTOzs7O1lBQUMsVUFBQyxpQkFBaUI7Z0JBQ3pCLElBQUksaUJBQWlCLElBQUksaUJBQWlCLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtvQkFDckQsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUI7Ozs7b0JBQUUsVUFBQyxJQUFJLElBQUssT0FBQSxLQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFsQyxDQUFrQyxFQUFDLENBQUM7aUJBQ3hFO3FCQUFNO29CQUNMLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQzFCLG1CQUFBO3dCQUNFLEVBQUUsRUFBRSxJQUFJLEVBQUU7d0JBQ1YsT0FBTyxFQUFFLElBQUk7d0JBQ2IsS0FBSyxFQUFFLDJCQUFvQixLQUFLLE1BQUc7cUJBQ3BDLEVBQWlCLENBQ25CLENBQUM7aUJBQ0g7WUFDSCxDQUFDLEVBQ0YsQ0FBQztRQUNOLENBQUMsRUFBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsY0FBYyxFQUFFLENBQUM7SUFDbEUsQ0FBQzs7Ozs7O0lBRUQsc0NBQVk7Ozs7O0lBQVosVUFBYSxLQUFVLEVBQUUsZUFBb0I7UUFDM0MsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZO2VBQ2pCLEtBQUs7ZUFDTCxLQUFLLENBQUMsSUFBSSxLQUFLLE1BQU07ZUFDckIsS0FBSyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3pCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1lBQ3pCLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7U0FDNUI7SUFDSCxDQUFDOzs7OztJQUVELGdDQUFNOzs7O0lBQU4sVUFBTyxlQUFvQjtRQUEzQixpQkFRQztRQVBDLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxTQUFTOzs7UUFBQztZQUN6QyxJQUFJLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLEVBQUU7Z0JBQzVCLEtBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDYixLQUFJLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxlQUFlLEVBQUUsUUFBUSxDQUFDLENBQUM7Z0JBQ3pELEtBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxTQUFTLEVBQUUsTUFBTSxDQUFDLENBQUM7YUFDdkY7UUFDSCxDQUFDLEVBQUMsQ0FBQyxDQUFDO0lBQ04sQ0FBQzs7OztJQUVELHVDQUFhOzs7SUFBYjtRQUNFLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRTtZQUM1QixJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7U0FDZDtJQUNILENBQUM7Ozs7O0lBRUQsOEJBQUk7Ozs7SUFBSixVQUFLLGVBQW9CO1FBQ3ZCLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDeEIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsZUFBZSxFQUFFLFFBQVEsRUFBRSxFQUFFLENBQUMsQ0FBQztRQUMxRCxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsU0FBUyxDQUFDLENBQUM7SUFDbkYsQ0FBQzs7OztJQUVELCtCQUFLOzs7SUFBTDtRQUNFLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1FBQzFCLENBQUMsbUJBQUEsUUFBUSxDQUFDLGFBQWEsRUFBZSxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDL0MsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUNwRCxDQUFDOzs7O0lBRUQscUNBQVc7OztJQUFYO1FBQ0UsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUNsQyxDQUFDOzs7OztJQUVELGdDQUFNOzs7O0lBQU4sVUFBTyxlQUFvQjtRQUN6QixJQUFJLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDekMsSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFO1lBQ3JCLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7U0FDNUI7UUFDRCxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ3BELENBQUM7Ozs7OztJQUVELHlDQUFlOzs7OztJQUFmLFVBQWdCLElBQWtCLEVBQUUsS0FBcUM7UUFDdkUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDNUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNyQyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDdkIsQ0FBQztJQUVELHNCQUFJLHVDQUFVOzs7O1FBQWQ7WUFDRSxPQUFPLElBQUksQ0FBQztRQUNkLENBQUM7OztPQUFBOzs7OztJQUVELGlDQUFPOzs7O0lBQVAsVUFBUSxLQUFVO1FBQ2hCLFFBQVEsS0FBSyxDQUFDLE9BQU8sRUFBRTtZQUNyQixLQUFLLEtBQUs7O29CQUNGLElBQUksR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxJQUFJO2dCQUM1QyxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztnQkFDakMsTUFBTTtZQUNSLEtBQUssTUFBTTtnQkFDVCxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7Z0JBQ2IsTUFBTTtZQUNSLEtBQUssUUFBUSxDQUFDO1lBQ2QsS0FBSyxVQUFVO2dCQUNiLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNqQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsT0FBTyxLQUFLLFFBQVEsQ0FBQyxDQUFDO2dCQUM3RSxNQUFNO1lBQ1I7Z0JBQ0UsSUFBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO2dCQUNyQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQztnQkFDeEQsTUFBTTtTQUNUO0lBQ0gsQ0FBQzs7Ozs7O0lBRUQsb0NBQVU7Ozs7O0lBQVYsVUFBVyxJQUFrQyxFQUFFLEVBQVc7UUFDeEQsSUFBSSxtQkFBQSxJQUFJLEVBQWlCLElBQUksQ0FBQyxtQkFBQSxJQUFJLEVBQWlCLENBQUMsQ0FBQyxLQUFLLElBQUksQ0FBQyxtQkFBQSxJQUFJLEVBQWlCLENBQUMsQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUN0RyxJQUFJLEVBQUUsRUFBRTtnQkFDTixJQUFJLENBQUMsVUFBVSxDQUFDLHFCQUFxQixFQUFFLENBQUM7YUFDekM7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO2FBQ3JDO1lBQ0QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUM7U0FDdEQ7SUFDSCxDQUFDOzs7OztJQUVELHlDQUFlOzs7O0lBQWYsVUFBZ0IsSUFBUztRQUN2QixJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN0QyxDQUFDOztnQkE1TEYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxjQUFjO29CQUN4QixnbEZBQXNDO29CQUV0QyxVQUFVLEVBQUUsQ0FBQyxlQUFlLEVBQUUsQ0FBQztvQkFDL0IsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07O2lCQUNoRDs7OztnQkF0Qk8sV0FBVztnQkFFWCxhQUFhO2dCQUNMLGNBQWM7Z0JBRXRCLE1BQU07Z0JBR04sc0JBQXNCO2dCQVg1QixTQUFTOzs7a0NBMkJSLEtBQUs7aUNBQ0wsTUFBTTsrQkFDTixNQUFNO2tDQUVOLEtBQUs7MkJBR0wsZUFBZSxTQUFDLGlCQUFpQixFQUFFLEVBQUMsV0FBVyxFQUFFLEtBQUssRUFBQzt3QkFDdkQsWUFBWSxTQUFDLGtCQUFrQjs4QkFlL0IsU0FBUyxTQUFDLFFBQVE7NkJBQ2xCLFNBQVMsU0FBQyxZQUFZOzhCQUN0QixTQUFTLFNBQUMsYUFBYTs7SUE0SjFCLHNCQUFDO0NBQUEsQUE3TEQsSUE2TEM7U0F0TFksZUFBZTs7O0lBQzFCLDBDQUF3Qzs7SUFDeEMseUNBQTZEOztJQUM3RCx1Q0FBMkQ7O0lBRTNELDBDQUM4Qzs7SUFFOUMsbUNBQWlHOztJQUNqRyxnQ0FBdUU7Ozs7O0lBQ3ZFLHFDQUFtRTs7SUFFbkUsb0NBRUc7O0lBRUgsdUNBQXNCOztJQUN0Qix1Q0FBbUM7O0lBQ25DLHVDQUFrQzs7SUFFbEMsNkNBQW9DOztJQUNwQyxvQ0FBa0I7O0lBQ2xCLHFDQUFtQjs7SUFDbkIseUNBQXVCOztJQUN2QixzQ0FBMkM7O0lBQzNDLHFDQUE4Qzs7SUFDOUMsc0NBQWlEOztJQUVqRCxzQ0FBMkI7Ozs7O0lBT3pCLDZCQUF1Qjs7Ozs7SUFDdkIsd0NBQXNDOztJQUN0Qyx5Q0FBcUM7Ozs7O0lBQ3JDLGlDQUF3Qjs7Ozs7SUFDeEIsMENBQStDOzs7OztJQUMvQyxtQ0FBNkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xuICBBZnRlclZpZXdJbml0LFxuICBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneSxcbiAgQ29tcG9uZW50LFxuICBDb250ZW50Q2hpbGRyZW4sXG4gIEV2ZW50RW1pdHRlcixcbiAgSW5wdXQsXG4gIE9uSW5pdCxcbiAgT3V0cHV0LFxuICBRdWVyeUxpc3QsXG4gIFZpZXdDaGlsZCxcbiAgVmlld0NoaWxkcmVuLFxuICBSZW5kZXJlcjIsXG4gIE9uRGVzdHJveVxufSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7Rm9ybUJ1aWxkZXIsIEZvcm1Hcm91cH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHtNZW51SXRlbUNvbXBvbmVudH0gZnJvbSAnLi4vLi4vbmF2L21lbnUvbWVudS1pdGVtL21lbnUtaXRlbS5jb21wb25lbnQnO1xuaW1wb3J0IHtTZWFyY2hTZXJ2aWNlfSBmcm9tICcuL3NlYXJjaC5zZXJ2aWNlJztcbmltcG9ydCB7SG90a2V5LCBIb3RrZXlzU2VydmljZX0gZnJvbSAnYW5ndWxhcjItaG90a2V5cyc7XG5pbXBvcnQge01hdElucHV0LCBNYXRCdXR0b259IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcbmltcG9ydCB7Um91dGVyfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHtHcnVwb1Blc3F1aXNhLCBJdGVtUGVzcXVpc2F9IGZyb20gJy4vc2VhcmNoLm1vZGVsJztcbmltcG9ydCB7c2VhcmNoQW5pbWF0aW9ufSBmcm9tICcuL3NsaWRlLWFuaW1hdGlvbnMnO1xuaW1wb3J0IHtTZWFyY2hIaXN0b3JpY29TZXJ2aWNlfSBmcm9tICcuL3NlYXJjaC1oaXN0b3JpY28uc2VydmljZSc7XG5pbXBvcnQgKiBhcyBfIGZyb20gJ2xvZGFzaCc7XG5pbXBvcnQge2d1aWR9IGZyb20gJ0BkYXRvcmFtYS9ha2l0YSc7XG5pbXBvcnQge3RpbWVyLCBTdWJzY3JpcHRpb259IGZyb20gJ3J4anMnO1xuaW1wb3J0IHtBY3RpdmVEZXNjZW5kYW50S2V5TWFuYWdlcn0gZnJvbSAnQGFuZ3VsYXIvY2RrL2ExMXknO1xuaW1wb3J0IHtFTlRFUiwgRVNDQVBFLCBVUF9BUlJPVywgRE9XTl9BUlJPV30gZnJvbSAnQGFuZ3VsYXIvY2RrL2tleWNvZGVzJztcbmltcG9ydCB7SGlnaGxpZ2h0Q29tcG9uZW50fSBmcm9tICcuLi8uLi9uYXYvaGlnaGxpZ2h0L2hpZ2hsaWdodC5jb21wb25lbnQnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICd1aWtpdC1zZWFyY2gnLFxuICB0ZW1wbGF0ZVVybDogJy4vc2VhcmNoLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vc2VhcmNoLmNvbXBvbmVudC5zY3NzJ10sXG4gIGFuaW1hdGlvbnM6IFtzZWFyY2hBbmltYXRpb24oKV0sXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoXG59KVxuZXhwb3J0IGNsYXNzIFNlYXJjaENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgQWZ0ZXJWaWV3SW5pdCwgT25EZXN0cm95IHtcbiAgQElucHV0KCkgZ3J1cG9QZXNxdWlzYWRvOiBHcnVwb1Blc3F1aXNhO1xuICBAT3V0cHV0KCkgYWRpY2lvbmFyR3J1cG8gPSBuZXcgRXZlbnRFbWl0dGVyPEdydXBvUGVzcXVpc2E+KCk7XG4gIEBPdXRwdXQoKSByZW1vdmVyR3J1cG8gPSBuZXcgRXZlbnRFbWl0dGVyPEdydXBvUGVzcXVpc2E+KCk7XG5cbiAgQElucHV0KClcbiAgcGxhY2Vob2xkZXJUZXh0ID0gJ1Blc3F1aXNhIChDdHJsICsgQWx0ICsgMyknO1xuXG4gIEBDb250ZW50Q2hpbGRyZW4oTWVudUl0ZW1Db21wb25lbnQsIHtkZXNjZW5kYW50czogZmFsc2V9KSBjaGlsZHJlbjogUXVlcnlMaXN0PE1lbnVJdGVtQ29tcG9uZW50PjtcbiAgQFZpZXdDaGlsZHJlbihIaWdobGlnaHRDb21wb25lbnQpIGl0ZW1zOiBRdWVyeUxpc3Q8SGlnaGxpZ2h0Q29tcG9uZW50PjtcbiAgcHJpdmF0ZSBrZXlNYW5hZ2VyOiBBY3RpdmVEZXNjZW5kYW50S2V5TWFuYWdlcjxIaWdobGlnaHRDb21wb25lbnQ+O1xuXG4gIHN0YXRlRm9ybTogRm9ybUdyb3VwID0gdGhpcy5mYi5ncm91cCh7XG4gICAgc2VhcmNoR3JvdXBzOiAnJyxcbiAgfSk7XG5cbiAgc2VhcmNoT3BlbmVkOiBib29sZWFuO1xuICBzZWFyY2hHcm91cHM6IEdydXBvUGVzcXVpc2FbXSA9IFtdO1xuICBzdWJzY3JpcHRpb24gPSBuZXcgU3Vic2NyaXB0aW9uKCk7XG5cbiAgc2VhcmNoR3JvdXBPcHRpb25zOiBHcnVwb1Blc3F1aXNhW107XG4gIGhpZ2hsaWdodDogc3RyaW5nO1xuICBvcGVuU2VhcmNoID0gZmFsc2U7XG4gIGl0ZW1QZXNxdWlzYWRvOiBzdHJpbmc7XG4gIEBWaWV3Q2hpbGQoJ3NlYXJjaCcpIHNlYXJjaElucHV0OiBNYXRJbnB1dDtcbiAgQFZpZXdDaGlsZCgnc2VhcmNoT3BlbicpIHNlYXJjaE9wZW46IE1hdElucHV0O1xuICBAVmlld0NoaWxkKCdidXR0b25DbG9zZScpIGJ1dHRvbkNsb3NlOiBNYXRCdXR0b247XG5cbiAgcHVibGljIGlzQW5pbWF0aW5nID0gZmFsc2U7XG5cbiAgZ2V0IGhhc1Jlc3VsdHMoKSB7XG4gICAgcmV0dXJuIHRoaXMub3BlblNlYXJjaCAmJiB0aGlzLnNlYXJjaEdyb3VwT3B0aW9ucy5sZW5ndGg7XG4gIH1cblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIGZiOiBGb3JtQnVpbGRlcixcbiAgICBwcm90ZWN0ZWQgc2VhcmNoU2VydmljZTogU2VhcmNoU2VydmljZSxcbiAgICBwdWJsaWMgaG90a2V5c1NlcnZpY2U6IEhvdGtleXNTZXJ2aWNlLFxuICAgIHByb3RlY3RlZCByb3V0ZXI6IFJvdXRlcixcbiAgICBwcml2YXRlIHNlYXJjaEhpc3RvcmljbzogU2VhcmNoSGlzdG9yaWNvU2VydmljZSxcbiAgICBwcm90ZWN0ZWQgcmVuZGVyZXI6IFJlbmRlcmVyMixcbiAgKSB7XG4gIH1cblxuICBuZ0FmdGVyVmlld0luaXQoKTogdm9pZCB7XG5cbiAgICBpZiAoZG9jdW1lbnQuYm9keS5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKS53aWR0aCA8PSA5NzApIHtcbiAgICAgIHRoaXMucGxhY2Vob2xkZXJUZXh0ID0gJ1Blc3F1aXNhJztcbiAgICB9XG5cbiAgICB0aGlzLmhvdGtleXNTZXJ2aWNlLmFkZChuZXcgSG90a2V5KCdjdHJsK2FsdCszJywgKGV2ZW50OiBLZXlib2FyZEV2ZW50KTogYm9vbGVhbiA9PiB7XG4gICAgICB0aGlzLnNlYXJjaE9wZW4uZm9jdXMoKTtcbiAgICAgIHJldHVybiBmYWxzZTsgLy8gUHJldmVudCBidWJibGluZ1xuICAgIH0pKTtcblxuICAgIHRpbWVyKDEwMDApLnN1YnNjcmliZSgoKSA9PiB7XG4gICAgICB0aGlzLnNlYXJjaE9wZW4uZm9jdXMoKTtcbiAgICB9KTtcblxuICAgIHRoaXMua2V5TWFuYWdlciA9IG5ldyBBY3RpdmVEZXNjZW5kYW50S2V5TWFuYWdlcih0aGlzLml0ZW1zKS53aXRoV3JhcCgpLndpdGhUeXBlQWhlYWQoKTtcbiAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKHRoaXMuYnV0dG9uQ2xvc2UuX2VsZW1lbnRSZWYubmF0aXZlRWxlbWVudCwgJ2Rpc3BsYXknLCAnbm9uZScpO1xuICB9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgdGhpcy5zdGF0ZUZvcm0uZ2V0KCdzZWFyY2hHcm91cHMnKS52YWx1ZUNoYW5nZXMuc3Vic2NyaWJlKHZhbHVlID0+IHtcbiAgICAgIHRoaXMuaXRlbVBlc3F1aXNhZG8gPSB2YWx1ZTtcbiAgICAgIHRoaXMuc2VhcmNoR3JvdXBPcHRpb25zID0gW107XG4gICAgICB0aGlzLnNlYXJjaFNlcnZpY2UuYnVzY2FyKHZhbHVlKVxuICAgICAgICAuc3Vic2NyaWJlKChpdGVuc01lbnVQZXNxdWlzYSkgPT4ge1xuICAgICAgICAgICAgaWYgKGl0ZW5zTWVudVBlc3F1aXNhICYmIGl0ZW5zTWVudVBlc3F1aXNhLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgICAgXy5tYXAoaXRlbnNNZW51UGVzcXVpc2EsIChpdGVtKSA9PiB0aGlzLnNlYXJjaEdyb3VwT3B0aW9ucy5wdXNoKGl0ZW0pKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgIHRoaXMuc2VhcmNoR3JvdXBPcHRpb25zLnB1c2goXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgaWQ6IGd1aWQoKSxcbiAgICAgICAgICAgICAgICAgIGlzQXZpc286IHRydWUsXG4gICAgICAgICAgICAgICAgICB0aXRsZTogYE7Do28gZW5jb250cmFtb3MgJyR7dmFsdWV9J2BcbiAgICAgICAgICAgICAgICB9IGFzIEdydXBvUGVzcXVpc2FcbiAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgICk7XG4gICAgfSk7XG5cbiAgICB0aGlzLnNlYXJjaEdyb3VwT3B0aW9ucyA9IHRoaXMuc2VhcmNoSGlzdG9yaWNvLm9idGVySGlzdG9yaWNvKCk7XG4gIH1cblxuICBvcGVuVG9TZWFyY2goZXZlbnQ6IGFueSwgc2VhcmNoQ29udGFpbmVyOiBhbnkpOiB2b2lkIHtcbiAgICBpZiAoIXRoaXMuc2VhcmNoT3BlbmVkXG4gICAgICAmJiBldmVudFxuICAgICAgJiYgZXZlbnQuY29kZSA9PT0gJ0tleUEnXG4gICAgICAmJiBldmVudC5rZXkubGVuZ3RoID4gMCkge1xuICAgICAgdGhpcy5zZWFyY2hPcGVuZWQgPSB0cnVlO1xuICAgICAgdGhpcy5vcGVuKHNlYXJjaENvbnRhaW5lcik7XG4gICAgfVxuICB9XG5cbiAgb25CbHVyKHNlYXJjaENvbnRhaW5lcjogYW55KSB7XG4gICAgdGhpcy5zdWJzY3JpcHRpb24uYWRkKHRpbWVyKDE1MCkuc3Vic2NyaWJlKCgpID0+IHtcbiAgICAgIGlmICghdGhpcy5zZWFyY2hPcGVuLmZvY3VzZWQpIHtcbiAgICAgICAgdGhpcy5jbG9zZSgpO1xuICAgICAgICB0aGlzLnJlbmRlcmVyLnJlbW92ZUF0dHJpYnV0ZShzZWFyY2hDb250YWluZXIsICdvcGVuZWQnKTtcbiAgICAgICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0aGlzLmJ1dHRvbkNsb3NlLl9lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQsICdkaXNwbGF5JywgJ25vbmUnKTtcbiAgICAgIH1cbiAgICB9KSk7XG4gIH1cblxuICBjbG9zZVRvU2VhcmNoKCk6IHZvaWQge1xuICAgIGlmICghdGhpcy5zZWFyY2hPcGVuLmZvY3VzZWQpIHtcbiAgICAgIHRoaXMuY2xvc2UoKTtcbiAgICB9XG4gIH1cblxuICBvcGVuKHNlYXJjaENvbnRhaW5lcjogYW55KTogdm9pZCB7XG4gICAgdGhpcy5zZWFyY2hPcGVuLmZvY3VzKCk7XG4gICAgdGhpcy5yZW5kZXJlci5zZXRBdHRyaWJ1dGUoc2VhcmNoQ29udGFpbmVyLCAnb3BlbmVkJywgJycpO1xuICAgIHRoaXMucmVuZGVyZXIucmVtb3ZlU3R5bGUodGhpcy5idXR0b25DbG9zZS5fZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LCAnZGlzcGxheScpO1xuICB9XG5cbiAgY2xvc2UoKTogdm9pZCB7XG4gICAgdGhpcy5zZWFyY2hPcGVuZWQgPSBmYWxzZTtcbiAgICAoZG9jdW1lbnQuYWN0aXZlRWxlbWVudCBhcyBIVE1MRWxlbWVudCkuYmx1cigpO1xuICAgIHRoaXMuc3RhdGVGb3JtLmNvbnRyb2xzLnNlYXJjaEdyb3Vwcy5zZXRWYWx1ZSgnJyk7XG4gIH1cblxuICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcbiAgICB0aGlzLnN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xuICB9XG5cbiAgdG9nZ2xlKHNlYXJjaENvbnRhaW5lcjogYW55KTogdm9pZCB7XG4gICAgdGhpcy5zZWFyY2hPcGVuZWQgPSAoIXRoaXMuc2VhcmNoT3BlbmVkKTtcbiAgICBpZiAodGhpcy5zZWFyY2hPcGVuZWQpIHtcbiAgICAgIHRoaXMub3BlbihzZWFyY2hDb250YWluZXIpO1xuICAgIH1cbiAgICB0aGlzLnN0YXRlRm9ybS5jb250cm9scy5zZWFyY2hHcm91cHMuc2V0VmFsdWUoJycpO1xuICB9XG5cbiAgZ29Ub0FkZEhpc3RvcmljKGl0ZW06IEl0ZW1QZXNxdWlzYSwgZ3JvdXA6IChJdGVtUGVzcXVpc2EgfCBHcnVwb1Blc3F1aXNhKSk6IHZvaWQge1xuICAgIHRoaXMuc2VhcmNoSGlzdG9yaWNvLnNhbHZhckhpc3Rvcmljbyhncm91cCk7XG4gICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoW2l0ZW0ubGlua1swXV0pO1xuICAgIHRoaXMuY2xvc2VUb1NlYXJjaCgpO1xuICB9XG5cbiAgZ2V0IGhlaWdodFNpemUoKTogc3RyaW5nIHtcbiAgICByZXR1cm4gbnVsbDtcbiAgfVxuXG4gIG9uS2V5dXAoZXZlbnQ6IGFueSk6IHZvaWQge1xuICAgIHN3aXRjaCAoZXZlbnQua2V5Q29kZSkge1xuICAgICAgY2FzZSBFTlRFUjpcbiAgICAgICAgY29uc3QgaXRlbSA9IHRoaXMua2V5TWFuYWdlci5hY3RpdmVJdGVtLml0ZW07XG4gICAgICAgIHRoaXMuZ29Ub0FkZEhpc3RvcmljKGl0ZW0sIGl0ZW0pO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgRVNDQVBFOlxuICAgICAgICB0aGlzLmNsb3NlKCk7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSBVUF9BUlJPVzpcbiAgICAgIGNhc2UgRE9XTl9BUlJPVzpcbiAgICAgICAgdGhpcy5rZXlNYW5hZ2VyLm9uS2V5ZG93bihldmVudCk7XG4gICAgICAgIHRoaXMuYWN0aXZlSXRlbSh0aGlzLmtleU1hbmFnZXIuYWN0aXZlSXRlbS5pdGVtLCBldmVudC5rZXlDb2RlID09PSBVUF9BUlJPVyk7XG4gICAgICAgIGJyZWFrO1xuICAgICAgZGVmYXVsdDpcbiAgICAgICAgdGhpcy5rZXlNYW5hZ2VyLnNldEZpcnN0SXRlbUFjdGl2ZSgpO1xuICAgICAgICB0aGlzLmFjdGl2ZUl0ZW0odGhpcy5rZXlNYW5hZ2VyLmFjdGl2ZUl0ZW0uaXRlbSwgZmFsc2UpO1xuICAgICAgICBicmVhaztcbiAgICB9XG4gIH1cblxuICBhY3RpdmVJdGVtKGl0ZW06IEdydXBvUGVzcXVpc2EgfCBJdGVtUGVzcXVpc2EsIHVwOiBib29sZWFuKTogdm9pZCB7XG4gICAgaWYgKGl0ZW0gYXMgR3J1cG9QZXNxdWlzYSAmJiAoaXRlbSBhcyBHcnVwb1Blc3F1aXNhKS5pdGVtcyAmJiAoaXRlbSBhcyBHcnVwb1Blc3F1aXNhKS5pdGVtcy5sZW5ndGggPiAwKSB7XG4gICAgICBpZiAodXApIHtcbiAgICAgICAgdGhpcy5rZXlNYW5hZ2VyLnNldFByZXZpb3VzSXRlbUFjdGl2ZSgpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5rZXlNYW5hZ2VyLnNldE5leHRJdGVtQWN0aXZlKCk7XG4gICAgICB9XG4gICAgICB0aGlzLmFjdGl2ZUl0ZW0odGhpcy5rZXlNYW5hZ2VyLmFjdGl2ZUl0ZW0uaXRlbSwgdXApO1xuICAgIH1cbiAgfVxuXG4gIG9uTW91c2VvdmVySXRlbShpdGVtOiBhbnkpOiB2b2lkIHtcbiAgICB0aGlzLmtleU1hbmFnZXIuc2V0QWN0aXZlSXRlbShpdGVtKTtcbiAgfVxufVxuIl19