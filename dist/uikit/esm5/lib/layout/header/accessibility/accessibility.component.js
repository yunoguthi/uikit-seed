/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ChangeDetectionStrategy, ViewChild, Renderer2, Input } from '@angular/core';
import { MatSlider } from '@angular/material/slider';
import { MatSlideToggle } from '@angular/material';
var AccessibilityComponent = /** @class */ (function () {
    function AccessibilityComponent(renderer) {
        this.renderer = renderer;
        this.showIa = false;
        this.showShortcuts = true;
    }
    /**
     * @return {?}
     */
    AccessibilityComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @return {?}
     */
    AccessibilityComponent.prototype.ngAfterViewInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.slider.valueChange.subscribe((/**
         * @param {?} valor
         * @return {?}
         */
        function (valor) {
            _this.renderer.setStyle(document.body, 'font-size', valor + 'em');
        }));
        this.toggleContrast.toggleChange.subscribe((/**
         * @return {?}
         */
        function () {
            if (!_this.toggleContrast.checked) {
                _this.renderer.setAttribute(document.querySelector('.logotipo .is-mobile'), 'src', '../../assets/images/uikit-logotipo-mobile-bw.svg');
                _this.renderer.setAttribute(document.querySelector('.logotipo .is-desktop'), 'src', '../../assets/images/uikit-logotipo-bw.svg');
                _this.renderer.addClass(document.body, 'dark');
            }
            else {
                _this.renderer.setAttribute(document.querySelector('.logotipo .is-mobile'), 'src', '../../assets/images/uikit-logotipo-mobile.svg');
                _this.renderer.setAttribute(document.querySelector('.logotipo .is-desktop'), 'src', '../../assets/images/uikit-logotipo.svg');
                _this.renderer.removeClass(document.body, 'dark');
            }
        }));
    };
    AccessibilityComponent.decorators = [
        { type: Component, args: [{
                    selector: 'uikit-accessibility',
                    template: "<mat-menu #accessibilityMenu=\"matMenu\">\n  <mat-list\n    class=\"dropdown-list\"\n    (click)=\"$event.stopPropagation()\"\n    style=\"display: contents;\"\n  >\n    <h3 mat-subheader>\n      Acessibilidade\n    </h3>\n\n    <mat-list>\n      <mat-list-item class=\"tamanho-fonte\">\n        <div mat-line>\n          <h4>Tamanho das fontes</h4>\n          <mat-slider\n            #slider\n            [max]=\"1.2\"\n            [min]=\"0.8\"\n            [step]=\"0.1\"\n            [value]=\"1\"\n            color=\"primary\"\n          >\n          </mat-slider>\n        </div>\n        <mat-divider></mat-divider>\n      </mat-list-item>\n\n      <mat-list-item class=\"contraste\">\n        <div mat-line>\n          <h4>Contraste</h4>\n          <p>\n            <mat-slide-toggle #toggleContrast color=\"primary\">{{\n              !toggleContrast.checked ? 'Ligar' : 'Desligar'\n            }}</mat-slide-toggle>\n          </p>\n        </div>\n        <mat-divider></mat-divider>\n      </mat-list-item>\n\n      <mat-list-item *ngIf=\"showShortcuts\">\n        <div mat-line>\n          <h4>Teclas de atalho</h4>\n\n          <p mat-line>\n            Pesquisa Principal\n            <mat-chip-list>\n              <mat-chip>ctrl</mat-chip> + <mat-chip>alt</mat-chip> +\n              <mat-chip color=\"primary\" selected>f</mat-chip>\n            </mat-chip-list>\n          </p>\n\n          <p mat-line>\n            Salvar conte\u00FAdo\n            <mat-chip-list>\n              <mat-chip>ctrl</mat-chip> +\n              <mat-chip color=\"primary\" selected>s</mat-chip>\n            </mat-chip-list>\n          </p>\n\n          <p mat-line>\n            Novo conte\u00FAdo\n            <mat-chip-list>\n              <mat-chip>ctrl</mat-chip> + <mat-chip>alt</mat-chip> +\n              <mat-chip color=\"primary\" selected>n</mat-chip>\n            </mat-chip-list>\n          </p>\n\n          <p mat-line>\n            Pesquisar conte\u00FAdo\n            <mat-chip-list>\n              <mat-chip>alt</mat-chip> +\n              <mat-chip color=\"primary\" selected>3</mat-chip>\n            </mat-chip-list>\n          </p>\n\n          <p mat-line>\n            Filtro avan\u00E7ado\n            <mat-chip-list>\n              <mat-chip>ctrl</mat-chip> + <mat-chip>alt</mat-chip> +\n              <mat-chip color=\"primary\" selected>3</mat-chip>\n            </mat-chip-list>\n          </p>\n\n          <p mat-line>\n            Voltar/Pr\u00F3xima p\u00E1gina\n            <mat-chip-list>\n              <mat-chip><mat-icon class=\"fas fa-arrow-left\" style=\"width: auto; margin: 0px !important;\"></mat-icon></mat-chip>\n              ou\n              <mat-chip><mat-icon class=\"fas fa-arrow-right\" style=\"width: auto; margin: 0px !important;\"></mat-icon></mat-chip>\n            </mat-chip-list>\n          </p>\n\n          <p mat-line>\n            Menu Principal\n            <mat-chip-list>\n              <mat-chip>ctrl</mat-chip> +\n              <mat-chip color=\"primary\" selected>m</mat-chip>\n            </mat-chip-list>\n          </p>\n\n          <p mat-line *ngIf=\"showIa\">\n            Falar com a Judi\n            <mat-chip-list>\n              <mat-chip>ctrl</mat-chip> + <mat-chip>alt</mat-chip> +\n              <mat-chip color=\"primary\" selected>j</mat-chip>\n            </mat-chip-list>\n          </p>\n        </div>\n        <mat-divider></mat-divider>\n      </mat-list-item>\n    </mat-list>\n  </mat-list>\n\n  <!-- <mat-action-list>\n    <a mat-list-item routerLink=\".\">ver todos</a>\n  </mat-action-list> -->\n</mat-menu>\n\n<button\n  mat-icon-button\n  color=\"primary\"\n  [matMenuTriggerFor]=\"accessibilityMenu\"\n  matTooltip=\"Acessibilidade\"\n  aria-label=\"Acessibilidade\"\n  tabindex=\"5\"\n>\n  <mat-icon class=\"fa-2x fas fa-universal-access acessibility\"></mat-icon>\n</button>\n",
                    changeDetection: ChangeDetectionStrategy.OnPush,
                    styles: [".mat-line .mat-slider-horizontal{max-width:275px;cursor:pointer}.mat-slider-horizontal{width:100%}"]
                }] }
    ];
    /** @nocollapse */
    AccessibilityComponent.ctorParameters = function () { return [
        { type: Renderer2 }
    ]; };
    AccessibilityComponent.propDecorators = {
        showIa: [{ type: Input }],
        showShortcuts: [{ type: Input }],
        slider: [{ type: ViewChild, args: [MatSlider,] }],
        toggleContrast: [{ type: ViewChild, args: [MatSlideToggle,] }]
    };
    return AccessibilityComponent;
}());
export { AccessibilityComponent };
if (false) {
    /** @type {?} */
    AccessibilityComponent.prototype.showIa;
    /** @type {?} */
    AccessibilityComponent.prototype.showShortcuts;
    /** @type {?} */
    AccessibilityComponent.prototype.slider;
    /** @type {?} */
    AccessibilityComponent.prototype.toggleContrast;
    /**
     * @type {?}
     * @private
     */
    AccessibilityComponent.prototype.renderer;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWNjZXNzaWJpbGl0eS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL2xheW91dC9oZWFkZXIvYWNjZXNzaWJpbGl0eS9hY2Nlc3NpYmlsaXR5LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSx1QkFBdUIsRUFBRSxTQUFTLEVBQWlCLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDdkgsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ3JELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUVuRDtJQWVFLGdDQUFvQixRQUFtQjtRQUFuQixhQUFRLEdBQVIsUUFBUSxDQUFXO1FBUDlCLFdBQU0sR0FBRyxLQUFLLENBQUM7UUFDZixrQkFBYSxHQUFHLElBQUksQ0FBQztJQU1hLENBQUM7Ozs7SUFFNUMseUNBQVE7OztJQUFSO0lBQ0EsQ0FBQzs7OztJQUVELGdEQUFlOzs7SUFBZjtRQUFBLGlCQWtCQztRQWpCQyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxTQUFTOzs7O1FBQUMsVUFBQSxLQUFLO1lBQ3JDLEtBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsV0FBVyxFQUFFLEtBQUssR0FBRyxJQUFJLENBQUMsQ0FBQztRQUNuRSxDQUFDLEVBQUMsQ0FBQztRQUVILElBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLFNBQVM7OztRQUFDO1lBRXpDLElBQUcsQ0FBQyxLQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sRUFBRTtnQkFDL0IsS0FBSSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxzQkFBc0IsQ0FBQyxFQUFFLEtBQUssRUFBRSxrREFBa0QsQ0FBQyxDQUFDO2dCQUN0SSxLQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLHVCQUF1QixDQUFDLEVBQUUsS0FBSyxFQUFFLDJDQUEyQyxDQUFDLENBQUM7Z0JBQ2hJLEtBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLENBQUM7YUFDL0M7aUJBQU07Z0JBQ0wsS0FBSSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxzQkFBc0IsQ0FBQyxFQUFFLEtBQUssRUFBRSwrQ0FBK0MsQ0FBQyxDQUFDO2dCQUNuSSxLQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLHVCQUF1QixDQUFDLEVBQUUsS0FBSyxFQUFFLHdDQUF3QyxDQUFDLENBQUM7Z0JBQzdILEtBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLENBQUM7YUFDbEQ7UUFFSCxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7O2dCQXRDRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLHFCQUFxQjtvQkFDL0Isa3pIQUE2QztvQkFFN0MsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07O2lCQUNoRDs7OztnQkFUOEUsU0FBUzs7O3lCQVlyRixLQUFLO2dDQUNMLEtBQUs7eUJBRUwsU0FBUyxTQUFDLFNBQVM7aUNBQ25CLFNBQVMsU0FBQyxjQUFjOztJQTRCM0IsNkJBQUM7Q0FBQSxBQXhDRCxJQXdDQztTQWxDWSxzQkFBc0I7OztJQUVqQyx3Q0FBd0I7O0lBQ3hCLCtDQUE4Qjs7SUFFOUIsd0NBQXdDOztJQUN4QyxnREFBMEQ7Ozs7O0lBRzlDLDBDQUEyQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneSwgVmlld0NoaWxkLCBBZnRlclZpZXdJbml0LCBSZW5kZXJlcjIsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBNYXRTbGlkZXIgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9zbGlkZXInO1xuaW1wb3J0IHsgTWF0U2xpZGVUb2dnbGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ3Vpa2l0LWFjY2Vzc2liaWxpdHknLFxuICB0ZW1wbGF0ZVVybDogJy4vYWNjZXNzaWJpbGl0eS5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2FjY2Vzc2liaWxpdHkuY29tcG9uZW50LnNjc3MnXSxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2hcbn0pXG5leHBvcnQgY2xhc3MgQWNjZXNzaWJpbGl0eUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgQWZ0ZXJWaWV3SW5pdCB7XG5cbiAgQElucHV0KCkgc2hvd0lhID0gZmFsc2U7XG4gIEBJbnB1dCgpIHNob3dTaG9ydGN1dHMgPSB0cnVlO1xuXG4gIEBWaWV3Q2hpbGQoTWF0U2xpZGVyKSBzbGlkZXI6IE1hdFNsaWRlcjtcbiAgQFZpZXdDaGlsZChNYXRTbGlkZVRvZ2dsZSkgdG9nZ2xlQ29udHJhc3Q6IE1hdFNsaWRlVG9nZ2xlO1xuXG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSByZW5kZXJlcjogUmVuZGVyZXIyKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcbiAgfVxuXG4gIG5nQWZ0ZXJWaWV3SW5pdCgpOiB2b2lkIHtcbiAgICB0aGlzLnNsaWRlci52YWx1ZUNoYW5nZS5zdWJzY3JpYmUodmFsb3IgPT4ge1xuICAgICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZShkb2N1bWVudC5ib2R5LCAnZm9udC1zaXplJywgdmFsb3IgKyAnZW0nKTtcbiAgICB9KTtcblxuICAgIHRoaXMudG9nZ2xlQ29udHJhc3QudG9nZ2xlQ2hhbmdlLnN1YnNjcmliZSgoKSA9PiB7XG5cbiAgICAgIGlmKCF0aGlzLnRvZ2dsZUNvbnRyYXN0LmNoZWNrZWQpIHtcbiAgICAgICAgdGhpcy5yZW5kZXJlci5zZXRBdHRyaWJ1dGUoZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmxvZ290aXBvIC5pcy1tb2JpbGUnKSwgJ3NyYycsICcuLi8uLi9hc3NldHMvaW1hZ2VzL3Vpa2l0LWxvZ290aXBvLW1vYmlsZS1idy5zdmcnKTtcbiAgICAgICAgdGhpcy5yZW5kZXJlci5zZXRBdHRyaWJ1dGUoZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmxvZ290aXBvIC5pcy1kZXNrdG9wJyksICdzcmMnLCAnLi4vLi4vYXNzZXRzL2ltYWdlcy91aWtpdC1sb2dvdGlwby1idy5zdmcnKTtcbiAgICAgICAgdGhpcy5yZW5kZXJlci5hZGRDbGFzcyhkb2N1bWVudC5ib2R5LCAnZGFyaycpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5yZW5kZXJlci5zZXRBdHRyaWJ1dGUoZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmxvZ290aXBvIC5pcy1tb2JpbGUnKSwgJ3NyYycsICcuLi8uLi9hc3NldHMvaW1hZ2VzL3Vpa2l0LWxvZ290aXBvLW1vYmlsZS5zdmcnKTtcbiAgICAgICAgdGhpcy5yZW5kZXJlci5zZXRBdHRyaWJ1dGUoZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmxvZ290aXBvIC5pcy1kZXNrdG9wJyksICdzcmMnLCAnLi4vLi4vYXNzZXRzL2ltYWdlcy91aWtpdC1sb2dvdGlwby5zdmcnKTtcbiAgICAgICAgdGhpcy5yZW5kZXJlci5yZW1vdmVDbGFzcyhkb2N1bWVudC5ib2R5LCAnZGFyaycpO1xuICAgICAgfVxuXG4gICAgfSk7XG4gIH1cblxufVxuIl19