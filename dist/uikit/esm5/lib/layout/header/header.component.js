/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { ChangeDetectionStrategy, Component, Input, Renderer2, ViewChild } from '@angular/core';
import { NavQuery } from '../nav/state/nav.query';
import { NavService } from '../nav/state/nav.service';
import { MatToolbar } from '@angular/material';
import { LayoutService } from '../layout.service';
var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(navQuery, navService, layoutService, renderer) {
        this.navQuery = navQuery;
        this.navService = navService;
        this.layoutService = layoutService;
        this.renderer = renderer;
        this.showNotifications = false;
        this.showUserInfo = false;
        this.showSystemInfo = false;
        this.showIa = false;
        this.showShortcuts = true;
        this.leftNav$ = this.navQuery.leftnav$;
        this.rightNav$ = this.navQuery.rightnav$;
    }
    /**
     * @return {?}
     */
    HeaderComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @return {?}
     */
    HeaderComponent.prototype.toggle = /**
     * @return {?}
     */
    function () {
        this.navService.toggleLeftNav();
    };
    /**
     * @return {?}
     */
    HeaderComponent.prototype.rightNavToggle = /**
     * @return {?}
     */
    function () {
        this.navService.toggleRightNav();
    };
    HeaderComponent.decorators = [
        { type: Component, args: [{
                    selector: 'uikit-header',
                    template: "<mat-toolbar>\n\n  <button tabindex=\"1\" mat-flat-button color=\"primary\" type=\"button\" aria-label=\"Abri menu de navega\u00E7\u00E3o\"\n    aria-controls=\"navigation\" matTooltip=\"Abrir menu de navega\u00E7\u00E3o\" (click)=\"toggle()\"\n    class=\"btn-hamburger hamburger--slider\" [class.is-active]=\"(leftNav$ | async).opened\">\n    <span class=\"hamburger-box\">\n      <span class=\"hamburger-inner\"></span>\n    </span>\n  </button>\n\n  <span class=\"logotipo\">\n    <a href=\"#\" tabindex=\"1\">\n      <div class=\"logomobile is-mobile\"></div>\n      <div class=\"logodesk is-desktop\"></div>\n    </a>\n  </span>\n\n  <span class=\"header-nav\">\n    <uikit-notification *ngIf=\"showNotifications == true\">\n      <div notifications>\n        <ng-content select=\"[header-notifications]\"></ng-content>\n      </div>\n    </uikit-notification>\n    <uikit-systeminfo *ngIf=\"showSystemInfo == true\">\n      <div systeminfo>\n        <ng-content select=\"[header-systeminfo]\"></ng-content>\n      </div>\n    </uikit-systeminfo>\n    <uikit-accessibility [showIa]=\"showIa\" [showShortcuts]=\"showShortcuts\"></uikit-accessibility>\n    <uikit-userinfo *ngIf=\"showUserInfo == true\">\n      <div userinfo>\n\n        <ng-content select=\"[header-userinfo]\"></ng-content>\n\n      </div>\n    </uikit-userinfo>\n\n    <button *ngIf=\"showIa\" class=\"aikit-button\" mat-flat-button color=\"primary\" type=\"button\"\n      aria-label=\"Abri menu de navega\u00E7\u00E3o\" aria-controls=\"navigation\" (click)=\"rightNavToggle()\"\n      matTooltip=\"Abri menu de navega\u00E7\u00E3o\" matTooltip=\"Notifica\u00E7\u00F5es\" aria-label=\"Notifica\u00E7\u00F5es\" matBadge=\"2\"\n      matBadgeColor=\"accent\" matBadgeSize=\"small\" tabindex=\"7\">\n      <img src=\"../../assets/images/iakit-logotipo-mobile.svg\" />\n    </button>\n  </span>\n</mat-toolbar>",
                    changeDetection: ChangeDetectionStrategy.OnPush,
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    HeaderComponent.ctorParameters = function () { return [
        { type: NavQuery },
        { type: NavService },
        { type: LayoutService },
        { type: Renderer2 }
    ]; };
    HeaderComponent.propDecorators = {
        showNotifications: [{ type: Input }],
        showUserInfo: [{ type: Input }],
        showSystemInfo: [{ type: Input }],
        showIa: [{ type: Input }],
        matToolbar: [{ type: ViewChild, args: [MatToolbar,] }],
        showShortcuts: [{ type: Input }]
    };
    return HeaderComponent;
}());
export { HeaderComponent };
if (false) {
    /** @type {?} */
    HeaderComponent.prototype.showNotifications;
    /** @type {?} */
    HeaderComponent.prototype.showUserInfo;
    /** @type {?} */
    HeaderComponent.prototype.showSystemInfo;
    /** @type {?} */
    HeaderComponent.prototype.showIa;
    /** @type {?} */
    HeaderComponent.prototype.matToolbar;
    /** @type {?} */
    HeaderComponent.prototype.showShortcuts;
    /** @type {?} */
    HeaderComponent.prototype.leftNav$;
    /** @type {?} */
    HeaderComponent.prototype.rightNav$;
    /**
     * @type {?}
     * @protected
     */
    HeaderComponent.prototype.navQuery;
    /**
     * @type {?}
     * @protected
     */
    HeaderComponent.prototype.navService;
    /**
     * @type {?}
     * @protected
     */
    HeaderComponent.prototype.layoutService;
    /**
     * @type {?}
     * @protected
     */
    HeaderComponent.prototype.renderer;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVhZGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvbGF5b3V0L2hlYWRlci9oZWFkZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsdUJBQXVCLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBVSxTQUFTLEVBQUUsU0FBUyxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3RHLE9BQU8sRUFBQyxRQUFRLEVBQUMsTUFBTSx3QkFBd0IsQ0FBQztBQUNoRCxPQUFPLEVBQUMsVUFBVSxFQUFDLE1BQU0sMEJBQTBCLENBQUM7QUFDcEQsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLG1CQUFtQixDQUFDO0FBQzdDLE9BQU8sRUFBQyxhQUFhLEVBQUMsTUFBTSxtQkFBbUIsQ0FBQztBQUVoRDtJQU9FLHlCQUFzQixRQUFrQixFQUFZLFVBQXNCLEVBQ3BELGFBQTRCLEVBQzVCLFFBQW1CO1FBRm5CLGFBQVEsR0FBUixRQUFRLENBQVU7UUFBWSxlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3BELGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBQzVCLGFBQVEsR0FBUixRQUFRLENBQVc7UUFHaEMsc0JBQWlCLEdBQUcsS0FBSyxDQUFDO1FBQzFCLGlCQUFZLEdBQUcsS0FBSyxDQUFDO1FBQ3JCLG1CQUFjLEdBQUcsS0FBSyxDQUFDO1FBQ3ZCLFdBQU0sR0FBRyxLQUFLLENBQUM7UUFFZixrQkFBYSxHQUFHLElBQUksQ0FBQztRQUV2QixhQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUM7UUFDbEMsY0FBUyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDO0lBVjNDLENBQUM7Ozs7SUFZRCxrQ0FBUTs7O0lBQVI7SUFDQSxDQUFDOzs7O0lBRUQsZ0NBQU07OztJQUFOO1FBQ0UsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLEVBQUUsQ0FBQztJQUNsQyxDQUFDOzs7O0lBRUQsd0NBQWM7OztJQUFkO1FBQ0UsSUFBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLEVBQUUsQ0FBQztJQUNuQyxDQUFDOztnQkEvQkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxjQUFjO29CQUN4QixxMkRBQXNDO29CQUV0QyxlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTs7aUJBQ2hEOzs7O2dCQVZPLFFBQVE7Z0JBQ1IsVUFBVTtnQkFFVixhQUFhO2dCQUpzQyxTQUFTOzs7b0NBa0JqRSxLQUFLOytCQUNMLEtBQUs7aUNBQ0wsS0FBSzt5QkFDTCxLQUFLOzZCQUNMLFNBQVMsU0FBQyxVQUFVO2dDQUNwQixLQUFLOztJQWdCUixzQkFBQztDQUFBLEFBakNELElBaUNDO1NBM0JZLGVBQWU7OztJQU0xQiw0Q0FBbUM7O0lBQ25DLHVDQUE4Qjs7SUFDOUIseUNBQWdDOztJQUNoQyxpQ0FBd0I7O0lBQ3hCLHFDQUE4Qzs7SUFDOUMsd0NBQThCOztJQUU5QixtQ0FBeUM7O0lBQ3pDLG9DQUEyQzs7Ozs7SUFiL0IsbUNBQTRCOzs7OztJQUFFLHFDQUFnQzs7Ozs7SUFDOUQsd0NBQXNDOzs7OztJQUN0QyxtQ0FBNkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NoYW5nZURldGVjdGlvblN0cmF0ZWd5LCBDb21wb25lbnQsIElucHV0LCBPbkluaXQsIFJlbmRlcmVyMiwgVmlld0NoaWxkfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7TmF2UXVlcnl9IGZyb20gJy4uL25hdi9zdGF0ZS9uYXYucXVlcnknO1xuaW1wb3J0IHtOYXZTZXJ2aWNlfSBmcm9tICcuLi9uYXYvc3RhdGUvbmF2LnNlcnZpY2UnO1xuaW1wb3J0IHtNYXRUb29sYmFyfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XG5pbXBvcnQge0xheW91dFNlcnZpY2V9IGZyb20gJy4uL2xheW91dC5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAndWlraXQtaGVhZGVyJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2hlYWRlci5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2hlYWRlci5jb21wb25lbnQuc2NzcyddLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaFxufSlcbmV4cG9ydCBjbGFzcyBIZWFkZXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBjb25zdHJ1Y3Rvcihwcm90ZWN0ZWQgbmF2UXVlcnk6IE5hdlF1ZXJ5LCBwcm90ZWN0ZWQgbmF2U2VydmljZTogTmF2U2VydmljZSxcbiAgICAgICAgICAgICAgcHJvdGVjdGVkIGxheW91dFNlcnZpY2U6IExheW91dFNlcnZpY2UsXG4gICAgICAgICAgICAgIHByb3RlY3RlZCByZW5kZXJlcjogUmVuZGVyZXIyKSB7XG4gIH1cblxuICBASW5wdXQoKSBzaG93Tm90aWZpY2F0aW9ucyA9IGZhbHNlO1xuICBASW5wdXQoKSBzaG93VXNlckluZm8gPSBmYWxzZTtcbiAgQElucHV0KCkgc2hvd1N5c3RlbUluZm8gPSBmYWxzZTtcbiAgQElucHV0KCkgc2hvd0lhID0gZmFsc2U7XG4gIEBWaWV3Q2hpbGQoTWF0VG9vbGJhcikgbWF0VG9vbGJhcjogTWF0VG9vbGJhcjtcbiAgQElucHV0KCkgc2hvd1Nob3J0Y3V0cyA9IHRydWU7XG5cbiAgcHVibGljIGxlZnROYXYkID0gdGhpcy5uYXZRdWVyeS5sZWZ0bmF2JDtcbiAgcHVibGljIHJpZ2h0TmF2JCA9IHRoaXMubmF2UXVlcnkucmlnaHRuYXYkO1xuXG4gIG5nT25Jbml0KCkge1xuICB9XG5cbiAgdG9nZ2xlKCkge1xuICAgIHRoaXMubmF2U2VydmljZS50b2dnbGVMZWZ0TmF2KCk7XG4gIH1cblxuICByaWdodE5hdlRvZ2dsZSgpIHtcbiAgICB0aGlzLm5hdlNlcnZpY2UudG9nZ2xlUmlnaHROYXYoKTtcbiAgfVxuXG59XG4iXX0=