/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ChangeDetectionStrategy } from '@angular/core';
import { AuthService } from '../../../shared/auth/auth.service';
import { ToastService } from '../../toast/toast.service';
var UserinfoComponent = /** @class */ (function () {
    function UserinfoComponent(authService, toastService) {
        this.authService = authService;
        this.toastService = toastService;
        this.user$ = this.authService.user.user$;
    }
    /**
     * @return {?}
     */
    UserinfoComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @return {?}
     */
    UserinfoComponent.prototype.login = /**
     * @return {?}
     */
    function () {
        var _this = this;
        return this.authService.authentication.login()
            .catch((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            _this.toastService.error('Error ao realizar o login!', (/** @type {?} */ (error)));
        }));
    };
    /**
     * @return {?}
     */
    UserinfoComponent.prototype.logout = /**
     * @return {?}
     */
    function () {
        try {
            this.authService.authentication.logout();
        }
        catch (error) {
            this.toastService.error('Error ao realizar o logout!', (/** @type {?} */ (error)));
        }
    };
    UserinfoComponent.decorators = [
        { type: Component, args: [{
                    selector: 'uikit-userinfo',
                    template: "<button *ngIf=\"(user$ | async).authenticated === false; else notAuthenticated\" mat-raised-button (click)=\"login()\"\n  class=\"btn-user-login\" color=\"primary\" tabindex=\"6\">\n  <span>Fazer Login</span>\n</button>\n\n<ng-template #notAuthenticated>\n  <mat-menu #userInfoMenu=\"matMenu\">\n    <mat-list class=\"dropdown-list\" (click)=\"$event.stopPropagation()\">\n      <div class=\"logged-info\">\n        <mat-list>\n          <mat-list-item>\n            <div mat-line>\n              <h4>\n\n                <b>\n                  {{ (user$ | async).nickname ? (user$ | async).nickname : '' }}\n                </b>\n\n                <br>\n\n                <i style=\"font-size: 13px;\">\n                  {{ (user$ | async).nickname && (user$ | async).name ? ' registrado(a) civilmente como ' : '' }}\n                </i>\n\n                <br>\n\n                <b style=\"font-size: 13px;\">\n                  {{ (user$ | async).name ? (user$ | async).name : '' }}\n                </b>\n\n              </h4>\n            </div>\n            <div mat-line *ngIf=\"(user$ | async).email\">\n              <h4>{{ (user$ | async).email }}</h4>\n            </div>\n          </mat-list-item>\n        </mat-list>\n      </div>\n\n      <mat-list *ngIf=\"(user$ | async) as user\">\n        <mat-list-item class=\"profiles\" *ngIf=\"user.roles as roles\">\n          <div mat-line>\n            <h4>\n              <mat-icon class=\"fas fa-id-card\"></mat-icon>\n              {{ roles.length === 0 ? 'Sem perfil' : (roles.length === 1 ? 'Perfil' : 'Perfis') }}\n            </h4>\n          </div>\n          <div mat-line *ngFor=\"let role of roles\">\n            {{ role }}\n          </div>\n        </mat-list-item>\n      </mat-list>\n\n      <mat-list>\n        <mat-list-item style=\"min-height: auto !important;\">\n          <div mat-line>\n            <ng-content></ng-content>\n          </div>\n        </mat-list-item>\n      </mat-list>\n\n      <mat-divider></mat-divider>\n      <mat-action-list>\n        <a (click)=\"logout()\" mat-list-item>Sair</a>\n      </mat-action-list>\n    </mat-list>\n  </mat-menu>\n\n  <button tabindex=\"6\" mat-button class=\"btn-user-info\" color=\"primary\" [matMenuTriggerFor]=\"userInfoMenu\"\n    matTooltip=\"Usu\u00E1rio Autenticado\" aria-label=\"Usu\u00E1rio Autenticado\">\n    <div class=\"userinfo\">\n      <div class=\"photo\">\n        <div class=\"picture\">\n          <mat-icon *ngIf=\"!(user$ | async).picture\" aria-label=\"Example icon-button with a heart icon\"\n            class=\"fas fa-user\"></mat-icon>\n          <img *ngIf=\"(user$ | async).picture\" [src]=\"(user$ | async).picture\" alt=\"Foto do usu\u00E1rio logado\" />\n        </div>\n        <mat-icon class=\"fas fa-chevron-circle-down\"></mat-icon>\n      </div>\n\n      <div class=\"info\">\n        <span class=\"name\">{{ (user$ | async).nickname || (user$ | async).given_name || (user$ | async).name }}</span>\n        <!-- <span *ngFor=\"let role of (user$ | async).roles\" class=\"location\">{{ role }}</span> -->\n      </div>\n    </div>\n  </button>\n</ng-template>\n",
                    changeDetection: ChangeDetectionStrategy.OnPush,
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    UserinfoComponent.ctorParameters = function () { return [
        { type: AuthService },
        { type: ToastService }
    ]; };
    return UserinfoComponent;
}());
export { UserinfoComponent };
if (false) {
    /** @type {?} */
    UserinfoComponent.prototype.user$;
    /**
     * @type {?}
     * @protected
     */
    UserinfoComponent.prototype.authService;
    /**
     * @type {?}
     * @protected
     */
    UserinfoComponent.prototype.toastService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlcmluZm8uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9sYXlvdXQvaGVhZGVyL3VzZXJpbmZvL3VzZXJpbmZvLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFNBQVMsRUFBVSx1QkFBdUIsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUV6RSxPQUFPLEVBQUMsV0FBVyxFQUFDLE1BQU0sbUNBQW1DLENBQUM7QUFDOUQsT0FBTyxFQUFDLFlBQVksRUFBQyxNQUFNLDJCQUEyQixDQUFDO0FBRXZEO0lBU0UsMkJBQ1ksV0FBd0IsRUFDeEIsWUFBMEI7UUFEMUIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsaUJBQVksR0FBWixZQUFZLENBQWM7UUFKdEMsVUFBSyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztJQUtwQyxDQUFDOzs7O0lBRUQsb0NBQVE7OztJQUFSO0lBRUEsQ0FBQzs7OztJQUVNLGlDQUFLOzs7SUFBWjtRQUFBLGlCQUtDO1FBSkMsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxLQUFLLEVBQUU7YUFDM0MsS0FBSzs7OztRQUFDLFVBQUEsS0FBSztZQUNWLEtBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLDRCQUE0QixFQUFFLG1CQUFBLEtBQUssRUFBUyxDQUFDLENBQUM7UUFDeEUsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7O0lBRU0sa0NBQU07OztJQUFiO1FBQ0UsSUFBSTtZQUNGLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLE1BQU0sRUFBRSxDQUFDO1NBQzFDO1FBQUMsT0FBTyxLQUFLLEVBQUU7WUFDZCxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyw2QkFBNkIsRUFBRSxtQkFBQSxLQUFLLEVBQVMsQ0FBQyxDQUFDO1NBQ3hFO0lBQ0gsQ0FBQzs7Z0JBL0JGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsZ0JBQWdCO29CQUMxQiw0a0dBQXdDO29CQUV4QyxlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTs7aUJBQ2hEOzs7O2dCQVJPLFdBQVc7Z0JBQ1gsWUFBWTs7SUFrQ3BCLHdCQUFDO0NBQUEsQUFoQ0QsSUFnQ0M7U0ExQlksaUJBQWlCOzs7SUFDNUIsa0NBQW9DOzs7OztJQUdsQyx3Q0FBa0M7Ozs7O0lBQ2xDLHlDQUFvQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBPbkluaXQsIENoYW5nZURldGVjdGlvblN0cmF0ZWd5fSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHtBdXRoU2VydmljZX0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL2F1dGgvYXV0aC5zZXJ2aWNlJztcbmltcG9ydCB7VG9hc3RTZXJ2aWNlfSBmcm9tICcuLi8uLi90b2FzdC90b2FzdC5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAndWlraXQtdXNlcmluZm8nLFxuICB0ZW1wbGF0ZVVybDogJy4vdXNlcmluZm8uY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi91c2VyaW5mby5jb21wb25lbnQuc2NzcyddLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaFxufSlcbmV4cG9ydCBjbGFzcyBVc2VyaW5mb0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIHVzZXIkID0gdGhpcy5hdXRoU2VydmljZS51c2VyLnVzZXIkO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByb3RlY3RlZCBhdXRoU2VydmljZTogQXV0aFNlcnZpY2UsXG4gICAgcHJvdGVjdGVkIHRvYXN0U2VydmljZTogVG9hc3RTZXJ2aWNlKSB7XG4gIH1cblxuICBuZ09uSW5pdCgpIHtcblxuICB9XG5cbiAgcHVibGljIGxvZ2luKCkge1xuICAgIHJldHVybiB0aGlzLmF1dGhTZXJ2aWNlLmF1dGhlbnRpY2F0aW9uLmxvZ2luKClcbiAgICAgIC5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIHRoaXMudG9hc3RTZXJ2aWNlLmVycm9yKCdFcnJvciBhbyByZWFsaXphciBvIGxvZ2luIScsIGVycm9yIGFzIEVycm9yKTtcbiAgICAgIH0pO1xuICB9XG5cbiAgcHVibGljIGxvZ291dCgpIHtcbiAgICB0cnkge1xuICAgICAgdGhpcy5hdXRoU2VydmljZS5hdXRoZW50aWNhdGlvbi5sb2dvdXQoKTtcbiAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgdGhpcy50b2FzdFNlcnZpY2UuZXJyb3IoJ0Vycm9yIGFvIHJlYWxpemFyIG8gbG9nb3V0IScsIGVycm9yIGFzIEVycm9yKTtcbiAgICB9XG4gIH1cbn1cbiJdfQ==