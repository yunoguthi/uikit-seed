/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ChangeDetectionStrategy } from '@angular/core';
import { NavQuery } from './state/nav.query';
import { NavService } from './state/nav.service';
var NavComponent = /** @class */ (function () {
    function NavComponent(navService, navQuery) {
        this.navService = navService;
        this.navQuery = navQuery;
        this.leftNav$ = this.navQuery.leftnav$;
        this.hasFavoritosService = this.navQuery.hasFavoritosService;
    }
    /**
     * @return {?}
     */
    NavComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () { };
    /**
     * @return {?}
     */
    NavComponent.prototype.togglePinLeftNav = /**
     * @return {?}
     */
    function () {
        this.navService.togglePinLeftNav();
    };
    NavComponent.decorators = [
        { type: Component, args: [{
                    selector: 'uikit-nav',
                    template: "<button tabindex=\"-1\" mat-icon-button color=\"primary\" class=\"btn-attach\" (click)=\"togglePinLeftNav()\"\n        [class.is-active]=\"(leftNav$ | async).opened\" [class.is-pinned]=\"(leftNav$ | async).pinned\">\n  <mat-icon class=\"fas fa-thumbtack\" aria-label=\"Fixar menu na tela\"></mat-icon>\n</button>\n\n<uikit-menu>\n  <div menu>\n\n      <ng-content select=\"[nav-menu]\"></ng-content>\n\n  </div>\n</uikit-menu>\n<uikit-favnav *ngIf=\"hasFavoritosService\"></uikit-favnav>\n",
                    changeDetection: ChangeDetectionStrategy.OnPush,
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    NavComponent.ctorParameters = function () { return [
        { type: NavService },
        { type: NavQuery }
    ]; };
    return NavComponent;
}());
export { NavComponent };
if (false) {
    /** @type {?} */
    NavComponent.prototype.leftNav$;
    /** @type {?} */
    NavComponent.prototype.hasFavoritosService;
    /**
     * @type {?}
     * @protected
     */
    NavComponent.prototype.navService;
    /**
     * @type {?}
     * @protected
     */
    NavComponent.prototype.navQuery;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvbGF5b3V0L25hdi9uYXYuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLHVCQUF1QixFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNFLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUM3QyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFFakQ7SUFXRSxzQkFDWSxVQUFzQixFQUN0QixRQUFrQjtRQURsQixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3RCLGFBQVEsR0FBUixRQUFRLENBQVU7UUFOdkIsYUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDO1FBRWxDLHdCQUFtQixHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUM7SUFLMUQsQ0FBQzs7OztJQUVOLCtCQUFROzs7SUFBUixjQUFZLENBQUM7Ozs7SUFFYix1Q0FBZ0I7OztJQUFoQjtRQUNFLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztJQUNyQyxDQUFDOztnQkFwQkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxXQUFXO29CQUNyQixxZkFBbUM7b0JBRW5DLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNOztpQkFDaEQ7Ozs7Z0JBUFEsVUFBVTtnQkFEVixRQUFROztJQXdCakIsbUJBQUM7Q0FBQSxBQXJCRCxJQXFCQztTQWZZLFlBQVk7OztJQUN2QixnQ0FBeUM7O0lBRXpDLDJDQUErRDs7Ozs7SUFHN0Qsa0NBQWdDOzs7OztJQUNoQyxnQ0FBNEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE5hdlF1ZXJ5IH0gZnJvbSAnLi9zdGF0ZS9uYXYucXVlcnknO1xuaW1wb3J0IHsgTmF2U2VydmljZSB9IGZyb20gJy4vc3RhdGUvbmF2LnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICd1aWtpdC1uYXYnLFxuICB0ZW1wbGF0ZVVybDogJy4vbmF2LmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vbmF2LmNvbXBvbmVudC5zY3NzJ10sXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoXG59KVxuZXhwb3J0IGNsYXNzIE5hdkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIHB1YmxpYyBsZWZ0TmF2JCA9IHRoaXMubmF2UXVlcnkubGVmdG5hdiQ7XG5cbiAgcHVibGljIGhhc0Zhdm9yaXRvc1NlcnZpY2UgPSB0aGlzLm5hdlF1ZXJ5Lmhhc0Zhdm9yaXRvc1NlcnZpY2U7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJvdGVjdGVkIG5hdlNlcnZpY2U6IE5hdlNlcnZpY2UsXG4gICAgcHJvdGVjdGVkIG5hdlF1ZXJ5OiBOYXZRdWVyeSxcbiAgICApIHt9XG5cbiAgbmdPbkluaXQoKSB7fVxuXG4gIHRvZ2dsZVBpbkxlZnROYXYoKSB7XG4gICAgdGhpcy5uYXZTZXJ2aWNlLnRvZ2dsZVBpbkxlZnROYXYoKTtcbiAgfVxufVxuIl19