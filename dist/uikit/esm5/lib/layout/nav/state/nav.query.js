/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable, Optional, Inject } from '@angular/core';
import { Query } from '@datorama/akita';
import { NavStore } from './nav.store';
import { FavoritosService, FAVORITOS_SERVICE_TOKEN } from '../../../utils/favoritos.service';
import * as i0 from "@angular/core";
import * as i1 from "./nav.store";
import * as i2 from "../../../utils/favoritos.service";
var NavQuery = /** @class */ (function (_super) {
    tslib_1.__extends(NavQuery, _super);
    function NavQuery(store, favoritosService) {
        var _this = _super.call(this, store) || this;
        _this.store = store;
        _this.favoritosService = favoritosService;
        _this.leftnav$ = _this.select((/**
         * @param {?} session
         * @return {?}
         */
        function (session) { return session.leftnav; }));
        _this.rightnav$ = _this.select((/**
         * @param {?} session
         * @return {?}
         */
        function (session) { return session.rightnav; }));
        _this.isLeftPinned$ = _this.select((/**
         * @param {?} session
         * @return {?}
         */
        function (session) { return session.leftnav.pinned; }));
        _this.isRightPinned$ = _this.select((/**
         * @param {?} session
         * @return {?}
         */
        function (session) { return session.rightnav.pinned; }));
        _this.hasFavoritosService = _this.favoritosService != null;
        return _this;
    }
    NavQuery.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    NavQuery.ctorParameters = function () { return [
        { type: NavStore },
        { type: FavoritosService, decorators: [{ type: Optional }, { type: Inject, args: [FAVORITOS_SERVICE_TOKEN,] }] }
    ]; };
    /** @nocollapse */ NavQuery.ngInjectableDef = i0.defineInjectable({ factory: function NavQuery_Factory() { return new NavQuery(i0.inject(i1.NavStore), i0.inject(i2.FAVORITOS_SERVICE_TOKEN, 8)); }, token: NavQuery, providedIn: "root" });
    return NavQuery;
}(Query));
export { NavQuery };
if (false) {
    /** @type {?} */
    NavQuery.prototype.leftnav$;
    /** @type {?} */
    NavQuery.prototype.rightnav$;
    /** @type {?} */
    NavQuery.prototype.isLeftPinned$;
    /** @type {?} */
    NavQuery.prototype.isRightPinned$;
    /** @type {?} */
    NavQuery.prototype.hasFavoritosService;
    /**
     * @type {?}
     * @protected
     */
    NavQuery.prototype.store;
    /**
     * @type {?}
     * @protected
     */
    NavQuery.prototype.favoritosService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2LnF1ZXJ5LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9sYXlvdXQvbmF2L3N0YXRlL25hdi5xdWVyeS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM3RCxPQUFPLEVBQWUsS0FBSyxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDckQsT0FBTyxFQUFFLFFBQVEsRUFBWSxNQUFNLGFBQWEsQ0FBQztBQUNqRCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQzs7OztBQUU3RjtJQUc4QixvQ0FBZTtJQVUzQyxrQkFDWSxLQUFlLEVBQzhCLGdCQUFrQztRQUYzRixZQUlFLGtCQUFNLEtBQUssQ0FBQyxTQUNiO1FBSlcsV0FBSyxHQUFMLEtBQUssQ0FBVTtRQUM4QixzQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBVnBGLGNBQVEsR0FBRyxLQUFJLENBQUMsTUFBTTs7OztRQUFDLFVBQUEsT0FBTyxJQUFJLE9BQUEsT0FBTyxDQUFDLE9BQU8sRUFBZixDQUFlLEVBQUMsQ0FBQztRQUNuRCxlQUFTLEdBQUcsS0FBSSxDQUFDLE1BQU07Ozs7UUFBQyxVQUFBLE9BQU8sSUFBSSxPQUFBLE9BQU8sQ0FBQyxRQUFRLEVBQWhCLENBQWdCLEVBQUMsQ0FBQztRQUVyRCxtQkFBYSxHQUFHLEtBQUksQ0FBQyxNQUFNOzs7O1FBQUMsVUFBQSxPQUFPLElBQUksT0FBQSxPQUFPLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBdEIsQ0FBc0IsRUFBQyxDQUFDO1FBQy9ELG9CQUFjLEdBQUcsS0FBSSxDQUFDLE1BQU07Ozs7UUFBQyxVQUFBLE9BQU8sSUFBSSxPQUFBLE9BQU8sQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUF2QixDQUF1QixFQUFDLENBQUM7UUFFakUseUJBQW1CLEdBQUcsS0FBSSxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQzs7SUFPM0QsQ0FBQzs7Z0JBbEJGLFVBQVUsU0FBQztvQkFDVixVQUFVLEVBQUUsTUFBTTtpQkFDbkI7Ozs7Z0JBTFEsUUFBUTtnQkFDUixnQkFBZ0IsdUJBaUJwQixRQUFRLFlBQUksTUFBTSxTQUFDLHVCQUF1Qjs7O21CQXBCL0M7Q0EyQkMsQUF0QkQsQ0FHOEIsS0FBSyxHQW1CbEM7U0FuQlksUUFBUTs7O0lBRW5CLDRCQUEwRDs7SUFDMUQsNkJBQTREOztJQUU1RCxpQ0FBc0U7O0lBQ3RFLGtDQUF3RTs7SUFFeEUsdUNBQTJEOzs7OztJQUd6RCx5QkFBeUI7Ozs7O0lBQ3pCLG9DQUF5RiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIE9wdGlvbmFsLCBJbmplY3QgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFF1ZXJ5RW50aXR5LCBRdWVyeSB9IGZyb20gJ0BkYXRvcmFtYS9ha2l0YSc7XG5pbXBvcnQgeyBOYXZTdG9yZSwgTmF2U3RhdGUgfSBmcm9tICcuL25hdi5zdG9yZSc7XG5pbXBvcnQgeyBGYXZvcml0b3NTZXJ2aWNlLCBGQVZPUklUT1NfU0VSVklDRV9UT0tFTiB9IGZyb20gJy4uLy4uLy4uL3V0aWxzL2Zhdm9yaXRvcy5zZXJ2aWNlJztcblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgTmF2UXVlcnkgZXh0ZW5kcyBRdWVyeTxOYXZTdGF0ZT4ge1xuXG4gIHB1YmxpYyBsZWZ0bmF2JCA9IHRoaXMuc2VsZWN0KHNlc3Npb24gPT4gc2Vzc2lvbi5sZWZ0bmF2KTtcbiAgcHVibGljIHJpZ2h0bmF2JCA9IHRoaXMuc2VsZWN0KHNlc3Npb24gPT4gc2Vzc2lvbi5yaWdodG5hdik7XG5cbiAgcHVibGljIGlzTGVmdFBpbm5lZCQgPSB0aGlzLnNlbGVjdChzZXNzaW9uID0+IHNlc3Npb24ubGVmdG5hdi5waW5uZWQpO1xuICBwdWJsaWMgaXNSaWdodFBpbm5lZCQgPSB0aGlzLnNlbGVjdChzZXNzaW9uID0+IHNlc3Npb24ucmlnaHRuYXYucGlubmVkKTtcblxuICBwdWJsaWMgaGFzRmF2b3JpdG9zU2VydmljZSA9IHRoaXMuZmF2b3JpdG9zU2VydmljZSAhPSBudWxsO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByb3RlY3RlZCBzdG9yZTogTmF2U3RvcmUsXG4gICAgQE9wdGlvbmFsKCkgQEluamVjdChGQVZPUklUT1NfU0VSVklDRV9UT0tFTikgcHJvdGVjdGVkIGZhdm9yaXRvc1NlcnZpY2U6IEZhdm9yaXRvc1NlcnZpY2VcbiAgICApIHtcbiAgICBzdXBlcihzdG9yZSk7XG4gIH1cblxuXG5cbn1cbiJdfQ==