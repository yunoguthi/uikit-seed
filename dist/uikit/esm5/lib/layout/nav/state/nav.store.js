/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { StoreConfig, Store } from '@datorama/akita';
import * as i0 from "@angular/core";
/**
 * @record
 */
export function NavState() { }
if (false) {
    /** @type {?} */
    NavState.prototype.leftnav;
    /** @type {?} */
    NavState.prototype.rightnav;
}
/**
 * @return {?}
 */
export function createInitialState() {
    return {
        leftnav: {
            opened: false,
            pinned: false
        },
        rightnav: {
            opened: false,
            pinned: false
        }
    };
}
var NavStore = /** @class */ (function (_super) {
    tslib_1.__extends(NavStore, _super);
    function NavStore() {
        return _super.call(this, createInitialState()) || this;
    }
    NavStore.decorators = [
        { type: Injectable, args: [{ providedIn: 'root' },] }
    ];
    /** @nocollapse */
    NavStore.ctorParameters = function () { return []; };
    /** @nocollapse */ NavStore.ngInjectableDef = i0.defineInjectable({ factory: function NavStore_Factory() { return new NavStore(); }, token: NavStore, providedIn: "root" });
    NavStore = tslib_1.__decorate([
        StoreConfig({ name: 'nav' }),
        tslib_1.__metadata("design:paramtypes", [])
    ], NavStore);
    return NavStore;
}(Store));
export { NavStore };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2LnN0b3JlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9sYXlvdXQvbmF2L3N0YXRlL25hdi5zdG9yZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUE0QixXQUFXLEVBQUUsS0FBSyxFQUFFLE1BQU0saUJBQWlCLENBQUM7Ozs7O0FBSS9FLDhCQVNDOzs7SUFSQywyQkFHRTs7SUFDRiw0QkFHRTs7Ozs7QUFHSixNQUFNLFVBQVUsa0JBQWtCO0lBQ2hDLE9BQU87UUFDTCxPQUFPLEVBQUU7WUFDUCxNQUFNLEVBQUUsS0FBSztZQUNiLE1BQU0sRUFBRSxLQUFLO1NBQ2Q7UUFDRCxRQUFRLEVBQUU7WUFDUixNQUFNLEVBQUUsS0FBSztZQUNiLE1BQU0sRUFBRSxLQUFLO1NBQ2Q7S0FDRixDQUFDO0FBQ0osQ0FBQzs7SUFJNkIsb0NBQWU7SUFDM0M7ZUFDRSxrQkFBTSxrQkFBa0IsRUFBRSxDQUFDO0lBQzdCLENBQUM7O2dCQUxGLFVBQVUsU0FBQyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUU7Ozs7O0lBRXJCLFFBQVE7UUFEcEIsV0FBVyxDQUFDLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBRSxDQUFDOztPQUNoQixRQUFRLENBSXBCO21CQW5DRDtDQW1DQyxDQUo2QixLQUFLLEdBSWxDO1NBSlksUUFBUSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEVudGl0eVN0YXRlLCBFbnRpdHlTdG9yZSwgU3RvcmVDb25maWcsIFN0b3JlIH0gZnJvbSAnQGRhdG9yYW1hL2FraXRhJztcblxuXG5cbmV4cG9ydCBpbnRlcmZhY2UgTmF2U3RhdGUge1xuICBsZWZ0bmF2OiB7XG4gICAgb3BlbmVkOiBib29sZWFuO1xuICAgIHBpbm5lZDogYm9vbGVhbjtcbiAgfTtcbiAgcmlnaHRuYXY6IHtcbiAgICBvcGVuZWQ6IGJvb2xlYW47XG4gICAgcGlubmVkOiBib29sZWFuO1xuICB9O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gY3JlYXRlSW5pdGlhbFN0YXRlKCk6IE5hdlN0YXRlIHtcbiAgcmV0dXJuIHtcbiAgICBsZWZ0bmF2OiB7XG4gICAgICBvcGVuZWQ6IGZhbHNlLFxuICAgICAgcGlubmVkOiBmYWxzZVxuICAgIH0sXG4gICAgcmlnaHRuYXY6IHtcbiAgICAgIG9wZW5lZDogZmFsc2UsXG4gICAgICBwaW5uZWQ6IGZhbHNlXG4gICAgfVxuICB9O1xufVxuXG5ASW5qZWN0YWJsZSh7IHByb3ZpZGVkSW46ICdyb290JyB9KVxuQFN0b3JlQ29uZmlnKHsgbmFtZTogJ25hdicgfSlcbmV4cG9ydCBjbGFzcyBOYXZTdG9yZSBleHRlbmRzIFN0b3JlPE5hdlN0YXRlPiB7XG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHN1cGVyKGNyZWF0ZUluaXRpYWxTdGF0ZSgpKTtcbiAgfVxufVxuIl19