/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { NavStore } from './nav.store';
import * as i0 from "@angular/core";
import * as i1 from "./nav.store";
var NavService = /** @class */ (function () {
    function NavService(navStore) {
        this.navStore = navStore;
    }
    /**
     * @return {?}
     */
    NavService.prototype.toggleLeftNav = /**
     * @return {?}
     */
    function () {
        this.navStore.update((/**
         * @param {?} state
         * @return {?}
         */
        function (state) { return ({
            leftnav: tslib_1.__assign({}, state.leftnav, { opened: !state.leftnav.opened, pinned: false })
        }); }));
    };
    /**
     * @return {?}
     */
    NavService.prototype.openLeftNav = /**
     * @return {?}
     */
    function () {
        this.navStore.update((/**
         * @param {?} state
         * @return {?}
         */
        function (state) { return ({
            leftnav: tslib_1.__assign({}, state.leftnav, { opened: true })
        }); }));
    };
    /**
     * @return {?}
     */
    NavService.prototype.closeLeftNav = /**
     * @return {?}
     */
    function () {
        this.navStore.update((/**
         * @param {?} state
         * @return {?}
         */
        function (state) { return ({
            leftnav: tslib_1.__assign({}, state.leftnav, { opened: false, pinned: false })
        }); }));
    };
    /**
     * @return {?}
     */
    NavService.prototype.pinLeftNav = /**
     * @return {?}
     */
    function () {
        this.navStore.update((/**
         * @param {?} state
         * @return {?}
         */
        function (state) { return ({
            leftnav: tslib_1.__assign({}, state.leftnav, { pinned: true })
        }); }));
    };
    /**
     * @return {?}
     */
    NavService.prototype.unpinLeftNav = /**
     * @return {?}
     */
    function () {
        this.navStore.update((/**
         * @param {?} state
         * @return {?}
         */
        function (state) { return ({
            leftnav: tslib_1.__assign({}, state.leftnav, { pinned: false })
        }); }));
    };
    /**
     * @return {?}
     */
    NavService.prototype.togglePinLeftNav = /**
     * @return {?}
     */
    function () {
        this.navStore.update((/**
         * @param {?} state
         * @return {?}
         */
        function (state) { return ({
            leftnav: tslib_1.__assign({}, state.leftnav, { pinned: !state.leftnav.pinned })
        }); }));
    };
    /**
     * @return {?}
     */
    NavService.prototype.toggleRightNav = /**
     * @return {?}
     */
    function () {
        this.navStore.update((/**
         * @param {?} state
         * @return {?}
         */
        function (state) { return ({
            rightnav: tslib_1.__assign({}, state.rightnav, { opened: !state.rightnav.opened, pinned: false })
        }); }));
    };
    /**
     * @return {?}
     */
    NavService.prototype.openRightNav = /**
     * @return {?}
     */
    function () {
        this.navStore.update((/**
         * @param {?} state
         * @return {?}
         */
        function (state) { return ({
            rightnav: tslib_1.__assign({}, state.rightnav, { opened: true })
        }); }));
    };
    /**
     * @return {?}
     */
    NavService.prototype.closeRightNav = /**
     * @return {?}
     */
    function () {
        this.navStore.update((/**
         * @param {?} state
         * @return {?}
         */
        function (state) { return ({
            rightnav: tslib_1.__assign({}, state.rightnav, { opened: false, pinned: false })
        }); }));
    };
    /**
     * @return {?}
     */
    NavService.prototype.pinRightNav = /**
     * @return {?}
     */
    function () {
        this.navStore.update((/**
         * @param {?} state
         * @return {?}
         */
        function (state) { return ({
            rightnav: tslib_1.__assign({}, state.rightnav, { pinned: true })
        }); }));
    };
    /**
     * @return {?}
     */
    NavService.prototype.unpinRightNav = /**
     * @return {?}
     */
    function () {
        this.navStore.update((/**
         * @param {?} state
         * @return {?}
         */
        function (state) { return ({
            rightnav: tslib_1.__assign({}, state.rightnav, { pinned: false })
        }); }));
    };
    /**
     * @return {?}
     */
    NavService.prototype.togglePinRightNav = /**
     * @return {?}
     */
    function () {
        this.navStore.update((/**
         * @param {?} state
         * @return {?}
         */
        function (state) { return ({
            rightnav: tslib_1.__assign({}, state.rightnav, { pinned: !state.rightnav.pinned })
        }); }));
    };
    NavService.decorators = [
        { type: Injectable, args: [{ providedIn: 'root' },] }
    ];
    /** @nocollapse */
    NavService.ctorParameters = function () { return [
        { type: NavStore }
    ]; };
    /** @nocollapse */ NavService.ngInjectableDef = i0.defineInjectable({ factory: function NavService_Factory() { return new NavService(i0.inject(i1.NavStore)); }, token: NavService, providedIn: "root" });
    return NavService;
}());
export { NavService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    NavService.prototype.navStore;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2LnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL2xheW91dC9uYXYvc3RhdGUvbmF2LnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxhQUFhLENBQUM7OztBQUV2QztJQUVFLG9CQUFvQixRQUFrQjtRQUFsQixhQUFRLEdBQVIsUUFBUSxDQUFVO0lBQUcsQ0FBQzs7OztJQUUxQyxrQ0FBYTs7O0lBQWI7UUFDRSxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU07Ozs7UUFBQyxVQUFBLEtBQUssSUFBSSxPQUFBLENBQUM7WUFDN0IsT0FBTyx1QkFDRixLQUFLLENBQUMsT0FBTyxJQUNoQixNQUFNLEVBQUUsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFDN0IsTUFBTSxFQUFFLEtBQUssR0FDZDtTQUNGLENBQUMsRUFONEIsQ0FNNUIsRUFBQyxDQUFDO0lBQ04sQ0FBQzs7OztJQUVELGdDQUFXOzs7SUFBWDtRQUNFLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTTs7OztRQUFDLFVBQUEsS0FBSyxJQUFJLE9BQUEsQ0FBQztZQUM3QixPQUFPLHVCQUNGLEtBQUssQ0FBQyxPQUFPLElBQ2hCLE1BQU0sRUFBRSxJQUFJLEdBQ2I7U0FDRixDQUFDLEVBTDRCLENBSzVCLEVBQUMsQ0FBQztJQUNOLENBQUM7Ozs7SUFFRCxpQ0FBWTs7O0lBQVo7UUFDRSxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU07Ozs7UUFBQyxVQUFBLEtBQUssSUFBSSxPQUFBLENBQUM7WUFDN0IsT0FBTyx1QkFDRixLQUFLLENBQUMsT0FBTyxJQUNoQixNQUFNLEVBQUUsS0FBSyxFQUNiLE1BQU0sRUFBRSxLQUFLLEdBQ2Q7U0FDRixDQUFDLEVBTjRCLENBTTVCLEVBQUMsQ0FBQztJQUNOLENBQUM7Ozs7SUFFRCwrQkFBVTs7O0lBQVY7UUFDRSxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU07Ozs7UUFBQyxVQUFBLEtBQUssSUFBSSxPQUFBLENBQUM7WUFDN0IsT0FBTyx1QkFDRixLQUFLLENBQUMsT0FBTyxJQUNoQixNQUFNLEVBQUUsSUFBSSxHQUNiO1NBQ0YsQ0FBQyxFQUw0QixDQUs1QixFQUFDLENBQUM7SUFDTixDQUFDOzs7O0lBRUQsaUNBQVk7OztJQUFaO1FBQ0UsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNOzs7O1FBQUMsVUFBQSxLQUFLLElBQUksT0FBQSxDQUFDO1lBQzdCLE9BQU8sdUJBQ0YsS0FBSyxDQUFDLE9BQU8sSUFDaEIsTUFBTSxFQUFFLEtBQUssR0FDZDtTQUNGLENBQUMsRUFMNEIsQ0FLNUIsRUFBQyxDQUFDO0lBQ04sQ0FBQzs7OztJQUVELHFDQUFnQjs7O0lBQWhCO1FBQ0UsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNOzs7O1FBQUMsVUFBQSxLQUFLLElBQUksT0FBQSxDQUFDO1lBQzdCLE9BQU8sdUJBQ0YsS0FBSyxDQUFDLE9BQU8sSUFDaEIsTUFBTSxFQUFFLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQzlCO1NBQ0YsQ0FBQyxFQUw0QixDQUs1QixFQUFDLENBQUM7SUFDTixDQUFDOzs7O0lBV0QsbUNBQWM7OztJQUFkO1FBQ0UsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNOzs7O1FBQUMsVUFBQSxLQUFLLElBQUksT0FBQSxDQUFDO1lBQzdCLFFBQVEsdUJBQ0gsS0FBSyxDQUFDLFFBQVEsSUFDakIsTUFBTSxFQUFFLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQzlCLE1BQU0sRUFBRSxLQUFLLEdBQ2Q7U0FDRixDQUFDLEVBTjRCLENBTTVCLEVBQUMsQ0FBQztJQUNOLENBQUM7Ozs7SUFFRCxpQ0FBWTs7O0lBQVo7UUFDRSxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU07Ozs7UUFBQyxVQUFBLEtBQUssSUFBSSxPQUFBLENBQUM7WUFDN0IsUUFBUSx1QkFDSCxLQUFLLENBQUMsUUFBUSxJQUNqQixNQUFNLEVBQUUsSUFBSSxHQUNiO1NBQ0YsQ0FBQyxFQUw0QixDQUs1QixFQUFDLENBQUM7SUFDTixDQUFDOzs7O0lBRUQsa0NBQWE7OztJQUFiO1FBQ0UsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNOzs7O1FBQUMsVUFBQSxLQUFLLElBQUksT0FBQSxDQUFDO1lBQzdCLFFBQVEsdUJBQ0gsS0FBSyxDQUFDLFFBQVEsSUFDakIsTUFBTSxFQUFFLEtBQUssRUFDYixNQUFNLEVBQUUsS0FBSyxHQUNkO1NBQ0YsQ0FBQyxFQU40QixDQU01QixFQUFDLENBQUM7SUFDTixDQUFDOzs7O0lBRUQsZ0NBQVc7OztJQUFYO1FBQ0UsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNOzs7O1FBQUMsVUFBQSxLQUFLLElBQUksT0FBQSxDQUFDO1lBQzdCLFFBQVEsdUJBQ0gsS0FBSyxDQUFDLFFBQVEsSUFDakIsTUFBTSxFQUFFLElBQUksR0FDYjtTQUNGLENBQUMsRUFMNEIsQ0FLNUIsRUFBQyxDQUFDO0lBQ04sQ0FBQzs7OztJQUVELGtDQUFhOzs7SUFBYjtRQUNFLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTTs7OztRQUFDLFVBQUEsS0FBSyxJQUFJLE9BQUEsQ0FBQztZQUM3QixRQUFRLHVCQUNILEtBQUssQ0FBQyxRQUFRLElBQ2pCLE1BQU0sRUFBRSxLQUFLLEdBQ2Q7U0FDRixDQUFDLEVBTDRCLENBSzVCLEVBQUMsQ0FBQztJQUNOLENBQUM7Ozs7SUFFRCxzQ0FBaUI7OztJQUFqQjtRQUNFLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTTs7OztRQUFDLFVBQUEsS0FBSyxJQUFJLE9BQUEsQ0FBQztZQUM3QixRQUFRLHVCQUNILEtBQUssQ0FBQyxRQUFRLElBQ2pCLE1BQU0sRUFBRSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUMvQjtTQUNGLENBQUMsRUFMNEIsQ0FLNUIsRUFBQyxDQUFDO0lBQ04sQ0FBQzs7Z0JBM0hGLFVBQVUsU0FBQyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUU7Ozs7Z0JBRnpCLFFBQVE7OztxQkFEakI7Q0ErSEMsQUE1SEQsSUE0SEM7U0EzSFksVUFBVTs7Ozs7O0lBQ1QsOEJBQTBCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTmF2U3RvcmUgfSBmcm9tICcuL25hdi5zdG9yZSc7XG5cbkBJbmplY3RhYmxlKHsgcHJvdmlkZWRJbjogJ3Jvb3QnIH0pXG5leHBvcnQgY2xhc3MgTmF2U2VydmljZSB7XG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgbmF2U3RvcmU6IE5hdlN0b3JlKSB7fVxuXG4gIHRvZ2dsZUxlZnROYXYoKSB7XG4gICAgdGhpcy5uYXZTdG9yZS51cGRhdGUoc3RhdGUgPT4gKHtcbiAgICAgIGxlZnRuYXY6IHtcbiAgICAgICAgLi4uc3RhdGUubGVmdG5hdixcbiAgICAgICAgb3BlbmVkOiAhc3RhdGUubGVmdG5hdi5vcGVuZWQsXG4gICAgICAgIHBpbm5lZDogZmFsc2VcbiAgICAgIH1cbiAgICB9KSk7XG4gIH1cblxuICBvcGVuTGVmdE5hdigpIHtcbiAgICB0aGlzLm5hdlN0b3JlLnVwZGF0ZShzdGF0ZSA9PiAoe1xuICAgICAgbGVmdG5hdjoge1xuICAgICAgICAuLi5zdGF0ZS5sZWZ0bmF2LFxuICAgICAgICBvcGVuZWQ6IHRydWVcbiAgICAgIH1cbiAgICB9KSk7XG4gIH1cblxuICBjbG9zZUxlZnROYXYoKSB7XG4gICAgdGhpcy5uYXZTdG9yZS51cGRhdGUoc3RhdGUgPT4gKHtcbiAgICAgIGxlZnRuYXY6IHtcbiAgICAgICAgLi4uc3RhdGUubGVmdG5hdixcbiAgICAgICAgb3BlbmVkOiBmYWxzZSxcbiAgICAgICAgcGlubmVkOiBmYWxzZVxuICAgICAgfVxuICAgIH0pKTtcbiAgfVxuXG4gIHBpbkxlZnROYXYoKSB7XG4gICAgdGhpcy5uYXZTdG9yZS51cGRhdGUoc3RhdGUgPT4gKHtcbiAgICAgIGxlZnRuYXY6IHtcbiAgICAgICAgLi4uc3RhdGUubGVmdG5hdixcbiAgICAgICAgcGlubmVkOiB0cnVlXG4gICAgICB9XG4gICAgfSkpO1xuICB9XG5cbiAgdW5waW5MZWZ0TmF2KCkge1xuICAgIHRoaXMubmF2U3RvcmUudXBkYXRlKHN0YXRlID0+ICh7XG4gICAgICBsZWZ0bmF2OiB7XG4gICAgICAgIC4uLnN0YXRlLmxlZnRuYXYsXG4gICAgICAgIHBpbm5lZDogZmFsc2VcbiAgICAgIH1cbiAgICB9KSk7XG4gIH1cblxuICB0b2dnbGVQaW5MZWZ0TmF2KCkge1xuICAgIHRoaXMubmF2U3RvcmUudXBkYXRlKHN0YXRlID0+ICh7XG4gICAgICBsZWZ0bmF2OiB7XG4gICAgICAgIC4uLnN0YXRlLmxlZnRuYXYsXG4gICAgICAgIHBpbm5lZDogIXN0YXRlLmxlZnRuYXYucGlubmVkXG4gICAgICB9XG4gICAgfSkpO1xuICB9XG5cblxuXG5cblxuXG5cblxuXG5cbiAgdG9nZ2xlUmlnaHROYXYoKSB7XG4gICAgdGhpcy5uYXZTdG9yZS51cGRhdGUoc3RhdGUgPT4gKHtcbiAgICAgIHJpZ2h0bmF2OiB7XG4gICAgICAgIC4uLnN0YXRlLnJpZ2h0bmF2LFxuICAgICAgICBvcGVuZWQ6ICFzdGF0ZS5yaWdodG5hdi5vcGVuZWQsXG4gICAgICAgIHBpbm5lZDogZmFsc2VcbiAgICAgIH1cbiAgICB9KSk7XG4gIH1cblxuICBvcGVuUmlnaHROYXYoKSB7XG4gICAgdGhpcy5uYXZTdG9yZS51cGRhdGUoc3RhdGUgPT4gKHtcbiAgICAgIHJpZ2h0bmF2OiB7XG4gICAgICAgIC4uLnN0YXRlLnJpZ2h0bmF2LFxuICAgICAgICBvcGVuZWQ6IHRydWVcbiAgICAgIH1cbiAgICB9KSk7XG4gIH1cblxuICBjbG9zZVJpZ2h0TmF2KCkge1xuICAgIHRoaXMubmF2U3RvcmUudXBkYXRlKHN0YXRlID0+ICh7XG4gICAgICByaWdodG5hdjoge1xuICAgICAgICAuLi5zdGF0ZS5yaWdodG5hdixcbiAgICAgICAgb3BlbmVkOiBmYWxzZSxcbiAgICAgICAgcGlubmVkOiBmYWxzZVxuICAgICAgfVxuICAgIH0pKTtcbiAgfVxuXG4gIHBpblJpZ2h0TmF2KCkge1xuICAgIHRoaXMubmF2U3RvcmUudXBkYXRlKHN0YXRlID0+ICh7XG4gICAgICByaWdodG5hdjoge1xuICAgICAgICAuLi5zdGF0ZS5yaWdodG5hdixcbiAgICAgICAgcGlubmVkOiB0cnVlXG4gICAgICB9XG4gICAgfSkpO1xuICB9XG5cbiAgdW5waW5SaWdodE5hdigpIHtcbiAgICB0aGlzLm5hdlN0b3JlLnVwZGF0ZShzdGF0ZSA9PiAoe1xuICAgICAgcmlnaHRuYXY6IHtcbiAgICAgICAgLi4uc3RhdGUucmlnaHRuYXYsXG4gICAgICAgIHBpbm5lZDogZmFsc2VcbiAgICAgIH1cbiAgICB9KSk7XG4gIH1cblxuICB0b2dnbGVQaW5SaWdodE5hdigpIHtcbiAgICB0aGlzLm5hdlN0b3JlLnVwZGF0ZShzdGF0ZSA9PiAoe1xuICAgICAgcmlnaHRuYXY6IHtcbiAgICAgICAgLi4uc3RhdGUucmlnaHRuYXYsXG4gICAgICAgIHBpbm5lZDogIXN0YXRlLnJpZ2h0bmF2LnBpbm5lZFxuICAgICAgfVxuICAgIH0pKTtcbiAgfVxufVxuIl19