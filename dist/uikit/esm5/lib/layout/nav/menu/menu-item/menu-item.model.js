/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { guid } from '@datorama/akita';
/**
 * @record
 */
export function MenuItem() { }
if (false) {
    /** @type {?} */
    MenuItem.prototype.id;
    /** @type {?|undefined} */
    MenuItem.prototype.children;
    /** @type {?} */
    MenuItem.prototype.title;
    /** @type {?|undefined} */
    MenuItem.prototype.icon;
    /** @type {?|undefined} */
    MenuItem.prototype.link;
    /** @type {?|undefined} */
    MenuItem.prototype.tags;
    /** @type {?|undefined} */
    MenuItem.prototype.nodeId;
}
/**
 * @param {?} item
 * @return {?}
 */
export function createItem(item) {
    return (/** @type {?} */ (tslib_1.__assign({}, item, { id: guid() })));
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVudS1pdGVtLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9sYXlvdXQvbmF2L21lbnUvbWVudS1pdGVtL21lbnUtaXRlbS5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBTSxJQUFJLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQzs7OztBQUUzQyw4QkFRQzs7O0lBUEMsc0JBQU87O0lBQ1AsNEJBQXNCOztJQUN0Qix5QkFBYzs7SUFDZCx3QkFBYzs7SUFDZCx3QkFBVzs7SUFDWCx3QkFBZ0I7O0lBQ2hCLDBCQUFZOzs7Ozs7QUFHZCxNQUFNLFVBQVUsVUFBVSxDQUFDLElBQWM7SUFDdkMsT0FBTyx3Q0FDRixJQUFJLElBQ1AsRUFBRSxFQUFFLElBQUksRUFBRSxLQUNDLENBQUM7QUFDaEIsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IElELCBndWlkIH0gZnJvbSAnQGRhdG9yYW1hL2FraXRhJztcblxuZXhwb3J0IGludGVyZmFjZSBNZW51SXRlbSB7XG4gIGlkOiBJRDtcbiAgY2hpbGRyZW4/OiBNZW51SXRlbVtdO1xuICB0aXRsZTogc3RyaW5nO1xuICBpY29uPzogc3RyaW5nO1xuICBsaW5rPzogYW55O1xuICB0YWdzPzogc3RyaW5nW107XG4gIG5vZGVJZD86IElEO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gY3JlYXRlSXRlbShpdGVtOiBNZW51SXRlbSkge1xuICByZXR1cm4ge1xuICAgIC4uLml0ZW0sXG4gICAgaWQ6IGd1aWQoKVxuICB9IGFzIE1lbnVJdGVtO1xufVxuIl19