/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { ChangeDetectionStrategy, Component, ContentChildren, Input, QueryList } from '@angular/core';
import { MatExpansionPanel } from '@angular/material';
import { FavNavsQuery } from '../favnav/state/favnavs.query';
import { Observable } from 'rxjs';
import { LayoutService } from '../../layout.service';
import { MenuItemComponent } from './menu-item/menu-item.component';
import { guid } from '@datorama/akita';
import { map } from 'rxjs/operators';
var MenuComponent = /** @class */ (function () {
    function MenuComponent(favNavsQuery, layoutService) {
        this.favNavsQuery = favNavsQuery;
        this.layoutService = layoutService;
        this.favorites$ = this.favNavsQuery.favorites$;
    }
    /**
     * @return {?}
     */
    MenuComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.favoritesMobile$ = this.favorites$.pipe(map((/**
         * @param {?} itens
         * @return {?}
         */
        function (itens) {
            /** @type {?} */
            var favoritesMobile = [(/** @type {?} */ ({ title: 'Meus Favoritos', icon: 'fas fa-star', children: itens }))];
            return favoritesMobile;
        })));
    };
    /**
     * @return {?}
     */
    MenuComponent.prototype.ngAfterContentInit = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var functionMap = (/**
         * @param {?} r
         * @return {?}
         */
        function (r) {
            /** @type {?} */
            var menuItem = (/** @type {?} */ ({
                id: r.id,
                title: r.title,
                link: r.link,
                icon: r.icon,
                tags: r.tags
            }));
            if (r.children.length > 1) {
                if (!r.id) {
                    r.id = guid();
                }
                menuItem.nodeId = r.id;
                menuItem.children = r.children.filter((/**
                 * @param {?} b
                 * @return {?}
                 */
                function (b) { return b !== r; })).map(functionMap);
            }
            return menuItem;
        });
        /** @type {?} */
        var menuItems = this.children.toArray().map(functionMap);
        if (this.itensRecursivo) {
            this.itensRecursivo.subscribe((/**
             * @param {?} c
             * @return {?}
             */
            function (c) { return menuItems.push.apply(menuItems, tslib_1.__spread(c)); }));
        }
        if (menuItems && menuItems.length > 0) {
            this.layoutService.setMenuItems(menuItems);
        }
    };
    MenuComponent.decorators = [
        { type: Component, args: [{
                    selector: 'uikit-menu',
                    template: "<mat-list tabindex=\"-1\" role=\"list\" (click)=\"$event.stopPropagation()\">\n  <uikit-menu-search></uikit-menu-search>\n  <ng-content select=\"[menu]\"></ng-content>\n</mat-list>\n",
                    changeDetection: ChangeDetectionStrategy.OnPush,
                    viewProviders: [MatExpansionPanel],
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    MenuComponent.ctorParameters = function () { return [
        { type: FavNavsQuery },
        { type: LayoutService }
    ]; };
    MenuComponent.propDecorators = {
        itensRecursivo: [{ type: Input }],
        children: [{ type: ContentChildren, args: [MenuItemComponent, { descendants: false },] }]
    };
    return MenuComponent;
}());
export { MenuComponent };
if (false) {
    /** @type {?} */
    MenuComponent.prototype.itensRecursivo;
    /**
     * @type {?}
     * @private
     */
    MenuComponent.prototype.children;
    /** @type {?} */
    MenuComponent.prototype.favorites$;
    /** @type {?} */
    MenuComponent.prototype.favoritesMobile$;
    /**
     * @type {?}
     * @protected
     */
    MenuComponent.prototype.favNavsQuery;
    /**
     * @type {?}
     * @protected
     */
    MenuComponent.prototype.layoutService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVudS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL2xheW91dC9uYXYvbWVudS9tZW51LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFFTCx1QkFBdUIsRUFDdkIsU0FBUyxFQUNULGVBQWUsRUFDZixLQUFLLEVBRUwsU0FBUyxFQUNWLE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFBQyxpQkFBaUIsRUFBQyxNQUFNLG1CQUFtQixDQUFDO0FBQ3BELE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSwrQkFBK0IsQ0FBQztBQUMzRCxPQUFPLEVBQUMsVUFBVSxFQUFDLE1BQU0sTUFBTSxDQUFDO0FBRWhDLE9BQU8sRUFBQyxhQUFhLEVBQUMsTUFBTSxzQkFBc0IsQ0FBQztBQUNuRCxPQUFPLEVBQUMsaUJBQWlCLEVBQUMsTUFBTSxpQ0FBaUMsQ0FBQztBQUNsRSxPQUFPLEVBQUMsSUFBSSxFQUFDLE1BQU0saUJBQWlCLENBQUM7QUFDckMsT0FBTyxFQUFDLEdBQUcsRUFBQyxNQUFNLGdCQUFnQixDQUFDO0FBRW5DO0lBZ0JFLHVCQUNZLFlBQTBCLEVBQzFCLGFBQTRCO1FBRDVCLGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBQzFCLGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBTGpDLGVBQVUsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQztJQU1qRCxDQUFDOzs7O0lBRUQsZ0NBQVE7OztJQUFSO1FBQ0UsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEdBQUc7Ozs7UUFBQyxVQUFDLEtBQXNCOztnQkFDaEUsZUFBZSxHQUFvQixDQUFDLG1CQUFBLEVBQUMsS0FBSyxFQUFFLGdCQUFnQixFQUFFLElBQUksRUFBRSxhQUFhLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBQyxFQUFPLENBQUM7WUFDakgsT0FBTyxlQUFlLENBQUM7UUFDekIsQ0FBQyxFQUFDLENBQUMsQ0FBQztJQUNOLENBQUM7Ozs7SUFFRCwwQ0FBa0I7OztJQUFsQjs7WUFDUSxXQUFXOzs7O1FBQUcsVUFBQyxDQUFvQjs7Z0JBQ2pDLFFBQVEsR0FBRyxtQkFBQTtnQkFDZixFQUFFLEVBQUUsQ0FBQyxDQUFDLEVBQUU7Z0JBQ1IsS0FBSyxFQUFFLENBQUMsQ0FBQyxLQUFLO2dCQUNkLElBQUksRUFBRSxDQUFDLENBQUMsSUFBSTtnQkFDWixJQUFJLEVBQUUsQ0FBQyxDQUFDLElBQUk7Z0JBQ1osSUFBSSxFQUFFLENBQUMsQ0FBQyxJQUFJO2FBQ2IsRUFBWTtZQUViLElBQUksQ0FBQyxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dCQUN6QixJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRTtvQkFDVCxDQUFDLENBQUMsRUFBRSxHQUFHLElBQUksRUFBRSxDQUFDO2lCQUNmO2dCQUNELFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLEVBQUUsQ0FBQztnQkFDdkIsUUFBUSxDQUFDLFFBQVEsR0FBRyxDQUFDLENBQUMsUUFBUSxDQUFDLE1BQU07Ozs7Z0JBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLEtBQUssQ0FBQyxFQUFQLENBQU8sRUFBQyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQzthQUN0RTtZQUVELE9BQU8sUUFBUSxDQUFDO1FBQ2xCLENBQUMsQ0FBQTs7WUFFSyxTQUFTLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDO1FBRTFELElBQUksSUFBSSxDQUFDLGNBQWMsRUFBRTtZQUN2QixJQUFJLENBQUMsY0FBYyxDQUFDLFNBQVM7Ozs7WUFBQyxVQUFDLENBQUMsSUFBSyxPQUFBLFNBQVMsQ0FBQyxJQUFJLE9BQWQsU0FBUyxtQkFBUyxDQUFDLElBQW5CLENBQW9CLEVBQUMsQ0FBQztTQUM1RDtRQUVELElBQUksU0FBUyxJQUFJLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3JDLElBQUksQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1NBQzVDO0lBQ0gsQ0FBQzs7Z0JBMURGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsWUFBWTtvQkFDdEIsa01BQW9DO29CQUVwQyxlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtvQkFDL0MsYUFBYSxFQUFFLENBQUMsaUJBQWlCLENBQUM7O2lCQUNuQzs7OztnQkFkTyxZQUFZO2dCQUdaLGFBQWE7OztpQ0FjbEIsS0FBSzsyQkFFTCxlQUFlLFNBQUMsaUJBQWlCLEVBQUUsRUFBQyxXQUFXLEVBQUUsS0FBSyxFQUFDOztJQWdEMUQsb0JBQUM7Q0FBQSxBQTNERCxJQTJEQztTQXBEWSxhQUFhOzs7SUFFeEIsdUNBQWdEOzs7OztJQUVoRCxpQ0FDK0M7O0lBQy9DLG1DQUFpRDs7SUFDakQseUNBQXdCOzs7OztJQUd0QixxQ0FBb0M7Ozs7O0lBQ3BDLHNDQUFzQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XG4gIEFmdGVyQ29udGVudEluaXQsXG4gIENoYW5nZURldGVjdGlvblN0cmF0ZWd5LFxuICBDb21wb25lbnQsXG4gIENvbnRlbnRDaGlsZHJlbixcbiAgSW5wdXQsXG4gIE9uSW5pdCxcbiAgUXVlcnlMaXN0XG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtNYXRFeHBhbnNpb25QYW5lbH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xuaW1wb3J0IHtGYXZOYXZzUXVlcnl9IGZyb20gJy4uL2Zhdm5hdi9zdGF0ZS9mYXZuYXZzLnF1ZXJ5JztcbmltcG9ydCB7T2JzZXJ2YWJsZX0gZnJvbSAncnhqcyc7XG5pbXBvcnQge01lbnVJdGVtfSBmcm9tICcuL21lbnUtaXRlbS9tZW51LWl0ZW0ubW9kZWwnO1xuaW1wb3J0IHtMYXlvdXRTZXJ2aWNlfSBmcm9tICcuLi8uLi9sYXlvdXQuc2VydmljZSc7XG5pbXBvcnQge01lbnVJdGVtQ29tcG9uZW50fSBmcm9tICcuL21lbnUtaXRlbS9tZW51LWl0ZW0uY29tcG9uZW50JztcbmltcG9ydCB7Z3VpZH0gZnJvbSAnQGRhdG9yYW1hL2FraXRhJztcbmltcG9ydCB7bWFwfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ3Vpa2l0LW1lbnUnLFxuICB0ZW1wbGF0ZVVybDogJy4vbWVudS5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL21lbnUuY29tcG9uZW50LnNjc3MnXSxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG4gIHZpZXdQcm92aWRlcnM6IFtNYXRFeHBhbnNpb25QYW5lbF1cbn0pXG5leHBvcnQgY2xhc3MgTWVudUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgQWZ0ZXJDb250ZW50SW5pdCB7XG5cbiAgQElucHV0KCkgaXRlbnNSZWN1cnNpdm86IE9ic2VydmFibGU8TWVudUl0ZW1bXT47XG5cbiAgQENvbnRlbnRDaGlsZHJlbihNZW51SXRlbUNvbXBvbmVudCwge2Rlc2NlbmRhbnRzOiBmYWxzZX0pXG4gIHByaXZhdGUgY2hpbGRyZW46IFF1ZXJ5TGlzdDxNZW51SXRlbUNvbXBvbmVudD47XG4gIHB1YmxpYyBmYXZvcml0ZXMkID0gdGhpcy5mYXZOYXZzUXVlcnkuZmF2b3JpdGVzJDtcbiAgcHVibGljIGZhdm9yaXRlc01vYmlsZSQ7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJvdGVjdGVkIGZhdk5hdnNRdWVyeTogRmF2TmF2c1F1ZXJ5LFxuICAgIHByb3RlY3RlZCBsYXlvdXRTZXJ2aWNlOiBMYXlvdXRTZXJ2aWNlLCApIHtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIHRoaXMuZmF2b3JpdGVzTW9iaWxlJCA9IHRoaXMuZmF2b3JpdGVzJC5waXBlKG1hcCgoaXRlbnM6IEFycmF5PE1lbnVJdGVtPikgPT4ge1xuICAgICAgY29uc3QgZmF2b3JpdGVzTW9iaWxlOiBBcnJheTxNZW51SXRlbT4gPSBbe3RpdGxlOiAnTWV1cyBGYXZvcml0b3MnLCBpY29uOiAnZmFzIGZhLXN0YXInLCBjaGlsZHJlbjogaXRlbnN9IGFzIGFueV07XG4gICAgICByZXR1cm4gZmF2b3JpdGVzTW9iaWxlO1xuICAgIH0pKTtcbiAgfVxuXG4gIG5nQWZ0ZXJDb250ZW50SW5pdCgpOiB2b2lkIHtcbiAgICBjb25zdCBmdW5jdGlvbk1hcCA9IChyOiBNZW51SXRlbUNvbXBvbmVudCkgPT4ge1xuICAgICAgY29uc3QgbWVudUl0ZW0gPSB7XG4gICAgICAgIGlkOiByLmlkLFxuICAgICAgICB0aXRsZTogci50aXRsZSxcbiAgICAgICAgbGluazogci5saW5rLFxuICAgICAgICBpY29uOiByLmljb24sXG4gICAgICAgIHRhZ3M6IHIudGFnc1xuICAgICAgfSBhcyBNZW51SXRlbTtcblxuICAgICAgaWYgKHIuY2hpbGRyZW4ubGVuZ3RoID4gMSkge1xuICAgICAgICBpZiAoIXIuaWQpIHtcbiAgICAgICAgICByLmlkID0gZ3VpZCgpO1xuICAgICAgICB9XG4gICAgICAgIG1lbnVJdGVtLm5vZGVJZCA9IHIuaWQ7XG4gICAgICAgIG1lbnVJdGVtLmNoaWxkcmVuID0gci5jaGlsZHJlbi5maWx0ZXIoYiA9PiBiICE9PSByKS5tYXAoZnVuY3Rpb25NYXApO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gbWVudUl0ZW07XG4gICAgfTtcblxuICAgIGNvbnN0IG1lbnVJdGVtcyA9IHRoaXMuY2hpbGRyZW4udG9BcnJheSgpLm1hcChmdW5jdGlvbk1hcCk7XG5cbiAgICBpZiAodGhpcy5pdGVuc1JlY3Vyc2l2bykge1xuICAgICAgdGhpcy5pdGVuc1JlY3Vyc2l2by5zdWJzY3JpYmUoKGMpID0+IG1lbnVJdGVtcy5wdXNoKC4uLmMpKTtcbiAgICB9XG5cbiAgICBpZiAobWVudUl0ZW1zICYmIG1lbnVJdGVtcy5sZW5ndGggPiAwKSB7XG4gICAgICB0aGlzLmxheW91dFNlcnZpY2Uuc2V0TWVudUl0ZW1zKG1lbnVJdGVtcyk7XG4gICAgfVxuICB9XG59XG4iXX0=