/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var Breadcrumb = /** @class */ (function () {
    function Breadcrumb() {
    }
    return Breadcrumb;
}());
export { Breadcrumb };
if (false) {
    /** @type {?} */
    Breadcrumb.prototype.title;
    /** @type {?} */
    Breadcrumb.prototype.terminal;
    /** @type {?} */
    Breadcrumb.prototype.link;
    /** @type {?} */
    Breadcrumb.prototype.route;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnJlYWRjcnVtYi5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvbGF5b3V0L25hdi9icmVhZGNydW1iL2JyZWFkY3J1bWJzL2JyZWFkY3J1bWIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUVBO0lBQUE7SUFLQSxDQUFDO0lBQUQsaUJBQUM7QUFBRCxDQUFDLEFBTEQsSUFLQzs7OztJQUpHLDJCQUFjOztJQUNkLDhCQUFrQjs7SUFDbEIsMEJBQWE7O0lBQ2IsMkJBQW9CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUm91dGUgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XG5cbmV4cG9ydCBjbGFzcyBCcmVhZGNydW1iIHtcbiAgICB0aXRsZTogc3RyaW5nO1xuICAgIHRlcm1pbmFsOiBib29sZWFuO1xuICAgIGxpbms6IHN0cmluZztcbiAgICByb3V0ZTogUm91dGUgfCBudWxsO1xufVxuIl19