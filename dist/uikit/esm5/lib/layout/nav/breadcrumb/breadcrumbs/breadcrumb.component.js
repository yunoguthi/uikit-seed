/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ApplicationRef } from '@angular/core';
import { BreadcrumbService } from './breadcrumb.service';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
var BreadcrumbComponent = /** @class */ (function () {
    // breadcrumbs: Breadcrumb[];
    function BreadcrumbComponent(breadcrumbService, router, applicationRef) {
        var _this = this;
        this.breadcrumbService = breadcrumbService;
        this.router = router;
        this.applicationRef = applicationRef;
        this.breadcrumbsSubject = new BehaviorSubject([]);
        this.breadcrumbs$ = this.breadcrumbsSubject.asObservable();
        this.breadcrumbService.breadcrumbChanged.subscribe((/**
         * @param {?} crumbs
         * @return {?}
         */
        function (crumbs) { _this.onBreadcrumbChange(crumbs); }));
    }
    /**
     * @return {?}
     */
    BreadcrumbComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.breadcrumbsSubject.next(this.breadcrumbService.breadcrumbs);
    };
    /**
     * @private
     * @param {?} crumbs
     * @return {?}
     */
    BreadcrumbComponent.prototype.onBreadcrumbChange = /**
     * @private
     * @param {?} crumbs
     * @return {?}
     */
    function (crumbs) {
        this.breadcrumbsSubject.next(crumbs);
        // this.applicationRef.tick();
    };
    BreadcrumbComponent.decorators = [
        { type: Component, args: [{
                    // tslint:disable-next-line:component-selector
                    selector: 'breadcrumb',
                    template: "<div #template>\n    <ng-content></ng-content>\n</div>\n<div class=\"container\" *ngIf=\"template.children.length == 0\">\n  <span *ngFor=\"let route of (breadcrumbs$ | async)\">\n  <a mat-button *ngIf=\"!route.terminal\" href=\"\" [routerLink]=\"[route.link]\">{{ route.title }}</a>\n  <a mat-button *ngIf=\"route.terminal\">{{ route.title }}</a>\n  </span>\n</div>"
                }] }
    ];
    /** @nocollapse */
    BreadcrumbComponent.ctorParameters = function () { return [
        { type: BreadcrumbService },
        { type: Router },
        { type: ApplicationRef }
    ]; };
    return BreadcrumbComponent;
}());
export { BreadcrumbComponent };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.breadcrumbsSubject;
    /** @type {?} */
    BreadcrumbComponent.prototype.breadcrumbs$;
    /**
     * @type {?}
     * @private
     */
    BreadcrumbComponent.prototype.breadcrumbService;
    /**
     * @type {?}
     * @private
     */
    BreadcrumbComponent.prototype.router;
    /**
     * @type {?}
     * @private
     */
    BreadcrumbComponent.prototype.applicationRef;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnJlYWRjcnVtYi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL2xheW91dC9uYXYvYnJlYWRjcnVtYi9icmVhZGNydW1icy9icmVhZGNydW1iLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxjQUFjLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFHbEUsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDekQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUN2QyxPQUFPLEVBQUUsTUFBTSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFFekM7SUFtQkksNkJBQTZCO0lBRTdCLDZCQUFvQixpQkFBb0MsRUFBVSxNQUFjLEVBQVUsY0FBOEI7UUFBeEgsaUJBRUM7UUFGbUIsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQUFVLFdBQU0sR0FBTixNQUFNLENBQVE7UUFBVSxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFMaEgsdUJBQWtCLEdBQUcsSUFBSSxlQUFlLENBQWUsRUFBRSxDQUFDLENBQUM7UUFDOUQsaUJBQVksR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsWUFBWSxFQUFFLENBQUM7UUFLekQsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGlCQUFpQixDQUFDLFNBQVM7Ozs7UUFBQyxVQUFDLE1BQW9CLElBQU8sS0FBSSxDQUFDLGtCQUFrQixDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFDLENBQUM7SUFDckgsQ0FBQzs7OztJQUVELHNDQUFROzs7SUFBUjtRQUNFLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFdBQVcsQ0FBQyxDQUFDO0lBQ25FLENBQUM7Ozs7OztJQUVPLGdEQUFrQjs7Ozs7SUFBMUIsVUFBMkIsTUFBb0I7UUFFM0MsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNyQyw4QkFBOEI7SUFDbEMsQ0FBQzs7Z0JBakNKLFNBQVMsU0FBQzs7b0JBRVAsUUFBUSxFQUFFLFlBQVk7b0JBQ3RCLFFBQVEsRUFDUixnWEFRRztpQkFDTjs7OztnQkFqQlEsaUJBQWlCO2dCQUVqQixNQUFNO2dCQUxhLGNBQWM7O0lBeUMxQywwQkFBQztDQUFBLEFBbENELElBa0NDO1NBcEJZLG1CQUFtQjs7Ozs7O0lBRTlCLGlEQUFxRTs7SUFDckUsMkNBQTZEOzs7OztJQUkvQyxnREFBNEM7Ozs7O0lBQUUscUNBQXNCOzs7OztJQUFFLDZDQUFzQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBBcHBsaWNhdGlvblJlZiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBCcmVhZGNydW1iIH0gZnJvbSAnLi9icmVhZGNydW1iJztcbmltcG9ydCB7IEJyZWFkY3J1bWJTZXJ2aWNlIH0gZnJvbSAnLi9icmVhZGNydW1iLnNlcnZpY2UnO1xuaW1wb3J0IHsgQmVoYXZpb3JTdWJqZWN0IH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuXG5AQ29tcG9uZW50KHtcbiAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6Y29tcG9uZW50LXNlbGVjdG9yXG4gICAgc2VsZWN0b3I6ICdicmVhZGNydW1iJyxcbiAgICB0ZW1wbGF0ZTpcbiAgICBgPGRpdiAjdGVtcGxhdGU+XG4gICAgPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PlxuPC9kaXY+XG48ZGl2IGNsYXNzPVwiY29udGFpbmVyXCIgKm5nSWY9XCJ0ZW1wbGF0ZS5jaGlsZHJlbi5sZW5ndGggPT0gMFwiPlxuICA8c3BhbiAqbmdGb3I9XCJsZXQgcm91dGUgb2YgKGJyZWFkY3J1bWJzJCB8IGFzeW5jKVwiPlxuICA8YSBtYXQtYnV0dG9uICpuZ0lmPVwiIXJvdXRlLnRlcm1pbmFsXCIgaHJlZj1cIlwiIFtyb3V0ZXJMaW5rXT1cIltyb3V0ZS5saW5rXVwiPnt7IHJvdXRlLnRpdGxlIH19PC9hPlxuICA8YSBtYXQtYnV0dG9uICpuZ0lmPVwicm91dGUudGVybWluYWxcIj57eyByb3V0ZS50aXRsZSB9fTwvYT5cbiAgPC9zcGFuPlxuPC9kaXY+YFxufSlcbmV4cG9ydCBjbGFzcyBCcmVhZGNydW1iQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBwcm90ZWN0ZWQgYnJlYWRjcnVtYnNTdWJqZWN0ID0gbmV3IEJlaGF2aW9yU3ViamVjdDxCcmVhZGNydW1iW10+KFtdKTtcbiAgcHVibGljIGJyZWFkY3J1bWJzJCA9IHRoaXMuYnJlYWRjcnVtYnNTdWJqZWN0LmFzT2JzZXJ2YWJsZSgpO1xuXG4gICAgLy8gYnJlYWRjcnVtYnM6IEJyZWFkY3J1bWJbXTtcblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgYnJlYWRjcnVtYlNlcnZpY2U6IEJyZWFkY3J1bWJTZXJ2aWNlLCBwcml2YXRlIHJvdXRlcjogUm91dGVyLCBwcml2YXRlIGFwcGxpY2F0aW9uUmVmOiBBcHBsaWNhdGlvblJlZikge1xuICAgICAgdGhpcy5icmVhZGNydW1iU2VydmljZS5icmVhZGNydW1iQ2hhbmdlZC5zdWJzY3JpYmUoKGNydW1iczogQnJlYWRjcnVtYltdKSA9PiB7IHRoaXMub25CcmVhZGNydW1iQ2hhbmdlKGNydW1icyk7IH0pO1xuICAgIH1cblxuICAgIG5nT25Jbml0KCk6IHZvaWQge1xuICAgICAgdGhpcy5icmVhZGNydW1ic1N1YmplY3QubmV4dCh0aGlzLmJyZWFkY3J1bWJTZXJ2aWNlLmJyZWFkY3J1bWJzKTtcbiAgICB9XG5cbiAgICBwcml2YXRlIG9uQnJlYWRjcnVtYkNoYW5nZShjcnVtYnM6IEJyZWFkY3J1bWJbXSkge1xuXG4gICAgICAgIHRoaXMuYnJlYWRjcnVtYnNTdWJqZWN0Lm5leHQoY3J1bWJzKTtcbiAgICAgICAgLy8gdGhpcy5hcHBsaWNhdGlvblJlZi50aWNrKCk7XG4gICAgfVxufVxuIl19