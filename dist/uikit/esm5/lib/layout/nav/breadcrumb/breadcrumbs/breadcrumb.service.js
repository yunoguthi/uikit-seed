/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, EventEmitter } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
var BreadcrumbService = /** @class */ (function () {
    function BreadcrumbService(router) {
        var _this = this;
        this.router = router;
        this.breadcrumbChanged = new EventEmitter(false);
        this.breadcrumbs = new Array();
        this.router.events.subscribe((/**
         * @param {?} routeEvent
         * @return {?}
         */
        function (routeEvent) { _this.onRouteEvent(routeEvent); }));
    }
    /**
     * @param {?} route
     * @param {?} name
     * @return {?}
     */
    BreadcrumbService.prototype.changeBreadcrumb = /**
     * @param {?} route
     * @param {?} name
     * @return {?}
     */
    function (route, name) {
        /** @type {?} */
        var rootUrl = this.createRootUrl(route);
        /** @type {?} */
        var breadcrumb = this.breadcrumbs.find((/**
         * @param {?} bc
         * @return {?}
         */
        function (bc) { return bc.link === rootUrl; }));
        if (!breadcrumb) {
            return;
        }
        breadcrumb.title = name;
        this.breadcrumbChanged.emit(this.breadcrumbs);
    };
    /**
     * @private
     * @param {?} routeEvent
     * @return {?}
     */
    BreadcrumbService.prototype.onRouteEvent = /**
     * @private
     * @param {?} routeEvent
     * @return {?}
     */
    function (routeEvent) {
        if (!(routeEvent instanceof NavigationEnd)) {
            return;
        }
        /** @type {?} */
        var route = this.router.routerState.root.snapshot;
        /** @type {?} */
        var url = '';
        /** @type {?} */
        var breadCrumbIndex = 0;
        /** @type {?} */
        var newCrumbs = [];
        while (route.firstChild != null) {
            route = route.firstChild;
            if (route.routeConfig === null) {
                continue;
            }
            if (!route.routeConfig.path) {
                continue;
            }
            url += "/" + this.createUrl(route);
            if (!route.data['breadcrumb']) {
                continue;
            }
            /** @type {?} */
            var newCrumb = this.createBreadcrumb(route, url)
            // if (breadCrumbIndex < this.breadcrumbs.length) {
            //   var existing = this.breadcrumbs[breadCrumbIndex++];
            //   if (existing && existing.route == route.routeConfig) {
            //     newCrumb.title = existing.title;
            //   }
            // }
            ;
            // if (breadCrumbIndex < this.breadcrumbs.length) {
            //   var existing = this.breadcrumbs[breadCrumbIndex++];
            //   if (existing && existing.route == route.routeConfig) {
            //     newCrumb.title = existing.title;
            //   }
            // }
            newCrumbs.push(newCrumb);
        }
        this.breadcrumbs = newCrumbs;
        this.breadcrumbChanged.emit(this.breadcrumbs);
    };
    /**
     * @private
     * @param {?} route
     * @param {?} link
     * @return {?}
     */
    BreadcrumbService.prototype.createBreadcrumb = /**
     * @private
     * @param {?} route
     * @param {?} link
     * @return {?}
     */
    function (route, link) {
        // Generates display text from data
        // -- Dynamic route params when ':[id]'
        /** @type {?} */
        var breadcrumb = route.data['breadcrumb'];
        /** @type {?} */
        var d = this.getTitleFormatted(breadcrumb, route);
        return {
            title: d,
            terminal: this.isTerminal(route),
            link: link,
            route: route.routeConfig
        };
    };
    /**
     * @param {?} breadcrumb
     * @param {?} route
     * @return {?}
     */
    BreadcrumbService.prototype.getTitleFormatted = /**
     * @param {?} breadcrumb
     * @param {?} route
     * @return {?}
     */
    function (breadcrumb, route) {
        /** @type {?} */
        var d = '';
        /** @type {?} */
        var split = breadcrumb.split(' ');
        split.forEach((/**
         * @param {?} s
         * @return {?}
         */
        function (s) {
            d += (s.indexOf(':') > -1 ? (route.params[s.slice(1)] ? route.params[s.slice(1)] : '') : s) + " ";
        }));
        d = d.slice(0, -1);
        return d;
    };
    /**
     * @private
     * @param {?} route
     * @return {?}
     */
    BreadcrumbService.prototype.isTerminal = /**
     * @private
     * @param {?} route
     * @return {?}
     */
    function (route) {
        return route.firstChild === null
            || route.firstChild.routeConfig === null
            || !route.firstChild.routeConfig.path;
    };
    /**
     * @private
     * @param {?} route
     * @return {?}
     */
    BreadcrumbService.prototype.createUrl = /**
     * @private
     * @param {?} route
     * @return {?}
     */
    function (route) {
        return route.url.map((/**
         * @param {?} s
         * @return {?}
         */
        function (s) { return s.toString(); })).join('/');
    };
    /**
     * @private
     * @param {?} route
     * @return {?}
     */
    BreadcrumbService.prototype.createRootUrl = /**
     * @private
     * @param {?} route
     * @return {?}
     */
    function (route) {
        /** @type {?} */
        var url = '';
        /** @type {?} */
        var next = route.root;
        while (next.firstChild !== null) {
            next = next.firstChild;
            if (next.routeConfig === null) {
                continue;
            }
            if (!next.routeConfig.path) {
                continue;
            }
            url += "/" + this.createUrl(next);
            if (next === route) {
                break;
            }
        }
        return url;
    };
    BreadcrumbService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    BreadcrumbService.ctorParameters = function () { return [
        { type: Router }
    ]; };
    return BreadcrumbService;
}());
export { BreadcrumbService };
if (false) {
    /** @type {?} */
    BreadcrumbService.prototype.breadcrumbChanged;
    /** @type {?} */
    BreadcrumbService.prototype.breadcrumbs;
    /**
     * @type {?}
     * @private
     */
    BreadcrumbService.prototype.router;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnJlYWRjcnVtYi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9sYXlvdXQvbmF2L2JyZWFkY3J1bWIvYnJlYWRjcnVtYnMvYnJlYWRjcnVtYi5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6RCxPQUFPLEVBQUUsTUFBTSxFQUFpQyxhQUFhLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUl2RjtJQU1JLDJCQUFvQixNQUFjO1FBQWxDLGlCQUVDO1FBRm1CLFdBQU0sR0FBTixNQUFNLENBQVE7UUFKbEMsc0JBQWlCLEdBQUcsSUFBSSxZQUFZLENBQWUsS0FBSyxDQUFDLENBQUM7UUFFbkQsZ0JBQVcsR0FBRyxJQUFJLEtBQUssRUFBYyxDQUFDO1FBR3pDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLFNBQVM7Ozs7UUFBQyxVQUFDLFVBQVUsSUFBTyxLQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFDLENBQUM7SUFDckYsQ0FBQzs7Ozs7O0lBRU0sNENBQWdCOzs7OztJQUF2QixVQUF3QixLQUE2QixFQUFFLElBQVk7O1lBQ3pELE9BQU8sR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQzs7WUFDbkMsVUFBVSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSTs7OztRQUFDLFVBQVUsRUFBRSxJQUFJLE9BQU8sRUFBRSxDQUFDLElBQUksS0FBSyxPQUFPLENBQUMsQ0FBQyxDQUFDLEVBQUM7UUFFdkYsSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUFFLE9BQU87U0FBRTtRQUU1QixVQUFVLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztRQUV4QixJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztJQUNsRCxDQUFDOzs7Ozs7SUFFTyx3Q0FBWTs7Ozs7SUFBcEIsVUFBcUIsVUFBaUI7UUFDbEMsSUFBSSxDQUFDLENBQUMsVUFBVSxZQUFZLGFBQWEsQ0FBQyxFQUFFO1lBQUUsT0FBTztTQUFFOztZQUVuRCxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVE7O1lBQzdDLEdBQUcsR0FBRyxFQUFFOztZQUVSLGVBQWUsR0FBRyxDQUFDOztZQUNuQixTQUFTLEdBQUcsRUFBRTtRQUVsQixPQUFPLEtBQUssQ0FBQyxVQUFVLElBQUksSUFBSSxFQUFFO1lBQzdCLEtBQUssR0FBRyxLQUFLLENBQUMsVUFBVSxDQUFDO1lBRXpCLElBQUksS0FBSyxDQUFDLFdBQVcsS0FBSyxJQUFJLEVBQUU7Z0JBQUUsU0FBUzthQUFFO1lBQzdDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRTtnQkFBRSxTQUFTO2FBQUU7WUFFMUMsR0FBRyxJQUFJLE1BQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUcsQ0FBQztZQUVuQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRTtnQkFBRSxTQUFTO2FBQUU7O2dCQUV4QyxRQUFRLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssRUFBRSxHQUFHLENBQUM7WUFFaEQsbURBQW1EO1lBQ25ELHdEQUF3RDtZQUV4RCwyREFBMkQ7WUFDM0QsdUNBQXVDO1lBQ3ZDLE1BQU07WUFDTixJQUFJOztZQU5KLG1EQUFtRDtZQUNuRCx3REFBd0Q7WUFFeEQsMkRBQTJEO1lBQzNELHVDQUF1QztZQUN2QyxNQUFNO1lBQ04sSUFBSTtZQUVKLFNBQVMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDNUI7UUFFRCxJQUFJLENBQUMsV0FBVyxHQUFHLFNBQVMsQ0FBQztRQUM3QixJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztJQUNsRCxDQUFDOzs7Ozs7O0lBRU8sNENBQWdCOzs7Ozs7SUFBeEIsVUFBeUIsS0FBNkIsRUFBRSxJQUFZOzs7O1lBRzVELFVBQVUsR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQzs7WUFFdkMsQ0FBQyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLEVBQUUsS0FBSyxDQUFDO1FBRWpELE9BQU87WUFDTCxLQUFLLEVBQUUsQ0FBQztZQUNOLFFBQVEsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQztZQUNoQyxJQUFJLEVBQUUsSUFBSTtZQUNWLEtBQUssRUFBRSxLQUFLLENBQUMsV0FBVztTQUMzQixDQUFDO0lBQ04sQ0FBQzs7Ozs7O0lBRU0sNkNBQWlCOzs7OztJQUF4QixVQUF5QixVQUFlLEVBQUUsS0FBNkI7O1lBQ2pFLENBQUMsR0FBRyxFQUFFOztZQUNKLEtBQUssR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQztRQUNuQyxLQUFLLENBQUMsT0FBTzs7OztRQUFDLFVBQUMsQ0FBUztZQUN0QixDQUFDLElBQUksQ0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBRyxDQUFDO1FBQ2xHLENBQUMsRUFBQyxDQUFDO1FBQ0gsQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDbkIsT0FBTyxDQUFDLENBQUM7SUFDWCxDQUFDOzs7Ozs7SUFFUyxzQ0FBVTs7Ozs7SUFBbEIsVUFBbUIsS0FBNkI7UUFDNUMsT0FBTyxLQUFLLENBQUMsVUFBVSxLQUFLLElBQUk7ZUFDekIsS0FBSyxDQUFDLFVBQVUsQ0FBQyxXQUFXLEtBQUssSUFBSTtlQUNyQyxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQztJQUM5QyxDQUFDOzs7Ozs7SUFFTyxxQ0FBUzs7Ozs7SUFBakIsVUFBa0IsS0FBNkI7UUFDM0MsT0FBTyxLQUFLLENBQUMsR0FBRyxDQUFDLEdBQUc7Ozs7UUFBQyxVQUFVLENBQUMsSUFBSSxPQUFPLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUMxRSxDQUFDOzs7Ozs7SUFFTyx5Q0FBYTs7Ozs7SUFBckIsVUFBc0IsS0FBNkI7O1lBQzNDLEdBQUcsR0FBRyxFQUFFOztZQUNSLElBQUksR0FBRyxLQUFLLENBQUMsSUFBSTtRQUVyQixPQUFPLElBQUksQ0FBQyxVQUFVLEtBQUssSUFBSSxFQUFFO1lBQzdCLElBQUksR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO1lBRXZCLElBQUksSUFBSSxDQUFDLFdBQVcsS0FBSyxJQUFJLEVBQUU7Z0JBQUUsU0FBUzthQUFFO1lBQzVDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRTtnQkFBRSxTQUFTO2FBQUU7WUFFekMsR0FBRyxJQUFJLE1BQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUcsQ0FBQztZQUVsQyxJQUFJLElBQUksS0FBSyxLQUFLLEVBQUU7Z0JBQUUsTUFBTTthQUFFO1NBQ2pDO1FBRUQsT0FBTyxHQUFHLENBQUM7SUFDZixDQUFDOztnQkE1R0osVUFBVTs7OztnQkFKRixNQUFNOztJQWlIZix3QkFBQztDQUFBLEFBN0dELElBNkdDO1NBNUdZLGlCQUFpQjs7O0lBQzFCLDhDQUEwRDs7SUFFMUQsd0NBQTZDOzs7OztJQUVqQyxtQ0FBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFJvdXRlciwgQWN0aXZhdGVkUm91dGVTbmFwc2hvdCwgRXZlbnQsIE5hdmlnYXRpb25FbmQgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuXG5pbXBvcnQgeyBCcmVhZGNydW1iIH0gZnJvbSAnLi9icmVhZGNydW1iJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIEJyZWFkY3J1bWJTZXJ2aWNlIHtcbiAgICBicmVhZGNydW1iQ2hhbmdlZCA9IG5ldyBFdmVudEVtaXR0ZXI8QnJlYWRjcnVtYltdPihmYWxzZSk7XG5cbiAgICBwdWJsaWMgYnJlYWRjcnVtYnMgPSBuZXcgQXJyYXk8QnJlYWRjcnVtYj4oKTtcblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgcm91dGVyOiBSb3V0ZXIpIHtcbiAgICAgICAgdGhpcy5yb3V0ZXIuZXZlbnRzLnN1YnNjcmliZSgocm91dGVFdmVudCkgPT4geyB0aGlzLm9uUm91dGVFdmVudChyb3V0ZUV2ZW50KTsgfSk7XG4gICAgfVxuXG4gICAgcHVibGljIGNoYW5nZUJyZWFkY3J1bWIocm91dGU6IEFjdGl2YXRlZFJvdXRlU25hcHNob3QsIG5hbWU6IHN0cmluZykge1xuICAgICAgICBjb25zdCByb290VXJsID0gdGhpcy5jcmVhdGVSb290VXJsKHJvdXRlKTtcbiAgICAgICAgY29uc3QgYnJlYWRjcnVtYiA9IHRoaXMuYnJlYWRjcnVtYnMuZmluZChmdW5jdGlvbiAoYmMpIHsgcmV0dXJuIGJjLmxpbmsgPT09IHJvb3RVcmw7IH0pO1xuXG4gICAgICAgIGlmICghYnJlYWRjcnVtYikgeyByZXR1cm47IH1cblxuICAgICAgICBicmVhZGNydW1iLnRpdGxlID0gbmFtZTtcblxuICAgICAgICB0aGlzLmJyZWFkY3J1bWJDaGFuZ2VkLmVtaXQodGhpcy5icmVhZGNydW1icyk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBvblJvdXRlRXZlbnQocm91dGVFdmVudDogRXZlbnQpIHtcbiAgICAgICAgaWYgKCEocm91dGVFdmVudCBpbnN0YW5jZW9mIE5hdmlnYXRpb25FbmQpKSB7IHJldHVybjsgfVxuXG4gICAgICAgIGxldCByb3V0ZSA9IHRoaXMucm91dGVyLnJvdXRlclN0YXRlLnJvb3Quc25hcHNob3Q7XG4gICAgICAgIGxldCB1cmwgPSAnJztcblxuICAgICAgICB2YXIgYnJlYWRDcnVtYkluZGV4ID0gMDtcbiAgICAgICAgdmFyIG5ld0NydW1icyA9IFtdO1xuXG4gICAgICAgIHdoaWxlIChyb3V0ZS5maXJzdENoaWxkICE9IG51bGwpIHtcbiAgICAgICAgICAgIHJvdXRlID0gcm91dGUuZmlyc3RDaGlsZDtcblxuICAgICAgICAgICAgaWYgKHJvdXRlLnJvdXRlQ29uZmlnID09PSBudWxsKSB7IGNvbnRpbnVlOyB9XG4gICAgICAgICAgICBpZiAoIXJvdXRlLnJvdXRlQ29uZmlnLnBhdGgpIHsgY29udGludWU7IH1cblxuICAgICAgICAgICAgdXJsICs9IGAvJHt0aGlzLmNyZWF0ZVVybChyb3V0ZSl9YDtcblxuICAgICAgICAgICAgaWYgKCFyb3V0ZS5kYXRhWydicmVhZGNydW1iJ10pIHsgY29udGludWU7IH1cblxuICAgICAgICAgICAgdmFyIG5ld0NydW1iID0gdGhpcy5jcmVhdGVCcmVhZGNydW1iKHJvdXRlLCB1cmwpXG5cbiAgICAgICAgICAgIC8vIGlmIChicmVhZENydW1iSW5kZXggPCB0aGlzLmJyZWFkY3J1bWJzLmxlbmd0aCkge1xuICAgICAgICAgICAgLy8gICB2YXIgZXhpc3RpbmcgPSB0aGlzLmJyZWFkY3J1bWJzW2JyZWFkQ3J1bWJJbmRleCsrXTtcblxuICAgICAgICAgICAgLy8gICBpZiAoZXhpc3RpbmcgJiYgZXhpc3Rpbmcucm91dGUgPT0gcm91dGUucm91dGVDb25maWcpIHtcbiAgICAgICAgICAgIC8vICAgICBuZXdDcnVtYi50aXRsZSA9IGV4aXN0aW5nLnRpdGxlO1xuICAgICAgICAgICAgLy8gICB9XG4gICAgICAgICAgICAvLyB9XG5cbiAgICAgICAgICAgIG5ld0NydW1icy5wdXNoKG5ld0NydW1iKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMuYnJlYWRjcnVtYnMgPSBuZXdDcnVtYnM7XG4gICAgICAgIHRoaXMuYnJlYWRjcnVtYkNoYW5nZWQuZW1pdCh0aGlzLmJyZWFkY3J1bWJzKTtcbiAgICB9XG5cbiAgICBwcml2YXRlIGNyZWF0ZUJyZWFkY3J1bWIocm91dGU6IEFjdGl2YXRlZFJvdXRlU25hcHNob3QsIGxpbms6IHN0cmluZyk6IEJyZWFkY3J1bWIge1xuICAgICAgLy8gR2VuZXJhdGVzIGRpc3BsYXkgdGV4dCBmcm9tIGRhdGFcbiAgICAgIC8vIC0tIER5bmFtaWMgcm91dGUgcGFyYW1zIHdoZW4gJzpbaWRdJ1xuICAgICAgY29uc3QgYnJlYWRjcnVtYiA9IHJvdXRlLmRhdGFbJ2JyZWFkY3J1bWInXTtcblxuICAgICAgbGV0IGQgPSB0aGlzLmdldFRpdGxlRm9ybWF0dGVkKGJyZWFkY3J1bWIsIHJvdXRlKTtcblxuICAgICAgcmV0dXJuIHtcbiAgICAgICAgdGl0bGU6IGQsXG4gICAgICAgICAgdGVybWluYWw6IHRoaXMuaXNUZXJtaW5hbChyb3V0ZSksXG4gICAgICAgICAgbGluazogbGluayxcbiAgICAgICAgICByb3V0ZTogcm91dGUucm91dGVDb25maWdcbiAgICAgIH07XG4gIH1cblxuICBwdWJsaWMgZ2V0VGl0bGVGb3JtYXR0ZWQoYnJlYWRjcnVtYjogYW55LCByb3V0ZTogQWN0aXZhdGVkUm91dGVTbmFwc2hvdCkge1xuICAgIGxldCBkID0gJyc7XG4gICAgY29uc3Qgc3BsaXQgPSBicmVhZGNydW1iLnNwbGl0KCcgJyk7XG4gICAgc3BsaXQuZm9yRWFjaCgoczogc3RyaW5nKSA9PiB7XG4gICAgICBkICs9IGAke3MuaW5kZXhPZignOicpID4gLTEgPyAocm91dGUucGFyYW1zW3Muc2xpY2UoMSldID8gcm91dGUucGFyYW1zW3Muc2xpY2UoMSldIDogJycpIDogc30gYDtcbiAgICB9KTtcbiAgICBkID0gZC5zbGljZSgwLCAtMSk7XG4gICAgcmV0dXJuIGQ7XG4gIH1cblxuICAgIHByaXZhdGUgaXNUZXJtaW5hbChyb3V0ZTogQWN0aXZhdGVkUm91dGVTbmFwc2hvdCkge1xuICAgICAgICByZXR1cm4gcm91dGUuZmlyc3RDaGlsZCA9PT0gbnVsbFxuICAgICAgICAgICAgfHwgcm91dGUuZmlyc3RDaGlsZC5yb3V0ZUNvbmZpZyA9PT0gbnVsbFxuICAgICAgICAgICAgfHwgIXJvdXRlLmZpcnN0Q2hpbGQucm91dGVDb25maWcucGF0aDtcbiAgICB9XG5cbiAgICBwcml2YXRlIGNyZWF0ZVVybChyb3V0ZTogQWN0aXZhdGVkUm91dGVTbmFwc2hvdCkge1xuICAgICAgICByZXR1cm4gcm91dGUudXJsLm1hcChmdW5jdGlvbiAocykgeyByZXR1cm4gcy50b1N0cmluZygpOyB9KS5qb2luKCcvJyk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBjcmVhdGVSb290VXJsKHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90KSB7XG4gICAgICAgIGxldCB1cmwgPSAnJztcbiAgICAgICAgbGV0IG5leHQgPSByb3V0ZS5yb290O1xuXG4gICAgICAgIHdoaWxlIChuZXh0LmZpcnN0Q2hpbGQgIT09IG51bGwpIHtcbiAgICAgICAgICAgIG5leHQgPSBuZXh0LmZpcnN0Q2hpbGQ7XG5cbiAgICAgICAgICAgIGlmIChuZXh0LnJvdXRlQ29uZmlnID09PSBudWxsKSB7IGNvbnRpbnVlOyB9XG4gICAgICAgICAgICBpZiAoIW5leHQucm91dGVDb25maWcucGF0aCkgeyBjb250aW51ZTsgfVxuXG4gICAgICAgICAgICB1cmwgKz0gYC8ke3RoaXMuY3JlYXRlVXJsKG5leHQpfWA7XG5cbiAgICAgICAgICAgIGlmIChuZXh0ID09PSByb3V0ZSkgeyBicmVhazsgfVxuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHVybDtcbiAgICB9XG59XG4iXX0=