/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ChangeDetectionStrategy, ApplicationRef, ViewChild, TemplateRef, ContentChild, ElementRef, Renderer2, NgZone, ChangeDetectorRef } from '@angular/core';
import { LayoutService } from '../../layout.service';
import { distinctUntilChanged, filter, map, flatMap, debounceTime } from 'rxjs/operators';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { Subscription, BehaviorSubject } from 'rxjs';
import { FavNavsService } from '../favnav/state/favnavs.service';
import { FavNavsQuery } from '../favnav/state/favnavs.query';
import { BreadcrumbService } from './breadcrumbs/breadcrumb.service';
import { PlatformLocation, Location } from '@angular/common';
import { ScrollDispatcher } from '@angular/cdk/scrolling';
import { PreviousRouteService } from '../../../components/previous-route.service';
var BreadcrumbComponent = /** @class */ (function () {
    function BreadcrumbComponent(layoutService, applicationRef, router, activatedRoute, favNavsService, favNavsQuery, renderer, zone, changeDetector, breadcrumbService, angularLocation, platformLocation, scrollDispatcher, previousRouteService) {
        var _this = this;
        this.layoutService = layoutService;
        this.applicationRef = applicationRef;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.favNavsService = favNavsService;
        this.favNavsQuery = favNavsQuery;
        this.renderer = renderer;
        this.zone = zone;
        this.changeDetector = changeDetector;
        this.breadcrumbService = breadcrumbService;
        this.angularLocation = angularLocation;
        this.platformLocation = platformLocation;
        this.scrollDispatcher = scrollDispatcher;
        this.previousRouteService = previousRouteService;
        this.subscription = new Subscription();
        this.menuItem$ = new BehaviorSubject(null);
        this.menuItemSubscription$ = Subscription.EMPTY;
        this.isFavorited = false;
        this.isMobile$ = this.layoutService.isMobile$;
        this.navigationEndSubscription = Subscription.EMPTY;
        this.data$ = new BehaviorSubject(null);
        this.hasActionsSubject = new BehaviorSubject(false);
        this.hasActions$ = this.hasActionsSubject.asObservable();
        this.possuiRecursoDeFavoritos = this.favNavsService.possuiRecursoDeFavoritos;
        this.hiddenShell$ = this.layoutService.hiddenShell$;
        this.isFixed$ = this.layoutService.isFixed$.pipe(distinctUntilChanged());
        this.subscription.add(this.scrollDispatcher.scrolled().pipe(debounceTime(75)).subscribe((/**
         * @param {?} x
         * @return {?}
         */
        function (x) {
            setTimeout((/**
             * @return {?}
             */
            function () { return _this.applicationRef.tick(); }));
        })));
    }
    /**
     * @return {?}
     */
    BreadcrumbComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.desabilitarBotaoVoltar$ = this.previousRouteService.desabilitarBotaoVoltar;
        // this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe(nav => {
        //   const params = this.activatedRoute.snapshot.params;
        //   console.log('params', params);
        // });
        // this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe(nav => {
        // // this.activatedRoute.params.subscribe(params => {
        //   const params = this.activatedRoute.snapshot.params;
        //   const data = this.activatedRoute.root.firstChild.snapshot.data;
        //   if (data && data.breadcrumb) {
        //     let title: string = data.breadcrumb;
        //     const parametros = Object.keys(params);
        //     parametros.forEach(parametro => {
        //       title = title.replace(`:${parametro}`, params[parametro]);
        //     });
        //     this.breadcrumbService.changeBreadcrumb(this.activatedRoute.snapshot, data.breadcrumb);
        //   }
        // });
        //
        // this.breadcrumbService.breadcrumbChanged.subscribe(crumbs => {
        //   this.breadcrumbs = crumbs.map(crumb => ({ label: crumb.displayName, url: `#${crumb.url}` }));
        // });
        //
        this.activatedRoute.paramMap.pipe(map((/**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var child = _this.activatedRoute.firstChild;
            while (child) {
                if (child.firstChild) {
                    child = child.firstChild;
                }
                else if (child.snapshot.data) {
                    return child.snapshot.data;
                }
                else {
                    return null;
                }
            }
            return null;
        })))
            .subscribe((/**
         * @param {?} customData
         * @return {?}
         */
        function (customData) {
            /** @type {?} */
            var route = _this.router.routerState.root.snapshot;
            while (route.firstChild != null) {
                route = route.firstChild;
            }
            /** @type {?} */
            var data = route.data;
            /** @type {?} */
            var favItem = _this.getFavItem(data);
            // favItem.icon = customData ? customData.icon : null;
            _this.data$.next(favItem);
            if (favItem) {
                _this.isFavorited$ = _this.favNavsQuery.isFavorited((/** @type {?} */ (favItem)));
                _this.isFavorited$.subscribe((/**
                 * @param {?} r
                 * @return {?}
                 */
                function (r) { return _this.isFavorited = r; }));
            }
        }));
        this.navigationEndSubscription = this.router.events.pipe(filter((/**
         * @param {?} event
         * @return {?}
         */
        function (event) { return event instanceof NavigationEnd; })), map((/**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var child = _this.activatedRoute.firstChild;
            while (child) {
                if (child.firstChild) {
                    child = child.firstChild;
                }
                else if (child.snapshot.data) {
                    return child.snapshot.data;
                }
                else {
                    return null;
                }
            }
            return null;
        })))
            .subscribe((/**
         * @param {?} customData
         * @return {?}
         */
        function (customData) {
            /** @type {?} */
            var route = _this.router.routerState.root.snapshot;
            while (route.firstChild != null) {
                route = route.firstChild;
            }
            /** @type {?} */
            var data = route.data;
            /** @type {?} */
            var favItem = _this.getFavItem(data);
            // favItem.icon = customData ? customData.icon : null;
            _this.data$.next(favItem);
            if (favItem) {
                _this.isFavorited$ = _this.favNavsQuery.isFavorited((/** @type {?} */ (favItem)));
                _this.isFavorited$.subscribe((/**
                 * @param {?} r
                 * @return {?}
                 */
                function (r) { return _this.isFavorited = r; }));
            }
        }));
        this.data$.subscribe((/**
         * @param {?} data
         * @return {?}
         */
        function (data) {
            _this.menuItemSubscription$ = _this.layoutService.menuItemsFlattened$.pipe(flatMap((/**
             * @param {?} r
             * @return {?}
             */
            function (r) { return r; })), filter((/**
             * @param {?} r
             * @return {?}
             */
            function (r) {
                if (data == null || data.id == null) {
                    return false;
                }
                else {
                    /** @type {?} */
                    var retorno = r.id === data.id;
                    return retorno;
                }
            }))).subscribe((/**
             * @param {?} item
             * @return {?}
             */
            function (item) {
                _this.menuItem$.next(item);
            }));
        }));
    };
    /**
     * @private
     * @param {?} customData
     * @return {?}
     */
    BreadcrumbComponent.prototype.getFavItem = /**
     * @private
     * @param {?} customData
     * @return {?}
     */
    function (customData) {
        /** @type {?} */
        var data = customData;
        /** @type {?} */
        var lastBreadCrumb = this.breadcrumbService.breadcrumbs[this.breadcrumbService.breadcrumbs.length - 1];
        if (lastBreadCrumb != null) {
            /** @type {?} */
            var title = lastBreadCrumb.title;
            // this.breadcrumbService.getTitleFormatted(data.breadcrumb, this.activatedRoute.snapshot);
            /** @type {?} */
            var favItem = { link: this.router.url, title: title, icon: data ? data.icon : null };
            return favItem;
        }
    };
    /**
     * @return {?}
     */
    BreadcrumbComponent.prototype.updateIfHasActions = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var elements = document.getElementsByClassName('uikit-actions');
        /** @type {?} */
        var element = elements.item(0);
        if (element != null) {
            /** @type {?} */
            var hasChildNodes = element.hasChildNodes();
            this.hasActionsSubject.next(hasChildNodes);
        }
        else {
            this.hasActionsSubject.next(false);
        }
    };
    /**
     * @return {?}
     */
    BreadcrumbComponent.prototype.toggleFavoritos = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var customData = this.data$.getValue();
        /** @type {?} */
        var favItem = this.getFavItem(customData);
        this.favNavsService.toggleItem((/** @type {?} */ (favItem)));
    };
    /**
     * @return {?}
     */
    BreadcrumbComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        if (this.navigationEndSubscription && this.navigationEndSubscription.closed == false) {
            this.navigationEndSubscription.unsubscribe();
        }
        if (this.menuItemSubscription$ && this.menuItemSubscription$.closed == false) {
            this.menuItemSubscription$.unsubscribe();
        }
        if (this.subscription && this.subscription.closed == false) {
            this.subscription.unsubscribe();
        }
    };
    /**
     * @return {?}
     */
    BreadcrumbComponent.prototype.goBack = /**
     * @return {?}
     */
    function () {
        //    if (document.referrer.indexOf(location.host) !== -1) {
        //history.go(-1);
        this.previousRouteService.removerRotaAcessada();
        this.angularLocation.back();
        //  } else {
        //      this.router.navigate([`../`], { relativeTo: this.activatedRoute });
        //}
    };
    BreadcrumbComponent.decorators = [
        { type: Component, args: [{
                    selector: 'uikit-breadcrumb',
                    template: "<div class=\"breadcrumb\"\n     [ngClass]=\"{'noshell': !(hiddenShell$ | async)}\"\n     *ngIf=\"(data$ | async) != null && (data$ | async).title != null\">\n\n  <button [disabled]=\"desabilitarBotaoVoltar$ | async\" type=\"button\" mat-icon-button color=\"\"  (click)=\"goBack()\">\n    <mat-icon class=\"fas fa-chevron-left\"></mat-icon>\n  </button>\n\n  <button tabindex=\"0\" *ngIf=\"possuiRecursoDeFavoritos; else naoPossuiRecursoDeFavoritos\" mat-icon-button\n    [class.btn-favorite]=\"possuiRecursoDeFavoritos\" [class.active]=\"isFavorited$ | async\"\n    (click)=\"toggleFavoritos()\">\n    <mat-icon class=\"fa-lg {{ (data$ | async)?.icon }}\"></mat-icon>\n  </button>\n\n  <ng-template #naoPossuiRecursoDeFavoritos>\n    <button tabindex=\"-1\"  mat-icon-button class=\"btn-favorite\" [class.btn-favorite]=\"possuiRecursoDeFavoritos\"\n      [class.active]=\"isFavorited$ | async\">\n      <mat-icon class=\"fa-lg {{ (data$ | async)?.icon }}\"></mat-icon>\n    </button>\n  </ng-template>\n\n  <breadcrumb #parent>\n    <ng-container\n     *ngFor=\"let route of (parent.breadcrumbs$ | async)\">\n      <a mat-button *ngIf=\"!route.terminal\" tabindex=\"1\" href=\"\" [routerLink]=\"[route.link]\">{{ route.title }}</a>\n      <a mat-button *ngIf=\"route.terminal\">{{ route.title }}</a>\n    </ng-container>\n  </breadcrumb>\n\n</div>\n",
                    changeDetection: ChangeDetectionStrategy.Default,
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    BreadcrumbComponent.ctorParameters = function () { return [
        { type: LayoutService },
        { type: ApplicationRef },
        { type: Router },
        { type: ActivatedRoute },
        { type: FavNavsService },
        { type: FavNavsQuery },
        { type: Renderer2 },
        { type: NgZone },
        { type: ChangeDetectorRef },
        { type: BreadcrumbService },
        { type: Location },
        { type: PlatformLocation },
        { type: ScrollDispatcher },
        { type: PreviousRouteService }
    ]; };
    BreadcrumbComponent.propDecorators = {
        actions: [{ type: ContentChild, args: ['uikitActions',] }],
        templateActions: [{ type: ViewChild, args: ['templateActions',] }],
        templateFinder: [{ type: ViewChild, args: ['templateFinder',] }],
        templateFilters: [{ type: ViewChild, args: ['templateFilters',] }],
        templatePaginator: [{ type: ViewChild, args: ['templatePaginator',] }]
    };
    return BreadcrumbComponent;
}());
export { BreadcrumbComponent };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.subscription;
    /** @type {?} */
    BreadcrumbComponent.prototype.menuItem$;
    /** @type {?} */
    BreadcrumbComponent.prototype.menuItemSubscription$;
    /** @type {?} */
    BreadcrumbComponent.prototype.isFavorited$;
    /** @type {?} */
    BreadcrumbComponent.prototype.isFavorited;
    /** @type {?} */
    BreadcrumbComponent.prototype.isMobile$;
    /** @type {?} */
    BreadcrumbComponent.prototype.navigationEndSubscription;
    /** @type {?} */
    BreadcrumbComponent.prototype.data$;
    /** @type {?} */
    BreadcrumbComponent.prototype.actions;
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.hasActionsSubject;
    /** @type {?} */
    BreadcrumbComponent.prototype.hasActions$;
    /** @type {?} */
    BreadcrumbComponent.prototype.templateActions;
    /** @type {?} */
    BreadcrumbComponent.prototype.templateFinder;
    /** @type {?} */
    BreadcrumbComponent.prototype.templateFilters;
    /** @type {?} */
    BreadcrumbComponent.prototype.templatePaginator;
    /** @type {?} */
    BreadcrumbComponent.prototype.possuiRecursoDeFavoritos;
    /** @type {?} */
    BreadcrumbComponent.prototype.hiddenShell$;
    /** @type {?} */
    BreadcrumbComponent.prototype.isFixed$;
    /** @type {?} */
    BreadcrumbComponent.prototype.desabilitarBotaoVoltar$;
    /** @type {?} */
    BreadcrumbComponent.prototype.layoutService;
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.applicationRef;
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.router;
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.activatedRoute;
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.favNavsService;
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.favNavsQuery;
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.renderer;
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.zone;
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.changeDetector;
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.breadcrumbService;
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.angularLocation;
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.platformLocation;
    /**
     * @type {?}
     * @private
     */
    BreadcrumbComponent.prototype.scrollDispatcher;
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.previousRouteService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnJlYWRjcnVtYi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL2xheW91dC9uYXYvYnJlYWRjcnVtYi9icmVhZGNydW1iLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSx1QkFBdUIsRUFBRSxjQUFjLEVBQUUsU0FBUyxFQUFFLFdBQVcsRUFBK0IsWUFBWSxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFLGlCQUFpQixFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ2hOLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUNyRCxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRSxPQUFPLEVBQVksWUFBWSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDcEcsT0FBTyxFQUFFLE1BQU0sRUFBbUIsYUFBYSxFQUFTLGNBQWMsRUFBeUMsTUFBTSxpQkFBaUIsQ0FBQztBQUN2SSxPQUFPLEVBQUUsWUFBWSxFQUFFLGVBQWUsRUFBYyxNQUFNLE1BQU0sQ0FBQztBQUVqRSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0saUNBQWlDLENBQUM7QUFDakUsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBQzdELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ3JFLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxRQUFRLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUM3RCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUMxRCxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSw0Q0FBNEMsQ0FBQztBQUdsRjtJQWlDRSw2QkFDUyxhQUE0QixFQUN6QixjQUE4QixFQUM5QixNQUFjLEVBQ2QsY0FBOEIsRUFDOUIsY0FBOEIsRUFDOUIsWUFBMEIsRUFDMUIsUUFBbUIsRUFDbkIsSUFBWSxFQUNaLGNBQWlDLEVBQ2pDLGlCQUFvQyxFQUNwQyxlQUF5QixFQUN6QixnQkFBa0MsRUFDcEMsZ0JBQWtDLEVBQ2hDLG9CQUEwQztRQWR0RCxpQkFtQkM7UUFsQlEsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFDekIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBQzFCLGFBQVEsR0FBUixRQUFRLENBQVc7UUFDbkIsU0FBSSxHQUFKLElBQUksQ0FBUTtRQUNaLG1CQUFjLEdBQWQsY0FBYyxDQUFtQjtRQUNqQyxzQkFBaUIsR0FBakIsaUJBQWlCLENBQW1CO1FBQ3BDLG9CQUFlLEdBQWYsZUFBZSxDQUFVO1FBQ3pCLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFDcEMscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUNoQyx5QkFBb0IsR0FBcEIsb0JBQW9CLENBQXNCO1FBdkM1QyxpQkFBWSxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFFckMsY0FBUyxHQUFHLElBQUksZUFBZSxDQUFXLElBQUksQ0FBQyxDQUFDO1FBQ2hELDBCQUFxQixHQUFHLFlBQVksQ0FBQyxLQUFLLENBQUM7UUFHM0MsZ0JBQVcsR0FBRyxLQUFLLENBQUM7UUFDcEIsY0FBUyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDO1FBRXpDLDhCQUF5QixHQUFHLFlBQVksQ0FBQyxLQUFLLENBQUM7UUFDL0MsVUFBSyxHQUFHLElBQUksZUFBZSxDQUFNLElBQUksQ0FBQyxDQUFDO1FBR3BDLHNCQUFpQixHQUFHLElBQUksZUFBZSxDQUFVLEtBQUssQ0FBQyxDQUFDO1FBQzNELGdCQUFXLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFlBQVksRUFBRSxDQUFDO1FBT3BELDZCQUF3QixHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsd0JBQXdCLENBQUM7UUFFL0UsaUJBQVksR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBQztRQXVCeEMsYUFBUSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDLENBQUM7UUFMekUsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsRUFBRSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxTQUFTOzs7O1FBQUMsVUFBQSxDQUFDO1lBQ3ZGLFVBQVU7OztZQUFDLGNBQU0sT0FBQSxLQUFJLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxFQUExQixDQUEwQixFQUFDLENBQUM7UUFDL0MsQ0FBQyxFQUFDLENBQUMsQ0FBQztJQUNOLENBQUM7Ozs7SUFNRCxzQ0FBUTs7O0lBQVI7UUFBQSxpQkF1SEM7UUF0SEMsSUFBSSxDQUFDLHVCQUF1QixHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxzQkFBc0IsQ0FBQztRQUVoRiw4RkFBOEY7UUFDOUYsd0RBQXdEO1FBQ3hELG1DQUFtQztRQUNuQyxNQUFNO1FBRU4sOEZBQThGO1FBQzlGLHNEQUFzRDtRQUN0RCx3REFBd0Q7UUFDeEQsb0VBQW9FO1FBQ3BFLG1DQUFtQztRQUNuQywyQ0FBMkM7UUFDM0MsOENBQThDO1FBQzlDLHdDQUF3QztRQUN4QyxtRUFBbUU7UUFDbkUsVUFBVTtRQUNWLDhGQUE4RjtRQUM5RixNQUFNO1FBQ04sTUFBTTtRQUVOLEVBQUU7UUFDRixpRUFBaUU7UUFDakUsa0dBQWtHO1FBQ2xHLE1BQU07UUFHTixFQUFFO1FBRUYsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUc7OztRQUFDOztnQkFDaEMsS0FBSyxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsVUFBVTtZQUMxQyxPQUFPLEtBQUssRUFBRTtnQkFDWixJQUFJLEtBQUssQ0FBQyxVQUFVLEVBQUU7b0JBQ3BCLEtBQUssR0FBRyxLQUFLLENBQUMsVUFBVSxDQUFDO2lCQUMxQjtxQkFBTSxJQUFJLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFO29CQUM5QixPQUFPLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDO2lCQUM1QjtxQkFBTTtvQkFDTCxPQUFPLElBQUksQ0FBQztpQkFDYjthQUNGO1lBQ0QsT0FBTyxJQUFJLENBQUM7UUFDZCxDQUFDLEVBQUMsQ0FBQzthQUNGLFNBQVM7Ozs7UUFBQyxVQUFBLFVBQVU7O2dCQUNmLEtBQUssR0FBRyxLQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUTtZQUNqRCxPQUFPLEtBQUssQ0FBQyxVQUFVLElBQUksSUFBSSxFQUFFO2dCQUMvQixLQUFLLEdBQUcsS0FBSyxDQUFDLFVBQVUsQ0FBQzthQUMxQjs7Z0JBQ0ssSUFBSSxHQUFHLEtBQUssQ0FBQyxJQUFJOztnQkFFakIsT0FBTyxHQUFHLEtBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDO1lBQ3JDLHNEQUFzRDtZQUV0RCxLQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUN6QixJQUFJLE9BQU8sRUFBRTtnQkFDWCxLQUFJLENBQUMsWUFBWSxHQUFHLEtBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLG1CQUFBLE9BQU8sRUFBWSxDQUFDLENBQUM7Z0JBQ3ZFLEtBQUksQ0FBQyxZQUFZLENBQUMsU0FBUzs7OztnQkFBQyxVQUFBLENBQUMsSUFBSSxPQUFBLEtBQUksQ0FBQyxXQUFXLEdBQUcsQ0FBQyxFQUFwQixDQUFvQixFQUFDLENBQUM7YUFDeEQ7UUFDSCxDQUFDLEVBQUMsQ0FBQztRQUVILElBQUksQ0FBQyx5QkFBeUIsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQ3RELE1BQU07Ozs7UUFBQyxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUssWUFBWSxhQUFhLEVBQTlCLENBQThCLEVBQUMsRUFDL0MsR0FBRzs7O1FBQUM7O2dCQUNFLEtBQUssR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLFVBQVU7WUFDMUMsT0FBTyxLQUFLLEVBQUU7Z0JBQ1osSUFBSSxLQUFLLENBQUMsVUFBVSxFQUFFO29CQUNwQixLQUFLLEdBQUcsS0FBSyxDQUFDLFVBQVUsQ0FBQztpQkFDMUI7cUJBQU0sSUFBSSxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRTtvQkFDOUIsT0FBTyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQztpQkFDNUI7cUJBQU07b0JBQ0wsT0FBTyxJQUFJLENBQUM7aUJBQ2I7YUFDRjtZQUNELE9BQU8sSUFBSSxDQUFDO1FBQ2QsQ0FBQyxFQUFDLENBQUM7YUFDRixTQUFTOzs7O1FBQUMsVUFBQSxVQUFVOztnQkFDZixLQUFLLEdBQUcsS0FBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVE7WUFDakQsT0FBTyxLQUFLLENBQUMsVUFBVSxJQUFJLElBQUksRUFBRTtnQkFDL0IsS0FBSyxHQUFHLEtBQUssQ0FBQyxVQUFVLENBQUM7YUFDMUI7O2dCQUNLLElBQUksR0FBRyxLQUFLLENBQUMsSUFBSTs7Z0JBSWpCLE9BQU8sR0FBRyxLQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQztZQUNyQyxzREFBc0Q7WUFDdEQsS0FBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7WUFHekIsSUFBSSxPQUFPLEVBQUU7Z0JBQ1gsS0FBSSxDQUFDLFlBQVksR0FBRyxLQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxtQkFBQSxPQUFPLEVBQVksQ0FBQyxDQUFDO2dCQUN2RSxLQUFJLENBQUMsWUFBWSxDQUFDLFNBQVM7Ozs7Z0JBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxLQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsRUFBcEIsQ0FBb0IsRUFBQyxDQUFDO2FBQ3hEO1FBQ0gsQ0FBQyxFQUFDLENBQUM7UUFNTCxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVM7Ozs7UUFBQyxVQUFBLElBQUk7WUFFdkIsS0FBSSxDQUFDLHFCQUFxQixHQUFHLEtBQUksQ0FBQyxhQUFhLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUN0RSxPQUFPOzs7O1lBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLEVBQUQsQ0FBQyxFQUFDLEVBQ2YsTUFBTTs7OztZQUFDLFVBQUEsQ0FBQztnQkFDTixJQUFJLElBQUksSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLEVBQUUsSUFBSSxJQUFJLEVBQUU7b0JBQ25DLE9BQU8sS0FBSyxDQUFDO2lCQUNkO3FCQUFNOzt3QkFDQyxPQUFPLEdBQUcsQ0FBQyxDQUFDLEVBQUUsS0FBSyxJQUFJLENBQUMsRUFBRTtvQkFDaEMsT0FBTyxPQUFPLENBQUM7aUJBQ2hCO1lBQ0gsQ0FBQyxFQUFDLENBQ0gsQ0FBQyxTQUFTOzs7O1lBQUMsVUFBQSxJQUFJO2dCQUNkLEtBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBSTVCLENBQUMsRUFBQyxDQUFDO1FBQ0wsQ0FBQyxFQUFDLENBQUM7SUFFTCxDQUFDOzs7Ozs7SUFDTyx3Q0FBVTs7Ozs7SUFBbEIsVUFBbUIsVUFBVTs7WUFDckIsSUFBSSxHQUFHLFVBQVU7O1lBQ2pCLGNBQWMsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztRQUN4RyxJQUFJLGNBQWMsSUFBSSxJQUFJLEVBQUU7O2dCQUNwQixLQUFLLEdBQUcsY0FBYyxDQUFDLEtBQUs7OztnQkFDNUIsT0FBTyxHQUFHLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLEtBQUssT0FBQSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRTtZQUMvRSxPQUFPLE9BQU8sQ0FBQztTQUNoQjtJQUNILENBQUM7Ozs7SUFFRCxnREFBa0I7OztJQUFsQjs7WUFDUSxRQUFRLEdBQUcsUUFBUSxDQUFDLHNCQUFzQixDQUFDLGVBQWUsQ0FBQzs7WUFDM0QsT0FBTyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1FBQ2hDLElBQUksT0FBTyxJQUFJLElBQUksRUFBRTs7Z0JBQ2IsYUFBYSxHQUFHLE9BQU8sQ0FBQyxhQUFhLEVBQUU7WUFDN0MsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztTQUM1QzthQUFNO1lBQ0wsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNwQztJQUNILENBQUM7Ozs7SUFLRCw2Q0FBZTs7O0lBQWY7O1lBQ1EsVUFBVSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFOztZQUNsQyxPQUFPLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUM7UUFDM0MsSUFBSSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsbUJBQUEsT0FBTyxFQUFZLENBQUMsQ0FBQztJQUN0RCxDQUFDOzs7O0lBRUQseUNBQVc7OztJQUFYO1FBQ0UsSUFBSSxJQUFJLENBQUMseUJBQXlCLElBQUksSUFBSSxDQUFDLHlCQUF5QixDQUFDLE1BQU0sSUFBSSxLQUFLLEVBQUU7WUFDcEYsSUFBSSxDQUFDLHlCQUF5QixDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQzlDO1FBRUQsSUFBSSxJQUFJLENBQUMscUJBQXFCLElBQUksSUFBSSxDQUFDLHFCQUFxQixDQUFDLE1BQU0sSUFBSSxLQUFLLEVBQUU7WUFDNUUsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQzFDO1FBRUQsSUFBSSxJQUFJLENBQUMsWUFBWSxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxJQUFJLEtBQUssRUFBRTtZQUMxRCxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQ2pDO0lBRUgsQ0FBQzs7OztJQUVNLG9DQUFNOzs7SUFBYjtRQUNFLDREQUE0RDtRQUN0RCxpQkFBaUI7UUFDdkIsSUFBSSxDQUFDLG9CQUFvQixDQUFDLG1CQUFtQixFQUFFLENBQUM7UUFDaEQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUMxQixZQUFZO1FBQ2QsMkVBQTJFO1FBQ3ZFLEdBQUc7SUFDTCxDQUFDOztnQkF2T04sU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxrQkFBa0I7b0JBQzVCLGkxQ0FBMEM7b0JBRTFDLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxPQUFPOztpQkFDakQ7Ozs7Z0JBbEJRLGFBQWE7Z0JBRCtCLGNBQWM7Z0JBRzFELE1BQU07Z0JBQXlDLGNBQWM7Z0JBRzdELGNBQWM7Z0JBQ2QsWUFBWTtnQkFQK0gsU0FBUztnQkFBRSxNQUFNO2dCQUFFLGlCQUFpQjtnQkFRL0ssaUJBQWlCO2dCQUNDLFFBQVE7Z0JBQTFCLGdCQUFnQjtnQkFDaEIsZ0JBQWdCO2dCQUNoQixvQkFBb0I7OzswQkF1QjFCLFlBQVksU0FBQyxjQUFjO2tDQUkzQixTQUFTLFNBQUMsaUJBQWlCO2lDQUMzQixTQUFTLFNBQUMsZ0JBQWdCO2tDQUMxQixTQUFTLFNBQUMsaUJBQWlCO29DQUMzQixTQUFTLFNBQUMsbUJBQW1COztJQTZNaEMsMEJBQUM7Q0FBQSxBQXhPRCxJQXdPQztTQWxPWSxtQkFBbUI7Ozs7OztJQUU5QiwyQ0FBNEM7O0lBRTVDLHdDQUF1RDs7SUFDdkQsb0RBQWtEOztJQUVsRCwyQ0FBeUM7O0lBQ3pDLDBDQUEyQjs7SUFDM0Isd0NBQWdEOztJQUVoRCx3REFBc0Q7O0lBQ3RELG9DQUE4Qzs7SUFFOUMsc0NBQWtEOzs7OztJQUNsRCxnREFBa0U7O0lBQ2xFLDBDQUEyRDs7SUFFM0QsOENBQWdFOztJQUNoRSw2Q0FBOEQ7O0lBQzlELDhDQUFnRTs7SUFDaEUsZ0RBQW9FOztJQUVwRSx1REFBK0U7O0lBRS9FLDJDQUErQzs7SUF1Qi9DLHVDQUEyRTs7SUFFM0Usc0RBQW9EOztJQXRCbEQsNENBQW1DOzs7OztJQUNuQyw2Q0FBd0M7Ozs7O0lBQ3hDLHFDQUF3Qjs7Ozs7SUFDeEIsNkNBQXdDOzs7OztJQUN4Qyw2Q0FBd0M7Ozs7O0lBQ3hDLDJDQUFvQzs7Ozs7SUFDcEMsdUNBQTZCOzs7OztJQUM3QixtQ0FBc0I7Ozs7O0lBQ3RCLDZDQUEyQzs7Ozs7SUFDM0MsZ0RBQThDOzs7OztJQUM5Qyw4Q0FBbUM7Ozs7O0lBQ25DLCtDQUE0Qzs7Ozs7SUFDNUMsK0NBQTBDOzs7OztJQUMxQyxtREFBb0QiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3ksIEFwcGxpY2F0aW9uUmVmLCBWaWV3Q2hpbGQsIFRlbXBsYXRlUmVmLCBPbkRlc3Ryb3ksIEFmdGVyQ29udGVudEluaXQsIENvbnRlbnRDaGlsZCwgRWxlbWVudFJlZiwgUmVuZGVyZXIyLCBOZ1pvbmUsIENoYW5nZURldGVjdG9yUmVmIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBMYXlvdXRTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbGF5b3V0LnNlcnZpY2UnO1xuaW1wb3J0IHsgZGlzdGluY3RVbnRpbENoYW5nZWQsIGZpbHRlciwgbWFwLCBmbGF0TWFwLCBkZWJvdW5jZSwgZGVib3VuY2VUaW1lIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHsgUm91dGVyLCBOYXZpZ2F0aW9uU3RhcnQsIE5hdmlnYXRpb25FbmQsIEV2ZW50LCBBY3RpdmF0ZWRSb3V0ZSwgUm91dGVzUmVjb2duaXplZCwgUm91dGVyU3RhdGVTbmFwc2hvdCB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQgeyBTdWJzY3JpcHRpb24sIEJlaGF2aW9yU3ViamVjdCwgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgTWVudUl0ZW0gfSBmcm9tICcuLi9tZW51L21lbnUtaXRlbS9tZW51LWl0ZW0ubW9kZWwnO1xuaW1wb3J0IHsgRmF2TmF2c1NlcnZpY2UgfSBmcm9tICcuLi9mYXZuYXYvc3RhdGUvZmF2bmF2cy5zZXJ2aWNlJztcbmltcG9ydCB7IEZhdk5hdnNRdWVyeSB9IGZyb20gJy4uL2Zhdm5hdi9zdGF0ZS9mYXZuYXZzLnF1ZXJ5JztcbmltcG9ydCB7IEJyZWFkY3J1bWJTZXJ2aWNlIH0gZnJvbSAnLi9icmVhZGNydW1icy9icmVhZGNydW1iLnNlcnZpY2UnO1xuaW1wb3J0IHsgUGxhdGZvcm1Mb2NhdGlvbiwgTG9jYXRpb24gfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHsgU2Nyb2xsRGlzcGF0Y2hlciB9IGZyb20gJ0Bhbmd1bGFyL2Nkay9zY3JvbGxpbmcnO1xuaW1wb3J0IHsgUHJldmlvdXNSb3V0ZVNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9jb21wb25lbnRzL3ByZXZpb3VzLXJvdXRlLnNlcnZpY2UnO1xuXG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ3Vpa2l0LWJyZWFkY3J1bWInLFxuICB0ZW1wbGF0ZVVybDogJy4vYnJlYWRjcnVtYi5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2JyZWFkY3J1bWIuY29tcG9uZW50LnNjc3MnXSxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5EZWZhdWx0XG59KVxuZXhwb3J0IGNsYXNzIEJyZWFkY3J1bWJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XG5cbiAgcHJvdGVjdGVkIHN1YnNjcmlwdGlvbiA9IG5ldyBTdWJzY3JpcHRpb24oKTtcblxuICBwdWJsaWMgbWVudUl0ZW0kID0gbmV3IEJlaGF2aW9yU3ViamVjdDxNZW51SXRlbT4obnVsbCk7XG4gIHB1YmxpYyBtZW51SXRlbVN1YnNjcmlwdGlvbiQgPSBTdWJzY3JpcHRpb24uRU1QVFk7XG5cbiAgcHVibGljIGlzRmF2b3JpdGVkJDogT2JzZXJ2YWJsZTxib29sZWFuPjtcbiAgcHVibGljIGlzRmF2b3JpdGVkID0gZmFsc2U7XG4gIHB1YmxpYyBpc01vYmlsZSQgPSB0aGlzLmxheW91dFNlcnZpY2UuaXNNb2JpbGUkO1xuXG4gIHB1YmxpYyBuYXZpZ2F0aW9uRW5kU3Vic2NyaXB0aW9uID0gU3Vic2NyaXB0aW9uLkVNUFRZO1xuICBwdWJsaWMgZGF0YSQgPSBuZXcgQmVoYXZpb3JTdWJqZWN0PGFueT4obnVsbCk7XG5cbiAgQENvbnRlbnRDaGlsZCgndWlraXRBY3Rpb25zJykgYWN0aW9uczogRWxlbWVudFJlZjtcbiAgcHJvdGVjdGVkIGhhc0FjdGlvbnNTdWJqZWN0ID0gbmV3IEJlaGF2aW9yU3ViamVjdDxib29sZWFuPihmYWxzZSk7XG4gIHB1YmxpYyBoYXNBY3Rpb25zJCA9IHRoaXMuaGFzQWN0aW9uc1N1YmplY3QuYXNPYnNlcnZhYmxlKCk7XG5cbiAgQFZpZXdDaGlsZCgndGVtcGxhdGVBY3Rpb25zJykgdGVtcGxhdGVBY3Rpb25zOiBUZW1wbGF0ZVJlZjxhbnk+O1xuICBAVmlld0NoaWxkKCd0ZW1wbGF0ZUZpbmRlcicpIHRlbXBsYXRlRmluZGVyOiBUZW1wbGF0ZVJlZjxhbnk+O1xuICBAVmlld0NoaWxkKCd0ZW1wbGF0ZUZpbHRlcnMnKSB0ZW1wbGF0ZUZpbHRlcnM6IFRlbXBsYXRlUmVmPGFueT47XG4gIEBWaWV3Q2hpbGQoJ3RlbXBsYXRlUGFnaW5hdG9yJykgdGVtcGxhdGVQYWdpbmF0b3I6IFRlbXBsYXRlUmVmPGFueT47XG5cbiAgcHVibGljIHBvc3N1aVJlY3Vyc29EZUZhdm9yaXRvcyA9IHRoaXMuZmF2TmF2c1NlcnZpY2UucG9zc3VpUmVjdXJzb0RlRmF2b3JpdG9zO1xuXG4gIGhpZGRlblNoZWxsJCA9IHRoaXMubGF5b3V0U2VydmljZS5oaWRkZW5TaGVsbCQ7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHVibGljIGxheW91dFNlcnZpY2U6IExheW91dFNlcnZpY2UsXG4gICAgcHJvdGVjdGVkIGFwcGxpY2F0aW9uUmVmOiBBcHBsaWNhdGlvblJlZixcbiAgICBwcm90ZWN0ZWQgcm91dGVyOiBSb3V0ZXIsXG4gICAgcHJvdGVjdGVkIGFjdGl2YXRlZFJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSxcbiAgICBwcm90ZWN0ZWQgZmF2TmF2c1NlcnZpY2U6IEZhdk5hdnNTZXJ2aWNlLFxuICAgIHByb3RlY3RlZCBmYXZOYXZzUXVlcnk6IEZhdk5hdnNRdWVyeSxcbiAgICBwcm90ZWN0ZWQgcmVuZGVyZXI6IFJlbmRlcmVyMixcbiAgICBwcm90ZWN0ZWQgem9uZTogTmdab25lLFxuICAgIHByb3RlY3RlZCBjaGFuZ2VEZXRlY3RvcjogQ2hhbmdlRGV0ZWN0b3JSZWYsXG4gICAgcHJvdGVjdGVkIGJyZWFkY3J1bWJTZXJ2aWNlOiBCcmVhZGNydW1iU2VydmljZSxcbiAgICBwcm90ZWN0ZWQgYW5ndWxhckxvY2F0aW9uOiBMb2NhdGlvbixcbiAgICBwcm90ZWN0ZWQgcGxhdGZvcm1Mb2NhdGlvbjogUGxhdGZvcm1Mb2NhdGlvbixcbiAgICBwcml2YXRlIHNjcm9sbERpc3BhdGNoZXI6IFNjcm9sbERpc3BhdGNoZXIsXG4gICAgcHJvdGVjdGVkIHByZXZpb3VzUm91dGVTZXJ2aWNlOiBQcmV2aW91c1JvdXRlU2VydmljZSxcbiAgKSB7XG4gICAgdGhpcy5zdWJzY3JpcHRpb24uYWRkKHRoaXMuc2Nyb2xsRGlzcGF0Y2hlci5zY3JvbGxlZCgpLnBpcGUoZGVib3VuY2VUaW1lKDc1KSkuc3Vic2NyaWJlKHggPT4ge1xuICAgICAgc2V0VGltZW91dCgoKSA9PiB0aGlzLmFwcGxpY2F0aW9uUmVmLnRpY2soKSk7XG4gICAgfSkpO1xuICB9XG5cbiAgcHVibGljIGlzRml4ZWQkID0gdGhpcy5sYXlvdXRTZXJ2aWNlLmlzRml4ZWQkLnBpcGUoZGlzdGluY3RVbnRpbENoYW5nZWQoKSk7XG5cbiAgcHVibGljIGRlc2FiaWxpdGFyQm90YW9Wb2x0YXIkOiBPYnNlcnZhYmxlPGJvb2xlYW4+O1xuXG4gIG5nT25Jbml0KCkge1xuICAgIHRoaXMuZGVzYWJpbGl0YXJCb3Rhb1ZvbHRhciQgPSB0aGlzLnByZXZpb3VzUm91dGVTZXJ2aWNlLmRlc2FiaWxpdGFyQm90YW9Wb2x0YXI7XG5cbiAgICAvLyB0aGlzLnJvdXRlci5ldmVudHMucGlwZShmaWx0ZXIoZXZlbnQgPT4gZXZlbnQgaW5zdGFuY2VvZiBOYXZpZ2F0aW9uRW5kKSkuc3Vic2NyaWJlKG5hdiA9PiB7XG4gICAgLy8gICBjb25zdCBwYXJhbXMgPSB0aGlzLmFjdGl2YXRlZFJvdXRlLnNuYXBzaG90LnBhcmFtcztcbiAgICAvLyAgIGNvbnNvbGUubG9nKCdwYXJhbXMnLCBwYXJhbXMpO1xuICAgIC8vIH0pO1xuXG4gICAgLy8gdGhpcy5yb3V0ZXIuZXZlbnRzLnBpcGUoZmlsdGVyKGV2ZW50ID0+IGV2ZW50IGluc3RhbmNlb2YgTmF2aWdhdGlvbkVuZCkpLnN1YnNjcmliZShuYXYgPT4ge1xuICAgIC8vIC8vIHRoaXMuYWN0aXZhdGVkUm91dGUucGFyYW1zLnN1YnNjcmliZShwYXJhbXMgPT4ge1xuICAgIC8vICAgY29uc3QgcGFyYW1zID0gdGhpcy5hY3RpdmF0ZWRSb3V0ZS5zbmFwc2hvdC5wYXJhbXM7XG4gICAgLy8gICBjb25zdCBkYXRhID0gdGhpcy5hY3RpdmF0ZWRSb3V0ZS5yb290LmZpcnN0Q2hpbGQuc25hcHNob3QuZGF0YTtcbiAgICAvLyAgIGlmIChkYXRhICYmIGRhdGEuYnJlYWRjcnVtYikge1xuICAgIC8vICAgICBsZXQgdGl0bGU6IHN0cmluZyA9IGRhdGEuYnJlYWRjcnVtYjtcbiAgICAvLyAgICAgY29uc3QgcGFyYW1ldHJvcyA9IE9iamVjdC5rZXlzKHBhcmFtcyk7XG4gICAgLy8gICAgIHBhcmFtZXRyb3MuZm9yRWFjaChwYXJhbWV0cm8gPT4ge1xuICAgIC8vICAgICAgIHRpdGxlID0gdGl0bGUucmVwbGFjZShgOiR7cGFyYW1ldHJvfWAsIHBhcmFtc1twYXJhbWV0cm9dKTtcbiAgICAvLyAgICAgfSk7XG4gICAgLy8gICAgIHRoaXMuYnJlYWRjcnVtYlNlcnZpY2UuY2hhbmdlQnJlYWRjcnVtYih0aGlzLmFjdGl2YXRlZFJvdXRlLnNuYXBzaG90LCBkYXRhLmJyZWFkY3J1bWIpO1xuICAgIC8vICAgfVxuICAgIC8vIH0pO1xuXG4gICAgLy9cbiAgICAvLyB0aGlzLmJyZWFkY3J1bWJTZXJ2aWNlLmJyZWFkY3J1bWJDaGFuZ2VkLnN1YnNjcmliZShjcnVtYnMgPT4ge1xuICAgIC8vICAgdGhpcy5icmVhZGNydW1icyA9IGNydW1icy5tYXAoY3J1bWIgPT4gKHsgbGFiZWw6IGNydW1iLmRpc3BsYXlOYW1lLCB1cmw6IGAjJHtjcnVtYi51cmx9YCB9KSk7XG4gICAgLy8gfSk7XG5cblxuICAgIC8vXG5cbiAgICB0aGlzLmFjdGl2YXRlZFJvdXRlLnBhcmFtTWFwLnBpcGUobWFwKCgpID0+IHtcbiAgICAgIGxldCBjaGlsZCA9IHRoaXMuYWN0aXZhdGVkUm91dGUuZmlyc3RDaGlsZDtcbiAgICAgIHdoaWxlIChjaGlsZCkge1xuICAgICAgICBpZiAoY2hpbGQuZmlyc3RDaGlsZCkge1xuICAgICAgICAgIGNoaWxkID0gY2hpbGQuZmlyc3RDaGlsZDtcbiAgICAgICAgfSBlbHNlIGlmIChjaGlsZC5zbmFwc2hvdC5kYXRhKSB7XG4gICAgICAgICAgcmV0dXJuIGNoaWxkLnNuYXBzaG90LmRhdGE7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIHJldHVybiBudWxsO1xuICAgIH0pKVxuICAgIC5zdWJzY3JpYmUoY3VzdG9tRGF0YSA9PiB7XG4gICAgICBsZXQgcm91dGUgPSB0aGlzLnJvdXRlci5yb3V0ZXJTdGF0ZS5yb290LnNuYXBzaG90O1xuICAgICAgd2hpbGUgKHJvdXRlLmZpcnN0Q2hpbGQgIT0gbnVsbCkge1xuICAgICAgICByb3V0ZSA9IHJvdXRlLmZpcnN0Q2hpbGQ7XG4gICAgICB9XG4gICAgICBjb25zdCBkYXRhID0gcm91dGUuZGF0YTtcblxuICAgICAgY29uc3QgZmF2SXRlbSA9IHRoaXMuZ2V0RmF2SXRlbShkYXRhKTtcbiAgICAgIC8vIGZhdkl0ZW0uaWNvbiA9IGN1c3RvbURhdGEgPyBjdXN0b21EYXRhLmljb24gOiBudWxsO1xuXG4gICAgICB0aGlzLmRhdGEkLm5leHQoZmF2SXRlbSk7XG4gICAgICBpZiAoZmF2SXRlbSkge1xuICAgICAgICB0aGlzLmlzRmF2b3JpdGVkJCA9IHRoaXMuZmF2TmF2c1F1ZXJ5LmlzRmF2b3JpdGVkKGZhdkl0ZW0gYXMgTWVudUl0ZW0pO1xuICAgICAgICB0aGlzLmlzRmF2b3JpdGVkJC5zdWJzY3JpYmUociA9PiB0aGlzLmlzRmF2b3JpdGVkID0gcik7XG4gICAgICB9XG4gICAgfSk7XG5cbiAgICB0aGlzLm5hdmlnYXRpb25FbmRTdWJzY3JpcHRpb24gPSB0aGlzLnJvdXRlci5ldmVudHMucGlwZShcbiAgICAgIGZpbHRlcihldmVudCA9PiBldmVudCBpbnN0YW5jZW9mIE5hdmlnYXRpb25FbmQpLFxuICAgICAgbWFwKCgpID0+IHtcbiAgICAgICAgbGV0IGNoaWxkID0gdGhpcy5hY3RpdmF0ZWRSb3V0ZS5maXJzdENoaWxkO1xuICAgICAgICB3aGlsZSAoY2hpbGQpIHtcbiAgICAgICAgICBpZiAoY2hpbGQuZmlyc3RDaGlsZCkge1xuICAgICAgICAgICAgY2hpbGQgPSBjaGlsZC5maXJzdENoaWxkO1xuICAgICAgICAgIH0gZWxzZSBpZiAoY2hpbGQuc25hcHNob3QuZGF0YSkge1xuICAgICAgICAgICAgcmV0dXJuIGNoaWxkLnNuYXBzaG90LmRhdGE7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgIH0pKVxuICAgICAgLnN1YnNjcmliZShjdXN0b21EYXRhID0+IHtcbiAgICAgICAgbGV0IHJvdXRlID0gdGhpcy5yb3V0ZXIucm91dGVyU3RhdGUucm9vdC5zbmFwc2hvdDtcbiAgICAgICAgd2hpbGUgKHJvdXRlLmZpcnN0Q2hpbGQgIT0gbnVsbCkge1xuICAgICAgICAgIHJvdXRlID0gcm91dGUuZmlyc3RDaGlsZDtcbiAgICAgICAgfVxuICAgICAgICBjb25zdCBkYXRhID0gcm91dGUuZGF0YTtcblxuXG5cbiAgICAgICAgY29uc3QgZmF2SXRlbSA9IHRoaXMuZ2V0RmF2SXRlbShkYXRhKTtcbiAgICAgICAgLy8gZmF2SXRlbS5pY29uID0gY3VzdG9tRGF0YSA/IGN1c3RvbURhdGEuaWNvbiA6IG51bGw7XG4gICAgICAgIHRoaXMuZGF0YSQubmV4dChmYXZJdGVtKTtcblxuXG4gICAgICAgIGlmIChmYXZJdGVtKSB7XG4gICAgICAgICAgdGhpcy5pc0Zhdm9yaXRlZCQgPSB0aGlzLmZhdk5hdnNRdWVyeS5pc0Zhdm9yaXRlZChmYXZJdGVtIGFzIE1lbnVJdGVtKTtcbiAgICAgICAgICB0aGlzLmlzRmF2b3JpdGVkJC5zdWJzY3JpYmUociA9PiB0aGlzLmlzRmF2b3JpdGVkID0gcik7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuXG5cblxuXG5cbiAgICB0aGlzLmRhdGEkLnN1YnNjcmliZShkYXRhID0+IHtcblxuICAgICAgdGhpcy5tZW51SXRlbVN1YnNjcmlwdGlvbiQgPSB0aGlzLmxheW91dFNlcnZpY2UubWVudUl0ZW1zRmxhdHRlbmVkJC5waXBlKFxuICAgICAgICBmbGF0TWFwKHIgPT4gciksXG4gICAgICAgIGZpbHRlcihyID0+IHtcbiAgICAgICAgICBpZiAoZGF0YSA9PSBudWxsIHx8IGRhdGEuaWQgPT0gbnVsbCkge1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBjb25zdCByZXRvcm5vID0gci5pZCA9PT0gZGF0YS5pZDtcbiAgICAgICAgICAgIHJldHVybiByZXRvcm5vO1xuICAgICAgICAgIH1cbiAgICAgICAgfSlcbiAgICAgICkuc3Vic2NyaWJlKGl0ZW0gPT4ge1xuICAgICAgICB0aGlzLm1lbnVJdGVtJC5uZXh0KGl0ZW0pO1xuXG5cblxuICAgICAgfSk7XG4gICAgfSk7XG5cbiAgfVxuICBwcml2YXRlIGdldEZhdkl0ZW0oY3VzdG9tRGF0YSkge1xuICAgIGNvbnN0IGRhdGEgPSBjdXN0b21EYXRhO1xuICAgIGNvbnN0IGxhc3RCcmVhZENydW1iID0gdGhpcy5icmVhZGNydW1iU2VydmljZS5icmVhZGNydW1ic1t0aGlzLmJyZWFkY3J1bWJTZXJ2aWNlLmJyZWFkY3J1bWJzLmxlbmd0aCAtIDFdO1xuICAgIGlmIChsYXN0QnJlYWRDcnVtYiAhPSBudWxsKSB7XG4gICAgICBjb25zdCB0aXRsZSA9IGxhc3RCcmVhZENydW1iLnRpdGxlOyAvLyB0aGlzLmJyZWFkY3J1bWJTZXJ2aWNlLmdldFRpdGxlRm9ybWF0dGVkKGRhdGEuYnJlYWRjcnVtYiwgdGhpcy5hY3RpdmF0ZWRSb3V0ZS5zbmFwc2hvdCk7XG4gICAgICBjb25zdCBmYXZJdGVtID0geyBsaW5rOiB0aGlzLnJvdXRlci51cmwsIHRpdGxlLCBpY29uOiBkYXRhID8gZGF0YS5pY29uIDogbnVsbCB9O1xuICAgICAgcmV0dXJuIGZhdkl0ZW07XG4gICAgfVxuICB9XG5cbiAgdXBkYXRlSWZIYXNBY3Rpb25zKCkge1xuICAgIGNvbnN0IGVsZW1lbnRzID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgndWlraXQtYWN0aW9ucycpO1xuICAgIGNvbnN0IGVsZW1lbnQgPSBlbGVtZW50cy5pdGVtKDApO1xuICAgIGlmIChlbGVtZW50ICE9IG51bGwpIHtcbiAgICAgIGNvbnN0IGhhc0NoaWxkTm9kZXMgPSBlbGVtZW50Lmhhc0NoaWxkTm9kZXMoKTtcbiAgICAgIHRoaXMuaGFzQWN0aW9uc1N1YmplY3QubmV4dChoYXNDaGlsZE5vZGVzKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5oYXNBY3Rpb25zU3ViamVjdC5uZXh0KGZhbHNlKTtcbiAgICB9XG4gIH1cblxuXG5cblxuICB0b2dnbGVGYXZvcml0b3MoKSB7XG4gICAgY29uc3QgY3VzdG9tRGF0YSA9IHRoaXMuZGF0YSQuZ2V0VmFsdWUoKTtcbiAgICBjb25zdCBmYXZJdGVtID0gdGhpcy5nZXRGYXZJdGVtKGN1c3RvbURhdGEpO1xuICAgIHRoaXMuZmF2TmF2c1NlcnZpY2UudG9nZ2xlSXRlbShmYXZJdGVtIGFzIE1lbnVJdGVtKTtcbiAgfVxuXG4gIG5nT25EZXN0cm95KCk6IHZvaWQge1xuICAgIGlmICh0aGlzLm5hdmlnYXRpb25FbmRTdWJzY3JpcHRpb24gJiYgdGhpcy5uYXZpZ2F0aW9uRW5kU3Vic2NyaXB0aW9uLmNsb3NlZCA9PSBmYWxzZSkge1xuICAgICAgdGhpcy5uYXZpZ2F0aW9uRW5kU3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XG4gICAgfVxuXG4gICAgaWYgKHRoaXMubWVudUl0ZW1TdWJzY3JpcHRpb24kICYmIHRoaXMubWVudUl0ZW1TdWJzY3JpcHRpb24kLmNsb3NlZCA9PSBmYWxzZSkge1xuICAgICAgdGhpcy5tZW51SXRlbVN1YnNjcmlwdGlvbiQudW5zdWJzY3JpYmUoKTtcbiAgICB9XG5cbiAgICBpZiAodGhpcy5zdWJzY3JpcHRpb24gJiYgdGhpcy5zdWJzY3JpcHRpb24uY2xvc2VkID09IGZhbHNlKSB7XG4gICAgICB0aGlzLnN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xuICAgIH1cblxuICB9XG5cbiAgcHVibGljIGdvQmFjaygpIHtcbiAgICAvLyAgICBpZiAoZG9jdW1lbnQucmVmZXJyZXIuaW5kZXhPZihsb2NhdGlvbi5ob3N0KSAhPT0gLTEpIHtcbiAgICAgICAgICAvL2hpc3RvcnkuZ28oLTEpO1xuICAgIHRoaXMucHJldmlvdXNSb3V0ZVNlcnZpY2UucmVtb3ZlclJvdGFBY2Vzc2FkYSgpO1xuICAgIHRoaXMuYW5ndWxhckxvY2F0aW9uLmJhY2soKTtcbiAgICAgIC8vICB9IGVsc2Uge1xuICAgIC8vICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoW2AuLi9gXSwgeyByZWxhdGl2ZVRvOiB0aGlzLmFjdGl2YXRlZFJvdXRlIH0pO1xuICAgICAgICAvL31cbiAgICAgIH1cbn1cbiJdfQ==