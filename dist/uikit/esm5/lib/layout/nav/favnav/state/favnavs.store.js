/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { EntityStore, StoreConfig } from '@datorama/akita';
import * as i0 from "@angular/core";
/**
 * @record
 */
export function FavNavsState() { }
var FavNavsStore = /** @class */ (function (_super) {
    tslib_1.__extends(FavNavsStore, _super);
    function FavNavsStore() {
        return _super.call(this) || this;
    }
    FavNavsStore.decorators = [
        { type: Injectable, args: [{ providedIn: 'root' },] }
    ];
    /** @nocollapse */
    FavNavsStore.ctorParameters = function () { return []; };
    /** @nocollapse */ FavNavsStore.ngInjectableDef = i0.defineInjectable({ factory: function FavNavsStore_Factory() { return new FavNavsStore(); }, token: FavNavsStore, providedIn: "root" });
    FavNavsStore = tslib_1.__decorate([
        StoreConfig({ name: 'favnavs' }),
        tslib_1.__metadata("design:paramtypes", [])
    ], FavNavsStore);
    return FavNavsStore;
}(EntityStore));
export { FavNavsStore };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmF2bmF2cy5zdG9yZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvbGF5b3V0L25hdi9mYXZuYXYvc3RhdGUvZmF2bmF2cy5zdG9yZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFlLFdBQVcsRUFBRSxXQUFXLEVBQWUsTUFBTSxpQkFBaUIsQ0FBQzs7Ozs7QUFHckYsa0NBQStEOztJQUk3Qix3Q0FBbUM7SUFDbkU7ZUFDRSxpQkFBTztJQUNULENBQUM7O2dCQUxGLFVBQVUsU0FBQyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUU7Ozs7O0lBRXJCLFlBQVk7UUFEeEIsV0FBVyxDQUFDLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxDQUFDOztPQUNwQixZQUFZLENBSXhCO3VCQVpEO0NBWUMsQ0FKaUMsV0FBVyxHQUk1QztTQUpZLFlBQVkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBFbnRpdHlTdGF0ZSwgRW50aXR5U3RvcmUsIFN0b3JlQ29uZmlnLCB0cmFuc2FjdGlvbiB9IGZyb20gJ0BkYXRvcmFtYS9ha2l0YSc7XG5pbXBvcnQgeyBNZW51SXRlbSB9IGZyb20gJy4uLy4uL21lbnUvbWVudS1pdGVtL21lbnUtaXRlbS5tb2RlbCc7XG5cbmV4cG9ydCBpbnRlcmZhY2UgRmF2TmF2c1N0YXRlIGV4dGVuZHMgRW50aXR5U3RhdGU8TWVudUl0ZW0+IHsgfVxuXG5ASW5qZWN0YWJsZSh7IHByb3ZpZGVkSW46ICdyb290JyB9KVxuQFN0b3JlQ29uZmlnKHsgbmFtZTogJ2Zhdm5hdnMnIH0pXG5leHBvcnQgY2xhc3MgRmF2TmF2c1N0b3JlIGV4dGVuZHMgRW50aXR5U3RvcmU8RmF2TmF2c1N0YXRlLCBNZW51SXRlbT4ge1xuICBjb25zdHJ1Y3RvcigpIHtcbiAgICBzdXBlcigpO1xuICB9XG59XG4iXX0=