/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Inject, Injectable, Optional } from '@angular/core';
import { FavNavsStore } from './favnavs.store';
import { createItem } from '../../menu/menu-item/menu-item.model';
import { FavNavsQuery } from './favnavs.query';
import { moveItemInArray } from '@angular/cdk/drag-drop';
import { FAVORITOS_SERVICE_TOKEN, FavoritosService } from '../../../../utils/favoritos.service';
import { Router } from '@angular/router';
import { isDefined } from '@datorama/akita';
import { UserService } from '../../../../shared/auth/authentication/user.service';
import * as i0 from "@angular/core";
import * as i1 from "./favnavs.store";
import * as i2 from "./favnavs.query";
import * as i3 from "@angular/router";
import * as i4 from "../../../../shared/auth/authentication/user.service";
import * as i5 from "../../../../utils/favoritos.service";
var FavNavsService = /** @class */ (function () {
    function FavNavsService(favNavsStore, favNavsQuery, router, userService, favoritosService) {
        var _this = this;
        this.favNavsStore = favNavsStore;
        this.favNavsQuery = favNavsQuery;
        this.router = router;
        this.userService = userService;
        this.favoritosService = favoritosService;
        this.possuiRecursoDeFavoritos = this.favoritosService != null;
        this.possuiRecursoDeFavoritos = this.favoritosService != null;
        if (this.possuiRecursoDeFavoritos) {
            // Para cada alteração do usuário, devo então buscar os favoritos deste
            this.userService.user$.subscribe((/**
             * @param {?} user
             * @return {?}
             */
            function (user) {
                _this.favoritosService.buscar()
                    .subscribe((/**
                 * @param {?} menuItens
                 * @return {?}
                 */
                function (menuItens) {
                    _this.favNavsStore.set(menuItens);
                }));
            }));
            this.favoritosService.buscar()
                .subscribe((/**
             * @param {?} menuItens
             * @return {?}
             */
            function (menuItens) {
                _this.favNavsStore.set(menuItens);
            }));
        }
    }
    /**
     * @param {?} item
     * @return {?}
     */
    FavNavsService.prototype.toggleItem = /**
     * @param {?} item
     * @return {?}
     */
    function (item) {
        /** @type {?} */
        var itemFavorited = this.favNavsQuery.getFavorited(item);
        if (isDefined(itemFavorited)) {
            this.remover(itemFavorited);
        }
        else {
            if (isDefined(item.link)) {
                /** @type {?} */
                var parsedUrl = this.router.createUrlTree(Array.isArray(item.link) ? item.link : [item.link]);
                item.link = parsedUrl.toString();
            }
            /** @type {?} */
            var newItem = createItem(item);
            this.adicionar(newItem);
        }
    };
    /**
     * @param {?} indiceOrigem
     * @param {?} indiceDestino
     * @return {?}
     */
    FavNavsService.prototype.moverItens = /**
     * @param {?} indiceOrigem
     * @param {?} indiceDestino
     * @return {?}
     */
    function (indiceOrigem, indiceDestino) {
        /** @type {?} */
        var itens = this.favNavsQuery.getAll();
        moveItemInArray(itens, indiceOrigem, indiceDestino);
        this.substituirColecao(itens);
    };
    /**
     * @private
     * @param {?} item
     * @return {?}
     */
    FavNavsService.prototype.adicionar = /**
     * @private
     * @param {?} item
     * @return {?}
     */
    function (item) {
        this.favNavsStore.add(item);
        this.atualizar();
    };
    /**
     * @private
     * @param {?} item
     * @return {?}
     */
    FavNavsService.prototype.remover = /**
     * @private
     * @param {?} item
     * @return {?}
     */
    function (item) {
        this.favNavsStore.remove(item.id);
        this.atualizar();
    };
    /**
     * @private
     * @param {?} itens
     * @return {?}
     */
    FavNavsService.prototype.substituirColecao = /**
     * @private
     * @param {?} itens
     * @return {?}
     */
    function (itens) {
        this.favNavsStore.set(itens);
        this.atualizar();
    };
    /**
     * @private
     * @return {?}
     */
    FavNavsService.prototype.atualizar = /**
     * @private
     * @return {?}
     */
    function () {
        if (this.possuiRecursoDeFavoritos) {
            /** @type {?} */
            var itens = this.favNavsQuery.getAll();
            this.favoritosService.salvar(itens)
                // .pipe(
                //   retryWhen(errors => errors.pipe(delay(1000), take(5)))
                // )
                .subscribe();
        }
    };
    FavNavsService.decorators = [
        { type: Injectable, args: [{ providedIn: 'root' },] }
    ];
    /** @nocollapse */
    FavNavsService.ctorParameters = function () { return [
        { type: FavNavsStore },
        { type: FavNavsQuery },
        { type: Router },
        { type: UserService },
        { type: FavoritosService, decorators: [{ type: Optional }, { type: Inject, args: [FAVORITOS_SERVICE_TOKEN,] }] }
    ]; };
    /** @nocollapse */ FavNavsService.ngInjectableDef = i0.defineInjectable({ factory: function FavNavsService_Factory() { return new FavNavsService(i0.inject(i1.FavNavsStore), i0.inject(i2.FavNavsQuery), i0.inject(i3.Router), i0.inject(i4.UserService), i0.inject(i5.FAVORITOS_SERVICE_TOKEN, 8)); }, token: FavNavsService, providedIn: "root" });
    return FavNavsService;
}());
export { FavNavsService };
if (false) {
    /** @type {?} */
    FavNavsService.prototype.possuiRecursoDeFavoritos;
    /**
     * @type {?}
     * @private
     */
    FavNavsService.prototype.favNavsStore;
    /**
     * @type {?}
     * @private
     */
    FavNavsService.prototype.favNavsQuery;
    /**
     * @type {?}
     * @private
     */
    FavNavsService.prototype.router;
    /**
     * @type {?}
     * @private
     */
    FavNavsService.prototype.userService;
    /**
     * @type {?}
     * @private
     */
    FavNavsService.prototype.favoritosService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmF2bmF2cy5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9sYXlvdXQvbmF2L2Zhdm5hdi9zdGF0ZS9mYXZuYXZzLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxNQUFNLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUMzRCxPQUFPLEVBQUMsWUFBWSxFQUFDLE1BQU0saUJBQWlCLENBQUM7QUFDN0MsT0FBTyxFQUFDLFVBQVUsRUFBVyxNQUFNLHNDQUFzQyxDQUFDO0FBQzFFLE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUM3QyxPQUFPLEVBQUMsZUFBZSxFQUFDLE1BQU0sd0JBQXdCLENBQUM7QUFDdkQsT0FBTyxFQUFDLHVCQUF1QixFQUFFLGdCQUFnQixFQUFDLE1BQU0scUNBQXFDLENBQUM7QUFDOUYsT0FBTyxFQUFDLE1BQU0sRUFBQyxNQUFNLGlCQUFpQixDQUFDO0FBQ3ZDLE9BQU8sRUFBQyxTQUFTLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUMxQyxPQUFPLEVBQUMsV0FBVyxFQUFDLE1BQU0scURBQXFELENBQUM7Ozs7Ozs7QUFHaEY7SUFJRSx3QkFDVSxZQUEwQixFQUMxQixZQUEwQixFQUMxQixNQUFjLEVBQ2QsV0FBd0IsRUFDcUIsZ0JBQWtDO1FBTHpGLGlCQXlCRztRQXhCTyxpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUMxQixpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUMxQixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDcUIscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQVB6Riw2QkFBd0IsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLElBQUksSUFBSSxDQUFDO1FBUXJELElBQUksQ0FBQyx3QkFBd0IsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLElBQUksSUFBSSxDQUFDO1FBRTlELElBQUksSUFBSSxDQUFDLHdCQUF3QixFQUFFO1lBQ2pDLHVFQUF1RTtZQUN2RSxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxTQUFTOzs7O1lBQUMsVUFBQSxJQUFJO2dCQUVuQyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxFQUFFO3FCQUM3QixTQUFTOzs7O2dCQUFDLFVBQUMsU0FBUztvQkFDbkIsS0FBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQ25DLENBQUMsRUFBQyxDQUFDO1lBRUwsQ0FBQyxFQUFDLENBQUM7WUFFSCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxFQUFFO2lCQUMzQixTQUFTOzs7O1lBQUMsVUFBQyxTQUFTO2dCQUNuQixLQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUNuQyxDQUFDLEVBQUMsQ0FBQztTQUNOO0lBRUgsQ0FBQzs7Ozs7SUFFSCxtQ0FBVTs7OztJQUFWLFVBQVcsSUFBYzs7WUFDakIsYUFBYSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQztRQUUxRCxJQUFJLFNBQVMsQ0FBQyxhQUFhLENBQUMsRUFBRTtZQUM1QixJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1NBQzdCO2FBQU07WUFDTCxJQUFJLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUU7O29CQUNsQixTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUMvRixJQUFJLENBQUMsSUFBSSxHQUFHLFNBQVMsQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUNsQzs7Z0JBRUssT0FBTyxHQUFHLFVBQVUsQ0FBQyxJQUFJLENBQUM7WUFDaEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQztTQUN6QjtJQUNILENBQUM7Ozs7OztJQUVELG1DQUFVOzs7OztJQUFWLFVBQVcsWUFBb0IsRUFBRSxhQUFxQjs7WUFDOUMsS0FBSyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFO1FBQ3hDLGVBQWUsQ0FBQyxLQUFLLEVBQUUsWUFBWSxFQUFFLGFBQWEsQ0FBQyxDQUFDO1FBQ3BELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNoQyxDQUFDOzs7Ozs7SUFFTyxrQ0FBUzs7Ozs7SUFBakIsVUFBa0IsSUFBYztRQUM5QixJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM1QixJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7SUFDbkIsQ0FBQzs7Ozs7O0lBRU8sZ0NBQU87Ozs7O0lBQWYsVUFBZ0IsSUFBYztRQUM1QixJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDbEMsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO0lBQ25CLENBQUM7Ozs7OztJQUVPLDBDQUFpQjs7Ozs7SUFBekIsVUFBMEIsS0FBaUI7UUFDekMsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDN0IsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO0lBQ25CLENBQUM7Ozs7O0lBRU8sa0NBQVM7Ozs7SUFBakI7UUFDRSxJQUFJLElBQUksQ0FBQyx3QkFBd0IsRUFBRTs7Z0JBQzNCLEtBQUssR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRTtZQUN4QyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQztnQkFDakMsU0FBUztnQkFDVCwyREFBMkQ7Z0JBQzNELElBQUk7aUJBQ0wsU0FBUyxFQUFFLENBQUM7U0FDZDtJQUNILENBQUM7O2dCQTdFRixVQUFVLFNBQUMsRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFOzs7O2dCQVYxQixZQUFZO2dCQUVaLFlBQVk7Z0JBR1osTUFBTTtnQkFFTixXQUFXO2dCQUhjLGdCQUFnQix1QkFlNUMsUUFBUSxZQUFJLE1BQU0sU0FBQyx1QkFBdUI7Ozt5QkFwQi9DO0NBeUZDLEFBOUVELElBOEVDO1NBN0VZLGNBQWM7OztJQUN6QixrREFBeUQ7Ozs7O0lBR3ZELHNDQUFrQzs7Ozs7SUFDbEMsc0NBQWtDOzs7OztJQUNsQyxnQ0FBc0I7Ozs7O0lBQ3RCLHFDQUFnQzs7Ozs7SUFDaEMsMENBQXVGIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtJbmplY3QsIEluamVjdGFibGUsIE9wdGlvbmFsfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7RmF2TmF2c1N0b3JlfSBmcm9tICcuL2Zhdm5hdnMuc3RvcmUnO1xuaW1wb3J0IHtjcmVhdGVJdGVtLCBNZW51SXRlbX0gZnJvbSAnLi4vLi4vbWVudS9tZW51LWl0ZW0vbWVudS1pdGVtLm1vZGVsJztcbmltcG9ydCB7RmF2TmF2c1F1ZXJ5fSBmcm9tICcuL2Zhdm5hdnMucXVlcnknO1xuaW1wb3J0IHttb3ZlSXRlbUluQXJyYXl9IGZyb20gJ0Bhbmd1bGFyL2Nkay9kcmFnLWRyb3AnO1xuaW1wb3J0IHtGQVZPUklUT1NfU0VSVklDRV9UT0tFTiwgRmF2b3JpdG9zU2VydmljZX0gZnJvbSAnLi4vLi4vLi4vLi4vdXRpbHMvZmF2b3JpdG9zLnNlcnZpY2UnO1xuaW1wb3J0IHtSb3V0ZXJ9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQge2lzRGVmaW5lZH0gZnJvbSAnQGRhdG9yYW1hL2FraXRhJztcbmltcG9ydCB7VXNlclNlcnZpY2V9IGZyb20gJy4uLy4uLy4uLy4uL3NoYXJlZC9hdXRoL2F1dGhlbnRpY2F0aW9uL3VzZXIuc2VydmljZSc7XG5cblxuQEluamVjdGFibGUoeyBwcm92aWRlZEluOiAncm9vdCcgfSlcbmV4cG9ydCBjbGFzcyBGYXZOYXZzU2VydmljZSB7XG4gIHBvc3N1aVJlY3Vyc29EZUZhdm9yaXRvcyA9IHRoaXMuZmF2b3JpdG9zU2VydmljZSAhPSBudWxsO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByaXZhdGUgZmF2TmF2c1N0b3JlOiBGYXZOYXZzU3RvcmUsXG4gICAgcHJpdmF0ZSBmYXZOYXZzUXVlcnk6IEZhdk5hdnNRdWVyeSxcbiAgICBwcml2YXRlIHJvdXRlcjogUm91dGVyLFxuICAgIHByaXZhdGUgdXNlclNlcnZpY2U6IFVzZXJTZXJ2aWNlLFxuICAgIEBPcHRpb25hbCgpIEBJbmplY3QoRkFWT1JJVE9TX1NFUlZJQ0VfVE9LRU4pIHByaXZhdGUgZmF2b3JpdG9zU2VydmljZTogRmF2b3JpdG9zU2VydmljZSkge1xuICAgICAgdGhpcy5wb3NzdWlSZWN1cnNvRGVGYXZvcml0b3MgPSB0aGlzLmZhdm9yaXRvc1NlcnZpY2UgIT0gbnVsbDtcblxuICAgICAgaWYgKHRoaXMucG9zc3VpUmVjdXJzb0RlRmF2b3JpdG9zKSB7XG4gICAgICAgIC8vIFBhcmEgY2FkYSBhbHRlcmHDp8OjbyBkbyB1c3XDoXJpbywgZGV2byBlbnTDo28gYnVzY2FyIG9zIGZhdm9yaXRvcyBkZXN0ZVxuICAgICAgICB0aGlzLnVzZXJTZXJ2aWNlLnVzZXIkLnN1YnNjcmliZSh1c2VyID0+IHtcblxuICAgICAgICAgIHRoaXMuZmF2b3JpdG9zU2VydmljZS5idXNjYXIoKVxuICAgICAgICAgIC5zdWJzY3JpYmUoKG1lbnVJdGVucykgPT4ge1xuICAgICAgICAgICAgdGhpcy5mYXZOYXZzU3RvcmUuc2V0KG1lbnVJdGVucyk7XG4gICAgICAgICAgfSk7XG5cbiAgICAgICAgfSk7XG5cbiAgICAgICAgdGhpcy5mYXZvcml0b3NTZXJ2aWNlLmJ1c2NhcigpXG4gICAgICAgICAgLnN1YnNjcmliZSgobWVudUl0ZW5zKSA9PiB7XG4gICAgICAgICAgICB0aGlzLmZhdk5hdnNTdG9yZS5zZXQobWVudUl0ZW5zKTtcbiAgICAgICAgICB9KTtcbiAgICAgIH1cblxuICAgIH1cblxuICB0b2dnbGVJdGVtKGl0ZW06IE1lbnVJdGVtKSB7XG4gICAgY29uc3QgaXRlbUZhdm9yaXRlZCA9IHRoaXMuZmF2TmF2c1F1ZXJ5LmdldEZhdm9yaXRlZChpdGVtKTtcblxuICAgIGlmIChpc0RlZmluZWQoaXRlbUZhdm9yaXRlZCkpIHtcbiAgICAgIHRoaXMucmVtb3ZlcihpdGVtRmF2b3JpdGVkKTtcbiAgICB9IGVsc2Uge1xuICAgICAgaWYgKGlzRGVmaW5lZChpdGVtLmxpbmspKSB7XG4gICAgICAgIGNvbnN0IHBhcnNlZFVybCA9IHRoaXMucm91dGVyLmNyZWF0ZVVybFRyZWUoQXJyYXkuaXNBcnJheShpdGVtLmxpbmspID8gaXRlbS5saW5rIDogW2l0ZW0ubGlua10pO1xuICAgICAgICBpdGVtLmxpbmsgPSBwYXJzZWRVcmwudG9TdHJpbmcoKTtcbiAgICAgIH1cblxuICAgICAgY29uc3QgbmV3SXRlbSA9IGNyZWF0ZUl0ZW0oaXRlbSk7XG4gICAgICB0aGlzLmFkaWNpb25hcihuZXdJdGVtKTtcbiAgICB9XG4gIH1cblxuICBtb3Zlckl0ZW5zKGluZGljZU9yaWdlbTogbnVtYmVyLCBpbmRpY2VEZXN0aW5vOiBudW1iZXIpIHtcbiAgICBjb25zdCBpdGVucyA9IHRoaXMuZmF2TmF2c1F1ZXJ5LmdldEFsbCgpO1xuICAgIG1vdmVJdGVtSW5BcnJheShpdGVucywgaW5kaWNlT3JpZ2VtLCBpbmRpY2VEZXN0aW5vKTtcbiAgICB0aGlzLnN1YnN0aXR1aXJDb2xlY2FvKGl0ZW5zKTtcbiAgfVxuXG4gIHByaXZhdGUgYWRpY2lvbmFyKGl0ZW06IE1lbnVJdGVtKSB7XG4gICAgdGhpcy5mYXZOYXZzU3RvcmUuYWRkKGl0ZW0pO1xuICAgIHRoaXMuYXR1YWxpemFyKCk7XG4gIH1cblxuICBwcml2YXRlIHJlbW92ZXIoaXRlbTogTWVudUl0ZW0pIHtcbiAgICB0aGlzLmZhdk5hdnNTdG9yZS5yZW1vdmUoaXRlbS5pZCk7XG4gICAgdGhpcy5hdHVhbGl6YXIoKTtcbiAgfVxuXG4gIHByaXZhdGUgc3Vic3RpdHVpckNvbGVjYW8oaXRlbnM6IE1lbnVJdGVtW10pIHtcbiAgICB0aGlzLmZhdk5hdnNTdG9yZS5zZXQoaXRlbnMpO1xuICAgIHRoaXMuYXR1YWxpemFyKCk7XG4gIH1cblxuICBwcml2YXRlIGF0dWFsaXphcigpIHtcbiAgICBpZiAodGhpcy5wb3NzdWlSZWN1cnNvRGVGYXZvcml0b3MpIHtcbiAgICAgIGNvbnN0IGl0ZW5zID0gdGhpcy5mYXZOYXZzUXVlcnkuZ2V0QWxsKCk7XG4gICAgICB0aGlzLmZhdm9yaXRvc1NlcnZpY2Uuc2FsdmFyKGl0ZW5zKVxuICAgICAgICAvLyAucGlwZShcbiAgICAgICAgLy8gICByZXRyeVdoZW4oZXJyb3JzID0+IGVycm9ycy5waXBlKGRlbGF5KDEwMDApLCB0YWtlKDUpKSlcbiAgICAgICAgLy8gKVxuICAgICAgLnN1YnNjcmliZSgpO1xuICAgIH1cbiAgfVxufVxuIl19