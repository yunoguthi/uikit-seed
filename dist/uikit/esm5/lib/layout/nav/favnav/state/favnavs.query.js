/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { QueryEntity, isDefined, toBoolean } from '@datorama/akita';
import { FavNavsStore } from './favnavs.store';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { map } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "./favnavs.store";
import * as i2 from "@angular/router";
import * as i3 from "@angular/common";
var FavNavsQuery = /** @class */ (function (_super) {
    tslib_1.__extends(FavNavsQuery, _super);
    function FavNavsQuery(store, router, ngLocation) {
        var _this = _super.call(this, store) || this;
        _this.store = store;
        _this.router = router;
        _this.ngLocation = ngLocation;
        _this.favorites$ = _this.selectAll();
        return _this;
    }
    /**
     * @param {?} menuItem
     * @return {?}
     */
    FavNavsQuery.prototype.isFavorited = /**
     * @param {?} menuItem
     * @return {?}
     */
    function (menuItem) {
        var _this = this;
        return this.selectAll({
            filterBy: (/**
             * @param {?} item
             * @return {?}
             */
            function (item) {
                /** @type {?} */
                var menuItemLink = _this.router.createUrlTree(Array.isArray(menuItem.link) ? menuItem.link : [menuItem.link]).toString();
                return item.link === menuItemLink;
            })
        }).pipe(map((/**
         * @param {?} value
         * @return {?}
         */
        function (value) { return toBoolean(value); })));
    };
    /**
     * @param {?} menuItem
     * @return {?}
     */
    FavNavsQuery.prototype.getIsFavorited = /**
     * @param {?} menuItem
     * @return {?}
     */
    function (menuItem) {
        var _this = this;
        return this.getAll({
            filterBy: (/**
             * @param {?} item
             * @return {?}
             */
            function (item) {
                /** @type {?} */
                var menuItemLink = _this.router.createUrlTree(Array.isArray(menuItem.link) ? menuItem.link : [menuItem.link]).toString();
                return item.link === menuItemLink;
            })
        }).length > 0;
    };
    /**
     * @param {?} menuItem
     * @return {?}
     */
    FavNavsQuery.prototype.getFavorited = /**
     * @param {?} menuItem
     * @return {?}
     */
    function (menuItem) {
        var _this = this;
        return this.getAll({
            filterBy: (/**
             * @param {?} item
             * @return {?}
             */
            function (item) {
                /** @type {?} */
                var menuItemLink = _this.router.createUrlTree(Array.isArray(menuItem.link) ? menuItem.link : [menuItem.link]).toString();
                return item.link === menuItemLink;
            })
        }).filter((/**
         * @param {?} item
         * @return {?}
         */
        function (item) { return isDefined(item); }))[0];
    };
    FavNavsQuery.decorators = [
        { type: Injectable, args: [{ providedIn: 'root' },] }
    ];
    /** @nocollapse */
    FavNavsQuery.ctorParameters = function () { return [
        { type: FavNavsStore },
        { type: Router },
        { type: Location }
    ]; };
    /** @nocollapse */ FavNavsQuery.ngInjectableDef = i0.defineInjectable({ factory: function FavNavsQuery_Factory() { return new FavNavsQuery(i0.inject(i1.FavNavsStore), i0.inject(i2.Router), i0.inject(i3.Location)); }, token: FavNavsQuery, providedIn: "root" });
    return FavNavsQuery;
}(QueryEntity));
export { FavNavsQuery };
if (false) {
    /** @type {?} */
    FavNavsQuery.prototype.favorites$;
    /**
     * @type {?}
     * @protected
     */
    FavNavsQuery.prototype.store;
    /**
     * @type {?}
     * @protected
     */
    FavNavsQuery.prototype.router;
    /**
     * @type {?}
     * @protected
     */
    FavNavsQuery.prototype.ngLocation;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmF2bmF2cy5xdWVyeS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvbGF5b3V0L25hdi9mYXZuYXYvc3RhdGUvZmF2bmF2cy5xdWVyeS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFNLFdBQVcsRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFTLE1BQU0saUJBQWlCLENBQUM7QUFDL0UsT0FBTyxFQUFFLFlBQVksRUFBZ0IsTUFBTSxpQkFBaUIsQ0FBQztBQUU3RCxPQUFPLEVBQUUsTUFBTSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDekMsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRTNDLE9BQU8sRUFBRSxHQUFHLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQzs7Ozs7QUFFckM7SUFDa0Msd0NBQW1DO0lBR25FLHNCQUNZLEtBQW1CLEVBQ25CLE1BQWMsRUFDZCxVQUFvQjtRQUhoQyxZQUlFLGtCQUFNLEtBQUssQ0FBQyxTQUNiO1FBSlcsV0FBSyxHQUFMLEtBQUssQ0FBYztRQUNuQixZQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsZ0JBQVUsR0FBVixVQUFVLENBQVU7UUFMaEMsZ0JBQVUsR0FBRyxLQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7O0lBTzlCLENBQUM7Ozs7O0lBRUQsa0NBQVc7Ozs7SUFBWCxVQUFZLFFBQWtCO1FBQTlCLGlCQU9DO1FBTkMsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO1lBQ3BCLFFBQVE7Ozs7WUFBRSxVQUFBLElBQUk7O29CQUNOLFlBQVksR0FBRyxLQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxRQUFRLEVBQUU7Z0JBQ3pILE9BQU8sSUFBSSxDQUFDLElBQUksS0FBSyxZQUFZLENBQUM7WUFDcEMsQ0FBQyxDQUFBO1NBQ0YsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHOzs7O1FBQUMsVUFBQSxLQUFLLElBQUksT0FBQSxTQUFTLENBQUMsS0FBSyxDQUFDLEVBQWhCLENBQWdCLEVBQUMsQ0FBQyxDQUFDO0lBQzFDLENBQUM7Ozs7O0lBRUQscUNBQWM7Ozs7SUFBZCxVQUFlLFFBQWtCO1FBQWpDLGlCQU9DO1FBTkMsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDO1lBQ2pCLFFBQVE7Ozs7WUFBRSxVQUFBLElBQUk7O29CQUNOLFlBQVksR0FBRyxLQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxRQUFRLEVBQUU7Z0JBQ3pILE9BQU8sSUFBSSxDQUFDLElBQUksS0FBSyxZQUFZLENBQUM7WUFDcEMsQ0FBQyxDQUFBO1NBQ0YsQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7SUFDaEIsQ0FBQzs7Ozs7SUFFRCxtQ0FBWTs7OztJQUFaLFVBQWEsUUFBa0I7UUFBL0IsaUJBT0M7UUFOQyxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUM7WUFDakIsUUFBUTs7OztZQUFFLFVBQUEsSUFBSTs7b0JBQ04sWUFBWSxHQUFHLEtBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLFFBQVEsRUFBRTtnQkFDekgsT0FBTyxJQUFJLENBQUMsSUFBSSxLQUFLLFlBQVksQ0FBQztZQUNwQyxDQUFDLENBQUE7U0FDRixDQUFDLENBQUMsTUFBTTs7OztRQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFmLENBQWUsRUFBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3hDLENBQUM7O2dCQXBDRixVQUFVLFNBQUMsRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFOzs7O2dCQVB6QixZQUFZO2dCQUVaLE1BQU07Z0JBQ04sUUFBUTs7O3VCQUxqQjtDQThDQyxBQXJDRCxDQUNrQyxXQUFXLEdBb0M1QztTQXBDWSxZQUFZOzs7SUFDdkIsa0NBQThCOzs7OztJQUc1Qiw2QkFBNkI7Ozs7O0lBQzdCLDhCQUF3Qjs7Ozs7SUFDeEIsa0NBQThCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSUQsIFF1ZXJ5RW50aXR5LCBpc0RlZmluZWQsIHRvQm9vbGVhbiwgaXNOaWwgfSBmcm9tICdAZGF0b3JhbWEvYWtpdGEnO1xuaW1wb3J0IHsgRmF2TmF2c1N0b3JlLCBGYXZOYXZzU3RhdGUgfSBmcm9tICcuL2Zhdm5hdnMuc3RvcmUnO1xuaW1wb3J0IHsgTWVudUl0ZW0gfSBmcm9tICcuLi8uLi9tZW51L21lbnUtaXRlbS9tZW51LWl0ZW0ubW9kZWwnO1xuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IExvY2F0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IG1hcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcblxuQEluamVjdGFibGUoeyBwcm92aWRlZEluOiAncm9vdCcgfSlcbmV4cG9ydCBjbGFzcyBGYXZOYXZzUXVlcnkgZXh0ZW5kcyBRdWVyeUVudGl0eTxGYXZOYXZzU3RhdGUsIE1lbnVJdGVtPiB7XG4gIGZhdm9yaXRlcyQgPSB0aGlzLnNlbGVjdEFsbCgpO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByb3RlY3RlZCBzdG9yZTogRmF2TmF2c1N0b3JlLFxuICAgIHByb3RlY3RlZCByb3V0ZXI6IFJvdXRlcixcbiAgICBwcm90ZWN0ZWQgbmdMb2NhdGlvbjogTG9jYXRpb24pIHtcbiAgICBzdXBlcihzdG9yZSk7XG4gIH1cblxuICBpc0Zhdm9yaXRlZChtZW51SXRlbTogTWVudUl0ZW0pOiBPYnNlcnZhYmxlPGJvb2xlYW4+IHtcbiAgICByZXR1cm4gdGhpcy5zZWxlY3RBbGwoe1xuICAgICAgZmlsdGVyQnk6IGl0ZW0gPT4ge1xuICAgICAgICBjb25zdCBtZW51SXRlbUxpbmsgPSB0aGlzLnJvdXRlci5jcmVhdGVVcmxUcmVlKEFycmF5LmlzQXJyYXkobWVudUl0ZW0ubGluaykgPyBtZW51SXRlbS5saW5rIDogW21lbnVJdGVtLmxpbmtdKS50b1N0cmluZygpO1xuICAgICAgICByZXR1cm4gaXRlbS5saW5rID09PSBtZW51SXRlbUxpbms7XG4gICAgICB9XG4gICAgfSkucGlwZShtYXAodmFsdWUgPT4gdG9Cb29sZWFuKHZhbHVlKSkpO1xuICB9XG5cbiAgZ2V0SXNGYXZvcml0ZWQobWVudUl0ZW06IE1lbnVJdGVtKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0QWxsKHtcbiAgICAgIGZpbHRlckJ5OiBpdGVtID0+IHtcbiAgICAgICAgY29uc3QgbWVudUl0ZW1MaW5rID0gdGhpcy5yb3V0ZXIuY3JlYXRlVXJsVHJlZShBcnJheS5pc0FycmF5KG1lbnVJdGVtLmxpbmspID8gbWVudUl0ZW0ubGluayA6IFttZW51SXRlbS5saW5rXSkudG9TdHJpbmcoKTtcbiAgICAgICAgcmV0dXJuIGl0ZW0ubGluayA9PT0gbWVudUl0ZW1MaW5rO1xuICAgICAgfVxuICAgIH0pLmxlbmd0aCA+IDA7XG4gIH1cblxuICBnZXRGYXZvcml0ZWQobWVudUl0ZW06IE1lbnVJdGVtKTogTWVudUl0ZW0ge1xuICAgIHJldHVybiB0aGlzLmdldEFsbCh7XG4gICAgICBmaWx0ZXJCeTogaXRlbSA9PiB7XG4gICAgICAgIGNvbnN0IG1lbnVJdGVtTGluayA9IHRoaXMucm91dGVyLmNyZWF0ZVVybFRyZWUoQXJyYXkuaXNBcnJheShtZW51SXRlbS5saW5rKSA/IG1lbnVJdGVtLmxpbmsgOiBbbWVudUl0ZW0ubGlua10pLnRvU3RyaW5nKCk7XG4gICAgICAgIHJldHVybiBpdGVtLmxpbmsgPT09IG1lbnVJdGVtTGluaztcbiAgICAgIH1cbiAgICB9KS5maWx0ZXIoaXRlbSA9PiBpc0RlZmluZWQoaXRlbSkpWzBdO1xuICB9XG59XG4iXX0=