/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, ChangeDetectionStrategy } from '@angular/core';
import { NavQuery } from '../state/nav.query';
import { FavNavsQuery } from './state/favnavs.query';
import { FavNavsService } from './state/favnavs.service';
var FavnavComponent = /** @class */ (function () {
    // favorites = [
    //   'fas fa-info-circle',
    //   'fas fa-file-contract',
    //   'fas fa-hard-hat',
    //   'fas fa-plug',
    //   'fas fa-moon',
    //   'fas fa-sms',
    //   'fas fa-comments',
    //   'fas fa-ethernet'
    // ];
    function FavnavComponent(navQuery, favNavsQuery, favNavsService) {
        this.navQuery = navQuery;
        this.favNavsQuery = favNavsQuery;
        this.favNavsService = favNavsService;
        this.leftNav$ = this.navQuery.leftnav$;
        this.favorites$ = this.favNavsQuery.favorites$;
        this.favorites = [
        // { title: 'aaa', icon: 'fas fa-info-circle' },
        // { title: 'bbb', icon: 'fas fa-info-circle' },
        // { title: 'ccc', icon: 'fas fa-info-circle' },
        // { title: 'ddd', icon: 'fas fa-info-circle' },
        ];
    }
    /**
     * @return {?}
     */
    FavnavComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.favoritesSubscription = this.favorites$.subscribe((/**
         * @param {?} favs
         * @return {?}
         */
        function (favs) {
            var _a;
            /** @type {?} */
            var newArrary = favs.slice();
            _this.favorites.length = 0;
            (_a = _this.favorites).push.apply(_a, tslib_1.__spread(newArrary));
        }));
    };
    /**
     * @return {?}
     */
    FavnavComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        if (this.favoritesSubscription && !this.favoritesSubscription.closed) {
            this.favoritesSubscription.unsubscribe();
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    FavnavComponent.prototype.drop = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.favNavsService.moverItens(event.previousIndex, event.currentIndex);
    };
    FavnavComponent.decorators = [
        { type: Component, args: [{
                    selector: 'uikit-favnav',
                    template: "<!-- favnav -->\n<div class=\"favnav\" [class.is-closed]=\"(favorites$ | async).length === 0\" [class.is-active]=\"(leftNav$ | async).opened\">\n  <div cdkDropList (cdkDropListDropped)=\"drop($event)\">\n    <a [routerLinkActive]=\"'active'\" [routerLink]=\"favorite.link\" [routerLinkActiveOptions]=\"{exact:true}\" mat-icon-button\n      *ngFor=\"let favorite of (favorites$ | async)\" cdkDrag matTooltip=\"Ir para '{{ favorite.title }}'\"\n      attr.aria-label=\"Ir para '{{ favorite.title }}'\" [matTooltipPosition]=\"'right'\">\n\n      <div class=\"favnav-placeholder\" *cdkDragPlaceholder></div>\n      <mat-icon class=\"fa-lg {{ favorite.icon }}\"></mat-icon>\n    </a>\n  </div>\n</div>\n",
                    changeDetection: ChangeDetectionStrategy.OnPush,
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    FavnavComponent.ctorParameters = function () { return [
        { type: NavQuery },
        { type: FavNavsQuery },
        { type: FavNavsService }
    ]; };
    return FavnavComponent;
}());
export { FavnavComponent };
if (false) {
    /** @type {?} */
    FavnavComponent.prototype.leftNav$;
    /** @type {?} */
    FavnavComponent.prototype.favorites$;
    /** @type {?} */
    FavnavComponent.prototype.favorites;
    /** @type {?} */
    FavnavComponent.prototype.favoritesSubscription;
    /**
     * @type {?}
     * @protected
     */
    FavnavComponent.prototype.navQuery;
    /**
     * @type {?}
     * @protected
     */
    FavnavComponent.prototype.favNavsQuery;
    /**
     * @type {?}
     * @protected
     */
    FavnavComponent.prototype.favNavsService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmF2bmF2LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvbGF5b3V0L25hdi9mYXZuYXYvZmF2bmF2LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsdUJBQXVCLEVBQWEsTUFBTSxlQUFlLENBQUM7QUFFdEYsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBQzlDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUVyRCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFHekQ7SUFvQkUsZ0JBQWdCO0lBQ2hCLDBCQUEwQjtJQUMxQiw0QkFBNEI7SUFDNUIsdUJBQXVCO0lBQ3ZCLG1CQUFtQjtJQUNuQixtQkFBbUI7SUFDbkIsa0JBQWtCO0lBQ2xCLHVCQUF1QjtJQUN2QixzQkFBc0I7SUFDdEIsS0FBSztJQUVMLHlCQUNZLFFBQWtCLEVBQ2xCLFlBQTBCLEVBQzFCLGNBQThCO1FBRjlCLGFBQVEsR0FBUixRQUFRLENBQVU7UUFDbEIsaUJBQVksR0FBWixZQUFZLENBQWM7UUFDMUIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBekJuQyxhQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUM7UUFFekMsZUFBVSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDO1FBQzFDLGNBQVMsR0FBZTtRQUN0QixnREFBZ0Q7UUFDaEQsZ0RBQWdEO1FBQ2hELGdEQUFnRDtRQUNoRCxnREFBZ0Q7U0FDakQsQ0FBQztJQWtCRSxDQUFDOzs7O0lBRUwsa0NBQVE7OztJQUFSO1FBQUEsaUJBTUM7UUFMQyxJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTOzs7O1FBQUMsVUFBQyxJQUFJOzs7Z0JBQ3BELFNBQVMsR0FBRyxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQzlCLEtBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztZQUMxQixDQUFBLEtBQUEsS0FBSSxDQUFDLFNBQVMsQ0FBQSxDQUFDLElBQUksNEJBQUksU0FBUyxHQUFFO1FBQ3BDLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7OztJQUVELHFDQUFXOzs7SUFBWDtRQUNFLElBQUksSUFBSSxDQUFDLHFCQUFxQixJQUFJLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLE1BQU0sRUFBRTtZQUNwRSxJQUFJLENBQUMscUJBQXFCLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDMUM7SUFDSCxDQUFDOzs7OztJQUVELDhCQUFJOzs7O0lBQUosVUFBSyxLQUE4QjtRQUNqQyxJQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsYUFBYSxFQUFFLEtBQUssQ0FBQyxZQUFZLENBQUMsQ0FBQztJQUMxRSxDQUFDOztnQkFyREYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxjQUFjO29CQUN4Qixzc0JBQXNDO29CQUV0QyxlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTs7aUJBQ2hEOzs7O2dCQVhRLFFBQVE7Z0JBQ1IsWUFBWTtnQkFFWixjQUFjOztJQXlEdkIsc0JBQUM7Q0FBQSxBQXRERCxJQXNEQztTQWhEWSxlQUFlOzs7SUFHMUIsbUNBQXlDOztJQUV6QyxxQ0FBMEM7O0lBQzFDLG9DQUtFOztJQUVGLGdEQUFvQzs7Ozs7SUFhbEMsbUNBQTRCOzs7OztJQUM1Qix1Q0FBb0M7Ozs7O0lBQ3BDLHlDQUF3QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneSwgT25EZXN0cm95IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDZGtEcmFnRHJvcCB9IGZyb20gJ0Bhbmd1bGFyL2Nkay9kcmFnLWRyb3AnO1xuaW1wb3J0IHsgTmF2UXVlcnkgfSBmcm9tICcuLi9zdGF0ZS9uYXYucXVlcnknO1xuaW1wb3J0IHsgRmF2TmF2c1F1ZXJ5IH0gZnJvbSAnLi9zdGF0ZS9mYXZuYXZzLnF1ZXJ5JztcbmltcG9ydCB7IFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgRmF2TmF2c1NlcnZpY2UgfSBmcm9tICcuL3N0YXRlL2Zhdm5hdnMuc2VydmljZSc7XG5pbXBvcnQgeyBNZW51SXRlbSB9IGZyb20gJy4uL21lbnUvbWVudS1pdGVtL21lbnUtaXRlbS5tb2RlbCc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ3Vpa2l0LWZhdm5hdicsXG4gIHRlbXBsYXRlVXJsOiAnLi9mYXZuYXYuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9mYXZuYXYuY29tcG9uZW50LnNjc3MnXSxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2hcbn0pXG5leHBvcnQgY2xhc3MgRmF2bmF2Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xuXG5cbiAgcHVibGljIGxlZnROYXYkID0gdGhpcy5uYXZRdWVyeS5sZWZ0bmF2JDtcblxuICBmYXZvcml0ZXMkID0gdGhpcy5mYXZOYXZzUXVlcnkuZmF2b3JpdGVzJDtcbiAgZmF2b3JpdGVzOiBNZW51SXRlbVtdID0gW1xuICAgIC8vIHsgdGl0bGU6ICdhYWEnLCBpY29uOiAnZmFzIGZhLWluZm8tY2lyY2xlJyB9LFxuICAgIC8vIHsgdGl0bGU6ICdiYmInLCBpY29uOiAnZmFzIGZhLWluZm8tY2lyY2xlJyB9LFxuICAgIC8vIHsgdGl0bGU6ICdjY2MnLCBpY29uOiAnZmFzIGZhLWluZm8tY2lyY2xlJyB9LFxuICAgIC8vIHsgdGl0bGU6ICdkZGQnLCBpY29uOiAnZmFzIGZhLWluZm8tY2lyY2xlJyB9LFxuICBdO1xuXG4gIGZhdm9yaXRlc1N1YnNjcmlwdGlvbjogU3Vic2NyaXB0aW9uO1xuICAvLyBmYXZvcml0ZXMgPSBbXG4gIC8vICAgJ2ZhcyBmYS1pbmZvLWNpcmNsZScsXG4gIC8vICAgJ2ZhcyBmYS1maWxlLWNvbnRyYWN0JyxcbiAgLy8gICAnZmFzIGZhLWhhcmQtaGF0JyxcbiAgLy8gICAnZmFzIGZhLXBsdWcnLFxuICAvLyAgICdmYXMgZmEtbW9vbicsXG4gIC8vICAgJ2ZhcyBmYS1zbXMnLFxuICAvLyAgICdmYXMgZmEtY29tbWVudHMnLFxuICAvLyAgICdmYXMgZmEtZXRoZXJuZXQnXG4gIC8vIF07XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJvdGVjdGVkIG5hdlF1ZXJ5OiBOYXZRdWVyeSxcbiAgICBwcm90ZWN0ZWQgZmF2TmF2c1F1ZXJ5OiBGYXZOYXZzUXVlcnksXG4gICAgcHJvdGVjdGVkIGZhdk5hdnNTZXJ2aWNlOiBGYXZOYXZzU2VydmljZVxuICApIHsgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIHRoaXMuZmF2b3JpdGVzU3Vic2NyaXB0aW9uID0gdGhpcy5mYXZvcml0ZXMkLnN1YnNjcmliZSgoZmF2cykgPT4ge1xuICAgICAgY29uc3QgbmV3QXJyYXJ5ID0gZmF2cy5zbGljZSgpO1xuICAgICAgdGhpcy5mYXZvcml0ZXMubGVuZ3RoID0gMDtcbiAgICAgIHRoaXMuZmF2b3JpdGVzLnB1c2goLi4ubmV3QXJyYXJ5KTtcbiAgICB9KTtcbiAgfVxuXG4gIG5nT25EZXN0cm95KCk6IHZvaWQge1xuICAgIGlmICh0aGlzLmZhdm9yaXRlc1N1YnNjcmlwdGlvbiAmJiAhdGhpcy5mYXZvcml0ZXNTdWJzY3JpcHRpb24uY2xvc2VkKSB7XG4gICAgICB0aGlzLmZhdm9yaXRlc1N1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xuICAgIH1cbiAgfVxuXG4gIGRyb3AoZXZlbnQ6IENka0RyYWdEcm9wPE1lbnVJdGVtW10+KSB7XG4gICAgdGhpcy5mYXZOYXZzU2VydmljZS5tb3Zlckl0ZW5zKGV2ZW50LnByZXZpb3VzSW5kZXgsIGV2ZW50LmN1cnJlbnRJbmRleCk7XG4gIH1cbn1cbiJdfQ==