/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { LayoutService } from '../../layout.service';
import * as Fuse from 'fuse.js';
import { map } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "../../layout.service";
var MenuSearchService = /** @class */ (function () {
    function MenuSearchService(layoutService) {
        this.layoutService = layoutService;
        this.options = {
            keys: ['title', 'tags',
                'children.title', 'children.tags',
                'children.children.title', 'children.children.title.tags',
                'children.children.children.title', 'children.children.children.title.tags']
        };
        this.menuItemsSubject$ = new BehaviorSubject([]);
        this.menuItems$ = this.menuItemsSubject$.asObservable();
    }
    /**
     * @param {?} value
     * @return {?}
     */
    MenuSearchService.prototype.buscar = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        var _this = this;
        if (!value || value === '') {
            this.layoutService.menuItems$.subscribe((/**
             * @param {?} itens
             * @return {?}
             */
            function (itens) { return _this.menuItemsSubject$.next(itens); }));
        }
        else if (this.menuItems$) {
            /** @type {?} */
            var menuItens_1 = [];
            this.menuItems$.pipe(map((/**
             * @param {?} listMenuItem
             * @return {?}
             */
            function (listMenuItem) { return _this.applyFilter(listMenuItem, value, _this.options); })))
                .subscribe((/**
             * @param {?} itens
             * @return {?}
             */
            function (itens) { return menuItens_1 = _this.groupReturnFilter(itens, value); }));
            this.menuItemsSubject$.next(menuItens_1);
        }
        else {
            this.layoutService.menuItems$.pipe(map((/**
             * @param {?} listMenuItem
             * @return {?}
             */
            function (listMenuItem) { return _this.applyFilter(listMenuItem, value, _this.options); })))
                .subscribe((/**
             * @param {?} itens
             * @return {?}
             */
            function (itens) { return _this.menuItemsSubject$.next(_this.groupReturnFilter(itens, value)); }));
        }
    };
    /**
     * @private
     * @param {?} listMenuItem
     * @param {?} value
     * @param {?} options
     * @return {?}
     */
    MenuSearchService.prototype.applyFilter = /**
     * @private
     * @param {?} listMenuItem
     * @param {?} value
     * @param {?} options
     * @return {?}
     */
    function (listMenuItem, value, options) {
        /** @type {?} */
        var fuse = new Fuse(listMenuItem, options);
        return fuse.search(value);
    };
    /**
     * @private
     * @param {?} itens
     * @param {?} value
     * @return {?}
     */
    MenuSearchService.prototype.groupReturnFilter = /**
     * @private
     * @param {?} itens
     * @param {?} value
     * @return {?}
     */
    function (itens, value) {
        var _this = this;
        /** @type {?} */
        var menuItens = [];
        /** @type {?} */
        var functionCreateItem = (/**
         * @param {?} menuItem
         * @return {?}
         */
        function (menuItem) {
            return (/** @type {?} */ ({
                title: menuItem.title,
                icon: menuItem.icon,
                link: menuItem.link,
                tags: menuItem.tags,
                nodeId: menuItem.nodeId,
            }));
        });
        /** @type {?} */
        var functionMapItem = (/**
         * @param {?} menuItem
         * @return {?}
         */
        function (menuItem) {
            if (menuItem.children && menuItem.children.length > 0) {
                _this.applyFilter(menuItem.children, value, _this.options).map((/**
                 * @param {?} itemIt
                 * @return {?}
                 */
                function (itemIt) { return functionMapItem(itemIt); }));
            }
            else {
                menuItens.push(functionCreateItem(menuItem));
            }
        });
        itens.map((/**
         * @param {?} menuItem
         * @return {?}
         */
        function (menuItem) { return functionMapItem(menuItem); }));
        return menuItens;
    };
    MenuSearchService.decorators = [
        { type: Injectable, args: [{ providedIn: 'root' },] }
    ];
    /** @nocollapse */
    MenuSearchService.ctorParameters = function () { return [
        { type: LayoutService }
    ]; };
    /** @nocollapse */ MenuSearchService.ngInjectableDef = i0.defineInjectable({ factory: function MenuSearchService_Factory() { return new MenuSearchService(i0.inject(i1.LayoutService)); }, token: MenuSearchService, providedIn: "root" });
    return MenuSearchService;
}());
export { MenuSearchService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    MenuSearchService.prototype.options;
    /**
     * @type {?}
     * @private
     */
    MenuSearchService.prototype.menuItemsSubject$;
    /** @type {?} */
    MenuSearchService.prototype.menuItems$;
    /**
     * @type {?}
     * @protected
     */
    MenuSearchService.prototype.layoutService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVudS1zZWFyY2guc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvbGF5b3V0L25hdi9tZW51LXNlYXJjaC9tZW51LXNlYXJjaC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsVUFBVSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBQyxlQUFlLEVBQUMsTUFBTSxNQUFNLENBQUM7QUFFckMsT0FBTyxFQUFDLGFBQWEsRUFBQyxNQUFNLHNCQUFzQixDQUFDO0FBQ25ELE9BQU8sS0FBSyxJQUFJLE1BQU0sU0FBUyxDQUFDO0FBQ2hDLE9BQU8sRUFBQyxHQUFHLEVBQUMsTUFBTSxnQkFBZ0IsQ0FBQzs7O0FBRW5DO0lBV0UsMkJBQXNCLGFBQTRCO1FBQTVCLGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBVDFDLFlBQU8sR0FBRztZQUNoQixJQUFJLEVBQUUsQ0FBQyxPQUFPLEVBQUUsTUFBTTtnQkFDcEIsZ0JBQWdCLEVBQUUsZUFBZTtnQkFDakMseUJBQXlCLEVBQUUsOEJBQThCO2dCQUN6RCxrQ0FBa0MsRUFBRSx1Q0FBdUMsQ0FBQztTQUMvRSxDQUFDO1FBQ00sc0JBQWlCLEdBQUcsSUFBSSxlQUFlLENBQWEsRUFBRSxDQUFDLENBQUM7UUFDekQsZUFBVSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUcxRCxDQUFDOzs7OztJQUVNLGtDQUFNOzs7O0lBQWIsVUFBYyxLQUFhO1FBQTNCLGlCQWdCQztRQWRDLElBQUksQ0FBQyxLQUFLLElBQUksS0FBSyxLQUFLLEVBQUUsRUFBRTtZQUMxQixJQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxTQUFTOzs7O1lBQUMsVUFBQyxLQUFLLElBQUssT0FBQSxLQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFsQyxDQUFrQyxFQUFDLENBQUM7U0FDeEY7YUFBTSxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7O2dCQUV0QixXQUFTLEdBQWUsRUFBRTtZQUM5QixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHOzs7O1lBQUMsVUFBQSxZQUFZLElBQUksT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksRUFBRSxLQUFLLEVBQUUsS0FBSSxDQUFDLE9BQU8sQ0FBQyxFQUFuRCxDQUFtRCxFQUFDLENBQUM7aUJBQzNGLFNBQVM7Ozs7WUFBQyxVQUFDLEtBQUssSUFBSyxPQUFBLFdBQVMsR0FBRyxLQUFJLENBQUMsaUJBQWlCLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxFQUFoRCxDQUFnRCxFQUFDLENBQUM7WUFDMUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxXQUFTLENBQUMsQ0FBQztTQUV4QzthQUFNO1lBRUwsSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEdBQUc7Ozs7WUFBQyxVQUFBLFlBQVksSUFBSSxPQUFBLEtBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxFQUFFLEtBQUssRUFBRSxLQUFJLENBQUMsT0FBTyxDQUFDLEVBQW5ELENBQW1ELEVBQUMsQ0FBQztpQkFDekcsU0FBUzs7OztZQUFDLFVBQUMsS0FBSyxJQUFLLE9BQUEsS0FBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsaUJBQWlCLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFDLEVBQWpFLENBQWlFLEVBQUMsQ0FBQztTQUM1RjtJQUNILENBQUM7Ozs7Ozs7O0lBRU8sdUNBQVc7Ozs7Ozs7SUFBbkIsVUFBb0IsWUFBd0IsRUFBRSxLQUFhLEVBQUUsT0FBMkI7O1lBQ2hGLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUUsT0FBTyxDQUFDO1FBQzVDLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUM1QixDQUFDOzs7Ozs7O0lBRU8sNkNBQWlCOzs7Ozs7SUFBekIsVUFBMEIsS0FBaUIsRUFBRSxLQUFhO1FBQTFELGlCQXNCQzs7WUFyQk8sU0FBUyxHQUFlLEVBQUU7O1lBQzFCLGtCQUFrQjs7OztRQUFHLFVBQUMsUUFBa0I7WUFDNUMsT0FBTyxtQkFBQTtnQkFDTCxLQUFLLEVBQUUsUUFBUSxDQUFDLEtBQUs7Z0JBQ3JCLElBQUksRUFBRSxRQUFRLENBQUMsSUFBSTtnQkFDbkIsSUFBSSxFQUFFLFFBQVEsQ0FBQyxJQUFJO2dCQUNuQixJQUFJLEVBQUUsUUFBUSxDQUFDLElBQUk7Z0JBQ25CLE1BQU0sRUFBRSxRQUFRLENBQUMsTUFBTTthQUN4QixFQUFZLENBQUM7UUFDaEIsQ0FBQyxDQUFBOztZQUVLLGVBQWU7Ozs7UUFBRyxVQUFDLFFBQWtCO1lBQ3pDLElBQUksUUFBUSxDQUFDLFFBQVEsSUFBSSxRQUFRLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ3JELEtBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRSxLQUFLLEVBQUUsS0FBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEdBQUc7Ozs7Z0JBQUMsVUFBQSxNQUFNLElBQUksT0FBQSxlQUFlLENBQUMsTUFBTSxDQUFDLEVBQXZCLENBQXVCLEVBQUMsQ0FBQzthQUNqRztpQkFBTTtnQkFDTCxTQUFTLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7YUFDOUM7UUFDSCxDQUFDLENBQUE7UUFFRCxLQUFLLENBQUMsR0FBRzs7OztRQUFDLFVBQUMsUUFBa0IsSUFBSyxPQUFBLGVBQWUsQ0FBQyxRQUFRLENBQUMsRUFBekIsQ0FBeUIsRUFBQyxDQUFDO1FBQzdELE9BQU8sU0FBUyxDQUFDO0lBQ25CLENBQUM7O2dCQTNERixVQUFVLFNBQUMsRUFBQyxVQUFVLEVBQUUsTUFBTSxFQUFDOzs7O2dCQUp4QixhQUFhOzs7NEJBSHJCO0NBbUVDLEFBNURELElBNERDO1NBM0RZLGlCQUFpQjs7Ozs7O0lBQzVCLG9DQUtFOzs7OztJQUNGLDhDQUFnRTs7SUFDaEUsdUNBQTBEOzs7OztJQUU5QywwQ0FBc0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0luamVjdGFibGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtCZWhhdmlvclN1YmplY3R9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHtNZW51SXRlbX0gZnJvbSAnLi4vbWVudS9tZW51LWl0ZW0vbWVudS1pdGVtLm1vZGVsJztcbmltcG9ydCB7TGF5b3V0U2VydmljZX0gZnJvbSAnLi4vLi4vbGF5b3V0LnNlcnZpY2UnO1xuaW1wb3J0ICogYXMgRnVzZSBmcm9tICdmdXNlLmpzJztcbmltcG9ydCB7bWFwfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5cbkBJbmplY3RhYmxlKHtwcm92aWRlZEluOiAncm9vdCd9KVxuZXhwb3J0IGNsYXNzIE1lbnVTZWFyY2hTZXJ2aWNlIHtcbiAgcHJpdmF0ZSBvcHRpb25zID0ge1xuICAgIGtleXM6IFsndGl0bGUnLCAndGFncycsXG4gICAgICAnY2hpbGRyZW4udGl0bGUnLCAnY2hpbGRyZW4udGFncycsXG4gICAgICAnY2hpbGRyZW4uY2hpbGRyZW4udGl0bGUnLCAnY2hpbGRyZW4uY2hpbGRyZW4udGl0bGUudGFncycsXG4gICAgICAnY2hpbGRyZW4uY2hpbGRyZW4uY2hpbGRyZW4udGl0bGUnLCAnY2hpbGRyZW4uY2hpbGRyZW4uY2hpbGRyZW4udGl0bGUudGFncyddXG4gIH07XG4gIHByaXZhdGUgbWVudUl0ZW1zU3ViamVjdCQgPSBuZXcgQmVoYXZpb3JTdWJqZWN0PE1lbnVJdGVtW10+KFtdKTtcbiAgcHVibGljIG1lbnVJdGVtcyQgPSB0aGlzLm1lbnVJdGVtc1N1YmplY3QkLmFzT2JzZXJ2YWJsZSgpO1xuXG4gIGNvbnN0cnVjdG9yKHByb3RlY3RlZCBsYXlvdXRTZXJ2aWNlOiBMYXlvdXRTZXJ2aWNlKSB7XG4gIH1cblxuICBwdWJsaWMgYnVzY2FyKHZhbHVlOiBzdHJpbmcpIHtcblxuICAgIGlmICghdmFsdWUgfHwgdmFsdWUgPT09ICcnKSB7XG4gICAgICB0aGlzLmxheW91dFNlcnZpY2UubWVudUl0ZW1zJC5zdWJzY3JpYmUoKGl0ZW5zKSA9PiB0aGlzLm1lbnVJdGVtc1N1YmplY3QkLm5leHQoaXRlbnMpKTtcbiAgICB9IGVsc2UgaWYgKHRoaXMubWVudUl0ZW1zJCkge1xuXG4gICAgICBsZXQgbWVudUl0ZW5zOiBNZW51SXRlbVtdID0gW107XG4gICAgICB0aGlzLm1lbnVJdGVtcyQucGlwZShtYXAobGlzdE1lbnVJdGVtID0+IHRoaXMuYXBwbHlGaWx0ZXIobGlzdE1lbnVJdGVtLCB2YWx1ZSwgdGhpcy5vcHRpb25zKSkpXG4gICAgICAgIC5zdWJzY3JpYmUoKGl0ZW5zKSA9PiBtZW51SXRlbnMgPSB0aGlzLmdyb3VwUmV0dXJuRmlsdGVyKGl0ZW5zLCB2YWx1ZSkpO1xuICAgICAgdGhpcy5tZW51SXRlbXNTdWJqZWN0JC5uZXh0KG1lbnVJdGVucyk7XG5cbiAgICB9IGVsc2Uge1xuXG4gICAgICB0aGlzLmxheW91dFNlcnZpY2UubWVudUl0ZW1zJC5waXBlKG1hcChsaXN0TWVudUl0ZW0gPT4gdGhpcy5hcHBseUZpbHRlcihsaXN0TWVudUl0ZW0sIHZhbHVlLCB0aGlzLm9wdGlvbnMpKSlcbiAgICAgICAgLnN1YnNjcmliZSgoaXRlbnMpID0+IHRoaXMubWVudUl0ZW1zU3ViamVjdCQubmV4dCh0aGlzLmdyb3VwUmV0dXJuRmlsdGVyKGl0ZW5zLCB2YWx1ZSkpKTtcbiAgICB9XG4gIH1cblxuICBwcml2YXRlIGFwcGx5RmlsdGVyKGxpc3RNZW51SXRlbTogTWVudUl0ZW1bXSwgdmFsdWU6IHN0cmluZywgb3B0aW9uczogeyBrZXlzOiBzdHJpbmdbXSB9KSB7XG4gICAgY29uc3QgZnVzZSA9IG5ldyBGdXNlKGxpc3RNZW51SXRlbSwgb3B0aW9ucyk7XG4gICAgcmV0dXJuIGZ1c2Uuc2VhcmNoKHZhbHVlKTtcbiAgfVxuXG4gIHByaXZhdGUgZ3JvdXBSZXR1cm5GaWx0ZXIoaXRlbnM6IE1lbnVJdGVtW10sIHZhbHVlOiBzdHJpbmcpOiBNZW51SXRlbVtdIHtcbiAgICBjb25zdCBtZW51SXRlbnM6IE1lbnVJdGVtW10gPSBbXTtcbiAgICBjb25zdCBmdW5jdGlvbkNyZWF0ZUl0ZW0gPSAobWVudUl0ZW06IE1lbnVJdGVtKSA9PiB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICB0aXRsZTogbWVudUl0ZW0udGl0bGUsXG4gICAgICAgIGljb246IG1lbnVJdGVtLmljb24sXG4gICAgICAgIGxpbms6IG1lbnVJdGVtLmxpbmssXG4gICAgICAgIHRhZ3M6IG1lbnVJdGVtLnRhZ3MsXG4gICAgICAgIG5vZGVJZDogbWVudUl0ZW0ubm9kZUlkLFxuICAgICAgfSBhcyBNZW51SXRlbTtcbiAgICB9O1xuXG4gICAgY29uc3QgZnVuY3Rpb25NYXBJdGVtID0gKG1lbnVJdGVtOiBNZW51SXRlbSkgPT4ge1xuICAgICAgaWYgKG1lbnVJdGVtLmNoaWxkcmVuICYmIG1lbnVJdGVtLmNoaWxkcmVuLmxlbmd0aCA+IDApIHtcbiAgICAgICAgdGhpcy5hcHBseUZpbHRlcihtZW51SXRlbS5jaGlsZHJlbiwgdmFsdWUsIHRoaXMub3B0aW9ucykubWFwKGl0ZW1JdCA9PiBmdW5jdGlvbk1hcEl0ZW0oaXRlbUl0KSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBtZW51SXRlbnMucHVzaChmdW5jdGlvbkNyZWF0ZUl0ZW0obWVudUl0ZW0pKTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgaXRlbnMubWFwKChtZW51SXRlbTogTWVudUl0ZW0pID0+IGZ1bmN0aW9uTWFwSXRlbShtZW51SXRlbSkpO1xuICAgIHJldHVybiBtZW51SXRlbnM7XG4gIH1cbn1cblxuIl19