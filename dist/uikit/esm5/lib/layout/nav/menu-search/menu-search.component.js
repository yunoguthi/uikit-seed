/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MenuSearchService } from './menu-search.service';
import { Hotkey, HotkeysService } from 'angular2-hotkeys';
import { MatInput } from '@angular/material/input';
import { NavService } from '../state/nav.service';
import { DOWN_ARROW, ENTER, ESCAPE, UP_ARROW } from '@angular/cdk/keycodes';
import { HighlightComponent } from '../highlight/highlight.component';
import { ActiveDescendantKeyManager } from '@angular/cdk/a11y';
import { map } from 'rxjs/operators';
import { NestedTreeControl } from '@angular/cdk/tree';
import { Router } from '@angular/router';
var MenuSearchComponent = /** @class */ (function () {
    function MenuSearchComponent(fb, menuSearchService, hotkeysService, navService, router) {
        this.fb = fb;
        this.menuSearchService = menuSearchService;
        this.hotkeysService = hotkeysService;
        this.navService = navService;
        this.router = router;
        this.placeholderText = document.body.getBoundingClientRect().width > 970 ? 'Pesquisa (Ctrl + Alt + 3)' : 'Pesquisa';
        this.eventSearch = new EventEmitter();
        this.treeControl = new NestedTreeControl((/**
         * @param {?} node
         * @return {?}
         */
        function (node) { return node.children; }));
        this.menuItems$ = this.menuSearchService.menuItems$;
        this.stateForm = this.fb.group({
            searchGroups: '',
        });
    }
    /**
     * @return {?}
     */
    MenuSearchComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.menuSearchService.buscar('');
        this.stateForm.get('searchGroups').valueChanges.subscribe((/**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            _this.menuSearchService.buscar(value);
        }));
    };
    /**
     * @return {?}
     */
    MenuSearchComponent.prototype.ngAfterViewInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.hotkeysService.add(new Hotkey('ctrl+alt+3', (/**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            _this.navService.toggleLeftNav();
            _this.searchOpen.focus();
            return false;
        })));
        if (this.itensMenu) {
            this.keyManager = new ActiveDescendantKeyManager(this.itensMenu).withWrap().withTypeAhead();
        }
    };
    /**
     * @param {?} item
     * @return {?}
     */
    MenuSearchComponent.prototype.goToLink = /**
     * @param {?} item
     * @return {?}
     */
    function (item) {
        this.router.navigate([item.link]);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    MenuSearchComponent.prototype.onKeyup = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        switch (event.keyCode) {
            case ENTER:
                this.goToLink(this.keyManager.activeItem.item);
                break;
            case ESCAPE:
                this.menuSearchService.buscar('');
                this.searchOpen.value = '';
                break;
            case UP_ARROW:
            case DOWN_ARROW:
                this.keyManager.onKeydown(event);
                this.activeItem(this.keyManager.activeItem.item, event.keyCode === UP_ARROW);
                this.expandAllNode();
                break;
        }
    };
    /**
     * @private
     * @return {?}
     */
    MenuSearchComponent.prototype.expandAllNode = /**
     * @private
     * @return {?}
     */
    function () {
        var _this = this;
        this.menuItems$.pipe(map((/**
         * @param {?} menuItens
         * @return {?}
         */
        function (menuItens) {
            /** @type {?} */
            var funcItens = (/**
             * @param {?} itens
             * @return {?}
             */
            function (itens) {
                itens.forEach((/**
                 * @param {?} item
                 * @return {?}
                 */
                function (item) {
                    if (item.children) {
                        _this.treeControl.expand(item);
                        funcItens(item.children);
                    }
                }));
            });
            funcItens(menuItens);
        }))).subscribe();
    };
    /**
     * @private
     * @param {?} item
     * @param {?} up
     * @return {?}
     */
    MenuSearchComponent.prototype.activeItem = /**
     * @private
     * @param {?} item
     * @param {?} up
     * @return {?}
     */
    function (item, up) {
        if (item.children && item.children.length > 0) {
            if (up) {
                this.keyManager.setPreviousItemActive();
            }
            else {
                this.keyManager.setNextItemActive();
            }
            this.activeItem(this.keyManager.activeItem.item, up);
        }
    };
    MenuSearchComponent.decorators = [
        { type: Component, args: [{
                    selector: 'uikit-menu-search',
                    template: "<form [formGroup]=\"stateForm\" autocomplete=\"off\">\n  <div search-menu-container>\n    <input class=\"search-menu\" tabindex=\"3\" matInput #searchOpen=\"matInput\" placeholder=\"{{placeholderText}}\" (keyup)=\"onKeyup($event)\"\n           formControlName=\"searchGroups\"/>\n\n    <uikit-menu-item></uikit-menu-item>\n  </div>\n</form>\n",
                    changeDetection: ChangeDetectionStrategy.OnPush,
                    styles: [".search-menu{background-image:url(\"data:image/svg+xml,%3Csvg aria-hidden='true' focusable='false' data-prefix='fas' data-icon='search' role='img' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512' class='svg-inline--fa fa-search fa-w-16 fa-2x' style=' color: %23004bcb; font-size: 16px;%0A'%3E%3Cpath fill='currentColor' d='M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z' class=''%3E%3C/path%3E%3C/svg%3E\");background-repeat:no-repeat;background-position:right;background-size:5%;background-position-x:97%}.icon-back{position:absolute;padding:13px;margin-left:85%}input{color:#004bcb;font-size:13px;padding:15px 15px 15px 18px;width:600px;height:45px;line-height:34px;top:0;margin-top:0;margin-bottom:3px;background:#edf3ff;border-top:1px solid #d9e3f7;border-bottom:1px solid #d9e3f7}input::-webkit-input-placeholder{color:#004bcb}input:-moz-placeholder{color:#004bcb}input::-moz-placeholder{color:#004bcb}input:-ms-input-placeholder{color:#004bcb}i[search-icon]{font-size:15px;color:#004bcb}.mat-list-base{paddint-top:0!important}.dark input{color:#fff!important;background:#555!important;border-bottom:2px solid #fff!important}.dark input::-webkit-input-placeholder{color:#fff!important}.dark input:-moz-placeholder{color:#fff!important}.dark input::-moz-placeholder{color:#fff!important}.dark input:-ms-input-placeholder{color:#fff!important}.dark i.icon-search{color:#fff!important}.dark i.icon-search:hover{background:0 0}.dark[opened] button[clear-button] i{color:#fff!important}.dark[opened] [resultados-container]{background:#555!important;color:#fff!important;border-right:1px solid #333!important;border-left:1px solid #333!important;border-bottom:1px solid #333!important}.dark[opened] [resultados-container] span{color:#fff!important;border:0!important}.dark[opened] input{background:#555!important;color:#fff!important}.dark[opened] input::-webkit-input-placeholder{color:#fff!important}.dark[opened] input:-moz-placeholder{color:#fff!important}.dark[opened] input::-moz-placeholder{color:#fff!important}.dark[opened] input:-ms-input-placeholder{color:#fff!important}.dark[opened] i.icon-search{color:#fff!important}.dark[opened] i.icon-search:hover{background:0 0}.dark[opened] .icon-sobre{margin-left:-40px}"]
                }] }
    ];
    /** @nocollapse */
    MenuSearchComponent.ctorParameters = function () { return [
        { type: FormBuilder },
        { type: MenuSearchService },
        { type: HotkeysService },
        { type: NavService },
        { type: Router }
    ]; };
    MenuSearchComponent.propDecorators = {
        placeholderText: [{ type: Input }],
        eventSearch: [{ type: Output }],
        searchOpen: [{ type: ViewChild, args: ['searchOpen',] }],
        itensMenu: [{ type: ViewChildren, args: [HighlightComponent,] }]
    };
    return MenuSearchComponent;
}());
export { MenuSearchComponent };
if (false) {
    /** @type {?} */
    MenuSearchComponent.prototype.placeholderText;
    /** @type {?} */
    MenuSearchComponent.prototype.eventSearch;
    /** @type {?} */
    MenuSearchComponent.prototype.searchOpen;
    /** @type {?} */
    MenuSearchComponent.prototype.itensMenu;
    /**
     * @type {?}
     * @private
     */
    MenuSearchComponent.prototype.keyManager;
    /** @type {?} */
    MenuSearchComponent.prototype.treeControl;
    /** @type {?} */
    MenuSearchComponent.prototype.menuItems$;
    /** @type {?} */
    MenuSearchComponent.prototype.stateForm;
    /**
     * @type {?}
     * @private
     */
    MenuSearchComponent.prototype.fb;
    /**
     * @type {?}
     * @protected
     */
    MenuSearchComponent.prototype.menuSearchService;
    /** @type {?} */
    MenuSearchComponent.prototype.hotkeysService;
    /**
     * @type {?}
     * @protected
     */
    MenuSearchComponent.prototype.navService;
    /**
     * @type {?}
     * @protected
     */
    MenuSearchComponent.prototype.router;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVudS1zZWFyY2guY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9sYXlvdXQvbmF2L21lbnUtc2VhcmNoL21lbnUtc2VhcmNoLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUVMLHVCQUF1QixFQUN2QixTQUFTLEVBQ1QsWUFBWSxFQUNaLEtBQUssRUFFTCxNQUFNLEVBQUUsU0FBUyxFQUNqQixTQUFTLEVBQUUsWUFBWSxFQUN4QixNQUFNLGVBQWUsQ0FBQztBQUN2QixPQUFPLEVBQUMsV0FBVyxFQUFZLE1BQU0sZ0JBQWdCLENBQUM7QUFDdEQsT0FBTyxFQUFDLGlCQUFpQixFQUFDLE1BQU0sdUJBQXVCLENBQUM7QUFDeEQsT0FBTyxFQUFDLE1BQU0sRUFBRSxjQUFjLEVBQUMsTUFBTSxrQkFBa0IsQ0FBQztBQUN4RCxPQUFPLEVBQUMsUUFBUSxFQUFDLE1BQU0seUJBQXlCLENBQUM7QUFDakQsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLHNCQUFzQixDQUFDO0FBQ2hELE9BQU8sRUFBQyxVQUFVLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUMsTUFBTSx1QkFBdUIsQ0FBQztBQUMxRSxPQUFPLEVBQUMsa0JBQWtCLEVBQUMsTUFBTSxrQ0FBa0MsQ0FBQztBQUNwRSxPQUFPLEVBQUMsMEJBQTBCLEVBQUMsTUFBTSxtQkFBbUIsQ0FBQztBQUM3RCxPQUFPLEVBQUMsR0FBRyxFQUFDLE1BQU0sZ0JBQWdCLENBQUM7QUFFbkMsT0FBTyxFQUFDLGlCQUFpQixFQUFDLE1BQU0sbUJBQW1CLENBQUM7QUFDcEQsT0FBTyxFQUFDLE1BQU0sRUFBQyxNQUFNLGlCQUFpQixDQUFDO0FBRXZDO0lBb0JFLDZCQUNVLEVBQWUsRUFDYixpQkFBb0MsRUFDdkMsY0FBOEIsRUFDM0IsVUFBc0IsRUFDdEIsTUFBYztRQUpoQixPQUFFLEdBQUYsRUFBRSxDQUFhO1FBQ2Isc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQUN2QyxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDM0IsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUN0QixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBbEJqQixvQkFBZSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMscUJBQXFCLEVBQUUsQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQywyQkFBMkIsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDO1FBQzlHLGdCQUFXLEdBQXNCLElBQUksWUFBWSxFQUFFLENBQUM7UUFLdkQsZ0JBQVcsR0FBRyxJQUFJLGlCQUFpQjs7OztRQUFXLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLFFBQVEsRUFBYixDQUFhLEVBQUMsQ0FBQztRQUNyRSxlQUFVLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFVBQVUsQ0FBQztRQUV0RCxjQUFTLEdBQWMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7WUFDbkMsWUFBWSxFQUFFLEVBQUU7U0FDakIsQ0FBQyxDQUFDO0lBU0gsQ0FBQzs7OztJQUVNLHNDQUFROzs7SUFBZjtRQUFBLGlCQUtDO1FBSkMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUNsQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQyxZQUFZLENBQUMsU0FBUzs7OztRQUFDLFVBQUEsS0FBSztZQUM3RCxLQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3ZDLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7OztJQUVNLDZDQUFlOzs7SUFBdEI7UUFBQSxpQkFVQztRQVRDLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLElBQUksTUFBTSxDQUFDLFlBQVk7Ozs7UUFBRSxVQUFDLEtBQW9CO1lBQ3BFLEtBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxFQUFFLENBQUM7WUFDaEMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUN4QixPQUFPLEtBQUssQ0FBQztRQUNmLENBQUMsRUFBQyxDQUFDLENBQUM7UUFFSixJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDbEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLDBCQUEwQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxhQUFhLEVBQUUsQ0FBQztTQUM3RjtJQUNILENBQUM7Ozs7O0lBRU0sc0NBQVE7Ozs7SUFBZixVQUFnQixJQUFjO1FBQzVCLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7SUFDcEMsQ0FBQzs7Ozs7SUFFTSxxQ0FBTzs7OztJQUFkLFVBQWUsS0FBVTtRQUN2QixRQUFRLEtBQUssQ0FBQyxPQUFPLEVBQUU7WUFDckIsS0FBSyxLQUFLO2dCQUNSLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQy9DLE1BQU07WUFDUixLQUFLLE1BQU07Z0JBQ1QsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDbEMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO2dCQUMzQixNQUFNO1lBQ1IsS0FBSyxRQUFRLENBQUM7WUFDZCxLQUFLLFVBQVU7Z0JBQ2IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ2pDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxPQUFPLEtBQUssUUFBUSxDQUFDLENBQUM7Z0JBQzdFLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztnQkFDckIsTUFBTTtTQUNUO0lBQ0gsQ0FBQzs7Ozs7SUFFTywyQ0FBYTs7OztJQUFyQjtRQUFBLGlCQWFDO1FBWkMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsR0FBRzs7OztRQUFDLFVBQUMsU0FBcUI7O2dCQUN2QyxTQUFTOzs7O1lBQUcsVUFBQyxLQUFpQjtnQkFDbEMsS0FBSyxDQUFDLE9BQU87Ozs7Z0JBQUMsVUFBQyxJQUFJO29CQUNqQixJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7d0JBQ2pCLEtBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUM5QixTQUFTLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO3FCQUMxQjtnQkFDSCxDQUFDLEVBQUMsQ0FBQztZQUNMLENBQUMsQ0FBQTtZQUVELFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUN2QixDQUFDLEVBQUMsQ0FBQyxDQUFDLFNBQVMsRUFBRSxDQUFDO0lBQ2xCLENBQUM7Ozs7Ozs7SUFFTyx3Q0FBVTs7Ozs7O0lBQWxCLFVBQW1CLElBQWMsRUFBRSxFQUFXO1FBQzVDLElBQUksSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDN0MsSUFBSSxFQUFFLEVBQUU7Z0JBQ04sSUFBSSxDQUFDLFVBQVUsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO2FBQ3pDO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxVQUFVLENBQUMsaUJBQWlCLEVBQUUsQ0FBQzthQUNyQztZQUNELElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1NBQ3REO0lBQ0gsQ0FBQzs7Z0JBOUZGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsbUJBQW1CO29CQUM3QixrV0FBMkM7b0JBRTNDLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNOztpQkFDaEQ7Ozs7Z0JBbEJPLFdBQVc7Z0JBQ1gsaUJBQWlCO2dCQUNULGNBQWM7Z0JBRXRCLFVBQVU7Z0JBT1YsTUFBTTs7O2tDQVNYLEtBQUs7OEJBQ0wsTUFBTTs2QkFDTixTQUFTLFNBQUMsWUFBWTs0QkFFdEIsWUFBWSxTQUFDLGtCQUFrQjs7SUFvRmxDLDBCQUFDO0NBQUEsQUEvRkQsSUErRkM7U0F6RlksbUJBQW1COzs7SUFDOUIsOENBQXdIOztJQUN4SCwwQ0FBOEQ7O0lBQzlELHlDQUE4Qzs7SUFFOUMsd0NBQTJFOzs7OztJQUMzRSx5Q0FBbUU7O0lBQ25FLDBDQUE0RTs7SUFDNUUseUNBQXNEOztJQUV0RCx3Q0FFRzs7Ozs7SUFHRCxpQ0FBdUI7Ozs7O0lBQ3ZCLGdEQUE4Qzs7SUFDOUMsNkNBQXFDOzs7OztJQUNyQyx5Q0FBZ0M7Ozs7O0lBQ2hDLHFDQUF3QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XG4gIEFmdGVyVmlld0luaXQsXG4gIENoYW5nZURldGVjdGlvblN0cmF0ZWd5LFxuICBDb21wb25lbnQsXG4gIEV2ZW50RW1pdHRlcixcbiAgSW5wdXQsXG4gIE9uSW5pdCxcbiAgT3V0cHV0LCBRdWVyeUxpc3QsXG4gIFZpZXdDaGlsZCwgVmlld0NoaWxkcmVuXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtGb3JtQnVpbGRlciwgRm9ybUdyb3VwfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQge01lbnVTZWFyY2hTZXJ2aWNlfSBmcm9tICcuL21lbnUtc2VhcmNoLnNlcnZpY2UnO1xuaW1wb3J0IHtIb3RrZXksIEhvdGtleXNTZXJ2aWNlfSBmcm9tICdhbmd1bGFyMi1ob3RrZXlzJztcbmltcG9ydCB7TWF0SW5wdXR9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2lucHV0JztcbmltcG9ydCB7TmF2U2VydmljZX0gZnJvbSAnLi4vc3RhdGUvbmF2LnNlcnZpY2UnO1xuaW1wb3J0IHtET1dOX0FSUk9XLCBFTlRFUiwgRVNDQVBFLCBVUF9BUlJPV30gZnJvbSAnQGFuZ3VsYXIvY2RrL2tleWNvZGVzJztcbmltcG9ydCB7SGlnaGxpZ2h0Q29tcG9uZW50fSBmcm9tICcuLi9oaWdobGlnaHQvaGlnaGxpZ2h0LmNvbXBvbmVudCc7XG5pbXBvcnQge0FjdGl2ZURlc2NlbmRhbnRLZXlNYW5hZ2VyfSBmcm9tICdAYW5ndWxhci9jZGsvYTExeSc7XG5pbXBvcnQge21hcH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHtNZW51SXRlbX0gZnJvbSAnLi4vbWVudS9tZW51LWl0ZW0vbWVudS1pdGVtLm1vZGVsJztcbmltcG9ydCB7TmVzdGVkVHJlZUNvbnRyb2x9IGZyb20gJ0Bhbmd1bGFyL2Nkay90cmVlJztcbmltcG9ydCB7Um91dGVyfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICd1aWtpdC1tZW51LXNlYXJjaCcsXG4gIHRlbXBsYXRlVXJsOiAnLi9tZW51LXNlYXJjaC5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL21lbnUtc2VhcmNoLmNvbXBvbmVudC5zY3NzJ10sXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoXG59KVxuZXhwb3J0IGNsYXNzIE1lbnVTZWFyY2hDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIEFmdGVyVmlld0luaXQge1xuICBASW5wdXQoKSBwbGFjZWhvbGRlclRleHQgPSBkb2N1bWVudC5ib2R5LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLndpZHRoID4gOTcwID8gJ1Blc3F1aXNhIChDdHJsICsgQWx0ICsgMyknIDogJ1Blc3F1aXNhJztcbiAgQE91dHB1dCgpIGV2ZW50U2VhcmNoOiBFdmVudEVtaXR0ZXI8YW55PiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgQFZpZXdDaGlsZCgnc2VhcmNoT3BlbicpIHNlYXJjaE9wZW46IE1hdElucHV0O1xuXG4gIEBWaWV3Q2hpbGRyZW4oSGlnaGxpZ2h0Q29tcG9uZW50KSBpdGVuc01lbnU6IFF1ZXJ5TGlzdDxIaWdobGlnaHRDb21wb25lbnQ+O1xuICBwcml2YXRlIGtleU1hbmFnZXI6IEFjdGl2ZURlc2NlbmRhbnRLZXlNYW5hZ2VyPEhpZ2hsaWdodENvbXBvbmVudD47XG4gIHB1YmxpYyB0cmVlQ29udHJvbCA9IG5ldyBOZXN0ZWRUcmVlQ29udHJvbDxNZW51SXRlbT4obm9kZSA9PiBub2RlLmNoaWxkcmVuKTtcbiAgcHVibGljIG1lbnVJdGVtcyQgPSB0aGlzLm1lbnVTZWFyY2hTZXJ2aWNlLm1lbnVJdGVtcyQ7XG5cbiAgc3RhdGVGb3JtOiBGb3JtR3JvdXAgPSB0aGlzLmZiLmdyb3VwKHtcbiAgICBzZWFyY2hHcm91cHM6ICcnLFxuICB9KTtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIGZiOiBGb3JtQnVpbGRlcixcbiAgICBwcm90ZWN0ZWQgbWVudVNlYXJjaFNlcnZpY2U6IE1lbnVTZWFyY2hTZXJ2aWNlLFxuICAgIHB1YmxpYyBob3RrZXlzU2VydmljZTogSG90a2V5c1NlcnZpY2UsXG4gICAgcHJvdGVjdGVkIG5hdlNlcnZpY2U6IE5hdlNlcnZpY2UsXG4gICAgcHJvdGVjdGVkIHJvdXRlcjogUm91dGVyLFxuICApIHtcbiAgfVxuXG4gIHB1YmxpYyBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICB0aGlzLm1lbnVTZWFyY2hTZXJ2aWNlLmJ1c2NhcignJyk7XG4gICAgdGhpcy5zdGF0ZUZvcm0uZ2V0KCdzZWFyY2hHcm91cHMnKS52YWx1ZUNoYW5nZXMuc3Vic2NyaWJlKHZhbHVlID0+IHtcbiAgICAgIHRoaXMubWVudVNlYXJjaFNlcnZpY2UuYnVzY2FyKHZhbHVlKTtcbiAgICB9KTtcbiAgfVxuXG4gIHB1YmxpYyBuZ0FmdGVyVmlld0luaXQoKTogdm9pZCB7XG4gICAgdGhpcy5ob3RrZXlzU2VydmljZS5hZGQobmV3IEhvdGtleSgnY3RybCthbHQrMycsIChldmVudDogS2V5Ym9hcmRFdmVudCk6IGJvb2xlYW4gPT4ge1xuICAgICAgdGhpcy5uYXZTZXJ2aWNlLnRvZ2dsZUxlZnROYXYoKTtcbiAgICAgIHRoaXMuc2VhcmNoT3Blbi5mb2N1cygpO1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH0pKTtcblxuICAgIGlmICh0aGlzLml0ZW5zTWVudSkge1xuICAgICAgdGhpcy5rZXlNYW5hZ2VyID0gbmV3IEFjdGl2ZURlc2NlbmRhbnRLZXlNYW5hZ2VyKHRoaXMuaXRlbnNNZW51KS53aXRoV3JhcCgpLndpdGhUeXBlQWhlYWQoKTtcbiAgICB9XG4gIH1cblxuICBwdWJsaWMgZ29Ub0xpbmsoaXRlbTogTWVudUl0ZW0pOiB2b2lkIHtcbiAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbaXRlbS5saW5rXSk7XG4gIH1cblxuICBwdWJsaWMgb25LZXl1cChldmVudDogYW55KTogdm9pZCB7XG4gICAgc3dpdGNoIChldmVudC5rZXlDb2RlKSB7XG4gICAgICBjYXNlIEVOVEVSOlxuICAgICAgICB0aGlzLmdvVG9MaW5rKHRoaXMua2V5TWFuYWdlci5hY3RpdmVJdGVtLml0ZW0pO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgRVNDQVBFOlxuICAgICAgICB0aGlzLm1lbnVTZWFyY2hTZXJ2aWNlLmJ1c2NhcignJyk7XG4gICAgICAgIHRoaXMuc2VhcmNoT3Blbi52YWx1ZSA9ICcnO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgVVBfQVJST1c6XG4gICAgICBjYXNlIERPV05fQVJST1c6XG4gICAgICAgIHRoaXMua2V5TWFuYWdlci5vbktleWRvd24oZXZlbnQpO1xuICAgICAgICB0aGlzLmFjdGl2ZUl0ZW0odGhpcy5rZXlNYW5hZ2VyLmFjdGl2ZUl0ZW0uaXRlbSwgZXZlbnQua2V5Q29kZSA9PT0gVVBfQVJST1cpO1xuICAgICAgICB0aGlzLmV4cGFuZEFsbE5vZGUoKTtcbiAgICAgICAgYnJlYWs7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBleHBhbmRBbGxOb2RlKCkge1xuICAgIHRoaXMubWVudUl0ZW1zJC5waXBlKG1hcCgobWVudUl0ZW5zOiBNZW51SXRlbVtdKSA9PiB7XG4gICAgICBjb25zdCBmdW5jSXRlbnMgPSAoaXRlbnM6IE1lbnVJdGVtW10pID0+IHtcbiAgICAgICAgaXRlbnMuZm9yRWFjaCgoaXRlbSkgPT4ge1xuICAgICAgICAgIGlmIChpdGVtLmNoaWxkcmVuKSB7XG4gICAgICAgICAgICB0aGlzLnRyZWVDb250cm9sLmV4cGFuZChpdGVtKTtcbiAgICAgICAgICAgIGZ1bmNJdGVucyhpdGVtLmNoaWxkcmVuKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgfTtcblxuICAgICAgZnVuY0l0ZW5zKG1lbnVJdGVucyk7XG4gICAgfSkpLnN1YnNjcmliZSgpO1xuICB9XG5cbiAgcHJpdmF0ZSBhY3RpdmVJdGVtKGl0ZW06IE1lbnVJdGVtLCB1cDogYm9vbGVhbik6IHZvaWQge1xuICAgIGlmIChpdGVtLmNoaWxkcmVuICYmIGl0ZW0uY2hpbGRyZW4ubGVuZ3RoID4gMCkge1xuICAgICAgaWYgKHVwKSB7XG4gICAgICAgIHRoaXMua2V5TWFuYWdlci5zZXRQcmV2aW91c0l0ZW1BY3RpdmUoKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMua2V5TWFuYWdlci5zZXROZXh0SXRlbUFjdGl2ZSgpO1xuICAgICAgfVxuICAgICAgdGhpcy5hY3RpdmVJdGVtKHRoaXMua2V5TWFuYWdlci5hY3RpdmVJdGVtLml0ZW0sIHVwKTtcbiAgICB9XG4gIH1cbn1cbiJdfQ==