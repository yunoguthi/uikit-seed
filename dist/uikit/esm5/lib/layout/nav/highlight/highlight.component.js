/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, HostBinding, Input } from '@angular/core';
var HighlightComponent = /** @class */ (function () {
    function HighlightComponent() {
        this.disabled = false;
        this._isActive = false;
    }
    Object.defineProperty(HighlightComponent.prototype, "isActive", {
        get: /**
         * @return {?}
         */
        function () {
            return this._isActive;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    HighlightComponent.prototype.setActiveStyles = /**
     * @return {?}
     */
    function () {
        this._isActive = true;
    };
    /**
     * @return {?}
     */
    HighlightComponent.prototype.setInactiveStyles = /**
     * @return {?}
     */
    function () {
        this._isActive = false;
    };
    /**
     * @return {?}
     */
    HighlightComponent.prototype.getLabel = /**
     * @return {?}
     */
    function () {
        return '';
    };
    HighlightComponent.decorators = [
        { type: Component, args: [{
                    selector: 'uikit-highlight',
                    template: "\n    <li [class.disabled]='disabled'>\n      <ng-content></ng-content>\n    </li>\n  "
                }] }
    ];
    HighlightComponent.propDecorators = {
        item: [{ type: Input }],
        disabled: [{ type: Input }],
        isActive: [{ type: HostBinding, args: ['class.active',] }]
    };
    return HighlightComponent;
}());
export { HighlightComponent };
if (false) {
    /** @type {?} */
    HighlightComponent.prototype.item;
    /** @type {?} */
    HighlightComponent.prototype.disabled;
    /**
     * @type {?}
     * @private
     */
    HighlightComponent.prototype._isActive;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGlnaGxpZ2h0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvbGF5b3V0L25hdi9oaWdobGlnaHQvaGlnaGxpZ2h0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRzlEO0lBQUE7UUFXVyxhQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ2xCLGNBQVMsR0FBRyxLQUFLLENBQUM7SUFpQjVCLENBQUM7SUFmQyxzQkFBaUMsd0NBQVE7Ozs7UUFBekM7WUFDRSxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUM7UUFDeEIsQ0FBQzs7O09BQUE7Ozs7SUFFRCw0Q0FBZTs7O0lBQWY7UUFDRSxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztJQUN4QixDQUFDOzs7O0lBRUQsOENBQWlCOzs7SUFBakI7UUFDRSxJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztJQUN6QixDQUFDOzs7O0lBRUQscUNBQVE7OztJQUFSO1FBQ0UsT0FBTyxFQUFFLENBQUM7SUFDWixDQUFDOztnQkE1QkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxpQkFBaUI7b0JBQzNCLFFBQVEsRUFBRSx3RkFJVDtpQkFFRjs7O3VCQUVFLEtBQUs7MkJBQ0wsS0FBSzsyQkFHTCxXQUFXLFNBQUMsY0FBYzs7SUFlN0IseUJBQUM7Q0FBQSxBQTdCRCxJQTZCQztTQXBCWSxrQkFBa0I7OztJQUM3QixrQ0FBYzs7SUFDZCxzQ0FBMEI7Ozs7O0lBQzFCLHVDQUEwQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSG9zdEJpbmRpbmcsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIaWdobGlnaHRhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL2ExMXknO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICd1aWtpdC1oaWdobGlnaHQnLFxuICB0ZW1wbGF0ZTogYFxuICAgIDxsaSBbY2xhc3MuZGlzYWJsZWRdPSdkaXNhYmxlZCc+XG4gICAgICA8bmctY29udGVudD48L25nLWNvbnRlbnQ+XG4gICAgPC9saT5cbiAgYCxcbiAgc3R5bGVzOiBbXSxcbn0pXG5leHBvcnQgY2xhc3MgSGlnaGxpZ2h0Q29tcG9uZW50IGltcGxlbWVudHMgSGlnaGxpZ2h0YWJsZSB7XG4gIEBJbnB1dCgpIGl0ZW07XG4gIEBJbnB1dCgpIGRpc2FibGVkID0gZmFsc2U7XG4gIHByaXZhdGUgX2lzQWN0aXZlID0gZmFsc2U7XG5cbiAgQEhvc3RCaW5kaW5nKCdjbGFzcy5hY3RpdmUnKSBnZXQgaXNBY3RpdmUoKSB7XG4gICAgcmV0dXJuIHRoaXMuX2lzQWN0aXZlO1xuICB9XG5cbiAgc2V0QWN0aXZlU3R5bGVzKCkge1xuICAgIHRoaXMuX2lzQWN0aXZlID0gdHJ1ZTtcbiAgfVxuXG4gIHNldEluYWN0aXZlU3R5bGVzKCkge1xuICAgIHRoaXMuX2lzQWN0aXZlID0gZmFsc2U7XG4gIH1cblxuICBnZXRMYWJlbCgpOiBzdHJpbmcge1xuICAgIHJldHVybiAnJztcbiAgfVxufVxuIl19