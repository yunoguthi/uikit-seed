/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { ScrollDispatcher } from '@angular/cdk/overlay';
import { DOCUMENT } from '@angular/common';
import { ChangeDetectionStrategy, Component, ContentChildren, ElementRef, Inject, Input, QueryList, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material';
import { ActivatedRoute, NavigationEnd, NavigationStart, Router } from '@angular/router';
import { Hotkey, HotkeysService } from 'angular2-hotkeys';
import { BehaviorSubject, merge, Subscription, timer } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { UpdateInfoService } from '../update/update-info.service';
import { UpdateService } from '../update/update.service';
import { NotificacaoComponent } from './header/notification/notificacao/notificacao.component';
import { LayoutType } from './layout-type.model';
import { LayoutService } from './layout.service';
import { BreadcrumbService } from './nav/breadcrumb/breadcrumbs/breadcrumb.service';
import { FavNavsQuery } from './nav/favnav/state/favnavs.query';
import { FavNavsService } from './nav/favnav/state/favnavs.service';
import { NavQuery } from './nav/state/nav.query';
import { NavService } from './nav/state/nav.service';
import { ToastService } from './toast/toast.service';
import { FormBuilder } from '@angular/forms';
import { UikitRrippleService } from "../utils/uikit-ripple.service";
import { PreviousRouteService } from '../components/previous-route.service';
var LayoutComponent = /** @class */ (function () {
    function LayoutComponent(navQuery, favNavsQuery, navService, scroll, layoutService, router, activatedRoute, updateInfoService, updateService, favNavsService, hotkeysService, breadcrumbService, toastService, uikitRrippleService, fb, document, previousRouteService) {
        this.navQuery = navQuery;
        this.favNavsQuery = favNavsQuery;
        this.navService = navService;
        this.scroll = scroll;
        this.layoutService = layoutService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.updateInfoService = updateInfoService;
        this.updateService = updateService;
        this.favNavsService = favNavsService;
        this.hotkeysService = hotkeysService;
        this.breadcrumbService = breadcrumbService;
        this.toastService = toastService;
        this.uikitRrippleService = uikitRrippleService;
        this.fb = fb;
        this.document = document;
        this.previousRouteService = previousRouteService;
        this.hasActionsSubject = new BehaviorSubject(false);
        this.hasActions$ = this.hasActionsSubject.asObservable();
        this.showShell = this.layoutService.showShell$;
        this.hiddenBreadcrumb = this.layoutService.showBreadcrumb$;
        this.showFullScrren = this.layoutService.showFullScreen$;
        this.hiddenShell = this.layoutService.hiddenShell$;
        this.showNotifications = false;
        this.showSystemInfo = false;
        this.showUserInfo = false;
        this.showEnvironment = false;
        this.showIa = false;
        this.environment = null;
        this.allowedTypes = [
            'normal',
            'fullscreen',
            'noshell',
            'noshellfullscreen',
            'noshellnobreadcrumb',
            'noshellnobreadcrumbfullscreen'
        ];
        this.defaultType = 'normal';
        this.showShortcuts = true;
        this.subscription = new Subscription();
        this.isFixed$ = this.layoutService.isFixed$;
        this.isMobile$ = this.layoutService.isMobile$;
        this.rightnav$ = this.navQuery.rightnav$;
        this.leftnav$ = this.navQuery.leftnav$;
        this.data$ = new BehaviorSubject(null);
        this.possuiRecursoDeFavoritos = this.favNavsService.possuiRecursoDeFavoritos;
        this.notificacoes$ = this.layoutService.notificacoes$;
        this.hasInstallOption$ = this.updateInfoService.hasInstallOption$;
        this.hasUpdate$ = this.updateInfoService.hasUpdate$;
        this.favorites$ = this.favNavsQuery.favorites$;
    }
    /**
     * @return {?}
     */
    LayoutComponent.prototype.ngAfterContentInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.notificacoesProjetadas.changes.subscribe((/**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var notificacoesModel = _this.notificacoesProjetadas.toArray().map((/**
             * @param {?} r
             * @return {?}
             */
            function (r) {
                return {
                    nome: r.nome,
                    icone: r.icone,
                    descricao: r.descricao,
                    data: r.data,
                    read: r.read
                };
            }));
            _this.layoutService.setNotificacoes(notificacoesModel);
        }));
        this.notificacoesProjetadas.notifyOnChanges();
    };
    /**
     * @return {?}
     */
    LayoutComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.uikitRrippleService.init();
        this.subscription.add(this.leftnav$.subscribe((/**
         * @param {?} leftnav
         * @return {?}
         */
        function (leftnav) {
            _this.leftnav = leftnav;
        })));
        this.subscription.add((this.closeWhenNotPinnedLeftSubscription = this.router.events
            .pipe(filter((/**
         * @param {?} event
         * @return {?}
         */
        function (event) { return event instanceof NavigationStart; })))
            .subscribe((/**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            if (_this.leftnav.pinned === false) {
                _this.navService.closeLeftNav();
            }
        }))));
        this.subscription.add(this.rightnav$.subscribe((/**
         * @param {?} rightnav
         * @return {?}
         */
        function (rightnav) {
            _this.rightnav = rightnav;
        })));
        this.subscription.add((this.closeWhenNotPinnedRightSubscription = this.router.events
            .pipe(filter((/**
         * @param {?} event
         * @return {?}
         */
        function (event) { return event instanceof NavigationStart; })))
            .subscribe((/**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            if (_this.rightnav.pinned === false) {
                _this.navService.closeRightNav();
            }
        }))));
        /** @type {?} */
        var nagivate = this.router.events.pipe(filter((/**
         * @param {?} event
         * @return {?}
         */
        function (event) { return event instanceof NavigationEnd; })));
        /** @type {?} */
        var params = this.activatedRoute.params;
        merge(nagivate, params).pipe(map((/**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var child = _this.activatedRoute.firstChild;
            while (child) {
                if (child.firstChild) {
                    child = child.firstChild;
                }
                else if (child.snapshot.data) {
                    return child.snapshot.data;
                }
                else {
                    return null;
                }
            }
            return null;
        })))
            .subscribe((/**
         * @param {?} customData
         * @return {?}
         */
        function (customData) {
            /** @type {?} */
            var route = _this.router.routerState.root.snapshot;
            while (route.firstChild != null) {
                route = route.firstChild;
            }
            /** @type {?} */
            var data = route.data;
            /** @type {?} */
            var favItem = _this.getFavItem(data);
            // console.log('layout - nagivate, params', favItem);
            // favItem.icon = customData ? customData.icon : null;
            _this.data$.next(favItem);
        }));
        // When connection become offline, then put the entire app with grayscale (0.8)
        window.addEventListener('online', (/**
         * @return {?}
         */
        function () {
            document.querySelector('body').style.filter = '';
            if (localStorage.getItem('offline') === 'true') {
                timer(600).subscribe((/**
                 * @return {?}
                 */
                function () { return _this.toastService.success('Conexão de rede restabelecida!'); }));
                localStorage.removeItem('offline');
            }
        }));
        window.addEventListener('offline', (/**
         * @return {?}
         */
        function () {
            document.querySelector('body').style.filter = 'grayscale(0.8)';
            localStorage.setItem('offline', 'true');
            timer(600).subscribe((/**
             * @return {?}
             */
            function () { return _this.toastService.warning('Sem conexão de rede!'); }));
        }));
        this.subscription.add((this.scrollSubscription = this.scroll
            .scrolled()
            .pipe(map((/**
         * @param {?} data
         * @return {?}
         */
        function (data) {
            if (data) {
                /** @type {?} */
                var element = data.getElementRef();
                if (element) {
                    return element.nativeElement.scrollTop;
                }
            }
        })))
            .subscribe((/**
         * @param {?} scrollY
         * @return {?}
         */
        function (scrollY) {
            if (scrollY) {
                _this.layoutService.setFixed(scrollY > 0);
            }
        }))));
        this.router.events
            .pipe(filter((/**
         * @param {?} event
         * @return {?}
         */
        function (event) { return event instanceof NavigationEnd; })))
            .subscribe((/**
         * @return {?}
         */
        function () {
            _this.setLayoutTypeByRouteEvent();
            _this.updateIfHasActions();
        }));
        setTimeout((/**
         * @return {?}
         */
        function () {
            _this.updateIfHasActions();
        }), 0);
    };
    /**
     * @return {?}
     */
    LayoutComponent.prototype.toggleMobile = /**
     * @return {?}
     */
    function () {
        this.layoutService.toggleMobile();
    };
    /**
     * @return {?}
     */
    LayoutComponent.prototype.updateIfHasActions = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var elements = document.getElementsByClassName('uikit-actions');
        /** @type {?} */
        var element = elements.item(0);
        if (element != null) {
            /** @type {?} */
            var hasChildNodes = element.hasChildNodes();
            this.hasActionsSubject.next(hasChildNodes);
        }
        else {
            this.hasActionsSubject.next(false);
        }
    };
    /**
     * @return {?}
     */
    LayoutComponent.prototype.ngAfterViewInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.hotkeysService.add(new Hotkey('ctrl+m', (/**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            _this.leftNavToggle();
            return false;
        })));
        this.hotkeysService.add(new Hotkey('ctrl+alt+j', (/**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            _this.rightNavToggle();
            return false;
        })));
        this.isVerifyTypeShellApply();
    };
    /**
     * @private
     * @param {?} customData
     * @return {?}
     */
    LayoutComponent.prototype.getFavItem = /**
     * @private
     * @param {?} customData
     * @return {?}
     */
    function (customData) {
        /** @type {?} */
        var data = customData;
        /** @type {?} */
        var lastBreadCrumb = this.breadcrumbService.breadcrumbs[this.breadcrumbService.breadcrumbs.length - 1];
        if (lastBreadCrumb != null) {
            /** @type {?} */
            var title = lastBreadCrumb.title;
            return { link: this.router.url, title: title, icon: data ? data.icon : null };
        }
    };
    /**
     * @return {?}
     */
    LayoutComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this.subscription.unsubscribe();
    };
    /**
     * @return {?}
     */
    LayoutComponent.prototype.leftNavToggle = /**
     * @return {?}
     */
    function () {
        this.navService.toggleLeftNav();
    };
    /**
     * @return {?}
     */
    LayoutComponent.prototype.leftNavClose = /**
     * @return {?}
     */
    function () {
        this.navService.closeLeftNav();
    };
    /**
     * @return {?}
     */
    LayoutComponent.prototype.rightNavToggle = /**
     * @return {?}
     */
    function () {
        this.navService.toggleRightNav();
    };
    /**
     * @return {?}
     */
    LayoutComponent.prototype.rightNavClose = /**
     * @return {?}
     */
    function () {
        this.navService.closeRightNav();
    };
    /**
     * @return {?}
     */
    LayoutComponent.prototype.navsClose = /**
     * @return {?}
     */
    function () {
        if (this.leftnav.opened && !this.leftnav.pinned) {
            this.navService.toggleLeftNav();
        }
        if (this.rightnav.opened && !this.rightnav.pinned) {
            this.navService.toggleRightNav();
        }
    };
    /**
     * @return {?}
     */
    LayoutComponent.prototype.install = /**
     * @return {?}
     */
    function () {
        this.updateInfoService.install();
    };
    /**
     * @return {?}
     */
    LayoutComponent.prototype.update = /**
     * @return {?}
     */
    function () {
        this.updateService.update();
    };
    /**
     * @return {?}
     */
    LayoutComponent.prototype.isVerifyTypeShellApply = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.showFullScrren.subscribe((/**
         * @param {?} isShowFullScreen
         * @return {?}
         */
        function (isShowFullScreen) {
            if (isShowFullScreen) {
                /** @type {?} */
                var fsDocElem = (/** @type {?} */ (_this.document.documentElement));
                // document.documentElement as FsDocumentElement;
                _this.openFullscreen(fsDocElem);
            }
        }));
    };
    /**
     * @param {?} fsDocElem
     * @return {?}
     */
    LayoutComponent.prototype.openFullscreen = /**
     * @param {?} fsDocElem
     * @return {?}
     */
    function (fsDocElem) {
        var _this = this;
        /*debugger;*/
        if (fsDocElem.requestFullscreen) {
            timer(600).subscribe((/**
             * @return {?}
             */
            function () {
                fsDocElem.requestFullscreen().catch((/**
                 * @param {?} error
                 * @return {?}
                 */
                function (error) { return console.error(error); }));
            }));
        }
        else if (fsDocElem.msRequestFullscreen) {
            try {
                timer(600).subscribe((/**
                 * @return {?}
                 */
                function () {
                    fsDocElem.msRequestFullscreen().catch((/**
                     * @param {?} error
                     * @return {?}
                     */
                    function (error) { return console.error(error); }));
                }));
            }
            catch (error) {
                console.error(error);
            }
        }
        else if (fsDocElem.mozRequestFullScreen) {
            try {
                timer(600).subscribe((/**
                 * @return {?}
                 */
                function () {
                    fsDocElem.mozRequestFullScreen().catch((/**
                     * @param {?} error
                     * @return {?}
                     */
                    function (error) { return console.error(error); }));
                }));
            }
            catch (error) {
                console.error(error);
            }
        }
        else if (fsDocElem.webkitRequestFullscreen) {
            try {
                timer(600).subscribe((/**
                 * @return {?}
                 */
                function () {
                    fsDocElem.webkitRequestFullscreen().catch((/**
                     * @param {?} error
                     * @return {?}
                     */
                    function (error) { return console.error(error); }));
                }));
            }
            catch (error) {
                console.error(error);
            }
        }
        timer(900).subscribe((/**
         * @return {?}
         */
        function () {
            ((/** @type {?} */ (_this.myClickFullscreen.nativeElement))).click();
        }));
    };
    /**
     * @private
     * @return {?}
     */
    LayoutComponent.prototype.closeFullscreen = /**
     * @private
     * @return {?}
     */
    function () {
        if (this.document.exitFullscreen) {
            this.document.exitFullscreen();
        }
        else if (this.document.mozCancelFullScreen) {
            /* Firefox */
            this.document.mozCancelFullScreen();
        }
        else if (this.document.webkitExitFullscreen) {
            /* Chrome, Safari and Opera */
            this.document.webkitExitFullscreen();
        }
        else if (this.document.msExitFullscreen) {
            /* IE/Edge */
            this.document.msExitFullscreen();
        }
    };
    /*** Region RouteLayoutApplication
     * Modo de apresentação das tela aplicado de forma totalmente configurável a forma geral e por rota.
     * ***/
    /**
     * Region RouteLayoutApplication
     * Modo de apresentação das tela aplicado de forma totalmente configurável a forma geral e por rota.
     * **
     * @return {?}
     */
    LayoutComponent.prototype.setLayoutTypeByRouteEvent = /**
     * Region RouteLayoutApplication
     * Modo de apresentação das tela aplicado de forma totalmente configurável a forma geral e por rota.
     * **
     * @return {?}
     */
    function () {
        /** @type {?} */
        var queryType = this.router.routerState.root.snapshot.queryParamMap.get('type');
        /** @type {?} */
        var dataRota = this.getData();
        /** @type {?} */
        var typeDesejado = 'normal';
        if (queryType) {
            // usuario informou via url (tem prioridade sobre todos)
            typeDesejado = queryType;
        }
        else if (dataRota && dataRota.defaultType) {
            // esta configurado na rota (tem prioridade somente sobre o global - sobrescrita)
            typeDesejado = dataRota.defaultType;
        }
        else {
            typeDesejado = this.defaultType;
            // busca o global
        }
        if (typeDesejado === 'noshell') {
            this.navService.toggleLeftNav();
        }
        /** @type {?} */
        var defaultTypeRoute = (dataRota && dataRota.defaultType) ? dataRota.defaultType : this.defaultType;
        if (dataRota && dataRota.allowedTypes && dataRota.allowedTypes.length > 0) {
            // existe configuracao de tipos permitidos na rota
            /** @type {?} */
            var ehPermitidoPelaRota = dataRota.allowedTypes.some((/**
             * @param {?} type
             * @return {?}
             */
            function (type) { return type === typeDesejado; }));
            if (ehPermitidoPelaRota) {
                this.layoutService.setType(LayoutType[typeDesejado]);
            }
            else {
                console.error("O parametro " + typeDesejado + " n\u00E3o est\u00E1 definido como permitido, favor verificar!");
                this.layoutService.setType(LayoutType[defaultTypeRoute]);
            }
        }
        else {
            // não existe configuração de tipos permitidos na rota devo então ver do componente layout (global da aplicação)
            /** @type {?} */
            var allowedTypesLayout = this.allowedTypes;
            /** @type {?} */
            var ehPermitidoGlobalmente = allowedTypesLayout.some((/**
             * @param {?} type
             * @return {?}
             */
            function (type) { return type === typeDesejado; }));
            if (ehPermitidoGlobalmente) {
                this.layoutService.setType(LayoutType[typeDesejado]);
            }
            else {
                console.error("O parametro " + typeDesejado + " n\u00E3o est\u00E1 definido como permitido, favor verificar!");
                this.layoutService.setType(LayoutType[defaultTypeRoute]);
            }
        }
        if ((typeDesejado === 'normal' || defaultTypeRoute === 'normal') && this.document.fullscreenElement) {
            this.closeFullscreen();
        }
    };
    /**
     * @private
     * @return {?}
     */
    LayoutComponent.prototype.getData = /**
     * @private
     * @return {?}
     */
    function () {
        /** @type {?} */
        var route = this.router.routerState.root.snapshot;
        while (route.firstChild != null) {
            route = route.firstChild;
            if (route.routeConfig === null) {
                continue;
            }
            if (!route.routeConfig.path) {
                continue;
            }
            if (!route.data.defaultType) {
                continue;
            }
            return (/** @type {?} */ (route.data));
        }
    };
    LayoutComponent.decorators = [
        { type: Component, args: [{
                    selector: 'uikit-layout',
                    template: "<button #myClickFullscreen style=\"display: none;\"></button>\n<mat-sidenav-container\n  attr.data-environment=\"{{ environment }}\" [class.environment]=\"!showEnvironment\" *ngIf=\"(showShell | async)\"\n  [hasBackdrop]=\"(!(leftnav$ | async).pinned && (leftnav$ | async).opened) || (!(rightnav$ | async).pinned && (rightnav$ | async).opened)\"\n  (backdropClick)=\"navsClose();\"\n  [class.is-fixed]=\"(isFixed$ | async)\"\n  [class.is-mobileAction]=\"(isMobile$ | async)\"\n  [class.is-rightPinned]=\"(rightnav$ | async).pinned\" [class.is-leftPinned]=\"(leftnav$ | async).pinned\"\n  [class.is-favorite]=\"(possuiRecursoDeFavoritos) && (favorites$ | async).length > 0\">\n  <mat-sidenav class=\"uikit-nav\" #sidenav position=\"start\" disableClose=\"true\"\n               *ngIf=\"(hiddenShell | async)\"\n               [mode]=\"(leftnav$ | async).pinned ? 'side' : 'over'\" [opened]=\"(leftnav$ | async).opened\"\n               (keydown.escape)=\"leftNavClose()\" [fixedInViewport]=\"true\">\n    <uikit-nav>\n      <div nav-menu>\n        <ng-content select=\"[uikit-menu]\"></ng-content>\n        <ng-content select=\"[layout-nav-menu-extras]\"></ng-content>\n      </div>\n    </uikit-nav>\n  </mat-sidenav>\n\n  <mat-sidenav *ngIf=\"showIa\"\n               #aikitSideNav class=\"aikit-nav\" disableClose=\"true\" position=\"end\"\n               [opened]=\"(rightnav$ | async).opened\"\n               [mode]=\"(rightnav$ | async).pinned ? 'side' : 'over'\"\n               (keydown.escape)=\"rightNavClose()\" [fixedInViewport]=\"false\">\n\n    <uikit-ia></uikit-ia>\n\n  </mat-sidenav>\n  <mat-sidenav-content cdkScrollable [ngClass]=\"{\n  'noshell': !(hiddenShell | async),\n  'noshellnobreadcrumb': !(hiddenBreadcrumb | async)\n  }\">\n    <uikit-header [showNotifications]=\"showNotifications\"\n                  [showSystemInfo]=\"showSystemInfo\"\n                  [showUserInfo]=\"showUserInfo\" [showIa]=\"showIa\" [showShortcuts]=\"showShortcuts\">\n\n      <div header-notifications>\n        <ng-container *ngTemplateOutlet=\"notificacoes\"></ng-container>\n      </div>\n\n      <div header-systeminfo>\n        <ng-content select=\"[layout-header-systeminfo]\"></ng-content>\n\n        <mat-action-list *ngIf=\"(hasInstallOption$ | async) || (hasUpdate$ | async)\">\n          <!-- <mat-divider></mat-divider>-->\n          <a mat-list-item (click)=\"install()\" *ngIf=\"(hasInstallOption$ | async)\">Instalar sistema</a>\n          <a mat-list-item (click)=\"update()\" *ngIf=\"(hasUpdate$ | async)\">Atualizar vers\u00E3o</a>\n        </mat-action-list>\n      </div>\n\n      <div header-userinfo>\n\n        <ng-content select=\"[layout-header-userinfo]\"></ng-content>\n\n      </div>\n\n    </uikit-header>\n    <uikit-breadcrumb *ngIf=\"(hiddenBreadcrumb | async)\"></uikit-breadcrumb>\n\n    <div [hidden]=\"!(hasActions$ | async)\">\n      <button class=\"nav-mobile is-mobile\" mat-fab (click)=\"toggleMobile()\">\n        <mat-icon class=\"fas fa-ellipsis-h\" [class.fa-times]=\"(isMobile$ | async)\"></mat-icon>\n      </button>\n    </div>\n\n    <ng-container *ngTemplateOutlet=\"projecao\"></ng-container>\n  </mat-sidenav-content>\n</mat-sidenav-container>\n\n<ng-template *ngIf=\"(showShell | async)\">\n  <ng-container *ngTemplateOutlet=\"projecao\"></ng-container>\n</ng-template>\n\n<ng-template #projecao>\n  <div style=\"height: 100%\" [ngClass]=\"{'noshell' : !(showShell | async),\n                   'noshellnobreadcrumb': !(hiddenBreadcrumb | async)}\">\n    <ng-content></ng-content>\n  </div>\n</ng-template>\n\n<div hidden>\n  <ng-content select=\"[layout-nav-menu]\"></ng-content>\n</div>\n<div hidden>\n  <ng-content select=\"[layout-header-notifications]\"></ng-content>\n</div>\n\n<ng-template #notificacoes>\n  <ng-container *ngFor=\"let notificacao of (notificacoes$ | async)\">\n    <uikit-notificacao [nome]=\"notificacao.nome\" [icone]=\"notificacao.icone\" [descricao]=\"notificacao.descricao\"\n                       [data]=\"notificacao.data\" [read]=\"notificacao.read\"></uikit-notificacao>\n  </ng-container>\n</ng-template>\n\n",
                    changeDetection: ChangeDetectionStrategy.OnPush,
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    LayoutComponent.ctorParameters = function () { return [
        { type: NavQuery },
        { type: FavNavsQuery },
        { type: NavService },
        { type: ScrollDispatcher },
        { type: LayoutService },
        { type: Router },
        { type: ActivatedRoute },
        { type: UpdateInfoService },
        { type: UpdateService },
        { type: FavNavsService },
        { type: HotkeysService },
        { type: BreadcrumbService },
        { type: ToastService },
        { type: UikitRrippleService },
        { type: FormBuilder },
        { type: undefined, decorators: [{ type: Inject, args: [DOCUMENT,] }] },
        { type: PreviousRouteService }
    ]; };
    LayoutComponent.propDecorators = {
        myClickFullscreen: [{ type: ViewChild, args: ['myClickFullscreen',] }],
        showNotifications: [{ type: Input }],
        showSystemInfo: [{ type: Input }],
        showUserInfo: [{ type: Input }],
        showEnvironment: [{ type: Input }],
        showIa: [{ type: Input }],
        environment: [{ type: Input }],
        allowedTypes: [{ type: Input }],
        defaultType: [{ type: Input }],
        showShortcuts: [{ type: Input }],
        notificacoesProjetadas: [{ type: ContentChildren, args: [NotificacaoComponent, { descendants: false },] }],
        sidenav: [{ type: ViewChild, args: [MatSidenav,] }]
    };
    return LayoutComponent;
}());
export { LayoutComponent };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.hasActionsSubject;
    /** @type {?} */
    LayoutComponent.prototype.hasActions$;
    /** @type {?} */
    LayoutComponent.prototype.myClickFullscreen;
    /** @type {?} */
    LayoutComponent.prototype.showShell;
    /** @type {?} */
    LayoutComponent.prototype.hiddenBreadcrumb;
    /** @type {?} */
    LayoutComponent.prototype.showFullScrren;
    /** @type {?} */
    LayoutComponent.prototype.hiddenShell;
    /** @type {?} */
    LayoutComponent.prototype.showNotifications;
    /** @type {?} */
    LayoutComponent.prototype.showSystemInfo;
    /** @type {?} */
    LayoutComponent.prototype.showUserInfo;
    /** @type {?} */
    LayoutComponent.prototype.showEnvironment;
    /** @type {?} */
    LayoutComponent.prototype.showIa;
    /** @type {?} */
    LayoutComponent.prototype.environment;
    /** @type {?} */
    LayoutComponent.prototype.allowedTypes;
    /** @type {?} */
    LayoutComponent.prototype.defaultType;
    /** @type {?} */
    LayoutComponent.prototype.showShortcuts;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.subscription;
    /** @type {?} */
    LayoutComponent.prototype.isFixed$;
    /** @type {?} */
    LayoutComponent.prototype.isMobile$;
    /** @type {?} */
    LayoutComponent.prototype.rightnav$;
    /** @type {?} */
    LayoutComponent.prototype.rightnav;
    /** @type {?} */
    LayoutComponent.prototype.leftnav$;
    /** @type {?} */
    LayoutComponent.prototype.leftnav;
    /** @type {?} */
    LayoutComponent.prototype.data$;
    /** @type {?} */
    LayoutComponent.prototype.possuiRecursoDeFavoritos;
    /**
     * @type {?}
     * @private
     */
    LayoutComponent.prototype.scrollSubscription;
    /** @type {?} */
    LayoutComponent.prototype.notificacoesProjetadas;
    /** @type {?} */
    LayoutComponent.prototype.sidenav;
    /** @type {?} */
    LayoutComponent.prototype.notificacoes$;
    /**
     * @type {?}
     * @private
     */
    LayoutComponent.prototype.closeWhenNotPinnedLeftSubscription;
    /**
     * @type {?}
     * @private
     */
    LayoutComponent.prototype.closeWhenNotPinnedRightSubscription;
    /** @type {?} */
    LayoutComponent.prototype.hasInstallOption$;
    /** @type {?} */
    LayoutComponent.prototype.hasUpdate$;
    /** @type {?} */
    LayoutComponent.prototype.favorites$;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.navQuery;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.favNavsQuery;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.navService;
    /** @type {?} */
    LayoutComponent.prototype.scroll;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.layoutService;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.router;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.activatedRoute;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.updateInfoService;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.updateService;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.favNavsService;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.hotkeysService;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.breadcrumbService;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.toastService;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.uikitRrippleService;
    /**
     * @type {?}
     * @private
     */
    LayoutComponent.prototype.fb;
    /**
     * @type {?}
     * @private
     */
    LayoutComponent.prototype.document;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.previousRouteService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGF5b3V0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvbGF5b3V0L2xheW91dC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBZ0IsZ0JBQWdCLEVBQUMsTUFBTSxzQkFBc0IsQ0FBQztBQUNyRSxPQUFPLEVBQUMsUUFBUSxFQUFDLE1BQU0saUJBQWlCLENBQUM7QUFDekMsT0FBTyxFQUdMLHVCQUF1QixFQUN2QixTQUFTLEVBQ1QsZUFBZSxFQUNmLFVBQVUsRUFDVixNQUFNLEVBQ04sS0FBSyxFQUdMLFNBQVMsRUFDVCxTQUFTLEVBQ1YsTUFBTSxlQUFlLENBQUM7QUFDdkIsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLG1CQUFtQixDQUFDO0FBQzdDLE9BQU8sRUFBQyxjQUFjLEVBQUUsYUFBYSxFQUFFLGVBQWUsRUFBRSxNQUFNLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUV2RixPQUFPLEVBQUMsTUFBTSxFQUFFLGNBQWMsRUFBQyxNQUFNLGtCQUFrQixDQUFDO0FBQ3hELE9BQU8sRUFBQyxlQUFlLEVBQUUsS0FBSyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUMsTUFBTSxNQUFNLENBQUM7QUFDakUsT0FBTyxFQUFDLE1BQU0sRUFBRSxHQUFHLEVBQUMsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzQyxPQUFPLEVBQUMsaUJBQWlCLEVBQUMsTUFBTSwrQkFBK0IsQ0FBQztBQUNoRSxPQUFPLEVBQUMsYUFBYSxFQUFDLE1BQU0sMEJBQTBCLENBQUM7QUFDdkQsT0FBTyxFQUFDLG9CQUFvQixFQUFDLE1BQU0seURBQXlELENBQUM7QUFDN0YsT0FBTyxFQUFvQixVQUFVLEVBQUMsTUFBTSxxQkFBcUIsQ0FBQztBQUNsRSxPQUFPLEVBQUMsYUFBYSxFQUFDLE1BQU0sa0JBQWtCLENBQUM7QUFDL0MsT0FBTyxFQUFDLGlCQUFpQixFQUFDLE1BQU0saURBQWlELENBQUM7QUFDbEYsT0FBTyxFQUFDLFlBQVksRUFBQyxNQUFNLGtDQUFrQyxDQUFDO0FBQzlELE9BQU8sRUFBQyxjQUFjLEVBQUMsTUFBTSxvQ0FBb0MsQ0FBQztBQUVsRSxPQUFPLEVBQUMsUUFBUSxFQUFDLE1BQU0sdUJBQXVCLENBQUM7QUFDL0MsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLHlCQUF5QixDQUFDO0FBQ25ELE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSx1QkFBdUIsQ0FBQztBQUNuRCxPQUFPLEVBQUMsV0FBVyxFQUFDLE1BQU0sZ0JBQWdCLENBQUM7QUFDM0MsT0FBTyxFQUFDLG1CQUFtQixFQUFDLE1BQU0sK0JBQStCLENBQUM7QUFFbEUsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFFNUU7SUFpRUUseUJBQ1ksUUFBa0IsRUFDbEIsWUFBMEIsRUFDMUIsVUFBc0IsRUFDekIsTUFBd0IsRUFDckIsYUFBNEIsRUFDNUIsTUFBYyxFQUNkLGNBQThCLEVBQzlCLGlCQUFvQyxFQUNwQyxhQUE0QixFQUM1QixjQUE4QixFQUM5QixjQUE4QixFQUM5QixpQkFBb0MsRUFDcEMsWUFBMEIsRUFDMUIsbUJBQXdDLEVBQzFDLEVBQWUsRUFDRyxRQUFhLEVBQzdCLG9CQUEwQztRQWhCMUMsYUFBUSxHQUFSLFFBQVEsQ0FBVTtRQUNsQixpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUMxQixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3pCLFdBQU0sR0FBTixNQUFNLENBQWtCO1FBQ3JCLGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBQzVCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQUNwQyxrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUM1QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBbUI7UUFDcEMsaUJBQVksR0FBWixZQUFZLENBQWM7UUFDMUIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUMxQyxPQUFFLEdBQUYsRUFBRSxDQUFhO1FBQ0csYUFBUSxHQUFSLFFBQVEsQ0FBSztRQUM3Qix5QkFBb0IsR0FBcEIsb0JBQW9CLENBQXNCO1FBMUU1QyxzQkFBaUIsR0FBRyxJQUFJLGVBQWUsQ0FBVSxLQUFLLENBQUMsQ0FBQztRQUMzRCxnQkFBVyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUlwRCxjQUFTLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUM7UUFDMUMscUJBQWdCLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxlQUFlLENBQUM7UUFDdEQsbUJBQWMsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLGVBQWUsQ0FBQztRQUNwRCxnQkFBVyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDO1FBRXJDLHNCQUFpQixHQUFHLEtBQUssQ0FBQztRQUMxQixtQkFBYyxHQUFHLEtBQUssQ0FBQztRQUN2QixpQkFBWSxHQUFHLEtBQUssQ0FBQztRQUNyQixvQkFBZSxHQUFHLEtBQUssQ0FBQztRQUN4QixXQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ2YsZ0JBQVcsR0FBVyxJQUFJLENBQUM7UUFDM0IsaUJBQVksR0FBRztZQUM3QixRQUFRO1lBQ1IsWUFBWTtZQUNaLFNBQVM7WUFDVCxtQkFBbUI7WUFDbkIscUJBQXFCO1lBQ3JCLCtCQUErQjtTQUNoQyxDQUFDO1FBQ2MsZ0JBQVcsR0FBRyxRQUFRLENBQUM7UUFDdkIsa0JBQWEsR0FBRyxJQUFJLENBQUM7UUFFM0IsaUJBQVksR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBRXJDLGFBQVEsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQztRQUN2QyxjQUFTLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUM7UUFFekMsY0FBUyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDO1FBRXBDLGFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQztRQUdsQyxVQUFLLEdBQUcsSUFBSSxlQUFlLENBQU0sSUFBSSxDQUFDLENBQUM7UUFFOUMsNkJBQXdCLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyx3QkFBd0IsQ0FBQztRQVNqRSxrQkFBYSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDO1FBS2pELHNCQUFpQixHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBQztRQUM3RCxlQUFVLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFVBQVUsQ0FBQztRQUMvQyxlQUFVLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUM7SUFxQmpELENBQUM7Ozs7SUFFRCw0Q0FBa0I7OztJQUFsQjtRQUFBLGlCQWVDO1FBYkMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLE9BQU8sQ0FBQyxTQUFTOzs7UUFBQzs7Z0JBQ3RDLGlCQUFpQixHQUFHLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxHQUFHOzs7O1lBQUMsVUFBQSxDQUFDO2dCQUNuRSxPQUFPO29CQUNMLElBQUksRUFBRSxDQUFDLENBQUMsSUFBSTtvQkFDWixLQUFLLEVBQUUsQ0FBQyxDQUFDLEtBQUs7b0JBQ2QsU0FBUyxFQUFFLENBQUMsQ0FBQyxTQUFTO29CQUN0QixJQUFJLEVBQUUsQ0FBQyxDQUFDLElBQUk7b0JBQ1osSUFBSSxFQUFFLENBQUMsQ0FBQyxJQUFJO2lCQUNiLENBQUM7WUFDSixDQUFDLEVBQUM7WUFDRixLQUFJLENBQUMsYUFBYSxDQUFDLGVBQWUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQ3hELENBQUMsRUFBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLHNCQUFzQixDQUFDLGVBQWUsRUFBRSxDQUFDO0lBQ2hELENBQUM7Ozs7SUFFRCxrQ0FBUTs7O0lBQVI7UUFBQSxpQkErR0M7UUE5R0MsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksRUFBRSxDQUFDO1FBRWhDLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUNuQixJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVM7Ozs7UUFBQyxVQUFBLE9BQU87WUFDN0IsS0FBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7UUFDekIsQ0FBQyxFQUFDLENBQ0gsQ0FBQztRQUVGLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUNuQixDQUFDLElBQUksQ0FBQyxrQ0FBa0MsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU07YUFDMUQsSUFBSSxDQUFDLE1BQU07Ozs7UUFBQyxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUssWUFBWSxlQUFlLEVBQWhDLENBQWdDLEVBQUMsQ0FBQzthQUN2RCxTQUFTOzs7O1FBQUMsVUFBQSxLQUFLO1lBQ2QsSUFBSSxLQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sS0FBSyxLQUFLLEVBQUU7Z0JBQ2pDLEtBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxFQUFFLENBQUM7YUFDaEM7UUFDSCxDQUFDLEVBQUMsQ0FBQyxDQUNOLENBQUM7UUFFRixJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FDbkIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTOzs7O1FBQUMsVUFBQSxRQUFRO1lBQy9CLEtBQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO1FBQzNCLENBQUMsRUFBQyxDQUNILENBQUM7UUFFRixJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FDbkIsQ0FBQyxJQUFJLENBQUMsbUNBQW1DLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNO2FBQzNELElBQUksQ0FBQyxNQUFNOzs7O1FBQUMsVUFBQSxLQUFLLElBQUksT0FBQSxLQUFLLFlBQVksZUFBZSxFQUFoQyxDQUFnQyxFQUFDLENBQUM7YUFDdkQsU0FBUzs7OztRQUFDLFVBQUEsS0FBSztZQUNkLElBQUksS0FBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEtBQUssS0FBSyxFQUFFO2dCQUNsQyxLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsRUFBRSxDQUFDO2FBQ2pDO1FBQ0gsQ0FBQyxFQUFDLENBQUMsQ0FDTixDQUFDOztZQUVJLFFBQVEsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTTs7OztRQUFDLFVBQUEsS0FBSyxJQUFJLE9BQUEsS0FBSyxZQUFZLGFBQWEsRUFBOUIsQ0FBOEIsRUFBQyxDQUFDOztZQUNuRixNQUFNLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNO1FBRXpDLEtBQUssQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUMxQixHQUFHOzs7UUFBQzs7Z0JBQ0UsS0FBSyxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsVUFBVTtZQUMxQyxPQUFPLEtBQUssRUFBRTtnQkFDWixJQUFJLEtBQUssQ0FBQyxVQUFVLEVBQUU7b0JBQ3BCLEtBQUssR0FBRyxLQUFLLENBQUMsVUFBVSxDQUFDO2lCQUMxQjtxQkFBTSxJQUFJLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFO29CQUM5QixPQUFPLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDO2lCQUM1QjtxQkFBTTtvQkFDTCxPQUFPLElBQUksQ0FBQztpQkFDYjthQUNGO1lBQ0QsT0FBTyxJQUFJLENBQUM7UUFDZCxDQUFDLEVBQUMsQ0FBQzthQUNGLFNBQVM7Ozs7UUFBQyxVQUFBLFVBQVU7O2dCQUNmLEtBQUssR0FBRyxLQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUTtZQUNqRCxPQUFPLEtBQUssQ0FBQyxVQUFVLElBQUksSUFBSSxFQUFFO2dCQUMvQixLQUFLLEdBQUcsS0FBSyxDQUFDLFVBQVUsQ0FBQzthQUMxQjs7Z0JBQ0ssSUFBSSxHQUFHLEtBQUssQ0FBQyxJQUFJOztnQkFHakIsT0FBTyxHQUFHLEtBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDO1lBQ3JDLHFEQUFxRDtZQUNyRCxzREFBc0Q7WUFDdEQsS0FBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDM0IsQ0FBQyxFQUFDLENBQUM7UUFFTCwrRUFBK0U7UUFDL0UsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFFBQVE7OztRQUFFO1lBQ2hDLFFBQVEsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUM7WUFDakQsSUFBSSxZQUFZLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxLQUFLLE1BQU0sRUFBRTtnQkFDOUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLFNBQVM7OztnQkFBQyxjQUFNLE9BQUEsS0FBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsZ0NBQWdDLENBQUMsRUFBM0QsQ0FBMkQsRUFBQyxDQUFDO2dCQUN4RixZQUFZLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2FBQ3BDO1FBQ0gsQ0FBQyxFQUFDLENBQUM7UUFDSCxNQUFNLENBQUMsZ0JBQWdCLENBQUMsU0FBUzs7O1FBQUU7WUFDakMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLGdCQUFnQixDQUFDO1lBQy9ELFlBQVksQ0FBQyxPQUFPLENBQUMsU0FBUyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBQ3hDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxTQUFTOzs7WUFBQyxjQUFNLE9BQUEsS0FBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsc0JBQXNCLENBQUMsRUFBakQsQ0FBaUQsRUFBQyxDQUFDO1FBQ2hGLENBQUMsRUFBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQ25CLENBQUMsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxNQUFNO2FBQ25DLFFBQVEsRUFBRTthQUNWLElBQUksQ0FDSCxHQUFHOzs7O1FBQ0QsVUFBQyxJQUFtQjtZQUNsQixJQUFJLElBQUksRUFBRTs7b0JBQ0YsT0FBTyxHQUFHLElBQUksQ0FBQyxhQUFhLEVBQUU7Z0JBQ3BDLElBQUksT0FBTyxFQUFFO29CQUNYLE9BQU8sT0FBTyxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUM7aUJBQ3hDO2FBQ0Y7UUFDSCxDQUFDLEVBQ0YsQ0FDRjthQUNBLFNBQVM7Ozs7UUFBQyxVQUFDLE9BQWU7WUFDekIsSUFBSSxPQUFPLEVBQUU7Z0JBQ1gsS0FBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQyxDQUFDO2FBQzFDO1FBQ0gsQ0FBQyxFQUFDLENBQUMsQ0FDTixDQUFDO1FBRUYsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNO2FBQ2YsSUFBSSxDQUFDLE1BQU07Ozs7UUFBQyxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUssWUFBWSxhQUFhLEVBQTlCLENBQThCLEVBQUMsQ0FBQzthQUNyRCxTQUFTOzs7UUFBQztZQUNULEtBQUksQ0FBQyx5QkFBeUIsRUFBRSxDQUFDO1lBQ2pDLEtBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1FBQzVCLENBQUMsRUFBQyxDQUFDO1FBQ0wsVUFBVTs7O1FBQUM7WUFDVCxLQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztRQUM1QixDQUFDLEdBQUUsQ0FBQyxDQUFDLENBQUM7SUFDUixDQUFDOzs7O0lBRUQsc0NBQVk7OztJQUFaO1FBQ0UsSUFBSSxDQUFDLGFBQWEsQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUNwQyxDQUFDOzs7O0lBRUQsNENBQWtCOzs7SUFBbEI7O1lBQ1EsUUFBUSxHQUFHLFFBQVEsQ0FBQyxzQkFBc0IsQ0FBQyxlQUFlLENBQUM7O1lBQzNELE9BQU8sR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztRQUNoQyxJQUFJLE9BQU8sSUFBSSxJQUFJLEVBQUU7O2dCQUNiLGFBQWEsR0FBRyxPQUFPLENBQUMsYUFBYSxFQUFFO1lBQzdDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7U0FDNUM7YUFBTTtZQUNMLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDcEM7SUFDSCxDQUFDOzs7O0lBRUQseUNBQWU7OztJQUFmO1FBQUEsaUJBWUM7UUFWQyxJQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxRQUFROzs7O1FBQUUsVUFBQyxLQUFvQjtZQUNoRSxLQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7WUFDckIsT0FBTyxLQUFLLENBQUM7UUFDZixDQUFDLEVBQUMsQ0FBQyxDQUFDO1FBQ0osSUFBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsSUFBSSxNQUFNLENBQUMsWUFBWTs7OztRQUFFLFVBQUMsS0FBb0I7WUFDcEUsS0FBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBQ3RCLE9BQU8sS0FBSyxDQUFDO1FBQ2YsQ0FBQyxFQUFDLENBQUMsQ0FBQztRQUVKLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO0lBQ2hDLENBQUM7Ozs7OztJQUVPLG9DQUFVOzs7OztJQUFsQixVQUFtQixVQUFVOztZQUNyQixJQUFJLEdBQUcsVUFBVTs7WUFDakIsY0FBYyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFdBQVcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO1FBQ3hHLElBQUksY0FBYyxJQUFJLElBQUksRUFBRTs7Z0JBQ3BCLEtBQUssR0FBRyxjQUFjLENBQUMsS0FBSztZQUNsQyxPQUFPLEVBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLEtBQUssT0FBQSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBQyxDQUFDO1NBQ3RFO0lBQ0gsQ0FBQzs7OztJQUVELHFDQUFXOzs7SUFBWDtRQUNFLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDbEMsQ0FBQzs7OztJQUVELHVDQUFhOzs7SUFBYjtRQUNFLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDbEMsQ0FBQzs7OztJQUVELHNDQUFZOzs7SUFBWjtRQUNFLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDakMsQ0FBQzs7OztJQUVELHdDQUFjOzs7SUFBZDtRQUNFLElBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxFQUFFLENBQUM7SUFDbkMsQ0FBQzs7OztJQUVELHVDQUFhOzs7SUFBYjtRQUNFLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDbEMsQ0FBQzs7OztJQUVELG1DQUFTOzs7SUFBVDtRQUNFLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRTtZQUMvQyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsRUFBRSxDQUFDO1NBQ2pDO1FBRUQsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFO1lBQ2pELElBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxFQUFFLENBQUM7U0FDbEM7SUFDSCxDQUFDOzs7O0lBRUQsaUNBQU87OztJQUFQO1FBQ0UsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQ25DLENBQUM7Ozs7SUFFRCxnQ0FBTTs7O0lBQU47UUFDRSxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sRUFBRSxDQUFDO0lBQzlCLENBQUM7Ozs7SUFFTSxnREFBc0I7OztJQUE3QjtRQUFBLGlCQVVDO1FBUkMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTOzs7O1FBQUMsVUFBQyxnQkFBeUI7WUFDdEQsSUFBSSxnQkFBZ0IsRUFBRTs7b0JBQ2QsU0FBUyxHQUFHLG1CQUFBLEtBQUksQ0FBQyxRQUFRLENBQUMsZUFBZSxFQUFxQjtnQkFDcEUsaURBQWlEO2dCQUNqRCxLQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxDQUFDO2FBQ2hDO1FBQ0gsQ0FBQyxFQUFDLENBQUM7SUFFTCxDQUFDOzs7OztJQUVNLHdDQUFjOzs7O0lBQXJCLFVBQXNCLFNBQWM7UUFBcEMsaUJBa0NDO1FBakNDLGFBQWE7UUFDYixJQUFJLFNBQVMsQ0FBQyxpQkFBaUIsRUFBRTtZQUMvQixLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsU0FBUzs7O1lBQUM7Z0JBQ25CLFNBQVMsQ0FBQyxpQkFBaUIsRUFBRSxDQUFDLEtBQUs7Ozs7Z0JBQUMsVUFBQSxLQUFLLElBQUksT0FBQSxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxFQUFwQixDQUFvQixFQUFDLENBQUM7WUFDckUsQ0FBQyxFQUFDLENBQUM7U0FDSjthQUFNLElBQUksU0FBUyxDQUFDLG1CQUFtQixFQUFFO1lBQ3hDLElBQUk7Z0JBQ0YsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLFNBQVM7OztnQkFBQztvQkFDbkIsU0FBUyxDQUFDLG1CQUFtQixFQUFFLENBQUMsS0FBSzs7OztvQkFBQyxVQUFBLEtBQUssSUFBSSxPQUFBLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEVBQXBCLENBQW9CLEVBQUMsQ0FBQztnQkFDdkUsQ0FBQyxFQUFDLENBQUM7YUFDSjtZQUFDLE9BQU8sS0FBSyxFQUFFO2dCQUNkLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDdEI7U0FDRjthQUFNLElBQUksU0FBUyxDQUFDLG9CQUFvQixFQUFFO1lBQ3pDLElBQUk7Z0JBQ0YsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLFNBQVM7OztnQkFBQztvQkFDbkIsU0FBUyxDQUFDLG9CQUFvQixFQUFFLENBQUMsS0FBSzs7OztvQkFBQyxVQUFBLEtBQUssSUFBSSxPQUFBLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEVBQXBCLENBQW9CLEVBQUMsQ0FBQztnQkFDeEUsQ0FBQyxFQUFDLENBQUM7YUFDSjtZQUFDLE9BQU8sS0FBSyxFQUFFO2dCQUNkLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDdEI7U0FDRjthQUFNLElBQUksU0FBUyxDQUFDLHVCQUF1QixFQUFFO1lBQzVDLElBQUk7Z0JBQ0YsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLFNBQVM7OztnQkFBQztvQkFDbkIsU0FBUyxDQUFDLHVCQUF1QixFQUFFLENBQUMsS0FBSzs7OztvQkFBQyxVQUFBLEtBQUssSUFBSSxPQUFBLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEVBQXBCLENBQW9CLEVBQUMsQ0FBQztnQkFDM0UsQ0FBQyxFQUFDLENBQUM7YUFDSjtZQUFDLE9BQU8sS0FBSyxFQUFFO2dCQUNkLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDdEI7U0FDRjtRQUNELEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxTQUFTOzs7UUFBQztZQUNuQixDQUFDLG1CQUFBLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLEVBQWUsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ2hFLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7SUFFTyx5Q0FBZTs7OztJQUF2QjtRQUNFLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxjQUFjLEVBQUU7WUFDaEMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxjQUFjLEVBQUUsQ0FBQztTQUNoQzthQUFNLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsRUFBRTtZQUM1QyxhQUFhO1lBQ2IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO1NBQ3JDO2FBQU0sSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLG9CQUFvQixFQUFFO1lBQzdDLDhCQUE4QjtZQUM5QixJQUFJLENBQUMsUUFBUSxDQUFDLG9CQUFvQixFQUFFLENBQUM7U0FDdEM7YUFBTSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLEVBQUU7WUFDekMsYUFBYTtZQUNiLElBQUksQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztTQUNsQztJQUNILENBQUM7SUFFRDs7V0FFTzs7Ozs7OztJQUNBLG1EQUF5Qjs7Ozs7O0lBQWhDOztZQUNRLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDOztZQUMzRSxRQUFRLEdBQUcsSUFBSSxDQUFDLE9BQU8sRUFBRTs7WUFFM0IsWUFBWSxHQUFHLFFBQVE7UUFDM0IsSUFBSSxTQUFTLEVBQUU7WUFDYix3REFBd0Q7WUFDeEQsWUFBWSxHQUFHLFNBQVMsQ0FBQztTQUMxQjthQUFNLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxXQUFXLEVBQUU7WUFDM0MsaUZBQWlGO1lBQ2pGLFlBQVksR0FBRyxRQUFRLENBQUMsV0FBVyxDQUFDO1NBQ3JDO2FBQU07WUFDTCxZQUFZLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQztZQUNoQyxpQkFBaUI7U0FDbEI7UUFFRCxJQUFJLFlBQVksS0FBSyxTQUFTLEVBQUU7WUFDOUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLEVBQUUsQ0FBQztTQUNqQzs7WUFFSyxnQkFBZ0IsR0FBRyxDQUFDLFFBQVEsSUFBSSxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXO1FBQ3JHLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxZQUFZLElBQUksUUFBUSxDQUFDLFlBQVksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFOzs7Z0JBRW5FLG1CQUFtQixHQUFHLFFBQVEsQ0FBQyxZQUFZLENBQUMsSUFBSTs7OztZQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxLQUFLLFlBQVksRUFBckIsQ0FBcUIsRUFBQztZQUNyRixJQUFJLG1CQUFtQixFQUFFO2dCQUN2QixJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQzthQUN0RDtpQkFBTTtnQkFDTCxPQUFPLENBQUMsS0FBSyxDQUFDLGlCQUFlLFlBQVksa0VBQXFELENBQUMsQ0FBQztnQkFDaEcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQzthQUMxRDtTQUNGO2FBQU07OztnQkFFQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsWUFBWTs7Z0JBQ3RDLHNCQUFzQixHQUFHLGtCQUFrQixDQUFDLElBQUk7Ozs7WUFBQyxVQUFBLElBQUksSUFBSSxPQUFBLElBQUksS0FBSyxZQUFZLEVBQXJCLENBQXFCLEVBQUM7WUFDckYsSUFBSSxzQkFBc0IsRUFBRTtnQkFDMUIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7YUFDdEQ7aUJBQU07Z0JBQ0wsT0FBTyxDQUFDLEtBQUssQ0FBQyxpQkFBZSxZQUFZLGtFQUFxRCxDQUFDLENBQUM7Z0JBQ2hHLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7YUFDMUQ7U0FDRjtRQUNELElBQUksQ0FBQyxZQUFZLEtBQUssUUFBUSxJQUFJLGdCQUFnQixLQUFLLFFBQVEsQ0FBQyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsaUJBQWlCLEVBQUU7WUFDbkcsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO1NBQ3hCO0lBQ0gsQ0FBQzs7Ozs7SUFFTyxpQ0FBTzs7OztJQUFmOztZQUNNLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUTtRQUNqRCxPQUFPLEtBQUssQ0FBQyxVQUFVLElBQUksSUFBSSxFQUFFO1lBQy9CLEtBQUssR0FBRyxLQUFLLENBQUMsVUFBVSxDQUFDO1lBQ3pCLElBQUksS0FBSyxDQUFDLFdBQVcsS0FBSyxJQUFJLEVBQUU7Z0JBQzlCLFNBQVM7YUFDVjtZQUNELElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRTtnQkFDM0IsU0FBUzthQUNWO1lBQ0QsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFO2dCQUMzQixTQUFTO2FBQ1Y7WUFDRCxPQUFPLG1CQUFBLEtBQUssQ0FBQyxJQUFJLEVBQW1ELENBQUM7U0FDdEU7SUFDSCxDQUFDOztnQkFuYUYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxjQUFjO29CQUN4QixtaElBQXNDO29CQUV0QyxlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTs7aUJBQ2hEOzs7O2dCQWJPLFFBQVE7Z0JBSFIsWUFBWTtnQkFJWixVQUFVO2dCQWhDSyxnQkFBZ0I7Z0JBMEIvQixhQUFhO2dCQVRtQyxNQUFNO2dCQUF0RCxjQUFjO2dCQUtkLGlCQUFpQjtnQkFDakIsYUFBYTtnQkFNYixjQUFjO2dCQVZOLGNBQWM7Z0JBUXRCLGlCQUFpQjtnQkFNakIsWUFBWTtnQkFFWixtQkFBbUI7Z0JBRG5CLFdBQVc7Z0RBc0ZkLE1BQU0sU0FBQyxRQUFRO2dCQW5GWCxvQkFBb0I7OztvQ0FhMUIsU0FBUyxTQUFDLG1CQUFtQjtvQ0FPN0IsS0FBSztpQ0FDTCxLQUFLOytCQUNMLEtBQUs7a0NBQ0wsS0FBSzt5QkFDTCxLQUFLOzhCQUNMLEtBQUs7K0JBQ0wsS0FBSzs4QkFRTCxLQUFLO2dDQUNMLEtBQUs7eUNBa0JMLGVBQWUsU0FBQyxvQkFBb0IsRUFBRSxFQUFDLFdBQVcsRUFBRSxLQUFLLEVBQUM7MEJBRzFELFNBQVMsU0FBQyxVQUFVOztJQWdYdkIsc0JBQUM7Q0FBQSxBQXRhRCxJQXNhQztTQWhhWSxlQUFlOzs7Ozs7SUFFMUIsNENBQWtFOztJQUNsRSxzQ0FBMkQ7O0lBRTNELDRDQUEyRTs7SUFFM0Usb0NBQWlEOztJQUNqRCwyQ0FBNkQ7O0lBQzdELHlDQUEyRDs7SUFDM0Qsc0NBQXFEOztJQUVyRCw0Q0FBMEM7O0lBQzFDLHlDQUF1Qzs7SUFDdkMsdUNBQXFDOztJQUNyQywwQ0FBd0M7O0lBQ3hDLGlDQUErQjs7SUFDL0Isc0NBQTJDOztJQUMzQyx1Q0FPRTs7SUFDRixzQ0FBdUM7O0lBQ3ZDLHdDQUFxQzs7Ozs7SUFFckMsdUNBQTRDOztJQUU1QyxtQ0FBOEM7O0lBQzlDLG9DQUFnRDs7SUFFaEQsb0NBQTJDOztJQUMzQyxtQ0FBc0Q7O0lBQ3RELG1DQUF5Qzs7SUFDekMsa0NBQXFEOztJQUVyRCxnQ0FBOEM7O0lBRTlDLG1EQUF3RTs7Ozs7SUFFeEUsNkNBQXlDOztJQUV6QyxpREFDK0Q7O0lBRS9ELGtDQUEyQzs7SUFFM0Msd0NBQXdEOzs7OztJQUV4RCw2REFBeUQ7Ozs7O0lBQ3pELDhEQUEwRDs7SUFFMUQsNENBQW9FOztJQUNwRSxxQ0FBc0Q7O0lBQ3RELHFDQUFpRDs7Ozs7SUFHL0MsbUNBQTRCOzs7OztJQUM1Qix1Q0FBb0M7Ozs7O0lBQ3BDLHFDQUFnQzs7SUFDaEMsaUNBQStCOzs7OztJQUMvQix3Q0FBc0M7Ozs7O0lBQ3RDLGlDQUF3Qjs7Ozs7SUFDeEIseUNBQXdDOzs7OztJQUN4Qyw0Q0FBOEM7Ozs7O0lBQzlDLHdDQUFzQzs7Ozs7SUFDdEMseUNBQXdDOzs7OztJQUN4Qyx5Q0FBd0M7Ozs7O0lBQ3hDLDRDQUE4Qzs7Ozs7SUFDOUMsdUNBQW9DOzs7OztJQUNwQyw4Q0FBa0Q7Ozs7O0lBQ2xELDZCQUF1Qjs7Ozs7SUFDdkIsbUNBQXVDOzs7OztJQUN2QywrQ0FBb0QiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0Nka1Njcm9sbGFibGUsIFNjcm9sbERpc3BhdGNoZXJ9IGZyb20gJ0Bhbmd1bGFyL2Nkay9vdmVybGF5JztcbmltcG9ydCB7RE9DVU1FTlR9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQge1xuICBBZnRlckNvbnRlbnRJbml0LFxuICBBZnRlclZpZXdJbml0LFxuICBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneSxcbiAgQ29tcG9uZW50LFxuICBDb250ZW50Q2hpbGRyZW4sXG4gIEVsZW1lbnRSZWYsXG4gIEluamVjdCxcbiAgSW5wdXQsXG4gIE9uRGVzdHJveSxcbiAgT25Jbml0LFxuICBRdWVyeUxpc3QsXG4gIFZpZXdDaGlsZFxufSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7TWF0U2lkZW5hdn0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xuaW1wb3J0IHtBY3RpdmF0ZWRSb3V0ZSwgTmF2aWdhdGlvbkVuZCwgTmF2aWdhdGlvblN0YXJ0LCBSb3V0ZXJ9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5cbmltcG9ydCB7SG90a2V5LCBIb3RrZXlzU2VydmljZX0gZnJvbSAnYW5ndWxhcjItaG90a2V5cyc7XG5pbXBvcnQge0JlaGF2aW9yU3ViamVjdCwgbWVyZ2UsIFN1YnNjcmlwdGlvbiwgdGltZXJ9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHtmaWx0ZXIsIG1hcH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHtVcGRhdGVJbmZvU2VydmljZX0gZnJvbSAnLi4vdXBkYXRlL3VwZGF0ZS1pbmZvLnNlcnZpY2UnO1xuaW1wb3J0IHtVcGRhdGVTZXJ2aWNlfSBmcm9tICcuLi91cGRhdGUvdXBkYXRlLnNlcnZpY2UnO1xuaW1wb3J0IHtOb3RpZmljYWNhb0NvbXBvbmVudH0gZnJvbSAnLi9oZWFkZXIvbm90aWZpY2F0aW9uL25vdGlmaWNhY2FvL25vdGlmaWNhY2FvLmNvbXBvbmVudCc7XG5pbXBvcnQge0ZzRG9jdW1lbnRFbGVtZW50LCBMYXlvdXRUeXBlfSBmcm9tICcuL2xheW91dC10eXBlLm1vZGVsJztcbmltcG9ydCB7TGF5b3V0U2VydmljZX0gZnJvbSAnLi9sYXlvdXQuc2VydmljZSc7XG5pbXBvcnQge0JyZWFkY3J1bWJTZXJ2aWNlfSBmcm9tICcuL25hdi9icmVhZGNydW1iL2JyZWFkY3J1bWJzL2JyZWFkY3J1bWIuc2VydmljZSc7XG5pbXBvcnQge0Zhdk5hdnNRdWVyeX0gZnJvbSAnLi9uYXYvZmF2bmF2L3N0YXRlL2Zhdm5hdnMucXVlcnknO1xuaW1wb3J0IHtGYXZOYXZzU2VydmljZX0gZnJvbSAnLi9uYXYvZmF2bmF2L3N0YXRlL2Zhdm5hdnMuc2VydmljZSc7XG5cbmltcG9ydCB7TmF2UXVlcnl9IGZyb20gJy4vbmF2L3N0YXRlL25hdi5xdWVyeSc7XG5pbXBvcnQge05hdlNlcnZpY2V9IGZyb20gJy4vbmF2L3N0YXRlL25hdi5zZXJ2aWNlJztcbmltcG9ydCB7VG9hc3RTZXJ2aWNlfSBmcm9tICcuL3RvYXN0L3RvYXN0LnNlcnZpY2UnO1xuaW1wb3J0IHtGb3JtQnVpbGRlcn0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHtVaWtpdFJyaXBwbGVTZXJ2aWNlfSBmcm9tIFwiLi4vdXRpbHMvdWlraXQtcmlwcGxlLnNlcnZpY2VcIjtcblxuaW1wb3J0IHsgUHJldmlvdXNSb3V0ZVNlcnZpY2UgfSBmcm9tICcuLi9jb21wb25lbnRzL3ByZXZpb3VzLXJvdXRlLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICd1aWtpdC1sYXlvdXQnLFxuICB0ZW1wbGF0ZVVybDogJy4vbGF5b3V0LmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vbGF5b3V0LmNvbXBvbmVudC5zY3NzJ10sXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoXG59KVxuZXhwb3J0IGNsYXNzIExheW91dENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95LCBBZnRlclZpZXdJbml0LCBBZnRlckNvbnRlbnRJbml0IHtcblxuICBwcm90ZWN0ZWQgaGFzQWN0aW9uc1N1YmplY3QgPSBuZXcgQmVoYXZpb3JTdWJqZWN0PGJvb2xlYW4+KGZhbHNlKTtcbiAgcHVibGljIGhhc0FjdGlvbnMkID0gdGhpcy5oYXNBY3Rpb25zU3ViamVjdC5hc09ic2VydmFibGUoKTtcblxuICBAVmlld0NoaWxkKCdteUNsaWNrRnVsbHNjcmVlbicpIG15Q2xpY2tGdWxsc2NyZWVuOiBFbGVtZW50UmVmPEhUTUxFbGVtZW50PjtcblxuICBwdWJsaWMgc2hvd1NoZWxsID0gdGhpcy5sYXlvdXRTZXJ2aWNlLnNob3dTaGVsbCQ7XG4gIHB1YmxpYyBoaWRkZW5CcmVhZGNydW1iID0gdGhpcy5sYXlvdXRTZXJ2aWNlLnNob3dCcmVhZGNydW1iJDtcbiAgcHVibGljIHNob3dGdWxsU2NycmVuID0gdGhpcy5sYXlvdXRTZXJ2aWNlLnNob3dGdWxsU2NyZWVuJDtcbiAgcHVibGljIGhpZGRlblNoZWxsID0gdGhpcy5sYXlvdXRTZXJ2aWNlLmhpZGRlblNoZWxsJDtcblxuICBASW5wdXQoKSBwdWJsaWMgc2hvd05vdGlmaWNhdGlvbnMgPSBmYWxzZTtcbiAgQElucHV0KCkgcHVibGljIHNob3dTeXN0ZW1JbmZvID0gZmFsc2U7XG4gIEBJbnB1dCgpIHB1YmxpYyBzaG93VXNlckluZm8gPSBmYWxzZTtcbiAgQElucHV0KCkgcHVibGljIHNob3dFbnZpcm9ubWVudCA9IGZhbHNlO1xuICBASW5wdXQoKSBwdWJsaWMgc2hvd0lhID0gZmFsc2U7XG4gIEBJbnB1dCgpIHB1YmxpYyBlbnZpcm9ubWVudDogc3RyaW5nID0gbnVsbDtcbiAgQElucHV0KCkgcHVibGljIGFsbG93ZWRUeXBlcyA9IFtcbiAgICAnbm9ybWFsJyxcbiAgICAnZnVsbHNjcmVlbicsXG4gICAgJ25vc2hlbGwnLFxuICAgICdub3NoZWxsZnVsbHNjcmVlbicsXG4gICAgJ25vc2hlbGxub2JyZWFkY3J1bWInLFxuICAgICdub3NoZWxsbm9icmVhZGNydW1iZnVsbHNjcmVlbidcbiAgXTtcbiAgQElucHV0KCkgcHVibGljIGRlZmF1bHRUeXBlID0gJ25vcm1hbCc7XG4gIEBJbnB1dCgpIHB1YmxpYyBzaG93U2hvcnRjdXRzID0gdHJ1ZTtcblxuICBwcm90ZWN0ZWQgc3Vic2NyaXB0aW9uID0gbmV3IFN1YnNjcmlwdGlvbigpO1xuXG4gIHB1YmxpYyBpc0ZpeGVkJCA9IHRoaXMubGF5b3V0U2VydmljZS5pc0ZpeGVkJDtcbiAgcHVibGljIGlzTW9iaWxlJCA9IHRoaXMubGF5b3V0U2VydmljZS5pc01vYmlsZSQ7XG5cbiAgcHVibGljIHJpZ2h0bmF2JCA9IHRoaXMubmF2UXVlcnkucmlnaHRuYXYkO1xuICBwdWJsaWMgcmlnaHRuYXY6IHsgb3BlbmVkOiBib29sZWFuOyBwaW5uZWQ6IGJvb2xlYW4gfTtcbiAgcHVibGljIGxlZnRuYXYkID0gdGhpcy5uYXZRdWVyeS5sZWZ0bmF2JDtcbiAgcHVibGljIGxlZnRuYXY6IHsgb3BlbmVkOiBib29sZWFuOyBwaW5uZWQ6IGJvb2xlYW4gfTtcblxuICBwdWJsaWMgZGF0YSQgPSBuZXcgQmVoYXZpb3JTdWJqZWN0PGFueT4obnVsbCk7XG5cbiAgcG9zc3VpUmVjdXJzb0RlRmF2b3JpdG9zID0gdGhpcy5mYXZOYXZzU2VydmljZS5wb3NzdWlSZWN1cnNvRGVGYXZvcml0b3M7XG5cbiAgcHJpdmF0ZSBzY3JvbGxTdWJzY3JpcHRpb246IFN1YnNjcmlwdGlvbjtcblxuICBAQ29udGVudENoaWxkcmVuKE5vdGlmaWNhY2FvQ29tcG9uZW50LCB7ZGVzY2VuZGFudHM6IGZhbHNlfSlcbiAgcHVibGljIG5vdGlmaWNhY29lc1Byb2pldGFkYXM6IFF1ZXJ5TGlzdDxOb3RpZmljYWNhb0NvbXBvbmVudD47XG5cbiAgQFZpZXdDaGlsZChNYXRTaWRlbmF2KSBzaWRlbmF2OiBNYXRTaWRlbmF2O1xuXG4gIHB1YmxpYyBub3RpZmljYWNvZXMkID0gdGhpcy5sYXlvdXRTZXJ2aWNlLm5vdGlmaWNhY29lcyQ7XG5cbiAgcHJpdmF0ZSBjbG9zZVdoZW5Ob3RQaW5uZWRMZWZ0U3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XG4gIHByaXZhdGUgY2xvc2VXaGVuTm90UGlubmVkUmlnaHRTdWJzY3JpcHRpb246IFN1YnNjcmlwdGlvbjtcblxuICBwdWJsaWMgaGFzSW5zdGFsbE9wdGlvbiQgPSB0aGlzLnVwZGF0ZUluZm9TZXJ2aWNlLmhhc0luc3RhbGxPcHRpb24kO1xuICBwdWJsaWMgaGFzVXBkYXRlJCA9IHRoaXMudXBkYXRlSW5mb1NlcnZpY2UuaGFzVXBkYXRlJDtcbiAgcHVibGljIGZhdm9yaXRlcyQgPSB0aGlzLmZhdk5hdnNRdWVyeS5mYXZvcml0ZXMkO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByb3RlY3RlZCBuYXZRdWVyeTogTmF2UXVlcnksXG4gICAgcHJvdGVjdGVkIGZhdk5hdnNRdWVyeTogRmF2TmF2c1F1ZXJ5LFxuICAgIHByb3RlY3RlZCBuYXZTZXJ2aWNlOiBOYXZTZXJ2aWNlLFxuICAgIHB1YmxpYyBzY3JvbGw6IFNjcm9sbERpc3BhdGNoZXIsXG4gICAgcHJvdGVjdGVkIGxheW91dFNlcnZpY2U6IExheW91dFNlcnZpY2UsXG4gICAgcHJvdGVjdGVkIHJvdXRlcjogUm91dGVyLFxuICAgIHByb3RlY3RlZCBhY3RpdmF0ZWRSb3V0ZTogQWN0aXZhdGVkUm91dGUsXG4gICAgcHJvdGVjdGVkIHVwZGF0ZUluZm9TZXJ2aWNlOiBVcGRhdGVJbmZvU2VydmljZSxcbiAgICBwcm90ZWN0ZWQgdXBkYXRlU2VydmljZTogVXBkYXRlU2VydmljZSxcbiAgICBwcm90ZWN0ZWQgZmF2TmF2c1NlcnZpY2U6IEZhdk5hdnNTZXJ2aWNlLFxuICAgIHByb3RlY3RlZCBob3RrZXlzU2VydmljZTogSG90a2V5c1NlcnZpY2UsXG4gICAgcHJvdGVjdGVkIGJyZWFkY3J1bWJTZXJ2aWNlOiBCcmVhZGNydW1iU2VydmljZSxcbiAgICBwcm90ZWN0ZWQgdG9hc3RTZXJ2aWNlOiBUb2FzdFNlcnZpY2UsXG4gICAgcHJvdGVjdGVkIHVpa2l0UnJpcHBsZVNlcnZpY2U6IFVpa2l0UnJpcHBsZVNlcnZpY2UsXG4gICAgcHJpdmF0ZSBmYjogRm9ybUJ1aWxkZXIsXG4gICAgQEluamVjdChET0NVTUVOVCkgcHJpdmF0ZSBkb2N1bWVudDogYW55LFxuICAgIHByb3RlY3RlZCBwcmV2aW91c1JvdXRlU2VydmljZTogUHJldmlvdXNSb3V0ZVNlcnZpY2UsXG4gICkge1xuICB9XG5cbiAgbmdBZnRlckNvbnRlbnRJbml0KCk6IHZvaWQge1xuXG4gICAgdGhpcy5ub3RpZmljYWNvZXNQcm9qZXRhZGFzLmNoYW5nZXMuc3Vic2NyaWJlKCgpID0+IHtcbiAgICAgIGNvbnN0IG5vdGlmaWNhY29lc01vZGVsID0gdGhpcy5ub3RpZmljYWNvZXNQcm9qZXRhZGFzLnRvQXJyYXkoKS5tYXAociA9PiB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgbm9tZTogci5ub21lLFxuICAgICAgICAgIGljb25lOiByLmljb25lLFxuICAgICAgICAgIGRlc2NyaWNhbzogci5kZXNjcmljYW8sXG4gICAgICAgICAgZGF0YTogci5kYXRhLFxuICAgICAgICAgIHJlYWQ6IHIucmVhZFxuICAgICAgICB9O1xuICAgICAgfSk7XG4gICAgICB0aGlzLmxheW91dFNlcnZpY2Uuc2V0Tm90aWZpY2Fjb2VzKG5vdGlmaWNhY29lc01vZGVsKTtcbiAgICB9KTtcbiAgICB0aGlzLm5vdGlmaWNhY29lc1Byb2pldGFkYXMubm90aWZ5T25DaGFuZ2VzKCk7XG4gIH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICB0aGlzLnVpa2l0UnJpcHBsZVNlcnZpY2UuaW5pdCgpO1xuXG4gICAgdGhpcy5zdWJzY3JpcHRpb24uYWRkKFxuICAgICAgdGhpcy5sZWZ0bmF2JC5zdWJzY3JpYmUobGVmdG5hdiA9PiB7XG4gICAgICAgIHRoaXMubGVmdG5hdiA9IGxlZnRuYXY7XG4gICAgICB9KVxuICAgICk7XG5cbiAgICB0aGlzLnN1YnNjcmlwdGlvbi5hZGQoXG4gICAgICAodGhpcy5jbG9zZVdoZW5Ob3RQaW5uZWRMZWZ0U3Vic2NyaXB0aW9uID0gdGhpcy5yb3V0ZXIuZXZlbnRzXG4gICAgICAgIC5waXBlKGZpbHRlcihldmVudCA9PiBldmVudCBpbnN0YW5jZW9mIE5hdmlnYXRpb25TdGFydCkpXG4gICAgICAgIC5zdWJzY3JpYmUoZXZlbnQgPT4ge1xuICAgICAgICAgIGlmICh0aGlzLmxlZnRuYXYucGlubmVkID09PSBmYWxzZSkge1xuICAgICAgICAgICAgdGhpcy5uYXZTZXJ2aWNlLmNsb3NlTGVmdE5hdigpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSkpXG4gICAgKTtcblxuICAgIHRoaXMuc3Vic2NyaXB0aW9uLmFkZChcbiAgICAgIHRoaXMucmlnaHRuYXYkLnN1YnNjcmliZShyaWdodG5hdiA9PiB7XG4gICAgICAgIHRoaXMucmlnaHRuYXYgPSByaWdodG5hdjtcbiAgICAgIH0pXG4gICAgKTtcblxuICAgIHRoaXMuc3Vic2NyaXB0aW9uLmFkZChcbiAgICAgICh0aGlzLmNsb3NlV2hlbk5vdFBpbm5lZFJpZ2h0U3Vic2NyaXB0aW9uID0gdGhpcy5yb3V0ZXIuZXZlbnRzXG4gICAgICAgIC5waXBlKGZpbHRlcihldmVudCA9PiBldmVudCBpbnN0YW5jZW9mIE5hdmlnYXRpb25TdGFydCkpXG4gICAgICAgIC5zdWJzY3JpYmUoZXZlbnQgPT4ge1xuICAgICAgICAgIGlmICh0aGlzLnJpZ2h0bmF2LnBpbm5lZCA9PT0gZmFsc2UpIHtcbiAgICAgICAgICAgIHRoaXMubmF2U2VydmljZS5jbG9zZVJpZ2h0TmF2KCk7XG4gICAgICAgICAgfVxuICAgICAgICB9KSlcbiAgICApO1xuXG4gICAgY29uc3QgbmFnaXZhdGUgPSB0aGlzLnJvdXRlci5ldmVudHMucGlwZShmaWx0ZXIoZXZlbnQgPT4gZXZlbnQgaW5zdGFuY2VvZiBOYXZpZ2F0aW9uRW5kKSk7XG4gICAgY29uc3QgcGFyYW1zID0gdGhpcy5hY3RpdmF0ZWRSb3V0ZS5wYXJhbXM7XG5cbiAgICBtZXJnZShuYWdpdmF0ZSwgcGFyYW1zKS5waXBlKFxuICAgICAgbWFwKCgpID0+IHtcbiAgICAgICAgbGV0IGNoaWxkID0gdGhpcy5hY3RpdmF0ZWRSb3V0ZS5maXJzdENoaWxkO1xuICAgICAgICB3aGlsZSAoY2hpbGQpIHtcbiAgICAgICAgICBpZiAoY2hpbGQuZmlyc3RDaGlsZCkge1xuICAgICAgICAgICAgY2hpbGQgPSBjaGlsZC5maXJzdENoaWxkO1xuICAgICAgICAgIH0gZWxzZSBpZiAoY2hpbGQuc25hcHNob3QuZGF0YSkge1xuICAgICAgICAgICAgcmV0dXJuIGNoaWxkLnNuYXBzaG90LmRhdGE7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgIH0pKVxuICAgICAgLnN1YnNjcmliZShjdXN0b21EYXRhID0+IHtcbiAgICAgICAgbGV0IHJvdXRlID0gdGhpcy5yb3V0ZXIucm91dGVyU3RhdGUucm9vdC5zbmFwc2hvdDtcbiAgICAgICAgd2hpbGUgKHJvdXRlLmZpcnN0Q2hpbGQgIT0gbnVsbCkge1xuICAgICAgICAgIHJvdXRlID0gcm91dGUuZmlyc3RDaGlsZDtcbiAgICAgICAgfVxuICAgICAgICBjb25zdCBkYXRhID0gcm91dGUuZGF0YTtcblxuXG4gICAgICAgIGNvbnN0IGZhdkl0ZW0gPSB0aGlzLmdldEZhdkl0ZW0oZGF0YSk7XG4gICAgICAgIC8vIGNvbnNvbGUubG9nKCdsYXlvdXQgLSBuYWdpdmF0ZSwgcGFyYW1zJywgZmF2SXRlbSk7XG4gICAgICAgIC8vIGZhdkl0ZW0uaWNvbiA9IGN1c3RvbURhdGEgPyBjdXN0b21EYXRhLmljb24gOiBudWxsO1xuICAgICAgICB0aGlzLmRhdGEkLm5leHQoZmF2SXRlbSk7XG4gICAgICB9KTtcblxuICAgIC8vIFdoZW4gY29ubmVjdGlvbiBiZWNvbWUgb2ZmbGluZSwgdGhlbiBwdXQgdGhlIGVudGlyZSBhcHAgd2l0aCBncmF5c2NhbGUgKDAuOClcbiAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcignb25saW5lJywgKCkgPT4ge1xuICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcignYm9keScpLnN0eWxlLmZpbHRlciA9ICcnO1xuICAgICAgaWYgKGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdvZmZsaW5lJykgPT09ICd0cnVlJykge1xuICAgICAgICB0aW1lcig2MDApLnN1YnNjcmliZSgoKSA9PiB0aGlzLnRvYXN0U2VydmljZS5zdWNjZXNzKCdDb25leMOjbyBkZSByZWRlIHJlc3RhYmVsZWNpZGEhJykpO1xuICAgICAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbSgnb2ZmbGluZScpO1xuICAgICAgfVxuICAgIH0pO1xuICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdvZmZsaW5lJywgKCkgPT4ge1xuICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcignYm9keScpLnN0eWxlLmZpbHRlciA9ICdncmF5c2NhbGUoMC44KSc7XG4gICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnb2ZmbGluZScsICd0cnVlJyk7XG4gICAgICB0aW1lcig2MDApLnN1YnNjcmliZSgoKSA9PiB0aGlzLnRvYXN0U2VydmljZS53YXJuaW5nKCdTZW0gY29uZXjDo28gZGUgcmVkZSEnKSk7XG4gICAgfSk7XG5cbiAgICB0aGlzLnN1YnNjcmlwdGlvbi5hZGQoXG4gICAgICAodGhpcy5zY3JvbGxTdWJzY3JpcHRpb24gPSB0aGlzLnNjcm9sbFxuICAgICAgICAuc2Nyb2xsZWQoKVxuICAgICAgICAucGlwZShcbiAgICAgICAgICBtYXAoXG4gICAgICAgICAgICAoZGF0YTogQ2RrU2Nyb2xsYWJsZSkgPT4ge1xuICAgICAgICAgICAgICBpZiAoZGF0YSkge1xuICAgICAgICAgICAgICAgIGNvbnN0IGVsZW1lbnQgPSBkYXRhLmdldEVsZW1lbnRSZWYoKTtcbiAgICAgICAgICAgICAgICBpZiAoZWxlbWVudCkge1xuICAgICAgICAgICAgICAgICAgcmV0dXJuIGVsZW1lbnQubmF0aXZlRWxlbWVudC5zY3JvbGxUb3A7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgKVxuICAgICAgICApXG4gICAgICAgIC5zdWJzY3JpYmUoKHNjcm9sbFk6IG51bWJlcikgPT4ge1xuICAgICAgICAgIGlmIChzY3JvbGxZKSB7XG4gICAgICAgICAgICB0aGlzLmxheW91dFNlcnZpY2Uuc2V0Rml4ZWQoc2Nyb2xsWSA+IDApO1xuICAgICAgICAgIH1cbiAgICAgICAgfSkpXG4gICAgKTtcblxuICAgIHRoaXMucm91dGVyLmV2ZW50c1xuICAgICAgLnBpcGUoZmlsdGVyKGV2ZW50ID0+IGV2ZW50IGluc3RhbmNlb2YgTmF2aWdhdGlvbkVuZCkpXG4gICAgICAuc3Vic2NyaWJlKCgpID0+IHtcbiAgICAgICAgdGhpcy5zZXRMYXlvdXRUeXBlQnlSb3V0ZUV2ZW50KCk7XG4gICAgICAgIHRoaXMudXBkYXRlSWZIYXNBY3Rpb25zKCk7XG4gICAgICB9KTtcbiAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgIHRoaXMudXBkYXRlSWZIYXNBY3Rpb25zKCk7XG4gICAgfSwgMCk7XG4gIH1cblxuICB0b2dnbGVNb2JpbGUoKSB7XG4gICAgdGhpcy5sYXlvdXRTZXJ2aWNlLnRvZ2dsZU1vYmlsZSgpO1xuICB9XG5cbiAgdXBkYXRlSWZIYXNBY3Rpb25zKCkge1xuICAgIGNvbnN0IGVsZW1lbnRzID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgndWlraXQtYWN0aW9ucycpO1xuICAgIGNvbnN0IGVsZW1lbnQgPSBlbGVtZW50cy5pdGVtKDApO1xuICAgIGlmIChlbGVtZW50ICE9IG51bGwpIHtcbiAgICAgIGNvbnN0IGhhc0NoaWxkTm9kZXMgPSBlbGVtZW50Lmhhc0NoaWxkTm9kZXMoKTtcbiAgICAgIHRoaXMuaGFzQWN0aW9uc1N1YmplY3QubmV4dChoYXNDaGlsZE5vZGVzKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5oYXNBY3Rpb25zU3ViamVjdC5uZXh0KGZhbHNlKTtcbiAgICB9XG4gIH1cblxuICBuZ0FmdGVyVmlld0luaXQoKSB7XG5cbiAgICB0aGlzLmhvdGtleXNTZXJ2aWNlLmFkZChuZXcgSG90a2V5KCdjdHJsK20nLCAoZXZlbnQ6IEtleWJvYXJkRXZlbnQpOiBib29sZWFuID0+IHtcbiAgICAgIHRoaXMubGVmdE5hdlRvZ2dsZSgpO1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH0pKTtcbiAgICB0aGlzLmhvdGtleXNTZXJ2aWNlLmFkZChuZXcgSG90a2V5KCdjdHJsK2FsdCtqJywgKGV2ZW50OiBLZXlib2FyZEV2ZW50KTogYm9vbGVhbiA9PiB7XG4gICAgICB0aGlzLnJpZ2h0TmF2VG9nZ2xlKCk7XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfSkpO1xuXG4gICAgdGhpcy5pc1ZlcmlmeVR5cGVTaGVsbEFwcGx5KCk7XG4gIH1cblxuICBwcml2YXRlIGdldEZhdkl0ZW0oY3VzdG9tRGF0YSkge1xuICAgIGNvbnN0IGRhdGEgPSBjdXN0b21EYXRhO1xuICAgIGNvbnN0IGxhc3RCcmVhZENydW1iID0gdGhpcy5icmVhZGNydW1iU2VydmljZS5icmVhZGNydW1ic1t0aGlzLmJyZWFkY3J1bWJTZXJ2aWNlLmJyZWFkY3J1bWJzLmxlbmd0aCAtIDFdO1xuICAgIGlmIChsYXN0QnJlYWRDcnVtYiAhPSBudWxsKSB7XG4gICAgICBjb25zdCB0aXRsZSA9IGxhc3RCcmVhZENydW1iLnRpdGxlOyAvLyB0aGlzLmJyZWFkY3J1bWJTZXJ2aWNlLmdldFRpdGxlRm9ybWF0dGVkKGRhdGEuYnJlYWRjcnVtYiwgdGhpcy5hY3RpdmF0ZWRSb3V0ZS5zbmFwc2hvdCk7XG4gICAgICByZXR1cm4ge2xpbms6IHRoaXMucm91dGVyLnVybCwgdGl0bGUsIGljb246IGRhdGEgPyBkYXRhLmljb24gOiBudWxsfTtcbiAgICB9XG4gIH1cblxuICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcbiAgICB0aGlzLnN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xuICB9XG5cbiAgbGVmdE5hdlRvZ2dsZSgpIHtcbiAgICB0aGlzLm5hdlNlcnZpY2UudG9nZ2xlTGVmdE5hdigpO1xuICB9XG5cbiAgbGVmdE5hdkNsb3NlKCkge1xuICAgIHRoaXMubmF2U2VydmljZS5jbG9zZUxlZnROYXYoKTtcbiAgfVxuXG4gIHJpZ2h0TmF2VG9nZ2xlKCkge1xuICAgIHRoaXMubmF2U2VydmljZS50b2dnbGVSaWdodE5hdigpO1xuICB9XG5cbiAgcmlnaHROYXZDbG9zZSgpIHtcbiAgICB0aGlzLm5hdlNlcnZpY2UuY2xvc2VSaWdodE5hdigpO1xuICB9XG5cbiAgbmF2c0Nsb3NlKCkge1xuICAgIGlmICh0aGlzLmxlZnRuYXYub3BlbmVkICYmICF0aGlzLmxlZnRuYXYucGlubmVkKSB7XG4gICAgICB0aGlzLm5hdlNlcnZpY2UudG9nZ2xlTGVmdE5hdigpO1xuICAgIH1cblxuICAgIGlmICh0aGlzLnJpZ2h0bmF2Lm9wZW5lZCAmJiAhdGhpcy5yaWdodG5hdi5waW5uZWQpIHtcbiAgICAgIHRoaXMubmF2U2VydmljZS50b2dnbGVSaWdodE5hdigpO1xuICAgIH1cbiAgfVxuXG4gIGluc3RhbGwoKSB7XG4gICAgdGhpcy51cGRhdGVJbmZvU2VydmljZS5pbnN0YWxsKCk7XG4gIH1cblxuICB1cGRhdGUoKSB7XG4gICAgdGhpcy51cGRhdGVTZXJ2aWNlLnVwZGF0ZSgpO1xuICB9XG5cbiAgcHVibGljIGlzVmVyaWZ5VHlwZVNoZWxsQXBwbHkoKSB7XG5cbiAgICB0aGlzLnNob3dGdWxsU2NycmVuLnN1YnNjcmliZSgoaXNTaG93RnVsbFNjcmVlbjogYm9vbGVhbikgPT4ge1xuICAgICAgaWYgKGlzU2hvd0Z1bGxTY3JlZW4pIHtcbiAgICAgICAgY29uc3QgZnNEb2NFbGVtID0gdGhpcy5kb2N1bWVudC5kb2N1bWVudEVsZW1lbnQgYXMgRnNEb2N1bWVudEVsZW1lbnQ7XG4gICAgICAgIC8vIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudCBhcyBGc0RvY3VtZW50RWxlbWVudDtcbiAgICAgICAgdGhpcy5vcGVuRnVsbHNjcmVlbihmc0RvY0VsZW0pO1xuICAgICAgfVxuICAgIH0pO1xuXG4gIH1cblxuICBwdWJsaWMgb3BlbkZ1bGxzY3JlZW4oZnNEb2NFbGVtOiBhbnkpOiB2b2lkIHtcbiAgICAvKmRlYnVnZ2VyOyovXG4gICAgaWYgKGZzRG9jRWxlbS5yZXF1ZXN0RnVsbHNjcmVlbikge1xuICAgICAgdGltZXIoNjAwKS5zdWJzY3JpYmUoKCkgPT4ge1xuICAgICAgICBmc0RvY0VsZW0ucmVxdWVzdEZ1bGxzY3JlZW4oKS5jYXRjaChlcnJvciA9PiBjb25zb2xlLmVycm9yKGVycm9yKSk7XG4gICAgICB9KTtcbiAgICB9IGVsc2UgaWYgKGZzRG9jRWxlbS5tc1JlcXVlc3RGdWxsc2NyZWVuKSB7XG4gICAgICB0cnkge1xuICAgICAgICB0aW1lcig2MDApLnN1YnNjcmliZSgoKSA9PiB7XG4gICAgICAgICAgZnNEb2NFbGVtLm1zUmVxdWVzdEZ1bGxzY3JlZW4oKS5jYXRjaChlcnJvciA9PiBjb25zb2xlLmVycm9yKGVycm9yKSk7XG4gICAgICAgIH0pO1xuICAgICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgICAgY29uc29sZS5lcnJvcihlcnJvcik7XG4gICAgICB9XG4gICAgfSBlbHNlIGlmIChmc0RvY0VsZW0ubW96UmVxdWVzdEZ1bGxTY3JlZW4pIHtcbiAgICAgIHRyeSB7XG4gICAgICAgIHRpbWVyKDYwMCkuc3Vic2NyaWJlKCgpID0+IHtcbiAgICAgICAgICBmc0RvY0VsZW0ubW96UmVxdWVzdEZ1bGxTY3JlZW4oKS5jYXRjaChlcnJvciA9PiBjb25zb2xlLmVycm9yKGVycm9yKSk7XG4gICAgICAgIH0pO1xuICAgICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgICAgY29uc29sZS5lcnJvcihlcnJvcik7XG4gICAgICB9XG4gICAgfSBlbHNlIGlmIChmc0RvY0VsZW0ud2Via2l0UmVxdWVzdEZ1bGxzY3JlZW4pIHtcbiAgICAgIHRyeSB7XG4gICAgICAgIHRpbWVyKDYwMCkuc3Vic2NyaWJlKCgpID0+IHtcbiAgICAgICAgICBmc0RvY0VsZW0ud2Via2l0UmVxdWVzdEZ1bGxzY3JlZW4oKS5jYXRjaChlcnJvciA9PiBjb25zb2xlLmVycm9yKGVycm9yKSk7XG4gICAgICAgIH0pO1xuICAgICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgICAgY29uc29sZS5lcnJvcihlcnJvcik7XG4gICAgICB9XG4gICAgfVxuICAgIHRpbWVyKDkwMCkuc3Vic2NyaWJlKCgpID0+IHtcbiAgICAgICh0aGlzLm15Q2xpY2tGdWxsc2NyZWVuLm5hdGl2ZUVsZW1lbnQgYXMgSFRNTEVsZW1lbnQpLmNsaWNrKCk7XG4gICAgfSk7XG4gIH1cblxuICBwcml2YXRlIGNsb3NlRnVsbHNjcmVlbigpOiB2b2lkIHtcbiAgICBpZiAodGhpcy5kb2N1bWVudC5leGl0RnVsbHNjcmVlbikge1xuICAgICAgdGhpcy5kb2N1bWVudC5leGl0RnVsbHNjcmVlbigpO1xuICAgIH0gZWxzZSBpZiAodGhpcy5kb2N1bWVudC5tb3pDYW5jZWxGdWxsU2NyZWVuKSB7XG4gICAgICAvKiBGaXJlZm94ICovXG4gICAgICB0aGlzLmRvY3VtZW50Lm1vekNhbmNlbEZ1bGxTY3JlZW4oKTtcbiAgICB9IGVsc2UgaWYgKHRoaXMuZG9jdW1lbnQud2Via2l0RXhpdEZ1bGxzY3JlZW4pIHtcbiAgICAgIC8qIENocm9tZSwgU2FmYXJpIGFuZCBPcGVyYSAqL1xuICAgICAgdGhpcy5kb2N1bWVudC53ZWJraXRFeGl0RnVsbHNjcmVlbigpO1xuICAgIH0gZWxzZSBpZiAodGhpcy5kb2N1bWVudC5tc0V4aXRGdWxsc2NyZWVuKSB7XG4gICAgICAvKiBJRS9FZGdlICovXG4gICAgICB0aGlzLmRvY3VtZW50Lm1zRXhpdEZ1bGxzY3JlZW4oKTtcbiAgICB9XG4gIH1cblxuICAvKioqIFJlZ2lvbiBSb3V0ZUxheW91dEFwcGxpY2F0aW9uXG4gICAqIE1vZG8gZGUgYXByZXNlbnRhw6fDo28gZGFzIHRlbGEgYXBsaWNhZG8gZGUgZm9ybWEgdG90YWxtZW50ZSBjb25maWd1csOhdmVsIGEgZm9ybWEgZ2VyYWwgZSBwb3Igcm90YS5cbiAgICogKioqL1xuICBwdWJsaWMgc2V0TGF5b3V0VHlwZUJ5Um91dGVFdmVudCgpIHtcbiAgICBjb25zdCBxdWVyeVR5cGUgPSB0aGlzLnJvdXRlci5yb3V0ZXJTdGF0ZS5yb290LnNuYXBzaG90LnF1ZXJ5UGFyYW1NYXAuZ2V0KCd0eXBlJyk7XG4gICAgY29uc3QgZGF0YVJvdGEgPSB0aGlzLmdldERhdGEoKTtcblxuICAgIGxldCB0eXBlRGVzZWphZG8gPSAnbm9ybWFsJztcbiAgICBpZiAocXVlcnlUeXBlKSB7XG4gICAgICAvLyB1c3VhcmlvIGluZm9ybW91IHZpYSB1cmwgKHRlbSBwcmlvcmlkYWRlIHNvYnJlIHRvZG9zKVxuICAgICAgdHlwZURlc2VqYWRvID0gcXVlcnlUeXBlO1xuICAgIH0gZWxzZSBpZiAoZGF0YVJvdGEgJiYgZGF0YVJvdGEuZGVmYXVsdFR5cGUpIHtcbiAgICAgIC8vIGVzdGEgY29uZmlndXJhZG8gbmEgcm90YSAodGVtIHByaW9yaWRhZGUgc29tZW50ZSBzb2JyZSBvIGdsb2JhbCAtIHNvYnJlc2NyaXRhKVxuICAgICAgdHlwZURlc2VqYWRvID0gZGF0YVJvdGEuZGVmYXVsdFR5cGU7XG4gICAgfSBlbHNlIHtcbiAgICAgIHR5cGVEZXNlamFkbyA9IHRoaXMuZGVmYXVsdFR5cGU7XG4gICAgICAvLyBidXNjYSBvIGdsb2JhbFxuICAgIH1cblxuICAgIGlmICh0eXBlRGVzZWphZG8gPT09ICdub3NoZWxsJykge1xuICAgICAgdGhpcy5uYXZTZXJ2aWNlLnRvZ2dsZUxlZnROYXYoKTtcbiAgICB9XG5cbiAgICBjb25zdCBkZWZhdWx0VHlwZVJvdXRlID0gKGRhdGFSb3RhICYmIGRhdGFSb3RhLmRlZmF1bHRUeXBlKSA/IGRhdGFSb3RhLmRlZmF1bHRUeXBlIDogdGhpcy5kZWZhdWx0VHlwZTtcbiAgICBpZiAoZGF0YVJvdGEgJiYgZGF0YVJvdGEuYWxsb3dlZFR5cGVzICYmIGRhdGFSb3RhLmFsbG93ZWRUeXBlcy5sZW5ndGggPiAwKSB7XG4gICAgICAvLyBleGlzdGUgY29uZmlndXJhY2FvIGRlIHRpcG9zIHBlcm1pdGlkb3MgbmEgcm90YVxuICAgICAgY29uc3QgZWhQZXJtaXRpZG9QZWxhUm90YSA9IGRhdGFSb3RhLmFsbG93ZWRUeXBlcy5zb21lKHR5cGUgPT4gdHlwZSA9PT0gdHlwZURlc2VqYWRvKTtcbiAgICAgIGlmIChlaFBlcm1pdGlkb1BlbGFSb3RhKSB7XG4gICAgICAgIHRoaXMubGF5b3V0U2VydmljZS5zZXRUeXBlKExheW91dFR5cGVbdHlwZURlc2VqYWRvXSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBjb25zb2xlLmVycm9yKGBPIHBhcmFtZXRybyAke3R5cGVEZXNlamFkb30gbsOjbyBlc3TDoSBkZWZpbmlkbyBjb21vIHBlcm1pdGlkbywgZmF2b3IgdmVyaWZpY2FyIWApO1xuICAgICAgICB0aGlzLmxheW91dFNlcnZpY2Uuc2V0VHlwZShMYXlvdXRUeXBlW2RlZmF1bHRUeXBlUm91dGVdKTtcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgLy8gbsOjbyBleGlzdGUgY29uZmlndXJhw6fDo28gZGUgdGlwb3MgcGVybWl0aWRvcyBuYSByb3RhIGRldm8gZW50w6NvIHZlciBkbyBjb21wb25lbnRlIGxheW91dCAoZ2xvYmFsIGRhIGFwbGljYcOnw6NvKVxuICAgICAgY29uc3QgYWxsb3dlZFR5cGVzTGF5b3V0ID0gdGhpcy5hbGxvd2VkVHlwZXM7XG4gICAgICBjb25zdCBlaFBlcm1pdGlkb0dsb2JhbG1lbnRlID0gYWxsb3dlZFR5cGVzTGF5b3V0LnNvbWUodHlwZSA9PiB0eXBlID09PSB0eXBlRGVzZWphZG8pO1xuICAgICAgaWYgKGVoUGVybWl0aWRvR2xvYmFsbWVudGUpIHtcbiAgICAgICAgdGhpcy5sYXlvdXRTZXJ2aWNlLnNldFR5cGUoTGF5b3V0VHlwZVt0eXBlRGVzZWphZG9dKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGNvbnNvbGUuZXJyb3IoYE8gcGFyYW1ldHJvICR7dHlwZURlc2VqYWRvfSBuw6NvIGVzdMOhIGRlZmluaWRvIGNvbW8gcGVybWl0aWRvLCBmYXZvciB2ZXJpZmljYXIhYCk7XG4gICAgICAgIHRoaXMubGF5b3V0U2VydmljZS5zZXRUeXBlKExheW91dFR5cGVbZGVmYXVsdFR5cGVSb3V0ZV0pO1xuICAgICAgfVxuICAgIH1cbiAgICBpZiAoKHR5cGVEZXNlamFkbyA9PT0gJ25vcm1hbCcgfHwgZGVmYXVsdFR5cGVSb3V0ZSA9PT0gJ25vcm1hbCcpICYmIHRoaXMuZG9jdW1lbnQuZnVsbHNjcmVlbkVsZW1lbnQpIHtcbiAgICAgIHRoaXMuY2xvc2VGdWxsc2NyZWVuKCk7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBnZXREYXRhKCkge1xuICAgIGxldCByb3V0ZSA9IHRoaXMucm91dGVyLnJvdXRlclN0YXRlLnJvb3Quc25hcHNob3Q7XG4gICAgd2hpbGUgKHJvdXRlLmZpcnN0Q2hpbGQgIT0gbnVsbCkge1xuICAgICAgcm91dGUgPSByb3V0ZS5maXJzdENoaWxkO1xuICAgICAgaWYgKHJvdXRlLnJvdXRlQ29uZmlnID09PSBudWxsKSB7XG4gICAgICAgIGNvbnRpbnVlO1xuICAgICAgfVxuICAgICAgaWYgKCFyb3V0ZS5yb3V0ZUNvbmZpZy5wYXRoKSB7XG4gICAgICAgIGNvbnRpbnVlO1xuICAgICAgfVxuICAgICAgaWYgKCFyb3V0ZS5kYXRhLmRlZmF1bHRUeXBlKSB7XG4gICAgICAgIGNvbnRpbnVlO1xuICAgICAgfVxuICAgICAgcmV0dXJuIHJvdXRlLmRhdGEgYXMgeyBkZWZhdWx0VHlwZTogc3RyaW5nLCBhbGxvd2VkVHlwZXM6IHN0cmluZ1tdIH07XG4gICAgfVxuICB9XG5cbiAgLyoqKiBFbmRSZWdpb24gUm91dGVMYXlvdXRBcHBsaWNhdGlvbiAqKiovXG59XG4iXX0=