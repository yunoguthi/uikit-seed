/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { LayoutType } from './layout-type.model';
var LayoutService = /** @class */ (function () {
    function LayoutService() {
        this.isMobileSubject$ = new BehaviorSubject(false);
        this.isMobile$ = this.isMobileSubject$.asObservable();
        this.hiddenShellSubject$ = new BehaviorSubject(false);
        this.hiddenShell$ = this.hiddenShellSubject$.asObservable();
        this.showShellSubject$ = new BehaviorSubject(true);
        this.showShell$ = this.showShellSubject$.asObservable();
        this.showBreadcrumbSubject$ = new BehaviorSubject(true);
        this.showBreadcrumb$ = this.showBreadcrumbSubject$.asObservable();
        this.showFullScreenSubject$ = new BehaviorSubject(false);
        this.showFullScreen$ = this.showFullScreenSubject$.asObservable();
        this.isFixedSubject$ = new BehaviorSubject(false);
        this.isFixed$ = this.isFixedSubject$.asObservable();
        this.menuItemsSubject$ = new BehaviorSubject([]);
        this.menuItems$ = this.menuItemsSubject$.asObservable();
        this.notificacoesSubject$ = new BehaviorSubject([]);
        this.notificacoes$ = this.notificacoesSubject$.asObservable();
        this.menuItemsFlattened$ = this.menuItems$.pipe(map((/**
         * @param {?} menuItems
         * @return {?}
         */
        function (menuItems) {
            /** @type {?} */
            var menuItemsFlattened = (/** @type {?} */ ([]));
            /** @type {?} */
            var flattenObject = (/**
             * @param {?} obj
             * @return {?}
             */
            function (obj) {
                if (obj.children) {
                    obj.children.forEach((/**
                     * @param {?} value
                     * @return {?}
                     */
                    function (value) {
                        flattenObject(value);
                    }));
                }
                menuItemsFlattened.push(obj);
            });
            menuItems.forEach(flattenObject);
            return menuItemsFlattened;
        })));
    }
    /**
     * @param {?} notificacoesModel
     * @return {?}
     */
    LayoutService.prototype.setNotificacoes = /**
     * @param {?} notificacoesModel
     * @return {?}
     */
    function (notificacoesModel) {
        this.notificacoesSubject$.next(notificacoesModel);
    };
    /**
     * @param {?} fixed
     * @return {?}
     */
    LayoutService.prototype.setFixed = /**
     * @param {?} fixed
     * @return {?}
     */
    function (fixed) {
        this.isFixedSubject$.next(fixed);
    };
    /**
     * @param {?} menuItems
     * @return {?}
     */
    LayoutService.prototype.setMenuItems = /**
     * @param {?} menuItems
     * @return {?}
     */
    function (menuItems) {
        this.menuItemsSubject$.next(menuItems);
    };
    /**
     * @return {?}
     */
    LayoutService.prototype.toggleMobile = /**
     * @return {?}
     */
    function () {
        this.isMobileSubject$.next(!this.isMobileSubject$.getValue());
    };
    /**
     * @private
     * @param {?} noShell
     * @return {?}
     */
    LayoutService.prototype.noShell = /**
     * @private
     * @param {?} noShell
     * @return {?}
     */
    function (noShell) {
        this.hiddenShellSubject$.next(noShell);
    };
    /**
     * @private
     * @param {?} mostrarShell
     * @return {?}
     */
    LayoutService.prototype.showShell = /**
     * @private
     * @param {?} mostrarShell
     * @return {?}
     */
    function (mostrarShell) {
        this.showShellSubject$.next(mostrarShell);
    };
    /**
     * @private
     * @param {?} mostrarBreadcrumb
     * @return {?}
     */
    LayoutService.prototype.showBreadcrumb = /**
     * @private
     * @param {?} mostrarBreadcrumb
     * @return {?}
     */
    function (mostrarBreadcrumb) {
        this.showBreadcrumbSubject$.next(mostrarBreadcrumb);
    };
    /**
     * @private
     * @param {?} mostraFullScreen
     * @return {?}
     */
    LayoutService.prototype.showFullScreen = /**
     * @private
     * @param {?} mostraFullScreen
     * @return {?}
     */
    function (mostraFullScreen) {
        this.showFullScreenSubject$.next(mostraFullScreen);
    };
    /**
     * @private
     * @return {?}
     */
    LayoutService.prototype.noShellShowBreadFullScreen = /**
     * @private
     * @return {?}
     */
    function () {
        this.noShell(false);
        this.showFullScreen(true);
    };
    /**
     * @private
     * @return {?}
     */
    LayoutService.prototype.noShellNoBreadcrumb = /**
     * @private
     * @return {?}
     */
    function () {
        this.noShell(false);
        this.showBreadcrumb(false);
    };
    /**
     * @private
     * @return {?}
     */
    LayoutService.prototype.noShellNoBreadcrumbFullScreen = /**
     * @private
     * @return {?}
     */
    function () {
        this.noShellShowBreadFullScreen();
        this.showBreadcrumb(true);
    };
    /**
     * @param {?} layoutType
     * @return {?}
     */
    LayoutService.prototype.setType = /**
     * @param {?} layoutType
     * @return {?}
     */
    function (layoutType) {
        // const el = document.body;
        if (LayoutType.fullscreen === layoutType) {
            this.showFullScreen(true);
            this.noShell(true);
            this.showBreadcrumb(true);
        }
        else if (LayoutType.noshell === layoutType) {
            this.noShell(false);
            this.showBreadcrumb(true);
        }
        else if (LayoutType.noshellfullscreen === layoutType) {
            this.noShellShowBreadFullScreen();
        }
        else if (LayoutType.noshellnobreadcrumb === layoutType) {
            this.noShellNoBreadcrumb();
        }
        else if (LayoutType.noshellnobreadcrumbfullscreen === layoutType) {
            this.noShellNoBreadcrumbFullScreen();
        }
        else {
            this.noShell(true);
            this.showBreadcrumb(true);
            this.showShell(true);
        }
    };
    LayoutService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    LayoutService.ctorParameters = function () { return []; };
    return LayoutService;
}());
export { LayoutService };
if (false) {
    /** @type {?} */
    LayoutService.prototype.isMobileSubject$;
    /** @type {?} */
    LayoutService.prototype.isMobile$;
    /**
     * @type {?}
     * @protected
     */
    LayoutService.prototype.hiddenShellSubject$;
    /** @type {?} */
    LayoutService.prototype.hiddenShell$;
    /**
     * @type {?}
     * @protected
     */
    LayoutService.prototype.showShellSubject$;
    /** @type {?} */
    LayoutService.prototype.showShell$;
    /**
     * @type {?}
     * @protected
     */
    LayoutService.prototype.showBreadcrumbSubject$;
    /** @type {?} */
    LayoutService.prototype.showBreadcrumb$;
    /**
     * @type {?}
     * @protected
     */
    LayoutService.prototype.showFullScreenSubject$;
    /** @type {?} */
    LayoutService.prototype.showFullScreen$;
    /**
     * @type {?}
     * @private
     */
    LayoutService.prototype.isFixedSubject$;
    /** @type {?} */
    LayoutService.prototype.isFixed$;
    /**
     * @type {?}
     * @private
     */
    LayoutService.prototype.menuItemsSubject$;
    /** @type {?} */
    LayoutService.prototype.menuItems$;
    /**
     * @type {?}
     * @private
     */
    LayoutService.prototype.notificacoesSubject$;
    /** @type {?} */
    LayoutService.prototype.notificacoes$;
    /** @type {?} */
    LayoutService.prototype.menuItemsFlattened$;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGF5b3V0LnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL2xheW91dC9sYXlvdXQuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFVBQVUsRUFBUyxNQUFNLGVBQWUsQ0FBQztBQUVqRCxPQUFPLEVBQUMsR0FBRyxFQUFDLE1BQU0sZ0JBQWdCLENBQUM7QUFDbkMsT0FBTyxFQUFDLGVBQWUsRUFBQyxNQUFNLE1BQU0sQ0FBQztBQUVyQyxPQUFPLEVBQUMsVUFBVSxFQUFDLE1BQU0scUJBQXFCLENBQUM7QUFFL0M7SUFrQkU7UUFmQSxxQkFBZ0IsR0FBRyxJQUFJLGVBQWUsQ0FBVSxLQUFLLENBQUMsQ0FBQztRQUNoRCxjQUFTLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFlBQVksRUFBRSxDQUFDO1FBRTlDLHdCQUFtQixHQUFHLElBQUksZUFBZSxDQUFVLEtBQUssQ0FBQyxDQUFDO1FBQzdELGlCQUFZLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFlBQVksRUFBRSxDQUFDO1FBRXBELHNCQUFpQixHQUFHLElBQUksZUFBZSxDQUFVLElBQUksQ0FBQyxDQUFDO1FBQzFELGVBQVUsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsWUFBWSxFQUFFLENBQUM7UUFFaEQsMkJBQXNCLEdBQUcsSUFBSSxlQUFlLENBQVUsSUFBSSxDQUFDLENBQUM7UUFDL0Qsb0JBQWUsR0FBRyxJQUFJLENBQUMsc0JBQXNCLENBQUMsWUFBWSxFQUFFLENBQUM7UUFFMUQsMkJBQXNCLEdBQUcsSUFBSSxlQUFlLENBQVUsS0FBSyxDQUFDLENBQUM7UUFDaEUsb0JBQWUsR0FBRyxJQUFJLENBQUMsc0JBQXNCLENBQUMsWUFBWSxFQUFFLENBQUM7UUFLNUQsb0JBQWUsR0FBRyxJQUFJLGVBQWUsQ0FBVSxLQUFLLENBQUMsQ0FBQztRQUN2RCxhQUFRLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUU5QyxzQkFBaUIsR0FBRyxJQUFJLGVBQWUsQ0FBYSxFQUFFLENBQUMsQ0FBQztRQUN6RCxlQUFVLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFlBQVksRUFBRSxDQUFDO1FBRWxELHlCQUFvQixHQUFHLElBQUksZUFBZSxDQUFnQixFQUFFLENBQUMsQ0FBQztRQUMvRCxrQkFBYSxHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUV6RCx3QkFBbUIsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHOzs7O1FBQUMsVUFBQSxTQUFTOztnQkFDdkQsa0JBQWtCLEdBQUcsbUJBQUEsRUFBRSxFQUFjOztnQkFDckMsYUFBYTs7OztZQUFHLFVBQUMsR0FBYTtnQkFDbEMsSUFBSSxHQUFHLENBQUMsUUFBUSxFQUFFO29CQUNoQixHQUFHLENBQUMsUUFBUSxDQUFDLE9BQU87Ozs7b0JBQUMsVUFBQyxLQUFLO3dCQUN6QixhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQ3ZCLENBQUMsRUFBQyxDQUFDO2lCQUNKO2dCQUNELGtCQUFrQixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUMvQixDQUFDLENBQUE7WUFDRCxTQUFTLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQ2pDLE9BQU8sa0JBQWtCLENBQUM7UUFDNUIsQ0FBQyxFQUFDLENBQUMsQ0FBQztJQXZCSixDQUFDOzs7OztJQXlCRCx1Q0FBZTs7OztJQUFmLFVBQWdCLGlCQUFnQztRQUM5QyxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7SUFDcEQsQ0FBQzs7Ozs7SUFFTSxnQ0FBUTs7OztJQUFmLFVBQWdCLEtBQWM7UUFDNUIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDbkMsQ0FBQzs7Ozs7SUFFRCxvQ0FBWTs7OztJQUFaLFVBQWEsU0FBcUI7UUFDaEMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUN6QyxDQUFDOzs7O0lBRUQsb0NBQVk7OztJQUFaO1FBQ0UsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO0lBQ2hFLENBQUM7Ozs7OztJQUVPLCtCQUFPOzs7OztJQUFmLFVBQWdCLE9BQWdCO1FBQzlCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDekMsQ0FBQzs7Ozs7O0lBRU8saUNBQVM7Ozs7O0lBQWpCLFVBQWtCLFlBQXFCO1FBQ3JDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDNUMsQ0FBQzs7Ozs7O0lBRU8sc0NBQWM7Ozs7O0lBQXRCLFVBQXVCLGlCQUEwQjtRQUMvQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7SUFDdEQsQ0FBQzs7Ozs7O0lBRU8sc0NBQWM7Ozs7O0lBQXRCLFVBQXVCLGdCQUF5QjtRQUM5QyxJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7SUFDckQsQ0FBQzs7Ozs7SUFFTyxrREFBMEI7Ozs7SUFBbEM7UUFDRSxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3BCLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDNUIsQ0FBQzs7Ozs7SUFFTywyQ0FBbUI7Ozs7SUFBM0I7UUFDRSxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3BCLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDN0IsQ0FBQzs7Ozs7SUFFTyxxREFBNkI7Ozs7SUFBckM7UUFDRSxJQUFJLENBQUMsMEJBQTBCLEVBQUUsQ0FBQztRQUNsQyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzVCLENBQUM7Ozs7O0lBRU0sK0JBQU87Ozs7SUFBZCxVQUFlLFVBQXNCO1FBQ25DLDRCQUE0QjtRQUM1QixJQUFJLFVBQVUsQ0FBQyxVQUFVLEtBQUssVUFBVSxFQUFFO1lBQ3hDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDMUIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNuQixJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQzNCO2FBQU0sSUFBSSxVQUFVLENBQUMsT0FBTyxLQUFLLFVBQVUsRUFBRTtZQUM1QyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3BCLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDM0I7YUFBTSxJQUFJLFVBQVUsQ0FBQyxpQkFBaUIsS0FBSyxVQUFVLEVBQUU7WUFDdEQsSUFBSSxDQUFDLDBCQUEwQixFQUFFLENBQUM7U0FDbkM7YUFBTSxJQUFJLFVBQVUsQ0FBQyxtQkFBbUIsS0FBSyxVQUFVLEVBQUU7WUFDeEQsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7U0FDNUI7YUFBTSxJQUFJLFVBQVUsQ0FBQyw2QkFBNkIsS0FBSyxVQUFVLEVBQUU7WUFDbEUsSUFBSSxDQUFDLDZCQUE2QixFQUFFLENBQUM7U0FDdEM7YUFBTTtZQUNMLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDbkIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUMxQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3RCO0lBQ0gsQ0FBQzs7Z0JBL0dGLFVBQVU7Ozs7SUFnSFgsb0JBQUM7Q0FBQSxBQWhIRCxJQWdIQztTQS9HWSxhQUFhOzs7SUFFeEIseUNBQXVEOztJQUN2RCxrQ0FBd0Q7Ozs7O0lBRXhELDRDQUFvRTs7SUFDcEUscUNBQThEOzs7OztJQUU5RCwwQ0FBaUU7O0lBQ2pFLG1DQUEwRDs7Ozs7SUFFMUQsK0NBQXNFOztJQUN0RSx3Q0FBb0U7Ozs7O0lBRXBFLCtDQUF1RTs7SUFDdkUsd0NBQW9FOzs7OztJQUtwRSx3Q0FBOEQ7O0lBQzlELGlDQUFzRDs7Ozs7SUFFdEQsMENBQWdFOztJQUNoRSxtQ0FBMEQ7Ozs7O0lBRTFELDZDQUFzRTs7SUFDdEUsc0NBQWdFOztJQUVoRSw0Q0FZSSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SW5qZWN0YWJsZSwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7TWVudUl0ZW19IGZyb20gJy4vbmF2L21lbnUvbWVudS1pdGVtL21lbnUtaXRlbS5tb2RlbCc7XG5pbXBvcnQge21hcH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHtCZWhhdmlvclN1YmplY3R9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHtOb3RpZmljYWNhb30gZnJvbSAnLi9oZWFkZXIvbm90aWZpY2F0aW9uL25vdGlmaWNhY2FvL25vdGlmaWNhY2FvLm1vZGVsJztcbmltcG9ydCB7TGF5b3V0VHlwZX0gZnJvbSAnLi9sYXlvdXQtdHlwZS5tb2RlbCc7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBMYXlvdXRTZXJ2aWNlIHtcblxuICBpc01vYmlsZVN1YmplY3QkID0gbmV3IEJlaGF2aW9yU3ViamVjdDxib29sZWFuPihmYWxzZSk7XG4gIHB1YmxpYyBpc01vYmlsZSQgPSB0aGlzLmlzTW9iaWxlU3ViamVjdCQuYXNPYnNlcnZhYmxlKCk7XG5cbiAgcHJvdGVjdGVkIGhpZGRlblNoZWxsU3ViamVjdCQgPSBuZXcgQmVoYXZpb3JTdWJqZWN0PGJvb2xlYW4+KGZhbHNlKTtcbiAgcHVibGljIGhpZGRlblNoZWxsJCA9IHRoaXMuaGlkZGVuU2hlbGxTdWJqZWN0JC5hc09ic2VydmFibGUoKTtcblxuICBwcm90ZWN0ZWQgc2hvd1NoZWxsU3ViamVjdCQgPSBuZXcgQmVoYXZpb3JTdWJqZWN0PGJvb2xlYW4+KHRydWUpO1xuICBwdWJsaWMgc2hvd1NoZWxsJCA9IHRoaXMuc2hvd1NoZWxsU3ViamVjdCQuYXNPYnNlcnZhYmxlKCk7XG5cbiAgcHJvdGVjdGVkIHNob3dCcmVhZGNydW1iU3ViamVjdCQgPSBuZXcgQmVoYXZpb3JTdWJqZWN0PGJvb2xlYW4+KHRydWUpO1xuICBwdWJsaWMgc2hvd0JyZWFkY3J1bWIkID0gdGhpcy5zaG93QnJlYWRjcnVtYlN1YmplY3QkLmFzT2JzZXJ2YWJsZSgpO1xuXG4gIHByb3RlY3RlZCBzaG93RnVsbFNjcmVlblN1YmplY3QkID0gbmV3IEJlaGF2aW9yU3ViamVjdDxib29sZWFuPihmYWxzZSk7XG4gIHB1YmxpYyBzaG93RnVsbFNjcmVlbiQgPSB0aGlzLnNob3dGdWxsU2NyZWVuU3ViamVjdCQuYXNPYnNlcnZhYmxlKCk7XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gIH1cblxuICBwcml2YXRlIGlzRml4ZWRTdWJqZWN0JCA9IG5ldyBCZWhhdmlvclN1YmplY3Q8Ym9vbGVhbj4oZmFsc2UpO1xuICBwdWJsaWMgaXNGaXhlZCQgPSB0aGlzLmlzRml4ZWRTdWJqZWN0JC5hc09ic2VydmFibGUoKTtcblxuICBwcml2YXRlIG1lbnVJdGVtc1N1YmplY3QkID0gbmV3IEJlaGF2aW9yU3ViamVjdDxNZW51SXRlbVtdPihbXSk7XG4gIHB1YmxpYyBtZW51SXRlbXMkID0gdGhpcy5tZW51SXRlbXNTdWJqZWN0JC5hc09ic2VydmFibGUoKTtcblxuICBwcml2YXRlIG5vdGlmaWNhY29lc1N1YmplY3QkID0gbmV3IEJlaGF2aW9yU3ViamVjdDxOb3RpZmljYWNhb1tdPihbXSk7XG4gIHB1YmxpYyBub3RpZmljYWNvZXMkID0gdGhpcy5ub3RpZmljYWNvZXNTdWJqZWN0JC5hc09ic2VydmFibGUoKTtcblxuICBwdWJsaWMgbWVudUl0ZW1zRmxhdHRlbmVkJCA9IHRoaXMubWVudUl0ZW1zJC5waXBlKG1hcChtZW51SXRlbXMgPT4ge1xuICAgIGNvbnN0IG1lbnVJdGVtc0ZsYXR0ZW5lZCA9IFtdIGFzIE1lbnVJdGVtW107XG4gICAgY29uc3QgZmxhdHRlbk9iamVjdCA9IChvYmo6IE1lbnVJdGVtKSA9PiB7XG4gICAgICBpZiAob2JqLmNoaWxkcmVuKSB7XG4gICAgICAgIG9iai5jaGlsZHJlbi5mb3JFYWNoKCh2YWx1ZSkgPT4ge1xuICAgICAgICAgIGZsYXR0ZW5PYmplY3QodmFsdWUpO1xuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICAgIG1lbnVJdGVtc0ZsYXR0ZW5lZC5wdXNoKG9iaik7XG4gICAgfTtcbiAgICBtZW51SXRlbXMuZm9yRWFjaChmbGF0dGVuT2JqZWN0KTtcbiAgICByZXR1cm4gbWVudUl0ZW1zRmxhdHRlbmVkO1xuICB9KSk7XG5cbiAgc2V0Tm90aWZpY2Fjb2VzKG5vdGlmaWNhY29lc01vZGVsOiBOb3RpZmljYWNhb1tdKTogYW55IHtcbiAgICB0aGlzLm5vdGlmaWNhY29lc1N1YmplY3QkLm5leHQobm90aWZpY2Fjb2VzTW9kZWwpO1xuICB9XG5cbiAgcHVibGljIHNldEZpeGVkKGZpeGVkOiBib29sZWFuKSB7XG4gICAgdGhpcy5pc0ZpeGVkU3ViamVjdCQubmV4dChmaXhlZCk7XG4gIH1cblxuICBzZXRNZW51SXRlbXMobWVudUl0ZW1zOiBNZW51SXRlbVtdKSB7XG4gICAgdGhpcy5tZW51SXRlbXNTdWJqZWN0JC5uZXh0KG1lbnVJdGVtcyk7XG4gIH1cblxuICB0b2dnbGVNb2JpbGUoKSB7XG4gICAgdGhpcy5pc01vYmlsZVN1YmplY3QkLm5leHQoIXRoaXMuaXNNb2JpbGVTdWJqZWN0JC5nZXRWYWx1ZSgpKTtcbiAgfVxuXG4gIHByaXZhdGUgbm9TaGVsbChub1NoZWxsOiBib29sZWFuKSB7XG4gICAgdGhpcy5oaWRkZW5TaGVsbFN1YmplY3QkLm5leHQobm9TaGVsbCk7XG4gIH1cblxuICBwcml2YXRlIHNob3dTaGVsbChtb3N0cmFyU2hlbGw6IGJvb2xlYW4pIHtcbiAgICB0aGlzLnNob3dTaGVsbFN1YmplY3QkLm5leHQobW9zdHJhclNoZWxsKTtcbiAgfVxuXG4gIHByaXZhdGUgc2hvd0JyZWFkY3J1bWIobW9zdHJhckJyZWFkY3J1bWI6IGJvb2xlYW4pIHtcbiAgICB0aGlzLnNob3dCcmVhZGNydW1iU3ViamVjdCQubmV4dChtb3N0cmFyQnJlYWRjcnVtYik7XG4gIH1cblxuICBwcml2YXRlIHNob3dGdWxsU2NyZWVuKG1vc3RyYUZ1bGxTY3JlZW46IGJvb2xlYW4pIHtcbiAgICB0aGlzLnNob3dGdWxsU2NyZWVuU3ViamVjdCQubmV4dChtb3N0cmFGdWxsU2NyZWVuKTtcbiAgfVxuXG4gIHByaXZhdGUgbm9TaGVsbFNob3dCcmVhZEZ1bGxTY3JlZW4oKSB7XG4gICAgdGhpcy5ub1NoZWxsKGZhbHNlKTtcbiAgICB0aGlzLnNob3dGdWxsU2NyZWVuKHRydWUpO1xuICB9XG5cbiAgcHJpdmF0ZSBub1NoZWxsTm9CcmVhZGNydW1iKCkge1xuICAgIHRoaXMubm9TaGVsbChmYWxzZSk7XG4gICAgdGhpcy5zaG93QnJlYWRjcnVtYihmYWxzZSk7XG4gIH1cblxuICBwcml2YXRlIG5vU2hlbGxOb0JyZWFkY3J1bWJGdWxsU2NyZWVuKCkge1xuICAgIHRoaXMubm9TaGVsbFNob3dCcmVhZEZ1bGxTY3JlZW4oKTtcbiAgICB0aGlzLnNob3dCcmVhZGNydW1iKHRydWUpO1xuICB9XG5cbiAgcHVibGljIHNldFR5cGUobGF5b3V0VHlwZTogTGF5b3V0VHlwZSk6IHZvaWQge1xuICAgIC8vIGNvbnN0IGVsID0gZG9jdW1lbnQuYm9keTtcbiAgICBpZiAoTGF5b3V0VHlwZS5mdWxsc2NyZWVuID09PSBsYXlvdXRUeXBlKSB7XG4gICAgICB0aGlzLnNob3dGdWxsU2NyZWVuKHRydWUpO1xuICAgICAgdGhpcy5ub1NoZWxsKHRydWUpO1xuICAgICAgdGhpcy5zaG93QnJlYWRjcnVtYih0cnVlKTtcbiAgICB9IGVsc2UgaWYgKExheW91dFR5cGUubm9zaGVsbCA9PT0gbGF5b3V0VHlwZSkge1xuICAgICAgdGhpcy5ub1NoZWxsKGZhbHNlKTtcbiAgICAgIHRoaXMuc2hvd0JyZWFkY3J1bWIodHJ1ZSk7XG4gICAgfSBlbHNlIGlmIChMYXlvdXRUeXBlLm5vc2hlbGxmdWxsc2NyZWVuID09PSBsYXlvdXRUeXBlKSB7XG4gICAgICB0aGlzLm5vU2hlbGxTaG93QnJlYWRGdWxsU2NyZWVuKCk7XG4gICAgfSBlbHNlIGlmIChMYXlvdXRUeXBlLm5vc2hlbGxub2JyZWFkY3J1bWIgPT09IGxheW91dFR5cGUpIHtcbiAgICAgIHRoaXMubm9TaGVsbE5vQnJlYWRjcnVtYigpO1xuICAgIH0gZWxzZSBpZiAoTGF5b3V0VHlwZS5ub3NoZWxsbm9icmVhZGNydW1iZnVsbHNjcmVlbiA9PT0gbGF5b3V0VHlwZSkge1xuICAgICAgdGhpcy5ub1NoZWxsTm9CcmVhZGNydW1iRnVsbFNjcmVlbigpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLm5vU2hlbGwodHJ1ZSk7XG4gICAgICB0aGlzLnNob3dCcmVhZGNydW1iKHRydWUpO1xuICAgICAgdGhpcy5zaG93U2hlbGwodHJ1ZSk7XG4gICAgfVxuICB9XG59XG4iXX0=