/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ToastComponent } from './toast.component';
import { timer } from 'rxjs';
import * as i0 from "@angular/core";
import * as i1 from "ngx-toastr";
var ToastService = /** @class */ (function () {
    function ToastService(toastrService) {
        this.toastrService = toastrService;
        this.defaultConfig = {
            timeOut: 10000,
            toastComponent: ToastComponent,
            progressBar: true,
            positionClass: 'toast-bottom-right',
            extendedTimeOut: 5000,
        };
    }
    /**
     * @param {?} message
     * @param {?=} title
     * @param {?=} objectAction
     * @return {?}
     */
    ToastService.prototype.success = /**
     * @param {?} message
     * @param {?=} title
     * @param {?=} objectAction
     * @return {?}
     */
    function (message, title, objectAction) {
        /** @type {?} */
        var config = this.applyConfig({ message: message, title: title, objectAction: objectAction });
        return this.toastrService.success(config.message, config.title, config);
    };
    /**
     * @param {?} message
     * @param {?} error
     * @param {?=} title
     * @param {?=} objectAction
     * @return {?}
     */
    ToastService.prototype.error = /**
     * @param {?} message
     * @param {?} error
     * @param {?=} title
     * @param {?=} objectAction
     * @return {?}
     */
    function (message, error, title, objectAction) {
        /** @type {?} */
        var config = this.applyConfig({
            message: message,
            title: title,
            error: error,
            objectAction: objectAction,
            disableTimeOut: true,
            preventDuplicates: true,
            progressBar: false,
            autoDismiss: false,
            positionClass: 'toast-center-center',
            tapToDismiss: false,
            closeButton: true
        });
        return this.toastrService.error(config.message, config.title, config);
    };
    /**
     * @param {?} message
     * @param {?=} title
     * @param {?=} objectAction
     * @return {?}
     */
    ToastService.prototype.info = /**
     * @param {?} message
     * @param {?=} title
     * @param {?=} objectAction
     * @return {?}
     */
    function (message, title, objectAction) {
        /** @type {?} */
        var disableTimeOut = objectAction != null;
        /** @type {?} */
        var config = this.applyConfig({ message: message, title: title, objectAction: objectAction, disableTimeOut: disableTimeOut });
        return this.toastrService.info(config.message, config.title, config);
    };
    /**
     * @param {?} message
     * @param {?=} title
     * @param {?=} objectAction
     * @return {?}
     */
    ToastService.prototype.warning = /**
     * @param {?} message
     * @param {?=} title
     * @param {?=} objectAction
     * @return {?}
     */
    function (message, title, objectAction) {
        /** @type {?} */
        var disableTimeOut = objectAction != null;
        /** @type {?} */
        var config = this.applyConfig({ message: message, title: title, objectAction: objectAction, disableTimeOut: disableTimeOut });
        return this.toastrService.warning(config.message, config.title, config);
    };
    /**
     * @param {?} isLoading$
     * @param {?=} message
     * @param {?=} title
     * @param {?=} objectAction
     * @return {?}
     */
    ToastService.prototype.loading = /**
     * @param {?} isLoading$
     * @param {?=} message
     * @param {?=} title
     * @param {?=} objectAction
     * @return {?}
     */
    function (isLoading$, message, title, objectAction) {
        var _this = this;
        if (message === void 0) { message = 'Carregando...'; }
        if (this.loadingSubscription && this.loadingSubscription.closed === false) {
            if (this.loadingToaster && this.loadingToaster.toastRef) {
                try {
                    this.loadingToaster.toastRef.close();
                }
                catch (error) {
                }
            }
            this.loadingSubscription.unsubscribe();
        }
        this.loadingSubscription = isLoading$.subscribe((/**
         * @param {?} isLoading
         * @return {?}
         */
        function (isLoading) {
            timer().subscribe((/**
             * @return {?}
             */
            function () {
                if (isLoading) {
                    /** @type {?} */
                    var config = _this.applyConfig({
                        message: message === '' ? 'Carregando...' : message,
                        title: title,
                        objectAction: objectAction,
                        disableTimeOut: true,
                        preventDuplicates: true,
                        progressBar: false,
                        autoDismiss: false,
                        positionClass: 'toast-top-center',
                        tapToDismiss: false
                    });
                    _this.loadingToaster = _this.toastrService.show(config.message, config.title, config, 'toast-loading');
                }
                else {
                    if (_this.loadingToaster) {
                        _this.loadingToaster.toastRef.close();
                    }
                }
            }));
        }));
    };
    /**
     * @param {?=} toastId
     * @return {?}
     */
    ToastService.prototype.clear = /**
     * @param {?=} toastId
     * @return {?}
     */
    function (toastId) {
        this.toastrService.clear(toastId);
    };
    /**
     * @param {?} config
     * @return {?}
     */
    ToastService.prototype.show = /**
     * @param {?} config
     * @return {?}
     */
    function (config) {
        config = this.applyConfig(config);
        return this.toastrService.show(config.message, config.title, config);
    };
    /**
     * @private
     * @param {?=} override
     * @return {?}
     */
    ToastService.prototype.applyConfig = /**
     * @private
     * @param {?=} override
     * @return {?}
     */
    function (override) {
        if (override === void 0) { override = {}; }
        if (override.objectAction && override.objectAction.display.length > 17) {
            override.objectAction.display = override.objectAction.display.substring(0, 17);
        }
        return tslib_1.__assign({}, this.defaultConfig, override);
    };
    ToastService.decorators = [
        { type: Injectable, args: [{ providedIn: 'root' },] }
    ];
    /** @nocollapse */
    ToastService.ctorParameters = function () { return [
        { type: ToastrService }
    ]; };
    /** @nocollapse */ ToastService.ngInjectableDef = i0.defineInjectable({ factory: function ToastService_Factory() { return new ToastService(i0.inject(i1.ToastrService)); }, token: ToastService, providedIn: "root" });
    return ToastService;
}());
export { ToastService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    ToastService.prototype.loadingSubscription;
    /**
     * @type {?}
     * @private
     */
    ToastService.prototype.loadingToaster;
    /**
     * @type {?}
     * @private
     */
    ToastService.prototype.defaultConfig;
    /**
     * @type {?}
     * @protected
     */
    ToastService.prototype.toastrService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9hc3Quc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvbGF5b3V0L3RvYXN0L3RvYXN0LnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUMsVUFBVSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBQyxhQUFhLEVBQUMsTUFBTSxZQUFZLENBQUM7QUFFekMsT0FBTyxFQUFDLGNBQWMsRUFBQyxNQUFNLG1CQUFtQixDQUFDO0FBRWpELE9BQU8sRUFBK0IsS0FBSyxFQUFDLE1BQU0sTUFBTSxDQUFDOzs7QUFFekQ7SUFhRSxzQkFBc0IsYUFBNEI7UUFBNUIsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFSMUMsa0JBQWEsR0FBeUI7WUFDNUMsT0FBTyxFQUFFLEtBQUs7WUFDZCxjQUFjLEVBQUUsY0FBYztZQUM5QixXQUFXLEVBQUUsSUFBSTtZQUNqQixhQUFhLEVBQUUsb0JBQW9CO1lBQ25DLGVBQWUsRUFBRSxJQUFJO1NBQ3RCLENBQUM7SUFHRixDQUFDOzs7Ozs7O0lBRU0sOEJBQU87Ozs7OztJQUFkLFVBQWUsT0FBZSxFQUFFLEtBQWMsRUFBRSxZQUEwQjs7WUFDbEUsTUFBTSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsRUFBQyxPQUFPLFNBQUEsRUFBRSxLQUFLLE9BQUEsRUFBRSxZQUFZLGNBQUEsRUFBQyxDQUFDO1FBQy9ELE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQzFFLENBQUM7Ozs7Ozs7O0lBRU0sNEJBQUs7Ozs7Ozs7SUFBWixVQUFhLE9BQWUsRUFBRSxLQUFZLEVBQUUsS0FBYyxFQUFFLFlBQTBCOztZQUM5RSxNQUFNLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQztZQUM5QixPQUFPLFNBQUE7WUFDUCxLQUFLLE9BQUE7WUFDTCxLQUFLLE9BQUE7WUFDTCxZQUFZLGNBQUE7WUFDWixjQUFjLEVBQUUsSUFBSTtZQUNwQixpQkFBaUIsRUFBRSxJQUFJO1lBQ3ZCLFdBQVcsRUFBRSxLQUFLO1lBQ2xCLFdBQVcsRUFBRSxLQUFLO1lBQ2xCLGFBQWEsRUFBRSxxQkFBcUI7WUFDcEMsWUFBWSxFQUFFLEtBQUs7WUFDbkIsV0FBVyxFQUFFLElBQUk7U0FDbEIsQ0FBQztRQUVGLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ3hFLENBQUM7Ozs7Ozs7SUFFTSwyQkFBSTs7Ozs7O0lBQVgsVUFBWSxPQUFlLEVBQUUsS0FBYyxFQUFFLFlBQTBCOztZQUMvRCxjQUFjLEdBQUcsWUFBWSxJQUFJLElBQUk7O1lBQ3JDLE1BQU0sR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEVBQUMsT0FBTyxTQUFBLEVBQUUsS0FBSyxPQUFBLEVBQUUsWUFBWSxjQUFBLEVBQUUsY0FBYyxnQkFBQSxFQUFDLENBQUM7UUFDL0UsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxLQUFLLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDdkUsQ0FBQzs7Ozs7OztJQUVNLDhCQUFPOzs7Ozs7SUFBZCxVQUFlLE9BQWUsRUFBRSxLQUFjLEVBQUUsWUFBMEI7O1lBQ2xFLGNBQWMsR0FBRyxZQUFZLElBQUksSUFBSTs7WUFDckMsTUFBTSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsRUFBQyxPQUFPLFNBQUEsRUFBRSxLQUFLLE9BQUEsRUFBRSxZQUFZLGNBQUEsRUFBRSxjQUFjLGdCQUFBLEVBQUMsQ0FBQztRQUMvRSxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLEtBQUssRUFBRSxNQUFNLENBQUMsQ0FBQztJQUMxRSxDQUFDOzs7Ozs7OztJQUVNLDhCQUFPOzs7Ozs7O0lBQWQsVUFBZSxVQUErQixFQUFFLE9BQWlDLEVBQUUsS0FBYyxFQUFFLFlBQTBCO1FBQTdILGlCQXFDQztRQXJDK0Msd0JBQUEsRUFBQSx5QkFBaUM7UUFDL0UsSUFBSSxJQUFJLENBQUMsbUJBQW1CLElBQUksSUFBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sS0FBSyxLQUFLLEVBQUU7WUFDekUsSUFBSSxJQUFJLENBQUMsY0FBYyxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxFQUFFO2dCQUN2RCxJQUFJO29CQUNGLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRSxDQUFDO2lCQUN0QztnQkFBQyxPQUFPLEtBQUssRUFBRTtpQkFDZjthQUNGO1lBRUQsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQ3hDO1FBRUQsSUFBSSxDQUFDLG1CQUFtQixHQUFHLFVBQVUsQ0FBQyxTQUFTOzs7O1FBQUMsVUFBQSxTQUFTO1lBQ3ZELEtBQUssRUFBRSxDQUFDLFNBQVM7OztZQUFDO2dCQUNoQixJQUFJLFNBQVMsRUFBRTs7d0JBRVAsTUFBTSxHQUFHLEtBQUksQ0FBQyxXQUFXLENBQUM7d0JBQzlCLE9BQU8sRUFBRSxPQUFPLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLE9BQU87d0JBQ25ELEtBQUssT0FBQTt3QkFDTCxZQUFZLGNBQUE7d0JBQ1osY0FBYyxFQUFFLElBQUk7d0JBQ3BCLGlCQUFpQixFQUFFLElBQUk7d0JBQ3ZCLFdBQVcsRUFBRSxLQUFLO3dCQUNsQixXQUFXLEVBQUUsS0FBSzt3QkFDbEIsYUFBYSxFQUFFLGtCQUFrQjt3QkFDakMsWUFBWSxFQUFFLEtBQUs7cUJBQ3BCLENBQUM7b0JBRUYsS0FBSSxDQUFDLGNBQWMsR0FBRyxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxLQUFLLEVBQUUsTUFBTSxFQUFFLGVBQWUsQ0FBQyxDQUFDO2lCQUV0RztxQkFBTTtvQkFDTCxJQUFJLEtBQUksQ0FBQyxjQUFjLEVBQUU7d0JBQ3ZCLEtBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRSxDQUFDO3FCQUN0QztpQkFDRjtZQUNILENBQUMsRUFBQyxDQUFDO1FBQ0wsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDOzs7OztJQUVNLDRCQUFLOzs7O0lBQVosVUFBYSxPQUFnQjtRQUMzQixJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUNwQyxDQUFDOzs7OztJQUVNLDJCQUFJOzs7O0lBQVgsVUFBWSxNQUE0QjtRQUN0QyxNQUFNLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNsQyxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLEtBQUssRUFBRSxNQUFNLENBQUMsQ0FBQztJQUN2RSxDQUFDOzs7Ozs7SUFFTyxrQ0FBVzs7Ozs7SUFBbkIsVUFBb0IsUUFBbUM7UUFBbkMseUJBQUEsRUFBQSxhQUFtQztRQUNyRCxJQUFJLFFBQVEsQ0FBQyxZQUFZLElBQUksUUFBUSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLEVBQUUsRUFBRTtZQUN0RSxRQUFRLENBQUMsWUFBWSxDQUFDLE9BQU8sR0FBRyxRQUFRLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1NBQ2hGO1FBRUQsNEJBQVcsSUFBSSxDQUFDLGFBQWEsRUFBSyxRQUFRLEVBQUU7SUFDOUMsQ0FBQzs7Z0JBekdGLFVBQVUsU0FBQyxFQUFDLFVBQVUsRUFBRSxNQUFNLEVBQUM7Ozs7Z0JBTnhCLGFBQWE7Ozt1QkFEckI7Q0FpSEMsQUExR0QsSUEwR0M7U0F6R1ksWUFBWTs7Ozs7O0lBRXZCLDJDQUEwQzs7Ozs7SUFDMUMsc0NBQXFDOzs7OztJQUNyQyxxQ0FNRTs7Ozs7SUFFVSxxQ0FBc0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0luamVjdGFibGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtUb2FzdHJTZXJ2aWNlfSBmcm9tICduZ3gtdG9hc3RyJztcbmltcG9ydCB7VG9hc3RDb25maWd9IGZyb20gJy4vdG9hc3QuY29uZmlnJztcbmltcG9ydCB7VG9hc3RDb21wb25lbnR9IGZyb20gJy4vdG9hc3QuY29tcG9uZW50JztcbmltcG9ydCB7VG9hc3RlciwgVG9hc3RBY3Rpb259IGZyb20gJy4vdG9hc3QuYWN0aW9uJztcbmltcG9ydCB7U3Vic2NyaXB0aW9uLCBPYnNlcnZhYmxlLCBvZiwgdGltZXJ9IGZyb20gJ3J4anMnO1xuXG5ASW5qZWN0YWJsZSh7cHJvdmlkZWRJbjogJ3Jvb3QnfSlcbmV4cG9ydCBjbGFzcyBUb2FzdFNlcnZpY2Uge1xuXG4gIHByaXZhdGUgbG9hZGluZ1N1YnNjcmlwdGlvbjogU3Vic2NyaXB0aW9uO1xuICBwcml2YXRlIGxvYWRpbmdUb2FzdGVyOiBUb2FzdGVyPGFueT47XG4gIHByaXZhdGUgZGVmYXVsdENvbmZpZzogUGFydGlhbDxUb2FzdENvbmZpZz4gPSB7XG4gICAgdGltZU91dDogMTAwMDAsXG4gICAgdG9hc3RDb21wb25lbnQ6IFRvYXN0Q29tcG9uZW50LFxuICAgIHByb2dyZXNzQmFyOiB0cnVlLFxuICAgIHBvc2l0aW9uQ2xhc3M6ICd0b2FzdC1ib3R0b20tcmlnaHQnLFxuICAgIGV4dGVuZGVkVGltZU91dDogNTAwMCxcbiAgfTtcblxuICBjb25zdHJ1Y3Rvcihwcm90ZWN0ZWQgdG9hc3RyU2VydmljZTogVG9hc3RyU2VydmljZSkge1xuICB9XG5cbiAgcHVibGljIHN1Y2Nlc3MobWVzc2FnZTogc3RyaW5nLCB0aXRsZT86IHN0cmluZywgb2JqZWN0QWN0aW9uPzogVG9hc3RBY3Rpb24pOiBUb2FzdGVyPGFueT4ge1xuICAgIGNvbnN0IGNvbmZpZyA9IHRoaXMuYXBwbHlDb25maWcoe21lc3NhZ2UsIHRpdGxlLCBvYmplY3RBY3Rpb259KTtcbiAgICByZXR1cm4gdGhpcy50b2FzdHJTZXJ2aWNlLnN1Y2Nlc3MoY29uZmlnLm1lc3NhZ2UsIGNvbmZpZy50aXRsZSwgY29uZmlnKTtcbiAgfVxuXG4gIHB1YmxpYyBlcnJvcihtZXNzYWdlOiBzdHJpbmcsIGVycm9yOiBFcnJvciwgdGl0bGU/OiBzdHJpbmcsIG9iamVjdEFjdGlvbj86IFRvYXN0QWN0aW9uKTogVG9hc3Rlcjxhbnk+IHtcbiAgICBjb25zdCBjb25maWcgPSB0aGlzLmFwcGx5Q29uZmlnKHtcbiAgICAgIG1lc3NhZ2UsXG4gICAgICB0aXRsZSxcbiAgICAgIGVycm9yLFxuICAgICAgb2JqZWN0QWN0aW9uLFxuICAgICAgZGlzYWJsZVRpbWVPdXQ6IHRydWUsXG4gICAgICBwcmV2ZW50RHVwbGljYXRlczogdHJ1ZSxcbiAgICAgIHByb2dyZXNzQmFyOiBmYWxzZSxcbiAgICAgIGF1dG9EaXNtaXNzOiBmYWxzZSxcbiAgICAgIHBvc2l0aW9uQ2xhc3M6ICd0b2FzdC1jZW50ZXItY2VudGVyJyxcbiAgICAgIHRhcFRvRGlzbWlzczogZmFsc2UsXG4gICAgICBjbG9zZUJ1dHRvbjogdHJ1ZVxuICAgIH0pO1xuXG4gICAgcmV0dXJuIHRoaXMudG9hc3RyU2VydmljZS5lcnJvcihjb25maWcubWVzc2FnZSwgY29uZmlnLnRpdGxlLCBjb25maWcpO1xuICB9XG5cbiAgcHVibGljIGluZm8obWVzc2FnZTogc3RyaW5nLCB0aXRsZT86IHN0cmluZywgb2JqZWN0QWN0aW9uPzogVG9hc3RBY3Rpb24pOiBUb2FzdGVyPGFueT4ge1xuICAgIGNvbnN0IGRpc2FibGVUaW1lT3V0ID0gb2JqZWN0QWN0aW9uICE9IG51bGw7XG4gICAgY29uc3QgY29uZmlnID0gdGhpcy5hcHBseUNvbmZpZyh7bWVzc2FnZSwgdGl0bGUsIG9iamVjdEFjdGlvbiwgZGlzYWJsZVRpbWVPdXR9KTtcbiAgICByZXR1cm4gdGhpcy50b2FzdHJTZXJ2aWNlLmluZm8oY29uZmlnLm1lc3NhZ2UsIGNvbmZpZy50aXRsZSwgY29uZmlnKTtcbiAgfVxuXG4gIHB1YmxpYyB3YXJuaW5nKG1lc3NhZ2U6IHN0cmluZywgdGl0bGU/OiBzdHJpbmcsIG9iamVjdEFjdGlvbj86IFRvYXN0QWN0aW9uKTogVG9hc3Rlcjxhbnk+IHtcbiAgICBjb25zdCBkaXNhYmxlVGltZU91dCA9IG9iamVjdEFjdGlvbiAhPSBudWxsO1xuICAgIGNvbnN0IGNvbmZpZyA9IHRoaXMuYXBwbHlDb25maWcoe21lc3NhZ2UsIHRpdGxlLCBvYmplY3RBY3Rpb24sIGRpc2FibGVUaW1lT3V0fSk7XG4gICAgcmV0dXJuIHRoaXMudG9hc3RyU2VydmljZS53YXJuaW5nKGNvbmZpZy5tZXNzYWdlLCBjb25maWcudGl0bGUsIGNvbmZpZyk7XG4gIH1cblxuICBwdWJsaWMgbG9hZGluZyhpc0xvYWRpbmckOiBPYnNlcnZhYmxlPGJvb2xlYW4+LCBtZXNzYWdlOiBzdHJpbmcgPSAnQ2FycmVnYW5kby4uLicsIHRpdGxlPzogc3RyaW5nLCBvYmplY3RBY3Rpb24/OiBUb2FzdEFjdGlvbik6IHZvaWQge1xuICAgIGlmICh0aGlzLmxvYWRpbmdTdWJzY3JpcHRpb24gJiYgdGhpcy5sb2FkaW5nU3Vic2NyaXB0aW9uLmNsb3NlZCA9PT0gZmFsc2UpIHtcbiAgICAgIGlmICh0aGlzLmxvYWRpbmdUb2FzdGVyICYmIHRoaXMubG9hZGluZ1RvYXN0ZXIudG9hc3RSZWYpIHtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICB0aGlzLmxvYWRpbmdUb2FzdGVyLnRvYXN0UmVmLmNsb3NlKCk7XG4gICAgICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgdGhpcy5sb2FkaW5nU3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XG4gICAgfVxuXG4gICAgdGhpcy5sb2FkaW5nU3Vic2NyaXB0aW9uID0gaXNMb2FkaW5nJC5zdWJzY3JpYmUoaXNMb2FkaW5nID0+IHtcbiAgICAgIHRpbWVyKCkuc3Vic2NyaWJlKCgpID0+IHtcbiAgICAgICAgaWYgKGlzTG9hZGluZykge1xuXG4gICAgICAgICAgY29uc3QgY29uZmlnID0gdGhpcy5hcHBseUNvbmZpZyh7XG4gICAgICAgICAgICBtZXNzYWdlOiBtZXNzYWdlID09PSAnJyA/ICdDYXJyZWdhbmRvLi4uJyA6IG1lc3NhZ2UsXG4gICAgICAgICAgICB0aXRsZSxcbiAgICAgICAgICAgIG9iamVjdEFjdGlvbixcbiAgICAgICAgICAgIGRpc2FibGVUaW1lT3V0OiB0cnVlLFxuICAgICAgICAgICAgcHJldmVudER1cGxpY2F0ZXM6IHRydWUsXG4gICAgICAgICAgICBwcm9ncmVzc0JhcjogZmFsc2UsXG4gICAgICAgICAgICBhdXRvRGlzbWlzczogZmFsc2UsXG4gICAgICAgICAgICBwb3NpdGlvbkNsYXNzOiAndG9hc3QtdG9wLWNlbnRlcicsXG4gICAgICAgICAgICB0YXBUb0Rpc21pc3M6IGZhbHNlXG4gICAgICAgICAgfSk7XG5cbiAgICAgICAgICB0aGlzLmxvYWRpbmdUb2FzdGVyID0gdGhpcy50b2FzdHJTZXJ2aWNlLnNob3coY29uZmlnLm1lc3NhZ2UsIGNvbmZpZy50aXRsZSwgY29uZmlnLCAndG9hc3QtbG9hZGluZycpO1xuXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgaWYgKHRoaXMubG9hZGluZ1RvYXN0ZXIpIHtcbiAgICAgICAgICAgIHRoaXMubG9hZGluZ1RvYXN0ZXIudG9hc3RSZWYuY2xvc2UoKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG5cbiAgcHVibGljIGNsZWFyKHRvYXN0SWQ/OiBudW1iZXIpOiB2b2lkIHtcbiAgICB0aGlzLnRvYXN0clNlcnZpY2UuY2xlYXIodG9hc3RJZCk7XG4gIH1cblxuICBwdWJsaWMgc2hvdyhjb25maWc6IFBhcnRpYWw8VG9hc3RDb25maWc+KTogVG9hc3Rlcjxhbnk+IHtcbiAgICBjb25maWcgPSB0aGlzLmFwcGx5Q29uZmlnKGNvbmZpZyk7XG4gICAgcmV0dXJuIHRoaXMudG9hc3RyU2VydmljZS5zaG93KGNvbmZpZy5tZXNzYWdlLCBjb25maWcudGl0bGUsIGNvbmZpZyk7XG4gIH1cblxuICBwcml2YXRlIGFwcGx5Q29uZmlnKG92ZXJyaWRlOiBQYXJ0aWFsPFRvYXN0Q29uZmlnPiA9IHt9KTogUGFydGlhbDxUb2FzdENvbmZpZz4ge1xuICAgIGlmIChvdmVycmlkZS5vYmplY3RBY3Rpb24gJiYgb3ZlcnJpZGUub2JqZWN0QWN0aW9uLmRpc3BsYXkubGVuZ3RoID4gMTcpIHtcbiAgICAgIG92ZXJyaWRlLm9iamVjdEFjdGlvbi5kaXNwbGF5ID0gb3ZlcnJpZGUub2JqZWN0QWN0aW9uLmRpc3BsYXkuc3Vic3RyaW5nKDAsIDE3KTtcbiAgICB9XG5cbiAgICByZXR1cm4gey4uLnRoaXMuZGVmYXVsdENvbmZpZywgLi4ub3ZlcnJpZGV9O1xuICB9XG59XG4iXX0=