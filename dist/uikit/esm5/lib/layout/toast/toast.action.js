/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var ToastAction = /** @class */ (function () {
    function ToastAction() {
    }
    return ToastAction;
}());
export { ToastAction };
if (false) {
    /** @type {?} */
    ToastAction.prototype.display;
    /** @type {?} */
    ToastAction.prototype.action;
}
/**
 * @record
 * @template T
 */
export function Toaster() { }
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9hc3QuYWN0aW9uLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9sYXlvdXQvdG9hc3QvdG9hc3QuYWN0aW9uLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFFQTtJQUFBO0lBVUEsQ0FBQztJQUFELGtCQUFDO0FBQUQsQ0FBQyxBQVZELElBVUM7Ozs7SUFOQyw4QkFBZ0I7O0lBS2hCLDZCQUErQjs7Ozs7O0FBR2pDLDZCQUVDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQWN0aXZlVG9hc3QgfSBmcm9tICduZ3gtdG9hc3RyJztcblxuZXhwb3J0IGNsYXNzIFRvYXN0QWN0aW9uIHtcbiAgLypcbiAgICBEZXNjcmnDp8OjbyBkbyBsaW5rIGRvIGHDp8OjbyBubyB0b2FzdCAtLU3DoXhpbW8gZGUgY2FyYWN0ZXJlcyAxN1xuICAqL1xuICBkaXNwbGF5OiBzdHJpbmc7XG5cbiAgLypcbiAgICBBw6fDo28gYSBzZXIgZXhlY3V0YWRhIGFvIGNsaWNhciBubyBsaW5rIGF0cmlidWlkYSBubyBkaXNwbGF5XG4gICovXG4gIGFjdGlvbjogKCh2YWx1ZT86IGFueSkgPT4gYW55KTtcbn1cblxuZXhwb3J0IGludGVyZmFjZSBUb2FzdGVyPFQ+IGV4dGVuZHMgQWN0aXZlVG9hc3Q8VD4ge1xuXG59XG4iXX0=