/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToastComponent } from '../toast/toast.component';
import { ToastService } from '../toast/toast.service';
import { ToastrModule, ToastNoAnimationModule, ToastContainerModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
var ToastModule = /** @class */ (function () {
    function ToastModule() {
    }
    ToastModule.decorators = [
        { type: NgModule, args: [{
                    entryComponents: [ToastComponent],
                    declarations: [ToastComponent],
                    imports: [
                        CommonModule,
                        ToastNoAnimationModule,
                        ToastrModule.forRoot(),
                        ToastContainerModule,
                        BrowserAnimationsModule,
                    ],
                    exports: [ToastComponent],
                    providers: [ToastService]
                },] }
    ];
    return ToastModule;
}());
export { ToastModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9hc3QubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9sYXlvdXQvdG9hc3QvdG9hc3QubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsUUFBUSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3ZDLE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUM3QyxPQUFPLEVBQUMsY0FBYyxFQUFDLE1BQU0sMEJBQTBCLENBQUM7QUFDeEQsT0FBTyxFQUFDLFlBQVksRUFBQyxNQUFNLHdCQUF3QixDQUFDO0FBQ3BELE9BQU8sRUFDTCxZQUFZLEVBQ1osc0JBQXNCLEVBQ3RCLG9CQUFvQixFQUNyQixNQUFNLFlBQVksQ0FBQztBQUVwQixPQUFPLEVBQUMsdUJBQXVCLEVBQUMsTUFBTSxzQ0FBc0MsQ0FBQztBQUU3RTtJQUFBO0lBY0EsQ0FBQzs7Z0JBZEEsUUFBUSxTQUFDO29CQUNSLGVBQWUsRUFBRSxDQUFDLGNBQWMsQ0FBQztvQkFDakMsWUFBWSxFQUFFLENBQUMsY0FBYyxDQUFDO29CQUM5QixPQUFPLEVBQUU7d0JBQ1AsWUFBWTt3QkFDWixzQkFBc0I7d0JBQ3RCLFlBQVksQ0FBQyxPQUFPLEVBQUU7d0JBQ3RCLG9CQUFvQjt3QkFDcEIsdUJBQXVCO3FCQUN4QjtvQkFDRCxPQUFPLEVBQUUsQ0FBQyxjQUFjLENBQUM7b0JBQ3pCLFNBQVMsRUFBRSxDQUFDLFlBQVksQ0FBQztpQkFDMUI7O0lBRUQsa0JBQUM7Q0FBQSxBQWRELElBY0M7U0FEWSxXQUFXIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtOZ01vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0NvbW1vbk1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7VG9hc3RDb21wb25lbnR9IGZyb20gJy4uL3RvYXN0L3RvYXN0LmNvbXBvbmVudCc7XG5pbXBvcnQge1RvYXN0U2VydmljZX0gZnJvbSAnLi4vdG9hc3QvdG9hc3Quc2VydmljZSc7XG5pbXBvcnQge1xuICBUb2FzdHJNb2R1bGUsXG4gIFRvYXN0Tm9BbmltYXRpb25Nb2R1bGUsXG4gIFRvYXN0Q29udGFpbmVyTW9kdWxlXG59IGZyb20gJ25neC10b2FzdHInO1xuXG5pbXBvcnQge0Jyb3dzZXJBbmltYXRpb25zTW9kdWxlfSBmcm9tICdAYW5ndWxhci9wbGF0Zm9ybS1icm93c2VyL2FuaW1hdGlvbnMnO1xuXG5ATmdNb2R1bGUoe1xuICBlbnRyeUNvbXBvbmVudHM6IFtUb2FzdENvbXBvbmVudF0sXG4gIGRlY2xhcmF0aW9uczogW1RvYXN0Q29tcG9uZW50XSxcbiAgaW1wb3J0czogW1xuICAgIENvbW1vbk1vZHVsZSxcbiAgICBUb2FzdE5vQW5pbWF0aW9uTW9kdWxlLFxuICAgIFRvYXN0ck1vZHVsZS5mb3JSb290KCksXG4gICAgVG9hc3RDb250YWluZXJNb2R1bGUsXG4gICAgQnJvd3NlckFuaW1hdGlvbnNNb2R1bGUsXG4gIF0sXG4gIGV4cG9ydHM6IFtUb2FzdENvbXBvbmVudF0sXG4gIHByb3ZpZGVyczogW1RvYXN0U2VydmljZV1cbn0pXG5leHBvcnQgY2xhc3MgVG9hc3RNb2R1bGUge1xufVxuIl19