/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function ToastConfig() { }
if (false) {
    /**
     * Display message toast
     * default: empty
     * @type {?}
     */
    ToastConfig.prototype.message;
    /**
     * Display title toast
     * default: empty
     * @type {?|undefined}
     */
    ToastConfig.prototype.title;
    /**
     * Dysplay error
     * default: null
     * @type {?|undefined}
     */
    ToastConfig.prototype.error;
    /**
     * Dysplay icon
     * default: null
     * @type {?|undefined}
     */
    ToastConfig.prototype.icon;
    /**
     * objectAction : { display: string, action: (() => any)}
     * default: null
     * @type {?}
     */
    ToastConfig.prototype.objectAction;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9hc3QuY29uZmlnLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9sYXlvdXQvdG9hc3QvdG9hc3QuY29uZmlnLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFHQSxpQ0E4QkM7Ozs7Ozs7SUF6QkMsOEJBQWdCOzs7Ozs7SUFNaEIsNEJBQWU7Ozs7OztJQU1mLDRCQUFjOzs7Ozs7SUFNZCwyQkFBYzs7Ozs7O0lBTWQsbUNBQTBCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgR2xvYmFsQ29uZmlnIH0gZnJvbSAnbmd4LXRvYXN0cic7XG5pbXBvcnQgeyBUb2FzdEFjdGlvbiB9IGZyb20gJy4vdG9hc3QuYWN0aW9uJztcblxuZXhwb3J0IGludGVyZmFjZSBUb2FzdENvbmZpZyBleHRlbmRzIEdsb2JhbENvbmZpZyB7XG4gIC8qKlxuICAgKiBEaXNwbGF5IG1lc3NhZ2UgdG9hc3RcbiAgICogZGVmYXVsdDogZW1wdHlcbiAgICovXG4gIG1lc3NhZ2U6IHN0cmluZztcblxuICAvKipcbiAgICogRGlzcGxheSB0aXRsZSB0b2FzdFxuICAgKiBkZWZhdWx0OiBlbXB0eVxuICAgKi9cbiAgdGl0bGU/OiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIER5c3BsYXkgZXJyb3JcbiAgICogZGVmYXVsdDogbnVsbFxuICAgKi9cbiAgZXJyb3I/OiBFcnJvcjtcblxuICAvKipcbiAgICogRHlzcGxheSBpY29uXG4gICAqIGRlZmF1bHQ6IG51bGxcbiAgICovXG4gIGljb24/OiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIG9iamVjdEFjdGlvbiA6IHsgZGlzcGxheTogc3RyaW5nLCBhY3Rpb246ICgoKSA9PiBhbnkpfVxuICAgKiBkZWZhdWx0OiBudWxsXG4gICAqL1xuICBvYmplY3RBY3Rpb246IFRvYXN0QWN0aW9uO1xufVxuIl19