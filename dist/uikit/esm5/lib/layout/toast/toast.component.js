/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, Renderer2 } from '@angular/core';
import { Toast, ToastrService, ToastPackage } from 'ngx-toastr';
import { Hotkey, HotkeysService } from 'angular2-hotkeys';
var ToastComponent = /** @class */ (function (_super) {
    tslib_1.__extends(ToastComponent, _super);
    function ToastComponent(toastrService, toastPackage, renderer, hotkeysService) {
        var _this = _super.call(this, toastrService, toastPackage) || this;
        _this.toastrService = toastrService;
        _this.toastPackage = toastPackage;
        _this.renderer = renderer;
        _this.hotkeysService = hotkeysService;
        return _this;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    ToastComponent.prototype.action = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        event.stopPropagation();
        this.options.objectAction.action();
        this.toastPackage.triggerAction();
        return false;
    };
    /**
     * @return {?}
     */
    ToastComponent.prototype.ngAfterViewInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (this.options.error) {
            this.hotkeysService.add(new Hotkey('esc', (/**
             * @param {?} event
             * @return {?}
             */
            function (event) {
                _this.toastrService.clear(_this.toastrService.currentlyActive);
                _this.remove();
                return false;
            })));
            /** @type {?} */
            var toast = this.toastrService.toasts.find((/**
             * @param {?} c
             * @return {?}
             */
            function (c) { return c.toastId === _this.toastPackage.toastId && !c.toastRef.isInactive(); }));
            if (toast) {
                this.divCenter = this.renderer.createElement('div');
                this.renderer.addClass(this.divCenter, 'block-toast');
                /** @type {?} */
                var existBlockToast = this.toastrService.toasts.some((/**
                 * @param {?} c
                 * @return {?}
                 */
                function (c) { return _this.isBlockBodyToast(c.toastRef); }));
                if (!existBlockToast) {
                    this.blockBodyToast = true;
                    document.body.appendChild(this.divCenter);
                }
                toast.onHidden.subscribe((/**
                 * @return {?}
                 */
                function () {
                    if (_this.blockBodyToast) {
                        document.body.removeChild(_this.divCenter);
                    }
                }));
            }
        }
    };
    /**
     * @param {?} toast
     * @return {?}
     */
    ToastComponent.prototype.isBlockBodyToast = /**
     * @param {?} toast
     * @return {?}
     */
    function (toast) {
        if (toast && (toast.componentInstance instanceof ToastComponent)) {
            return toast.componentInstance.blockBodyToast;
        }
        return false;
    };
    ToastComponent.decorators = [
        { type: Component, args: [{
                    selector: '[uikit-toast-component]',
                    template: "<div *ngIf=\"!options.error\" class=\"toast-content-left\">\n  <div class=\"toast-content-icon\" *ngIf=\"options.icon\">\n    <i [class]=\"options.icon\"></i>\n  </div>\n</div>\n<div *ngIf=\"options.error\" class=\"toast-content-topo\">\n  <p>Ops! Algo aconteceu.</p>\n</div>\n<button *ngIf=\"options.closeButton\" (click)=\"remove()\" class=\"toast-close-button\" aria-label=\"Close\">\n  <span aria-hidden=\"true\">&times;</span>\n</button>\n<div class=\"row\" [style.display]=\"state.value === 'inactive' ? 'none' : ''\">\n  <div class=\"col-9\">\n    <div *ngIf=\"options.title\" [class]=\"options.titleClass\" [attr.aria-label]=\"title\">\n      {{ options.title }}\n    </div>\n    <div *ngIf=\"options.message && options.enableHtml\" role=\"alert\" aria-live=\"polite\" [class]=\"options.messageClass\"\n         [innerHTML]=\"message\">\n    </div>\n    <div *ngIf=\"options.message && !options.enableHtml\" role=\"alert\" aria-live=\"polite\" [class]=\"options.messageClass\"\n         [attr.aria-label]=\"message\">\n      {{ options.message }}\n    </div>\n  </div>\n  <div *ngIf=\"options.objectAction && options.objectAction.display && !options.error\"\n       class=\"col-3 text-right toast-action\">\n    <a class=\"btn btn-link btn-sm\" (click)=\"action($event)\">\n      {{ options.objectAction.display }}\n    </a>\n  </div>\n</div>\n<div *ngIf=\"options.error\">\n  <div class=\"icone\"><i [class]=\"options.icon\"></i></div>\n  <details class=\"toast-error-details\">\n\n    <summary>{{options.error.name}}</summary>\n    <div class=\"toast-details-error\" id=\"style-3\">\n      <p>Message: {{options.error.message}}</p>\n      <p>Stack: {{options.error.stack}}</p>\n    </div>\n  </details>\n</div>\n\n<div *ngIf=\"options.progressBar\">\n  <div class=\"toast-progress\" [style.width]=\"width + '%'\"></div>\n</div>\n",
                    animations: [
                        trigger('flyInOut', [
                            state('inactive', style({ opacity: 0 })),
                            state('active', style({ opacity: 1 })),
                            state('removed', style({ opacity: 0 })),
                            transition('inactive => active', animate('{{ easeTime }}ms {{ easing }}')),
                            transition('active => removed', animate('{{ easeTime }}ms {{ easing }}'))
                        ])
                    ],
                    preserveWhitespaces: false,
                    styles: [".btn-link{font-size:11px}.toast-action{text-align:center;text-transform:uppercase}"]
                }] }
    ];
    /** @nocollapse */
    ToastComponent.ctorParameters = function () { return [
        { type: ToastrService },
        { type: ToastPackage },
        { type: Renderer2 },
        { type: HotkeysService }
    ]; };
    return ToastComponent;
}(Toast));
export { ToastComponent };
if (false) {
    /** @type {?} */
    ToastComponent.prototype.options;
    /**
     * @type {?}
     * @protected
     */
    ToastComponent.prototype.divCenter;
    /** @type {?} */
    ToastComponent.prototype.blockBodyToast;
    /**
     * @type {?}
     * @protected
     */
    ToastComponent.prototype.toastrService;
    /** @type {?} */
    ToastComponent.prototype.toastPackage;
    /**
     * @type {?}
     * @protected
     */
    ToastComponent.prototype.renderer;
    /** @type {?} */
    ToastComponent.prototype.hotkeysService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9hc3QuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9sYXlvdXQvdG9hc3QvdG9hc3QuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUNMLE9BQU8sRUFDUCxLQUFLLEVBQ0wsS0FBSyxFQUNMLFVBQVUsRUFDVixPQUFPLEVBQ1IsTUFBTSxxQkFBcUIsQ0FBQztBQUM3QixPQUFPLEVBQWdCLFNBQVMsRUFBRSxTQUFTLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDbEUsT0FBTyxFQUFDLEtBQUssRUFBRSxhQUFhLEVBQUUsWUFBWSxFQUFDLE1BQU0sWUFBWSxDQUFDO0FBRzlELE9BQU8sRUFBQyxNQUFNLEVBQUUsY0FBYyxFQUFDLE1BQU0sa0JBQWtCLENBQUM7QUFFeEQ7SUFxQm9DLDBDQUFLO0lBS3ZDLHdCQUNZLGFBQTRCLEVBQy9CLFlBQTBCLEVBQ3ZCLFFBQW1CLEVBQ3RCLGNBQThCO1FBSnZDLFlBTUUsa0JBQU0sYUFBYSxFQUFFLFlBQVksQ0FBQyxTQUNuQztRQU5XLG1CQUFhLEdBQWIsYUFBYSxDQUFlO1FBQy9CLGtCQUFZLEdBQVosWUFBWSxDQUFjO1FBQ3ZCLGNBQVEsR0FBUixRQUFRLENBQVc7UUFDdEIsb0JBQWMsR0FBZCxjQUFjLENBQWdCOztJQUd2QyxDQUFDOzs7OztJQUVELCtCQUFNOzs7O0lBQU4sVUFBTyxLQUFZO1FBQ2pCLEtBQUssQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUN4QixJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNuQyxJQUFJLENBQUMsWUFBWSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ2xDLE9BQU8sS0FBSyxDQUFDO0lBQ2YsQ0FBQzs7OztJQUVELHdDQUFlOzs7SUFBZjtRQUFBLGlCQTZCQztRQTVCQyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFO1lBRXRCLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLElBQUksTUFBTSxDQUFDLEtBQUs7Ozs7WUFBRSxVQUFDLEtBQW9CO2dCQUM3RCxLQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxLQUFJLENBQUMsYUFBYSxDQUFDLGVBQWUsQ0FBQyxDQUFDO2dCQUM3RCxLQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7Z0JBQ2QsT0FBTyxLQUFLLENBQUM7WUFDZixDQUFDLEVBQUMsQ0FBQyxDQUFDOztnQkFFRSxLQUFLLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsSUFBSTs7OztZQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLE9BQU8sS0FBSyxLQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sSUFBSSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLEVBQW5FLENBQW1FLEVBQUM7WUFFdEgsSUFBSSxLQUFLLEVBQUU7Z0JBQ1QsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDcEQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxhQUFhLENBQUMsQ0FBQzs7b0JBRWhELGVBQWUsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxJQUFJOzs7O2dCQUFDLFVBQUMsQ0FBQyxJQUFLLE9BQUEsS0FBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsRUFBakMsQ0FBaUMsRUFBQztnQkFFaEcsSUFBSSxDQUFDLGVBQWUsRUFBRTtvQkFDcEIsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7b0JBQzNCLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztpQkFDM0M7Z0JBRUQsS0FBSyxDQUFDLFFBQVEsQ0FBQyxTQUFTOzs7Z0JBQUM7b0JBQ3ZCLElBQUksS0FBSSxDQUFDLGNBQWMsRUFBRTt3QkFDdkIsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO3FCQUMzQztnQkFDSCxDQUFDLEVBQUMsQ0FBQzthQUNKO1NBQ0Y7SUFDSCxDQUFDOzs7OztJQUVELHlDQUFnQjs7OztJQUFoQixVQUFpQixLQUFvQjtRQUNuQyxJQUFJLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsWUFBWSxjQUFjLENBQUMsRUFBRTtZQUNoRSxPQUFPLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLENBQUM7U0FDL0M7UUFFRCxPQUFPLEtBQUssQ0FBQztJQUNmLENBQUM7O2dCQS9FRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLHlCQUF5QjtvQkFDbkMsMnpEQUFxQztvQkFFckMsVUFBVSxFQUFFO3dCQUNWLE9BQU8sQ0FBQyxVQUFVLEVBQUU7NEJBQ2xCLEtBQUssQ0FBQyxVQUFVLEVBQUUsS0FBSyxDQUFDLEVBQUMsT0FBTyxFQUFFLENBQUMsRUFBQyxDQUFDLENBQUM7NEJBQ3RDLEtBQUssQ0FBQyxRQUFRLEVBQUUsS0FBSyxDQUFDLEVBQUMsT0FBTyxFQUFFLENBQUMsRUFBQyxDQUFDLENBQUM7NEJBQ3BDLEtBQUssQ0FBQyxTQUFTLEVBQUUsS0FBSyxDQUFDLEVBQUMsT0FBTyxFQUFFLENBQUMsRUFBQyxDQUFDLENBQUM7NEJBQ3JDLFVBQVUsQ0FDUixvQkFBb0IsRUFDcEIsT0FBTyxDQUFDLCtCQUErQixDQUFDLENBQ3pDOzRCQUNELFVBQVUsQ0FDUixtQkFBbUIsRUFDbkIsT0FBTyxDQUFDLCtCQUErQixDQUFDLENBQ3pDO3lCQUNGLENBQUM7cUJBQ0g7b0JBQ0QsbUJBQW1CLEVBQUUsS0FBSzs7aUJBQzNCOzs7O2dCQXpCYyxhQUFhO2dCQUFFLFlBQVk7Z0JBRFIsU0FBUztnQkFJM0IsY0FBYzs7SUFrRjlCLHFCQUFDO0NBQUEsQUFoRkQsQ0FxQm9DLEtBQUssR0EyRHhDO1NBM0RZLGNBQWM7OztJQUN6QixpQ0FBcUI7Ozs7O0lBQ3JCLG1DQUF5Qjs7SUFDekIsd0NBQXdCOzs7OztJQUd0Qix1Q0FBc0M7O0lBQ3RDLHNDQUFpQzs7Ozs7SUFDakMsa0NBQTZCOztJQUM3Qix3Q0FBcUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xuICBhbmltYXRlLFxuICBzdGF0ZSxcbiAgc3R5bGUsXG4gIHRyYW5zaXRpb24sXG4gIHRyaWdnZXJcbn0gZnJvbSAnQGFuZ3VsYXIvYW5pbWF0aW9ucyc7XG5pbXBvcnQge0FmdGVyVmlld0luaXQsIENvbXBvbmVudCwgUmVuZGVyZXIyfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7VG9hc3QsIFRvYXN0clNlcnZpY2UsIFRvYXN0UGFja2FnZX0gZnJvbSAnbmd4LXRvYXN0cic7XG5pbXBvcnQge1RvYXN0Q29uZmlnfSBmcm9tICcuL3RvYXN0LmNvbmZpZyc7XG5pbXBvcnQge1RvYXN0UmVmfSBmcm9tICduZ3gtdG9hc3RyL3RvYXN0ci90b2FzdC1pbmplY3Rvcic7XG5pbXBvcnQge0hvdGtleSwgSG90a2V5c1NlcnZpY2V9IGZyb20gJ2FuZ3VsYXIyLWhvdGtleXMnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdbdWlraXQtdG9hc3QtY29tcG9uZW50XScsXG4gIHRlbXBsYXRlVXJsOiAnLi90b2FzdC5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL3RvYXN0LmNvbXBvbmVudC5zY3NzJ10sXG4gIGFuaW1hdGlvbnM6IFtcbiAgICB0cmlnZ2VyKCdmbHlJbk91dCcsIFtcbiAgICAgIHN0YXRlKCdpbmFjdGl2ZScsIHN0eWxlKHtvcGFjaXR5OiAwfSkpLFxuICAgICAgc3RhdGUoJ2FjdGl2ZScsIHN0eWxlKHtvcGFjaXR5OiAxfSkpLFxuICAgICAgc3RhdGUoJ3JlbW92ZWQnLCBzdHlsZSh7b3BhY2l0eTogMH0pKSxcbiAgICAgIHRyYW5zaXRpb24oXG4gICAgICAgICdpbmFjdGl2ZSA9PiBhY3RpdmUnLFxuICAgICAgICBhbmltYXRlKCd7eyBlYXNlVGltZSB9fW1zIHt7IGVhc2luZyB9fScpXG4gICAgICApLFxuICAgICAgdHJhbnNpdGlvbihcbiAgICAgICAgJ2FjdGl2ZSA9PiByZW1vdmVkJyxcbiAgICAgICAgYW5pbWF0ZSgne3sgZWFzZVRpbWUgfX1tcyB7eyBlYXNpbmcgfX0nKVxuICAgICAgKVxuICAgIF0pXG4gIF0sXG4gIHByZXNlcnZlV2hpdGVzcGFjZXM6IGZhbHNlLFxufSlcbmV4cG9ydCBjbGFzcyBUb2FzdENvbXBvbmVudCBleHRlbmRzIFRvYXN0IGltcGxlbWVudHMgQWZ0ZXJWaWV3SW5pdCB7XG4gIG9wdGlvbnM6IFRvYXN0Q29uZmlnO1xuICBwcm90ZWN0ZWQgZGl2Q2VudGVyOiBhbnk7XG4gIGJsb2NrQm9keVRvYXN0OiBib29sZWFuO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByb3RlY3RlZCB0b2FzdHJTZXJ2aWNlOiBUb2FzdHJTZXJ2aWNlLFxuICAgIHB1YmxpYyB0b2FzdFBhY2thZ2U6IFRvYXN0UGFja2FnZSxcbiAgICBwcm90ZWN0ZWQgcmVuZGVyZXI6IFJlbmRlcmVyMixcbiAgICBwdWJsaWMgaG90a2V5c1NlcnZpY2U6IEhvdGtleXNTZXJ2aWNlLFxuICApIHtcbiAgICBzdXBlcih0b2FzdHJTZXJ2aWNlLCB0b2FzdFBhY2thZ2UpO1xuICB9XG5cbiAgYWN0aW9uKGV2ZW50OiBFdmVudCkge1xuICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xuICAgIHRoaXMub3B0aW9ucy5vYmplY3RBY3Rpb24uYWN0aW9uKCk7XG4gICAgdGhpcy50b2FzdFBhY2thZ2UudHJpZ2dlckFjdGlvbigpO1xuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuXG4gIG5nQWZ0ZXJWaWV3SW5pdCgpOiB2b2lkIHtcbiAgICBpZiAodGhpcy5vcHRpb25zLmVycm9yKSB7XG5cbiAgICAgIHRoaXMuaG90a2V5c1NlcnZpY2UuYWRkKG5ldyBIb3RrZXkoJ2VzYycsIChldmVudDogS2V5Ym9hcmRFdmVudCk6IGJvb2xlYW4gPT4ge1xuICAgICAgICB0aGlzLnRvYXN0clNlcnZpY2UuY2xlYXIodGhpcy50b2FzdHJTZXJ2aWNlLmN1cnJlbnRseUFjdGl2ZSk7XG4gICAgICAgIHRoaXMucmVtb3ZlKCk7XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgIH0pKTtcblxuICAgICAgY29uc3QgdG9hc3QgPSB0aGlzLnRvYXN0clNlcnZpY2UudG9hc3RzLmZpbmQoYyA9PiBjLnRvYXN0SWQgPT09IHRoaXMudG9hc3RQYWNrYWdlLnRvYXN0SWQgJiYgIWMudG9hc3RSZWYuaXNJbmFjdGl2ZSgpKTtcblxuICAgICAgaWYgKHRvYXN0KSB7XG4gICAgICAgIHRoaXMuZGl2Q2VudGVyID0gdGhpcy5yZW5kZXJlci5jcmVhdGVFbGVtZW50KCdkaXYnKTtcbiAgICAgICAgdGhpcy5yZW5kZXJlci5hZGRDbGFzcyh0aGlzLmRpdkNlbnRlciwgJ2Jsb2NrLXRvYXN0Jyk7XG5cbiAgICAgICAgY29uc3QgZXhpc3RCbG9ja1RvYXN0ID0gdGhpcy50b2FzdHJTZXJ2aWNlLnRvYXN0cy5zb21lKChjKSA9PiB0aGlzLmlzQmxvY2tCb2R5VG9hc3QoYy50b2FzdFJlZikpO1xuXG4gICAgICAgIGlmICghZXhpc3RCbG9ja1RvYXN0KSB7XG4gICAgICAgICAgdGhpcy5ibG9ja0JvZHlUb2FzdCA9IHRydWU7XG4gICAgICAgICAgZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZCh0aGlzLmRpdkNlbnRlcik7XG4gICAgICAgIH1cblxuICAgICAgICB0b2FzdC5vbkhpZGRlbi5zdWJzY3JpYmUoKCkgPT4ge1xuICAgICAgICAgIGlmICh0aGlzLmJsb2NrQm9keVRvYXN0KSB7XG4gICAgICAgICAgICBkb2N1bWVudC5ib2R5LnJlbW92ZUNoaWxkKHRoaXMuZGl2Q2VudGVyKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIGlzQmxvY2tCb2R5VG9hc3QodG9hc3Q6IFRvYXN0UmVmPGFueT4pOiBib29sZWFuIHtcbiAgICBpZiAodG9hc3QgJiYgKHRvYXN0LmNvbXBvbmVudEluc3RhbmNlIGluc3RhbmNlb2YgVG9hc3RDb21wb25lbnQpKSB7XG4gICAgICByZXR1cm4gdG9hc3QuY29tcG9uZW50SW5zdGFuY2UuYmxvY2tCb2R5VG9hc3Q7XG4gICAgfVxuXG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG59XG4iXX0=