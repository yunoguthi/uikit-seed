/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ChangeDetectionStrategy } from '@angular/core';
import { NavQuery } from './../nav/state/nav.query';
import { Subscription } from 'rxjs';
import { NavService } from './../nav/state/nav.service';
var IaComponent = /** @class */ (function () {
    function IaComponent(navQuery, navService) {
        this.navQuery = navQuery;
        this.navService = navService;
        this.rightnav$ = this.navQuery.rightnav$;
        this.subscription = new Subscription();
    }
    /**
     * @return {?}
     */
    IaComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.subscription.add(this.rightnav$.subscribe((/**
         * @param {?} rightnav
         * @return {?}
         */
        function (rightnav) {
            _this.rightnav = rightnav;
        })));
    };
    /**
     * @return {?}
     */
    IaComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this.subscription.unsubscribe();
    };
    /**
     * @return {?}
     */
    IaComponent.prototype.rightNavToggle = /**
     * @return {?}
     */
    function () {
        this.navService.toggleRightNav();
    };
    /**
     * @return {?}
     */
    IaComponent.prototype.rightNavOpen = /**
     * @return {?}
     */
    function () {
        this.navService.openRightNav();
    };
    /**
     * @return {?}
     */
    IaComponent.prototype.rightNavClose = /**
     * @return {?}
     */
    function () {
        this.navService.closeRightNav();
    };
    /**
     * @return {?}
     */
    IaComponent.prototype.togglePinRightNav = /**
     * @return {?}
     */
    function () {
        this.navService.togglePinRightNav();
    };
    IaComponent.decorators = [
        { type: Component, args: [{
                    selector: 'uikit-ia',
                    template: "<mat-toolbar>\n  <button\n    class=\"aikit-button\"\n    mat-flat-button\n    type=\"button\"\n    aria-label=\"Abri menu de navega\u00E7\u00E3o\"\n    aria-controls=\"navigation\"\n    matTooltip=\"Abri menu de navega\u00E7\u00E3o\"\n    (click)=\"rightNavToggle()\"\n    tabindex=\"-1\"\n  >\n    <mat-icon class=\"fas fa-times\" aria-label=\"Fixar menu na tela\"></mat-icon>\n  </button>\n\n  <button\n    mat-icon-button\n    color=\"primary\"\n    class=\"btn-attach-chat\"\n    (click)=\"togglePinRightNav()\"\n    [class.is-active]=\"(rightnav$ | async).opened\"\n    [class.is-pinned]=\"(rightnav$ | async).pinned\"\n    tabindex=\"-1\"\n  >\n    <mat-icon\n      class=\"fas fa-thumbtack\"\n      aria-label=\"Fixar menu na tela\"\n    ></mat-icon>\n  </button>\n\n  <img src=\"../../assets/images/iakit-logotipo-mobile.svg\" class=\"is-mobile\" />\n  <img src=\"../../assets/images/iakit-logotipo.svg\" class=\"is-desktop\" />\n</mat-toolbar>\n\n<div class=\"chat\">\n  <div class=\"messages\">\n    <div class=\"message left\">\n      <picture> </picture>\n      <div>Oi, meu nome \u00E9 Judi, em que posso ajudar?</div>\n    </div>\n\n    <div class=\"message right\">\n      <picture>\n        <mat-icon\n          aria-label=\"Example icon-button with a heart icon\"\n          class=\"fas fa-user\"\n        ></mat-icon>\n      </picture>\n      <div>Mensagem do Usu\u00E1rio</div>\n    </div>\n  </div>\n</div>\n\n<div class=\"actions\">\n  <form>\n    <mat-form-field color=\"primary\" appearance=\"fill\">\n      <mat-label>Criar uma nova anota\u00E7\u00E3o</mat-label>\n      <input matInput #message maxlength=\"256\" placeholder=\"Message\" cdkFocusInitial />\n    </mat-form-field>\n    <button mat-flat-button color=\"primary\">ENVIAR</button>\n  </form>\n</div>\n",
                    changeDetection: ChangeDetectionStrategy.OnPush,
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    IaComponent.ctorParameters = function () { return [
        { type: NavQuery },
        { type: NavService }
    ]; };
    return IaComponent;
}());
export { IaComponent };
if (false) {
    /** @type {?} */
    IaComponent.prototype.rightnav$;
    /** @type {?} */
    IaComponent.prototype.rightnav;
    /**
     * @type {?}
     * @protected
     */
    IaComponent.prototype.subscription;
    /**
     * @type {?}
     * @protected
     */
    IaComponent.prototype.navQuery;
    /**
     * @type {?}
     * @protected
     */
    IaComponent.prototype.navService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaWEuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9sYXlvdXQvaWEvaWEuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLHVCQUF1QixFQUFhLE1BQU0sZUFBZSxDQUFDO0FBQ3RGLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUNwRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ3BDLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUV4RDtJQVlFLHFCQUNZLFFBQWtCLEVBQ2xCLFVBQXNCO1FBRHRCLGFBQVEsR0FBUixRQUFRLENBQVU7UUFDbEIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQU4zQixjQUFTLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUM7UUFFakMsaUJBQVksR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO0lBSU4sQ0FBQzs7OztJQUV2Qyw4QkFBUTs7O0lBQVI7UUFBQSxpQkFNQztRQUxDLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUNuQixJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVM7Ozs7UUFBQyxVQUFBLFFBQVE7WUFDL0IsS0FBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7UUFDM0IsQ0FBQyxFQUFDLENBQ0gsQ0FBQztJQUNKLENBQUM7Ozs7SUFHRCxpQ0FBVzs7O0lBQVg7UUFDRSxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQ2xDLENBQUM7Ozs7SUFFRCxvQ0FBYzs7O0lBQWQ7UUFDRSxJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsRUFBRSxDQUFDO0lBQ25DLENBQUM7Ozs7SUFDRCxrQ0FBWTs7O0lBQVo7UUFDRSxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksRUFBRSxDQUFDO0lBQ2pDLENBQUM7Ozs7SUFDRCxtQ0FBYTs7O0lBQWI7UUFDRSxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ2xDLENBQUM7Ozs7SUFDRCx1Q0FBaUI7OztJQUFqQjtRQUNFLElBQUksQ0FBQyxVQUFVLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztJQUN0QyxDQUFDOztnQkF4Q0YsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxVQUFVO29CQUNwQix3d0RBQWtDO29CQUVsQyxlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTs7aUJBQ2hEOzs7O2dCQVRRLFFBQVE7Z0JBRVIsVUFBVTs7SUE0Q25CLGtCQUFDO0NBQUEsQUExQ0QsSUEwQ0M7U0FwQ1ksV0FBVzs7O0lBRXRCLGdDQUEyQzs7SUFDM0MsK0JBQXNEOzs7OztJQUN0RCxtQ0FBNEM7Ozs7O0lBRzFDLCtCQUE0Qjs7Ozs7SUFDNUIsaUNBQWdDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIENoYW5nZURldGVjdGlvblN0cmF0ZWd5LCBPbkRlc3Ryb3kgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE5hdlF1ZXJ5IH0gZnJvbSAnLi8uLi9uYXYvc3RhdGUvbmF2LnF1ZXJ5JztcbmltcG9ydCB7IFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgTmF2U2VydmljZSB9IGZyb20gJy4vLi4vbmF2L3N0YXRlL25hdi5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAndWlraXQtaWEnLFxuICB0ZW1wbGF0ZVVybDogJy4vaWEuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9pYS5jb21wb25lbnQuc2NzcyddLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaFxufSlcbmV4cG9ydCBjbGFzcyBJYUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcblxuICBwdWJsaWMgcmlnaHRuYXYkID0gdGhpcy5uYXZRdWVyeS5yaWdodG5hdiQ7XG4gIHB1YmxpYyByaWdodG5hdjogeyBvcGVuZWQ6IGJvb2xlYW47IHBpbm5lZDogYm9vbGVhbiB9O1xuICBwcm90ZWN0ZWQgc3Vic2NyaXB0aW9uID0gbmV3IFN1YnNjcmlwdGlvbigpO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByb3RlY3RlZCBuYXZRdWVyeTogTmF2UXVlcnksXG4gICAgcHJvdGVjdGVkIG5hdlNlcnZpY2U6IE5hdlNlcnZpY2UpIHsgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIHRoaXMuc3Vic2NyaXB0aW9uLmFkZChcbiAgICAgIHRoaXMucmlnaHRuYXYkLnN1YnNjcmliZShyaWdodG5hdiA9PiB7XG4gICAgICAgIHRoaXMucmlnaHRuYXYgPSByaWdodG5hdjtcbiAgICAgIH0pXG4gICAgKTtcbiAgfVxuXG5cbiAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XG4gICAgdGhpcy5zdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcbiAgfVxuXG4gIHJpZ2h0TmF2VG9nZ2xlKCkge1xuICAgIHRoaXMubmF2U2VydmljZS50b2dnbGVSaWdodE5hdigpO1xuICB9XG4gIHJpZ2h0TmF2T3BlbigpIHtcbiAgICB0aGlzLm5hdlNlcnZpY2Uub3BlblJpZ2h0TmF2KCk7XG4gIH1cbiAgcmlnaHROYXZDbG9zZSgpIHtcbiAgICB0aGlzLm5hdlNlcnZpY2UuY2xvc2VSaWdodE5hdigpO1xuICB9XG4gIHRvZ2dsZVBpblJpZ2h0TmF2KCkge1xuICAgIHRoaXMubmF2U2VydmljZS50b2dnbGVQaW5SaWdodE5hdigpO1xuICB9XG5cbn1cbiJdfQ==