import { UpdateService } from './update.service';
import { UpdateInfoService } from './update-info.service';
export declare class UpdateModule {
    updateService: UpdateService;
    updateInfoService: UpdateInfoService;
    constructor(updateService: UpdateService, updateInfoService: UpdateInfoService);
}
