import { ApplicationRef, OnDestroy, NgZone } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { UpdateInfoService } from './update-info.service';
import { LogService } from '../utils/log/log.service';
import { ToastService } from '../layout/toast/toast.service';
export declare class UpdateService implements OnDestroy {
    protected matDialog: MatDialog;
    protected toastService: ToastService;
    protected appRef: ApplicationRef;
    protected ngZone: NgZone;
    protected router: Router;
    protected swUpdate: SwUpdate;
    protected logService: LogService;
    protected updateInfoService: UpdateInfoService;
    private primeiroAcesso;
    private hasUpdate;
    private updateSubscription;
    private matDialogRef;
    constructor(matDialog: MatDialog, toastService: ToastService, appRef: ApplicationRef, ngZone: NgZone, router: Router, swUpdate: SwUpdate, logService: LogService, updateInfoService: UpdateInfoService);
    ngOnDestroy(): void;
    update(): void;
    private checkUpdate;
}
