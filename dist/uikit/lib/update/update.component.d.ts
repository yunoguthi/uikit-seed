import { UpdateInfoService } from './update-info.service';
export declare class UpdateComponent {
    protected updateInfoService: UpdateInfoService;
    hasUpdate$: import("rxjs").Observable<boolean>;
    constructor(updateInfoService: UpdateInfoService);
}
