import { BehaviorSubject } from 'rxjs';
import { SwUpdate } from '@angular/service-worker';
import { LogService } from '../utils/log/log.service';
export declare class UpdateInfoService {
    protected swUpdate: SwUpdate;
    protected logService: LogService;
    protected hasUpdateSubject$: BehaviorSubject<boolean>;
    hasUpdate$: import("rxjs").Observable<boolean>;
    protected installPromptEventSubject$: BehaviorSubject<any>;
    protected installPromptEvent$: import("rxjs").Observable<any>;
    hasInstallOption$: import("rxjs").Observable<boolean>;
    constructor(swUpdate: SwUpdate, logService: LogService);
    setUpdateAsAvailable(): void;
    install(): void;
}
