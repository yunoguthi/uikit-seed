import { OperationsManagerService } from './operations-manager.service';
export declare class OperationsModule {
    protected operationsManagerService: OperationsManagerService;
    constructor(operationsManagerService: OperationsManagerService);
}
