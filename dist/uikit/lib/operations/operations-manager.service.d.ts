import { OperationsService } from './operations.service';
import { MatSnackBar } from '@angular/material';
export declare class OperationsManagerService {
    protected operationsService: OperationsService;
    protected snackBar: MatSnackBar;
    private isLoadSnackbarActive;
    constructor(operationsService: OperationsService, snackBar: MatSnackBar);
    close(): void;
}
