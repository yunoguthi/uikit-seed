import { OnInit } from '@angular/core';
import { OperationsService } from './operations.service';
import { Observable } from 'rxjs';
import { OperationsSnackBar } from './operations.snackbar';
import { OperationsManagerService } from './operations-manager.service';
export declare class OperationsComponent implements OnInit {
    protected operationsService: OperationsService;
    protected operationsManagerService: OperationsManagerService;
    operations$: Observable<OperationsSnackBar[]>;
    isLoading$: Observable<boolean>;
    constructor(operationsService: OperationsService, operationsManagerService: OperationsManagerService);
    ngOnInit(): void;
    close(): void;
}
