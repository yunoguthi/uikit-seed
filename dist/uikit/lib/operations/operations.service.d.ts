import { Observable } from 'rxjs';
import { OperationsSnackBar } from './operations.snackbar';
export declare class OperationsService {
    private operationsSubject$;
    operations$: Observable<OperationsSnackBar[]>;
    private isLoadingBehaviourSubject$;
    isLoading$: Observable<boolean>;
    isLoading: boolean;
    constructor();
    create<T>(description?: string, operation?: Observable<T>, undoOperation?: (() => Observable<any>) | PromiseLike<any> | any, allowToCancel?: boolean, createAsProcessing?: boolean): Observable<T> & {
        snackbar: OperationsSnackBar;
    };
    cancelAll(cancellable?: (operation: OperationsSnackBar) => boolean): void;
    private operationIsFinished;
    private checkLoading;
    private addOperation;
    private removeOperation;
}
