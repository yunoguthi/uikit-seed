import { Subscription, Observable } from 'rxjs';
export declare enum OperationState {
    Idle = "idle",
    Processing = "processing",
    Processed = "processed",
    Cancelling = "cancelling",
    Cancelled = "cancelled",
    Undoing = "undoing",
    Undone = "undone",
    Error = "error"
}
export declare class OperationsSnackBar {
    description: string;
    allowToCancel: boolean;
    private operationStateSubject$;
    operationState$: Observable<OperationState>;
    operationState: OperationState;
    error: Error;
    operation$: Observable<any>;
    protected undoOperation: (() => any) | Observable<any> | PromiseLike<any>;
    private isUndoableSubject$;
    protected isUndoable$: Observable<boolean>;
    protected isUndoable: boolean;
    private canUndoSubject$;
    canUndo$: Observable<boolean>;
    canUndo: boolean;
    protected cancelOperation: Subscription;
    private isCancellableSubject$;
    protected isCancellable$: Observable<boolean>;
    protected isCancellable: boolean;
    private canCancelSubject$;
    canCancel$: Observable<boolean>;
    canCancel: boolean;
    constructor(description: string, allowToCancel?: boolean);
    setCancelOperation(subscription: Subscription): void;
    setUndoOperation(undoFunction: (() => any) | Observable<any> | PromiseLike<any>): void;
    setOperation(operation: Observable<any>): void;
    setAsProcessed(): void;
    setError(error: Error): Observable<never>;
    protected setState(operationState: OperationState): void;
    setAsStarted(): void;
    undo(): any;
    cancel(): void;
}
