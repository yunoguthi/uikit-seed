import { OnInit } from '@angular/core';
import { Location } from '@angular/common';
export declare class NotFoundComponent implements OnInit {
    private ngLocation;
    imageSrc: any;
    constructor(ngLocation: Location);
    ngOnInit(): void;
    goBack(): void;
}
