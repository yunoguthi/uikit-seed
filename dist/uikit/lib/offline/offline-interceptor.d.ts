import { Observable } from 'rxjs';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
export declare class OfflineInterceptor implements HttpInterceptor {
    private onlineChanges$;
    readonly isOnline: boolean;
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>;
}
