import { OnInit, OnDestroy, AfterViewInit, EventEmitter, TemplateRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroupDirective } from '@angular/forms';
import { MatDialog, MatDialogRef, MatInput } from '@angular/material';
import { HotkeysService } from 'angular2-hotkeys';
import { FilterService } from './filter.service';
export declare class FilterComponent implements OnInit, AfterViewInit, OnDestroy {
    protected hotkeysService: HotkeysService;
    protected filterService: FilterService;
    dialog: MatDialog;
    clickPesquisa: EventEmitter<any>;
    disabled: boolean;
    searchInput: MatInput;
    template: TemplateRef<{}>;
    filtersForm: FormGroupDirective;
    filterBadge$: import("rxjs").Observable<number>;
    showFilters$: import("rxjs").Observable<boolean>;
    protected subscriptions: Subscription[];
    dialogRef: MatDialogRef<any>;
    constructor(hotkeysService: HotkeysService, filterService: FilterService, dialog: MatDialog);
    ngOnInit(): void;
    ngAfterViewInit(): void;
    ngOnDestroy(): void;
    pesquisar(): void;
    toggleFilters(): void;
    onNoClick(): void;
}
