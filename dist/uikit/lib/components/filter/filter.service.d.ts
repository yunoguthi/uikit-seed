import { BehaviorSubject } from 'rxjs';
export declare class FilterService {
    protected filterBadgeSubject: BehaviorSubject<number>;
    filterBadge$: import("rxjs").Observable<number>;
    protected showFiltersSubject: BehaviorSubject<boolean>;
    showFilters$: import("rxjs").Observable<boolean>;
    constructor();
    setCountFilter(count: number): void;
    setShowFilters(): void;
}
