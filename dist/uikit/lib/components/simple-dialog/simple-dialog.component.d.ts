import { OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
export declare enum DialogResult {
    Confirmed = "Confirmado",
    Cancelled = "Cancelado"
}
export declare enum ButtonDialog {
    YesNo = 0,
    OkCancel = 1
}
export interface DialogOptions {
    title: string;
    description: string;
    items?: any[];
    button?: ButtonDialog;
}
export declare class SimpleDialogComponent implements OnInit {
    dialogRef: MatDialogRef<any, DialogResult>;
    data: DialogOptions;
    columns: string[];
    cancelButtonText: string;
    confirmButtonText: string;
    constructor(dialogRef: MatDialogRef<any, DialogResult>, data: DialogOptions);
    onCancelClick(): void;
    onConfirmClick(): void;
    ngOnInit(): void;
}
