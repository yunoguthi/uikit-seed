import { MatDialog } from '@angular/material';
import { DialogResult } from './simple-dialog.component';
export declare class DialogService {
    protected dialog: MatDialog;
    constructor(dialog: MatDialog);
    show(description: string, items?: any[], title?: string, width?: string): import("rxjs").Observable<DialogResult>;
}
