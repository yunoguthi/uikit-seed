import { AfterViewInit, Renderer2, EventEmitter } from '@angular/core';
import { Location } from '@angular/common';
import { MatButton } from '@angular/material';
export declare enum SaveButtonSelection {
    Salvar = "Salvar",
    SalvarVoltar = "Salvar e voltar",
    SalvarNovo = "Salvar e novo"
}
export declare class SaveButtonComponent implements AfterViewInit {
    protected renderer: Renderer2;
    protected ngLocation: Location;
    /** Use the parent form to save (using submit), new (reset) and back (history) */
    save: EventEmitter<SaveButtonSelection>;
    protected submitButton: MatButton;
    protected resetButton: MatButton;
    selection: SaveButtonSelection;
    disabled: boolean;
    constructor(renderer: Renderer2, ngLocation: Location);
    ngAfterViewInit(): void;
    saveAction(): void;
    saveBackAction(): void;
    saveNewAction(): void;
    action(): void;
}
