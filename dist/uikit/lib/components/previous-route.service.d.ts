import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
export declare class PreviousRouteService {
    private router;
    private history;
    desabilitarBotaoVoltar: BehaviorSubject<boolean>;
    private rotasAcessadas;
    constructor(router: Router);
    getHistory(): string[];
    getPreviousUrl(): string;
    removerRotaAcessada: () => any[];
}
