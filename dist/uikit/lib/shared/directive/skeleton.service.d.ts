import { ElementRef, Renderer2 } from '@angular/core';
export declare class SkeletonService {
    protected el: ElementRef;
    protected renderer: Renderer2;
    constructor(el: ElementRef, renderer: Renderer2);
    show(): void;
    hide(): void;
}
