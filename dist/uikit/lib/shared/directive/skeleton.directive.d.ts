import { ElementRef, Renderer2 } from '@angular/core';
import { SkeletonService } from "./skeleton.service";
export declare class SkeletonDirective {
    protected el: ElementRef;
    protected renderer: Renderer2;
    isLoading: any;
    protected skeletonService: SkeletonService;
    constructor(el: ElementRef, renderer: Renderer2);
}
