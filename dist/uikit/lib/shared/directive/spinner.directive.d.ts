import { ElementRef, Renderer2, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';
import { SpinnerService } from "./spinner.service";
export declare class SpinnerDirective {
    protected el: ElementRef;
    protected renderer: Renderer2;
    protected viewContainerRef: ViewContainerRef;
    protected componentFactoryResolver: ComponentFactoryResolver;
    isLoading: any;
    protected spinnerService: SpinnerService;
    constructor(el: ElementRef, renderer: Renderer2, viewContainerRef: ViewContainerRef, componentFactoryResolver: ComponentFactoryResolver);
}
