import { ComponentFactoryResolver, ElementRef, Renderer2, ViewContainerRef } from '@angular/core';
import { MatSpinner } from "@angular/material/progress-spinner";
export declare class SpinnerService {
    protected el: ElementRef;
    protected renderer: Renderer2;
    protected viewContainerRef: ViewContainerRef;
    protected componentFactoryResolver: ComponentFactoryResolver;
    protected spinner: MatSpinner;
    protected divCenter: any;
    constructor(el: ElementRef, renderer: Renderer2, viewContainerRef: ViewContainerRef, componentFactoryResolver: ComponentFactoryResolver);
    Init(): void;
    hide(): void;
    show(): void;
}
