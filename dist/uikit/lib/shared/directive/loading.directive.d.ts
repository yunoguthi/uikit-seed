import { ElementRef, Renderer2, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';
import { MatTable } from "@angular/material/table";
import { SkeletonService } from "./skeleton.service";
import { SpinnerService } from "./spinner.service";
export declare class LoadingDirective {
    protected el: ElementRef;
    protected renderer: Renderer2;
    protected viewContainerRef: ViewContainerRef;
    protected componentFactoryResolver: ComponentFactoryResolver;
    matTable: MatTable<any>;
    isLoading: any;
    protected skeletonService: SkeletonService;
    protected spinnerService: SpinnerService;
    constructor(el: ElementRef, renderer: Renderer2, viewContainerRef: ViewContainerRef, componentFactoryResolver: ComponentFactoryResolver, matTable: MatTable<any>);
    show(): void;
    hide(): void;
}
