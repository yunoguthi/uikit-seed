export declare class LocalStorageService {
    get(key: any): any;
    set(key: any, value: any): void;
    delete(key: any): any;
}
