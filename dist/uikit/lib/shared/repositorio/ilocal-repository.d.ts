export interface ILocalRepository {
    salvarItem<T>(key: string, data: T): any;
    obterItem<T>(key: any): T;
    deletar(key: any): void;
}
