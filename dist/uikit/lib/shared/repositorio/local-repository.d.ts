import { ILocalRepository } from './ilocal-repository';
import { LocalStorageService } from './localstorage.service';
export declare class LocalRepository implements ILocalRepository {
    private localStorageService;
    constructor(localStorageService: LocalStorageService);
    salvarItem<T>(key: string, data: T): void;
    obterItem<T>(key: any): T;
    deletar(key: any): void;
}
