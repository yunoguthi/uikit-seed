import { PipeTransform } from '@angular/core';
export declare class HighlightPipe implements PipeTransform {
    transform(value: string, term: string): string;
    createRegex(input: any): RegExp;
    createReplacements(str: any): any;
}
