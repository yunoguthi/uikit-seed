import { FiltroOperador } from './filter';
export declare enum SearchMode {
    FullTextSearch = 0,
    Filtro = 1
}
export interface Pesquisa {
    termo: string;
    config?: {
        modo?: SearchMode;
        filtro?: {
            campos: string[];
            operador?: FiltroOperador;
        };
    };
}
