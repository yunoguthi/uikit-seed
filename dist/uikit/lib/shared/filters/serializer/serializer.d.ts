import { ParametrosDaRequisicao } from '../request-parameters';
export interface Serializer<TReturn = string> {
    serialize(parametros: ParametrosDaRequisicao): TReturn;
}
