import { InjectionToken } from '@angular/core';
import { FiltroComposto, Filtro, FiltroOperador, FiltroCompostoOperador } from '../filter';
import { ParametrosDaRequisicao } from '../request-parameters';
import { Serializer } from './serializer';
import { Ordenacao } from '../sort';
import { Paginacao } from '../page';
import { SearchMode, Pesquisa } from '../pesquisa';
export declare const SERIALIZER_TOKEN: InjectionToken<Serializer<string>>;
export declare abstract class BaseSerializerService implements Serializer {
    protected abstract getFiltroOperadorMapeado(filtroOperador: FiltroOperador): string;
    protected abstract getFiltroCompostoOperadorMapeado(FiltroCompostoOperador: FiltroCompostoOperador): string;
    /**
     * Formato que sera serializado a
     * Tag correspondente a query string
     * enviada na requizição da pesquisa.
     *
     * A palavra chave para substituição pelo filtro é `{filtros}`
     * */
    protected abstract filtroClausulaFormato: string;
    /**
     *  Corresponde a tag que sera aplicada na formatação dos filtros.
     *
     *  A palavra chave para substituição pelo filtro é `{filtro}`
     * */
    protected abstract filtroFormato: string;
    /**
     * Habilita suporte para modo de pesquisa.
     * */
    protected abstract suportaPesquisa: boolean;
    /**
     * Habilita suporte para modo de pesquisa utilizando a lógica com filtros.
     * */
    protected abstract suportaFiltros: boolean;
    /**
     *  Habilita suporte para modo de pesquisa utilizando a lógica com paginação.
     * */
    protected abstract suportaPaginacao: boolean;
    /**
     * Habilita suporte para modo de pesquisa utilizando a lógica com ordenação.
     * */
    protected abstract suportaOrdenacao: boolean;
    /**
     * Formato que sera serializado a
     * Tag correspondente a query string 'skip'
     * enviada na requisição.
     *
     * A palavra chave para substituição é `{pagina}`
     * */
    protected abstract paginacaoPaginaClausulaFormato: string;
    /**
     *  Corresponde a tag que sera aplicada na formatação das clausula dos itens por página.
     *
     *  A palavra chave para substituição é `{itensPorPagina}`
     * */
    protected abstract paginacaoItensPorPaginaClausulaFormato: string;
    /**
     * Formato que sera serializado a
     * Tag correspondente a query string
     * enviada na requizição da ordernação da pesquisa.
     *
     * A palavra chave para substituição é `{ordenacoes}`
     * */
    protected abstract ordenacaoClausulaFormato: string;
    /**
     *  Corresponde a tag que sera aplicada na formatação das clausula de ordenação.
     *
     *  As palavras chaves para substituição é `{campo}` e `{direcao}`
     * */
    protected abstract ordenacaoFormato: string;
    /**
     *  Corresponde ao separador que sera aplicado na serialização das ordenações.
     * */
    protected abstract ordenacoesSeparador: string;
    protected quantidadeDeFiltrosAlinhadosSuportados: number;
    serializeFiltros(parametrosDaRequisicao: ParametrosDaRequisicao): string;
    serializeFiltroComposto(filtroComposto: FiltroComposto, nivel?: number): string;
    serializeFiltro(filtro: Filtro): string;
    protected tratarArray(stringArray: string[], join?: string): string;
    protected filtroValorInterceptor(filtro: Filtro): string;
    protected filtroCampoInterceptor(filtro: Filtro): string;
    protected pesquisaClausulaFormato?: string;
    protected pesquisaTermoInterceptor(pesquisa: Pesquisa): string;
    protected pesquisaFiltroOperadorPadrao: FiltroOperador;
    protected pesquisaFiltroCompostoOperadorPadrao: FiltroCompostoOperador;
    protected readonly pesquisaModoPadrao: SearchMode;
    serializePesquisa(parametrosDaRequisicao: ParametrosDaRequisicao): string;
    serialize(parametros: ParametrosDaRequisicao): string;
    protected serializeInterceptor?(parametros: ParametrosDaRequisicao): string;
    serializePaginacao(parametrosDaRequisicao: ParametrosDaRequisicao): string;
    protected paginacaoPaginaInterceptor(paginacao: Paginacao): number;
    protected paginacaoItensPorPaginaInterceptor(paginacao: Paginacao): number;
    serializeOrdenacao(parametrosDaRequisicao: ParametrosDaRequisicao): string;
    protected ordenacaoCampoInterceptor(ordenacao: Ordenacao): string;
    protected ordenacaoDirecaoInterceptor(ordenacao: Ordenacao): "desc" | "asc";
    protected format(formato: string, ...args: any): string;
}
