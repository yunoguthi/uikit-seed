import { BaseSerializerService } from '../base-serializer.service';
import { FiltroCompostoOperador, FiltroOperador } from '../../filter';
export declare class RestSerializerService extends BaseSerializerService {
    protected pesquisaFiltroOperadorPadrao: FiltroOperador;
    protected suportaPesquisa: boolean;
    protected suportaFiltros: boolean;
    protected suportaPaginacao: boolean;
    protected suportaOrdenacao: boolean;
    protected getFiltroOperadorMapeado(filtroOperador: FiltroOperador): string;
    protected getFiltroCompostoOperadorMapeado(filtroCompostoOperador: FiltroCompostoOperador): string;
    protected quantidadeDeFiltrosAlinhadosSuportados: number;
    protected filtroClausulaFormato: string;
    protected filtroFormato: string;
    protected paginacaoPaginaClausulaFormato: string;
    protected paginacaoItensPorPaginaClausulaFormato: string;
    protected ordenacaoClausulaFormato: string;
    protected ordenacaoFormato: string;
    protected ordenacoesSeparador: string;
}
