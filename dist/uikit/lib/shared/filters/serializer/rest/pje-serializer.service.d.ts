import { BaseSerializerService } from '../base-serializer.service';
import { InjectionToken } from '@angular/core';
import { Filtro, FiltroCompostoOperador, FiltroOperador } from '../../filter';
import { ParametrosDaRequisicao } from '../../request-parameters';
import { Paginacao } from '../../page';
export declare const PjeDataSerializerServiceConfigToken: InjectionToken<PjeDataSerializerServiceConfig>;
export interface PjeDataSerializerServiceConfig {
    requestCount: boolean;
}
export declare class PjeSerializerService extends BaseSerializerService {
    protected suportaPesquisa: boolean;
    protected suportaFiltros: boolean;
    protected suportaPaginacao: boolean;
    protected suportaOrdenacao: boolean;
    protected ordenacaoClausulaFormato: string;
    protected ordenacaoFormato: string;
    protected ordenacoesSeparador: string;
    protected paginacaoPaginaClausulaFormato: string;
    protected paginacaoItensPorPaginaClausulaFormato: string;
    protected filtroClausulaFormato: string;
    protected filtroFormato: string;
    protected pesquisaClausulaFormato: string;
    protected pesquisaFiltroCompostoOperadorPadrao: FiltroCompostoOperador;
    dataConfig: PjeDataSerializerServiceConfig;
    constructor(oDataSerializerServiceConfig?: PjeDataSerializerServiceConfig);
    protected paginacaoPaginaInterceptor(paginacao: Paginacao): number;
    protected getFiltroOperadorMapeado(filtroOperador: FiltroOperador): string;
    serializePaginacao(parametrosDaRequisicao: ParametrosDaRequisicao): string;
    protected getFiltroCompostoOperadorMapeado(filtroCompostoOperador: FiltroCompostoOperador): string;
    protected filtroValorInterceptor(filtro: Filtro): string;
}
