import { BaseSerializerService } from '../base-serializer.service';
import { InjectionToken } from '@angular/core';
import { Filtro, FiltroCompostoOperador, FiltroOperador } from '../../filter';
import { ParametrosDaRequisicao } from '../../request-parameters';
import { Paginacao } from '../../page';
export declare const ODataSerializerServiceConfigToken: InjectionToken<ODataSerializerServiceConfig>;
export interface ODataSerializerServiceConfig {
    requestCount: boolean;
}
export declare class ODataSerializerService extends BaseSerializerService {
    protected pesquisaFiltroOperadorPadrao: FiltroOperador;
    protected suportaPesquisa: boolean;
    protected suportaFiltros: boolean;
    protected suportaPaginacao: boolean;
    protected suportaOrdenacao: boolean;
    pesquisaClausulaFormato: string;
    oDataConfig: ODataSerializerServiceConfig;
    constructor(oDataSerializerServiceConfig?: ODataSerializerServiceConfig);
    protected serializeInterceptor(parametro: ParametrosDaRequisicao): string;
    ordenacaoClausulaFormato: string;
    ordenacaoFormato: string;
    ordenacoesSeparador: string;
    protected paginacaoPaginaInterceptor(paginacao: Paginacao): number;
    paginacaoPaginaClausulaFormato: string;
    paginacaoItensPorPaginaClausulaFormato: string;
    filtroClausulaFormato: string;
    filtroFormato: string;
    protected getFiltroOperadorMapeado(filtroOperador: FiltroOperador): string;
    protected getFiltroCompostoOperadorMapeado(filtroCompostoOperador: FiltroCompostoOperador): string;
    protected filtroValorInterceptor(filtro: Filtro): string;
}
