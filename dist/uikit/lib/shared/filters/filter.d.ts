export declare function isFilterComposite(possivelFiltroComposto: FiltroComposto | Filtro): possivelFiltroComposto is FiltroComposto;
export declare enum FiltroCompostoOperador {
    E = "and",
    Ou = "or"
}
export interface FiltroComposto {
    operador: FiltroCompostoOperador;
    filtros: Array<FiltroComposto | Filtro>;
}
export interface Filtro<TCampo = any> {
    campo: string;
    operador: FiltroOperador;
    valor?: TCampo;
}
export declare enum FiltroOperador {
    Igual = "Igual",
    Diferente = "Diferente",
    ComecaCom = "Come\u00E7a com",
    TerminaCom = "Termina com",
    Contem = "Cont\u00E9m",
    NaoContem = "N\u00E3o cont\u00E9m",
    MaiorOuIgual = "Maior ou igual",
    Maior = "Maior",
    Menor = "Menor",
    MenorOuIgual = "Menor ou igual",
    Em = "Em",
    NaoEm = "N\u00E3o em"
}
