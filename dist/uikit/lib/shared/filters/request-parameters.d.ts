import { FiltroComposto } from './filter';
import { Ordenacao } from './sort';
import { Paginacao } from './page';
import { Pesquisa } from './pesquisa';
export interface ParametrosDaRequisicao {
    filtros: FiltroComposto;
    ordenacoes: Ordenacao[];
    paginacao: Paginacao;
    pesquisa: Pesquisa;
}
