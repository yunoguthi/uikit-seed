import { Observable, Subject } from 'rxjs';
export declare function prepare<T>(callback: () => void): (source: Observable<T>) => Observable<T>;
export declare function indicate<T>(indicator: Subject<boolean>): (source: Observable<T>) => Observable<T>;
