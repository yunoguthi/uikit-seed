import { User } from './user.model';
export declare class ProviderUserTransformationService<SourceUser, DestinationUser extends User> {
    constructor();
    transform(user: SourceUser): DestinationUser;
}
