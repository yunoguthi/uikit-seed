import { Location, PlatformLocation, LocationStrategy } from '@angular/common';
import { Router } from '@angular/router';
import { User } from './user.model';
import { IAuthenticationManager } from './authentication-manager';
import { UserService } from './user.service';
import { IProviderAuthenticationService } from './abstraction/provider-authentication.service';
export declare class AuthenticationService<TLoginParam = any, TLogoutParam = any> {
    protected userService: UserService;
    private ngLocation;
    private platformLocation;
    private locationStrategy;
    private router;
    private providerAuthenticationService?;
    protected authenticationManager?: IAuthenticationManager;
    constructor(userService: UserService, ngLocation: Location, platformLocation: PlatformLocation, locationStrategy: LocationStrategy, router: Router, providerAuthenticationService?: IProviderAuthenticationService<User>, authenticationManager?: IAuthenticationManager);
    login(args?: {
        param?: TLoginParam;
        enderecoParaVoltar?: string;
    }): Promise<void>;
    private load;
    protected goToLastUri(): void;
    logout(param?: TLogoutParam): Promise<void>;
}
