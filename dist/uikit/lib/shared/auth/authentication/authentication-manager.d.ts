import { User } from './user.model';
export interface IAuthenticationManager<TFrameworkUser extends User = User, TApplicationUser extends User = User> {
    transform(user: TFrameworkUser): Promise<TApplicationUser>;
}
