import { HttpClient } from '@angular/common/http';
import { IAuthenticationManager } from '../authentication-manager';
import { AuthorizationConfig } from '../../authorization/authorization-config.token';
import { OpenIDConnectUser, UserWithAuthorizations } from '../user.model';
export declare class Wso2AuthenticationManager implements IAuthenticationManager {
    protected httpClient: HttpClient;
    protected authorizationConfig: AuthorizationConfig;
    constructor(httpClient: HttpClient, authorizationConfig: AuthorizationConfig);
    transform(user: OpenIDConnectUser): Promise<OpenIDConnectUser & UserWithAuthorizations>;
}
