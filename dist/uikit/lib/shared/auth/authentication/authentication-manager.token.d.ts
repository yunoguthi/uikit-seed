import { InjectionToken } from '@angular/core';
import { IAuthenticationManager } from './authentication-manager';
export declare const IAuthenticationManagerToken: InjectionToken<IAuthenticationManager<import("./user.model").User, import("./user.model").User>>;
