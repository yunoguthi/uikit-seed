import { User } from '../user.model';
import { Observable } from 'rxjs';
export interface IProviderAuthenticationService<TUser extends User = User, TLoginParam extends any = any, TLogoutParam extends any = any> {
    user$: Observable<TUser>;
    login(param: TLoginParam): Promise<void>;
    logout(param?: TLogoutParam): Promise<void>;
}
