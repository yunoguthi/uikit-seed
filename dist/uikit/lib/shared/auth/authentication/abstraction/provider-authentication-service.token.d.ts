import { InjectionToken } from '@angular/core';
import { IProviderAuthenticationService } from './provider-authentication.service';
export declare const IAuthenticationServiceToken: InjectionToken<IProviderAuthenticationService<import("../user.model").User, any, any>>;
