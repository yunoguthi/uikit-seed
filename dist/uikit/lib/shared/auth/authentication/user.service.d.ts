import { Observable } from 'rxjs';
import { User } from './user.model';
export declare class AnonymousUser implements User {
    sub: string;
    authenticated: boolean;
    name: string;
    email: string;
}
export declare class UserService<TUser extends User = User> {
    protected anonymousUser: AnonymousUser;
    private userSubject$;
    user$: Observable<TUser>;
    readonly userValue: TUser;
    /** Seta o usuário (não deve ser usado na aplicação [somente em testes]).
     * Para login e logout utilize o serviço 'AuthenticationService'
     */
    load(user: TUser): void;
    /** Seta o usuário como o anônimo (não deve ser usado na aplicação [somente em testes]).
     * Para login e logout utilize o serviço 'AuthenticationService'
     */
    unload(): void;
}
