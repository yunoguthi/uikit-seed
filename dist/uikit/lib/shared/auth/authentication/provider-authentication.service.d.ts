import { IProviderAuthenticationService } from './abstraction/provider-authentication.service';
import { User } from './user.model';
export declare abstract class ProviderAuthenticationService<TUserProvider extends any = any, TUser extends User = User, TLoginParam extends any = any, TLogoutParam extends any = any> implements IProviderAuthenticationService {
    private userProviderSubject;
    private userSubject;
    user$: import("rxjs").Observable<TUser>;
    abstract login(param?: TLoginParam): Promise<void>;
    logout(param?: TLogoutParam): Promise<void>;
    constructor();
    protected abstract transform(providerUser: TUserProvider): TUser;
    private loadUser;
    protected loadProviderUser(user: TUserProvider): void;
}
