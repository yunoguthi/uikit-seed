import { HttpClient } from '@angular/common/http';
import { AuthorizationConfig } from '../../authorization/authorization-config.token';
import { OpenIDConnectUser, UserWithAuthorizations } from '../user.model';
import { IAuthenticationManager } from '../authentication-manager';
export declare class KeycloakAuthenticationManager implements IAuthenticationManager {
    protected httpClient: HttpClient;
    protected authorizationConfig: AuthorizationConfig;
    constructor(httpClient: HttpClient, authorizationConfig: AuthorizationConfig);
    transform(user: OpenIDConnectUser): Promise<OpenIDConnectUser & UserWithAuthorizations>;
}
