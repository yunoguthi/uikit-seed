import { OnInit, ApplicationRef, OnDestroy } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ToastService } from '../../../../layout/toast/toast.service';
import { IProviderAuthenticationService } from '../../authentication/abstraction/provider-authentication.service';
import { BehaviorSubject } from 'rxjs';
import { UserService } from '../../authentication/user.service';
import { Router } from '@angular/router';
import { PlatformLocation, LocationStrategy } from '@angular/common';
export declare class LoginIframeComponent implements OnInit, OnDestroy {
    private sanitizer;
    protected applicationRef: ApplicationRef;
    protected toastService: ToastService;
    private providerAuthenticationService;
    protected userService: UserService;
    protected router: Router;
    private platformLocation;
    private locationStrategy;
    url: any;
    contador: number;
    isLoading$: BehaviorSubject<boolean>;
    private subscription;
    constructor(sanitizer: DomSanitizer, applicationRef: ApplicationRef, toastService: ToastService, providerAuthenticationService: IProviderAuthenticationService, userService: UserService, router: Router, platformLocation: PlatformLocation, locationStrategy: LocationStrategy);
    ngOnDestroy(): void;
    ngOnInit(): void;
    onLoad(myIframe: any): void;
    protected goToLastUri(): void;
}
