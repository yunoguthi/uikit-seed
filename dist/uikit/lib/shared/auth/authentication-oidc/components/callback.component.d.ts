import { OnInit } from '@angular/core';
import { AuthenticationService } from '../../authentication/authentication.service';
import { UserService } from '../../authentication/user.service';
export declare class CallbackComponent implements OnInit {
    protected authenticationService: AuthenticationService;
    protected userService: UserService;
    constructor(authenticationService: AuthenticationService, userService: UserService);
    ngOnInit(): void;
}
