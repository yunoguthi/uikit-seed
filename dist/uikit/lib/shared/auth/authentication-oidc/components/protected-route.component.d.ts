import { OnInit } from '@angular/core';
import { UserService } from '../../authentication/user.service';
import { Router } from '@angular/router';
import { PlatformLocation, LocationStrategy } from '@angular/common';
export declare class ProtectedRouteComponent implements OnInit {
    private userService;
    protected router: Router;
    private platformLocation;
    private locationStrategy;
    constructor(userService: UserService, router: Router, platformLocation: PlatformLocation, locationStrategy: LocationStrategy);
    ngOnInit(): void;
}
