import { OpenIDConnectUser } from './../authentication/user.model';
import { Router, ActivatedRoute } from '@angular/router';
import { ProviderAuthenticationService } from '../authentication/provider-authentication.service';
import { OpenIDConnectSettings } from './oidc-module';
import { PlatformLocation, LocationStrategy } from '@angular/common';
export declare class OidcAuthenticationService extends ProviderAuthenticationService<Oidc.User, OpenIDConnectUser, void, boolean> {
    authenticationSettings: Oidc.UserManagerSettings & OpenIDConnectSettings;
    protected router: Router;
    protected activatedRoute: ActivatedRoute;
    protected platformLocation: PlatformLocation;
    protected locationStrategy: LocationStrategy;
    protected userManager: Oidc.UserManager;
    userValue: Oidc.User;
    readonly needRenewUser: boolean;
    renewUser(): Promise<() => void>;
    constructor(authenticationSettings: Oidc.UserManagerSettings & OpenIDConnectSettings, router: Router, activatedRoute: ActivatedRoute, platformLocation: PlatformLocation, locationStrategy: LocationStrategy);
    loadOidcUser(user: any): void;
    transform(user: Oidc.User): OpenIDConnectUser;
    login(args?: any): Promise<any>;
    logout(ssoLogout?: boolean): Promise<void>;
}
