import { OidcAuthenticationService } from './oidc-authentication.service';
import { InjectionToken } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserManagerSettings } from 'oidc-client';
import { PlatformLocation, LocationStrategy } from '@angular/common';
export interface OpenIDConnectSettings extends UserManagerSettings {
    client_uri: string;
    /** Modo de como efetuar o login. (Padrão é 'iframe' quando não informado) */
    login_mode?: 'redirect' | 'popup' | 'iframe';
}
export declare const OIDC_CONFIG: InjectionToken<OpenIDConnectSettings>;
export declare class OidcConfigDepHolder {
    openIDConnectSettings: OpenIDConnectSettings;
    constructor(openIDConnectSettings: OpenIDConnectSettings);
}
export declare function InitOidcAuthenticationService(oidcConfigDepHolder: OidcConfigDepHolder, router: Router, platformLocation: PlatformLocation, activatedRoute: ActivatedRoute, locationStrategy: LocationStrategy): OidcAuthenticationService;
export declare class OidcAuthModule {
}
