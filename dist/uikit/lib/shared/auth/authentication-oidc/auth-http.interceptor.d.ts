import { Injector } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LogService } from '../../../utils/log/log.service';
import { UserService } from '../authentication/user.service';
export declare class OidcAuthHttpInterceptor implements HttpInterceptor {
    protected userService: UserService;
    private injector;
    protected logService?: LogService;
    constructor(userService: UserService, injector: Injector, logService?: LogService);
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>;
}
