import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserService } from './authentication/user.service';
import { LogService } from '../../utils/log/log.service';
export declare class AuthHttpInterceptor implements HttpInterceptor {
    protected userService: UserService;
    protected logService: LogService;
    constructor(userService: UserService, logService: LogService);
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>;
}
