import { AuthenticationService } from './authentication/authentication.service';
import { AuthorizationService } from './authorization/authorization.service';
import { UserService } from './authentication/user.service';
export declare class AuthService {
    user: UserService;
    authentication?: AuthenticationService;
    authorization?: AuthorizationService;
    constructor(user: UserService, authentication?: AuthenticationService, authorization?: AuthorizationService);
}
