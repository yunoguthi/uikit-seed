import { HttpClient } from '@angular/common/http';
import { AuthorizationConfig } from '../authorization-config.token';
import { UserService } from '../../authentication/user.service';
import { IAuthorizationManager } from '../authorization.service';
export declare class XacmlAuthorizationManager implements IAuthorizationManager {
    protected userService: UserService;
    protected httpClient: HttpClient;
    protected authorizationConfig: AuthorizationConfig;
    constructor(userService: UserService, httpClient: HttpClient, authorizationConfig: AuthorizationConfig);
    authorize(action: string, resource: string): Promise<boolean>;
}
