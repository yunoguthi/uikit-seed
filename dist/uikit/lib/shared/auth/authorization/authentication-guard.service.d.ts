import { ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from '../authentication/user.service';
import { Location, PlatformLocation } from '@angular/common';
import { GuardService } from './guard.service';
export declare class AuthenticationGuardService extends GuardService {
    protected identityService: UserService;
    protected router: Router;
    protected angularLocation: Location;
    protected platformLocation: PlatformLocation;
    constructor(identityService: UserService, router: Router, angularLocation: Location, platformLocation: PlatformLocation);
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Promise<boolean> | Observable<boolean>;
}
