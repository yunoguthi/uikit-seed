import { InjectionToken } from '@angular/core';
import { IAuthorizationManager } from './authorization.service';
export declare let IAuthorizationManagerToken: InjectionToken<IAuthorizationManager>;
