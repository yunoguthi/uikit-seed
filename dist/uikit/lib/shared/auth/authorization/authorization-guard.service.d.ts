import { ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Location, PlatformLocation } from '@angular/common';
import { GuardService } from './guard.service';
import { AuthorizationService } from './authorization.service';
export declare abstract class AuthorizationGuardService extends GuardService {
    protected authorizationService: AuthorizationService;
    protected router: Router;
    protected angularLocation: Location;
    protected platformLocation: PlatformLocation;
    constructor(authorizationService: AuthorizationService, router: Router, angularLocation: Location, platformLocation: PlatformLocation);
    abstract canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean>;
}
