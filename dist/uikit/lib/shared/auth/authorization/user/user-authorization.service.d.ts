import { UserService } from '../../authentication/user.service';
import { IAuthorizationManager } from '../authorization.service';
export declare class UserAuthorizationManager implements IAuthorizationManager {
    protected userService: UserService;
    constructor(userService: UserService);
    authorize(action: string, resource: string): Promise<boolean>;
}
