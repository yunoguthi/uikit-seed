import { AuthorizationConfig } from './../authorization-config.token';
import { HttpClient } from '@angular/common/http';
import { IAuthorizationManager } from '../authorization.service';
export declare class UmaAuthorizationManager implements IAuthorizationManager {
    protected httpClient: HttpClient;
    protected authorizationConfig: AuthorizationConfig;
    constructor(httpClient: HttpClient, authorizationConfig: AuthorizationConfig);
    authorize(action: string, resource: string): Promise<boolean>;
}
