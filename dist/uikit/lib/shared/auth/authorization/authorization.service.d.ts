export interface IAuthorizationManager {
    /**
     * Método responsável por verificar se o usuário possui permissão de executar a ação '{action}' no recurso '{resource}'
     * Attribute Based - https://en.wikipedia.org/wiki/Attribute-based_access_control
     */
    authorize(action: string, resource: string): Promise<boolean>;
}
export declare class AuthorizationService {
    private authorizationService?;
    constructor(authorizationService?: IAuthorizationManager);
    private validate;
    authorize(action: string, resource: string): Promise<boolean>;
}
