import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthorizationGuardService } from './authorization-guard.service';
export declare class RouteAuthorizationGuardService extends AuthorizationGuardService {
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean>;
}
