import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivateChild, UrlTree } from '@angular/router';
import { Location, PlatformLocation } from '@angular/common';
import { Observable } from 'rxjs';
export declare abstract class GuardService implements CanActivate, CanActivateChild {
    protected router: Router;
    protected angularLocation: Location;
    protected platformLocation: PlatformLocation;
    protected readonly routerStateSnapshot: RouterStateSnapshot;
    protected readonly activatedRouteSnapshot: ActivatedRouteSnapshot;
    constructor(router: Router, angularLocation: Location, platformLocation: PlatformLocation);
    canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree>;
    abstract canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Promise<boolean> | Observable<boolean>;
    allow(): boolean;
    deny(state: RouterStateSnapshot, login?: 'auto' | 'manual', setCallback?: boolean): boolean;
}
