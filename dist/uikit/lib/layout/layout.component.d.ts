import { ScrollDispatcher } from '@angular/cdk/overlay';
import { AfterContentInit, AfterViewInit, ElementRef, OnDestroy, OnInit, QueryList } from '@angular/core';
import { MatSidenav } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { HotkeysService } from 'angular2-hotkeys';
import { BehaviorSubject, Subscription } from 'rxjs';
import { UpdateInfoService } from '../update/update-info.service';
import { UpdateService } from '../update/update.service';
import { NotificacaoComponent } from './header/notification/notificacao/notificacao.component';
import { LayoutService } from './layout.service';
import { BreadcrumbService } from './nav/breadcrumb/breadcrumbs/breadcrumb.service';
import { FavNavsQuery } from './nav/favnav/state/favnavs.query';
import { FavNavsService } from './nav/favnav/state/favnavs.service';
import { NavQuery } from './nav/state/nav.query';
import { NavService } from './nav/state/nav.service';
import { ToastService } from './toast/toast.service';
import { FormBuilder } from '@angular/forms';
import { UikitRrippleService } from "../utils/uikit-ripple.service";
import { PreviousRouteService } from '../components/previous-route.service';
export declare class LayoutComponent implements OnInit, OnDestroy, AfterViewInit, AfterContentInit {
    protected navQuery: NavQuery;
    protected favNavsQuery: FavNavsQuery;
    protected navService: NavService;
    scroll: ScrollDispatcher;
    protected layoutService: LayoutService;
    protected router: Router;
    protected activatedRoute: ActivatedRoute;
    protected updateInfoService: UpdateInfoService;
    protected updateService: UpdateService;
    protected favNavsService: FavNavsService;
    protected hotkeysService: HotkeysService;
    protected breadcrumbService: BreadcrumbService;
    protected toastService: ToastService;
    protected uikitRrippleService: UikitRrippleService;
    private fb;
    private document;
    protected previousRouteService: PreviousRouteService;
    protected hasActionsSubject: BehaviorSubject<boolean>;
    hasActions$: import("rxjs").Observable<boolean>;
    myClickFullscreen: ElementRef<HTMLElement>;
    showShell: import("rxjs").Observable<boolean>;
    hiddenBreadcrumb: import("rxjs").Observable<boolean>;
    showFullScrren: import("rxjs").Observable<boolean>;
    hiddenShell: import("rxjs").Observable<boolean>;
    showNotifications: boolean;
    showSystemInfo: boolean;
    showUserInfo: boolean;
    showEnvironment: boolean;
    showIa: boolean;
    environment: string;
    allowedTypes: string[];
    defaultType: string;
    showShortcuts: boolean;
    protected subscription: Subscription;
    isFixed$: import("rxjs").Observable<boolean>;
    isMobile$: import("rxjs").Observable<boolean>;
    rightnav$: import("rxjs").Observable<{
        opened: boolean;
        pinned: boolean;
    }>;
    rightnav: {
        opened: boolean;
        pinned: boolean;
    };
    leftnav$: import("rxjs").Observable<{
        opened: boolean;
        pinned: boolean;
    }>;
    leftnav: {
        opened: boolean;
        pinned: boolean;
    };
    data$: BehaviorSubject<any>;
    possuiRecursoDeFavoritos: boolean;
    private scrollSubscription;
    notificacoesProjetadas: QueryList<NotificacaoComponent>;
    sidenav: MatSidenav;
    notificacoes$: import("rxjs").Observable<import("../../public_api").Notificacao[]>;
    private closeWhenNotPinnedLeftSubscription;
    private closeWhenNotPinnedRightSubscription;
    hasInstallOption$: import("rxjs").Observable<boolean>;
    hasUpdate$: import("rxjs").Observable<boolean>;
    favorites$: import("rxjs").Observable<import("../../public_api").MenuItem[]>;
    constructor(navQuery: NavQuery, favNavsQuery: FavNavsQuery, navService: NavService, scroll: ScrollDispatcher, layoutService: LayoutService, router: Router, activatedRoute: ActivatedRoute, updateInfoService: UpdateInfoService, updateService: UpdateService, favNavsService: FavNavsService, hotkeysService: HotkeysService, breadcrumbService: BreadcrumbService, toastService: ToastService, uikitRrippleService: UikitRrippleService, fb: FormBuilder, document: any, previousRouteService: PreviousRouteService);
    ngAfterContentInit(): void;
    ngOnInit(): void;
    toggleMobile(): void;
    updateIfHasActions(): void;
    ngAfterViewInit(): void;
    private getFavItem;
    ngOnDestroy(): void;
    leftNavToggle(): void;
    leftNavClose(): void;
    rightNavToggle(): void;
    rightNavClose(): void;
    navsClose(): void;
    install(): void;
    update(): void;
    isVerifyTypeShellApply(): void;
    openFullscreen(fsDocElem: any): void;
    private closeFullscreen;
    /*** Region RouteLayoutApplication
     * Modo de apresentação das tela aplicado de forma totalmente configurável a forma geral e por rota.
     * ***/
    setLayoutTypeByRouteEvent(): void;
    private getData;
}
