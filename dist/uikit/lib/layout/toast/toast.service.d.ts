import { ToastrService } from 'ngx-toastr';
import { ToastConfig } from './toast.config';
import { Toaster, ToastAction } from './toast.action';
import { Observable } from 'rxjs';
export declare class ToastService {
    protected toastrService: ToastrService;
    private loadingSubscription;
    private loadingToaster;
    private defaultConfig;
    constructor(toastrService: ToastrService);
    success(message: string, title?: string, objectAction?: ToastAction): Toaster<any>;
    error(message: string, error: Error, title?: string, objectAction?: ToastAction): Toaster<any>;
    info(message: string, title?: string, objectAction?: ToastAction): Toaster<any>;
    warning(message: string, title?: string, objectAction?: ToastAction): Toaster<any>;
    loading(isLoading$: Observable<boolean>, message?: string, title?: string, objectAction?: ToastAction): void;
    clear(toastId?: number): void;
    show(config: Partial<ToastConfig>): Toaster<any>;
    private applyConfig;
}
