import { ActiveToast } from 'ngx-toastr';
export declare class ToastAction {
    display: string;
    action: ((value?: any) => any);
}
export interface Toaster<T> extends ActiveToast<T> {
}
