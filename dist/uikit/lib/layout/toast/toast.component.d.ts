import { AfterViewInit, Renderer2 } from '@angular/core';
import { Toast, ToastrService, ToastPackage } from 'ngx-toastr';
import { ToastConfig } from './toast.config';
import { ToastRef } from 'ngx-toastr/toastr/toast-injector';
import { HotkeysService } from 'angular2-hotkeys';
export declare class ToastComponent extends Toast implements AfterViewInit {
    protected toastrService: ToastrService;
    toastPackage: ToastPackage;
    protected renderer: Renderer2;
    hotkeysService: HotkeysService;
    options: ToastConfig;
    protected divCenter: any;
    blockBodyToast: boolean;
    constructor(toastrService: ToastrService, toastPackage: ToastPackage, renderer: Renderer2, hotkeysService: HotkeysService);
    action(event: Event): boolean;
    ngAfterViewInit(): void;
    isBlockBodyToast(toast: ToastRef<any>): boolean;
}
