import { OnInit } from '@angular/core';
export interface Section {
    name: string;
    date: string;
    description: string;
    read: boolean;
}
export declare class SysteminfoComponent implements OnInit {
    folders: Section[];
    constructor();
    ngOnInit(): void;
}
