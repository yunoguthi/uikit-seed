import { OnInit, AfterContentInit, OnDestroy } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';
import { LayoutService } from '../../layout.service';
export interface Section {
    icon: string;
    name: string;
    description: string;
    read: boolean;
}
export declare class NotificationComponent implements OnInit, OnDestroy, AfterContentInit {
    protected layoutService: LayoutService;
    folders: Section[];
    notificacoes$: import("rxjs").Observable<import("./notificacao/notificacao.model").Notificacao[]>;
    quantidadeDeNotificacoesNaoLidas$: BehaviorSubject<number>;
    subscriptions: Subscription[];
    constructor(layoutService: LayoutService);
    ngOnInit(): void;
    ngOnDestroy(): void;
    ngAfterContentInit(): void;
}
