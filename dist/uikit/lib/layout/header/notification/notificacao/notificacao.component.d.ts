import { OnInit } from '@angular/core';
export declare class NotificacaoComponent implements OnInit {
    nome?: string;
    icone?: string;
    descricao: string;
    data: string;
    read: boolean;
    constructor();
    ngOnInit(): void;
}
