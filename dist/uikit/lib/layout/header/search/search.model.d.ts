import { ID } from '@datorama/akita';
export interface ItemPesquisa {
    id: ID;
    title: string;
    icon?: string;
    tags: string[];
    link: string;
}
export interface GrupoPesquisa {
    id: ID;
    title: string;
    icon?: string;
    isHeader?: boolean;
    isAviso?: boolean;
    items: ItemPesquisa[] | GrupoPesquisa[];
}
export declare function criarNovoGrupo(): GrupoPesquisa;
export declare function popularItens(itemParam: any[]): any;
