import { SearchState, SearchStore } from './search.store';
import { GrupoPesquisa } from '../search.model';
import { QueryEntity } from '@datorama/akita';
export declare class SearchQuery extends QueryEntity<SearchState, GrupoPesquisa> {
    protected store: SearchStore;
    constructor(store: SearchStore);
}
