import { GrupoPesquisa } from '../search.model';
import { EntityState, EntityStore } from '@datorama/akita';
export interface SearchState extends EntityState<GrupoPesquisa> {
}
export declare class SearchStore extends EntityStore<SearchState, GrupoPesquisa> {
    constructor();
    adicionarGrupoPesquisa(valueJaSetado: any, groupItem: any): void;
    addGroupBase(groupItem: any): void;
    removerGrupoPesquisa(title: string): void;
}
