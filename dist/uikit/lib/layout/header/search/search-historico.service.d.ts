import { LocalRepository } from '../../../shared/repositorio/local-repository';
import { Observable } from 'rxjs';
export declare class SearchHistoricoService {
    private readonly repositorio;
    constructor(repositorio: LocalRepository);
    obterHistorico(): any;
    salvarHistorico(historico: any): Observable<any>;
    private isEqual;
}
