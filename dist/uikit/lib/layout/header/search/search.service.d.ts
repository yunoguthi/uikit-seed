import { Observable } from 'rxjs';
import { GrupoPesquisa, ItemPesquisa } from './search.model';
import { SearchAbstractService } from '../../../utils/abstract/search-abstract.service';
export declare class SearchService {
    protected services: SearchAbstractService[];
    constructor(services: SearchAbstractService[]);
    buscar(term: string): Observable<(GrupoPesquisa | ItemPesquisa)[]>;
    adicionarGrupoPesquisa(group: GrupoPesquisa[]): void;
}
