import { OnInit } from '@angular/core';
import { AuthService } from '../../../shared/auth/auth.service';
import { ToastService } from '../../toast/toast.service';
export declare class UserinfoComponent implements OnInit {
    protected authService: AuthService;
    protected toastService: ToastService;
    user$: import("rxjs").Observable<import("../../../../public_api").User>;
    constructor(authService: AuthService, toastService: ToastService);
    ngOnInit(): void;
    login(): Promise<void>;
    logout(): void;
}
