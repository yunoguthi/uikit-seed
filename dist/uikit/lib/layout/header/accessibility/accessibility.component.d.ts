import { OnInit, AfterViewInit, Renderer2 } from '@angular/core';
import { MatSlider } from '@angular/material/slider';
import { MatSlideToggle } from '@angular/material';
export declare class AccessibilityComponent implements OnInit, AfterViewInit {
    private renderer;
    showIa: boolean;
    showShortcuts: boolean;
    slider: MatSlider;
    toggleContrast: MatSlideToggle;
    constructor(renderer: Renderer2);
    ngOnInit(): void;
    ngAfterViewInit(): void;
}
