import { OnInit, Renderer2 } from '@angular/core';
import { NavQuery } from '../nav/state/nav.query';
import { NavService } from '../nav/state/nav.service';
import { MatToolbar } from '@angular/material';
import { LayoutService } from '../layout.service';
export declare class HeaderComponent implements OnInit {
    protected navQuery: NavQuery;
    protected navService: NavService;
    protected layoutService: LayoutService;
    protected renderer: Renderer2;
    constructor(navQuery: NavQuery, navService: NavService, layoutService: LayoutService, renderer: Renderer2);
    showNotifications: boolean;
    showUserInfo: boolean;
    showSystemInfo: boolean;
    showIa: boolean;
    matToolbar: MatToolbar;
    showShortcuts: boolean;
    leftNav$: import("rxjs").Observable<{
        opened: boolean;
        pinned: boolean;
    }>;
    rightNav$: import("rxjs").Observable<{
        opened: boolean;
        pinned: boolean;
    }>;
    ngOnInit(): void;
    toggle(): void;
    rightNavToggle(): void;
}
