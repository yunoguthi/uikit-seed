import { MenuItem } from '../menu/menu-item/menu-item.model';
import { LayoutService } from '../../layout.service';
export declare class MenuSearchService {
    protected layoutService: LayoutService;
    private options;
    private menuItemsSubject$;
    menuItems$: import("rxjs").Observable<MenuItem[]>;
    constructor(layoutService: LayoutService);
    buscar(value: string): void;
    private applyFilter;
    private groupReturnFilter;
}
