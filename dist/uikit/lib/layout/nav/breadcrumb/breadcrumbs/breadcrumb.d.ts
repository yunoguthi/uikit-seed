import { Route } from "@angular/router";
export declare class Breadcrumb {
    title: string;
    terminal: boolean;
    link: string;
    route: Route | null;
}
