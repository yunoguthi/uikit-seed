import { OnInit, ApplicationRef } from '@angular/core';
import { Breadcrumb } from './breadcrumb';
import { BreadcrumbService } from './breadcrumb.service';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
export declare class BreadcrumbComponent implements OnInit {
    private breadcrumbService;
    private router;
    private applicationRef;
    protected breadcrumbsSubject: BehaviorSubject<Breadcrumb[]>;
    breadcrumbs$: import("rxjs").Observable<Breadcrumb[]>;
    constructor(breadcrumbService: BreadcrumbService, router: Router, applicationRef: ApplicationRef);
    ngOnInit(): void;
    private onBreadcrumbChange;
}
