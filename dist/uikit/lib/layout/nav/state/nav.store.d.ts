import { Store } from '@datorama/akita';
export interface NavState {
    leftnav: {
        opened: boolean;
        pinned: boolean;
    };
    rightnav: {
        opened: boolean;
        pinned: boolean;
    };
}
export declare function createInitialState(): NavState;
export declare class NavStore extends Store<NavState> {
    constructor();
}
