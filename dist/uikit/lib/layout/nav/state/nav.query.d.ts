import { Query } from '@datorama/akita';
import { NavStore, NavState } from './nav.store';
import { FavoritosService } from '../../../utils/favoritos.service';
export declare class NavQuery extends Query<NavState> {
    protected store: NavStore;
    protected favoritosService: FavoritosService;
    leftnav$: import("rxjs").Observable<{
        opened: boolean;
        pinned: boolean;
    }>;
    rightnav$: import("rxjs").Observable<{
        opened: boolean;
        pinned: boolean;
    }>;
    isLeftPinned$: import("rxjs").Observable<boolean>;
    isRightPinned$: import("rxjs").Observable<boolean>;
    hasFavoritosService: boolean;
    constructor(store: NavStore, favoritosService: FavoritosService);
}
