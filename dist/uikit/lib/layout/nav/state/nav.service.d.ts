import { NavStore } from './nav.store';
export declare class NavService {
    private navStore;
    constructor(navStore: NavStore);
    toggleLeftNav(): void;
    openLeftNav(): void;
    closeLeftNav(): void;
    pinLeftNav(): void;
    unpinLeftNav(): void;
    togglePinLeftNav(): void;
    toggleRightNav(): void;
    openRightNav(): void;
    closeRightNav(): void;
    pinRightNav(): void;
    unpinRightNav(): void;
    togglePinRightNav(): void;
}
