import { ID } from '@datorama/akita';
export interface MenuItem {
    id: ID;
    children?: MenuItem[];
    title: string;
    icon?: string;
    link?: any;
    tags?: string[];
    nodeId?: ID;
}
export declare function createItem(item: MenuItem): MenuItem;
