import { AfterContentInit, OnInit } from '@angular/core';
import { FavNavsQuery } from '../favnav/state/favnavs.query';
import { Observable } from 'rxjs';
import { MenuItem } from './menu-item/menu-item.model';
import { LayoutService } from '../../layout.service';
export declare class MenuComponent implements OnInit, AfterContentInit {
    protected favNavsQuery: FavNavsQuery;
    protected layoutService: LayoutService;
    itensRecursivo: Observable<MenuItem[]>;
    private children;
    favorites$: Observable<MenuItem[]>;
    favoritesMobile$: any;
    constructor(favNavsQuery: FavNavsQuery, layoutService: LayoutService);
    ngOnInit(): void;
    ngAfterContentInit(): void;
}
