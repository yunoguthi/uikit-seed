import { OnInit } from '@angular/core';
import { NavQuery } from './state/nav.query';
import { NavService } from './state/nav.service';
export declare class NavComponent implements OnInit {
    protected navService: NavService;
    protected navQuery: NavQuery;
    leftNav$: import("rxjs").Observable<{
        opened: boolean;
        pinned: boolean;
    }>;
    hasFavoritosService: boolean;
    constructor(navService: NavService, navQuery: NavQuery);
    ngOnInit(): void;
    togglePinLeftNav(): void;
}
