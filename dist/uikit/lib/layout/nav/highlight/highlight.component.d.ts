import { Highlightable } from '@angular/cdk/a11y';
export declare class HighlightComponent implements Highlightable {
    item: any;
    disabled: boolean;
    private _isActive;
    readonly isActive: boolean;
    setActiveStyles(): void;
    setInactiveStyles(): void;
    getLabel(): string;
}
