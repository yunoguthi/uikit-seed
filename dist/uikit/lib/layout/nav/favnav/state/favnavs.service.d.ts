import { FavNavsStore } from './favnavs.store';
import { MenuItem } from '../../menu/menu-item/menu-item.model';
import { FavNavsQuery } from './favnavs.query';
import { FavoritosService } from '../../../../utils/favoritos.service';
import { Router } from '@angular/router';
import { UserService } from '../../../../shared/auth/authentication/user.service';
export declare class FavNavsService {
    private favNavsStore;
    private favNavsQuery;
    private router;
    private userService;
    private favoritosService;
    possuiRecursoDeFavoritos: boolean;
    constructor(favNavsStore: FavNavsStore, favNavsQuery: FavNavsQuery, router: Router, userService: UserService, favoritosService: FavoritosService);
    toggleItem(item: MenuItem): void;
    moverItens(indiceOrigem: number, indiceDestino: number): void;
    private adicionar;
    private remover;
    private substituirColecao;
    private atualizar;
}
