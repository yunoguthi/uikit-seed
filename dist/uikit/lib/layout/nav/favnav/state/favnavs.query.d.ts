import { QueryEntity } from '@datorama/akita';
import { FavNavsStore, FavNavsState } from './favnavs.store';
import { MenuItem } from '../../menu/menu-item/menu-item.model';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Observable } from 'rxjs';
export declare class FavNavsQuery extends QueryEntity<FavNavsState, MenuItem> {
    protected store: FavNavsStore;
    protected router: Router;
    protected ngLocation: Location;
    favorites$: Observable<MenuItem[]>;
    constructor(store: FavNavsStore, router: Router, ngLocation: Location);
    isFavorited(menuItem: MenuItem): Observable<boolean>;
    getIsFavorited(menuItem: MenuItem): boolean;
    getFavorited(menuItem: MenuItem): MenuItem;
}
