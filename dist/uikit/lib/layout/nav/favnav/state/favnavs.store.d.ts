import { EntityState, EntityStore } from '@datorama/akita';
import { MenuItem } from '../../menu/menu-item/menu-item.model';
export interface FavNavsState extends EntityState<MenuItem> {
}
export declare class FavNavsStore extends EntityStore<FavNavsState, MenuItem> {
    constructor();
}
