import { OnInit, OnDestroy } from '@angular/core';
import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { NavQuery } from '../state/nav.query';
import { FavNavsQuery } from './state/favnavs.query';
import { Subscription } from 'rxjs';
import { FavNavsService } from './state/favnavs.service';
import { MenuItem } from '../menu/menu-item/menu-item.model';
export declare class FavnavComponent implements OnInit, OnDestroy {
    protected navQuery: NavQuery;
    protected favNavsQuery: FavNavsQuery;
    protected favNavsService: FavNavsService;
    leftNav$: import("rxjs").Observable<{
        opened: boolean;
        pinned: boolean;
    }>;
    favorites$: import("rxjs").Observable<MenuItem[]>;
    favorites: MenuItem[];
    favoritesSubscription: Subscription;
    constructor(navQuery: NavQuery, favNavsQuery: FavNavsQuery, favNavsService: FavNavsService);
    ngOnInit(): void;
    ngOnDestroy(): void;
    drop(event: CdkDragDrop<MenuItem[]>): void;
}
