export declare enum LayoutType {
    normal = 0,
    fullscreen = 1,
    noshell = 2,
    noshellfullscreen = 3,
    noshellnobreadcrumb = 4,
    noshellnobreadcrumbfullscreen = 5
}
export interface FsDocumentElement extends HTMLElement {
    msRequestFullscreen?: () => void;
    mozRequestFullScreen?: () => void;
}
