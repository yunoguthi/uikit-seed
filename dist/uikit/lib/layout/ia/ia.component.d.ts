import { OnInit, OnDestroy } from '@angular/core';
import { NavQuery } from './../nav/state/nav.query';
import { Subscription } from 'rxjs';
import { NavService } from './../nav/state/nav.service';
export declare class IaComponent implements OnInit, OnDestroy {
    protected navQuery: NavQuery;
    protected navService: NavService;
    rightnav$: import("rxjs").Observable<{
        opened: boolean;
        pinned: boolean;
    }>;
    rightnav: {
        opened: boolean;
        pinned: boolean;
    };
    protected subscription: Subscription;
    constructor(navQuery: NavQuery, navService: NavService);
    ngOnInit(): void;
    ngOnDestroy(): void;
    rightNavToggle(): void;
    rightNavOpen(): void;
    rightNavClose(): void;
    togglePinRightNav(): void;
}
