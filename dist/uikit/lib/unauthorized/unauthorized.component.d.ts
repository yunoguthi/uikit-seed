import { OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { AuthenticationService } from '../shared/auth/authentication/authentication.service';
import { UserService } from '../shared/auth/authentication/user.service';
import { ToastService } from '../layout/toast/toast.service';
import { PreviousRouteService } from '../components/previous-route.service';
import { IProviderAuthenticationService } from '../shared/auth/authentication/abstraction/provider-authentication.service';
export declare class UnauthorizedComponent implements OnInit {
    private authenticationService;
    private userService;
    private ngLocation;
    private toastService;
    private previousRouteService;
    private router;
    private providerAuthenticationService;
    imageSrc: any;
    isAuthenticated$: import("rxjs").Observable<boolean>;
    isLoading: boolean;
    constructor(authenticationService: AuthenticationService, userService: UserService, ngLocation: Location, toastService: ToastService, previousRouteService: PreviousRouteService, router: Router, providerAuthenticationService: IProviderAuthenticationService);
    ngOnInit(): void;
    login(): void;
    goBack(): void;
}
