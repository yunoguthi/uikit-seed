import { GrupoPesquisa, ItemPesquisa } from '../layout/header/search/search.model';
import { SearchQuery } from '../layout/header/search/state/search.query';
import { SearchStore } from '../layout/header/search/state/search.store';
import { SearchAbstractService } from './abstract/search-abstract.service';
import { Observable } from 'rxjs';
import { MenuItem } from '../layout/nav/menu/menu-item/menu-item.model';
import { LayoutService } from '../layout/layout.service';
export declare class SearchKeepService implements SearchAbstractService {
    protected layoutService: LayoutService;
    private searchQuery;
    private searchStore;
    protected searchGroups: Observable<MenuItem[]>;
    protected searchGroupsAll: Observable<MenuItem[]>;
    constructor(layoutService: LayoutService, searchQuery: SearchQuery, searchStore: SearchStore);
    addGroup(groups: GrupoPesquisa[]): void;
    removeGroup(group: GrupoPesquisa): void;
    searchGroup(term: string): Observable<GrupoPesquisa | ItemPesquisa>;
    private _filterGroup;
    private applyFilter;
    private groupReturnFilter;
}
