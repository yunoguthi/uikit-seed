import { AfterContentInit, ElementRef } from '@angular/core';
export declare class AutoFocusDirective implements AfterContentInit {
    private el;
    appAutoFocus: boolean;
    constructor(el: ElementRef);
    ngAfterContentInit(): void;
    hideKeyboard(el: any): void;
}
