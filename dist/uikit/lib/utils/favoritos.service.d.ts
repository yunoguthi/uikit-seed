import { InjectionToken } from '@angular/core';
import { MenuItem } from '../layout/nav/menu/menu-item/menu-item.model';
import { Observable } from 'rxjs';
export declare const FAVORITOS_SERVICE_TOKEN: InjectionToken<FavoritosService>;
export declare abstract class FavoritosService {
    constructor();
    /**
     * Salva os favoritos do usuário
     * @param itens Itens da lista de favoritos
     */
    abstract salvar(itens: MenuItem[]): Observable<void>;
    /**
     * Retorna os favoritos do usuário
     */
    abstract buscar(): Observable<MenuItem[]>;
}
