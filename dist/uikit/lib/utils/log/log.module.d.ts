import { LogConsumersService } from './log.service';
export declare class LogModule {
    logConsumersService: LogConsumersService;
    constructor(logConsumersService: LogConsumersService, parentModule?: LogModule);
}
