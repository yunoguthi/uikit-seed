import { BehaviorSubject } from 'rxjs';
import { OnDestroy, InjectionToken } from '@angular/core';
export declare enum LogLevel {
    Debug = 0,
    Info = 1,
    Warn = 2,
    Error = 3
}
export interface Log {
    message: string;
    level: LogLevel;
}
export declare class LogService {
    protected loggerSubject$: BehaviorSubject<Log>;
    logger$: import("rxjs").Observable<Log>;
    private infosToMessage;
    protected log(level?: LogLevel, ...infos: any[]): void;
    debug(...data: any[]): void;
    info(...data: any[]): void;
    warn(...data: any[]): void;
    error(...data: any[]): void;
}
export declare const LOG_CONSUMER_SERVICE: InjectionToken<LogConsumer>;
export declare class LogConsumersService {
    protected logService: LogService;
    constructor(logService: LogService, services?: LogConsumer[]);
}
export declare abstract class LogConsumer implements OnDestroy {
    private logSubscription;
    minimalConsumerLevel: LogLevel;
    protected consume(log: Log): void;
    configure(logService: LogService): void;
    protected abstract consumeDebug(message: string): any;
    protected abstract consumeInfo(message: string): any;
    protected abstract consumeWarn(message: string): any;
    protected abstract consumeError(message: string): any;
    ngOnDestroy(): void;
}
export declare class LogConsoleConsumerService extends LogConsumer {
    protected consumeDebug(message: string): void;
    protected consumeInfo(message: string): void;
    protected consumeWarn(message: string): void;
    protected consumeError(message: string): void;
}
