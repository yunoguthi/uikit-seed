import { InjectionToken } from '@angular/core';
import { GrupoPesquisa, ItemPesquisa } from '../../layout/header/search/search.model';
import { Observable } from 'rxjs';
export declare const SEARCH_TOKEN: InjectionToken<SearchAbstractService>;
export declare abstract class SearchAbstractService {
    protected constructor();
    abstract searchGroup(term: string): Observable<GrupoPesquisa | ItemPesquisa>;
    abstract addGroup(group: GrupoPesquisa[]): any;
    abstract removeGroup(group: GrupoPesquisa): any;
}
