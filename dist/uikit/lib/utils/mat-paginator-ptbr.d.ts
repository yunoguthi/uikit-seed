import { MatPaginatorIntl } from '@angular/material';
export declare class MatPaginatorIntlPtBr extends MatPaginatorIntl {
    itemsPerPageLabel: string;
    nextPageLabel: string;
    previousPageLabel: string;
    getRangeLabel: (page: any, pageSize: any, length: any) => string;
}
