/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, Optional, ApplicationRef, NgZone } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { MatDialog } from '@angular/material';
import { UpdateComponent } from './update.component';
import { first, concat } from 'rxjs/operators';
import { interval, Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { UpdateInfoService } from './update-info.service';
import { LogService } from '../utils/log/log.service';
import { resetStores } from '@datorama/akita';
import { ToastService } from '../layout/toast/toast.service';
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/dialog";
import * as i2 from "../layout/toast/toast.service";
import * as i3 from "@angular/router";
import * as i4 from "@angular/service-worker";
import * as i5 from "../utils/log/log.service";
import * as i6 from "./update-info.service";
export class UpdateService {
    /**
     * @param {?} matDialog
     * @param {?} toastService
     * @param {?} appRef
     * @param {?} ngZone
     * @param {?} router
     * @param {?} swUpdate
     * @param {?} logService
     * @param {?} updateInfoService
     */
    constructor(matDialog, toastService, appRef, ngZone, router, swUpdate, logService, updateInfoService) {
        this.matDialog = matDialog;
        this.toastService = toastService;
        this.appRef = appRef;
        this.ngZone = ngZone;
        this.router = router;
        this.swUpdate = swUpdate;
        this.logService = logService;
        this.updateInfoService = updateInfoService;
        this.primeiroAcesso = true;
        this.hasUpdate = false;
        this.updateSubscription = Subscription.EMPTY;
        this.matDialogRef = null;
        if (this.swUpdate && this.swUpdate.isEnabled) {
            navigator.serviceWorker.getRegistrations().then((/**
             * @param {?} registrations
             * @return {?}
             */
            registrations => {
                /** @type {?} */
                const possuiSwRegistrado = registrations.length > 0;
                if (possuiSwRegistrado) {
                    // Mostra dialogo para carregando atualizações
                    this.matDialogRef = this.matDialog.open(UpdateComponent, (/** @type {?} */ ({ disableClose: true, hasBackdrop: true })));
                }
            }));
            this.updateSubscription = this.updateInfoService.hasUpdate$.subscribe((/**
             * @param {?} hasUpdate
             * @return {?}
             */
            hasUpdate => this.hasUpdate = hasUpdate));
            this.swUpdate.available.subscribe((/**
             * @param {?} event
             * @return {?}
             */
            (event) => {
                this.updateInfoService.setUpdateAsAvailable();
                this.logService.info(`Current version is: ${JSON.stringify(event.current)}`);
                this.logService.info(`Available version is: ${JSON.stringify(event.available)}`);
                if (this.primeiroAcesso) {
                    this.update();
                }
                else {
                    if (this.matDialogRef) {
                        this.matDialogRef.close();
                    }
                }
            }));
            this.swUpdate.activated.subscribe((/**
             * @param {?} event
             * @return {?}
             */
            (event) => {
                this.logService.info(`Old version was: ${JSON.stringify(event.previous)}`);
                this.logService.info(`New version is: ${JSON.stringify(event.current)}`);
                localStorage.setItem('updated', 'true');
                this.logService.debug(`Atualizando...`);
                try {
                    resetStores();
                }
                catch (error) {
                    this.logService.error(error);
                }
                document.location.reload(true);
            }));
            /** @type {?} */
            const appIsStable$ = appRef.isStable.pipe(first((/**
             * @param {?} isStable
             * @return {?}
             */
            isStable => isStable === true)));
            /** @type {?} */
            const everyTime$ = interval(1 * 60 * 5000);
            /** @type {?} */
            const everyTimeOnceAppIsStable$ = appIsStable$.pipe(concat(everyTime$));
            everyTimeOnceAppIsStable$.subscribe((/**
             * @return {?}
             */
            () => this.checkUpdate()));
            this.checkUpdate();
        }
        if (localStorage.getItem('updated') === 'true') {
            this.toastService.success('Atualização realizada com sucesso!');
            localStorage.removeItem('updated');
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.updateSubscription && this.updateSubscription.closed) {
            this.updateSubscription.unsubscribe();
        }
    }
    /**
     * @return {?}
     */
    update() {
        if (this.matDialogRef == null) {
            this.matDialogRef = this.matDialog.open(UpdateComponent, (/** @type {?} */ ({ disableClose: true, hasBackdrop: true })));
        }
        this.swUpdate.activateUpdate();
    }
    /**
     * @private
     * @return {?}
     */
    checkUpdate() {
        this.logService.debug(`Verificando por atualização...`);
        this.swUpdate.checkForUpdate()
            .then((/**
         * @return {?}
         */
        () => {
            this.logService.debug('checkForUpdate - then');
            this.ngZone.run((/**
             * @return {?}
             */
            () => {
                this.primeiroAcesso = false;
                if (!this.hasUpdate) {
                    this.logService.debug(`this.matDialogRef`);
                    if (this.matDialogRef) {
                        this.logService.debug(`this.matDialogRef.close()`);
                        this.matDialogRef.close();
                    }
                }
            }));
        })).catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            this.logService.debug('checkForUpdate - error');
            this.logService.debug(error);
            this.ngZone.run((/**
             * @return {?}
             */
            () => {
                /** @type {?} */
                let erro = null;
                if (error.message) {
                    erro = error.message;
                }
                else {
                    erro = error;
                }
                this.logService.error(erro);
                if (this.matDialogRef) {
                    this.matDialogRef.close();
                }
            }));
        }));
    }
}
UpdateService.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */
UpdateService.ctorParameters = () => [
    { type: MatDialog },
    { type: ToastService },
    { type: ApplicationRef },
    { type: NgZone },
    { type: Router },
    { type: SwUpdate, decorators: [{ type: Optional }] },
    { type: LogService, decorators: [{ type: Optional }] },
    { type: UpdateInfoService }
];
/** @nocollapse */ UpdateService.ngInjectableDef = i0.defineInjectable({ factory: function UpdateService_Factory() { return new UpdateService(i0.inject(i1.MatDialog), i0.inject(i2.ToastService), i0.inject(i0.ApplicationRef), i0.inject(i0.NgZone), i0.inject(i3.Router), i0.inject(i4.SwUpdate, 8), i0.inject(i5.LogService, 8), i0.inject(i6.UpdateInfoService)); }, token: UpdateService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    UpdateService.prototype.primeiroAcesso;
    /**
     * @type {?}
     * @private
     */
    UpdateService.prototype.hasUpdate;
    /**
     * @type {?}
     * @private
     */
    UpdateService.prototype.updateSubscription;
    /**
     * @type {?}
     * @private
     */
    UpdateService.prototype.matDialogRef;
    /**
     * @type {?}
     * @protected
     */
    UpdateService.prototype.matDialog;
    /**
     * @type {?}
     * @protected
     */
    UpdateService.prototype.toastService;
    /**
     * @type {?}
     * @protected
     */
    UpdateService.prototype.appRef;
    /**
     * @type {?}
     * @protected
     */
    UpdateService.prototype.ngZone;
    /**
     * @type {?}
     * @protected
     */
    UpdateService.prototype.router;
    /**
     * @type {?}
     * @protected
     */
    UpdateService.prototype.swUpdate;
    /**
     * @type {?}
     * @protected
     */
    UpdateService.prototype.logService;
    /**
     * @type {?}
     * @protected
     */
    UpdateService.prototype.updateInfoService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXBkYXRlLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL3VwZGF0ZS91cGRhdGUuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsY0FBYyxFQUE0QixNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDdkcsT0FBTyxFQUFFLFFBQVEsRUFBOEMsTUFBTSx5QkFBeUIsQ0FBQztBQUMvRixPQUFPLEVBQUUsU0FBUyxFQUFpQyxNQUFNLG1CQUFtQixDQUFDO0FBQzdFLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUNyRCxPQUFPLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBVSxNQUFNLGdCQUFnQixDQUFDO0FBQ3ZELE9BQU8sRUFBaUIsUUFBUSxFQUFFLFlBQVksRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUM3RCxPQUFPLEVBQUUsTUFBTSxFQUFrQyxNQUFNLGlCQUFpQixDQUFDO0FBQ3pFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQzFELE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUN0RCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDOUMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLCtCQUErQixDQUFDOzs7Ozs7OztBQUc3RCxNQUFNLE9BQU8sYUFBYTs7Ozs7Ozs7Ozs7SUFReEIsWUFDWSxTQUFvQixFQUNwQixZQUEwQixFQUMxQixNQUFzQixFQUN0QixNQUFjLEVBQ2QsTUFBYyxFQUNGLFFBQWtCLEVBQ2xCLFVBQXNCLEVBQ2xDLGlCQUFvQztRQVBwQyxjQUFTLEdBQVQsU0FBUyxDQUFXO1FBQ3BCLGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBQzFCLFdBQU0sR0FBTixNQUFNLENBQWdCO1FBQ3RCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ0YsYUFBUSxHQUFSLFFBQVEsQ0FBVTtRQUNsQixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ2xDLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBbUI7UUFkeEMsbUJBQWMsR0FBRyxJQUFJLENBQUM7UUFDdEIsY0FBUyxHQUFHLEtBQUssQ0FBQztRQUNsQix1QkFBa0IsR0FBRyxZQUFZLENBQUMsS0FBSyxDQUFDO1FBRXhDLGlCQUFZLEdBQWtDLElBQUksQ0FBQztRQWF6RCxJQUFJLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUU7WUFFNUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLElBQUk7Ozs7WUFBQyxhQUFhLENBQUMsRUFBRTs7c0JBQ3hELGtCQUFrQixHQUFHLGFBQWEsQ0FBQyxNQUFNLEdBQUcsQ0FBQztnQkFDbkQsSUFBSSxrQkFBa0IsRUFBRTtvQkFDdEIsOENBQThDO29CQUM5QyxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFDckQsbUJBQUEsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUUsRUFBb0MsQ0FBQyxDQUFDO2lCQUNsRjtZQUNILENBQUMsRUFBQyxDQUFDO1lBRUgsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsU0FBUzs7OztZQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLEVBQUMsQ0FBQztZQUUvRyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxTQUFTOzs7O1lBQUMsQ0FBQyxLQUEyQixFQUFFLEVBQUU7Z0JBQ2hFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO2dCQUM5QyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyx1QkFBdUIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUM3RSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyx5QkFBeUIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUVqRixJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUU7b0JBQ3ZCLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztpQkFDZjtxQkFBTTtvQkFDTCxJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7d0JBQ3JCLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7cUJBQzNCO2lCQUNGO1lBQ0gsQ0FBQyxFQUFDLENBQUM7WUFFSCxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxTQUFTOzs7O1lBQUMsQ0FBQyxLQUEyQixFQUFFLEVBQUU7Z0JBQ2hFLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLG9CQUFvQixJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQzNFLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLG1CQUFtQixJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBRXpFLFlBQVksQ0FBQyxPQUFPLENBQUMsU0FBUyxFQUFFLE1BQU0sQ0FBQyxDQUFDO2dCQUN4QyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO2dCQUV4QyxJQUFJO29CQUNGLFdBQVcsRUFBRSxDQUFDO2lCQUNmO2dCQUFDLE9BQU8sS0FBSyxFQUFFO29CQUNkLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUM5QjtnQkFDRCxRQUFRLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNqQyxDQUFDLEVBQUMsQ0FBQzs7a0JBRUcsWUFBWSxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUs7Ozs7WUFBQyxRQUFRLENBQUMsRUFBRSxDQUFDLFFBQVEsS0FBSyxJQUFJLEVBQUMsQ0FBQzs7a0JBQ3pFLFVBQVUsR0FBRyxRQUFRLENBQUMsQ0FBQyxHQUFHLEVBQUUsR0FBRyxJQUFJLENBQUM7O2tCQUNwQyx5QkFBeUIsR0FBRyxZQUFZLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUV2RSx5QkFBeUIsQ0FBQyxTQUFTOzs7WUFBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLEVBQUMsQ0FBQztZQUU5RCxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDcEI7UUFFRCxJQUFJLFlBQVksQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLEtBQUssTUFBTSxFQUFFO1lBQzlDLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLG9DQUFvQyxDQUFDLENBQUM7WUFDaEUsWUFBWSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQztTQUNwQztJQUNILENBQUM7Ozs7SUFFRCxXQUFXO1FBQ1QsSUFBSSxJQUFJLENBQUMsa0JBQWtCLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLE1BQU0sRUFBRTtZQUM3RCxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDdkM7SUFDSCxDQUFDOzs7O0lBRU0sTUFBTTtRQUNYLElBQUksSUFBSSxDQUFDLFlBQVksSUFBSSxJQUFJLEVBQUU7WUFDN0IsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQ3JELG1CQUFBLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxXQUFXLEVBQUUsSUFBSSxFQUFFLEVBQW9DLENBQUMsQ0FBQztTQUNsRjtRQUVELElBQUksQ0FBQyxRQUFRLENBQUMsY0FBYyxFQUFFLENBQUM7SUFDakMsQ0FBQzs7Ozs7SUFFTyxXQUFXO1FBQ2pCLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLGdDQUFnQyxDQUFDLENBQUM7UUFDeEQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxjQUFjLEVBQUU7YUFDM0IsSUFBSTs7O1FBQUMsR0FBRyxFQUFFO1lBQ1QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsdUJBQXVCLENBQUMsQ0FBQztZQUMvQyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUc7OztZQUFDLEdBQUcsRUFBRTtnQkFDbkIsSUFBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7Z0JBQzVCLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFO29CQUNuQixJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO29CQUMzQyxJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7d0JBQ3JCLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLDJCQUEyQixDQUFDLENBQUM7d0JBQ25ELElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7cUJBQzNCO2lCQUNGO1lBQ0gsQ0FBQyxFQUFDLENBQUM7UUFDTCxDQUFDLEVBQUMsQ0FBQyxLQUFLOzs7O1FBQUMsS0FBSyxDQUFDLEVBQUU7WUFDZixJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO1lBQ2hELElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzdCLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRzs7O1lBQUMsR0FBRyxFQUFFOztvQkFDZixJQUFJLEdBQUcsSUFBSTtnQkFDZixJQUFJLEtBQUssQ0FBQyxPQUFPLEVBQUU7b0JBQ2pCLElBQUksR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDO2lCQUN0QjtxQkFBTTtvQkFDTCxJQUFJLEdBQUcsS0FBSyxDQUFDO2lCQUNkO2dCQUNELElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUM1QixJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7b0JBQ3JCLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7aUJBQzNCO1lBQ0gsQ0FBQyxFQUFDLENBQUM7UUFDTCxDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7OztZQTNIRixVQUFVLFNBQUMsRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFOzs7O1lBVnpCLFNBQVM7WUFRVCxZQUFZO1lBVlUsY0FBYztZQUE0QixNQUFNO1lBTXRFLE1BQU07WUFMTixRQUFRLHVCQTBCWixRQUFRO1lBbkJKLFVBQVUsdUJBb0JkLFFBQVE7WUFyQkosaUJBQWlCOzs7Ozs7OztJQVF4Qix1Q0FBOEI7Ozs7O0lBQzlCLGtDQUEwQjs7Ozs7SUFDMUIsMkNBQWdEOzs7OztJQUVoRCxxQ0FBMkQ7Ozs7O0lBR3pELGtDQUE4Qjs7Ozs7SUFDOUIscUNBQW9DOzs7OztJQUNwQywrQkFBZ0M7Ozs7O0lBQ2hDLCtCQUF3Qjs7Ozs7SUFDeEIsK0JBQXdCOzs7OztJQUN4QixpQ0FBd0M7Ozs7O0lBQ3hDLG1DQUE0Qzs7Ozs7SUFDNUMsMENBQThDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSwgT3B0aW9uYWwsIEFwcGxpY2F0aW9uUmVmLCBPbkRlc3Ryb3ksIEFmdGVyVmlld0luaXQsIE5nWm9uZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgU3dVcGRhdGUsIFVwZGF0ZUF2YWlsYWJsZUV2ZW50LCBVcGRhdGVBY3RpdmF0ZWRFdmVudCB9IGZyb20gJ0Bhbmd1bGFyL3NlcnZpY2Utd29ya2VyJztcbmltcG9ydCB7IE1hdERpYWxvZywgTWF0RGlhbG9nQ29uZmlnLCBNYXREaWFsb2dSZWYgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XG5pbXBvcnQgeyBVcGRhdGVDb21wb25lbnQgfSBmcm9tICcuL3VwZGF0ZS5jb21wb25lbnQnO1xuaW1wb3J0IHsgZmlyc3QsIGNvbmNhdCwgZmlsdGVyIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHsgY29tYmluZUxhdGVzdCwgaW50ZXJ2YWwsIFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgUm91dGVyLCBOYXZpZ2F0aW9uU3RhcnQsIE5hdmlnYXRpb25FbmQgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHsgVXBkYXRlSW5mb1NlcnZpY2UgfSBmcm9tICcuL3VwZGF0ZS1pbmZvLnNlcnZpY2UnO1xuaW1wb3J0IHsgTG9nU2VydmljZSB9IGZyb20gJy4uL3V0aWxzL2xvZy9sb2cuc2VydmljZSc7XG5pbXBvcnQgeyByZXNldFN0b3JlcyB9IGZyb20gJ0BkYXRvcmFtYS9ha2l0YSc7XG5pbXBvcnQgeyBUb2FzdFNlcnZpY2UgfSBmcm9tICcuLi9sYXlvdXQvdG9hc3QvdG9hc3Quc2VydmljZSc7XG5cbkBJbmplY3RhYmxlKHsgcHJvdmlkZWRJbjogJ3Jvb3QnIH0pXG5leHBvcnQgY2xhc3MgVXBkYXRlU2VydmljZSBpbXBsZW1lbnRzIE9uRGVzdHJveSB7XG5cbiAgcHJpdmF0ZSBwcmltZWlyb0FjZXNzbyA9IHRydWU7XG4gIHByaXZhdGUgaGFzVXBkYXRlID0gZmFsc2U7XG4gIHByaXZhdGUgdXBkYXRlU3Vic2NyaXB0aW9uID0gU3Vic2NyaXB0aW9uLkVNUFRZO1xuXG4gIHByaXZhdGUgbWF0RGlhbG9nUmVmOiBNYXREaWFsb2dSZWY8VXBkYXRlQ29tcG9uZW50PiA9IG51bGw7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJvdGVjdGVkIG1hdERpYWxvZzogTWF0RGlhbG9nLFxuICAgIHByb3RlY3RlZCB0b2FzdFNlcnZpY2U6IFRvYXN0U2VydmljZSxcbiAgICBwcm90ZWN0ZWQgYXBwUmVmOiBBcHBsaWNhdGlvblJlZixcbiAgICBwcm90ZWN0ZWQgbmdab25lOiBOZ1pvbmUsXG4gICAgcHJvdGVjdGVkIHJvdXRlcjogUm91dGVyLFxuICAgIEBPcHRpb25hbCgpIHByb3RlY3RlZCBzd1VwZGF0ZTogU3dVcGRhdGUsXG4gICAgQE9wdGlvbmFsKCkgcHJvdGVjdGVkIGxvZ1NlcnZpY2U6IExvZ1NlcnZpY2UsXG4gICAgcHJvdGVjdGVkIHVwZGF0ZUluZm9TZXJ2aWNlOiBVcGRhdGVJbmZvU2VydmljZVxuICApIHtcblxuICAgIGlmICh0aGlzLnN3VXBkYXRlICYmIHRoaXMuc3dVcGRhdGUuaXNFbmFibGVkKSB7XG5cbiAgICAgIG5hdmlnYXRvci5zZXJ2aWNlV29ya2VyLmdldFJlZ2lzdHJhdGlvbnMoKS50aGVuKHJlZ2lzdHJhdGlvbnMgPT4ge1xuICAgICAgICBjb25zdCBwb3NzdWlTd1JlZ2lzdHJhZG8gPSByZWdpc3RyYXRpb25zLmxlbmd0aCA+IDA7XG4gICAgICAgIGlmIChwb3NzdWlTd1JlZ2lzdHJhZG8pIHtcbiAgICAgICAgICAvLyBNb3N0cmEgZGlhbG9nbyBwYXJhIGNhcnJlZ2FuZG8gYXR1YWxpemHDp8O1ZXNcbiAgICAgICAgICB0aGlzLm1hdERpYWxvZ1JlZiA9IHRoaXMubWF0RGlhbG9nLm9wZW4oVXBkYXRlQ29tcG9uZW50LFxuICAgICAgICAgICAgeyBkaXNhYmxlQ2xvc2U6IHRydWUsIGhhc0JhY2tkcm9wOiB0cnVlIH0gYXMgTWF0RGlhbG9nQ29uZmlnPFVwZGF0ZUNvbXBvbmVudD4pO1xuICAgICAgICB9XG4gICAgICB9KTtcblxuICAgICAgdGhpcy51cGRhdGVTdWJzY3JpcHRpb24gPSB0aGlzLnVwZGF0ZUluZm9TZXJ2aWNlLmhhc1VwZGF0ZSQuc3Vic2NyaWJlKGhhc1VwZGF0ZSA9PiB0aGlzLmhhc1VwZGF0ZSA9IGhhc1VwZGF0ZSk7XG5cbiAgICAgIHRoaXMuc3dVcGRhdGUuYXZhaWxhYmxlLnN1YnNjcmliZSgoZXZlbnQ6IFVwZGF0ZUF2YWlsYWJsZUV2ZW50KSA9PiB7XG4gICAgICAgIHRoaXMudXBkYXRlSW5mb1NlcnZpY2Uuc2V0VXBkYXRlQXNBdmFpbGFibGUoKTtcbiAgICAgICAgdGhpcy5sb2dTZXJ2aWNlLmluZm8oYEN1cnJlbnQgdmVyc2lvbiBpczogJHtKU09OLnN0cmluZ2lmeShldmVudC5jdXJyZW50KX1gKTtcbiAgICAgICAgdGhpcy5sb2dTZXJ2aWNlLmluZm8oYEF2YWlsYWJsZSB2ZXJzaW9uIGlzOiAke0pTT04uc3RyaW5naWZ5KGV2ZW50LmF2YWlsYWJsZSl9YCk7XG5cbiAgICAgICAgaWYgKHRoaXMucHJpbWVpcm9BY2Vzc28pIHtcbiAgICAgICAgICB0aGlzLnVwZGF0ZSgpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGlmICh0aGlzLm1hdERpYWxvZ1JlZikge1xuICAgICAgICAgICAgdGhpcy5tYXREaWFsb2dSZWYuY2xvc2UoKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH0pO1xuXG4gICAgICB0aGlzLnN3VXBkYXRlLmFjdGl2YXRlZC5zdWJzY3JpYmUoKGV2ZW50OiBVcGRhdGVBY3RpdmF0ZWRFdmVudCkgPT4ge1xuICAgICAgICB0aGlzLmxvZ1NlcnZpY2UuaW5mbyhgT2xkIHZlcnNpb24gd2FzOiAke0pTT04uc3RyaW5naWZ5KGV2ZW50LnByZXZpb3VzKX1gKTtcbiAgICAgICAgdGhpcy5sb2dTZXJ2aWNlLmluZm8oYE5ldyB2ZXJzaW9uIGlzOiAke0pTT04uc3RyaW5naWZ5KGV2ZW50LmN1cnJlbnQpfWApO1xuXG4gICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCd1cGRhdGVkJywgJ3RydWUnKTtcbiAgICAgICAgdGhpcy5sb2dTZXJ2aWNlLmRlYnVnKGBBdHVhbGl6YW5kby4uLmApO1xuXG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgcmVzZXRTdG9yZXMoKTtcbiAgICAgICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgICAgICB0aGlzLmxvZ1NlcnZpY2UuZXJyb3IoZXJyb3IpO1xuICAgICAgICB9XG4gICAgICAgIGRvY3VtZW50LmxvY2F0aW9uLnJlbG9hZCh0cnVlKTtcbiAgICAgIH0pO1xuXG4gICAgICBjb25zdCBhcHBJc1N0YWJsZSQgPSBhcHBSZWYuaXNTdGFibGUucGlwZShmaXJzdChpc1N0YWJsZSA9PiBpc1N0YWJsZSA9PT0gdHJ1ZSkpO1xuICAgICAgY29uc3QgZXZlcnlUaW1lJCA9IGludGVydmFsKDEgKiA2MCAqIDUwMDApO1xuICAgICAgY29uc3QgZXZlcnlUaW1lT25jZUFwcElzU3RhYmxlJCA9IGFwcElzU3RhYmxlJC5waXBlKGNvbmNhdChldmVyeVRpbWUkKSk7XG5cbiAgICAgIGV2ZXJ5VGltZU9uY2VBcHBJc1N0YWJsZSQuc3Vic2NyaWJlKCgpID0+IHRoaXMuY2hlY2tVcGRhdGUoKSk7XG5cbiAgICAgIHRoaXMuY2hlY2tVcGRhdGUoKTtcbiAgICB9XG5cbiAgICBpZiAobG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3VwZGF0ZWQnKSA9PT0gJ3RydWUnKSB7XG4gICAgICB0aGlzLnRvYXN0U2VydmljZS5zdWNjZXNzKCdBdHVhbGl6YcOnw6NvIHJlYWxpemFkYSBjb20gc3VjZXNzbyEnKTtcbiAgICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKCd1cGRhdGVkJyk7XG4gICAgfVxuICB9XG5cbiAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XG4gICAgaWYgKHRoaXMudXBkYXRlU3Vic2NyaXB0aW9uICYmIHRoaXMudXBkYXRlU3Vic2NyaXB0aW9uLmNsb3NlZCkge1xuICAgICAgdGhpcy51cGRhdGVTdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcbiAgICB9XG4gIH1cblxuICBwdWJsaWMgdXBkYXRlKCkge1xuICAgIGlmICh0aGlzLm1hdERpYWxvZ1JlZiA9PSBudWxsKSB7XG4gICAgICB0aGlzLm1hdERpYWxvZ1JlZiA9IHRoaXMubWF0RGlhbG9nLm9wZW4oVXBkYXRlQ29tcG9uZW50LFxuICAgICAgICB7IGRpc2FibGVDbG9zZTogdHJ1ZSwgaGFzQmFja2Ryb3A6IHRydWUgfSBhcyBNYXREaWFsb2dDb25maWc8VXBkYXRlQ29tcG9uZW50Pik7XG4gICAgfVxuXG4gICAgdGhpcy5zd1VwZGF0ZS5hY3RpdmF0ZVVwZGF0ZSgpO1xuICB9XG5cbiAgcHJpdmF0ZSBjaGVja1VwZGF0ZSgpIHtcbiAgICB0aGlzLmxvZ1NlcnZpY2UuZGVidWcoYFZlcmlmaWNhbmRvIHBvciBhdHVhbGl6YcOnw6NvLi4uYCk7XG4gICAgdGhpcy5zd1VwZGF0ZS5jaGVja0ZvclVwZGF0ZSgpXG4gICAgICAudGhlbigoKSA9PiB7XG4gICAgICAgIHRoaXMubG9nU2VydmljZS5kZWJ1ZygnY2hlY2tGb3JVcGRhdGUgLSB0aGVuJyk7XG4gICAgICAgIHRoaXMubmdab25lLnJ1bigoKSA9PiB7XG4gICAgICAgICAgdGhpcy5wcmltZWlyb0FjZXNzbyA9IGZhbHNlO1xuICAgICAgICAgIGlmICghdGhpcy5oYXNVcGRhdGUpIHtcbiAgICAgICAgICAgIHRoaXMubG9nU2VydmljZS5kZWJ1ZyhgdGhpcy5tYXREaWFsb2dSZWZgKTtcbiAgICAgICAgICAgIGlmICh0aGlzLm1hdERpYWxvZ1JlZikge1xuICAgICAgICAgICAgICB0aGlzLmxvZ1NlcnZpY2UuZGVidWcoYHRoaXMubWF0RGlhbG9nUmVmLmNsb3NlKClgKTtcbiAgICAgICAgICAgICAgdGhpcy5tYXREaWFsb2dSZWYuY2xvc2UoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgfSkuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICB0aGlzLmxvZ1NlcnZpY2UuZGVidWcoJ2NoZWNrRm9yVXBkYXRlIC0gZXJyb3InKTtcbiAgICAgICAgdGhpcy5sb2dTZXJ2aWNlLmRlYnVnKGVycm9yKTtcbiAgICAgICAgdGhpcy5uZ1pvbmUucnVuKCgpID0+IHtcbiAgICAgICAgICBsZXQgZXJybyA9IG51bGw7XG4gICAgICAgICAgaWYgKGVycm9yLm1lc3NhZ2UpIHtcbiAgICAgICAgICAgIGVycm8gPSBlcnJvci5tZXNzYWdlO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBlcnJvID0gZXJyb3I7XG4gICAgICAgICAgfVxuICAgICAgICAgIHRoaXMubG9nU2VydmljZS5lcnJvcihlcnJvKTtcbiAgICAgICAgICBpZiAodGhpcy5tYXREaWFsb2dSZWYpIHtcbiAgICAgICAgICAgIHRoaXMubWF0RGlhbG9nUmVmLmNsb3NlKCk7XG4gICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgIH0pO1xuICB9XG59XG4iXX0=