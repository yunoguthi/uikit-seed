/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatDialogModule, MatProgressSpinnerModule } from '@angular/material';
import { UpdateComponent } from './update.component';
import { UpdateService } from './update.service';
import { UpdateInfoService } from './update-info.service';
export class UpdateModule {
    /**
     * @param {?} updateService
     * @param {?} updateInfoService
     */
    constructor(updateService, updateInfoService) {
        this.updateService = updateService;
        this.updateInfoService = updateInfoService;
    }
}
UpdateModule.decorators = [
    { type: NgModule, args: [{
                entryComponents: [UpdateComponent],
                declarations: [UpdateComponent],
                imports: [
                    CommonModule,
                    MatDialogModule,
                    MatProgressSpinnerModule
                ],
                exports: [UpdateComponent]
            },] }
];
/** @nocollapse */
UpdateModule.ctorParameters = () => [
    { type: UpdateService },
    { type: UpdateInfoService }
];
if (false) {
    /** @type {?} */
    UpdateModule.prototype.updateService;
    /** @type {?} */
    UpdateModule.prototype.updateInfoService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXBkYXRlLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvdXBkYXRlL3VwZGF0ZS5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxlQUFlLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUM5RSxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFDckQsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBQ2pELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBYTFELE1BQU0sT0FBTyxZQUFZOzs7OztJQUN2QixZQUFtQixhQUE0QixFQUN0QyxpQkFBb0M7UUFEMUIsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFDdEMsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtJQUU3QyxDQUFDOzs7WUFkRixRQUFRLFNBQUM7Z0JBQ1IsZUFBZSxFQUFFLENBQUMsZUFBZSxDQUFDO2dCQUNsQyxZQUFZLEVBQUUsQ0FBQyxlQUFlLENBQUM7Z0JBQy9CLE9BQU8sRUFBRTtvQkFDUCxZQUFZO29CQUNaLGVBQWU7b0JBQ2Ysd0JBQXdCO2lCQUN6QjtnQkFDRCxPQUFPLEVBQUUsQ0FBQyxlQUFlLENBQUM7YUFDM0I7Ozs7WUFiUSxhQUFhO1lBQ2IsaUJBQWlCOzs7O0lBY1oscUNBQW1DOztJQUM3Qyx5Q0FBMkMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7IE1hdERpYWxvZ01vZHVsZSwgTWF0UHJvZ3Jlc3NTcGlubmVyTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xuaW1wb3J0IHsgVXBkYXRlQ29tcG9uZW50IH0gZnJvbSAnLi91cGRhdGUuY29tcG9uZW50JztcbmltcG9ydCB7IFVwZGF0ZVNlcnZpY2UgfSBmcm9tICcuL3VwZGF0ZS5zZXJ2aWNlJztcbmltcG9ydCB7IFVwZGF0ZUluZm9TZXJ2aWNlIH0gZnJvbSAnLi91cGRhdGUtaW5mby5zZXJ2aWNlJztcblxuXG5ATmdNb2R1bGUoe1xuICBlbnRyeUNvbXBvbmVudHM6IFtVcGRhdGVDb21wb25lbnRdLFxuICBkZWNsYXJhdGlvbnM6IFtVcGRhdGVDb21wb25lbnRdLFxuICBpbXBvcnRzOiBbXG4gICAgQ29tbW9uTW9kdWxlLFxuICAgIE1hdERpYWxvZ01vZHVsZSxcbiAgICBNYXRQcm9ncmVzc1NwaW5uZXJNb2R1bGVcbiAgXSxcbiAgZXhwb3J0czogW1VwZGF0ZUNvbXBvbmVudF1cbn0pXG5leHBvcnQgY2xhc3MgVXBkYXRlTW9kdWxlIHtcbiAgY29uc3RydWN0b3IocHVibGljIHVwZGF0ZVNlcnZpY2U6IFVwZGF0ZVNlcnZpY2UsXG4gICAgcHVibGljIHVwZGF0ZUluZm9TZXJ2aWNlOiBVcGRhdGVJbmZvU2VydmljZSkge1xuXG4gIH1cbn1cbiJdfQ==