/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ChangeDetectionStrategy } from '@angular/core';
import { UpdateInfoService } from './update-info.service';
export class UpdateComponent {
    /**
     * @param {?} updateInfoService
     */
    constructor(updateInfoService) {
        this.updateInfoService = updateInfoService;
        this.hasUpdate$ = this.updateInfoService.hasUpdate$;
    }
}
UpdateComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-update',
                template: `
  <mat-progress-spinner [color]="'primary'" [mode]="'indeterminate'"></mat-progress-spinner>
  <div *ngIf="!(hasUpdate$ | async);else hasUpdate">
    Carregando...
  </div>
  <ng-template #hasUpdate>
    Atualizando...
  </ng-template>
  `,
                changeDetection: ChangeDetectionStrategy.OnPush
            }] }
];
/** @nocollapse */
UpdateComponent.ctorParameters = () => [
    { type: UpdateInfoService }
];
if (false) {
    /** @type {?} */
    UpdateComponent.prototype.hasUpdate$;
    /**
     * @type {?}
     * @protected
     */
    UpdateComponent.prototype.updateInfoService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXBkYXRlLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvdXBkYXRlL3VwZGF0ZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsdUJBQXVCLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0UsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFnQjFELE1BQU0sT0FBTyxlQUFlOzs7O0lBSTFCLFlBQXNCLGlCQUFvQztRQUFwQyxzQkFBaUIsR0FBakIsaUJBQWlCLENBQW1CO1FBRm5ELGVBQVUsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDO0lBRVEsQ0FBQzs7O1lBbEJoRSxTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLFlBQVk7Z0JBQ3RCLFFBQVEsRUFBRTs7Ozs7Ozs7R0FRVDtnQkFFRCxlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTthQUNoRDs7OztZQWZRLGlCQUFpQjs7OztJQWtCeEIscUNBQXNEOzs7OztJQUUxQyw0Q0FBOEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFVwZGF0ZUluZm9TZXJ2aWNlIH0gZnJvbSAnLi91cGRhdGUtaW5mby5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnYXBwLXVwZGF0ZScsXG4gIHRlbXBsYXRlOiBgXG4gIDxtYXQtcHJvZ3Jlc3Mtc3Bpbm5lciBbY29sb3JdPVwiJ3ByaW1hcnknXCIgW21vZGVdPVwiJ2luZGV0ZXJtaW5hdGUnXCI+PC9tYXQtcHJvZ3Jlc3Mtc3Bpbm5lcj5cbiAgPGRpdiAqbmdJZj1cIiEoaGFzVXBkYXRlJCB8IGFzeW5jKTtlbHNlIGhhc1VwZGF0ZVwiPlxuICAgIENhcnJlZ2FuZG8uLi5cbiAgPC9kaXY+XG4gIDxuZy10ZW1wbGF0ZSAjaGFzVXBkYXRlPlxuICAgIEF0dWFsaXphbmRvLi4uXG4gIDwvbmctdGVtcGxhdGU+XG4gIGAsXG4gIHN0eWxlczogW10sXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoXG59KVxuZXhwb3J0IGNsYXNzIFVwZGF0ZUNvbXBvbmVudCB7XG5cbiAgcHVibGljIGhhc1VwZGF0ZSQgPSB0aGlzLnVwZGF0ZUluZm9TZXJ2aWNlLmhhc1VwZGF0ZSQ7XG5cbiAgY29uc3RydWN0b3IocHJvdGVjdGVkIHVwZGF0ZUluZm9TZXJ2aWNlOiBVcGRhdGVJbmZvU2VydmljZSkgeyB9XG5cbn1cbiJdfQ==