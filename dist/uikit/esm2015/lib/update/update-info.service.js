/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, Optional } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { SwUpdate } from '@angular/service-worker';
import { LogService } from '../utils/log/log.service';
import { map } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "@angular/service-worker";
import * as i2 from "../utils/log/log.service";
export class UpdateInfoService {
    /**
     * @param {?} swUpdate
     * @param {?} logService
     */
    constructor(swUpdate, logService) {
        this.swUpdate = swUpdate;
        this.logService = logService;
        this.hasUpdateSubject$ = new BehaviorSubject(false);
        this.hasUpdate$ = this.hasUpdateSubject$.asObservable();
        this.installPromptEventSubject$ = new BehaviorSubject(null);
        this.installPromptEvent$ = this.installPromptEventSubject$.asObservable();
        this.hasInstallOption$ = this.installPromptEvent$.pipe(map((/**
         * @param {?} r
         * @return {?}
         */
        r => r != null)));
        window.addEventListener('beforeinstallprompt', (/**
         * @param {?} event
         * @return {?}
         */
        (event) => {
            this.logService.debug('beforeinstallprompt fired!');
            // Prevent Chrome <= 67 from automatically showing the prompt
            // event.preventDefault();
            // Stash the event so it can be triggered later.
            this.installPromptEventSubject$.next(event);
            // Update the install UI to notify the user app can be installed
            // (doing that by the btnInstall that is displayed if there a installPromptEvent instance)
            //(<any>document.querySelector('#btnInstall')).disabled = false;
        }));
    }
    /**
     * @return {?}
     */
    setUpdateAsAvailable() {
        this.logService.debug(`Atualização encontrada!`);
        this.hasUpdateSubject$.next(true);
    }
    /**
     * @return {?}
     */
    install() {
        this.logService.debug('install fired!');
        /** @type {?} */
        const installPromptEvent = this.installPromptEventSubject$.getValue();
        if (installPromptEvent) {
            this.logService.debug('installPromptEvent');
            installPromptEvent.prompt();
            // Wait for the user to respond to the prompt
            installPromptEvent.userChoice.then((/**
             * @param {?} choice
             * @return {?}
             */
            (choice) => {
                if (choice.outcome === 'accepted') {
                    this.logService.debug('User accepted the A2HS prompt');
                }
                else {
                    this.logService.debug('User dismissed the A2HS prompt');
                }
                // Clear the saved prompt since it can't be used again
                this.installPromptEventSubject$.next(null);
            }));
        }
    }
}
UpdateInfoService.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */
UpdateInfoService.ctorParameters = () => [
    { type: SwUpdate, decorators: [{ type: Optional }] },
    { type: LogService }
];
/** @nocollapse */ UpdateInfoService.ngInjectableDef = i0.defineInjectable({ factory: function UpdateInfoService_Factory() { return new UpdateInfoService(i0.inject(i1.SwUpdate, 8), i0.inject(i2.LogService)); }, token: UpdateInfoService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @protected
     */
    UpdateInfoService.prototype.hasUpdateSubject$;
    /** @type {?} */
    UpdateInfoService.prototype.hasUpdate$;
    /**
     * @type {?}
     * @protected
     */
    UpdateInfoService.prototype.installPromptEventSubject$;
    /**
     * @type {?}
     * @protected
     */
    UpdateInfoService.prototype.installPromptEvent$;
    /** @type {?} */
    UpdateInfoService.prototype.hasInstallOption$;
    /**
     * @type {?}
     * @protected
     */
    UpdateInfoService.prototype.swUpdate;
    /**
     * @type {?}
     * @protected
     */
    UpdateInfoService.prototype.logService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXBkYXRlLWluZm8uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvdXBkYXRlL3VwZGF0ZS1pbmZvLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3JELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDdkMsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQ25ELE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUN0RCxPQUFPLEVBQUUsR0FBRyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7Ozs7QUFHckMsTUFBTSxPQUFPLGlCQUFpQjs7Ozs7SUFVNUIsWUFDd0IsUUFBa0IsRUFDOUIsVUFBc0I7UUFEVixhQUFRLEdBQVIsUUFBUSxDQUFVO1FBQzlCLGVBQVUsR0FBVixVQUFVLENBQVk7UUFWeEIsc0JBQWlCLEdBQUcsSUFBSSxlQUFlLENBQVUsS0FBSyxDQUFDLENBQUM7UUFDM0QsZUFBVSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUVoRCwrQkFBMEIsR0FBRyxJQUFJLGVBQWUsQ0FBTSxJQUFJLENBQUMsQ0FBQztRQUM1RCx3QkFBbUIsR0FBRyxJQUFJLENBQUMsMEJBQTBCLENBQUMsWUFBWSxFQUFFLENBQUM7UUFFeEUsc0JBQWlCLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxHQUFHOzs7O1FBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksSUFBSSxFQUFDLENBQUMsQ0FBQztRQVE1RSxNQUFNLENBQUMsZ0JBQWdCLENBQUMscUJBQXFCOzs7O1FBQUUsQ0FBQyxLQUFLLEVBQUUsRUFBRTtZQUN2RCxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyw0QkFBNEIsQ0FBQyxDQUFDO1lBQ3BELDZEQUE2RDtZQUM3RCwwQkFBMEI7WUFHMUIsZ0RBQWdEO1lBQ2hELElBQUksQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFFNUMsZ0VBQWdFO1lBQ2hFLDBGQUEwRjtZQUMxRixnRUFBZ0U7UUFDbEUsQ0FBQyxFQUFDLENBQUM7SUFFTCxDQUFDOzs7O0lBRU0sb0JBQW9CO1FBQ3pCLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLHlCQUF5QixDQUFDLENBQUM7UUFDakQsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNwQyxDQUFDOzs7O0lBR00sT0FBTztRQUNaLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLGdCQUFnQixDQUFDLENBQUM7O2NBQ2xDLGtCQUFrQixHQUFHLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxRQUFRLEVBQUU7UUFDckUsSUFBSSxrQkFBa0IsRUFBRTtZQUN0QixJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1lBQzVDLGtCQUFrQixDQUFDLE1BQU0sRUFBRSxDQUFDO1lBQzVCLDZDQUE2QztZQUM3QyxrQkFBa0IsQ0FBQyxVQUFVLENBQUMsSUFBSTs7OztZQUFDLENBQUMsTUFBTSxFQUFFLEVBQUU7Z0JBQzVDLElBQUksTUFBTSxDQUFDLE9BQU8sS0FBSyxVQUFVLEVBQUU7b0JBQ2pDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLCtCQUErQixDQUFDLENBQUM7aUJBQ3hEO3FCQUFNO29CQUNMLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLGdDQUFnQyxDQUFDLENBQUM7aUJBQ3pEO2dCQUNELHNEQUFzRDtnQkFDdEQsSUFBSSxDQUFDLDBCQUEwQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM3QyxDQUFDLEVBQUMsQ0FBQztTQUNKO0lBQ0gsQ0FBQzs7O1lBeERGLFVBQVUsU0FBQyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUU7Ozs7WUFKekIsUUFBUSx1QkFnQlosUUFBUTtZQWZKLFVBQVU7Ozs7Ozs7O0lBTWpCLDhDQUFrRTs7SUFDbEUsdUNBQTBEOzs7OztJQUUxRCx1REFBc0U7Ozs7O0lBQ3RFLGdEQUErRTs7SUFFL0UsOENBQThFOzs7OztJQUc1RSxxQ0FBd0M7Ozs7O0lBQ3hDLHVDQUFnQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIE9wdGlvbmFsIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBCZWhhdmlvclN1YmplY3QgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IFN3VXBkYXRlIH0gZnJvbSAnQGFuZ3VsYXIvc2VydmljZS13b3JrZXInO1xuaW1wb3J0IHsgTG9nU2VydmljZSB9IGZyb20gJy4uL3V0aWxzL2xvZy9sb2cuc2VydmljZSc7XG5pbXBvcnQgeyBtYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5cbkBJbmplY3RhYmxlKHsgcHJvdmlkZWRJbjogJ3Jvb3QnIH0pXG5leHBvcnQgY2xhc3MgVXBkYXRlSW5mb1NlcnZpY2Uge1xuXG4gIHByb3RlY3RlZCBoYXNVcGRhdGVTdWJqZWN0JCA9IG5ldyBCZWhhdmlvclN1YmplY3Q8Ym9vbGVhbj4oZmFsc2UpO1xuICBwdWJsaWMgaGFzVXBkYXRlJCA9IHRoaXMuaGFzVXBkYXRlU3ViamVjdCQuYXNPYnNlcnZhYmxlKCk7XG5cbiAgcHJvdGVjdGVkIGluc3RhbGxQcm9tcHRFdmVudFN1YmplY3QkID0gbmV3IEJlaGF2aW9yU3ViamVjdDxhbnk+KG51bGwpO1xuICBwcm90ZWN0ZWQgaW5zdGFsbFByb21wdEV2ZW50JCA9IHRoaXMuaW5zdGFsbFByb21wdEV2ZW50U3ViamVjdCQuYXNPYnNlcnZhYmxlKCk7XG5cbiAgcHVibGljIGhhc0luc3RhbGxPcHRpb24kID0gdGhpcy5pbnN0YWxsUHJvbXB0RXZlbnQkLnBpcGUobWFwKHIgPT4gciAhPSBudWxsKSk7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgQE9wdGlvbmFsKCkgcHJvdGVjdGVkIHN3VXBkYXRlOiBTd1VwZGF0ZSxcbiAgICBwcm90ZWN0ZWQgbG9nU2VydmljZTogTG9nU2VydmljZSkge1xuXG5cblxuICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdiZWZvcmVpbnN0YWxscHJvbXB0JywgKGV2ZW50KSA9PiB7XG4gICAgICB0aGlzLmxvZ1NlcnZpY2UuZGVidWcoJ2JlZm9yZWluc3RhbGxwcm9tcHQgZmlyZWQhJyk7XG4gICAgICAvLyBQcmV2ZW50IENocm9tZSA8PSA2NyBmcm9tIGF1dG9tYXRpY2FsbHkgc2hvd2luZyB0aGUgcHJvbXB0XG4gICAgICAvLyBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXG5cbiAgICAgIC8vIFN0YXNoIHRoZSBldmVudCBzbyBpdCBjYW4gYmUgdHJpZ2dlcmVkIGxhdGVyLlxuICAgICAgdGhpcy5pbnN0YWxsUHJvbXB0RXZlbnRTdWJqZWN0JC5uZXh0KGV2ZW50KTtcblxuICAgICAgLy8gVXBkYXRlIHRoZSBpbnN0YWxsIFVJIHRvIG5vdGlmeSB0aGUgdXNlciBhcHAgY2FuIGJlIGluc3RhbGxlZFxuICAgICAgLy8gKGRvaW5nIHRoYXQgYnkgdGhlIGJ0bkluc3RhbGwgdGhhdCBpcyBkaXNwbGF5ZWQgaWYgdGhlcmUgYSBpbnN0YWxsUHJvbXB0RXZlbnQgaW5zdGFuY2UpXG4gICAgICAvLyg8YW55PmRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyNidG5JbnN0YWxsJykpLmRpc2FibGVkID0gZmFsc2U7XG4gICAgfSk7XG5cbiAgfVxuXG4gIHB1YmxpYyBzZXRVcGRhdGVBc0F2YWlsYWJsZSgpIHtcbiAgICB0aGlzLmxvZ1NlcnZpY2UuZGVidWcoYEF0dWFsaXphw6fDo28gZW5jb250cmFkYSFgKTtcbiAgICB0aGlzLmhhc1VwZGF0ZVN1YmplY3QkLm5leHQodHJ1ZSk7XG4gIH1cblxuXG4gIHB1YmxpYyBpbnN0YWxsKCkge1xuICAgIHRoaXMubG9nU2VydmljZS5kZWJ1ZygnaW5zdGFsbCBmaXJlZCEnKTtcbiAgICBjb25zdCBpbnN0YWxsUHJvbXB0RXZlbnQgPSB0aGlzLmluc3RhbGxQcm9tcHRFdmVudFN1YmplY3QkLmdldFZhbHVlKCk7XG4gICAgaWYgKGluc3RhbGxQcm9tcHRFdmVudCkge1xuICAgICAgdGhpcy5sb2dTZXJ2aWNlLmRlYnVnKCdpbnN0YWxsUHJvbXB0RXZlbnQnKTtcbiAgICAgIGluc3RhbGxQcm9tcHRFdmVudC5wcm9tcHQoKTtcbiAgICAgIC8vIFdhaXQgZm9yIHRoZSB1c2VyIHRvIHJlc3BvbmQgdG8gdGhlIHByb21wdFxuICAgICAgaW5zdGFsbFByb21wdEV2ZW50LnVzZXJDaG9pY2UudGhlbigoY2hvaWNlKSA9PiB7XG4gICAgICAgIGlmIChjaG9pY2Uub3V0Y29tZSA9PT0gJ2FjY2VwdGVkJykge1xuICAgICAgICAgIHRoaXMubG9nU2VydmljZS5kZWJ1ZygnVXNlciBhY2NlcHRlZCB0aGUgQTJIUyBwcm9tcHQnKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB0aGlzLmxvZ1NlcnZpY2UuZGVidWcoJ1VzZXIgZGlzbWlzc2VkIHRoZSBBMkhTIHByb21wdCcpO1xuICAgICAgICB9XG4gICAgICAgIC8vIENsZWFyIHRoZSBzYXZlZCBwcm9tcHQgc2luY2UgaXQgY2FuJ3QgYmUgdXNlZCBhZ2FpblxuICAgICAgICB0aGlzLmluc3RhbGxQcm9tcHRFdmVudFN1YmplY3QkLm5leHQobnVsbCk7XG4gICAgICB9KTtcbiAgICB9XG4gIH1cblxuXG59XG4iXX0=