/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { BehaviorSubject, timer, of } from 'rxjs';
import { OperationsSnackBar, OperationState } from './operations.snackbar';
import { distinctUntilChanged } from 'rxjs/operators';
import * as i0 from "@angular/core";
export class OperationsService {
    constructor() {
        this.operationsSubject$ = new BehaviorSubject([]);
        this.operations$ = this.operationsSubject$.asObservable();
        this.isLoadingBehaviourSubject$ = new BehaviorSubject(false);
        this.isLoading$ = this.isLoadingBehaviourSubject$.asObservable().pipe(distinctUntilChanged());
        this.isLoading = false;
    }
    /**
     * @template T
     * @param {?=} description
     * @param {?=} operation
     * @param {?=} undoOperation
     * @param {?=} allowToCancel
     * @param {?=} createAsProcessing
     * @return {?}
     */
    create(description = 'Carregando', operation, undoOperation, allowToCancel = false, createAsProcessing = true) {
        /** @type {?} */
        const operationsSnackBar = new OperationsSnackBar(description, allowToCancel);
        if (operation) {
            operationsSnackBar.setOperation(operation);
        }
        if (undoOperation) {
            operationsSnackBar.setUndoOperation(undoOperation);
        }
        this.addOperation(operationsSnackBar);
        if (createAsProcessing) {
            operationsSnackBar.setAsStarted();
        }
        /** @type {?} */
        const operationDefault = operationsSnackBar.operation$ ? operationsSnackBar.operation$ : of();
        return Object.assign(operationDefault, { snackbar: operationsSnackBar });
    }
    /**
     * @param {?=} cancellable
     * @return {?}
     */
    cancelAll(cancellable = (/**
     * @param {?} operation
     * @return {?}
     */
    (operation) => operation.canCancel)) {
        this.operationsSubject$.getValue().forEach((/**
         * @param {?} operation
         * @return {?}
         */
        operation => {
            if (cancellable(operation)) {
                operation.cancel();
            }
        }));
    }
    /**
     * @private
     * @param {?} operationState
     * @return {?}
     */
    operationIsFinished(operationState) {
        return operationState === OperationState.Processed ||
            operationState === OperationState.Cancelled ||
            operationState === OperationState.Undone ||
            operationState === OperationState.Error;
    }
    /**
     * @private
     * @return {?}
     */
    checkLoading() {
        /** @type {?} */
        const operations = this.operationsSubject$.getValue();
        /** @type {?} */
        const allFinished = operations.every((/**
         * @param {?} l
         * @return {?}
         */
        l => this.operationIsFinished(l.operationState)));
        this.isLoading = allFinished === false;
        this.isLoadingBehaviourSubject$.next(this.isLoading);
    }
    /**
     * @private
     * @param {?} loadingDialog
     * @return {?}
     */
    addOperation(loadingDialog) {
        /** @type {?} */
        const actualLoadingDialogs = this.operationsSubject$.getValue();
        loadingDialog.operationState$.subscribe((/**
         * @param {?} os
         * @return {?}
         */
        os => {
            this.checkLoading();
            if (this.operationIsFinished(os)) {
                timer(2500).subscribe((/**
                 * @return {?}
                 */
                () => {
                    if (this.operationIsFinished(os)) {
                        this.removeOperation(loadingDialog);
                    }
                }));
            }
        }));
        actualLoadingDialogs.push(loadingDialog);
        this.operationsSubject$.next(actualLoadingDialogs);
    }
    /**
     * @private
     * @param {?} loadingDialog
     * @return {?}
     */
    removeOperation(loadingDialog) {
        /** @type {?} */
        const actualLoadingDialogs = this.operationsSubject$.getValue();
        /** @type {?} */
        const index = actualLoadingDialogs.indexOf(loadingDialog);
        if (index !== -1) {
            actualLoadingDialogs.splice(index, 1);
            this.operationsSubject$.next(actualLoadingDialogs);
        }
    }
}
OperationsService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
OperationsService.ctorParameters = () => [];
/** @nocollapse */ OperationsService.ngInjectableDef = i0.defineInjectable({ factory: function OperationsService_Factory() { return new OperationsService(); }, token: OperationsService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    OperationsService.prototype.operationsSubject$;
    /** @type {?} */
    OperationsService.prototype.operations$;
    /**
     * @type {?}
     * @private
     */
    OperationsService.prototype.isLoadingBehaviourSubject$;
    /** @type {?} */
    OperationsService.prototype.isLoading$;
    /** @type {?} */
    OperationsService.prototype.isLoading;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3BlcmF0aW9ucy5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9vcGVyYXRpb25zL29wZXJhdGlvbnMuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsZUFBZSxFQUFFLEtBQUssRUFBYyxFQUFFLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDOUQsT0FBTyxFQUFFLGtCQUFrQixFQUFFLGNBQWMsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQzNFLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLGdCQUFnQixDQUFDOztBQUt0RCxNQUFNLE9BQU8saUJBQWlCO0lBUTVCO1FBUFEsdUJBQWtCLEdBQUcsSUFBSSxlQUFlLENBQXVCLEVBQUUsQ0FBQyxDQUFDO1FBQ3BFLGdCQUFXLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFlBQVksRUFBRSxDQUFDO1FBRXBELCtCQUEwQixHQUFHLElBQUksZUFBZSxDQUFVLEtBQUssQ0FBQyxDQUFDO1FBQ2xFLGVBQVUsR0FBRyxJQUFJLENBQUMsMEJBQTBCLENBQUMsWUFBWSxFQUFFLENBQUMsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUMsQ0FBQztRQUN6RixjQUFTLEdBQUcsS0FBSyxDQUFDO0lBSXpCLENBQUM7Ozs7Ozs7Ozs7SUFFTSxNQUFNLENBQ1gsV0FBVyxHQUFHLFlBQVksRUFDMUIsU0FBeUIsRUFDekIsYUFBZ0UsRUFDaEUsYUFBYSxHQUFHLEtBQUssRUFDckIsa0JBQWtCLEdBQUcsSUFBSTs7Y0FFbkIsa0JBQWtCLEdBQUcsSUFBSSxrQkFBa0IsQ0FBQyxXQUFXLEVBQUUsYUFBYSxDQUFDO1FBRTdFLElBQUksU0FBUyxFQUFFO1lBQ2Isa0JBQWtCLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1NBQzVDO1FBRUQsSUFBSSxhQUFhLEVBQUU7WUFDakIsa0JBQWtCLENBQUMsZ0JBQWdCLENBQUMsYUFBYSxDQUFDLENBQUM7U0FDcEQ7UUFFRCxJQUFJLENBQUMsWUFBWSxDQUFDLGtCQUFrQixDQUFDLENBQUM7UUFFdEMsSUFBSSxrQkFBa0IsRUFBRTtZQUN0QixrQkFBa0IsQ0FBQyxZQUFZLEVBQUUsQ0FBQztTQUNuQzs7Y0FFSyxnQkFBZ0IsR0FBRyxrQkFBa0IsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLGtCQUFrQixDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFO1FBQzdGLE9BQU8sTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsRUFBRSxFQUFFLFFBQVEsRUFBRSxrQkFBa0IsRUFBRSxDQUFDLENBQUM7SUFDM0UsQ0FBQzs7Ozs7SUFFTSxTQUFTLENBQUMsV0FBVzs7OztJQUFHLENBQUMsU0FBNkIsRUFBRSxFQUFFLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQTtRQUNuRixJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxFQUFFLENBQUMsT0FBTzs7OztRQUFDLFNBQVMsQ0FBQyxFQUFFO1lBQ3JELElBQUksV0FBVyxDQUFDLFNBQVMsQ0FBQyxFQUFFO2dCQUMxQixTQUFTLENBQUMsTUFBTSxFQUFFLENBQUM7YUFDcEI7UUFDSCxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7OztJQUVPLG1CQUFtQixDQUFDLGNBQThCO1FBQ3hELE9BQU8sY0FBYyxLQUFLLGNBQWMsQ0FBQyxTQUFTO1lBQ2hELGNBQWMsS0FBSyxjQUFjLENBQUMsU0FBUztZQUMzQyxjQUFjLEtBQUssY0FBYyxDQUFDLE1BQU07WUFDeEMsY0FBYyxLQUFLLGNBQWMsQ0FBQyxLQUFLLENBQUM7SUFDNUMsQ0FBQzs7Ozs7SUFFTyxZQUFZOztjQUNaLFVBQVUsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxFQUFFOztjQUMvQyxXQUFXLEdBQUcsVUFBVSxDQUFDLEtBQUs7Ozs7UUFBQyxDQUFDLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsY0FBYyxDQUFDLEVBQUM7UUFDckYsSUFBSSxDQUFDLFNBQVMsR0FBRyxXQUFXLEtBQUssS0FBSyxDQUFDO1FBQ3ZDLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQ3ZELENBQUM7Ozs7OztJQUVPLFlBQVksQ0FBQyxhQUFpQzs7Y0FDOUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsRUFBRTtRQUMvRCxhQUFhLENBQUMsZUFBZSxDQUFDLFNBQVM7Ozs7UUFBQyxFQUFFLENBQUMsRUFBRTtZQUMzQyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7WUFDcEIsSUFBSSxJQUFJLENBQUMsbUJBQW1CLENBQUMsRUFBRSxDQUFDLEVBQUU7Z0JBQ2hDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxTQUFTOzs7Z0JBQUMsR0FBRyxFQUFFO29CQUN6QixJQUFJLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFLENBQUMsRUFBRTt3QkFDaEMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxhQUFhLENBQUMsQ0FBQztxQkFDckM7Z0JBQ0gsQ0FBQyxFQUFDLENBQUM7YUFDSjtRQUNILENBQUMsRUFBQyxDQUFDO1FBRUgsb0JBQW9CLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ3pDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQztJQUNyRCxDQUFDOzs7Ozs7SUFFTyxlQUFlLENBQUMsYUFBaUM7O2NBQ2pELG9CQUFvQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLEVBQUU7O2NBQ3pELEtBQUssR0FBRyxvQkFBb0IsQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDO1FBQ3pELElBQUksS0FBSyxLQUFLLENBQUMsQ0FBQyxFQUFFO1lBQ2hCLG9CQUFvQixDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDdEMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1NBQ3BEO0lBQ0gsQ0FBQzs7O1lBeEZGLFVBQVUsU0FBQztnQkFDVixVQUFVLEVBQUUsTUFBTTthQUNuQjs7Ozs7Ozs7OztJQUVDLCtDQUEyRTs7SUFDM0Usd0NBQTREOzs7OztJQUU1RCx1REFBeUU7O0lBQ3pFLHVDQUFnRzs7SUFDaEcsc0NBQXlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQmVoYXZpb3JTdWJqZWN0LCB0aW1lciwgT2JzZXJ2YWJsZSwgb2YgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IE9wZXJhdGlvbnNTbmFja0JhciwgT3BlcmF0aW9uU3RhdGUgfSBmcm9tICcuL29wZXJhdGlvbnMuc25hY2tiYXInO1xuaW1wb3J0IHsgZGlzdGluY3RVbnRpbENoYW5nZWQgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIE9wZXJhdGlvbnNTZXJ2aWNlIHtcbiAgcHJpdmF0ZSBvcGVyYXRpb25zU3ViamVjdCQgPSBuZXcgQmVoYXZpb3JTdWJqZWN0PE9wZXJhdGlvbnNTbmFja0JhcltdPihbXSk7XG4gIHB1YmxpYyBvcGVyYXRpb25zJCA9IHRoaXMub3BlcmF0aW9uc1N1YmplY3QkLmFzT2JzZXJ2YWJsZSgpO1xuXG4gIHByaXZhdGUgaXNMb2FkaW5nQmVoYXZpb3VyU3ViamVjdCQgPSBuZXcgQmVoYXZpb3JTdWJqZWN0PGJvb2xlYW4+KGZhbHNlKTtcbiAgcHVibGljIGlzTG9hZGluZyQgPSB0aGlzLmlzTG9hZGluZ0JlaGF2aW91clN1YmplY3QkLmFzT2JzZXJ2YWJsZSgpLnBpcGUoZGlzdGluY3RVbnRpbENoYW5nZWQoKSk7XG4gIHB1YmxpYyBpc0xvYWRpbmcgPSBmYWxzZTtcblxuICBjb25zdHJ1Y3RvcigpIHtcblxuICB9XG5cbiAgcHVibGljIGNyZWF0ZTxUPihcbiAgICBkZXNjcmlwdGlvbiA9ICdDYXJyZWdhbmRvJyxcbiAgICBvcGVyYXRpb24/OiBPYnNlcnZhYmxlPFQ+LFxuICAgIHVuZG9PcGVyYXRpb24/OiAoKCkgPT4gT2JzZXJ2YWJsZTxhbnk+KSB8IFByb21pc2VMaWtlPGFueT4gfCBhbnksXG4gICAgYWxsb3dUb0NhbmNlbCA9IGZhbHNlLFxuICAgIGNyZWF0ZUFzUHJvY2Vzc2luZyA9IHRydWUsXG4gICk6IE9ic2VydmFibGU8VD4gJiB7IHNuYWNrYmFyOiBPcGVyYXRpb25zU25hY2tCYXIgfSB7XG4gICAgY29uc3Qgb3BlcmF0aW9uc1NuYWNrQmFyID0gbmV3IE9wZXJhdGlvbnNTbmFja0JhcihkZXNjcmlwdGlvbiwgYWxsb3dUb0NhbmNlbCk7XG5cbiAgICBpZiAob3BlcmF0aW9uKSB7XG4gICAgICBvcGVyYXRpb25zU25hY2tCYXIuc2V0T3BlcmF0aW9uKG9wZXJhdGlvbik7XG4gICAgfVxuXG4gICAgaWYgKHVuZG9PcGVyYXRpb24pIHtcbiAgICAgIG9wZXJhdGlvbnNTbmFja0Jhci5zZXRVbmRvT3BlcmF0aW9uKHVuZG9PcGVyYXRpb24pO1xuICAgIH1cblxuICAgIHRoaXMuYWRkT3BlcmF0aW9uKG9wZXJhdGlvbnNTbmFja0Jhcik7XG5cbiAgICBpZiAoY3JlYXRlQXNQcm9jZXNzaW5nKSB7XG4gICAgICBvcGVyYXRpb25zU25hY2tCYXIuc2V0QXNTdGFydGVkKCk7XG4gICAgfVxuXG4gICAgY29uc3Qgb3BlcmF0aW9uRGVmYXVsdCA9IG9wZXJhdGlvbnNTbmFja0Jhci5vcGVyYXRpb24kID8gb3BlcmF0aW9uc1NuYWNrQmFyLm9wZXJhdGlvbiQgOiBvZigpO1xuICAgIHJldHVybiBPYmplY3QuYXNzaWduKG9wZXJhdGlvbkRlZmF1bHQsIHsgc25hY2tiYXI6IG9wZXJhdGlvbnNTbmFja0JhciB9KTtcbiAgfVxuXG4gIHB1YmxpYyBjYW5jZWxBbGwoY2FuY2VsbGFibGUgPSAob3BlcmF0aW9uOiBPcGVyYXRpb25zU25hY2tCYXIpID0+IG9wZXJhdGlvbi5jYW5DYW5jZWwpIHtcbiAgICB0aGlzLm9wZXJhdGlvbnNTdWJqZWN0JC5nZXRWYWx1ZSgpLmZvckVhY2gob3BlcmF0aW9uID0+IHtcbiAgICAgIGlmIChjYW5jZWxsYWJsZShvcGVyYXRpb24pKSB7XG4gICAgICAgIG9wZXJhdGlvbi5jYW5jZWwoKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIHByaXZhdGUgb3BlcmF0aW9uSXNGaW5pc2hlZChvcGVyYXRpb25TdGF0ZTogT3BlcmF0aW9uU3RhdGUpIHtcbiAgICByZXR1cm4gb3BlcmF0aW9uU3RhdGUgPT09IE9wZXJhdGlvblN0YXRlLlByb2Nlc3NlZCB8fFxuICAgICAgb3BlcmF0aW9uU3RhdGUgPT09IE9wZXJhdGlvblN0YXRlLkNhbmNlbGxlZCB8fFxuICAgICAgb3BlcmF0aW9uU3RhdGUgPT09IE9wZXJhdGlvblN0YXRlLlVuZG9uZSB8fFxuICAgICAgb3BlcmF0aW9uU3RhdGUgPT09IE9wZXJhdGlvblN0YXRlLkVycm9yO1xuICB9XG5cbiAgcHJpdmF0ZSBjaGVja0xvYWRpbmcoKSB7XG4gICAgY29uc3Qgb3BlcmF0aW9ucyA9IHRoaXMub3BlcmF0aW9uc1N1YmplY3QkLmdldFZhbHVlKCk7XG4gICAgY29uc3QgYWxsRmluaXNoZWQgPSBvcGVyYXRpb25zLmV2ZXJ5KGwgPT4gdGhpcy5vcGVyYXRpb25Jc0ZpbmlzaGVkKGwub3BlcmF0aW9uU3RhdGUpKTtcbiAgICB0aGlzLmlzTG9hZGluZyA9IGFsbEZpbmlzaGVkID09PSBmYWxzZTtcbiAgICB0aGlzLmlzTG9hZGluZ0JlaGF2aW91clN1YmplY3QkLm5leHQodGhpcy5pc0xvYWRpbmcpO1xuICB9XG5cbiAgcHJpdmF0ZSBhZGRPcGVyYXRpb24obG9hZGluZ0RpYWxvZzogT3BlcmF0aW9uc1NuYWNrQmFyKSB7XG4gICAgY29uc3QgYWN0dWFsTG9hZGluZ0RpYWxvZ3MgPSB0aGlzLm9wZXJhdGlvbnNTdWJqZWN0JC5nZXRWYWx1ZSgpO1xuICAgIGxvYWRpbmdEaWFsb2cub3BlcmF0aW9uU3RhdGUkLnN1YnNjcmliZShvcyA9PiB7XG4gICAgICB0aGlzLmNoZWNrTG9hZGluZygpO1xuICAgICAgaWYgKHRoaXMub3BlcmF0aW9uSXNGaW5pc2hlZChvcykpIHtcbiAgICAgICAgdGltZXIoMjUwMCkuc3Vic2NyaWJlKCgpID0+IHtcbiAgICAgICAgICBpZiAodGhpcy5vcGVyYXRpb25Jc0ZpbmlzaGVkKG9zKSkge1xuICAgICAgICAgICAgdGhpcy5yZW1vdmVPcGVyYXRpb24obG9hZGluZ0RpYWxvZyk7XG4gICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9KTtcblxuICAgIGFjdHVhbExvYWRpbmdEaWFsb2dzLnB1c2gobG9hZGluZ0RpYWxvZyk7XG4gICAgdGhpcy5vcGVyYXRpb25zU3ViamVjdCQubmV4dChhY3R1YWxMb2FkaW5nRGlhbG9ncyk7XG4gIH1cblxuICBwcml2YXRlIHJlbW92ZU9wZXJhdGlvbihsb2FkaW5nRGlhbG9nOiBPcGVyYXRpb25zU25hY2tCYXIpIHtcbiAgICBjb25zdCBhY3R1YWxMb2FkaW5nRGlhbG9ncyA9IHRoaXMub3BlcmF0aW9uc1N1YmplY3QkLmdldFZhbHVlKCk7XG4gICAgY29uc3QgaW5kZXggPSBhY3R1YWxMb2FkaW5nRGlhbG9ncy5pbmRleE9mKGxvYWRpbmdEaWFsb2cpO1xuICAgIGlmIChpbmRleCAhPT0gLTEpIHtcbiAgICAgIGFjdHVhbExvYWRpbmdEaWFsb2dzLnNwbGljZShpbmRleCwgMSk7XG4gICAgICB0aGlzLm9wZXJhdGlvbnNTdWJqZWN0JC5uZXh0KGFjdHVhbExvYWRpbmdEaWFsb2dzKTtcbiAgICB9XG4gIH1cbn1cbiJdfQ==