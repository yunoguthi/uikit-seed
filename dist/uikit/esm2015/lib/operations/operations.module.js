/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OperationsComponent } from './operations.component';
import { MatProgressSpinnerModule, MatSnackBarModule, MatListModule, MatExpansionModule, MatButtonModule, MatIconModule, MatTooltipModule } from '@angular/material';
import { OperationsService } from './operations.service';
import { OperationsManagerService } from './operations-manager.service';
export class OperationsModule {
    /**
     * @param {?} operationsManagerService
     */
    constructor(operationsManagerService) {
        this.operationsManagerService = operationsManagerService;
    }
}
OperationsModule.decorators = [
    { type: NgModule, args: [{
                entryComponents: [OperationsComponent],
                declarations: [OperationsComponent],
                imports: [
                    CommonModule,
                    MatProgressSpinnerModule,
                    MatSnackBarModule,
                    MatListModule,
                    MatExpansionModule,
                    MatButtonModule,
                    MatIconModule,
                    MatTooltipModule
                ],
                exports: [OperationsComponent],
                providers: [OperationsService, OperationsManagerService]
            },] }
];
/** @nocollapse */
OperationsModule.ctorParameters = () => [
    { type: OperationsManagerService }
];
if (false) {
    /**
     * @type {?}
     * @protected
     */
    OperationsModule.prototype.operationsManagerService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3BlcmF0aW9ucy5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL29wZXJhdGlvbnMvb3BlcmF0aW9ucy5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQzdELE9BQU8sRUFDTCx3QkFBd0IsRUFBRSxpQkFBaUIsRUFBRSxhQUFhLEVBQzFELGtCQUFrQixFQUFFLGVBQWUsRUFBRSxhQUFhLEVBQUUsZ0JBQWdCLEVBQ3JFLE1BQU0sbUJBQW1CLENBQUM7QUFDM0IsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDekQsT0FBTyxFQUFFLHdCQUF3QixFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFrQnhFLE1BQU0sT0FBTyxnQkFBZ0I7Ozs7SUFDM0IsWUFBc0Isd0JBQWtEO1FBQWxELDZCQUF3QixHQUF4Qix3QkFBd0IsQ0FBMEI7SUFDeEUsQ0FBQzs7O1lBbEJGLFFBQVEsU0FBQztnQkFDUixlQUFlLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQztnQkFDdEMsWUFBWSxFQUFFLENBQUMsbUJBQW1CLENBQUM7Z0JBQ25DLE9BQU8sRUFBRTtvQkFDUCxZQUFZO29CQUNaLHdCQUF3QjtvQkFDeEIsaUJBQWlCO29CQUNqQixhQUFhO29CQUNiLGtCQUFrQjtvQkFDbEIsZUFBZTtvQkFDZixhQUFhO29CQUNiLGdCQUFnQjtpQkFDakI7Z0JBQ0QsT0FBTyxFQUFFLENBQUMsbUJBQW1CLENBQUM7Z0JBQzlCLFNBQVMsRUFBRSxDQUFDLGlCQUFpQixFQUFFLHdCQUF3QixDQUFDO2FBQ3pEOzs7O1lBakJRLHdCQUF3Qjs7Ozs7OztJQW1CbkIsb0RBQTREIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQgeyBPcGVyYXRpb25zQ29tcG9uZW50IH0gZnJvbSAnLi9vcGVyYXRpb25zLmNvbXBvbmVudCc7XG5pbXBvcnQge1xuICBNYXRQcm9ncmVzc1NwaW5uZXJNb2R1bGUsIE1hdFNuYWNrQmFyTW9kdWxlLCBNYXRMaXN0TW9kdWxlLFxuICBNYXRFeHBhbnNpb25Nb2R1bGUsIE1hdEJ1dHRvbk1vZHVsZSwgTWF0SWNvbk1vZHVsZSwgTWF0VG9vbHRpcE1vZHVsZVxufSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XG5pbXBvcnQgeyBPcGVyYXRpb25zU2VydmljZSB9IGZyb20gJy4vb3BlcmF0aW9ucy5zZXJ2aWNlJztcbmltcG9ydCB7IE9wZXJhdGlvbnNNYW5hZ2VyU2VydmljZSB9IGZyb20gJy4vb3BlcmF0aW9ucy1tYW5hZ2VyLnNlcnZpY2UnO1xuXG5ATmdNb2R1bGUoe1xuICBlbnRyeUNvbXBvbmVudHM6IFtPcGVyYXRpb25zQ29tcG9uZW50XSxcbiAgZGVjbGFyYXRpb25zOiBbT3BlcmF0aW9uc0NvbXBvbmVudF0sXG4gIGltcG9ydHM6IFtcbiAgICBDb21tb25Nb2R1bGUsXG4gICAgTWF0UHJvZ3Jlc3NTcGlubmVyTW9kdWxlLFxuICAgIE1hdFNuYWNrQmFyTW9kdWxlLFxuICAgIE1hdExpc3RNb2R1bGUsXG4gICAgTWF0RXhwYW5zaW9uTW9kdWxlLFxuICAgIE1hdEJ1dHRvbk1vZHVsZSxcbiAgICBNYXRJY29uTW9kdWxlLFxuICAgIE1hdFRvb2x0aXBNb2R1bGVcbiAgXSxcbiAgZXhwb3J0czogW09wZXJhdGlvbnNDb21wb25lbnRdLFxuICBwcm92aWRlcnM6IFtPcGVyYXRpb25zU2VydmljZSwgT3BlcmF0aW9uc01hbmFnZXJTZXJ2aWNlXVxufSlcbmV4cG9ydCBjbGFzcyBPcGVyYXRpb25zTW9kdWxlIHtcbiAgY29uc3RydWN0b3IocHJvdGVjdGVkIG9wZXJhdGlvbnNNYW5hZ2VyU2VydmljZTogT3BlcmF0aW9uc01hbmFnZXJTZXJ2aWNlKSB7XG4gIH1cbn1cbiJdfQ==