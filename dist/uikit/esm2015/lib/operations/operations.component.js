/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ViewEncapsulation } from '@angular/core';
import { OperationsService } from './operations.service';
import { trigger, style, state, transition, animate } from '@angular/animations';
import { OperationsManagerService } from './operations-manager.service';
export class OperationsComponent {
    /**
     * @param {?} operationsService
     * @param {?} operationsManagerService
     */
    constructor(operationsService, operationsManagerService) {
        this.operationsService = operationsService;
        this.operationsManagerService = operationsManagerService;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.operations$ = this.operationsService.operations$;
        this.isLoading$ = this.operationsService.isLoading$;
    }
    /**
     * @return {?}
     */
    close() {
        this.operationsManagerService.close();
    }
}
OperationsComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-operations',
                template: "<mat-expansion-panel [expanded]=\"(operations$ | async).length == 1\">\n\n  <mat-expansion-panel-header *ngIf=\"(operations$ | async).length > 1\">\n    <mat-panel-title>\n      <div *ngIf=\"(isLoading$ | async); else notLoading\">\n        Processando {{ (operations$ | async).length }} opera\u00E7\u00F5es...\n      </div>\n      <ng-template #notLoading>\n        Finalizado\n      </ng-template>\n    </mat-panel-title>\n\n  </mat-expansion-panel-header>\n\n  <mat-list>\n    <mat-list-item *ngFor=\"let operation of (operations$ | async)\" [@fadeInOut]>\n      <h4 matLine>{{ operation.description }}</h4>\n\n      <mat-progress-spinner\n        *ngIf=\"\n        (operation.operationState$ | async) === 'processing' ||\n        (operation.operationState$ | async) === 'cancelling' ||\n        (operation.operationState$ | async) === 'undoing'\n        \"\n        [diameter]=\"20\" mode=\"indeterminate\" color=\"primary\"></mat-progress-spinner>\n\n      <div *ngIf=\"(operation.operationState$ | async) === 'processed'\">Finalizado</div>\n      <div *ngIf=\"(operation.operationState$ | async) === 'cancelling'\">Cancelando</div>\n      <div *ngIf=\"(operation.operationState$ | async) === 'cancelled'\">Cancelado</div>\n      <div *ngIf=\"(operation.operationState$ | async) === 'undoing'\">Desfazendo</div>\n      <div *ngIf=\"(operation.operationState$ | async) === 'undone'\">Desfeito</div>\n      <div *ngIf=\"(operation.operationState$ | async) === 'error'\" matTooltip=\"{{ operation.error.message }}\">Erro</div>\n\n        <button *ngIf=\"operation.canUndo$ | async\"\n        (click)=\"operation.undo()\" mat-icon-button><mat-icon class=\"fas fa-undo\"></mat-icon></button>\n\n        <button *ngIf=\"operation.canCancel$ | async\"\n        (click)=\"operation.cancel()\" mat-icon-button><mat-icon class=\"fas fa-times-circle\"></mat-icon></button>\n\n    </mat-list-item>\n  </mat-list>\n\n</mat-expansion-panel>\n",
                encapsulation: ViewEncapsulation.None,
                animations: [
                    trigger('fadeInOut', [
                        state('void', style({
                            opacity: 0
                        })),
                        transition('void => *', animate(250)),
                        transition('* => void', animate(500)),
                    ]),
                ],
                styles: [".operation-dialog{background:0 0;padding:0;min-height:auto!important}.operation-dialog .mat-expansion-panel .mat-expansion-panel-header{background:rgba(0,0,0,.9)!important;border-radius:0;padding:0 15px}.operation-dialog .mat-expansion-panel .mat-expansion-panel-header .mat-expansion-indicator:after,.operation-dialog .mat-expansion-panel .mat-expansion-panel-header .mat-expansion-panel-header-title{color:#fff}.operation-dialog .mat-expansion-panel .mat-expansion-panel-body{padding:0 15px!important;background:rgba(0,0,0,.85)!important}.operation-dialog .mat-expansion-panel .mat-expansion-panel-body .mat-list{padding:0!important}.operation-dialog .mat-expansion-panel .mat-expansion-panel-body .mat-list .mat-list-item{min-height:initial!important;padding:10px 0!important;color:#fff!important;border-bottom:1px solid rgba(255,255,255,.1)}.operation-dialog .mat-expansion-panel .mat-expansion-panel-body .mat-list .mat-list-item:last-child{border:none}.operation-dialog .mat-list-item-content{padding:0!important}"]
            }] }
];
/** @nocollapse */
OperationsComponent.ctorParameters = () => [
    { type: OperationsService },
    { type: OperationsManagerService }
];
if (false) {
    /** @type {?} */
    OperationsComponent.prototype.operations$;
    /** @type {?} */
    OperationsComponent.prototype.isLoading$;
    /**
     * @type {?}
     * @protected
     */
    OperationsComponent.prototype.operationsService;
    /**
     * @type {?}
     * @protected
     */
    OperationsComponent.prototype.operationsManagerService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3BlcmF0aW9ucy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL29wZXJhdGlvbnMvb3BlcmF0aW9ucy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsaUJBQWlCLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDckUsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFHekQsT0FBTyxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxPQUFPLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUNqRixPQUFPLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQWlCeEUsTUFBTSxPQUFPLG1CQUFtQjs7Ozs7SUFLOUIsWUFDWSxpQkFBb0MsRUFDcEMsd0JBQWtEO1FBRGxELHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBbUI7UUFDcEMsNkJBQXdCLEdBQXhCLHdCQUF3QixDQUEwQjtJQUMxRCxDQUFDOzs7O0lBRUwsUUFBUTtRQUNOLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFdBQVcsQ0FBQztRQUN0RCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUM7SUFDdEQsQ0FBQzs7OztJQUVELEtBQUs7UUFDSCxJQUFJLENBQUMsd0JBQXdCLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDeEMsQ0FBQzs7O1lBaENGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsa0JBQWtCO2dCQUM1Qix5NURBQTBDO2dCQUUxQyxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTtnQkFDckMsVUFBVSxFQUFFO29CQUNWLE9BQU8sQ0FBQyxXQUFXLEVBQUU7d0JBQ25CLEtBQUssQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDOzRCQUNsQixPQUFPLEVBQUUsQ0FBQzt5QkFDWCxDQUFDLENBQUM7d0JBQ0gsVUFBVSxDQUFDLFdBQVcsRUFBRSxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7d0JBQ3JDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO3FCQUN0QyxDQUFDO2lCQUNIOzthQUNGOzs7O1lBcEJRLGlCQUFpQjtZQUlqQix3QkFBd0I7Ozs7SUFtQi9CLDBDQUFxRDs7SUFDckQseUNBQXVDOzs7OztJQUdyQyxnREFBOEM7Ozs7O0lBQzlDLHVEQUE0RCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBWaWV3RW5jYXBzdWxhdGlvbiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgT3BlcmF0aW9uc1NlcnZpY2UgfSBmcm9tICcuL29wZXJhdGlvbnMuc2VydmljZSc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBPcGVyYXRpb25zU25hY2tCYXIgfSBmcm9tICcuL29wZXJhdGlvbnMuc25hY2tiYXInO1xuaW1wb3J0IHsgdHJpZ2dlciwgc3R5bGUsIHN0YXRlLCB0cmFuc2l0aW9uLCBhbmltYXRlIH0gZnJvbSAnQGFuZ3VsYXIvYW5pbWF0aW9ucyc7XG5pbXBvcnQgeyBPcGVyYXRpb25zTWFuYWdlclNlcnZpY2UgfSBmcm9tICcuL29wZXJhdGlvbnMtbWFuYWdlci5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAndWlraXQtb3BlcmF0aW9ucycsXG4gIHRlbXBsYXRlVXJsOiAnLi9vcGVyYXRpb25zLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vb3BlcmF0aW9ucy5jb21wb25lbnQuc2NzcyddLFxuICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lLFxuICBhbmltYXRpb25zOiBbXG4gICAgdHJpZ2dlcignZmFkZUluT3V0JywgW1xuICAgICAgc3RhdGUoJ3ZvaWQnLCBzdHlsZSh7XG4gICAgICAgIG9wYWNpdHk6IDBcbiAgICAgIH0pKSxcbiAgICAgIHRyYW5zaXRpb24oJ3ZvaWQgPT4gKicsIGFuaW1hdGUoMjUwKSksXG4gICAgICB0cmFuc2l0aW9uKCcqID0+IHZvaWQnLCBhbmltYXRlKDUwMCkpLFxuICAgIF0pLFxuICBdXG59KVxuZXhwb3J0IGNsYXNzIE9wZXJhdGlvbnNDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIHB1YmxpYyBvcGVyYXRpb25zJDogT2JzZXJ2YWJsZTxPcGVyYXRpb25zU25hY2tCYXJbXT47XG4gIHB1YmxpYyBpc0xvYWRpbmckOiBPYnNlcnZhYmxlPGJvb2xlYW4+O1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByb3RlY3RlZCBvcGVyYXRpb25zU2VydmljZTogT3BlcmF0aW9uc1NlcnZpY2UsXG4gICAgcHJvdGVjdGVkIG9wZXJhdGlvbnNNYW5hZ2VyU2VydmljZTogT3BlcmF0aW9uc01hbmFnZXJTZXJ2aWNlXG4gICkgeyB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgdGhpcy5vcGVyYXRpb25zJCA9IHRoaXMub3BlcmF0aW9uc1NlcnZpY2Uub3BlcmF0aW9ucyQ7XG4gICAgdGhpcy5pc0xvYWRpbmckID0gdGhpcy5vcGVyYXRpb25zU2VydmljZS5pc0xvYWRpbmckO1xuICB9XG5cbiAgY2xvc2UoKSB7XG4gICAgdGhpcy5vcGVyYXRpb25zTWFuYWdlclNlcnZpY2UuY2xvc2UoKTtcbiAgfVxufVxuIl19