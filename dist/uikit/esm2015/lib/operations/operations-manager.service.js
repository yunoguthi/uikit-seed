/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { OperationsService } from './operations.service';
import { timer } from 'rxjs';
import { OperationsComponent } from './operations.component';
import { MatSnackBar } from '@angular/material';
export class OperationsManagerService {
    /**
     * @param {?} operationsService
     * @param {?} snackBar
     */
    constructor(operationsService, snackBar) {
        this.operationsService = operationsService;
        this.snackBar = snackBar;
        // Atualiza a variavel conforme o observable atualizar de valor
        this.operationsService.isLoading$.subscribe((/**
         * @param {?} isLoading
         * @return {?}
         */
        (isLoading) => {
            if (isLoading) {
                // Verifico se o snackbar (de operações) já está sendo exibida
                // para evitar abrir outra (ocasionando em um fecha/abre desnecessário)
                if (this.isLoadSnackbarActive()) {
                    /** @type {?} */
                    const loadingMaterialDialog = this.snackBar.openFromComponent(OperationsComponent, {
                        verticalPosition: 'bottom',
                        horizontalPosition: 'center',
                        panelClass: 'operation-dialog',
                        announcementMessage: 'Processando...',
                    });
                }
            }
            else {
                timer(2500).subscribe((/**
                 * @param {?} loading
                 * @return {?}
                 */
                (loading) => {
                    if (!this.operationsService.isLoading) {
                        this.close();
                    }
                }));
            }
        }));
    }
    /**
     * @private
     * @return {?}
     */
    isLoadSnackbarActive() {
        return !(this.snackBar._openedSnackBarRef &&
            this.snackBar._openedSnackBarRef.instance instanceof OperationsComponent);
    }
    /**
     * @return {?}
     */
    close() {
        this.snackBar.dismiss();
    }
}
OperationsManagerService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
OperationsManagerService.ctorParameters = () => [
    { type: OperationsService },
    { type: MatSnackBar }
];
if (false) {
    /**
     * @type {?}
     * @protected
     */
    OperationsManagerService.prototype.operationsService;
    /**
     * @type {?}
     * @protected
     */
    OperationsManagerService.prototype.snackBar;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3BlcmF0aW9ucy1tYW5hZ2VyLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL29wZXJhdGlvbnMvb3BlcmF0aW9ucy1tYW5hZ2VyLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDekQsT0FBTyxFQUFFLEtBQUssRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUM3QixPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUM3RCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFHaEQsTUFBTSxPQUFPLHdCQUF3Qjs7Ozs7SUFNbkMsWUFBc0IsaUJBQW9DLEVBQVksUUFBcUI7UUFBckUsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQUFZLGFBQVEsR0FBUixRQUFRLENBQWE7UUFFekYsK0RBQStEO1FBQy9ELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsU0FBUzs7OztRQUFDLENBQUMsU0FBUyxFQUFFLEVBQUU7WUFDeEQsSUFBSSxTQUFTLEVBQUU7Z0JBQ2IsOERBQThEO2dCQUM5RCx1RUFBdUU7Z0JBQ3ZFLElBQUksSUFBSSxDQUFDLG9CQUFvQixFQUFFLEVBQUU7OzBCQUV6QixxQkFBcUIsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLGlCQUFpQixDQUFDLG1CQUFtQixFQUMvRTt3QkFDRSxnQkFBZ0IsRUFBRSxRQUFRO3dCQUMxQixrQkFBa0IsRUFBRSxRQUFRO3dCQUM1QixVQUFVLEVBQUUsa0JBQWtCO3dCQUM5QixtQkFBbUIsRUFBRSxnQkFBZ0I7cUJBQ3RDLENBQ0Y7aUJBQ0Y7YUFDRjtpQkFBTTtnQkFDTCxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsU0FBUzs7OztnQkFBQyxDQUFDLE9BQU8sRUFBRSxFQUFFO29CQUNoQyxJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsRUFBRTt3QkFDckMsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO3FCQUNkO2dCQUNILENBQUMsRUFBQyxDQUFDO2FBQ0o7UUFDSCxDQUFDLEVBQUMsQ0FBQztJQUVMLENBQUM7Ozs7O0lBaENPLG9CQUFvQjtRQUMxQixPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGtCQUFrQjtZQUN2QyxJQUFJLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsWUFBWSxtQkFBbUIsQ0FBQyxDQUFDO0lBQzlFLENBQUM7Ozs7SUErQk0sS0FBSztRQUNWLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDMUIsQ0FBQzs7O1lBdENGLFVBQVU7Ozs7WUFMRixpQkFBaUI7WUFHakIsV0FBVzs7Ozs7OztJQVNOLHFEQUE4Qzs7Ozs7SUFBRSw0Q0FBK0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBPcGVyYXRpb25zU2VydmljZSB9IGZyb20gJy4vb3BlcmF0aW9ucy5zZXJ2aWNlJztcbmltcG9ydCB7IHRpbWVyIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBPcGVyYXRpb25zQ29tcG9uZW50IH0gZnJvbSAnLi9vcGVyYXRpb25zLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBNYXRTbmFja0JhciB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIE9wZXJhdGlvbnNNYW5hZ2VyU2VydmljZSB7XG4gIHByaXZhdGUgaXNMb2FkU25hY2tiYXJBY3RpdmUoKSB7XG4gICAgcmV0dXJuICEodGhpcy5zbmFja0Jhci5fb3BlbmVkU25hY2tCYXJSZWYgJiZcbiAgICAgIHRoaXMuc25hY2tCYXIuX29wZW5lZFNuYWNrQmFyUmVmLmluc3RhbmNlIGluc3RhbmNlb2YgT3BlcmF0aW9uc0NvbXBvbmVudCk7XG4gIH1cblxuICBjb25zdHJ1Y3Rvcihwcm90ZWN0ZWQgb3BlcmF0aW9uc1NlcnZpY2U6IE9wZXJhdGlvbnNTZXJ2aWNlLCBwcm90ZWN0ZWQgc25hY2tCYXI6IE1hdFNuYWNrQmFyKSB7XG5cbiAgICAvLyBBdHVhbGl6YSBhIHZhcmlhdmVsIGNvbmZvcm1lIG8gb2JzZXJ2YWJsZSBhdHVhbGl6YXIgZGUgdmFsb3JcbiAgICB0aGlzLm9wZXJhdGlvbnNTZXJ2aWNlLmlzTG9hZGluZyQuc3Vic2NyaWJlKChpc0xvYWRpbmcpID0+IHtcbiAgICAgIGlmIChpc0xvYWRpbmcpIHtcbiAgICAgICAgLy8gVmVyaWZpY28gc2UgbyBzbmFja2JhciAoZGUgb3BlcmHDp8O1ZXMpIGrDoSBlc3TDoSBzZW5kbyBleGliaWRhXG4gICAgICAgIC8vIHBhcmEgZXZpdGFyIGFicmlyIG91dHJhIChvY2FzaW9uYW5kbyBlbSB1bSBmZWNoYS9hYnJlIGRlc25lY2Vzc8OhcmlvKVxuICAgICAgICBpZiAodGhpcy5pc0xvYWRTbmFja2JhckFjdGl2ZSgpKSB7XG5cbiAgICAgICAgICBjb25zdCBsb2FkaW5nTWF0ZXJpYWxEaWFsb2cgPSB0aGlzLnNuYWNrQmFyLm9wZW5Gcm9tQ29tcG9uZW50KE9wZXJhdGlvbnNDb21wb25lbnQsXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIHZlcnRpY2FsUG9zaXRpb246ICdib3R0b20nLFxuICAgICAgICAgICAgICBob3Jpem9udGFsUG9zaXRpb246ICdjZW50ZXInLFxuICAgICAgICAgICAgICBwYW5lbENsYXNzOiAnb3BlcmF0aW9uLWRpYWxvZycsXG4gICAgICAgICAgICAgIGFubm91bmNlbWVudE1lc3NhZ2U6ICdQcm9jZXNzYW5kby4uLicsXG4gICAgICAgICAgICB9XG4gICAgICAgICAgKTtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGltZXIoMjUwMCkuc3Vic2NyaWJlKChsb2FkaW5nKSA9PiB7XG4gICAgICAgICAgaWYgKCF0aGlzLm9wZXJhdGlvbnNTZXJ2aWNlLmlzTG9hZGluZykge1xuICAgICAgICAgICAgdGhpcy5jbG9zZSgpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICB9XG4gICAgfSk7XG5cbiAgfVxuXG4gIHB1YmxpYyBjbG9zZSgpIHtcbiAgICB0aGlzLnNuYWNrQmFyLmRpc21pc3MoKTtcbiAgfVxufVxuIl19