/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { BehaviorSubject, combineLatest, throwError } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';
/** @enum {string} */
const OperationState = {
    Idle: 'idle',
    Processing: 'processing',
    Processed: 'processed',
    Cancelling: 'cancelling',
    Cancelled: 'cancelled',
    Undoing: 'undoing',
    Undone: 'undone',
    Error: 'error',
};
export { OperationState };
export class OperationsSnackBar {
    /**
     * @param {?} description
     * @param {?=} allowToCancel
     */
    constructor(description, allowToCancel = false) {
        this.description = description;
        this.allowToCancel = allowToCancel;
        this.operationStateSubject$ = new BehaviorSubject(OperationState.Idle);
        this.operationState$ = this.operationStateSubject$.asObservable();
        this.operationState = OperationState.Processing;
        this.error = null;
        this.operation$ = null;
        this.undoOperation = null;
        this.isUndoableSubject$ = new BehaviorSubject(false);
        this.isUndoable$ = this.isUndoableSubject$.asObservable();
        this.isUndoable = false;
        this.canUndoSubject$ = new BehaviorSubject(false);
        this.canUndo$ = this.canUndoSubject$.asObservable();
        this.canUndo = false;
        this.cancelOperation = null;
        this.isCancellableSubject$ = new BehaviorSubject(false);
        this.isCancellable$ = this.isCancellableSubject$.asObservable();
        this.isCancellable = false;
        this.canCancelSubject$ = new BehaviorSubject(false);
        this.canCancel$ = this.canCancelSubject$.asObservable();
        this.canCancel = false;
        // Mantem atualizado as variáveis 'não observável'
        this.operationState$.subscribe((/**
         * @param {?} state
         * @return {?}
         */
        (state) => this.operationState = state));
        this.isUndoable$.subscribe((/**
         * @param {?} isUndoable
         * @return {?}
         */
        isUndoable => this.isUndoable = isUndoable));
        this.isCancellable$.subscribe((/**
         * @param {?} isCancellable
         * @return {?}
         */
        isCancellable => this.isCancellable = isCancellable));
        combineLatest(this.operationState$, this.isCancellable$).subscribe((/**
         * @param {?} value
         * @return {?}
         */
        (value) => {
            /** @type {?} */
            const state = value[0];
            /** @type {?} */
            const isCancellable = value[1];
            if (state === OperationState.Processing && this.allowToCancel) {
                this.canCancel = isCancellable;
            }
            else {
                this.canCancel = false;
            }
            this.canCancelSubject$.next(this.canCancel);
        }));
        combineLatest(this.operationState$, this.isUndoable$).subscribe((/**
         * @param {?} value
         * @return {?}
         */
        (value) => {
            /** @type {?} */
            const state = value[0];
            /** @type {?} */
            const isUndoable = value[1];
            if (state === OperationState.Processed) {
                this.canUndo = isUndoable;
            }
            else {
                this.canUndo = false;
            }
            this.canUndoSubject$.next(this.canUndo);
        }));
    }
    /**
     * @param {?} subscription
     * @return {?}
     */
    setCancelOperation(subscription) {
        if (!subscription) {
            throw Error('Não é possível setar uma operação de cancelar com a subscrição nula (null) ou indefinida (undefined)!');
        }
        if (subscription && subscription.closed === false) {
            this.cancelOperation = subscription;
            this.isCancellableSubject$.next(true);
        }
    }
    /**
     * @param {?} undoFunction
     * @return {?}
     */
    setUndoOperation(undoFunction) {
        if (!undoFunction) {
            throw Error('Não é possível setar uma operação de desfazer com a operação nula (null) ou indefinida (undefined)!');
        }
        else {
            this.undoOperation = undoFunction;
            this.isUndoableSubject$.next(true);
        }
    }
    /**
     * @param {?} operation
     * @return {?}
     */
    setOperation(operation) {
        /** @type {?} */
        const operationObservavel = operation
            .pipe(catchError((/**
         * @param {?} error
         * @return {?}
         */
        (error) => {
            return this.setError(error);
        })), finalize((/**
         * @return {?}
         */
        () => {
            this.setAsProcessed();
        })));
        this.operation$ = operationObservavel;
    }
    /**
     * @return {?}
     */
    setAsProcessed() {
        this.setState(OperationState.Processed);
    }
    /**
     * @param {?} error
     * @return {?}
     */
    setError(error) {
        this.operationStateSubject$.next(OperationState.Error);
        this.error = error;
        return throwError(error);
    }
    /**
     * @protected
     * @param {?} operationState
     * @return {?}
     */
    setState(operationState) {
        this.operationStateSubject$.next(operationState);
    }
    /**
     * @return {?}
     */
    setAsStarted() {
        this.setState(OperationState.Processing);
    }
    /**
     * @return {?}
     */
    undo() {
        if (!this.isUndoableSubject$.getValue()) {
            throw Error('Não é possível desfazer esta operação!');
        }
        /** @type {?} */
        let retorno = null;
        this.operationStateSubject$.next(OperationState.Undoing);
        /** @type {?} */
        const undoOperation = this.undoOperation;
        /** @type {?} */
        const observable = (/** @type {?} */ (undoOperation));
        /** @type {?} */
        const isObservable = observable.pipe;
        if (isObservable) {
            retorno = observable.pipe(catchError((/**
             * @param {?} error
             * @return {?}
             */
            error => this.setError(error))))
                .subscribe((/**
             * @return {?}
             */
            () => this.operationStateSubject$.next(OperationState.Undone)));
        }
        /** @type {?} */
        const promise = (/** @type {?} */ (undoOperation));
        /** @type {?} */
        const isPromise = promise.then;
        if (isPromise) {
            retorno = promise.then((/**
             * @return {?}
             */
            () => this.operationStateSubject$.next(OperationState.Undone)), (/**
             * @param {?} error
             * @return {?}
             */
            (error) => this.setError(error)));
        }
        /** @type {?} */
        const undoLambda = (/** @type {?} */ (undoOperation));
        /** @type {?} */
        const isUndoLambda = undoLambda.call;
        if ((!isObservable) && (!isPromise) && isUndoLambda) {
            try {
                retorno = undoLambda();
                this.operationStateSubject$.next(OperationState.Undone);
            }
            catch (error) {
                this.setError(error);
                throw error;
            }
        }
        return retorno;
    }
    /**
     * @return {?}
     */
    cancel() {
        if (!this.canCancel) {
            throw Error('Não é possível cancelar esta operação!');
        }
        this.operationStateSubject$.next(OperationState.Cancelling);
        if (this.cancelOperation && this.cancelOperation.closed === false) {
            this.cancelOperation.add((/**
             * @return {?}
             */
            () => {
                this.operationStateSubject$.next(OperationState.Cancelled);
                this.isCancellableSubject$.next(false);
            }));
            this.cancelOperation.unsubscribe();
        }
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    OperationsSnackBar.prototype.operationStateSubject$;
    /** @type {?} */
    OperationsSnackBar.prototype.operationState$;
    /** @type {?} */
    OperationsSnackBar.prototype.operationState;
    /** @type {?} */
    OperationsSnackBar.prototype.error;
    /** @type {?} */
    OperationsSnackBar.prototype.operation$;
    /**
     * @type {?}
     * @protected
     */
    OperationsSnackBar.prototype.undoOperation;
    /**
     * @type {?}
     * @private
     */
    OperationsSnackBar.prototype.isUndoableSubject$;
    /**
     * @type {?}
     * @protected
     */
    OperationsSnackBar.prototype.isUndoable$;
    /**
     * @type {?}
     * @protected
     */
    OperationsSnackBar.prototype.isUndoable;
    /**
     * @type {?}
     * @private
     */
    OperationsSnackBar.prototype.canUndoSubject$;
    /** @type {?} */
    OperationsSnackBar.prototype.canUndo$;
    /** @type {?} */
    OperationsSnackBar.prototype.canUndo;
    /**
     * @type {?}
     * @protected
     */
    OperationsSnackBar.prototype.cancelOperation;
    /**
     * @type {?}
     * @private
     */
    OperationsSnackBar.prototype.isCancellableSubject$;
    /**
     * @type {?}
     * @protected
     */
    OperationsSnackBar.prototype.isCancellable$;
    /**
     * @type {?}
     * @protected
     */
    OperationsSnackBar.prototype.isCancellable;
    /**
     * @type {?}
     * @private
     */
    OperationsSnackBar.prototype.canCancelSubject$;
    /** @type {?} */
    OperationsSnackBar.prototype.canCancel$;
    /** @type {?} */
    OperationsSnackBar.prototype.canCancel;
    /** @type {?} */
    OperationsSnackBar.prototype.description;
    /** @type {?} */
    OperationsSnackBar.prototype.allowToCancel;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3BlcmF0aW9ucy5zbmFja2Jhci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvb3BlcmF0aW9ucy9vcGVyYXRpb25zLnNuYWNrYmFyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQWdCLGVBQWUsRUFBYyxhQUFhLEVBQUUsVUFBVSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQzVGLE9BQU8sRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7OztJQUdwRCxNQUFPLE1BQU07SUFDYixZQUFhLFlBQVk7SUFDekIsV0FBWSxXQUFXO0lBQ3ZCLFlBQWEsWUFBWTtJQUN6QixXQUFZLFdBQVc7SUFDdkIsU0FBVSxTQUFTO0lBQ25CLFFBQVMsUUFBUTtJQUNqQixPQUFRLE9BQU87OztBQUdqQixNQUFNLE9BQU8sa0JBQWtCOzs7OztJQTZCN0IsWUFDUyxXQUFtQixFQUNuQixnQkFBZ0IsS0FBSztRQURyQixnQkFBVyxHQUFYLFdBQVcsQ0FBUTtRQUNuQixrQkFBYSxHQUFiLGFBQWEsQ0FBUTtRQTdCdEIsMkJBQXNCLEdBQUcsSUFBSSxlQUFlLENBQWlCLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNuRixvQkFBZSxHQUFHLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUM3RCxtQkFBYyxHQUFHLGNBQWMsQ0FBQyxVQUFVLENBQUM7UUFFM0MsVUFBSyxHQUFVLElBQUksQ0FBQztRQUVwQixlQUFVLEdBQW9CLElBQUksQ0FBQztRQUVoQyxrQkFBYSxHQUFxRCxJQUFJLENBQUM7UUFDekUsdUJBQWtCLEdBQUcsSUFBSSxlQUFlLENBQVUsS0FBSyxDQUFDLENBQUM7UUFDdkQsZ0JBQVcsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDckQsZUFBVSxHQUFHLEtBQUssQ0FBQztRQUVyQixvQkFBZSxHQUFHLElBQUksZUFBZSxDQUFVLEtBQUssQ0FBQyxDQUFDO1FBQ3ZELGFBQVEsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQy9DLFlBQU8sR0FBRyxLQUFLLENBQUM7UUFHYixvQkFBZSxHQUFpQixJQUFJLENBQUM7UUFDdkMsMEJBQXFCLEdBQUcsSUFBSSxlQUFlLENBQVUsS0FBSyxDQUFDLENBQUM7UUFDMUQsbUJBQWMsR0FBRyxJQUFJLENBQUMscUJBQXFCLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDM0Qsa0JBQWEsR0FBRyxLQUFLLENBQUM7UUFFeEIsc0JBQWlCLEdBQUcsSUFBSSxlQUFlLENBQVUsS0FBSyxDQUFDLENBQUM7UUFDekQsZUFBVSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUNuRCxjQUFTLEdBQUcsS0FBSyxDQUFDO1FBTXZCLGtEQUFrRDtRQUNsRCxJQUFJLENBQUMsZUFBZSxDQUFDLFNBQVM7Ozs7UUFBQyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLEVBQUMsQ0FBQztRQUN2RSxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVM7Ozs7UUFBQyxVQUFVLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxFQUFDLENBQUM7UUFDdkUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTOzs7O1FBQUMsYUFBYSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsYUFBYSxHQUFHLGFBQWEsRUFBQyxDQUFDO1FBRW5GLGFBQWEsQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxTQUFTOzs7O1FBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRTs7a0JBQ3JFLEtBQUssR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDOztrQkFDaEIsYUFBYSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDOUIsSUFBSSxLQUFLLEtBQUssY0FBYyxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO2dCQUM3RCxJQUFJLENBQUMsU0FBUyxHQUFHLGFBQWEsQ0FBQzthQUNoQztpQkFBTTtnQkFDTCxJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQzthQUN4QjtZQUNELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQzlDLENBQUMsRUFBQyxDQUFDO1FBRUgsYUFBYSxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLFNBQVM7Ozs7UUFBQyxDQUFDLEtBQUssRUFBRSxFQUFFOztrQkFDbEUsS0FBSyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUM7O2tCQUNoQixVQUFVLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUMzQixJQUFJLEtBQUssS0FBSyxjQUFjLENBQUMsU0FBUyxFQUFFO2dCQUN0QyxJQUFJLENBQUMsT0FBTyxHQUFHLFVBQVUsQ0FBQzthQUMzQjtpQkFBTTtnQkFDTCxJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQzthQUN0QjtZQUNELElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUMxQyxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRU0sa0JBQWtCLENBQUMsWUFBMEI7UUFDbEQsSUFBSSxDQUFDLFlBQVksRUFBRTtZQUNqQixNQUFNLEtBQUssQ0FBQyx1R0FBdUcsQ0FBQyxDQUFDO1NBQ3RIO1FBQ0QsSUFBSSxZQUFZLElBQUksWUFBWSxDQUFDLE1BQU0sS0FBSyxLQUFLLEVBQUU7WUFDakQsSUFBSSxDQUFDLGVBQWUsR0FBRyxZQUFZLENBQUM7WUFDcEMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUN2QztJQUNILENBQUM7Ozs7O0lBRU0sZ0JBQWdCLENBQUMsWUFBOEQ7UUFDcEYsSUFBSSxDQUFDLFlBQVksRUFBRTtZQUNqQixNQUFNLEtBQUssQ0FBQyxxR0FBcUcsQ0FBQyxDQUFDO1NBQ3BIO2FBQU07WUFDTCxJQUFJLENBQUMsYUFBYSxHQUFHLFlBQVksQ0FBQztZQUNsQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3BDO0lBQ0gsQ0FBQzs7Ozs7SUFFTSxZQUFZLENBQUMsU0FBMEI7O2NBQ3RDLG1CQUFtQixHQUFHLFNBQVM7YUFDbEMsSUFBSSxDQUNILFVBQVU7Ozs7UUFBQyxDQUFDLEtBQUssRUFBRSxFQUFFO1lBQ25CLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM5QixDQUFDLEVBQUMsRUFDRixRQUFROzs7UUFBQyxHQUFHLEVBQUU7WUFDWixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDeEIsQ0FBQyxFQUFDLENBQ0g7UUFFSCxJQUFJLENBQUMsVUFBVSxHQUFHLG1CQUFtQixDQUFDO0lBQ3hDLENBQUM7Ozs7SUFFTSxjQUFjO1FBQ25CLElBQUksQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQzFDLENBQUM7Ozs7O0lBRU0sUUFBUSxDQUFDLEtBQVk7UUFDMUIsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDdkQsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7UUFDbkIsT0FBTyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDM0IsQ0FBQzs7Ozs7O0lBRVMsUUFBUSxDQUFDLGNBQThCO1FBQy9DLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7SUFDbkQsQ0FBQzs7OztJQUVELFlBQVk7UUFDVixJQUFJLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUMzQyxDQUFDOzs7O0lBRU0sSUFBSTtRQUNULElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxFQUFFLEVBQUU7WUFDdkMsTUFBTSxLQUFLLENBQUMsd0NBQXdDLENBQUMsQ0FBQztTQUN2RDs7WUFDRyxPQUFPLEdBQUcsSUFBSTtRQUNsQixJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsQ0FBQzs7Y0FFbkQsYUFBYSxHQUFHLElBQUksQ0FBQyxhQUFhOztjQUNsQyxVQUFVLEdBQUcsbUJBQUEsYUFBYSxFQUFtQjs7Y0FDN0MsWUFBWSxHQUFHLFVBQVUsQ0FBQyxJQUFJO1FBQ3BDLElBQUksWUFBWSxFQUFFO1lBQ2hCLE9BQU8sR0FBRyxVQUFVLENBQUMsSUFBSSxDQUFDLFVBQVU7Ozs7WUFBQyxLQUFLLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEVBQUMsQ0FBQztpQkFDakUsU0FBUzs7O1lBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLEVBQUMsQ0FBQztTQUM3RTs7Y0FFSyxPQUFPLEdBQUcsbUJBQUEsYUFBYSxFQUFvQjs7Y0FDM0MsU0FBUyxHQUFHLE9BQU8sQ0FBQyxJQUFJO1FBQzlCLElBQUksU0FBUyxFQUFFO1lBQ2IsT0FBTyxHQUFHLE9BQU8sQ0FBQyxJQUFJOzs7WUFDcEIsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDOzs7O1lBQzdELENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxFQUNoQyxDQUFDO1NBQ0g7O2NBRUssVUFBVSxHQUFHLG1CQUFBLGFBQWEsRUFBZTs7Y0FDekMsWUFBWSxHQUFHLFVBQVUsQ0FBQyxJQUFJO1FBQ3BDLElBQUksQ0FBQyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxZQUFZLEVBQUU7WUFDbkQsSUFBSTtnQkFDRixPQUFPLEdBQUcsVUFBVSxFQUFFLENBQUM7Z0JBQ3ZCLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQ3pEO1lBQUMsT0FBTyxLQUFLLEVBQUU7Z0JBQ2QsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDckIsTUFBTSxLQUFLLENBQUM7YUFDYjtTQUNGO1FBRUQsT0FBTyxPQUFPLENBQUM7SUFDakIsQ0FBQzs7OztJQUdNLE1BQU07UUFDWCxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNuQixNQUFNLEtBQUssQ0FBQyx3Q0FBd0MsQ0FBQyxDQUFDO1NBQ3ZEO1FBQ0QsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDNUQsSUFBSSxJQUFJLENBQUMsZUFBZSxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxLQUFLLEtBQUssRUFBRTtZQUNqRSxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUc7OztZQUFDLEdBQUcsRUFBRTtnQkFDNUIsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQzNELElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDekMsQ0FBQyxFQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQ3BDO0lBQ0gsQ0FBQztDQUNGOzs7Ozs7SUFuS0Msb0RBQTBGOztJQUMxRiw2Q0FBb0U7O0lBQ3BFLDRDQUFrRDs7SUFFbEQsbUNBQTJCOztJQUUzQix3Q0FBMEM7Ozs7O0lBRTFDLDJDQUFpRjs7Ozs7SUFDakYsZ0RBQWlFOzs7OztJQUNqRSx5Q0FBK0Q7Ozs7O0lBQy9ELHdDQUE2Qjs7Ozs7SUFFN0IsNkNBQThEOztJQUM5RCxzQ0FBc0Q7O0lBQ3RELHFDQUF1Qjs7Ozs7SUFHdkIsNkNBQStDOzs7OztJQUMvQyxtREFBb0U7Ozs7O0lBQ3BFLDRDQUFxRTs7Ozs7SUFDckUsMkNBQWdDOzs7OztJQUVoQywrQ0FBZ0U7O0lBQ2hFLHdDQUEwRDs7SUFDMUQsdUNBQXlCOztJQUd2Qix5Q0FBMEI7O0lBQzFCLDJDQUE0QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFN1YnNjcmlwdGlvbiwgQmVoYXZpb3JTdWJqZWN0LCBPYnNlcnZhYmxlLCBjb21iaW5lTGF0ZXN0LCB0aHJvd0Vycm9yIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBjYXRjaEVycm9yLCBmaW5hbGl6ZSB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcblxuZXhwb3J0IGVudW0gT3BlcmF0aW9uU3RhdGUge1xuICBJZGxlID0gJ2lkbGUnLFxuICBQcm9jZXNzaW5nID0gJ3Byb2Nlc3NpbmcnLFxuICBQcm9jZXNzZWQgPSAncHJvY2Vzc2VkJyxcbiAgQ2FuY2VsbGluZyA9ICdjYW5jZWxsaW5nJyxcbiAgQ2FuY2VsbGVkID0gJ2NhbmNlbGxlZCcsXG4gIFVuZG9pbmcgPSAndW5kb2luZycsXG4gIFVuZG9uZSA9ICd1bmRvbmUnLFxuICBFcnJvciA9ICdlcnJvcidcbn1cblxuZXhwb3J0IGNsYXNzIE9wZXJhdGlvbnNTbmFja0JhciB7XG5cbiAgcHJpdmF0ZSBvcGVyYXRpb25TdGF0ZVN1YmplY3QkID0gbmV3IEJlaGF2aW9yU3ViamVjdDxPcGVyYXRpb25TdGF0ZT4oT3BlcmF0aW9uU3RhdGUuSWRsZSk7XG4gIHB1YmxpYyBvcGVyYXRpb25TdGF0ZSQgPSB0aGlzLm9wZXJhdGlvblN0YXRlU3ViamVjdCQuYXNPYnNlcnZhYmxlKCk7XG4gIHB1YmxpYyBvcGVyYXRpb25TdGF0ZSA9IE9wZXJhdGlvblN0YXRlLlByb2Nlc3Npbmc7XG5cbiAgcHVibGljIGVycm9yOiBFcnJvciA9IG51bGw7XG5cbiAgcHVibGljIG9wZXJhdGlvbiQ6IE9ic2VydmFibGU8YW55PiA9IG51bGw7XG5cbiAgcHJvdGVjdGVkIHVuZG9PcGVyYXRpb246ICgoKSA9PiBhbnkpIHwgT2JzZXJ2YWJsZTxhbnk+IHwgUHJvbWlzZUxpa2U8YW55PiA9IG51bGw7XG4gIHByaXZhdGUgaXNVbmRvYWJsZVN1YmplY3QkID0gbmV3IEJlaGF2aW9yU3ViamVjdDxib29sZWFuPihmYWxzZSk7XG4gIHByb3RlY3RlZCBpc1VuZG9hYmxlJCA9IHRoaXMuaXNVbmRvYWJsZVN1YmplY3QkLmFzT2JzZXJ2YWJsZSgpO1xuICBwcm90ZWN0ZWQgaXNVbmRvYWJsZSA9IGZhbHNlO1xuXG4gIHByaXZhdGUgY2FuVW5kb1N1YmplY3QkID0gbmV3IEJlaGF2aW9yU3ViamVjdDxib29sZWFuPihmYWxzZSk7XG4gIHB1YmxpYyBjYW5VbmRvJCA9IHRoaXMuY2FuVW5kb1N1YmplY3QkLmFzT2JzZXJ2YWJsZSgpO1xuICBwdWJsaWMgY2FuVW5kbyA9IGZhbHNlO1xuXG5cbiAgcHJvdGVjdGVkIGNhbmNlbE9wZXJhdGlvbjogU3Vic2NyaXB0aW9uID0gbnVsbDtcbiAgcHJpdmF0ZSBpc0NhbmNlbGxhYmxlU3ViamVjdCQgPSBuZXcgQmVoYXZpb3JTdWJqZWN0PGJvb2xlYW4+KGZhbHNlKTtcbiAgcHJvdGVjdGVkIGlzQ2FuY2VsbGFibGUkID0gdGhpcy5pc0NhbmNlbGxhYmxlU3ViamVjdCQuYXNPYnNlcnZhYmxlKCk7XG4gIHByb3RlY3RlZCBpc0NhbmNlbGxhYmxlID0gZmFsc2U7XG5cbiAgcHJpdmF0ZSBjYW5DYW5jZWxTdWJqZWN0JCA9IG5ldyBCZWhhdmlvclN1YmplY3Q8Ym9vbGVhbj4oZmFsc2UpO1xuICBwdWJsaWMgY2FuQ2FuY2VsJCA9IHRoaXMuY2FuQ2FuY2VsU3ViamVjdCQuYXNPYnNlcnZhYmxlKCk7XG4gIHB1YmxpYyBjYW5DYW5jZWwgPSBmYWxzZTtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwdWJsaWMgZGVzY3JpcHRpb246IHN0cmluZyxcbiAgICBwdWJsaWMgYWxsb3dUb0NhbmNlbCA9IGZhbHNlXG4gICkge1xuICAgIC8vIE1hbnRlbSBhdHVhbGl6YWRvIGFzIHZhcmnDoXZlaXMgJ27Do28gb2JzZXJ2w6F2ZWwnXG4gICAgdGhpcy5vcGVyYXRpb25TdGF0ZSQuc3Vic2NyaWJlKChzdGF0ZSkgPT4gdGhpcy5vcGVyYXRpb25TdGF0ZSA9IHN0YXRlKTtcbiAgICB0aGlzLmlzVW5kb2FibGUkLnN1YnNjcmliZShpc1VuZG9hYmxlID0+IHRoaXMuaXNVbmRvYWJsZSA9IGlzVW5kb2FibGUpO1xuICAgIHRoaXMuaXNDYW5jZWxsYWJsZSQuc3Vic2NyaWJlKGlzQ2FuY2VsbGFibGUgPT4gdGhpcy5pc0NhbmNlbGxhYmxlID0gaXNDYW5jZWxsYWJsZSk7XG5cbiAgICBjb21iaW5lTGF0ZXN0KHRoaXMub3BlcmF0aW9uU3RhdGUkLCB0aGlzLmlzQ2FuY2VsbGFibGUkKS5zdWJzY3JpYmUoKHZhbHVlKSA9PiB7XG4gICAgICBjb25zdCBzdGF0ZSA9IHZhbHVlWzBdO1xuICAgICAgY29uc3QgaXNDYW5jZWxsYWJsZSA9IHZhbHVlWzFdO1xuICAgICAgaWYgKHN0YXRlID09PSBPcGVyYXRpb25TdGF0ZS5Qcm9jZXNzaW5nICYmIHRoaXMuYWxsb3dUb0NhbmNlbCkge1xuICAgICAgICB0aGlzLmNhbkNhbmNlbCA9IGlzQ2FuY2VsbGFibGU7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLmNhbkNhbmNlbCA9IGZhbHNlO1xuICAgICAgfVxuICAgICAgdGhpcy5jYW5DYW5jZWxTdWJqZWN0JC5uZXh0KHRoaXMuY2FuQ2FuY2VsKTtcbiAgICB9KTtcblxuICAgIGNvbWJpbmVMYXRlc3QodGhpcy5vcGVyYXRpb25TdGF0ZSQsIHRoaXMuaXNVbmRvYWJsZSQpLnN1YnNjcmliZSgodmFsdWUpID0+IHtcbiAgICAgIGNvbnN0IHN0YXRlID0gdmFsdWVbMF07XG4gICAgICBjb25zdCBpc1VuZG9hYmxlID0gdmFsdWVbMV07XG4gICAgICBpZiAoc3RhdGUgPT09IE9wZXJhdGlvblN0YXRlLlByb2Nlc3NlZCkge1xuICAgICAgICB0aGlzLmNhblVuZG8gPSBpc1VuZG9hYmxlO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5jYW5VbmRvID0gZmFsc2U7XG4gICAgICB9XG4gICAgICB0aGlzLmNhblVuZG9TdWJqZWN0JC5uZXh0KHRoaXMuY2FuVW5kbyk7XG4gICAgfSk7XG4gIH1cblxuICBwdWJsaWMgc2V0Q2FuY2VsT3BlcmF0aW9uKHN1YnNjcmlwdGlvbjogU3Vic2NyaXB0aW9uKSB7XG4gICAgaWYgKCFzdWJzY3JpcHRpb24pIHtcbiAgICAgIHRocm93IEVycm9yKCdOw6NvIMOpIHBvc3PDrXZlbCBzZXRhciB1bWEgb3BlcmHDp8OjbyBkZSBjYW5jZWxhciBjb20gYSBzdWJzY3Jpw6fDo28gbnVsYSAobnVsbCkgb3UgaW5kZWZpbmlkYSAodW5kZWZpbmVkKSEnKTtcbiAgICB9XG4gICAgaWYgKHN1YnNjcmlwdGlvbiAmJiBzdWJzY3JpcHRpb24uY2xvc2VkID09PSBmYWxzZSkge1xuICAgICAgdGhpcy5jYW5jZWxPcGVyYXRpb24gPSBzdWJzY3JpcHRpb247XG4gICAgICB0aGlzLmlzQ2FuY2VsbGFibGVTdWJqZWN0JC5uZXh0KHRydWUpO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyBzZXRVbmRvT3BlcmF0aW9uKHVuZG9GdW5jdGlvbjogKCgpID0+IGFueSkgfCBPYnNlcnZhYmxlPGFueT4gfCBQcm9taXNlTGlrZTxhbnk+KSB7XG4gICAgaWYgKCF1bmRvRnVuY3Rpb24pIHtcbiAgICAgIHRocm93IEVycm9yKCdOw6NvIMOpIHBvc3PDrXZlbCBzZXRhciB1bWEgb3BlcmHDp8OjbyBkZSBkZXNmYXplciBjb20gYSBvcGVyYcOnw6NvIG51bGEgKG51bGwpIG91IGluZGVmaW5pZGEgKHVuZGVmaW5lZCkhJyk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMudW5kb09wZXJhdGlvbiA9IHVuZG9GdW5jdGlvbjtcbiAgICAgIHRoaXMuaXNVbmRvYWJsZVN1YmplY3QkLm5leHQodHJ1ZSk7XG4gICAgfVxuICB9XG5cbiAgcHVibGljIHNldE9wZXJhdGlvbihvcGVyYXRpb246IE9ic2VydmFibGU8YW55Pikge1xuICAgIGNvbnN0IG9wZXJhdGlvbk9ic2VydmF2ZWwgPSBvcGVyYXRpb25cbiAgICAgIC5waXBlKFxuICAgICAgICBjYXRjaEVycm9yKChlcnJvcikgPT4ge1xuICAgICAgICAgIHJldHVybiB0aGlzLnNldEVycm9yKGVycm9yKTtcbiAgICAgICAgfSksXG4gICAgICAgIGZpbmFsaXplKCgpID0+IHtcbiAgICAgICAgICB0aGlzLnNldEFzUHJvY2Vzc2VkKCk7XG4gICAgICAgIH0pLFxuICAgICAgKTtcblxuICAgIHRoaXMub3BlcmF0aW9uJCA9IG9wZXJhdGlvbk9ic2VydmF2ZWw7XG4gIH1cblxuICBwdWJsaWMgc2V0QXNQcm9jZXNzZWQoKTogdm9pZCB7XG4gICAgdGhpcy5zZXRTdGF0ZShPcGVyYXRpb25TdGF0ZS5Qcm9jZXNzZWQpO1xuICB9XG5cbiAgcHVibGljIHNldEVycm9yKGVycm9yOiBFcnJvcikge1xuICAgIHRoaXMub3BlcmF0aW9uU3RhdGVTdWJqZWN0JC5uZXh0KE9wZXJhdGlvblN0YXRlLkVycm9yKTtcbiAgICB0aGlzLmVycm9yID0gZXJyb3I7XG4gICAgcmV0dXJuIHRocm93RXJyb3IoZXJyb3IpO1xuICB9XG5cbiAgcHJvdGVjdGVkIHNldFN0YXRlKG9wZXJhdGlvblN0YXRlOiBPcGVyYXRpb25TdGF0ZSkge1xuICAgIHRoaXMub3BlcmF0aW9uU3RhdGVTdWJqZWN0JC5uZXh0KG9wZXJhdGlvblN0YXRlKTtcbiAgfVxuXG4gIHNldEFzU3RhcnRlZCgpIHtcbiAgICB0aGlzLnNldFN0YXRlKE9wZXJhdGlvblN0YXRlLlByb2Nlc3NpbmcpO1xuICB9XG5cbiAgcHVibGljIHVuZG8oKSB7XG4gICAgaWYgKCF0aGlzLmlzVW5kb2FibGVTdWJqZWN0JC5nZXRWYWx1ZSgpKSB7XG4gICAgICB0aHJvdyBFcnJvcignTsOjbyDDqSBwb3Nzw612ZWwgZGVzZmF6ZXIgZXN0YSBvcGVyYcOnw6NvIScpO1xuICAgIH1cbiAgICBsZXQgcmV0b3JubyA9IG51bGw7XG4gICAgdGhpcy5vcGVyYXRpb25TdGF0ZVN1YmplY3QkLm5leHQoT3BlcmF0aW9uU3RhdGUuVW5kb2luZyk7XG5cbiAgICBjb25zdCB1bmRvT3BlcmF0aW9uID0gdGhpcy51bmRvT3BlcmF0aW9uO1xuICAgIGNvbnN0IG9ic2VydmFibGUgPSB1bmRvT3BlcmF0aW9uIGFzIE9ic2VydmFibGU8YW55PjtcbiAgICBjb25zdCBpc09ic2VydmFibGUgPSBvYnNlcnZhYmxlLnBpcGU7XG4gICAgaWYgKGlzT2JzZXJ2YWJsZSkge1xuICAgICAgcmV0b3JubyA9IG9ic2VydmFibGUucGlwZShjYXRjaEVycm9yKGVycm9yID0+IHRoaXMuc2V0RXJyb3IoZXJyb3IpKSlcbiAgICAgICAgLnN1YnNjcmliZSgoKSA9PiB0aGlzLm9wZXJhdGlvblN0YXRlU3ViamVjdCQubmV4dChPcGVyYXRpb25TdGF0ZS5VbmRvbmUpKTtcbiAgICB9XG5cbiAgICBjb25zdCBwcm9taXNlID0gdW5kb09wZXJhdGlvbiBhcyBQcm9taXNlTGlrZTxhbnk+O1xuICAgIGNvbnN0IGlzUHJvbWlzZSA9IHByb21pc2UudGhlbjtcbiAgICBpZiAoaXNQcm9taXNlKSB7XG4gICAgICByZXRvcm5vID0gcHJvbWlzZS50aGVuKFxuICAgICAgICAoKSA9PiB0aGlzLm9wZXJhdGlvblN0YXRlU3ViamVjdCQubmV4dChPcGVyYXRpb25TdGF0ZS5VbmRvbmUpLFxuICAgICAgICAoZXJyb3IpID0+IHRoaXMuc2V0RXJyb3IoZXJyb3IpXG4gICAgICApO1xuICAgIH1cblxuICAgIGNvbnN0IHVuZG9MYW1iZGEgPSB1bmRvT3BlcmF0aW9uIGFzICgoKSA9PiBhbnkpO1xuICAgIGNvbnN0IGlzVW5kb0xhbWJkYSA9IHVuZG9MYW1iZGEuY2FsbDtcbiAgICBpZiAoKCFpc09ic2VydmFibGUpICYmICghaXNQcm9taXNlKSAmJiBpc1VuZG9MYW1iZGEpIHtcbiAgICAgIHRyeSB7XG4gICAgICAgIHJldG9ybm8gPSB1bmRvTGFtYmRhKCk7XG4gICAgICAgIHRoaXMub3BlcmF0aW9uU3RhdGVTdWJqZWN0JC5uZXh0KE9wZXJhdGlvblN0YXRlLlVuZG9uZSk7XG4gICAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgICB0aGlzLnNldEVycm9yKGVycm9yKTtcbiAgICAgICAgdGhyb3cgZXJyb3I7XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIHJldG9ybm87XG4gIH1cblxuXG4gIHB1YmxpYyBjYW5jZWwoKSB7XG4gICAgaWYgKCF0aGlzLmNhbkNhbmNlbCkge1xuICAgICAgdGhyb3cgRXJyb3IoJ07Do28gw6kgcG9zc8OtdmVsIGNhbmNlbGFyIGVzdGEgb3BlcmHDp8OjbyEnKTtcbiAgICB9XG4gICAgdGhpcy5vcGVyYXRpb25TdGF0ZVN1YmplY3QkLm5leHQoT3BlcmF0aW9uU3RhdGUuQ2FuY2VsbGluZyk7XG4gICAgaWYgKHRoaXMuY2FuY2VsT3BlcmF0aW9uICYmIHRoaXMuY2FuY2VsT3BlcmF0aW9uLmNsb3NlZCA9PT0gZmFsc2UpIHtcbiAgICAgIHRoaXMuY2FuY2VsT3BlcmF0aW9uLmFkZCgoKSA9PiB7XG4gICAgICAgIHRoaXMub3BlcmF0aW9uU3RhdGVTdWJqZWN0JC5uZXh0KE9wZXJhdGlvblN0YXRlLkNhbmNlbGxlZCk7XG4gICAgICAgIHRoaXMuaXNDYW5jZWxsYWJsZVN1YmplY3QkLm5leHQoZmFsc2UpO1xuICAgICAgfSk7XG4gICAgICB0aGlzLmNhbmNlbE9wZXJhdGlvbi51bnN1YnNjcmliZSgpO1xuICAgIH1cbiAgfVxufVxuIl19