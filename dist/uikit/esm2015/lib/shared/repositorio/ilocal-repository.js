/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function ILocalRepository() { }
if (false) {
    /**
     * @template T
     * @param {?} key
     * @param {?} data
     * @return {?}
     */
    ILocalRepository.prototype.salvarItem = function (key, data) { };
    /**
     * @template T
     * @param {?} key
     * @return {?}
     */
    ILocalRepository.prototype.obterItem = function (key) { };
    /**
     * @param {?} key
     * @return {?}
     */
    ILocalRepository.prototype.deletar = function (key) { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaWxvY2FsLXJlcG9zaXRvcnkuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9yZXBvc2l0b3Jpby9pbG9jYWwtcmVwb3NpdG9yeS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUEsc0NBT0M7Ozs7Ozs7O0lBTkMsaUVBQW9DOzs7Ozs7SUFFcEMsMERBQXFCOzs7OztJQUVyQix3REFBd0IiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgaW50ZXJmYWNlIElMb2NhbFJlcG9zaXRvcnkge1xuICBzYWx2YXJJdGVtPFQ+KGtleTogc3RyaW5nLCBkYXRhOiBUKTtcblxuICBvYnRlckl0ZW08VD4oa2V5KTogVDtcblxuICBkZWxldGFyKGtleTogYW55KTogdm9pZDtcblxufVxuIl19