/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { LocalStorageService } from './localstorage.service';
import * as i0 from "@angular/core";
import * as i1 from "./localstorage.service";
export class LocalRepository {
    /**
     * @param {?} localStorageService
     */
    constructor(localStorageService) {
        this.localStorageService = localStorageService;
    }
    /**
     * @template T
     * @param {?} key
     * @param {?} data
     * @return {?}
     */
    salvarItem(key, data) {
        this.localStorageService.set(key, data);
    }
    /**
     * @template T
     * @param {?} key
     * @return {?}
     */
    obterItem(key) {
        return (/** @type {?} */ (this.localStorageService.get(key)));
    }
    /**
     * @param {?} key
     * @return {?}
     */
    deletar(key) {
        this.localStorageService.delete(key);
    }
}
LocalRepository.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
LocalRepository.ctorParameters = () => [
    { type: LocalStorageService }
];
/** @nocollapse */ LocalRepository.ngInjectableDef = i0.defineInjectable({ factory: function LocalRepository_Factory() { return new LocalRepository(i0.inject(i1.LocalStorageService)); }, token: LocalRepository, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    LocalRepository.prototype.localStorageService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9jYWwtcmVwb3NpdG9yeS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL3JlcG9zaXRvcmlvL2xvY2FsLXJlcG9zaXRvcnkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUNBLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFDLG1CQUFtQixFQUFDLE1BQU0sd0JBQXdCLENBQUM7OztBQUszRCxNQUFNLE9BQU8sZUFBZTs7OztJQUUxQixZQUFvQixtQkFBd0M7UUFBeEMsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtJQUM1RCxDQUFDOzs7Ozs7O0lBRUQsVUFBVSxDQUFJLEdBQVcsRUFBRSxJQUFPO1FBQ2hDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQzFDLENBQUM7Ozs7OztJQUVELFNBQVMsQ0FBSSxHQUFRO1FBQ25CLE9BQU8sbUJBQUEsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFBSyxDQUFDO0lBQ2hELENBQUM7Ozs7O0lBRUQsT0FBTyxDQUFDLEdBQVE7UUFDZCxJQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7OztZQWxCRixVQUFVLFNBQUM7Z0JBQ1YsVUFBVSxFQUFFLE1BQU07YUFDbkI7Ozs7WUFKTyxtQkFBbUI7Ozs7Ozs7O0lBT2IsOENBQWdEIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtJTG9jYWxSZXBvc2l0b3J5fSBmcm9tICcuL2lsb2NhbC1yZXBvc2l0b3J5JztcbmltcG9ydCB7SW5qZWN0YWJsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0xvY2FsU3RvcmFnZVNlcnZpY2V9IGZyb20gJy4vbG9jYWxzdG9yYWdlLnNlcnZpY2UnO1xuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBMb2NhbFJlcG9zaXRvcnkgaW1wbGVtZW50cyBJTG9jYWxSZXBvc2l0b3J5IHtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGxvY2FsU3RvcmFnZVNlcnZpY2U6IExvY2FsU3RvcmFnZVNlcnZpY2UpIHtcbiAgfVxuXG4gIHNhbHZhckl0ZW08VD4oa2V5OiBzdHJpbmcsIGRhdGE6IFQpIHtcbiAgICB0aGlzLmxvY2FsU3RvcmFnZVNlcnZpY2Uuc2V0KGtleSwgZGF0YSk7XG4gIH1cblxuICBvYnRlckl0ZW08VD4oa2V5OiBhbnkpOiBUIHtcbiAgICByZXR1cm4gdGhpcy5sb2NhbFN0b3JhZ2VTZXJ2aWNlLmdldChrZXkpIGFzIFQ7XG4gIH1cblxuICBkZWxldGFyKGtleTogYW55KTogdm9pZCB7XG4gICAgdGhpcy5sb2NhbFN0b3JhZ2VTZXJ2aWNlLmRlbGV0ZShrZXkpO1xuICB9XG59XG4iXX0=