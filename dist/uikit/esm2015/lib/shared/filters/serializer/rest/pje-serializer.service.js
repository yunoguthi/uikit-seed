/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { BaseSerializerService } from '../base-serializer.service';
import { Inject, Injectable, InjectionToken, Optional } from '@angular/core';
import { FiltroCompostoOperador, FiltroOperador } from '../../filter';
import * as i0 from "@angular/core";
/** @type {?} */
export const PjeDataSerializerServiceConfigToken = new InjectionToken('PjeDataSerializerServiceConfigToken');
/**
 * @record
 */
export function PjeDataSerializerServiceConfig() { }
if (false) {
    /** @type {?} */
    PjeDataSerializerServiceConfig.prototype.requestCount;
}
export class PjeSerializerService extends BaseSerializerService {
    /**
     * @param {?=} oDataSerializerServiceConfig
     */
    constructor(oDataSerializerServiceConfig) {
        super();
        this.suportaPesquisa = false;
        this.suportaFiltros = true;
        this.suportaPaginacao = true;
        this.suportaOrdenacao = true;
        this.ordenacaoClausulaFormato = `order={ordenacoes}`;
        this.ordenacaoFormato = `{"{campo}":"{direcao}"}`;
        this.ordenacoesSeparador = `, `;
        this.paginacaoPaginaClausulaFormato = `page={"page":{pagina}, `;
        this.paginacaoItensPorPaginaClausulaFormato = `"size":{itensPorPagina}}`;
        this.filtroClausulaFormato = `filter={{filtros}}`;
        this.filtroFormato = `{filtro}`;
        this.pesquisaClausulaFormato = `simpleFilter={pesquisa}`;
        this.pesquisaFiltroCompostoOperadorPadrao = FiltroCompostoOperador.E;
        /** @type {?} */
        const defaultParameter = (/** @type {?} */ ({ requestCount: true }));
        this.dataConfig = Object.assign(defaultParameter, oDataSerializerServiceConfig);
    }
    /**
     * @protected
     * @param {?} paginacao
     * @return {?}
     */
    paginacaoPaginaInterceptor(paginacao) {
        /** @type {?} */
        const paginaZeroBased = paginacao.pagina - 1;
        return paginaZeroBased * paginacao.itensPorPagina;
    }
    /**
     * @protected
     * @param {?} filtroOperador
     * @return {?}
     */
    getFiltroOperadorMapeado(filtroOperador) {
        switch (filtroOperador) {
            case FiltroOperador.Igual: {
                return `"{campo}": {"eq": "{valor}"}`;
            }
            case FiltroOperador.Menor: {
                return `"{campo}": {"lt": "{valor}"}`;
            }
            case FiltroOperador.Maior: {
                return `"{campo}": {"gt": "{valor}"}`;
            }
            case FiltroOperador.MenorOuIgual: {
                return `"{campo}": {"le": "{valor}"}`;
            }
            case FiltroOperador.MaiorOuIgual: {
                return `"{campo}": {"ge": "{valor}"}`;
            }
            case FiltroOperador.Em: {
                return `"{campo}": {"in": ({valor})}`;
            }
            case FiltroOperador.NaoEm: {
                return `"{campo}": {"not in": ({valor})}`;
            }
            case FiltroOperador.ComecaCom: {
                return `"{campo}": {"starts-with": "{valor}"}`;
            }
            case FiltroOperador.TerminaCom: {
                return `"{campo}": {"ends-with": "{valor}"}`;
            }
            case FiltroOperador.Contem: {
                return `"{campo}": {"contains": "{valor}"}`;
            }
            case FiltroOperador.NaoContem: {
                return `"{campo}": {"not-contains": "{valor}"}`;
            }
        }
        throw new Error(`Não foi encontrado um operador equivalente a '${filtroOperador}'!`);
    }
    /**
     * @param {?} parametrosDaRequisicao
     * @return {?}
     */
    serializePaginacao(parametrosDaRequisicao) {
        if (!this.suportaPaginacao) {
            throw new Error('Este serializador não suporta paginacao!');
        }
        /** @type {?} */
        const serializaveis = [];
        /** @type {?} */
        const paginacaoPaginaClausulaFormato = this.paginacaoPaginaClausulaFormato;
        /** @type {?} */
        const paginacaoPaginaParaFormatar = {
            pagina: this.paginacaoPaginaInterceptor(parametrosDaRequisicao.paginacao),
        };
        /** @type {?} */
        const paginacaoPaginaSerializado = this.format(paginacaoPaginaClausulaFormato, paginacaoPaginaParaFormatar);
        serializaveis.push(paginacaoPaginaSerializado);
        /** @type {?} */
        const paginacaoItensPorPaginaClausulaFormato = this.paginacaoItensPorPaginaClausulaFormato;
        /** @type {?} */
        const paginacaoItensPorPaginaParaFormatar = {
            itensPorPagina: this.paginacaoItensPorPaginaInterceptor(parametrosDaRequisicao.paginacao),
        };
        /** @type {?} */
        const paginacaoItensPorPaginaSerializado = this.format(paginacaoItensPorPaginaClausulaFormato, paginacaoItensPorPaginaParaFormatar);
        serializaveis.push(paginacaoItensPorPaginaSerializado);
        return this.tratarArray(serializaveis, '');
    }
    /**
     * @protected
     * @param {?} filtroCompostoOperador
     * @return {?}
     */
    getFiltroCompostoOperadorMapeado(filtroCompostoOperador) {
        if (filtroCompostoOperador === FiltroCompostoOperador.E) {
            return ',';
        }
        else {
            // tslint:disable-next-line:max-line-length
            throw new Error('Rest puro não suporta filtros que utilizam "ou" como lógica! Implemente seu próprio serializador para customizar como gerar a requisição devidamente!');
        }
    }
    /**
     * @protected
     * @param {?} filtro
     * @return {?}
     */
    filtroValorInterceptor(filtro) {
        /** @type {?} */
        const valor = filtro.valor;
        if (valor instanceof Date) {
            return valor.toISOString();
        }
        else if (Array.isArray(valor)) {
            return `'${valor.join('\',\'')}'`;
        }
        else if (typeof valor === 'string') {
            return `'${valor}'`;
        }
        return valor;
    }
}
PjeSerializerService.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */
PjeSerializerService.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [PjeDataSerializerServiceConfigToken,] }, { type: Optional }] }
];
/** @nocollapse */ PjeSerializerService.ngInjectableDef = i0.defineInjectable({ factory: function PjeSerializerService_Factory() { return new PjeSerializerService(i0.inject(PjeDataSerializerServiceConfigToken, 8)); }, token: PjeSerializerService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @protected
     */
    PjeSerializerService.prototype.suportaPesquisa;
    /**
     * @type {?}
     * @protected
     */
    PjeSerializerService.prototype.suportaFiltros;
    /**
     * @type {?}
     * @protected
     */
    PjeSerializerService.prototype.suportaPaginacao;
    /**
     * @type {?}
     * @protected
     */
    PjeSerializerService.prototype.suportaOrdenacao;
    /**
     * @type {?}
     * @protected
     */
    PjeSerializerService.prototype.ordenacaoClausulaFormato;
    /**
     * @type {?}
     * @protected
     */
    PjeSerializerService.prototype.ordenacaoFormato;
    /**
     * @type {?}
     * @protected
     */
    PjeSerializerService.prototype.ordenacoesSeparador;
    /**
     * @type {?}
     * @protected
     */
    PjeSerializerService.prototype.paginacaoPaginaClausulaFormato;
    /**
     * @type {?}
     * @protected
     */
    PjeSerializerService.prototype.paginacaoItensPorPaginaClausulaFormato;
    /**
     * @type {?}
     * @protected
     */
    PjeSerializerService.prototype.filtroClausulaFormato;
    /**
     * @type {?}
     * @protected
     */
    PjeSerializerService.prototype.filtroFormato;
    /**
     * @type {?}
     * @protected
     */
    PjeSerializerService.prototype.pesquisaClausulaFormato;
    /**
     * @type {?}
     * @protected
     */
    PjeSerializerService.prototype.pesquisaFiltroCompostoOperadorPadrao;
    /** @type {?} */
    PjeSerializerService.prototype.dataConfig;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGplLXNlcmlhbGl6ZXIuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2ZpbHRlcnMvc2VyaWFsaXplci9yZXN0L3BqZS1zZXJpYWxpemVyLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxxQkFBcUIsRUFBQyxNQUFNLDRCQUE0QixDQUFDO0FBQ2pFLE9BQU8sRUFBQyxNQUFNLEVBQUUsVUFBVSxFQUFFLGNBQWMsRUFBRSxRQUFRLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDM0UsT0FBTyxFQUFTLHNCQUFzQixFQUFFLGNBQWMsRUFBQyxNQUFNLGNBQWMsQ0FBQzs7O0FBSTVFLE1BQU0sT0FBTyxtQ0FBbUMsR0FDOUMsSUFBSSxjQUFjLENBQWlDLHFDQUFxQyxDQUFDOzs7O0FBRTNGLG9EQUVDOzs7SUFEQyxzREFBc0I7O0FBSXhCLE1BQU0sT0FBTyxvQkFBcUIsU0FBUSxxQkFBcUI7Ozs7SUFpQjdELFlBQXFFLDRCQUE2RDtRQUNoSSxLQUFLLEVBQUUsQ0FBQztRQWpCQSxvQkFBZSxHQUFHLEtBQUssQ0FBQztRQUN4QixtQkFBYyxHQUFHLElBQUksQ0FBQztRQUN0QixxQkFBZ0IsR0FBRyxJQUFJLENBQUM7UUFDeEIscUJBQWdCLEdBQUcsSUFBSSxDQUFDO1FBQ3hCLDZCQUF3QixHQUFHLG9CQUFvQixDQUFDO1FBQ2hELHFCQUFnQixHQUFHLHlCQUF5QixDQUFDO1FBQzdDLHdCQUFtQixHQUFHLElBQUksQ0FBQztRQUMzQixtQ0FBOEIsR0FBRyx5QkFBeUIsQ0FBQztRQUMzRCwyQ0FBc0MsR0FBRywwQkFBMEIsQ0FBQztRQUNwRSwwQkFBcUIsR0FBRyxvQkFBb0IsQ0FBQztRQUM3QyxrQkFBYSxHQUFHLFVBQVUsQ0FBQztRQUMzQiw0QkFBdUIsR0FBRyx5QkFBeUIsQ0FBQztRQUNwRCx5Q0FBb0MsR0FBRyxzQkFBc0IsQ0FBQyxDQUFDLENBQUM7O2NBTWxFLGdCQUFnQixHQUFHLG1CQUFBLEVBQUMsWUFBWSxFQUFFLElBQUksRUFBQyxFQUFrQztRQUMvRSxJQUFJLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLEVBQUUsNEJBQTRCLENBQUMsQ0FBQztJQUNsRixDQUFDOzs7Ozs7SUFFUywwQkFBMEIsQ0FBQyxTQUFvQjs7Y0FDakQsZUFBZSxHQUFHLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQztRQUM1QyxPQUFPLGVBQWUsR0FBRyxTQUFTLENBQUMsY0FBYyxDQUFDO0lBQ3BELENBQUM7Ozs7OztJQUVTLHdCQUF3QixDQUFDLGNBQThCO1FBQy9ELFFBQVEsY0FBYyxFQUFFO1lBQ3RCLEtBQUssY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUN6QixPQUFPLDhCQUE4QixDQUFDO2FBQ3ZDO1lBQ0QsS0FBSyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3pCLE9BQU8sOEJBQThCLENBQUM7YUFDdkM7WUFDRCxLQUFLLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDekIsT0FBTyw4QkFBOEIsQ0FBQzthQUN2QztZQUNELEtBQUssY0FBYyxDQUFDLFlBQVksQ0FBQyxDQUFDO2dCQUNoQyxPQUFPLDhCQUE4QixDQUFDO2FBQ3ZDO1lBQ0QsS0FBSyxjQUFjLENBQUMsWUFBWSxDQUFDLENBQUM7Z0JBQ2hDLE9BQU8sOEJBQThCLENBQUM7YUFDdkM7WUFDRCxLQUFLLGNBQWMsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDdEIsT0FBTyw4QkFBOEIsQ0FBQzthQUN2QztZQUNELEtBQUssY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUN6QixPQUFPLGtDQUFrQyxDQUFDO2FBQzNDO1lBQ0QsS0FBSyxjQUFjLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQzdCLE9BQU8sdUNBQXVDLENBQUM7YUFDaEQ7WUFDRCxLQUFLLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDOUIsT0FBTyxxQ0FBcUMsQ0FBQzthQUM5QztZQUNELEtBQUssY0FBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUMxQixPQUFPLG9DQUFvQyxDQUFDO2FBQzdDO1lBQ0QsS0FBSyxjQUFjLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQzdCLE9BQU8sd0NBQXdDLENBQUM7YUFDakQ7U0FDRjtRQUNELE1BQU0sSUFBSSxLQUFLLENBQUMsaURBQWlELGNBQWMsSUFBSSxDQUFDLENBQUM7SUFDdkYsQ0FBQzs7Ozs7SUFFRCxrQkFBa0IsQ0FBQyxzQkFBOEM7UUFDL0QsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtZQUMxQixNQUFNLElBQUksS0FBSyxDQUFDLDBDQUEwQyxDQUFDLENBQUM7U0FDN0Q7O2NBRUssYUFBYSxHQUFhLEVBQUU7O2NBRTVCLDhCQUE4QixHQUFHLElBQUksQ0FBQyw4QkFBOEI7O2NBQ3BFLDJCQUEyQixHQUFHO1lBQ2xDLE1BQU0sRUFBRSxJQUFJLENBQUMsMEJBQTBCLENBQUMsc0JBQXNCLENBQUMsU0FBUyxDQUFDO1NBQzFFOztjQUNLLDBCQUEwQixHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsOEJBQThCLEVBQUUsMkJBQTJCLENBQUM7UUFDM0csYUFBYSxDQUFDLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxDQUFDOztjQUV6QyxzQ0FBc0MsR0FBRyxJQUFJLENBQUMsc0NBQXNDOztjQUNwRixtQ0FBbUMsR0FBRztZQUMxQyxjQUFjLEVBQUUsSUFBSSxDQUFDLGtDQUFrQyxDQUFDLHNCQUFzQixDQUFDLFNBQVMsQ0FBQztTQUMxRjs7Y0FDSyxrQ0FBa0MsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLHNDQUFzQyxFQUFFLG1DQUFtQyxDQUFDO1FBQ25JLGFBQWEsQ0FBQyxJQUFJLENBQUMsa0NBQWtDLENBQUMsQ0FBQztRQUV2RCxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBQzdDLENBQUM7Ozs7OztJQUVTLGdDQUFnQyxDQUFDLHNCQUE4QztRQUN2RixJQUFJLHNCQUFzQixLQUFLLHNCQUFzQixDQUFDLENBQUMsRUFBRTtZQUN2RCxPQUFPLEdBQUcsQ0FBQztTQUNaO2FBQU07WUFDTCwyQ0FBMkM7WUFDM0MsTUFBTSxJQUFJLEtBQUssQ0FBQyx1SkFBdUosQ0FBQyxDQUFDO1NBQzFLO0lBQ0gsQ0FBQzs7Ozs7O0lBRVMsc0JBQXNCLENBQUMsTUFBYzs7Y0FDdkMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxLQUFLO1FBQzFCLElBQUksS0FBSyxZQUFZLElBQUksRUFBRTtZQUN6QixPQUFPLEtBQUssQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUM1QjthQUFNLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUMvQixPQUFPLElBQUksS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDO1NBQ25DO2FBQU0sSUFBSSxPQUFPLEtBQUssS0FBSyxRQUFRLEVBQUU7WUFDcEMsT0FBTyxJQUFJLEtBQUssR0FBRyxDQUFDO1NBQ3JCO1FBQ0QsT0FBTyxLQUFLLENBQUM7SUFDZixDQUFDOzs7WUEvR0YsVUFBVSxTQUFDLEVBQUMsVUFBVSxFQUFFLE1BQU0sRUFBQzs7Ozs0Q0FrQmpCLE1BQU0sU0FBQyxtQ0FBbUMsY0FBRyxRQUFROzs7Ozs7OztJQWhCbEUsK0NBQWtDOzs7OztJQUNsQyw4Q0FBZ0M7Ozs7O0lBQ2hDLGdEQUFrQzs7Ozs7SUFDbEMsZ0RBQWtDOzs7OztJQUNsQyx3REFBMEQ7Ozs7O0lBQzFELGdEQUF1RDs7Ozs7SUFDdkQsbURBQXFDOzs7OztJQUNyQyw4REFBcUU7Ozs7O0lBQ3JFLHNFQUE4RTs7Ozs7SUFDOUUscURBQXVEOzs7OztJQUN2RCw2Q0FBcUM7Ozs7O0lBQ3JDLHVEQUE4RDs7Ozs7SUFDOUQsb0VBQTBFOztJQUUxRSwwQ0FBa0QiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0Jhc2VTZXJpYWxpemVyU2VydmljZX0gZnJvbSAnLi4vYmFzZS1zZXJpYWxpemVyLnNlcnZpY2UnO1xuaW1wb3J0IHtJbmplY3QsIEluamVjdGFibGUsIEluamVjdGlvblRva2VuLCBPcHRpb25hbH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0ZpbHRybywgRmlsdHJvQ29tcG9zdG9PcGVyYWRvciwgRmlsdHJvT3BlcmFkb3J9IGZyb20gJy4uLy4uL2ZpbHRlcic7XG5pbXBvcnQge1BhcmFtZXRyb3NEYVJlcXVpc2ljYW99IGZyb20gJy4uLy4uL3JlcXVlc3QtcGFyYW1ldGVycyc7XG5pbXBvcnQge1BhZ2luYWNhb30gZnJvbSAnLi4vLi4vcGFnZSc7XG5cbmV4cG9ydCBjb25zdCBQamVEYXRhU2VyaWFsaXplclNlcnZpY2VDb25maWdUb2tlbiA9XG4gIG5ldyBJbmplY3Rpb25Ub2tlbjxQamVEYXRhU2VyaWFsaXplclNlcnZpY2VDb25maWc+KCdQamVEYXRhU2VyaWFsaXplclNlcnZpY2VDb25maWdUb2tlbicpO1xuXG5leHBvcnQgaW50ZXJmYWNlIFBqZURhdGFTZXJpYWxpemVyU2VydmljZUNvbmZpZyB7XG4gIHJlcXVlc3RDb3VudDogYm9vbGVhbjtcbn1cblxuQEluamVjdGFibGUoe3Byb3ZpZGVkSW46ICdyb290J30pXG5leHBvcnQgY2xhc3MgUGplU2VyaWFsaXplclNlcnZpY2UgZXh0ZW5kcyBCYXNlU2VyaWFsaXplclNlcnZpY2Uge1xuICBwcm90ZWN0ZWQgc3Vwb3J0YVBlc3F1aXNhID0gZmFsc2U7XG4gIHByb3RlY3RlZCBzdXBvcnRhRmlsdHJvcyA9IHRydWU7XG4gIHByb3RlY3RlZCBzdXBvcnRhUGFnaW5hY2FvID0gdHJ1ZTtcbiAgcHJvdGVjdGVkIHN1cG9ydGFPcmRlbmFjYW8gPSB0cnVlO1xuICBwcm90ZWN0ZWQgb3JkZW5hY2FvQ2xhdXN1bGFGb3JtYXRvID0gYG9yZGVyPXtvcmRlbmFjb2VzfWA7XG4gIHByb3RlY3RlZCBvcmRlbmFjYW9Gb3JtYXRvID0gYHtcIntjYW1wb31cIjpcIntkaXJlY2FvfVwifWA7XG4gIHByb3RlY3RlZCBvcmRlbmFjb2VzU2VwYXJhZG9yID0gYCwgYDtcbiAgcHJvdGVjdGVkIHBhZ2luYWNhb1BhZ2luYUNsYXVzdWxhRm9ybWF0byA9IGBwYWdlPXtcInBhZ2VcIjp7cGFnaW5hfSwgYDtcbiAgcHJvdGVjdGVkIHBhZ2luYWNhb0l0ZW5zUG9yUGFnaW5hQ2xhdXN1bGFGb3JtYXRvID0gYFwic2l6ZVwiOntpdGVuc1BvclBhZ2luYX19YDtcbiAgcHJvdGVjdGVkIGZpbHRyb0NsYXVzdWxhRm9ybWF0byA9IGBmaWx0ZXI9e3tmaWx0cm9zfX1gO1xuICBwcm90ZWN0ZWQgZmlsdHJvRm9ybWF0byA9IGB7ZmlsdHJvfWA7XG4gIHByb3RlY3RlZCBwZXNxdWlzYUNsYXVzdWxhRm9ybWF0byA9IGBzaW1wbGVGaWx0ZXI9e3Blc3F1aXNhfWA7XG4gIHByb3RlY3RlZCBwZXNxdWlzYUZpbHRyb0NvbXBvc3RvT3BlcmFkb3JQYWRyYW8gPSBGaWx0cm9Db21wb3N0b09wZXJhZG9yLkU7XG5cbiAgcHVibGljIGRhdGFDb25maWc6IFBqZURhdGFTZXJpYWxpemVyU2VydmljZUNvbmZpZztcblxuICBjb25zdHJ1Y3RvcihASW5qZWN0KFBqZURhdGFTZXJpYWxpemVyU2VydmljZUNvbmZpZ1Rva2VuKSBAT3B0aW9uYWwoKSBvRGF0YVNlcmlhbGl6ZXJTZXJ2aWNlQ29uZmlnPzogUGplRGF0YVNlcmlhbGl6ZXJTZXJ2aWNlQ29uZmlnKSB7XG4gICAgc3VwZXIoKTtcbiAgICBjb25zdCBkZWZhdWx0UGFyYW1ldGVyID0ge3JlcXVlc3RDb3VudDogdHJ1ZX0gYXMgUGplRGF0YVNlcmlhbGl6ZXJTZXJ2aWNlQ29uZmlnO1xuICAgIHRoaXMuZGF0YUNvbmZpZyA9IE9iamVjdC5hc3NpZ24oZGVmYXVsdFBhcmFtZXRlciwgb0RhdGFTZXJpYWxpemVyU2VydmljZUNvbmZpZyk7XG4gIH1cblxuICBwcm90ZWN0ZWQgcGFnaW5hY2FvUGFnaW5hSW50ZXJjZXB0b3IocGFnaW5hY2FvOiBQYWdpbmFjYW8pIHtcbiAgICBjb25zdCBwYWdpbmFaZXJvQmFzZWQgPSBwYWdpbmFjYW8ucGFnaW5hIC0gMTtcbiAgICByZXR1cm4gcGFnaW5hWmVyb0Jhc2VkICogcGFnaW5hY2FvLml0ZW5zUG9yUGFnaW5hO1xuICB9XG5cbiAgcHJvdGVjdGVkIGdldEZpbHRyb09wZXJhZG9yTWFwZWFkbyhmaWx0cm9PcGVyYWRvcjogRmlsdHJvT3BlcmFkb3IpOiBzdHJpbmcge1xuICAgIHN3aXRjaCAoZmlsdHJvT3BlcmFkb3IpIHtcbiAgICAgIGNhc2UgRmlsdHJvT3BlcmFkb3IuSWd1YWw6IHtcbiAgICAgICAgcmV0dXJuIGBcIntjYW1wb31cIjoge1wiZXFcIjogXCJ7dmFsb3J9XCJ9YDtcbiAgICAgIH1cbiAgICAgIGNhc2UgRmlsdHJvT3BlcmFkb3IuTWVub3I6IHtcbiAgICAgICAgcmV0dXJuIGBcIntjYW1wb31cIjoge1wibHRcIjogXCJ7dmFsb3J9XCJ9YDtcbiAgICAgIH1cbiAgICAgIGNhc2UgRmlsdHJvT3BlcmFkb3IuTWFpb3I6IHtcbiAgICAgICAgcmV0dXJuIGBcIntjYW1wb31cIjoge1wiZ3RcIjogXCJ7dmFsb3J9XCJ9YDtcbiAgICAgIH1cbiAgICAgIGNhc2UgRmlsdHJvT3BlcmFkb3IuTWVub3JPdUlndWFsOiB7XG4gICAgICAgIHJldHVybiBgXCJ7Y2FtcG99XCI6IHtcImxlXCI6IFwie3ZhbG9yfVwifWA7XG4gICAgICB9XG4gICAgICBjYXNlIEZpbHRyb09wZXJhZG9yLk1haW9yT3VJZ3VhbDoge1xuICAgICAgICByZXR1cm4gYFwie2NhbXBvfVwiOiB7XCJnZVwiOiBcInt2YWxvcn1cIn1gO1xuICAgICAgfVxuICAgICAgY2FzZSBGaWx0cm9PcGVyYWRvci5FbToge1xuICAgICAgICByZXR1cm4gYFwie2NhbXBvfVwiOiB7XCJpblwiOiAoe3ZhbG9yfSl9YDtcbiAgICAgIH1cbiAgICAgIGNhc2UgRmlsdHJvT3BlcmFkb3IuTmFvRW06IHtcbiAgICAgICAgcmV0dXJuIGBcIntjYW1wb31cIjoge1wibm90IGluXCI6ICh7dmFsb3J9KX1gO1xuICAgICAgfVxuICAgICAgY2FzZSBGaWx0cm9PcGVyYWRvci5Db21lY2FDb206IHtcbiAgICAgICAgcmV0dXJuIGBcIntjYW1wb31cIjoge1wic3RhcnRzLXdpdGhcIjogXCJ7dmFsb3J9XCJ9YDtcbiAgICAgIH1cbiAgICAgIGNhc2UgRmlsdHJvT3BlcmFkb3IuVGVybWluYUNvbToge1xuICAgICAgICByZXR1cm4gYFwie2NhbXBvfVwiOiB7XCJlbmRzLXdpdGhcIjogXCJ7dmFsb3J9XCJ9YDtcbiAgICAgIH1cbiAgICAgIGNhc2UgRmlsdHJvT3BlcmFkb3IuQ29udGVtOiB7XG4gICAgICAgIHJldHVybiBgXCJ7Y2FtcG99XCI6IHtcImNvbnRhaW5zXCI6IFwie3ZhbG9yfVwifWA7XG4gICAgICB9XG4gICAgICBjYXNlIEZpbHRyb09wZXJhZG9yLk5hb0NvbnRlbToge1xuICAgICAgICByZXR1cm4gYFwie2NhbXBvfVwiOiB7XCJub3QtY29udGFpbnNcIjogXCJ7dmFsb3J9XCJ9YDtcbiAgICAgIH1cbiAgICB9XG4gICAgdGhyb3cgbmV3IEVycm9yKGBOw6NvIGZvaSBlbmNvbnRyYWRvIHVtIG9wZXJhZG9yIGVxdWl2YWxlbnRlIGEgJyR7ZmlsdHJvT3BlcmFkb3J9JyFgKTtcbiAgfVxuXG4gIHNlcmlhbGl6ZVBhZ2luYWNhbyhwYXJhbWV0cm9zRGFSZXF1aXNpY2FvOiBQYXJhbWV0cm9zRGFSZXF1aXNpY2FvKTogc3RyaW5nIHtcbiAgICBpZiAoIXRoaXMuc3Vwb3J0YVBhZ2luYWNhbykge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKCdFc3RlIHNlcmlhbGl6YWRvciBuw6NvIHN1cG9ydGEgcGFnaW5hY2FvIScpO1xuICAgIH1cblxuICAgIGNvbnN0IHNlcmlhbGl6YXZlaXM6IHN0cmluZ1tdID0gW107XG5cbiAgICBjb25zdCBwYWdpbmFjYW9QYWdpbmFDbGF1c3VsYUZvcm1hdG8gPSB0aGlzLnBhZ2luYWNhb1BhZ2luYUNsYXVzdWxhRm9ybWF0bztcbiAgICBjb25zdCBwYWdpbmFjYW9QYWdpbmFQYXJhRm9ybWF0YXIgPSB7XG4gICAgICBwYWdpbmE6IHRoaXMucGFnaW5hY2FvUGFnaW5hSW50ZXJjZXB0b3IocGFyYW1ldHJvc0RhUmVxdWlzaWNhby5wYWdpbmFjYW8pLFxuICAgIH07XG4gICAgY29uc3QgcGFnaW5hY2FvUGFnaW5hU2VyaWFsaXphZG8gPSB0aGlzLmZvcm1hdChwYWdpbmFjYW9QYWdpbmFDbGF1c3VsYUZvcm1hdG8sIHBhZ2luYWNhb1BhZ2luYVBhcmFGb3JtYXRhcik7XG4gICAgc2VyaWFsaXphdmVpcy5wdXNoKHBhZ2luYWNhb1BhZ2luYVNlcmlhbGl6YWRvKTtcblxuICAgIGNvbnN0IHBhZ2luYWNhb0l0ZW5zUG9yUGFnaW5hQ2xhdXN1bGFGb3JtYXRvID0gdGhpcy5wYWdpbmFjYW9JdGVuc1BvclBhZ2luYUNsYXVzdWxhRm9ybWF0bztcbiAgICBjb25zdCBwYWdpbmFjYW9JdGVuc1BvclBhZ2luYVBhcmFGb3JtYXRhciA9IHtcbiAgICAgIGl0ZW5zUG9yUGFnaW5hOiB0aGlzLnBhZ2luYWNhb0l0ZW5zUG9yUGFnaW5hSW50ZXJjZXB0b3IocGFyYW1ldHJvc0RhUmVxdWlzaWNhby5wYWdpbmFjYW8pLFxuICAgIH07XG4gICAgY29uc3QgcGFnaW5hY2FvSXRlbnNQb3JQYWdpbmFTZXJpYWxpemFkbyA9IHRoaXMuZm9ybWF0KHBhZ2luYWNhb0l0ZW5zUG9yUGFnaW5hQ2xhdXN1bGFGb3JtYXRvLCBwYWdpbmFjYW9JdGVuc1BvclBhZ2luYVBhcmFGb3JtYXRhcik7XG4gICAgc2VyaWFsaXphdmVpcy5wdXNoKHBhZ2luYWNhb0l0ZW5zUG9yUGFnaW5hU2VyaWFsaXphZG8pO1xuXG4gICAgcmV0dXJuIHRoaXMudHJhdGFyQXJyYXkoc2VyaWFsaXphdmVpcywgJycpO1xuICB9XG5cbiAgcHJvdGVjdGVkIGdldEZpbHRyb0NvbXBvc3RvT3BlcmFkb3JNYXBlYWRvKGZpbHRyb0NvbXBvc3RvT3BlcmFkb3I6IEZpbHRyb0NvbXBvc3RvT3BlcmFkb3IpOiBzdHJpbmcge1xuICAgIGlmIChmaWx0cm9Db21wb3N0b09wZXJhZG9yID09PSBGaWx0cm9Db21wb3N0b09wZXJhZG9yLkUpIHtcbiAgICAgIHJldHVybiAnLCc7XG4gICAgfSBlbHNlIHtcbiAgICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTptYXgtbGluZS1sZW5ndGhcbiAgICAgIHRocm93IG5ldyBFcnJvcignUmVzdCBwdXJvIG7Do28gc3Vwb3J0YSBmaWx0cm9zIHF1ZSB1dGlsaXphbSBcIm91XCIgY29tbyBsw7NnaWNhISBJbXBsZW1lbnRlIHNldSBwcsOzcHJpbyBzZXJpYWxpemFkb3IgcGFyYSBjdXN0b21pemFyIGNvbW8gZ2VyYXIgYSByZXF1aXNpw6fDo28gZGV2aWRhbWVudGUhJyk7XG4gICAgfVxuICB9XG5cbiAgcHJvdGVjdGVkIGZpbHRyb1ZhbG9ySW50ZXJjZXB0b3IoZmlsdHJvOiBGaWx0cm8pOiBzdHJpbmcge1xuICAgIGNvbnN0IHZhbG9yID0gZmlsdHJvLnZhbG9yO1xuICAgIGlmICh2YWxvciBpbnN0YW5jZW9mIERhdGUpIHtcbiAgICAgIHJldHVybiB2YWxvci50b0lTT1N0cmluZygpO1xuICAgIH0gZWxzZSBpZiAoQXJyYXkuaXNBcnJheSh2YWxvcikpIHtcbiAgICAgIHJldHVybiBgJyR7dmFsb3Iuam9pbignXFwnLFxcJycpfSdgO1xuICAgIH0gZWxzZSBpZiAodHlwZW9mIHZhbG9yID09PSAnc3RyaW5nJykge1xuICAgICAgcmV0dXJuIGAnJHt2YWxvcn0nYDtcbiAgICB9XG4gICAgcmV0dXJuIHZhbG9yO1xuICB9XG59XG4iXX0=