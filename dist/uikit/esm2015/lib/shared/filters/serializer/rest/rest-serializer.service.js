/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { BaseSerializerService } from '../base-serializer.service';
import { Injectable } from '@angular/core';
import { FiltroCompostoOperador, FiltroOperador, } from '../../filter';
import * as i0 from "@angular/core";
export class RestSerializerService extends BaseSerializerService {
    constructor() {
        super(...arguments);
        this.pesquisaFiltroOperadorPadrao = FiltroOperador.Contem;
        this.suportaPesquisa = false;
        this.suportaFiltros = true;
        this.suportaPaginacao = false;
        this.suportaOrdenacao = false;
        this.quantidadeDeFiltrosAlinhadosSuportados = 1;
        this.filtroClausulaFormato = '{filtros}';
        this.filtroFormato = '{filtro}';
    }
    /**
     * @protected
     * @param {?} filtroOperador
     * @return {?}
     */
    getFiltroOperadorMapeado(filtroOperador) {
        if (filtroOperador === FiltroOperador.Igual) {
            return '{campo}={valor}';
        }
        else {
            // tslint:disable-next-line:max-line-length
            throw new Error('Rest puro não suporta filtro que não seja o igual! Implemente seu próprio serializador para customizar como gerar a requisição devidamente!');
        }
    }
    /**
     * @protected
     * @param {?} filtroCompostoOperador
     * @return {?}
     */
    getFiltroCompostoOperadorMapeado(filtroCompostoOperador) {
        if (filtroCompostoOperador === FiltroCompostoOperador.E) {
            return '&';
        }
        else {
            // tslint:disable-next-line:max-line-length
            throw new Error('Rest puro não suporta filtros que utilizam "ou" como lógica! Implemente seu próprio serializador para customizar como gerar a requisição devidamente!');
        }
    }
}
RestSerializerService.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */ RestSerializerService.ngInjectableDef = i0.defineInjectable({ factory: function RestSerializerService_Factory() { return new RestSerializerService(); }, token: RestSerializerService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @protected
     */
    RestSerializerService.prototype.pesquisaFiltroOperadorPadrao;
    /**
     * @type {?}
     * @protected
     */
    RestSerializerService.prototype.suportaPesquisa;
    /**
     * @type {?}
     * @protected
     */
    RestSerializerService.prototype.suportaFiltros;
    /**
     * @type {?}
     * @protected
     */
    RestSerializerService.prototype.suportaPaginacao;
    /**
     * @type {?}
     * @protected
     */
    RestSerializerService.prototype.suportaOrdenacao;
    /**
     * @type {?}
     * @protected
     */
    RestSerializerService.prototype.quantidadeDeFiltrosAlinhadosSuportados;
    /**
     * @type {?}
     * @protected
     */
    RestSerializerService.prototype.filtroClausulaFormato;
    /**
     * @type {?}
     * @protected
     */
    RestSerializerService.prototype.filtroFormato;
    /**
     * @type {?}
     * @protected
     */
    RestSerializerService.prototype.paginacaoPaginaClausulaFormato;
    /**
     * @type {?}
     * @protected
     */
    RestSerializerService.prototype.paginacaoItensPorPaginaClausulaFormato;
    /**
     * @type {?}
     * @protected
     */
    RestSerializerService.prototype.ordenacaoClausulaFormato;
    /**
     * @type {?}
     * @protected
     */
    RestSerializerService.prototype.ordenacaoFormato;
    /**
     * @type {?}
     * @protected
     */
    RestSerializerService.prototype.ordenacoesSeparador;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVzdC1zZXJpYWxpemVyLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9maWx0ZXJzL3NlcmlhbGl6ZXIvcmVzdC9yZXN0LXNlcmlhbGl6ZXIuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLHFCQUFxQixFQUFDLE1BQU0sNEJBQTRCLENBQUM7QUFDakUsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQ0wsc0JBQXNCLEVBQ3RCLGNBQWMsR0FDZixNQUFNLGNBQWMsQ0FBQzs7QUFHdEIsTUFBTSxPQUFPLHFCQUFzQixTQUFRLHFCQUFxQjtJQURoRTs7UUFFWSxpQ0FBNEIsR0FBbUIsY0FBYyxDQUFDLE1BQU0sQ0FBQztRQUNyRSxvQkFBZSxHQUFHLEtBQUssQ0FBQztRQUN4QixtQkFBYyxHQUFHLElBQUksQ0FBQztRQUN0QixxQkFBZ0IsR0FBRyxLQUFLLENBQUM7UUFDekIscUJBQWdCLEdBQUcsS0FBSyxDQUFDO1FBb0J6QiwyQ0FBc0MsR0FBRyxDQUFDLENBQUM7UUFDM0MsMEJBQXFCLEdBQVcsV0FBVyxDQUFDO1FBQzVDLGtCQUFhLEdBQVcsVUFBVSxDQUFDO0tBUTlDOzs7Ozs7SUE1Qlcsd0JBQXdCLENBQUMsY0FBOEI7UUFDL0QsSUFBSSxjQUFjLEtBQUssY0FBYyxDQUFDLEtBQUssRUFBRTtZQUMzQyxPQUFPLGlCQUFpQixDQUFDO1NBQzFCO2FBQU07WUFDTCwyQ0FBMkM7WUFDM0MsTUFBTSxJQUFJLEtBQUssQ0FBQyw2SUFBNkksQ0FBQyxDQUFDO1NBQ2hLO0lBQ0gsQ0FBQzs7Ozs7O0lBRVMsZ0NBQWdDLENBQUMsc0JBQThDO1FBQ3ZGLElBQUksc0JBQXNCLEtBQUssc0JBQXNCLENBQUMsQ0FBQyxFQUFFO1lBQ3ZELE9BQU8sR0FBRyxDQUFDO1NBQ1o7YUFBTTtZQUNMLDJDQUEyQztZQUMzQyxNQUFNLElBQUksS0FBSyxDQUFDLHVKQUF1SixDQUFDLENBQUM7U0FDMUs7SUFDSCxDQUFDOzs7WUF4QkYsVUFBVSxTQUFDLEVBQUMsVUFBVSxFQUFFLE1BQU0sRUFBQzs7Ozs7Ozs7SUFFOUIsNkRBQStFOzs7OztJQUMvRSxnREFBa0M7Ozs7O0lBQ2xDLCtDQUFnQzs7Ozs7SUFDaEMsaURBQW1DOzs7OztJQUNuQyxpREFBbUM7Ozs7O0lBb0JuQyx1RUFBcUQ7Ozs7O0lBQ3JELHNEQUFzRDs7Ozs7SUFDdEQsOENBQTZDOzs7OztJQUU3QywrREFBaUQ7Ozs7O0lBQ2pELHVFQUF5RDs7Ozs7SUFFekQseURBQTJDOzs7OztJQUMzQyxpREFBbUM7Ozs7O0lBQ25DLG9EQUFzQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7QmFzZVNlcmlhbGl6ZXJTZXJ2aWNlfSBmcm9tICcuLi9iYXNlLXNlcmlhbGl6ZXIuc2VydmljZSc7XG5pbXBvcnQge0luamVjdGFibGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtcbiAgRmlsdHJvQ29tcG9zdG9PcGVyYWRvcixcbiAgRmlsdHJvT3BlcmFkb3IsXG59IGZyb20gJy4uLy4uL2ZpbHRlcic7XG5cbkBJbmplY3RhYmxlKHtwcm92aWRlZEluOiAncm9vdCd9KVxuZXhwb3J0IGNsYXNzIFJlc3RTZXJpYWxpemVyU2VydmljZSBleHRlbmRzIEJhc2VTZXJpYWxpemVyU2VydmljZSB7XG4gIHByb3RlY3RlZCBwZXNxdWlzYUZpbHRyb09wZXJhZG9yUGFkcmFvOiBGaWx0cm9PcGVyYWRvciA9IEZpbHRyb09wZXJhZG9yLkNvbnRlbTtcbiAgcHJvdGVjdGVkIHN1cG9ydGFQZXNxdWlzYSA9IGZhbHNlO1xuICBwcm90ZWN0ZWQgc3Vwb3J0YUZpbHRyb3MgPSB0cnVlO1xuICBwcm90ZWN0ZWQgc3Vwb3J0YVBhZ2luYWNhbyA9IGZhbHNlO1xuICBwcm90ZWN0ZWQgc3Vwb3J0YU9yZGVuYWNhbyA9IGZhbHNlO1xuXG4gIHByb3RlY3RlZCBnZXRGaWx0cm9PcGVyYWRvck1hcGVhZG8oZmlsdHJvT3BlcmFkb3I6IEZpbHRyb09wZXJhZG9yKTogc3RyaW5nIHtcbiAgICBpZiAoZmlsdHJvT3BlcmFkb3IgPT09IEZpbHRyb09wZXJhZG9yLklndWFsKSB7XG4gICAgICByZXR1cm4gJ3tjYW1wb309e3ZhbG9yfSc7XG4gICAgfSBlbHNlIHtcbiAgICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTptYXgtbGluZS1sZW5ndGhcbiAgICAgIHRocm93IG5ldyBFcnJvcignUmVzdCBwdXJvIG7Do28gc3Vwb3J0YSBmaWx0cm8gcXVlIG7Do28gc2VqYSBvIGlndWFsISBJbXBsZW1lbnRlIHNldSBwcsOzcHJpbyBzZXJpYWxpemFkb3IgcGFyYSBjdXN0b21pemFyIGNvbW8gZ2VyYXIgYSByZXF1aXNpw6fDo28gZGV2aWRhbWVudGUhJyk7XG4gICAgfVxuICB9XG5cbiAgcHJvdGVjdGVkIGdldEZpbHRyb0NvbXBvc3RvT3BlcmFkb3JNYXBlYWRvKGZpbHRyb0NvbXBvc3RvT3BlcmFkb3I6IEZpbHRyb0NvbXBvc3RvT3BlcmFkb3IpOiBzdHJpbmcge1xuICAgIGlmIChmaWx0cm9Db21wb3N0b09wZXJhZG9yID09PSBGaWx0cm9Db21wb3N0b09wZXJhZG9yLkUpIHtcbiAgICAgIHJldHVybiAnJic7XG4gICAgfSBlbHNlIHtcbiAgICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTptYXgtbGluZS1sZW5ndGhcbiAgICAgIHRocm93IG5ldyBFcnJvcignUmVzdCBwdXJvIG7Do28gc3Vwb3J0YSBmaWx0cm9zIHF1ZSB1dGlsaXphbSBcIm91XCIgY29tbyBsw7NnaWNhISBJbXBsZW1lbnRlIHNldSBwcsOzcHJpbyBzZXJpYWxpemFkb3IgcGFyYSBjdXN0b21pemFyIGNvbW8gZ2VyYXIgYSByZXF1aXNpw6fDo28gZGV2aWRhbWVudGUhJyk7XG4gICAgfVxuICB9XG5cbiAgcHJvdGVjdGVkIHF1YW50aWRhZGVEZUZpbHRyb3NBbGluaGFkb3NTdXBvcnRhZG9zID0gMTtcbiAgcHJvdGVjdGVkIGZpbHRyb0NsYXVzdWxhRm9ybWF0bzogc3RyaW5nID0gJ3tmaWx0cm9zfSc7XG4gIHByb3RlY3RlZCBmaWx0cm9Gb3JtYXRvOiBzdHJpbmcgPSAne2ZpbHRyb30nO1xuXG4gIHByb3RlY3RlZCBwYWdpbmFjYW9QYWdpbmFDbGF1c3VsYUZvcm1hdG86IHN0cmluZztcbiAgcHJvdGVjdGVkIHBhZ2luYWNhb0l0ZW5zUG9yUGFnaW5hQ2xhdXN1bGFGb3JtYXRvOiBzdHJpbmc7XG5cbiAgcHJvdGVjdGVkIG9yZGVuYWNhb0NsYXVzdWxhRm9ybWF0bzogc3RyaW5nO1xuICBwcm90ZWN0ZWQgb3JkZW5hY2FvRm9ybWF0bzogc3RyaW5nO1xuICBwcm90ZWN0ZWQgb3JkZW5hY29lc1NlcGFyYWRvcjogc3RyaW5nO1xufVxuIl19