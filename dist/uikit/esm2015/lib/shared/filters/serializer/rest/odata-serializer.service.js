/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { BaseSerializerService } from '../base-serializer.service';
import { Injectable, Inject, Optional, InjectionToken } from '@angular/core';
import { FiltroCompostoOperador, FiltroOperador } from '../../filter';
import * as i0 from "@angular/core";
/** @type {?} */
export const ODataSerializerServiceConfigToken = new InjectionToken('ODataSerializerServiceConfigToken');
/**
 * @record
 */
export function ODataSerializerServiceConfig() { }
if (false) {
    /** @type {?} */
    ODataSerializerServiceConfig.prototype.requestCount;
}
export class ODataSerializerService extends BaseSerializerService {
    /**
     * @param {?=} oDataSerializerServiceConfig
     */
    constructor(oDataSerializerServiceConfig) {
        super();
        this.pesquisaFiltroOperadorPadrao = FiltroOperador.Contem;
        this.suportaPesquisa = false;
        this.suportaFiltros = true;
        this.suportaPaginacao = true;
        this.suportaOrdenacao = true;
        this.pesquisaClausulaFormato = `$search={pesquisa}`;
        this.ordenacaoClausulaFormato = `$orderby={ordenacoes}`;
        this.ordenacaoFormato = `{campo} {direcao}`;
        this.ordenacoesSeparador = `, `;
        this.paginacaoPaginaClausulaFormato = `$skip={pagina}`;
        this.paginacaoItensPorPaginaClausulaFormato = `$take={itensPorPagina}`;
        this.filtroClausulaFormato = `$filter={filtros}`;
        this.filtroFormato = `({filtro})`;
        /** @type {?} */
        const defaultParameter = (/** @type {?} */ ({ requestCount: true }));
        this.oDataConfig = Object.assign(defaultParameter, oDataSerializerServiceConfig);
    }
    /**
     * @protected
     * @param {?} parametro
     * @return {?}
     */
    serializeInterceptor(parametro) {
        if (this.oDataConfig.requestCount) {
            return `$count=true`;
        }
    }
    /**
     * @protected
     * @param {?} paginacao
     * @return {?}
     */
    paginacaoPaginaInterceptor(paginacao) {
        /** @type {?} */
        const paginaZeroBased = paginacao.pagina - 1;
        /** @type {?} */
        const pagina = paginaZeroBased * paginacao.itensPorPagina;
        return pagina;
    }
    /**
     * @protected
     * @param {?} filtroOperador
     * @return {?}
     */
    getFiltroOperadorMapeado(filtroOperador) {
        switch (filtroOperador) {
            case FiltroOperador.ComecaCom: {
                return `startswith({campo},{valor})`;
            }
            case FiltroOperador.Diferente: {
                return `{campo} ne {valor}`;
            }
            case FiltroOperador.Contem: {
                return `contains({campo},{valor})`;
            }
            case FiltroOperador.Igual: {
                return `{campo} eq {valor}`;
            }
            case FiltroOperador.Maior: {
                return `{campo} gt {valor}`;
            }
            case FiltroOperador.MaiorOuIgual: {
                return `{campo} ge {valor}`;
            }
            case FiltroOperador.Menor: {
                return `{campo} lt {valor}`;
            }
            case FiltroOperador.MenorOuIgual: {
                return `{campo} le {valor}`;
            }
            case FiltroOperador.Em: {
                return `{campo} in ({valor})`;
            }
            case FiltroOperador.NaoContem: {
                return `contains({campo},{valor}) eq false`;
            }
            case FiltroOperador.TerminaCom: {
                return `endswith({campo},{valor})`;
            }
        }
        throw new Error(`Não foi encontrado um operador equivalente a '${filtroOperador}'!`);
    }
    /**
     * @protected
     * @param {?} filtroCompostoOperador
     * @return {?}
     */
    getFiltroCompostoOperadorMapeado(filtroCompostoOperador) {
        switch (filtroCompostoOperador) {
            case FiltroCompostoOperador.E: {
                return ` and `;
            }
            case FiltroCompostoOperador.Ou: {
                return ` or `;
            }
        }
        throw new Error(`Não foi encontrado um operador equivalente a '${filtroCompostoOperador}'!`);
    }
    /**
     * @protected
     * @param {?} filtro
     * @return {?}
     */
    filtroValorInterceptor(filtro) {
        /** @type {?} */
        const valor = filtro.valor;
        if (valor instanceof Date) {
            return valor.toISOString();
        }
        else if (Array.isArray(valor)) {
            return "'" + valor.join("','") + "'";
        }
        else if (typeof valor === 'string') {
            return `'${valor}'`;
        }
        return valor;
    }
}
ODataSerializerService.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */
ODataSerializerService.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [ODataSerializerServiceConfigToken,] }, { type: Optional }] }
];
/** @nocollapse */ ODataSerializerService.ngInjectableDef = i0.defineInjectable({ factory: function ODataSerializerService_Factory() { return new ODataSerializerService(i0.inject(ODataSerializerServiceConfigToken, 8)); }, token: ODataSerializerService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @protected
     */
    ODataSerializerService.prototype.pesquisaFiltroOperadorPadrao;
    /**
     * @type {?}
     * @protected
     */
    ODataSerializerService.prototype.suportaPesquisa;
    /**
     * @type {?}
     * @protected
     */
    ODataSerializerService.prototype.suportaFiltros;
    /**
     * @type {?}
     * @protected
     */
    ODataSerializerService.prototype.suportaPaginacao;
    /**
     * @type {?}
     * @protected
     */
    ODataSerializerService.prototype.suportaOrdenacao;
    /** @type {?} */
    ODataSerializerService.prototype.pesquisaClausulaFormato;
    /** @type {?} */
    ODataSerializerService.prototype.oDataConfig;
    /** @type {?} */
    ODataSerializerService.prototype.ordenacaoClausulaFormato;
    /** @type {?} */
    ODataSerializerService.prototype.ordenacaoFormato;
    /** @type {?} */
    ODataSerializerService.prototype.ordenacoesSeparador;
    /** @type {?} */
    ODataSerializerService.prototype.paginacaoPaginaClausulaFormato;
    /** @type {?} */
    ODataSerializerService.prototype.paginacaoItensPorPaginaClausulaFormato;
    /** @type {?} */
    ODataSerializerService.prototype.filtroClausulaFormato;
    /** @type {?} */
    ODataSerializerService.prototype.filtroFormato;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib2RhdGEtc2VyaWFsaXplci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvZmlsdGVycy9zZXJpYWxpemVyL3Jlc3Qvb2RhdGEtc2VyaWFsaXplci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMscUJBQXFCLEVBQUMsTUFBTSw0QkFBNEIsQ0FBQztBQUNqRSxPQUFPLEVBQUMsVUFBVSxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUUsY0FBYyxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQzNFLE9BQU8sRUFBUyxzQkFBc0IsRUFBRSxjQUFjLEVBQUMsTUFBTSxjQUFjLENBQUM7OztBQUk1RSxNQUFNLE9BQU8saUNBQWlDLEdBQUcsSUFBSSxjQUFjLENBQStCLG1DQUFtQyxDQUFDOzs7O0FBRXRJLGtEQUVDOzs7SUFEQyxvREFBc0I7O0FBSXhCLE1BQU0sT0FBTyxzQkFBdUIsU0FBUSxxQkFBcUI7Ozs7SUFXL0QsWUFBbUUsNEJBQTJEO1FBQzVILEtBQUssRUFBRSxDQUFDO1FBWEEsaUNBQTRCLEdBQW1CLGNBQWMsQ0FBQyxNQUFNLENBQUM7UUFDckUsb0JBQWUsR0FBRyxLQUFLLENBQUM7UUFDeEIsbUJBQWMsR0FBRyxJQUFJLENBQUM7UUFDdEIscUJBQWdCLEdBQUcsSUFBSSxDQUFDO1FBQ3hCLHFCQUFnQixHQUFHLElBQUksQ0FBQztRQUVsQyw0QkFBdUIsR0FBRyxvQkFBb0IsQ0FBQztRQWlCL0MsNkJBQXdCLEdBQUcsdUJBQXVCLENBQUM7UUFDbkQscUJBQWdCLEdBQUcsbUJBQW1CLENBQUM7UUFDdkMsd0JBQW1CLEdBQUcsSUFBSSxDQUFDO1FBUTNCLG1DQUE4QixHQUFHLGdCQUFnQixDQUFDO1FBQ2xELDJDQUFzQyxHQUFHLHdCQUF3QixDQUFDO1FBRWxFLDBCQUFxQixHQUFHLG1CQUFtQixDQUFDO1FBQzVDLGtCQUFhLEdBQUcsWUFBWSxDQUFDOztjQXpCckIsZ0JBQWdCLEdBQUcsbUJBQUEsRUFBQyxZQUFZLEVBQUUsSUFBSSxFQUFDLEVBQWdDO1FBQzdFLElBQUksQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsRUFBRSw0QkFBNEIsQ0FBQyxDQUFDO0lBQ25GLENBQUM7Ozs7OztJQUdTLG9CQUFvQixDQUFDLFNBQWlDO1FBQzlELElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLEVBQUU7WUFDakMsT0FBTyxhQUFhLENBQUM7U0FDdEI7SUFDSCxDQUFDOzs7Ozs7SUFNUywwQkFBMEIsQ0FBQyxTQUFvQjs7Y0FDakQsZUFBZSxHQUFHLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQzs7Y0FDdEMsTUFBTSxHQUFHLGVBQWUsR0FBRyxTQUFTLENBQUMsY0FBYztRQUN6RCxPQUFPLE1BQU0sQ0FBQztJQUNoQixDQUFDOzs7Ozs7SUFRUyx3QkFBd0IsQ0FBQyxjQUE4QjtRQUMvRCxRQUFRLGNBQWMsRUFBRTtZQUN0QixLQUFLLGNBQWMsQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDN0IsT0FBTyw2QkFBNkIsQ0FBQzthQUN0QztZQUNELEtBQUssY0FBYyxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUM3QixPQUFPLG9CQUFvQixDQUFDO2FBQzdCO1lBQ0QsS0FBSyxjQUFjLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQzFCLE9BQU8sMkJBQTJCLENBQUM7YUFDcEM7WUFDRCxLQUFLLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDekIsT0FBTyxvQkFBb0IsQ0FBQzthQUM3QjtZQUNELEtBQUssY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUN6QixPQUFPLG9CQUFvQixDQUFDO2FBQzdCO1lBQ0QsS0FBSyxjQUFjLENBQUMsWUFBWSxDQUFDLENBQUM7Z0JBQ2hDLE9BQU8sb0JBQW9CLENBQUM7YUFDN0I7WUFDRCxLQUFLLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDekIsT0FBTyxvQkFBb0IsQ0FBQzthQUM3QjtZQUNELEtBQUssY0FBYyxDQUFDLFlBQVksQ0FBQyxDQUFDO2dCQUNoQyxPQUFPLG9CQUFvQixDQUFDO2FBQzdCO1lBQ0QsS0FBSyxjQUFjLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ3RCLE9BQU8sc0JBQXNCLENBQUM7YUFDL0I7WUFDRCxLQUFLLGNBQWMsQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDN0IsT0FBTyxvQ0FBb0MsQ0FBQzthQUM3QztZQUNELEtBQUssY0FBYyxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUM5QixPQUFPLDJCQUEyQixDQUFDO2FBQ3BDO1NBQ0Y7UUFDRCxNQUFNLElBQUksS0FBSyxDQUFDLGlEQUFpRCxjQUFjLElBQUksQ0FBQyxDQUFDO0lBQ3ZGLENBQUM7Ozs7OztJQUVTLGdDQUFnQyxDQUFDLHNCQUE4QztRQUN2RixRQUFRLHNCQUFzQixFQUFFO1lBQzlCLEtBQUssc0JBQXNCLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzdCLE9BQU8sT0FBTyxDQUFDO2FBQ2hCO1lBQ0QsS0FBSyxzQkFBc0IsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDOUIsT0FBTyxNQUFNLENBQUM7YUFDZjtTQUNGO1FBQ0QsTUFBTSxJQUFJLEtBQUssQ0FBQyxpREFBaUQsc0JBQXNCLElBQUksQ0FBQyxDQUFDO0lBQy9GLENBQUM7Ozs7OztJQUVTLHNCQUFzQixDQUFDLE1BQWM7O2NBQ3ZDLEtBQUssR0FBRyxNQUFNLENBQUMsS0FBSztRQUMxQixJQUFJLEtBQUssWUFBWSxJQUFJLEVBQUU7WUFDekIsT0FBTyxLQUFLLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDNUI7YUFBTSxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDL0IsT0FBTyxHQUFHLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxHQUFHLENBQUM7U0FDdEM7YUFBTSxJQUFJLE9BQU8sS0FBSyxLQUFLLFFBQVEsRUFBRTtZQUNwQyxPQUFPLElBQUksS0FBSyxHQUFHLENBQUM7U0FDckI7UUFDRCxPQUFPLEtBQUssQ0FBQztJQUNmLENBQUM7OztZQXRHRixVQUFVLFNBQUMsRUFBQyxVQUFVLEVBQUUsTUFBTSxFQUFDOzs7OzRDQVlqQixNQUFNLFNBQUMsaUNBQWlDLGNBQUcsUUFBUTs7Ozs7Ozs7SUFWaEUsOERBQStFOzs7OztJQUMvRSxpREFBa0M7Ozs7O0lBQ2xDLGdEQUFnQzs7Ozs7SUFDaEMsa0RBQWtDOzs7OztJQUNsQyxrREFBa0M7O0lBRWxDLHlEQUErQzs7SUFFL0MsNkNBQTBDOztJQWUxQywwREFBbUQ7O0lBQ25ELGtEQUF1Qzs7SUFDdkMscURBQTJCOztJQVEzQixnRUFBa0Q7O0lBQ2xELHdFQUFrRTs7SUFFbEUsdURBQTRDOztJQUM1QywrQ0FBNkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0Jhc2VTZXJpYWxpemVyU2VydmljZX0gZnJvbSAnLi4vYmFzZS1zZXJpYWxpemVyLnNlcnZpY2UnO1xuaW1wb3J0IHtJbmplY3RhYmxlLCBJbmplY3QsIE9wdGlvbmFsLCBJbmplY3Rpb25Ub2tlbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0ZpbHRybywgRmlsdHJvQ29tcG9zdG9PcGVyYWRvciwgRmlsdHJvT3BlcmFkb3J9IGZyb20gJy4uLy4uL2ZpbHRlcic7XG5pbXBvcnQge1BhcmFtZXRyb3NEYVJlcXVpc2ljYW99IGZyb20gJy4uLy4uL3JlcXVlc3QtcGFyYW1ldGVycyc7XG5pbXBvcnQge1BhZ2luYWNhb30gZnJvbSAnLi4vLi4vcGFnZSc7XG5cbmV4cG9ydCBjb25zdCBPRGF0YVNlcmlhbGl6ZXJTZXJ2aWNlQ29uZmlnVG9rZW4gPSBuZXcgSW5qZWN0aW9uVG9rZW48T0RhdGFTZXJpYWxpemVyU2VydmljZUNvbmZpZz4oJ09EYXRhU2VyaWFsaXplclNlcnZpY2VDb25maWdUb2tlbicpO1xuXG5leHBvcnQgaW50ZXJmYWNlIE9EYXRhU2VyaWFsaXplclNlcnZpY2VDb25maWcge1xuICByZXF1ZXN0Q291bnQ6IGJvb2xlYW47XG59XG5cbkBJbmplY3RhYmxlKHtwcm92aWRlZEluOiAncm9vdCd9KVxuZXhwb3J0IGNsYXNzIE9EYXRhU2VyaWFsaXplclNlcnZpY2UgZXh0ZW5kcyBCYXNlU2VyaWFsaXplclNlcnZpY2Uge1xuICBwcm90ZWN0ZWQgcGVzcXVpc2FGaWx0cm9PcGVyYWRvclBhZHJhbzogRmlsdHJvT3BlcmFkb3IgPSBGaWx0cm9PcGVyYWRvci5Db250ZW07XG4gIHByb3RlY3RlZCBzdXBvcnRhUGVzcXVpc2EgPSBmYWxzZTtcbiAgcHJvdGVjdGVkIHN1cG9ydGFGaWx0cm9zID0gdHJ1ZTtcbiAgcHJvdGVjdGVkIHN1cG9ydGFQYWdpbmFjYW8gPSB0cnVlO1xuICBwcm90ZWN0ZWQgc3Vwb3J0YU9yZGVuYWNhbyA9IHRydWU7XG5cbiAgcGVzcXVpc2FDbGF1c3VsYUZvcm1hdG8gPSBgJHNlYXJjaD17cGVzcXVpc2F9YDtcblxuICBvRGF0YUNvbmZpZzogT0RhdGFTZXJpYWxpemVyU2VydmljZUNvbmZpZztcblxuICBjb25zdHJ1Y3RvcihASW5qZWN0KE9EYXRhU2VyaWFsaXplclNlcnZpY2VDb25maWdUb2tlbikgQE9wdGlvbmFsKCkgb0RhdGFTZXJpYWxpemVyU2VydmljZUNvbmZpZz86IE9EYXRhU2VyaWFsaXplclNlcnZpY2VDb25maWcpIHtcbiAgICBzdXBlcigpO1xuICAgIGNvbnN0IGRlZmF1bHRQYXJhbWV0ZXIgPSB7cmVxdWVzdENvdW50OiB0cnVlfSBhcyBPRGF0YVNlcmlhbGl6ZXJTZXJ2aWNlQ29uZmlnO1xuICAgIHRoaXMub0RhdGFDb25maWcgPSBPYmplY3QuYXNzaWduKGRlZmF1bHRQYXJhbWV0ZXIsIG9EYXRhU2VyaWFsaXplclNlcnZpY2VDb25maWcpO1xuICB9XG5cblxuICBwcm90ZWN0ZWQgc2VyaWFsaXplSW50ZXJjZXB0b3IocGFyYW1ldHJvOiBQYXJhbWV0cm9zRGFSZXF1aXNpY2FvKTogc3RyaW5nIHtcbiAgICBpZiAodGhpcy5vRGF0YUNvbmZpZy5yZXF1ZXN0Q291bnQpIHtcbiAgICAgIHJldHVybiBgJGNvdW50PXRydWVgO1xuICAgIH1cbiAgfVxuXG4gIG9yZGVuYWNhb0NsYXVzdWxhRm9ybWF0byA9IGAkb3JkZXJieT17b3JkZW5hY29lc31gO1xuICBvcmRlbmFjYW9Gb3JtYXRvID0gYHtjYW1wb30ge2RpcmVjYW99YDtcbiAgb3JkZW5hY29lc1NlcGFyYWRvciA9IGAsIGA7XG5cbiAgcHJvdGVjdGVkIHBhZ2luYWNhb1BhZ2luYUludGVyY2VwdG9yKHBhZ2luYWNhbzogUGFnaW5hY2FvKSB7XG4gICAgY29uc3QgcGFnaW5hWmVyb0Jhc2VkID0gcGFnaW5hY2FvLnBhZ2luYSAtIDE7XG4gICAgY29uc3QgcGFnaW5hID0gcGFnaW5hWmVyb0Jhc2VkICogcGFnaW5hY2FvLml0ZW5zUG9yUGFnaW5hOyAvLyBQYXJhIE9EYXRhIMOpICRza2lwLCBlbnTDo28gc2lnbmlmaWNhICdwdWxhIGl0ZW5zJyBlIG7Do28gJ3RyYWdhIGEgcMOhZ2luYScuXG4gICAgcmV0dXJuIHBhZ2luYTtcbiAgfVxuXG4gIHBhZ2luYWNhb1BhZ2luYUNsYXVzdWxhRm9ybWF0byA9IGAkc2tpcD17cGFnaW5hfWA7XG4gIHBhZ2luYWNhb0l0ZW5zUG9yUGFnaW5hQ2xhdXN1bGFGb3JtYXRvID0gYCR0YWtlPXtpdGVuc1BvclBhZ2luYX1gO1xuXG4gIGZpbHRyb0NsYXVzdWxhRm9ybWF0byA9IGAkZmlsdGVyPXtmaWx0cm9zfWA7XG4gIGZpbHRyb0Zvcm1hdG8gPSBgKHtmaWx0cm99KWA7XG5cbiAgcHJvdGVjdGVkIGdldEZpbHRyb09wZXJhZG9yTWFwZWFkbyhmaWx0cm9PcGVyYWRvcjogRmlsdHJvT3BlcmFkb3IpOiBzdHJpbmcge1xuICAgIHN3aXRjaCAoZmlsdHJvT3BlcmFkb3IpIHtcbiAgICAgIGNhc2UgRmlsdHJvT3BlcmFkb3IuQ29tZWNhQ29tOiB7XG4gICAgICAgIHJldHVybiBgc3RhcnRzd2l0aCh7Y2FtcG99LHt2YWxvcn0pYDtcbiAgICAgIH1cbiAgICAgIGNhc2UgRmlsdHJvT3BlcmFkb3IuRGlmZXJlbnRlOiB7XG4gICAgICAgIHJldHVybiBge2NhbXBvfSBuZSB7dmFsb3J9YDtcbiAgICAgIH1cbiAgICAgIGNhc2UgRmlsdHJvT3BlcmFkb3IuQ29udGVtOiB7XG4gICAgICAgIHJldHVybiBgY29udGFpbnMoe2NhbXBvfSx7dmFsb3J9KWA7XG4gICAgICB9XG4gICAgICBjYXNlIEZpbHRyb09wZXJhZG9yLklndWFsOiB7XG4gICAgICAgIHJldHVybiBge2NhbXBvfSBlcSB7dmFsb3J9YDtcbiAgICAgIH1cbiAgICAgIGNhc2UgRmlsdHJvT3BlcmFkb3IuTWFpb3I6IHtcbiAgICAgICAgcmV0dXJuIGB7Y2FtcG99IGd0IHt2YWxvcn1gO1xuICAgICAgfVxuICAgICAgY2FzZSBGaWx0cm9PcGVyYWRvci5NYWlvck91SWd1YWw6IHtcbiAgICAgICAgcmV0dXJuIGB7Y2FtcG99IGdlIHt2YWxvcn1gO1xuICAgICAgfVxuICAgICAgY2FzZSBGaWx0cm9PcGVyYWRvci5NZW5vcjoge1xuICAgICAgICByZXR1cm4gYHtjYW1wb30gbHQge3ZhbG9yfWA7XG4gICAgICB9XG4gICAgICBjYXNlIEZpbHRyb09wZXJhZG9yLk1lbm9yT3VJZ3VhbDoge1xuICAgICAgICByZXR1cm4gYHtjYW1wb30gbGUge3ZhbG9yfWA7XG4gICAgICB9XG4gICAgICBjYXNlIEZpbHRyb09wZXJhZG9yLkVtOiB7XG4gICAgICAgIHJldHVybiBge2NhbXBvfSBpbiAoe3ZhbG9yfSlgO1xuICAgICAgfVxuICAgICAgY2FzZSBGaWx0cm9PcGVyYWRvci5OYW9Db250ZW06IHtcbiAgICAgICAgcmV0dXJuIGBjb250YWlucyh7Y2FtcG99LHt2YWxvcn0pIGVxIGZhbHNlYDtcbiAgICAgIH1cbiAgICAgIGNhc2UgRmlsdHJvT3BlcmFkb3IuVGVybWluYUNvbToge1xuICAgICAgICByZXR1cm4gYGVuZHN3aXRoKHtjYW1wb30se3ZhbG9yfSlgO1xuICAgICAgfVxuICAgIH1cbiAgICB0aHJvdyBuZXcgRXJyb3IoYE7Do28gZm9pIGVuY29udHJhZG8gdW0gb3BlcmFkb3IgZXF1aXZhbGVudGUgYSAnJHtmaWx0cm9PcGVyYWRvcn0nIWApO1xuICB9XG5cbiAgcHJvdGVjdGVkIGdldEZpbHRyb0NvbXBvc3RvT3BlcmFkb3JNYXBlYWRvKGZpbHRyb0NvbXBvc3RvT3BlcmFkb3I6IEZpbHRyb0NvbXBvc3RvT3BlcmFkb3IpOiBzdHJpbmcge1xuICAgIHN3aXRjaCAoZmlsdHJvQ29tcG9zdG9PcGVyYWRvcikge1xuICAgICAgY2FzZSBGaWx0cm9Db21wb3N0b09wZXJhZG9yLkU6IHtcbiAgICAgICAgcmV0dXJuIGAgYW5kIGA7XG4gICAgICB9XG4gICAgICBjYXNlIEZpbHRyb0NvbXBvc3RvT3BlcmFkb3IuT3U6IHtcbiAgICAgICAgcmV0dXJuIGAgb3IgYDtcbiAgICAgIH1cbiAgICB9XG4gICAgdGhyb3cgbmV3IEVycm9yKGBOw6NvIGZvaSBlbmNvbnRyYWRvIHVtIG9wZXJhZG9yIGVxdWl2YWxlbnRlIGEgJyR7ZmlsdHJvQ29tcG9zdG9PcGVyYWRvcn0nIWApO1xuICB9XG5cbiAgcHJvdGVjdGVkIGZpbHRyb1ZhbG9ySW50ZXJjZXB0b3IoZmlsdHJvOiBGaWx0cm8pOiBzdHJpbmcge1xuICAgIGNvbnN0IHZhbG9yID0gZmlsdHJvLnZhbG9yO1xuICAgIGlmICh2YWxvciBpbnN0YW5jZW9mIERhdGUpIHtcbiAgICAgIHJldHVybiB2YWxvci50b0lTT1N0cmluZygpO1xuICAgIH0gZWxzZSBpZiAoQXJyYXkuaXNBcnJheSh2YWxvcikpIHtcbiAgICAgIHJldHVybiBcIidcIiArIHZhbG9yLmpvaW4oXCInLCdcIikgKyBcIidcIjtcbiAgICB9IGVsc2UgaWYgKHR5cGVvZiB2YWxvciA9PT0gJ3N0cmluZycpIHtcbiAgICAgIHJldHVybiBgJyR7dmFsb3J9J2A7XG4gICAgfVxuICAgIHJldHVybiB2YWxvcjtcbiAgfVxufVxuIl19