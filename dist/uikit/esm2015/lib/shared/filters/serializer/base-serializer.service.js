/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { InjectionToken } from '@angular/core';
import { FiltroOperador, isFilterComposite, FiltroCompostoOperador } from '../filter';
import { SearchMode } from '../pesquisa';
/** @type {?} */
export const SERIALIZER_TOKEN = new InjectionToken('Serializer');
/**
 * @abstract
 */
export class BaseSerializerService {
    constructor() {
        this.quantidadeDeFiltrosAlinhadosSuportados = Number.MAX_SAFE_INTEGER;
        this.pesquisaFiltroOperadorPadrao = FiltroOperador.Contem;
        this.pesquisaFiltroCompostoOperadorPadrao = FiltroCompostoOperador.Ou;
    }
    /**
     * @param {?} parametrosDaRequisicao
     * @return {?}
     */
    serializeFiltros(parametrosDaRequisicao) {
        if (!this.suportaFiltros) {
            throw new Error('Este serializador não suporta filtros!');
        }
        /** @type {?} */
        const formatoDaClausula = this.filtroClausulaFormato;
        /** @type {?} */
        const filtrosCompostoSerializado = this.serializeFiltroComposto(parametrosDaRequisicao.filtros);
        if (filtrosCompostoSerializado != null) {
            /** @type {?} */
            const filtrosSerializado = this.format(formatoDaClausula, { filtros: filtrosCompostoSerializado });
            return filtrosSerializado;
        }
        else {
            return null;
        }
    }
    /**
     * @param {?} filtroComposto
     * @param {?=} nivel
     * @return {?}
     */
    serializeFiltroComposto(filtroComposto, nivel = 1) {
        if (nivel > this.quantidadeDeFiltrosAlinhadosSuportados) {
            throw new Error(`A quantidade de filtros alinhados suportados é '${this.quantidadeDeFiltrosAlinhadosSuportados}' e foi alcançado o nível '${nivel}'!`);
        }
        /** @type {?} */
        const filtrosSerializadosArray = filtroComposto.filtros.map((/**
         * @param {?} filtro
         * @return {?}
         */
        filtro => {
            if (isFilterComposite(filtro)) {
                /** @type {?} */
                const formatoDosFiltros = this.filtroFormato;
                /** @type {?} */
                const proximoNivel = nivel + 1;
                /** @type {?} */
                const filtrosCompostoSerializado = this.serializeFiltroComposto(filtro, proximoNivel);
                /** @type {?} */
                const filtrosSerializado = this.format(formatoDosFiltros, { filtro: filtrosCompostoSerializado });
                return filtrosSerializado;
            }
            else {
                return this.serializeFiltro(filtro);
            }
        }));
        /** @type {?} */
        let filtroCompostoOperador = null;
        if (filtrosSerializadosArray.length > 1) {
            filtroCompostoOperador = this.getFiltroCompostoOperadorMapeado(filtroComposto.operador);
        }
        if (filtrosSerializadosArray.length > 0) {
            /** @type {?} */
            const filtrosSerializados = this.tratarArray(filtrosSerializadosArray, filtroCompostoOperador);
            return filtrosSerializados;
        }
        else {
            return null;
        }
    }
    /**
     * @param {?} filtro
     * @return {?}
     */
    serializeFiltro(filtro) {
        /** @type {?} */
        const filtroOperador = this.getFiltroOperadorMapeado(filtro.operador);
        /** @type {?} */
        const campo = this.filtroCampoInterceptor(filtro);
        /** @type {?} */
        const valor = this.filtroValorInterceptor(filtro);
        /** @type {?} */
        const filtroSerializado = this.format(filtroOperador, { campo, valor });
        return filtroSerializado;
    }
    /**
     * @protected
     * @param {?} stringArray
     * @param {?=} join
     * @return {?}
     */
    tratarArray(stringArray, join = '&') {
        /** @type {?} */
        const filtrosSerializados = stringArray.filter((/**
         * @param {?} s
         * @return {?}
         */
        s => s !== '' && s != null)).join(join);
        return filtrosSerializados;
    }
    /**
     * @protected
     * @param {?} filtro
     * @return {?}
     */
    filtroValorInterceptor(filtro) {
        return filtro.valor;
    }
    /**
     * @protected
     * @param {?} filtro
     * @return {?}
     */
    filtroCampoInterceptor(filtro) {
        return filtro.campo;
    }
    /**
     * @protected
     * @param {?} pesquisa
     * @return {?}
     */
    pesquisaTermoInterceptor(pesquisa) {
        return pesquisa.termo;
    }
    /**
     * @protected
     * @return {?}
     */
    get pesquisaModoPadrao() {
        if (this.suportaPesquisa) {
            return SearchMode.FullTextSearch;
        }
        else {
            if (this.suportaFiltros) {
                return SearchMode.Filtro;
            }
        }
    }
    /**
     * @param {?} parametrosDaRequisicao
     * @return {?}
     */
    serializePesquisa(parametrosDaRequisicao) {
        /** @type {?} */
        const termo = this.pesquisaTermoInterceptor(parametrosDaRequisicao.pesquisa);
        if (parametrosDaRequisicao.pesquisa.config == null) {
            parametrosDaRequisicao.pesquisa.config = { modo: this.pesquisaModoPadrao };
        }
        if (parametrosDaRequisicao.pesquisa.config.modo == null) {
            // Se não informar as configurações, deve prevalecer o que o serializador definiu como padrao
            parametrosDaRequisicao.pesquisa.config.modo = this.pesquisaModoPadrao;
        }
        if ((this.suportaFiltros && !this.suportaPesquisa) &&
            (parametrosDaRequisicao.pesquisa.config.modo === SearchMode.Filtro || this.pesquisaClausulaFormato == null)) {
            // Este é o caso quando a pesquisa é realizada por meio de filtro simples
            if (parametrosDaRequisicao.pesquisa.config.filtro.campos == null ||
                parametrosDaRequisicao.pesquisa.config.filtro.campos.length <= 0) {
                throw new Error('Na pesquisa com modo Filtro, os campos devem ser informados!');
            }
            /** @type {?} */
            const operador = parametrosDaRequisicao.pesquisa.config.filtro.operador ?
                parametrosDaRequisicao.pesquisa.config.filtro.operador : this.pesquisaFiltroOperadorPadrao;
            /** @type {?} */
            const filtros = parametrosDaRequisicao.pesquisa.config.filtro.campos.map((/**
             * @param {?} campo
             * @return {?}
             */
            campo => {
                /** @type {?} */
                const filtro = (/** @type {?} */ ({ campo, operador, valor: termo }));
                return filtro;
            }));
            /** @type {?} */
            const filtroComposto = (/** @type {?} */ ({ operador: this.pesquisaFiltroCompostoOperadorPadrao, filtros }));
            parametrosDaRequisicao.filtros.filtros.push(filtroComposto);
            return null;
        }
        else {
            if (!this.suportaPesquisa) {
                throw new Error('Este serializador não suporta pesquisa!');
            }
            /** @type {?} */
            const clausula = this.pesquisaClausulaFormato;
            /** @type {?} */
            const clausulaSerializada = this.format(clausula, { pesquisa: termo });
            return clausulaSerializada;
        }
    }
    /**
     * @param {?} parametros
     * @return {?}
     */
    serialize(parametros) {
        /** @type {?} */
        const serializaveis = [];
        if (parametros.pesquisa) {
            /** @type {?} */
            const serializacaoDaPesquisa = this.serializePesquisa(parametros);
            serializaveis.push(serializacaoDaPesquisa);
        }
        if (parametros.filtros) {
            /** @type {?} */
            const serializacaoDosFiltros = this.serializeFiltros(parametros);
            serializaveis.push(serializacaoDosFiltros);
        }
        if (parametros.paginacao) {
            /** @type {?} */
            const serializacaoDaPaginacao = this.serializePaginacao(parametros);
            serializaveis.push(serializacaoDaPaginacao);
        }
        if (parametros.ordenacoes) {
            /** @type {?} */
            const serializacaoDaOrdenacao = this.serializeOrdenacao(parametros);
            serializaveis.push(serializacaoDaOrdenacao);
        }
        if (this.serializeInterceptor) {
            /** @type {?} */
            const parametroSerializado = this.serializeInterceptor(parametros);
            serializaveis.push(parametroSerializado);
        }
        /** @type {?} */
        const parametrosSerializados = this.tratarArray(serializaveis);
        return parametrosSerializados;
    }
    /**
     * @param {?} parametrosDaRequisicao
     * @return {?}
     */
    serializePaginacao(parametrosDaRequisicao) {
        if (!this.suportaPaginacao) {
            throw new Error('Este serializador não suporta paginacao!');
        }
        /** @type {?} */
        const serializaveis = [];
        /** @type {?} */
        const paginacaoPaginaClausulaFormato = this.paginacaoPaginaClausulaFormato;
        /** @type {?} */
        const paginacaoPaginaParaFormatar = {
            pagina: this.paginacaoPaginaInterceptor(parametrosDaRequisicao.paginacao),
        };
        /** @type {?} */
        const paginacaoPaginaSerializado = this.format(paginacaoPaginaClausulaFormato, paginacaoPaginaParaFormatar);
        serializaveis.push(paginacaoPaginaSerializado);
        /** @type {?} */
        const paginacaoItensPorPaginaClausulaFormato = this.paginacaoItensPorPaginaClausulaFormato;
        /** @type {?} */
        const paginacaoItensPorPaginaParaFormatar = {
            itensPorPagina: this.paginacaoItensPorPaginaInterceptor(parametrosDaRequisicao.paginacao),
        };
        /** @type {?} */
        const paginacaoItensPorPaginaSerializado = this.format(paginacaoItensPorPaginaClausulaFormato, paginacaoItensPorPaginaParaFormatar);
        serializaveis.push(paginacaoItensPorPaginaSerializado);
        /** @type {?} */
        const parametrosSerializados = this.tratarArray(serializaveis);
        return parametrosSerializados;
    }
    /**
     * @protected
     * @param {?} paginacao
     * @return {?}
     */
    paginacaoPaginaInterceptor(paginacao) {
        return paginacao.pagina;
    }
    /**
     * @protected
     * @param {?} paginacao
     * @return {?}
     */
    paginacaoItensPorPaginaInterceptor(paginacao) {
        return paginacao.itensPorPagina;
    }
    /**
     * @param {?} parametrosDaRequisicao
     * @return {?}
     */
    serializeOrdenacao(parametrosDaRequisicao) {
        if (!this.suportaOrdenacao) {
            throw new Error('Este serializador não suporta ordenacao!');
        }
        // Name asc, Description desc
        /** @type {?} */
        const paginacaoArray = parametrosDaRequisicao.ordenacoes.map((/**
         * @param {?} ordenacao
         * @return {?}
         */
        ordenacao => {
            /** @type {?} */
            const ordenacaoFormato = this.ordenacaoFormato;
            // `{campo} {direcao}`;
            /** @type {?} */
            const ordenacaoParaFormatar = (/** @type {?} */ ({
                campo: this.ordenacaoCampoInterceptor(ordenacao),
                direcao: this.ordenacaoDirecaoInterceptor(ordenacao),
            }));
            /** @type {?} */
            const ordenacoesSerializadas = this.format(ordenacaoFormato, ordenacaoParaFormatar);
            return ordenacoesSerializadas;
        }));
        /** @type {?} */
        const ordenacoesSeparador = this.ordenacoesSeparador;
        /** @type {?} */
        const parametrosSerializados = this.tratarArray(paginacaoArray, ordenacoesSeparador);
        /** @type {?} */
        const ordenacaoClausula = this.ordenacaoClausulaFormato;
        // `$orderby={ordenacoes}`;
        /** @type {?} */
        const clausulaFormatada = this.format(ordenacaoClausula, { ordenacoes: parametrosSerializados });
        return clausulaFormatada;
    }
    /**
     * @protected
     * @param {?} ordenacao
     * @return {?}
     */
    ordenacaoCampoInterceptor(ordenacao) {
        return ordenacao.campo;
    }
    /**
     * @protected
     * @param {?} ordenacao
     * @return {?}
     */
    ordenacaoDirecaoInterceptor(ordenacao) {
        return ordenacao.direcao;
    }
    /**
     * @protected
     * @param {?} formato
     * @param {...?} args
     * @return {?}
     */
    format(formato, ...args) {
        if (typeof args[0] !== 'object') {
            return formato.replace(/{\d+}/g, (/**
             * @param {?} m
             * @return {?}
             */
            (m) => {
                /** @type {?} */
                const index = Number(m.replace(/\D/g, ''));
                return (args[index] ? args[index] : m);
            }));
        }
        else {
            /** @type {?} */
            const obj = args[0];
            return formato.replace(/{\w+}/g, (/**
             * @param {?} m
             * @return {?}
             */
            (m) => {
                /** @type {?} */
                const key = m.replace(/{|}/g, '');
                return (obj.hasOwnProperty(key) ? obj[key] : m);
            }));
        }
    }
}
if (false) {
    /**
     * Formato que sera serializado a
     * Tag correspondente a query string
     * enviada na requizição da pesquisa.
     *
     * A palavra chave para substituição pelo filtro é `{filtros}`
     *
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.filtroClausulaFormato;
    /**
     *  Corresponde a tag que sera aplicada na formatação dos filtros.
     *
     *  A palavra chave para substituição pelo filtro é `{filtro}`
     *
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.filtroFormato;
    /**
     * Habilita suporte para modo de pesquisa.
     *
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.suportaPesquisa;
    /**
     * Habilita suporte para modo de pesquisa utilizando a lógica com filtros.
     *
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.suportaFiltros;
    /**
     *  Habilita suporte para modo de pesquisa utilizando a lógica com paginação.
     *
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.suportaPaginacao;
    /**
     * Habilita suporte para modo de pesquisa utilizando a lógica com ordenação.
     *
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.suportaOrdenacao;
    /**
     * Formato que sera serializado a
     * Tag correspondente a query string 'skip'
     * enviada na requisição.
     *
     * A palavra chave para substituição é `{pagina}`
     *
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.paginacaoPaginaClausulaFormato;
    /**
     *  Corresponde a tag que sera aplicada na formatação das clausula dos itens por página.
     *
     *  A palavra chave para substituição é `{itensPorPagina}`
     *
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.paginacaoItensPorPaginaClausulaFormato;
    /**
     * Formato que sera serializado a
     * Tag correspondente a query string
     * enviada na requizição da ordernação da pesquisa.
     *
     * A palavra chave para substituição é `{ordenacoes}`
     *
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.ordenacaoClausulaFormato;
    /**
     *  Corresponde a tag que sera aplicada na formatação das clausula de ordenação.
     *
     *  As palavras chaves para substituição é `{campo}` e `{direcao}`
     *
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.ordenacaoFormato;
    /**
     *  Corresponde ao separador que sera aplicado na serialização das ordenações.
     *
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.ordenacoesSeparador;
    /**
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.quantidadeDeFiltrosAlinhadosSuportados;
    /**
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.pesquisaClausulaFormato;
    /**
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.pesquisaFiltroOperadorPadrao;
    /**
     * @type {?}
     * @protected
     */
    BaseSerializerService.prototype.pesquisaFiltroCompostoOperadorPadrao;
    /**
     * @abstract
     * @protected
     * @param {?} filtroOperador
     * @return {?}
     */
    BaseSerializerService.prototype.getFiltroOperadorMapeado = function (filtroOperador) { };
    /**
     * @abstract
     * @protected
     * @param {?} FiltroCompostoOperador
     * @return {?}
     */
    BaseSerializerService.prototype.getFiltroCompostoOperadorMapeado = function (FiltroCompostoOperador) { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFzZS1zZXJpYWxpemVyLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9maWx0ZXJzL3NlcmlhbGl6ZXIvYmFzZS1zZXJpYWxpemVyLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxjQUFjLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDN0MsT0FBTyxFQUF5QixjQUFjLEVBQUUsaUJBQWlCLEVBQUUsc0JBQXNCLEVBQUMsTUFBTSxXQUFXLENBQUM7QUFLNUcsT0FBTyxFQUFDLFVBQVUsRUFBVyxNQUFNLGFBQWEsQ0FBQzs7QUFFakQsTUFBTSxPQUFPLGdCQUFnQixHQUFHLElBQUksY0FBYyxDQUFhLFlBQVksQ0FBQzs7OztBQUU1RSxNQUFNLE9BQWdCLHFCQUFxQjtJQUEzQztRQXFFWSwyQ0FBc0MsR0FBRyxNQUFNLENBQUMsZ0JBQWdCLENBQUM7UUFzRWpFLGlDQUE0QixHQUFtQixjQUFjLENBQUMsTUFBTSxDQUFDO1FBQ3JFLHlDQUFvQyxHQUEyQixzQkFBc0IsQ0FBQyxFQUFFLENBQUM7SUFpS3JHLENBQUM7Ozs7O0lBdE9RLGdCQUFnQixDQUFDLHNCQUE4QztRQUNwRSxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRTtZQUN4QixNQUFNLElBQUksS0FBSyxDQUFDLHdDQUF3QyxDQUFDLENBQUM7U0FDM0Q7O2NBQ0ssaUJBQWlCLEdBQUcsSUFBSSxDQUFDLHFCQUFxQjs7Y0FDOUMsMEJBQTBCLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDLHNCQUFzQixDQUFDLE9BQU8sQ0FBQztRQUMvRixJQUFJLDBCQUEwQixJQUFJLElBQUksRUFBRTs7a0JBQ2hDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsaUJBQWlCLEVBQUUsRUFBQyxPQUFPLEVBQUUsMEJBQTBCLEVBQUMsQ0FBQztZQUNoRyxPQUFPLGtCQUFrQixDQUFDO1NBQzNCO2FBQU07WUFDTCxPQUFPLElBQUksQ0FBQztTQUNiO0lBQ0gsQ0FBQzs7Ozs7O0lBRU0sdUJBQXVCLENBQUMsY0FBOEIsRUFBRSxLQUFLLEdBQUcsQ0FBQztRQUN0RSxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsc0NBQXNDLEVBQUU7WUFDdkQsTUFBTSxJQUFJLEtBQUssQ0FBQyxtREFBbUQsSUFBSSxDQUFDLHNDQUFzQyw4QkFBOEIsS0FBSyxJQUFJLENBQUMsQ0FBQztTQUN4Sjs7Y0FDSyx3QkFBd0IsR0FBRyxjQUFjLENBQUMsT0FBTyxDQUFDLEdBQUc7Ozs7UUFBQyxNQUFNLENBQUMsRUFBRTtZQUNuRSxJQUFJLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxFQUFFOztzQkFDdkIsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLGFBQWE7O3NCQUN0QyxZQUFZLEdBQUcsS0FBSyxHQUFHLENBQUM7O3NCQUN4QiwwQkFBMEIsR0FBRyxJQUFJLENBQUMsdUJBQXVCLENBQUMsTUFBTSxFQUFFLFlBQVksQ0FBQzs7c0JBQy9FLGtCQUFrQixHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsaUJBQWlCLEVBQUUsRUFBQyxNQUFNLEVBQUUsMEJBQTBCLEVBQUMsQ0FBQztnQkFDL0YsT0FBTyxrQkFBa0IsQ0FBQzthQUMzQjtpQkFBTTtnQkFDTCxPQUFPLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDckM7UUFDSCxDQUFDLEVBQUM7O1lBQ0Usc0JBQXNCLEdBQUcsSUFBSTtRQUNqQyxJQUFJLHdCQUF3QixDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDdkMsc0JBQXNCLEdBQUcsSUFBSSxDQUFDLGdDQUFnQyxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUN6RjtRQUNELElBQUksd0JBQXdCLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTs7a0JBQ2pDLG1CQUFtQixHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsd0JBQXdCLEVBQUUsc0JBQXNCLENBQUM7WUFDOUYsT0FBTyxtQkFBbUIsQ0FBQztTQUM1QjthQUFNO1lBQ0wsT0FBTyxJQUFJLENBQUM7U0FDYjtJQUNILENBQUM7Ozs7O0lBRU0sZUFBZSxDQUFDLE1BQWM7O2NBQzdCLGNBQWMsR0FBRyxJQUFJLENBQUMsd0JBQXdCLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQzs7Y0FDL0QsS0FBSyxHQUFHLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxNQUFNLENBQUM7O2NBQzNDLEtBQUssR0FBRyxJQUFJLENBQUMsc0JBQXNCLENBQUMsTUFBTSxDQUFDOztjQUMzQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLGNBQWMsRUFBRSxFQUFDLEtBQUssRUFBRSxLQUFLLEVBQUMsQ0FBQztRQUNyRSxPQUFPLGlCQUFpQixDQUFDO0lBQzNCLENBQUM7Ozs7Ozs7SUFFUyxXQUFXLENBQUMsV0FBcUIsRUFBRSxJQUFJLEdBQUcsR0FBRzs7Y0FDL0MsbUJBQW1CLEdBQUcsV0FBVyxDQUFDLE1BQU07Ozs7UUFBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxJQUFJLElBQUksRUFBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7UUFDckYsT0FBTyxtQkFBbUIsQ0FBQztJQUM3QixDQUFDOzs7Ozs7SUFFUyxzQkFBc0IsQ0FBQyxNQUFjO1FBQzdDLE9BQU8sTUFBTSxDQUFDLEtBQUssQ0FBQztJQUN0QixDQUFDOzs7Ozs7SUFFUyxzQkFBc0IsQ0FBQyxNQUFjO1FBQzdDLE9BQU8sTUFBTSxDQUFDLEtBQUssQ0FBQztJQUN0QixDQUFDOzs7Ozs7SUFJUyx3QkFBd0IsQ0FBQyxRQUFrQjtRQUNuRCxPQUFPLFFBQVEsQ0FBQyxLQUFLLENBQUM7SUFDeEIsQ0FBQzs7Ozs7SUFLRCxJQUFjLGtCQUFrQjtRQUM5QixJQUFJLElBQUksQ0FBQyxlQUFlLEVBQUU7WUFDeEIsT0FBTyxVQUFVLENBQUMsY0FBYyxDQUFDO1NBQ2xDO2FBQU07WUFDTCxJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUU7Z0JBQ3ZCLE9BQU8sVUFBVSxDQUFDLE1BQU0sQ0FBQzthQUMxQjtTQUNGO0lBQ0gsQ0FBQzs7Ozs7SUFFTSxpQkFBaUIsQ0FBQyxzQkFBOEM7O2NBQy9ELEtBQUssR0FBRyxJQUFJLENBQUMsd0JBQXdCLENBQUMsc0JBQXNCLENBQUMsUUFBUSxDQUFDO1FBQzVFLElBQUksc0JBQXNCLENBQUMsUUFBUSxDQUFDLE1BQU0sSUFBSSxJQUFJLEVBQUU7WUFDbEQsc0JBQXNCLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxFQUFDLElBQUksRUFBRSxJQUFJLENBQUMsa0JBQWtCLEVBQUMsQ0FBQztTQUMxRTtRQUNELElBQUksc0JBQXNCLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxJQUFJLElBQUksSUFBSSxFQUFFO1lBQ3ZELDZGQUE2RjtZQUM3RixzQkFBc0IsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUM7U0FDdkU7UUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUM7WUFDaEQsQ0FBQyxzQkFBc0IsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLElBQUksS0FBSyxVQUFVLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyx1QkFBdUIsSUFBSSxJQUFJLENBQUMsRUFBRTtZQUM3Ryx5RUFBeUU7WUFDekUsSUFBSSxzQkFBc0IsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLElBQUksSUFBSTtnQkFDOUQsc0JBQXNCLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQUU7Z0JBQ2xFLE1BQU0sSUFBSSxLQUFLLENBQUMsOERBQThELENBQUMsQ0FBQzthQUNqRjs7a0JBQ0ssUUFBUSxHQUFHLHNCQUFzQixDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUN2RSxzQkFBc0IsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyw0QkFBNEI7O2tCQUN0RixPQUFPLEdBQUcsc0JBQXNCLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEdBQUc7Ozs7WUFBQyxLQUFLLENBQUMsRUFBRTs7c0JBQ3pFLE1BQU0sR0FBRyxtQkFBQSxFQUFDLEtBQUssRUFBRSxRQUFRLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBQyxFQUFVO2dCQUN4RCxPQUFPLE1BQU0sQ0FBQztZQUNoQixDQUFDLEVBQUM7O2tCQUNJLGNBQWMsR0FBRyxtQkFBQSxFQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsb0NBQW9DLEVBQUUsT0FBTyxFQUFDLEVBQWtCO1lBQ3ZHLHNCQUFzQixDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBQzVELE9BQU8sSUFBSSxDQUFDO1NBQ2I7YUFBTTtZQUNMLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFFO2dCQUN6QixNQUFNLElBQUksS0FBSyxDQUFDLHlDQUF5QyxDQUFDLENBQUM7YUFDNUQ7O2tCQUNLLFFBQVEsR0FBRyxJQUFJLENBQUMsdUJBQXVCOztrQkFDdkMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsRUFBQyxRQUFRLEVBQUUsS0FBSyxFQUFDLENBQUM7WUFDcEUsT0FBTyxtQkFBbUIsQ0FBQztTQUM1QjtJQUNILENBQUM7Ozs7O0lBRUQsU0FBUyxDQUFDLFVBQWtDOztjQUNwQyxhQUFhLEdBQWEsRUFBRTtRQUVsQyxJQUFJLFVBQVUsQ0FBQyxRQUFRLEVBQUU7O2tCQUNqQixzQkFBc0IsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDO1lBQ2pFLGFBQWEsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsQ0FBQztTQUM1QztRQUVELElBQUksVUFBVSxDQUFDLE9BQU8sRUFBRTs7a0JBQ2hCLHNCQUFzQixHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLENBQUM7WUFDaEUsYUFBYSxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO1NBQzVDO1FBRUQsSUFBSSxVQUFVLENBQUMsU0FBUyxFQUFFOztrQkFDbEIsdUJBQXVCLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFVBQVUsQ0FBQztZQUNuRSxhQUFhLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLENBQUM7U0FDN0M7UUFFRCxJQUFJLFVBQVUsQ0FBQyxVQUFVLEVBQUU7O2tCQUNuQix1QkFBdUIsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsVUFBVSxDQUFDO1lBQ25FLGFBQWEsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsQ0FBQztTQUM3QztRQUVELElBQUksSUFBSSxDQUFDLG9CQUFvQixFQUFFOztrQkFDdkIsb0JBQW9CLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFVBQVUsQ0FBQztZQUNsRSxhQUFhLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUM7U0FDMUM7O2NBRUssc0JBQXNCLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUM7UUFDOUQsT0FBTyxzQkFBc0IsQ0FBQztJQUNoQyxDQUFDOzs7OztJQUlELGtCQUFrQixDQUFDLHNCQUE4QztRQUMvRCxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFFO1lBQzFCLE1BQU0sSUFBSSxLQUFLLENBQUMsMENBQTBDLENBQUMsQ0FBQztTQUM3RDs7Y0FFSyxhQUFhLEdBQWEsRUFBRTs7Y0FFNUIsOEJBQThCLEdBQUcsSUFBSSxDQUFDLDhCQUE4Qjs7Y0FDcEUsMkJBQTJCLEdBQUc7WUFDbEMsTUFBTSxFQUFFLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxzQkFBc0IsQ0FBQyxTQUFTLENBQUM7U0FDMUU7O2NBQ0ssMEJBQTBCLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyw4QkFBOEIsRUFBRSwyQkFBMkIsQ0FBQztRQUMzRyxhQUFhLENBQUMsSUFBSSxDQUFDLDBCQUEwQixDQUFDLENBQUM7O2NBRXpDLHNDQUFzQyxHQUFHLElBQUksQ0FBQyxzQ0FBc0M7O2NBQ3BGLG1DQUFtQyxHQUFHO1lBQzFDLGNBQWMsRUFBRSxJQUFJLENBQUMsa0NBQWtDLENBQUMsc0JBQXNCLENBQUMsU0FBUyxDQUFDO1NBQzFGOztjQUNLLGtDQUFrQyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsc0NBQXNDLEVBQUUsbUNBQW1DLENBQUM7UUFDbkksYUFBYSxDQUFDLElBQUksQ0FBQyxrQ0FBa0MsQ0FBQyxDQUFDOztjQUVqRCxzQkFBc0IsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQztRQUM5RCxPQUFPLHNCQUFzQixDQUFDO0lBQ2hDLENBQUM7Ozs7OztJQUVTLDBCQUEwQixDQUFDLFNBQW9CO1FBQ3ZELE9BQU8sU0FBUyxDQUFDLE1BQU0sQ0FBQztJQUMxQixDQUFDOzs7Ozs7SUFFUyxrQ0FBa0MsQ0FBQyxTQUFvQjtRQUMvRCxPQUFPLFNBQVMsQ0FBQyxjQUFjLENBQUM7SUFDbEMsQ0FBQzs7Ozs7SUFFRCxrQkFBa0IsQ0FBQyxzQkFBOEM7UUFDL0QsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtZQUMxQixNQUFNLElBQUksS0FBSyxDQUFDLDBDQUEwQyxDQUFDLENBQUM7U0FDN0Q7OztjQUVLLGNBQWMsR0FBRyxzQkFBc0IsQ0FBQyxVQUFVLENBQUMsR0FBRzs7OztRQUFDLFNBQVMsQ0FBQyxFQUFFOztrQkFDakUsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLGdCQUFnQjs7O2tCQUN4QyxxQkFBcUIsR0FBRyxtQkFBQTtnQkFDNUIsS0FBSyxFQUFFLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxTQUFTLENBQUM7Z0JBQ2hELE9BQU8sRUFBRSxJQUFJLENBQUMsMkJBQTJCLENBQUMsU0FBUyxDQUFDO2FBQ3JELEVBQWE7O2tCQUNSLHNCQUFzQixHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLEVBQUUscUJBQXFCLENBQUM7WUFDbkYsT0FBTyxzQkFBc0IsQ0FBQztRQUNoQyxDQUFDLEVBQUM7O2NBRUksbUJBQW1CLEdBQUcsSUFBSSxDQUFDLG1CQUFtQjs7Y0FDOUMsc0JBQXNCLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLEVBQUUsbUJBQW1CLENBQUM7O2NBRTlFLGlCQUFpQixHQUFHLElBQUksQ0FBQyx3QkFBd0I7OztjQUNqRCxpQkFBaUIsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLGlCQUFpQixFQUFFLEVBQUMsVUFBVSxFQUFFLHNCQUFzQixFQUFDLENBQUM7UUFFOUYsT0FBTyxpQkFBaUIsQ0FBQztJQUMzQixDQUFDOzs7Ozs7SUFFUyx5QkFBeUIsQ0FBQyxTQUFvQjtRQUN0RCxPQUFPLFNBQVMsQ0FBQyxLQUFLLENBQUM7SUFDekIsQ0FBQzs7Ozs7O0lBRVMsMkJBQTJCLENBQUMsU0FBb0I7UUFDeEQsT0FBTyxTQUFTLENBQUMsT0FBTyxDQUFDO0lBQzNCLENBQUM7Ozs7Ozs7SUFFUyxNQUFNLENBQUMsT0FBZSxFQUFFLEdBQUcsSUFBUztRQUM1QyxJQUFJLE9BQU8sSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLFFBQVEsRUFBRTtZQUMvQixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsUUFBUTs7OztZQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUU7O3NCQUMvQixLQUFLLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQyxDQUFDO2dCQUMxQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3pDLENBQUMsRUFBQyxDQUFDO1NBQ0o7YUFBTTs7a0JBQ0MsR0FBRyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUM7WUFFbkIsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLFFBQVE7Ozs7WUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFOztzQkFDL0IsR0FBRyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FBQztnQkFDakMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDbEQsQ0FBQyxFQUFDLENBQUM7U0FDSjtJQUNILENBQUM7Q0FDRjs7Ozs7Ozs7Ozs7O0lBalNDLHNEQUFpRDs7Ozs7Ozs7O0lBTWpELDhDQUF5Qzs7Ozs7OztJQUt6QyxnREFBNEM7Ozs7Ozs7SUFJNUMsK0NBQTJDOzs7Ozs7O0lBSTNDLGlEQUE2Qzs7Ozs7OztJQUk3QyxpREFBNkM7Ozs7Ozs7Ozs7O0lBUTdDLCtEQUEwRDs7Ozs7Ozs7O0lBTTFELHVFQUFrRTs7Ozs7Ozs7Ozs7SUFRbEUseURBQW9EOzs7Ozs7Ozs7SUFNcEQsaURBQTRDOzs7Ozs7O0lBSTVDLG9EQUErQzs7Ozs7SUFFL0MsdUVBQTJFOzs7OztJQWdFM0Usd0RBQTJDOzs7OztJQU0zQyw2REFBK0U7Ozs7O0lBQy9FLHFFQUFtRzs7Ozs7OztJQTNJbkcseUZBQW9GOzs7Ozs7O0lBRXBGLHlHQUE0RyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SW5qZWN0aW9uVG9rZW59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtGaWx0cm9Db21wb3N0bywgRmlsdHJvLCBGaWx0cm9PcGVyYWRvciwgaXNGaWx0ZXJDb21wb3NpdGUsIEZpbHRyb0NvbXBvc3RvT3BlcmFkb3J9IGZyb20gJy4uL2ZpbHRlcic7XG5pbXBvcnQge1BhcmFtZXRyb3NEYVJlcXVpc2ljYW99IGZyb20gJy4uL3JlcXVlc3QtcGFyYW1ldGVycyc7XG5pbXBvcnQge1NlcmlhbGl6ZXJ9IGZyb20gJy4vc2VyaWFsaXplcic7XG5pbXBvcnQge09yZGVuYWNhb30gZnJvbSAnLi4vc29ydCc7XG5pbXBvcnQge1BhZ2luYWNhb30gZnJvbSAnLi4vcGFnZSc7XG5pbXBvcnQge1NlYXJjaE1vZGUsIFBlc3F1aXNhfSBmcm9tICcuLi9wZXNxdWlzYSc7XG5cbmV4cG9ydCBjb25zdCBTRVJJQUxJWkVSX1RPS0VOID0gbmV3IEluamVjdGlvblRva2VuPFNlcmlhbGl6ZXI+KCdTZXJpYWxpemVyJyk7XG5cbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBCYXNlU2VyaWFsaXplclNlcnZpY2UgaW1wbGVtZW50cyBTZXJpYWxpemVyIHtcbiAgcHJvdGVjdGVkIGFic3RyYWN0IGdldEZpbHRyb09wZXJhZG9yTWFwZWFkbyhmaWx0cm9PcGVyYWRvcjogRmlsdHJvT3BlcmFkb3IpOiBzdHJpbmc7XG5cbiAgcHJvdGVjdGVkIGFic3RyYWN0IGdldEZpbHRyb0NvbXBvc3RvT3BlcmFkb3JNYXBlYWRvKEZpbHRyb0NvbXBvc3RvT3BlcmFkb3I6IEZpbHRyb0NvbXBvc3RvT3BlcmFkb3IpOiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIEZvcm1hdG8gcXVlIHNlcmEgc2VyaWFsaXphZG8gYVxuICAgKiBUYWcgY29ycmVzcG9uZGVudGUgYSBxdWVyeSBzdHJpbmdcbiAgICogZW52aWFkYSBuYSByZXF1aXppw6fDo28gZGEgcGVzcXVpc2EuXG4gICAqXG4gICAqIEEgcGFsYXZyYSBjaGF2ZSBwYXJhIHN1YnN0aXR1acOnw6NvIHBlbG8gZmlsdHJvIMOpIGB7ZmlsdHJvc31gXG4gICAqICovXG4gIHByb3RlY3RlZCBhYnN0cmFjdCBmaWx0cm9DbGF1c3VsYUZvcm1hdG86IHN0cmluZztcbiAgLyoqXG4gICAqICBDb3JyZXNwb25kZSBhIHRhZyBxdWUgc2VyYSBhcGxpY2FkYSBuYSBmb3JtYXRhw6fDo28gZG9zIGZpbHRyb3MuXG4gICAqXG4gICAqICBBIHBhbGF2cmEgY2hhdmUgcGFyYSBzdWJzdGl0dWnDp8OjbyBwZWxvIGZpbHRybyDDqSBge2ZpbHRyb31gXG4gICAqICovXG4gIHByb3RlY3RlZCBhYnN0cmFjdCBmaWx0cm9Gb3JtYXRvOiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIEhhYmlsaXRhIHN1cG9ydGUgcGFyYSBtb2RvIGRlIHBlc3F1aXNhLlxuICAgKiAqL1xuICBwcm90ZWN0ZWQgYWJzdHJhY3Qgc3Vwb3J0YVBlc3F1aXNhOiBib29sZWFuO1xuICAvKipcbiAgICogSGFiaWxpdGEgc3Vwb3J0ZSBwYXJhIG1vZG8gZGUgcGVzcXVpc2EgdXRpbGl6YW5kbyBhIGzDs2dpY2EgY29tIGZpbHRyb3MuXG4gICAqICovXG4gIHByb3RlY3RlZCBhYnN0cmFjdCBzdXBvcnRhRmlsdHJvczogYm9vbGVhbjtcbiAgLyoqXG4gICAqICBIYWJpbGl0YSBzdXBvcnRlIHBhcmEgbW9kbyBkZSBwZXNxdWlzYSB1dGlsaXphbmRvIGEgbMOzZ2ljYSBjb20gcGFnaW5hw6fDo28uXG4gICAqICovXG4gIHByb3RlY3RlZCBhYnN0cmFjdCBzdXBvcnRhUGFnaW5hY2FvOiBib29sZWFuO1xuICAvKipcbiAgICogSGFiaWxpdGEgc3Vwb3J0ZSBwYXJhIG1vZG8gZGUgcGVzcXVpc2EgdXRpbGl6YW5kbyBhIGzDs2dpY2EgY29tIG9yZGVuYcOnw6NvLlxuICAgKiAqL1xuICBwcm90ZWN0ZWQgYWJzdHJhY3Qgc3Vwb3J0YU9yZGVuYWNhbzogYm9vbGVhbjtcbiAgLyoqXG4gICAqIEZvcm1hdG8gcXVlIHNlcmEgc2VyaWFsaXphZG8gYVxuICAgKiBUYWcgY29ycmVzcG9uZGVudGUgYSBxdWVyeSBzdHJpbmcgJ3NraXAnXG4gICAqIGVudmlhZGEgbmEgcmVxdWlzacOnw6NvLlxuICAgKlxuICAgKiBBIHBhbGF2cmEgY2hhdmUgcGFyYSBzdWJzdGl0dWnDp8OjbyDDqSBge3BhZ2luYX1gXG4gICAqICovXG4gIHByb3RlY3RlZCBhYnN0cmFjdCBwYWdpbmFjYW9QYWdpbmFDbGF1c3VsYUZvcm1hdG86IHN0cmluZztcbiAgLyoqXG4gICAqICBDb3JyZXNwb25kZSBhIHRhZyBxdWUgc2VyYSBhcGxpY2FkYSBuYSBmb3JtYXRhw6fDo28gZGFzIGNsYXVzdWxhIGRvcyBpdGVucyBwb3IgcMOhZ2luYS5cbiAgICpcbiAgICogIEEgcGFsYXZyYSBjaGF2ZSBwYXJhIHN1YnN0aXR1acOnw6NvIMOpIGB7aXRlbnNQb3JQYWdpbmF9YFxuICAgKiAqL1xuICBwcm90ZWN0ZWQgYWJzdHJhY3QgcGFnaW5hY2FvSXRlbnNQb3JQYWdpbmFDbGF1c3VsYUZvcm1hdG86IHN0cmluZztcbiAgLyoqXG4gICAqIEZvcm1hdG8gcXVlIHNlcmEgc2VyaWFsaXphZG8gYVxuICAgKiBUYWcgY29ycmVzcG9uZGVudGUgYSBxdWVyeSBzdHJpbmdcbiAgICogZW52aWFkYSBuYSByZXF1aXppw6fDo28gZGEgb3JkZXJuYcOnw6NvIGRhIHBlc3F1aXNhLlxuICAgKlxuICAgKiBBIHBhbGF2cmEgY2hhdmUgcGFyYSBzdWJzdGl0dWnDp8OjbyDDqSBge29yZGVuYWNvZXN9YFxuICAgKiAqL1xuICBwcm90ZWN0ZWQgYWJzdHJhY3Qgb3JkZW5hY2FvQ2xhdXN1bGFGb3JtYXRvOiBzdHJpbmc7XG4gIC8qKlxuICAgKiAgQ29ycmVzcG9uZGUgYSB0YWcgcXVlIHNlcmEgYXBsaWNhZGEgbmEgZm9ybWF0YcOnw6NvIGRhcyBjbGF1c3VsYSBkZSBvcmRlbmHDp8Ojby5cbiAgICpcbiAgICogIEFzIHBhbGF2cmFzIGNoYXZlcyBwYXJhIHN1YnN0aXR1acOnw6NvIMOpIGB7Y2FtcG99YCBlIGB7ZGlyZWNhb31gXG4gICAqICovXG4gIHByb3RlY3RlZCBhYnN0cmFjdCBvcmRlbmFjYW9Gb3JtYXRvOiBzdHJpbmc7XG4gIC8qKlxuICAgKiAgQ29ycmVzcG9uZGUgYW8gc2VwYXJhZG9yIHF1ZSBzZXJhIGFwbGljYWRvIG5hIHNlcmlhbGl6YcOnw6NvIGRhcyBvcmRlbmHDp8O1ZXMuXG4gICAqICovXG4gIHByb3RlY3RlZCBhYnN0cmFjdCBvcmRlbmFjb2VzU2VwYXJhZG9yOiBzdHJpbmc7XG5cbiAgcHJvdGVjdGVkIHF1YW50aWRhZGVEZUZpbHRyb3NBbGluaGFkb3NTdXBvcnRhZG9zID0gTnVtYmVyLk1BWF9TQUZFX0lOVEVHRVI7XG5cbiAgcHVibGljIHNlcmlhbGl6ZUZpbHRyb3MocGFyYW1ldHJvc0RhUmVxdWlzaWNhbzogUGFyYW1ldHJvc0RhUmVxdWlzaWNhbyk6IHN0cmluZyB7XG4gICAgaWYgKCF0aGlzLnN1cG9ydGFGaWx0cm9zKSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ0VzdGUgc2VyaWFsaXphZG9yIG7Do28gc3Vwb3J0YSBmaWx0cm9zIScpO1xuICAgIH1cbiAgICBjb25zdCBmb3JtYXRvRGFDbGF1c3VsYSA9IHRoaXMuZmlsdHJvQ2xhdXN1bGFGb3JtYXRvO1xuICAgIGNvbnN0IGZpbHRyb3NDb21wb3N0b1NlcmlhbGl6YWRvID0gdGhpcy5zZXJpYWxpemVGaWx0cm9Db21wb3N0byhwYXJhbWV0cm9zRGFSZXF1aXNpY2FvLmZpbHRyb3MpO1xuICAgIGlmIChmaWx0cm9zQ29tcG9zdG9TZXJpYWxpemFkbyAhPSBudWxsKSB7XG4gICAgICBjb25zdCBmaWx0cm9zU2VyaWFsaXphZG8gPSB0aGlzLmZvcm1hdChmb3JtYXRvRGFDbGF1c3VsYSwge2ZpbHRyb3M6IGZpbHRyb3NDb21wb3N0b1NlcmlhbGl6YWRvfSk7XG4gICAgICByZXR1cm4gZmlsdHJvc1NlcmlhbGl6YWRvO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG4gIH1cblxuICBwdWJsaWMgc2VyaWFsaXplRmlsdHJvQ29tcG9zdG8oZmlsdHJvQ29tcG9zdG86IEZpbHRyb0NvbXBvc3RvLCBuaXZlbCA9IDEpOiBzdHJpbmcge1xuICAgIGlmIChuaXZlbCA+IHRoaXMucXVhbnRpZGFkZURlRmlsdHJvc0FsaW5oYWRvc1N1cG9ydGFkb3MpIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcihgQSBxdWFudGlkYWRlIGRlIGZpbHRyb3MgYWxpbmhhZG9zIHN1cG9ydGFkb3Mgw6kgJyR7dGhpcy5xdWFudGlkYWRlRGVGaWx0cm9zQWxpbmhhZG9zU3Vwb3J0YWRvc30nIGUgZm9pIGFsY2Fuw6dhZG8gbyBuw612ZWwgJyR7bml2ZWx9JyFgKTtcbiAgICB9XG4gICAgY29uc3QgZmlsdHJvc1NlcmlhbGl6YWRvc0FycmF5ID0gZmlsdHJvQ29tcG9zdG8uZmlsdHJvcy5tYXAoZmlsdHJvID0+IHtcbiAgICAgIGlmIChpc0ZpbHRlckNvbXBvc2l0ZShmaWx0cm8pKSB7XG4gICAgICAgIGNvbnN0IGZvcm1hdG9Eb3NGaWx0cm9zID0gdGhpcy5maWx0cm9Gb3JtYXRvO1xuICAgICAgICBjb25zdCBwcm94aW1vTml2ZWwgPSBuaXZlbCArIDE7XG4gICAgICAgIGNvbnN0IGZpbHRyb3NDb21wb3N0b1NlcmlhbGl6YWRvID0gdGhpcy5zZXJpYWxpemVGaWx0cm9Db21wb3N0byhmaWx0cm8sIHByb3hpbW9OaXZlbCk7XG4gICAgICAgIGNvbnN0IGZpbHRyb3NTZXJpYWxpemFkbyA9IHRoaXMuZm9ybWF0KGZvcm1hdG9Eb3NGaWx0cm9zLCB7ZmlsdHJvOiBmaWx0cm9zQ29tcG9zdG9TZXJpYWxpemFkb30pO1xuICAgICAgICByZXR1cm4gZmlsdHJvc1NlcmlhbGl6YWRvO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc2VyaWFsaXplRmlsdHJvKGZpbHRybyk7XG4gICAgICB9XG4gICAgfSk7XG4gICAgbGV0IGZpbHRyb0NvbXBvc3RvT3BlcmFkb3IgPSBudWxsO1xuICAgIGlmIChmaWx0cm9zU2VyaWFsaXphZG9zQXJyYXkubGVuZ3RoID4gMSkge1xuICAgICAgZmlsdHJvQ29tcG9zdG9PcGVyYWRvciA9IHRoaXMuZ2V0RmlsdHJvQ29tcG9zdG9PcGVyYWRvck1hcGVhZG8oZmlsdHJvQ29tcG9zdG8ub3BlcmFkb3IpO1xuICAgIH1cbiAgICBpZiAoZmlsdHJvc1NlcmlhbGl6YWRvc0FycmF5Lmxlbmd0aCA+IDApIHtcbiAgICAgIGNvbnN0IGZpbHRyb3NTZXJpYWxpemFkb3MgPSB0aGlzLnRyYXRhckFycmF5KGZpbHRyb3NTZXJpYWxpemFkb3NBcnJheSwgZmlsdHJvQ29tcG9zdG9PcGVyYWRvcik7XG4gICAgICByZXR1cm4gZmlsdHJvc1NlcmlhbGl6YWRvcztcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuICB9XG5cbiAgcHVibGljIHNlcmlhbGl6ZUZpbHRybyhmaWx0cm86IEZpbHRybyk6IHN0cmluZyB7XG4gICAgY29uc3QgZmlsdHJvT3BlcmFkb3IgPSB0aGlzLmdldEZpbHRyb09wZXJhZG9yTWFwZWFkbyhmaWx0cm8ub3BlcmFkb3IpO1xuICAgIGNvbnN0IGNhbXBvID0gdGhpcy5maWx0cm9DYW1wb0ludGVyY2VwdG9yKGZpbHRybyk7XG4gICAgY29uc3QgdmFsb3IgPSB0aGlzLmZpbHRyb1ZhbG9ySW50ZXJjZXB0b3IoZmlsdHJvKTtcbiAgICBjb25zdCBmaWx0cm9TZXJpYWxpemFkbyA9IHRoaXMuZm9ybWF0KGZpbHRyb09wZXJhZG9yLCB7Y2FtcG8sIHZhbG9yfSk7XG4gICAgcmV0dXJuIGZpbHRyb1NlcmlhbGl6YWRvO1xuICB9XG5cbiAgcHJvdGVjdGVkIHRyYXRhckFycmF5KHN0cmluZ0FycmF5OiBzdHJpbmdbXSwgam9pbiA9ICcmJykge1xuICAgIGNvbnN0IGZpbHRyb3NTZXJpYWxpemFkb3MgPSBzdHJpbmdBcnJheS5maWx0ZXIocyA9PiBzICE9PSAnJyAmJiBzICE9IG51bGwpLmpvaW4oam9pbik7XG4gICAgcmV0dXJuIGZpbHRyb3NTZXJpYWxpemFkb3M7XG4gIH1cblxuICBwcm90ZWN0ZWQgZmlsdHJvVmFsb3JJbnRlcmNlcHRvcihmaWx0cm86IEZpbHRybyk6IHN0cmluZyB7XG4gICAgcmV0dXJuIGZpbHRyby52YWxvcjtcbiAgfVxuXG4gIHByb3RlY3RlZCBmaWx0cm9DYW1wb0ludGVyY2VwdG9yKGZpbHRybzogRmlsdHJvKTogc3RyaW5nIHtcbiAgICByZXR1cm4gZmlsdHJvLmNhbXBvO1xuICB9XG5cbiAgcHJvdGVjdGVkIHBlc3F1aXNhQ2xhdXN1bGFGb3JtYXRvPzogc3RyaW5nO1xuXG4gIHByb3RlY3RlZCBwZXNxdWlzYVRlcm1vSW50ZXJjZXB0b3IocGVzcXVpc2E6IFBlc3F1aXNhKTogc3RyaW5nIHtcbiAgICByZXR1cm4gcGVzcXVpc2EudGVybW87XG4gIH1cblxuICBwcm90ZWN0ZWQgcGVzcXVpc2FGaWx0cm9PcGVyYWRvclBhZHJhbzogRmlsdHJvT3BlcmFkb3IgPSBGaWx0cm9PcGVyYWRvci5Db250ZW07XG4gIHByb3RlY3RlZCBwZXNxdWlzYUZpbHRyb0NvbXBvc3RvT3BlcmFkb3JQYWRyYW86IEZpbHRyb0NvbXBvc3RvT3BlcmFkb3IgPSBGaWx0cm9Db21wb3N0b09wZXJhZG9yLk91O1xuXG4gIHByb3RlY3RlZCBnZXQgcGVzcXVpc2FNb2RvUGFkcmFvKCk6IFNlYXJjaE1vZGUge1xuICAgIGlmICh0aGlzLnN1cG9ydGFQZXNxdWlzYSkge1xuICAgICAgcmV0dXJuIFNlYXJjaE1vZGUuRnVsbFRleHRTZWFyY2g7XG4gICAgfSBlbHNlIHtcbiAgICAgIGlmICh0aGlzLnN1cG9ydGFGaWx0cm9zKSB7XG4gICAgICAgIHJldHVybiBTZWFyY2hNb2RlLkZpbHRybztcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBwdWJsaWMgc2VyaWFsaXplUGVzcXVpc2EocGFyYW1ldHJvc0RhUmVxdWlzaWNhbzogUGFyYW1ldHJvc0RhUmVxdWlzaWNhbyk6IHN0cmluZyB7XG4gICAgY29uc3QgdGVybW8gPSB0aGlzLnBlc3F1aXNhVGVybW9JbnRlcmNlcHRvcihwYXJhbWV0cm9zRGFSZXF1aXNpY2FvLnBlc3F1aXNhKTtcbiAgICBpZiAocGFyYW1ldHJvc0RhUmVxdWlzaWNhby5wZXNxdWlzYS5jb25maWcgPT0gbnVsbCkge1xuICAgICAgcGFyYW1ldHJvc0RhUmVxdWlzaWNhby5wZXNxdWlzYS5jb25maWcgPSB7bW9kbzogdGhpcy5wZXNxdWlzYU1vZG9QYWRyYW99O1xuICAgIH1cbiAgICBpZiAocGFyYW1ldHJvc0RhUmVxdWlzaWNhby5wZXNxdWlzYS5jb25maWcubW9kbyA9PSBudWxsKSB7XG4gICAgICAvLyBTZSBuw6NvIGluZm9ybWFyIGFzIGNvbmZpZ3VyYcOnw7VlcywgZGV2ZSBwcmV2YWxlY2VyIG8gcXVlIG8gc2VyaWFsaXphZG9yIGRlZmluaXUgY29tbyBwYWRyYW9cbiAgICAgIHBhcmFtZXRyb3NEYVJlcXVpc2ljYW8ucGVzcXVpc2EuY29uZmlnLm1vZG8gPSB0aGlzLnBlc3F1aXNhTW9kb1BhZHJhbztcbiAgICB9XG4gICAgaWYgKCh0aGlzLnN1cG9ydGFGaWx0cm9zICYmICF0aGlzLnN1cG9ydGFQZXNxdWlzYSkgJiZcbiAgICAgIChwYXJhbWV0cm9zRGFSZXF1aXNpY2FvLnBlc3F1aXNhLmNvbmZpZy5tb2RvID09PSBTZWFyY2hNb2RlLkZpbHRybyB8fCB0aGlzLnBlc3F1aXNhQ2xhdXN1bGFGb3JtYXRvID09IG51bGwpKSB7XG4gICAgICAvLyBFc3RlIMOpIG8gY2FzbyBxdWFuZG8gYSBwZXNxdWlzYSDDqSByZWFsaXphZGEgcG9yIG1laW8gZGUgZmlsdHJvIHNpbXBsZXNcbiAgICAgIGlmIChwYXJhbWV0cm9zRGFSZXF1aXNpY2FvLnBlc3F1aXNhLmNvbmZpZy5maWx0cm8uY2FtcG9zID09IG51bGwgfHxcbiAgICAgICAgcGFyYW1ldHJvc0RhUmVxdWlzaWNhby5wZXNxdWlzYS5jb25maWcuZmlsdHJvLmNhbXBvcy5sZW5ndGggPD0gMCkge1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ05hIHBlc3F1aXNhIGNvbSBtb2RvIEZpbHRybywgb3MgY2FtcG9zIGRldmVtIHNlciBpbmZvcm1hZG9zIScpO1xuICAgICAgfVxuICAgICAgY29uc3Qgb3BlcmFkb3IgPSBwYXJhbWV0cm9zRGFSZXF1aXNpY2FvLnBlc3F1aXNhLmNvbmZpZy5maWx0cm8ub3BlcmFkb3IgP1xuICAgICAgICBwYXJhbWV0cm9zRGFSZXF1aXNpY2FvLnBlc3F1aXNhLmNvbmZpZy5maWx0cm8ub3BlcmFkb3IgOiB0aGlzLnBlc3F1aXNhRmlsdHJvT3BlcmFkb3JQYWRyYW87XG4gICAgICBjb25zdCBmaWx0cm9zID0gcGFyYW1ldHJvc0RhUmVxdWlzaWNhby5wZXNxdWlzYS5jb25maWcuZmlsdHJvLmNhbXBvcy5tYXAoY2FtcG8gPT4ge1xuICAgICAgICBjb25zdCBmaWx0cm8gPSB7Y2FtcG8sIG9wZXJhZG9yLCB2YWxvcjogdGVybW99IGFzIEZpbHRybztcbiAgICAgICAgcmV0dXJuIGZpbHRybztcbiAgICAgIH0pO1xuICAgICAgY29uc3QgZmlsdHJvQ29tcG9zdG8gPSB7b3BlcmFkb3I6IHRoaXMucGVzcXVpc2FGaWx0cm9Db21wb3N0b09wZXJhZG9yUGFkcmFvLCBmaWx0cm9zfSBhcyBGaWx0cm9Db21wb3N0bztcbiAgICAgIHBhcmFtZXRyb3NEYVJlcXVpc2ljYW8uZmlsdHJvcy5maWx0cm9zLnB1c2goZmlsdHJvQ29tcG9zdG8pO1xuICAgICAgcmV0dXJuIG51bGw7XG4gICAgfSBlbHNlIHtcbiAgICAgIGlmICghdGhpcy5zdXBvcnRhUGVzcXVpc2EpIHtcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdFc3RlIHNlcmlhbGl6YWRvciBuw6NvIHN1cG9ydGEgcGVzcXVpc2EhJyk7XG4gICAgICB9XG4gICAgICBjb25zdCBjbGF1c3VsYSA9IHRoaXMucGVzcXVpc2FDbGF1c3VsYUZvcm1hdG87XG4gICAgICBjb25zdCBjbGF1c3VsYVNlcmlhbGl6YWRhID0gdGhpcy5mb3JtYXQoY2xhdXN1bGEsIHtwZXNxdWlzYTogdGVybW99KTtcbiAgICAgIHJldHVybiBjbGF1c3VsYVNlcmlhbGl6YWRhO1xuICAgIH1cbiAgfVxuXG4gIHNlcmlhbGl6ZShwYXJhbWV0cm9zOiBQYXJhbWV0cm9zRGFSZXF1aXNpY2FvKTogc3RyaW5nIHtcbiAgICBjb25zdCBzZXJpYWxpemF2ZWlzOiBzdHJpbmdbXSA9IFtdO1xuXG4gICAgaWYgKHBhcmFtZXRyb3MucGVzcXVpc2EpIHtcbiAgICAgIGNvbnN0IHNlcmlhbGl6YWNhb0RhUGVzcXVpc2EgPSB0aGlzLnNlcmlhbGl6ZVBlc3F1aXNhKHBhcmFtZXRyb3MpO1xuICAgICAgc2VyaWFsaXphdmVpcy5wdXNoKHNlcmlhbGl6YWNhb0RhUGVzcXVpc2EpO1xuICAgIH1cblxuICAgIGlmIChwYXJhbWV0cm9zLmZpbHRyb3MpIHtcbiAgICAgIGNvbnN0IHNlcmlhbGl6YWNhb0Rvc0ZpbHRyb3MgPSB0aGlzLnNlcmlhbGl6ZUZpbHRyb3MocGFyYW1ldHJvcyk7XG4gICAgICBzZXJpYWxpemF2ZWlzLnB1c2goc2VyaWFsaXphY2FvRG9zRmlsdHJvcyk7XG4gICAgfVxuXG4gICAgaWYgKHBhcmFtZXRyb3MucGFnaW5hY2FvKSB7XG4gICAgICBjb25zdCBzZXJpYWxpemFjYW9EYVBhZ2luYWNhbyA9IHRoaXMuc2VyaWFsaXplUGFnaW5hY2FvKHBhcmFtZXRyb3MpO1xuICAgICAgc2VyaWFsaXphdmVpcy5wdXNoKHNlcmlhbGl6YWNhb0RhUGFnaW5hY2FvKTtcbiAgICB9XG5cbiAgICBpZiAocGFyYW1ldHJvcy5vcmRlbmFjb2VzKSB7XG4gICAgICBjb25zdCBzZXJpYWxpemFjYW9EYU9yZGVuYWNhbyA9IHRoaXMuc2VyaWFsaXplT3JkZW5hY2FvKHBhcmFtZXRyb3MpO1xuICAgICAgc2VyaWFsaXphdmVpcy5wdXNoKHNlcmlhbGl6YWNhb0RhT3JkZW5hY2FvKTtcbiAgICB9XG5cbiAgICBpZiAodGhpcy5zZXJpYWxpemVJbnRlcmNlcHRvcikge1xuICAgICAgY29uc3QgcGFyYW1ldHJvU2VyaWFsaXphZG8gPSB0aGlzLnNlcmlhbGl6ZUludGVyY2VwdG9yKHBhcmFtZXRyb3MpO1xuICAgICAgc2VyaWFsaXphdmVpcy5wdXNoKHBhcmFtZXRyb1NlcmlhbGl6YWRvKTtcbiAgICB9XG5cbiAgICBjb25zdCBwYXJhbWV0cm9zU2VyaWFsaXphZG9zID0gdGhpcy50cmF0YXJBcnJheShzZXJpYWxpemF2ZWlzKTtcbiAgICByZXR1cm4gcGFyYW1ldHJvc1NlcmlhbGl6YWRvcztcbiAgfVxuXG4gIHByb3RlY3RlZCBzZXJpYWxpemVJbnRlcmNlcHRvcj8ocGFyYW1ldHJvczogUGFyYW1ldHJvc0RhUmVxdWlzaWNhbyk6IHN0cmluZztcblxuICBzZXJpYWxpemVQYWdpbmFjYW8ocGFyYW1ldHJvc0RhUmVxdWlzaWNhbzogUGFyYW1ldHJvc0RhUmVxdWlzaWNhbyk6IHN0cmluZyB7XG4gICAgaWYgKCF0aGlzLnN1cG9ydGFQYWdpbmFjYW8pIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcignRXN0ZSBzZXJpYWxpemFkb3IgbsOjbyBzdXBvcnRhIHBhZ2luYWNhbyEnKTtcbiAgICB9XG5cbiAgICBjb25zdCBzZXJpYWxpemF2ZWlzOiBzdHJpbmdbXSA9IFtdO1xuXG4gICAgY29uc3QgcGFnaW5hY2FvUGFnaW5hQ2xhdXN1bGFGb3JtYXRvID0gdGhpcy5wYWdpbmFjYW9QYWdpbmFDbGF1c3VsYUZvcm1hdG87XG4gICAgY29uc3QgcGFnaW5hY2FvUGFnaW5hUGFyYUZvcm1hdGFyID0ge1xuICAgICAgcGFnaW5hOiB0aGlzLnBhZ2luYWNhb1BhZ2luYUludGVyY2VwdG9yKHBhcmFtZXRyb3NEYVJlcXVpc2ljYW8ucGFnaW5hY2FvKSxcbiAgICB9O1xuICAgIGNvbnN0IHBhZ2luYWNhb1BhZ2luYVNlcmlhbGl6YWRvID0gdGhpcy5mb3JtYXQocGFnaW5hY2FvUGFnaW5hQ2xhdXN1bGFGb3JtYXRvLCBwYWdpbmFjYW9QYWdpbmFQYXJhRm9ybWF0YXIpO1xuICAgIHNlcmlhbGl6YXZlaXMucHVzaChwYWdpbmFjYW9QYWdpbmFTZXJpYWxpemFkbyk7XG5cbiAgICBjb25zdCBwYWdpbmFjYW9JdGVuc1BvclBhZ2luYUNsYXVzdWxhRm9ybWF0byA9IHRoaXMucGFnaW5hY2FvSXRlbnNQb3JQYWdpbmFDbGF1c3VsYUZvcm1hdG87XG4gICAgY29uc3QgcGFnaW5hY2FvSXRlbnNQb3JQYWdpbmFQYXJhRm9ybWF0YXIgPSB7XG4gICAgICBpdGVuc1BvclBhZ2luYTogdGhpcy5wYWdpbmFjYW9JdGVuc1BvclBhZ2luYUludGVyY2VwdG9yKHBhcmFtZXRyb3NEYVJlcXVpc2ljYW8ucGFnaW5hY2FvKSxcbiAgICB9O1xuICAgIGNvbnN0IHBhZ2luYWNhb0l0ZW5zUG9yUGFnaW5hU2VyaWFsaXphZG8gPSB0aGlzLmZvcm1hdChwYWdpbmFjYW9JdGVuc1BvclBhZ2luYUNsYXVzdWxhRm9ybWF0bywgcGFnaW5hY2FvSXRlbnNQb3JQYWdpbmFQYXJhRm9ybWF0YXIpO1xuICAgIHNlcmlhbGl6YXZlaXMucHVzaChwYWdpbmFjYW9JdGVuc1BvclBhZ2luYVNlcmlhbGl6YWRvKTtcblxuICAgIGNvbnN0IHBhcmFtZXRyb3NTZXJpYWxpemFkb3MgPSB0aGlzLnRyYXRhckFycmF5KHNlcmlhbGl6YXZlaXMpO1xuICAgIHJldHVybiBwYXJhbWV0cm9zU2VyaWFsaXphZG9zO1xuICB9XG5cbiAgcHJvdGVjdGVkIHBhZ2luYWNhb1BhZ2luYUludGVyY2VwdG9yKHBhZ2luYWNhbzogUGFnaW5hY2FvKSB7XG4gICAgcmV0dXJuIHBhZ2luYWNhby5wYWdpbmE7XG4gIH1cblxuICBwcm90ZWN0ZWQgcGFnaW5hY2FvSXRlbnNQb3JQYWdpbmFJbnRlcmNlcHRvcihwYWdpbmFjYW86IFBhZ2luYWNhbykge1xuICAgIHJldHVybiBwYWdpbmFjYW8uaXRlbnNQb3JQYWdpbmE7XG4gIH1cblxuICBzZXJpYWxpemVPcmRlbmFjYW8ocGFyYW1ldHJvc0RhUmVxdWlzaWNhbzogUGFyYW1ldHJvc0RhUmVxdWlzaWNhbyk6IHN0cmluZyB7XG4gICAgaWYgKCF0aGlzLnN1cG9ydGFPcmRlbmFjYW8pIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcignRXN0ZSBzZXJpYWxpemFkb3IgbsOjbyBzdXBvcnRhIG9yZGVuYWNhbyEnKTtcbiAgICB9XG4gICAgLy8gTmFtZSBhc2MsIERlc2NyaXB0aW9uIGRlc2NcbiAgICBjb25zdCBwYWdpbmFjYW9BcnJheSA9IHBhcmFtZXRyb3NEYVJlcXVpc2ljYW8ub3JkZW5hY29lcy5tYXAob3JkZW5hY2FvID0+IHtcbiAgICAgIGNvbnN0IG9yZGVuYWNhb0Zvcm1hdG8gPSB0aGlzLm9yZGVuYWNhb0Zvcm1hdG87IC8vIGB7Y2FtcG99IHtkaXJlY2FvfWA7XG4gICAgICBjb25zdCBvcmRlbmFjYW9QYXJhRm9ybWF0YXIgPSB7XG4gICAgICAgIGNhbXBvOiB0aGlzLm9yZGVuYWNhb0NhbXBvSW50ZXJjZXB0b3Iob3JkZW5hY2FvKSxcbiAgICAgICAgZGlyZWNhbzogdGhpcy5vcmRlbmFjYW9EaXJlY2FvSW50ZXJjZXB0b3Iob3JkZW5hY2FvKSxcbiAgICAgIH0gYXMgT3JkZW5hY2FvO1xuICAgICAgY29uc3Qgb3JkZW5hY29lc1NlcmlhbGl6YWRhcyA9IHRoaXMuZm9ybWF0KG9yZGVuYWNhb0Zvcm1hdG8sIG9yZGVuYWNhb1BhcmFGb3JtYXRhcik7XG4gICAgICByZXR1cm4gb3JkZW5hY29lc1NlcmlhbGl6YWRhcztcbiAgICB9KTtcblxuICAgIGNvbnN0IG9yZGVuYWNvZXNTZXBhcmFkb3IgPSB0aGlzLm9yZGVuYWNvZXNTZXBhcmFkb3I7XG4gICAgY29uc3QgcGFyYW1ldHJvc1NlcmlhbGl6YWRvcyA9IHRoaXMudHJhdGFyQXJyYXkocGFnaW5hY2FvQXJyYXksIG9yZGVuYWNvZXNTZXBhcmFkb3IpO1xuXG4gICAgY29uc3Qgb3JkZW5hY2FvQ2xhdXN1bGEgPSB0aGlzLm9yZGVuYWNhb0NsYXVzdWxhRm9ybWF0bzsgLy8gYCRvcmRlcmJ5PXtvcmRlbmFjb2VzfWA7XG4gICAgY29uc3QgY2xhdXN1bGFGb3JtYXRhZGEgPSB0aGlzLmZvcm1hdChvcmRlbmFjYW9DbGF1c3VsYSwge29yZGVuYWNvZXM6IHBhcmFtZXRyb3NTZXJpYWxpemFkb3N9KTtcblxuICAgIHJldHVybiBjbGF1c3VsYUZvcm1hdGFkYTtcbiAgfVxuXG4gIHByb3RlY3RlZCBvcmRlbmFjYW9DYW1wb0ludGVyY2VwdG9yKG9yZGVuYWNhbzogT3JkZW5hY2FvKSB7XG4gICAgcmV0dXJuIG9yZGVuYWNhby5jYW1wbztcbiAgfVxuXG4gIHByb3RlY3RlZCBvcmRlbmFjYW9EaXJlY2FvSW50ZXJjZXB0b3Iob3JkZW5hY2FvOiBPcmRlbmFjYW8pIHtcbiAgICByZXR1cm4gb3JkZW5hY2FvLmRpcmVjYW87XG4gIH1cblxuICBwcm90ZWN0ZWQgZm9ybWF0KGZvcm1hdG86IHN0cmluZywgLi4uYXJnczogYW55KSB7XG4gICAgaWYgKHR5cGVvZiBhcmdzWzBdICE9PSAnb2JqZWN0Jykge1xuICAgICAgcmV0dXJuIGZvcm1hdG8ucmVwbGFjZSgve1xcZCt9L2csIChtKSA9PiB7XG4gICAgICAgIGNvbnN0IGluZGV4ID0gTnVtYmVyKG0ucmVwbGFjZSgvXFxEL2csICcnKSk7XG4gICAgICAgIHJldHVybiAoYXJnc1tpbmRleF0gPyBhcmdzW2luZGV4XSA6IG0pO1xuICAgICAgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGNvbnN0IG9iaiA9IGFyZ3NbMF07XG5cbiAgICAgIHJldHVybiBmb3JtYXRvLnJlcGxhY2UoL3tcXHcrfS9nLCAobSkgPT4ge1xuICAgICAgICBjb25zdCBrZXkgPSBtLnJlcGxhY2UoL3t8fS9nLCAnJyk7XG4gICAgICAgIHJldHVybiAob2JqLmhhc093blByb3BlcnR5KGtleSkgPyBvYmpba2V5XSA6IG0pO1xuICAgICAgfSk7XG4gICAgfVxuICB9XG59XG5cblxuXG5cblxuIl19