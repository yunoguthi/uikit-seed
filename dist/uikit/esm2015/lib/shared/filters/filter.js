/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @param {?} possivelFiltroComposto
 * @return {?}
 */
export function isFilterComposite(possivelFiltroComposto) {
    return ((/** @type {?} */ (possivelFiltroComposto))).filtros !== undefined;
}
/** @enum {string} */
const FiltroCompostoOperador = {
    E: 'and',
    Ou: 'or',
};
export { FiltroCompostoOperador };
/**
 * @record
 */
export function FiltroComposto() { }
if (false) {
    /** @type {?} */
    FiltroComposto.prototype.operador;
    /** @type {?} */
    FiltroComposto.prototype.filtros;
}
/**
 * @record
 * @template TCampo
 */
export function Filtro() { }
if (false) {
    /** @type {?} */
    Filtro.prototype.campo;
    /** @type {?} */
    Filtro.prototype.operador;
    /** @type {?|undefined} */
    Filtro.prototype.valor;
}
/** @enum {string} */
const FiltroOperador = {
    Igual: 'Igual',
    Diferente: 'Diferente',
    ComecaCom: 'Começa com',
    TerminaCom: 'Termina com',
    Contem: 'Contém',
    NaoContem: 'Não contém',
    MaiorOuIgual: 'Maior ou igual',
    Maior: 'Maior',
    Menor: 'Menor',
    MenorOuIgual: 'Menor ou igual',
    Em: 'Em',
    NaoEm: 'Não em',
};
export { FiltroOperador };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsdGVyLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvZmlsdGVycy9maWx0ZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQSxNQUFNLFVBQVUsaUJBQWlCLENBQUMsc0JBQStDO0lBQy9FLE9BQU8sQ0FBQyxtQkFBQSxzQkFBc0IsRUFBa0IsQ0FBQyxDQUFDLE9BQU8sS0FBSyxTQUFTLENBQUM7QUFDMUUsQ0FBQzs7O0lBR0MsR0FBSSxLQUFLO0lBQ1QsSUFBSyxJQUFJOzs7Ozs7QUFHWCxvQ0FHQzs7O0lBRkMsa0NBQWlDOztJQUNqQyxpQ0FBd0M7Ozs7OztBQUcxQyw0QkFJQzs7O0lBSEMsdUJBQWM7O0lBQ2QsMEJBQXlCOztJQUN6Qix1QkFBZTs7OztJQUlmLE9BQVEsT0FBTztJQUNmLFdBQVksV0FBVztJQUN2QixXQUFZLFlBQVk7SUFDeEIsWUFBYSxhQUFhO0lBQzFCLFFBQVMsUUFBUTtJQUNqQixXQUFZLFlBQVk7SUFDeEIsY0FBZSxnQkFBZ0I7SUFDL0IsT0FBUSxPQUFPO0lBQ2YsT0FBUSxPQUFPO0lBQ2YsY0FBZSxnQkFBZ0I7SUFDL0IsSUFBSyxJQUFJO0lBQ1QsT0FBUSxRQUFRIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGZ1bmN0aW9uIGlzRmlsdGVyQ29tcG9zaXRlKHBvc3NpdmVsRmlsdHJvQ29tcG9zdG86IEZpbHRyb0NvbXBvc3RvIHwgRmlsdHJvKTogcG9zc2l2ZWxGaWx0cm9Db21wb3N0byBpcyBGaWx0cm9Db21wb3N0byB7XG4gIHJldHVybiAocG9zc2l2ZWxGaWx0cm9Db21wb3N0byBhcyBGaWx0cm9Db21wb3N0bykuZmlsdHJvcyAhPT0gdW5kZWZpbmVkO1xufVxuXG5leHBvcnQgZW51bSBGaWx0cm9Db21wb3N0b09wZXJhZG9yIHtcbiAgRSA9ICdhbmQnLFxuICBPdSA9ICdvcicsXG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgRmlsdHJvQ29tcG9zdG8ge1xuICBvcGVyYWRvcjogRmlsdHJvQ29tcG9zdG9PcGVyYWRvcjtcbiAgZmlsdHJvczogQXJyYXk8RmlsdHJvQ29tcG9zdG8gfCBGaWx0cm8+O1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIEZpbHRybzxUQ2FtcG8gPSBhbnk+IHtcbiAgY2FtcG86IHN0cmluZztcbiAgb3BlcmFkb3I6IEZpbHRyb09wZXJhZG9yO1xuICB2YWxvcj86IFRDYW1wbzsgLy8gcG9kZSBzZXIgbnVsbCAobm8gY2FzbyBkbyB0aXBvIGRvIGZpbHRybyBgY2FtcG8gZXEgbnVsbGApXG59XG5cbmV4cG9ydCBlbnVtIEZpbHRyb09wZXJhZG9yIHtcbiAgSWd1YWwgPSAnSWd1YWwnLFxuICBEaWZlcmVudGUgPSAnRGlmZXJlbnRlJyxcbiAgQ29tZWNhQ29tID0gJ0NvbWXDp2EgY29tJyxcbiAgVGVybWluYUNvbSA9ICdUZXJtaW5hIGNvbScsXG4gIENvbnRlbSA9ICdDb250w6ltJyxcbiAgTmFvQ29udGVtID0gJ07Do28gY29udMOpbScsXG4gIE1haW9yT3VJZ3VhbCA9ICdNYWlvciBvdSBpZ3VhbCcsXG4gIE1haW9yID0gJ01haW9yJyxcbiAgTWVub3IgPSAnTWVub3InLFxuICBNZW5vck91SWd1YWwgPSAnTWVub3Igb3UgaWd1YWwnLFxuICBFbSA9ICdFbScsXG4gIE5hb0VtID0gJ07Do28gZW0nXG59XG5cbi8vIGNvbnN0IGZpbHRyb3MgPSBbeyBjYW1wbzogJ25vbWUnLCBvcGVyYXRvcjogRmlsdGVyTG9naWNPcGVyYXRvci5JZ3VhbCwgdmFsdWU6ICdKb8OjbycgfV0gYXMgRmlsdGVyRGVzY3JpcHRvcltdO1xuLy8gY29uc3QgZmlsdHJvID0geyBvcGVyYXRvcjogJ2FuZCcsIGZpbHRlcnM6IGZpbHRyb3MgfSBhcyBGaWx0ZXJDb21wb3NpdGU7XG4iXX0=