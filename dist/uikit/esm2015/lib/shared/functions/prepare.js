/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { defer } from 'rxjs';
import { finalize } from 'rxjs/operators';
/**
 * @template T
 * @param {?} callback
 * @return {?}
 */
export function prepare(callback) {
    return (/**
     * @param {?} source
     * @return {?}
     */
    (source) => defer((/**
     * @return {?}
     */
    () => {
        callback();
        return source;
    })));
}
/**
 * @template T
 * @param {?} indicator
 * @return {?}
 */
export function indicate(indicator) {
    return (/**
     * @param {?} source
     * @return {?}
     */
    (source) => source.pipe(prepare((/**
     * @return {?}
     */
    () => indicator.next(true))), finalize((/**
     * @return {?}
     */
    () => indicator.next(false)))));
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJlcGFyZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2Z1bmN0aW9ucy9wcmVwYXJlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsS0FBSyxFQUFzQixNQUFNLE1BQU0sQ0FBQztBQUNoRCxPQUFPLEVBQUMsUUFBUSxFQUFDLE1BQU0sZ0JBQWdCLENBQUM7Ozs7OztBQUV4QyxNQUFNLFVBQVUsT0FBTyxDQUFJLFFBQW9CO0lBQzdDOzs7O0lBQU8sQ0FBQyxNQUFxQixFQUFpQixFQUFFLENBQUMsS0FBSzs7O0lBQUMsR0FBRyxFQUFFO1FBQzFELFFBQVEsRUFBRSxDQUFDO1FBQ1gsT0FBTyxNQUFNLENBQUM7SUFDaEIsQ0FBQyxFQUFDLEVBQUM7QUFDTCxDQUFDOzs7Ozs7QUFFRCxNQUFNLFVBQVUsUUFBUSxDQUFJLFNBQTJCO0lBQ3JEOzs7O0lBQU8sQ0FBQyxNQUFxQixFQUFpQixFQUFFLENBQUMsTUFBTSxDQUFDLElBQUksQ0FDMUQsT0FBTzs7O0lBQUMsR0FBRyxFQUFFLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBQyxFQUNuQyxRQUFROzs7SUFBQyxHQUFHLEVBQUUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFDLENBQ3RDLEVBQUM7QUFDSixDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtkZWZlciwgT2JzZXJ2YWJsZSwgU3ViamVjdH0gZnJvbSAncnhqcyc7XG5pbXBvcnQge2ZpbmFsaXplfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5cbmV4cG9ydCBmdW5jdGlvbiBwcmVwYXJlPFQ+KGNhbGxiYWNrOiAoKSA9PiB2b2lkKTogKHNvdXJjZTogT2JzZXJ2YWJsZTxUPikgPT4gT2JzZXJ2YWJsZTxUPiB7XG4gIHJldHVybiAoc291cmNlOiBPYnNlcnZhYmxlPFQ+KTogT2JzZXJ2YWJsZTxUPiA9PiBkZWZlcigoKSA9PiB7XG4gICAgY2FsbGJhY2soKTtcbiAgICByZXR1cm4gc291cmNlO1xuICB9KTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGluZGljYXRlPFQ+KGluZGljYXRvcjogU3ViamVjdDxib29sZWFuPik6IChzb3VyY2U6IE9ic2VydmFibGU8VD4pID0+IE9ic2VydmFibGU8VD4ge1xuICByZXR1cm4gKHNvdXJjZTogT2JzZXJ2YWJsZTxUPik6IE9ic2VydmFibGU8VD4gPT4gc291cmNlLnBpcGUoXG4gICAgcHJlcGFyZSgoKSA9PiBpbmRpY2F0b3IubmV4dCh0cnVlKSksXG4gICAgZmluYWxpemUoKCkgPT4gaW5kaWNhdG9yLm5leHQoZmFsc2UpKVxuICApO1xufVxuIl19