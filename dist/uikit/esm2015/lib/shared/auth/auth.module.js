/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { AuthService } from './auth.service';
import { AuthenticationGuardService } from './authorization/authentication-guard.service';
import { RouteAuthorizationGuardService } from './authorization/route-authorization-guard.service';
import { AuthorizationService } from './authorization/authorization.service';
import { UserService } from './authentication/user.service';
import { NgModule } from '@angular/core';
import { AuthenticationService } from './authentication/authentication.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { OidcAuthHttpInterceptor } from './authentication-oidc/auth-http.interceptor';
export class AuthModule {
}
AuthModule.decorators = [
    { type: NgModule, args: [{
                providers: [
                    AuthenticationGuardService,
                    AuthenticationService,
                    // ProviderUserTransformationService,
                    UserService,
                    AuthorizationService,
                    // AuthorizationGuardService,
                    RouteAuthorizationGuardService,
                    {
                        provide: HTTP_INTERCEPTORS,
                        useClass: OidcAuthHttpInterceptor,
                        multi: true,
                    },
                    AuthService
                ]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9hdXRoL2F1dGgubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDN0MsT0FBTyxFQUFFLDBCQUEwQixFQUFFLE1BQU0sOENBQThDLENBQUM7QUFDMUYsT0FBTyxFQUFFLDhCQUE4QixFQUFFLE1BQU0sbURBQW1ELENBQUM7QUFDbkcsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDN0UsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBQzVELE9BQU8sRUFBRSxRQUFRLEVBQTZCLE1BQU0sZUFBZSxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBRWhGLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBR3pELE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBd0J0RixNQUFNLE9BQU8sVUFBVTs7O1lBckJ0QixRQUFRLFNBQUM7Z0JBQ1IsU0FBUyxFQUFFO29CQUNULDBCQUEwQjtvQkFDMUIscUJBQXFCO29CQUNyQixxQ0FBcUM7b0JBQ3JDLFdBQVc7b0JBRVgsb0JBQW9CO29CQUNwQiw2QkFBNkI7b0JBQzdCLDhCQUE4QjtvQkFFOUI7d0JBQ0UsT0FBTyxFQUFFLGlCQUFpQjt3QkFDMUIsUUFBUSxFQUFFLHVCQUF1Qjt3QkFDakMsS0FBSyxFQUFFLElBQUk7cUJBQ1g7b0JBRUYsV0FBVztpQkFDWjthQUNGIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQXV0aFNlcnZpY2UgfSBmcm9tICcuL2F1dGguc2VydmljZSc7XG5pbXBvcnQgeyBBdXRoZW50aWNhdGlvbkd1YXJkU2VydmljZSB9IGZyb20gJy4vYXV0aG9yaXphdGlvbi9hdXRoZW50aWNhdGlvbi1ndWFyZC5zZXJ2aWNlJztcbmltcG9ydCB7IFJvdXRlQXV0aG9yaXphdGlvbkd1YXJkU2VydmljZSB9IGZyb20gJy4vYXV0aG9yaXphdGlvbi9yb3V0ZS1hdXRob3JpemF0aW9uLWd1YXJkLnNlcnZpY2UnO1xuaW1wb3J0IHsgQXV0aG9yaXphdGlvblNlcnZpY2UgfSBmcm9tICcuL2F1dGhvcml6YXRpb24vYXV0aG9yaXphdGlvbi5zZXJ2aWNlJztcbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSAnLi9hdXRoZW50aWNhdGlvbi91c2VyLnNlcnZpY2UnO1xuaW1wb3J0IHsgTmdNb2R1bGUsIFR5cGUsIE1vZHVsZVdpdGhQcm92aWRlcnMgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEF1dGhlbnRpY2F0aW9uU2VydmljZSB9IGZyb20gJy4vYXV0aGVudGljYXRpb24vYXV0aGVudGljYXRpb24uc2VydmljZSc7XG5pbXBvcnQgeyBBdXRob3JpemF0aW9uR3VhcmRTZXJ2aWNlIH0gZnJvbSAnLi9hdXRob3JpemF0aW9uL2F1dGhvcml6YXRpb24tZ3VhcmQuc2VydmljZSc7XG5pbXBvcnQgeyBIVFRQX0lOVEVSQ0VQVE9SUyB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IEF1dGhIdHRwSW50ZXJjZXB0b3IgfSBmcm9tICcuL2F1dGgtaHR0cC5pbnRlcmNlcHRvcic7XG5pbXBvcnQgeyBQcm92aWRlclVzZXJUcmFuc2Zvcm1hdGlvblNlcnZpY2UgfSBmcm9tICcuL2F1dGhlbnRpY2F0aW9uL3VzZXItdHJhbnNmb3JtYXRpb24uc2VydmljZSc7XG5pbXBvcnQgeyBPaWRjQXV0aEh0dHBJbnRlcmNlcHRvciB9IGZyb20gJy4vYXV0aGVudGljYXRpb24tb2lkYy9hdXRoLWh0dHAuaW50ZXJjZXB0b3InO1xuXG5cbkBOZ01vZHVsZSh7XG4gIHByb3ZpZGVyczogW1xuICAgIEF1dGhlbnRpY2F0aW9uR3VhcmRTZXJ2aWNlLFxuICAgIEF1dGhlbnRpY2F0aW9uU2VydmljZSxcbiAgICAvLyBQcm92aWRlclVzZXJUcmFuc2Zvcm1hdGlvblNlcnZpY2UsXG4gICAgVXNlclNlcnZpY2UsXG5cbiAgICBBdXRob3JpemF0aW9uU2VydmljZSxcbiAgICAvLyBBdXRob3JpemF0aW9uR3VhcmRTZXJ2aWNlLFxuICAgIFJvdXRlQXV0aG9yaXphdGlvbkd1YXJkU2VydmljZSxcblxuICAgIHtcbiAgICAgIHByb3ZpZGU6IEhUVFBfSU5URVJDRVBUT1JTLFxuICAgICAgdXNlQ2xhc3M6IE9pZGNBdXRoSHR0cEludGVyY2VwdG9yLFxuICAgICAgbXVsdGk6IHRydWUsXG4gICAgIH0sXG5cbiAgICBBdXRoU2VydmljZVxuICBdXG59XG4pXG5leHBvcnQgY2xhc3MgQXV0aE1vZHVsZSB7XG4gIC8vIHN0YXRpYyBmb3JSb290KFxuICAvLyAgIGF1dGhlbnRpY2F0aW9uUHJvdmlkZXJzOiAoYW55W10gfCBUeXBlPGFueT4gfCBNb2R1bGVXaXRoUHJvdmlkZXJzKVtdXG4gIC8vICk6IChhbnlbXSB8IFR5cGU8YW55PiB8IE1vZHVsZVdpdGhQcm92aWRlcnMpW10ge1xuICAvLyAgIGNvbnN0IG1vZHVsZUNvbmZpZyA9IFtcbiAgLy8gICAgIGF1dGhlbnRpY2F0aW9uUHJvdmlkZXJzLFxuICAvLyAgICAge1xuICAvLyAgICAgICBuZ01vZHVsZTogQXV0aE1vZHVsZSxcbiAgLy8gICAgICAgcHJvdmlkZXJzOiBbXG4gIC8vICAgICAgICAgQXV0aGVudGljYXRpb25HdWFyZFNlcnZpY2UsXG4gIC8vICAgICAgICAgQXV0aGVudGljYXRpb25TZXJ2aWNlLFxuICAvLyAgICAgICAgIElkZW50aXR5U2VydmljZSxcblxuICAvLyAgICAgICAgIEF1dGhvcml6YXRpb25TZXJ2aWNlLFxuICAvLyAgICAgICAgIEF1dGhvcml6YXRpb25HdWFyZFNlcnZpY2UsXG4gIC8vICAgICAgICAgUm91dGVBdXRob3JpemF0aW9uR3VhcmRTZXJ2aWNlLFxuICAvLyAgICAgICBdXG4gIC8vICAgICB9XG4gIC8vICAgXSBhcyAoYW55W10gfCBUeXBlPGFueT4gfCBNb2R1bGVXaXRoUHJvdmlkZXJzKVtdO1xuXG4gIC8vICAgcmV0dXJuIG1vZHVsZUNvbmZpZztcbiAgLy8gICAvLyBtb2R1bGVDb25maWcucHVzaChhdXRoZW50aWNhdGlvblByb3ZpZGVycyk7XG4gIC8vICAgLy8gLy9jb25zdCByZXRvcm5vQXRyaWJ1aWRvID0gT2JqZWN0LmFzc2lnbihtb2R1bGVDb25maWcsIGF1dGhlbnRpY2F0aW9uUHJvdmlkZXJzKTtcblxuICAvLyAgIC8vIHJldHVybiBtb2R1bGVDb25maWc7XG4gIC8vIH1cbn1cbiJdfQ==