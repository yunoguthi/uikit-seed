/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { AuthorizationGuardService } from './authorization-guard.service';
export class RouteAuthorizationGuardService extends AuthorizationGuardService {
    /**
     * @param {?} route
     * @param {?} state
     * @return {?}
     */
    canActivate(route, state) {
        return tslib_1.__awaiter(this, void 0, void 0, /** @this {!RouteAuthorizationGuardService} */ function* () {
            /** @type {?} */
            const permissionToCheck = route.data && (/** @type {?} */ (route.data.authorize));
            if (permissionToCheck) {
                console.log(`Verificando autorização para a rota '${state.url}'...`);
                /** @type {?} */
                const hasPermission = yield this.authorizationService.authorize(permissionToCheck.action, permissionToCheck.resource);
                console.log(`Autorização para a rota '${state.url}' foi '${(hasPermission ? 'concedida' : 'negada')}'!`);
                if (!hasPermission) {
                    return this.deny(state);
                }
                else {
                    return this.allow();
                }
            }
            else {
                throw new Error('Sem permissão para verificar!');
            }
        });
    }
}
RouteAuthorizationGuardService.decorators = [
    { type: Injectable }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicm91dGUtYXV0aG9yaXphdGlvbi1ndWFyZC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvYXV0aC9hdXRob3JpemF0aW9uL3JvdXRlLWF1dGhvcml6YXRpb24tZ3VhcmQuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFHM0MsT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0sK0JBQStCLENBQUM7QUFJMUUsTUFBTSxPQUFPLDhCQUErQixTQUFRLHlCQUF5Qjs7Ozs7O0lBQ3JFLFdBQVcsQ0FBQyxLQUE2QixFQUFFLEtBQTBCOzs7a0JBRW5FLGlCQUFpQixHQUFHLEtBQUssQ0FBQyxJQUFJLElBQUksbUJBQUEsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQXdDO1lBRXBHLElBQUksaUJBQWlCLEVBQUU7Z0JBQ3JCLE9BQU8sQ0FBQyxHQUFHLENBQUMsd0NBQXdDLEtBQUssQ0FBQyxHQUFHLE1BQU0sQ0FBQyxDQUFDOztzQkFDL0QsYUFBYSxHQUFHLE1BQU0sSUFBSSxDQUFDLG9CQUFvQixDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLEVBQUUsaUJBQWlCLENBQUMsUUFBUSxDQUFDO2dCQUNySCxPQUFPLENBQUMsR0FBRyxDQUFDLDRCQUE0QixLQUFLLENBQUMsR0FBRyxVQUFVLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDekcsSUFBSSxDQUFDLGFBQWEsRUFBRTtvQkFDbEIsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUN6QjtxQkFBTTtvQkFDTCxPQUFPLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztpQkFDckI7YUFDRjtpQkFBTTtnQkFDTCxNQUFNLElBQUksS0FBSyxDQUFDLCtCQUErQixDQUFDLENBQUM7YUFDakQ7UUFFSixDQUFDO0tBQUE7OztZQW5CRixVQUFVIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGVTbmFwc2hvdCwgUm91dGVyU3RhdGVTbmFwc2hvdCwgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IEF1dGhvcml6YXRpb25HdWFyZFNlcnZpY2UgfSBmcm9tICcuL2F1dGhvcml6YXRpb24tZ3VhcmQuc2VydmljZSc7XG5cblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIFJvdXRlQXV0aG9yaXphdGlvbkd1YXJkU2VydmljZSBleHRlbmRzIEF1dGhvcml6YXRpb25HdWFyZFNlcnZpY2Uge1xuICBhc3luYyBjYW5BY3RpdmF0ZShyb3V0ZTogQWN0aXZhdGVkUm91dGVTbmFwc2hvdCwgc3RhdGU6IFJvdXRlclN0YXRlU25hcHNob3QpOiBQcm9taXNlPGJvb2xlYW4+IHtcblxuICAgIGNvbnN0IHBlcm1pc3Npb25Ub0NoZWNrID0gcm91dGUuZGF0YSAmJiByb3V0ZS5kYXRhLmF1dGhvcml6ZSBhcyB7IGFjdGlvbjogc3RyaW5nLCByZXNvdXJjZTogc3RyaW5nIH07XG5cbiAgICBpZiAocGVybWlzc2lvblRvQ2hlY2spIHtcbiAgICAgIGNvbnNvbGUubG9nKGBWZXJpZmljYW5kbyBhdXRvcml6YcOnw6NvIHBhcmEgYSByb3RhICcke3N0YXRlLnVybH0nLi4uYCk7XG4gICAgICBjb25zdCBoYXNQZXJtaXNzaW9uID0gYXdhaXQgdGhpcy5hdXRob3JpemF0aW9uU2VydmljZS5hdXRob3JpemUocGVybWlzc2lvblRvQ2hlY2suYWN0aW9uLCBwZXJtaXNzaW9uVG9DaGVjay5yZXNvdXJjZSk7XG4gICAgICBjb25zb2xlLmxvZyhgQXV0b3JpemHDp8OjbyBwYXJhIGEgcm90YSAnJHtzdGF0ZS51cmx9JyBmb2kgJyR7KGhhc1Blcm1pc3Npb24gPyAnY29uY2VkaWRhJyA6ICduZWdhZGEnKX0nIWApO1xuICAgICAgaWYgKCFoYXNQZXJtaXNzaW9uKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmRlbnkoc3RhdGUpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuYWxsb3coKTtcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKCdTZW0gcGVybWlzc8OjbyBwYXJhIHZlcmlmaWNhciEnKTtcbiAgICAgfVxuXG4gIH1cbn1cbiJdfQ==