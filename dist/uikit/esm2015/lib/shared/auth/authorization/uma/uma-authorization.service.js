/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { AuthorizationConfigToken } from './../authorization-config.token';
import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { switchMap } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
import * as i2 from "../authorization-config.token";
export class UmaAuthorizationManager {
    /**
     * @param {?} httpClient
     * @param {?} authorizationConfig
     */
    constructor(httpClient, authorizationConfig) {
        this.httpClient = httpClient;
        this.authorizationConfig = authorizationConfig;
    }
    /**
     * @param {?} action
     * @param {?} resource
     * @return {?}
     */
    authorize(action, resource) {
        return tslib_1.__awaiter(this, void 0, void 0, /** @this {!UmaAuthorizationManager} */ function* () {
            // WSO2 - ${environment.settings.authentication.authority}api/identity/oauth2/uma/permission/v1.0/permission
            // KEYCLOAK - ${environment.settings.authentication.authority}authz/protection/permission
            return this.httpClient.post(`${this.authorizationConfig.umaConfig.permissionEndpoint}`, [
                {
                    resource_id: resource,
                    resource_scopes: [action]
                }
            ]).pipe(switchMap((/**
             * @param {?} result
             * @return {?}
             */
            (result) => {
                // WSO2 - `${environment.settings.authentication.authority}oauth2/token`,
                // KEYCLOAK - `${environment.settings.authentication.authority}protocol/openid-connect/token`,
                return this.httpClient.post(`${this.authorizationConfig.umaConfig.tokenEndpoint}`, new HttpParams()
                    .set('grant_type', 'urn:ietf:params:oauth:grant-type:uma-ticket')
                    // .set('audience', client)
                    .set('response_mode', 'decision')
                    .set('ticket', result.ticket)
                    .toString(), {
                    headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
                });
            }))).toPromise().then((/**
             * @param {?} result
             * @return {?}
             */
            (result) => {
                return result.result;
            }));
        });
    }
}
UmaAuthorizationManager.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
UmaAuthorizationManager.ctorParameters = () => [
    { type: HttpClient },
    { type: undefined, decorators: [{ type: Inject, args: [AuthorizationConfigToken,] }] }
];
/** @nocollapse */ UmaAuthorizationManager.ngInjectableDef = i0.defineInjectable({ factory: function UmaAuthorizationManager_Factory() { return new UmaAuthorizationManager(i0.inject(i1.HttpClient), i0.inject(i2.AuthorizationConfigToken)); }, token: UmaAuthorizationManager, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @protected
     */
    UmaAuthorizationManager.prototype.httpClient;
    /**
     * @type {?}
     * @protected
     */
    UmaAuthorizationManager.prototype.authorizationConfig;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidW1hLWF1dGhvcml6YXRpb24uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2F1dGgvYXV0aG9yaXphdGlvbi91bWEvdW1hLWF1dGhvcml6YXRpb24uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBdUIsd0JBQXdCLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQUNoRyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNuRCxPQUFPLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFBRSxVQUFVLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUMzRSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7Ozs7QUFRM0MsTUFBTSxPQUFPLHVCQUF1Qjs7Ozs7SUFDbEMsWUFDWSxVQUFzQixFQUNZLG1CQUF3QztRQUQxRSxlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ1ksd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtJQUV0RixDQUFDOzs7Ozs7SUFDWSxTQUFTLENBQUMsTUFBYyxFQUFFLFFBQWdCOztZQUVyRCw0R0FBNEc7WUFDNUcseUZBQXlGO1lBRXpGLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsU0FBUyxDQUFDLGtCQUFrQixFQUFFLEVBQ3BGO2dCQUNFO29CQUNFLFdBQVcsRUFBRSxRQUFRO29CQUNyQixlQUFlLEVBQUUsQ0FBRSxNQUFNLENBQUU7aUJBQzVCO2FBQ0YsQ0FDRixDQUFDLElBQUksQ0FBQyxTQUFTOzs7O1lBQUMsQ0FBQyxNQUEwQixFQUFFLEVBQUU7Z0JBRzlDLHlFQUF5RTtnQkFDekUsOEZBQThGO2dCQUU5RixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxhQUFhLEVBQUUsRUFDL0UsSUFBSSxVQUFVLEVBQUU7cUJBQ2YsR0FBRyxDQUFDLFlBQVksRUFBRSw2Q0FBNkMsQ0FBQztvQkFDakUsMkJBQTJCO3FCQUMxQixHQUFHLENBQUMsZUFBZSxFQUFFLFVBQVUsQ0FBQztxQkFDaEMsR0FBRyxDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDO3FCQUM1QixRQUFRLEVBQUUsRUFDWDtvQkFDRSxPQUFPLEVBQUUsSUFBSSxXQUFXLEVBQUUsQ0FBQyxHQUFHLENBQUMsY0FBYyxFQUFFLG1DQUFtQyxDQUFDO2lCQUNwRixDQUNGLENBQUM7WUFFSixDQUFDLEVBQUMsQ0FBQyxDQUFDLFNBQVMsRUFBRSxDQUFDLElBQUk7Ozs7WUFBQyxDQUFDLE1BQTJCLEVBQUUsRUFBRTtnQkFFbkQsT0FBTyxNQUFNLENBQUMsTUFBTSxDQUFDO1lBRXZCLENBQUMsRUFBQyxDQUFDO1FBRUwsQ0FBQztLQUFBOzs7WUE3Q0YsVUFBVSxTQUFDO2dCQUNWLFVBQVUsRUFBRSxNQUFNO2FBQ25COzs7O1lBUlEsVUFBVTs0Q0FZZCxNQUFNLFNBQUMsd0JBQXdCOzs7Ozs7OztJQURoQyw2Q0FBZ0M7Ozs7O0lBQ2hDLHNEQUFvRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEF1dGhvcml6YXRpb25Db25maWcsIEF1dGhvcml6YXRpb25Db25maWdUb2tlbiB9IGZyb20gJy4vLi4vYXV0aG9yaXphdGlvbi1jb25maWcudG9rZW4nO1xuaW1wb3J0IHsgSW5qZWN0YWJsZSwgSW5qZWN0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwQ2xpZW50LCBIdHRwSGVhZGVycywgSHR0cFBhcmFtcyB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IHN3aXRjaE1hcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vYXV0aGVudGljYXRpb24vdXNlci5zZXJ2aWNlJztcbmltcG9ydCB7IElBdXRob3JpemF0aW9uTWFuYWdlciB9IGZyb20gJy4uL2F1dGhvcml6YXRpb24uc2VydmljZSc7XG5cblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgVW1hQXV0aG9yaXphdGlvbk1hbmFnZXIgaW1wbGVtZW50cyBJQXV0aG9yaXphdGlvbk1hbmFnZXIge1xuICBjb25zdHJ1Y3RvcihcbiAgICBwcm90ZWN0ZWQgaHR0cENsaWVudDogSHR0cENsaWVudCxcbiAgICBASW5qZWN0KEF1dGhvcml6YXRpb25Db25maWdUb2tlbikgcHJvdGVjdGVkIGF1dGhvcml6YXRpb25Db25maWc6IEF1dGhvcml6YXRpb25Db25maWcsXG4gICkge1xuICB9XG4gIHB1YmxpYyBhc3luYyBhdXRob3JpemUoYWN0aW9uOiBzdHJpbmcsIHJlc291cmNlOiBzdHJpbmcpOiBQcm9taXNlPGJvb2xlYW4+IHtcblxuICAgIC8vIFdTTzIgLSAke2Vudmlyb25tZW50LnNldHRpbmdzLmF1dGhlbnRpY2F0aW9uLmF1dGhvcml0eX1hcGkvaWRlbnRpdHkvb2F1dGgyL3VtYS9wZXJtaXNzaW9uL3YxLjAvcGVybWlzc2lvblxuICAgIC8vIEtFWUNMT0FLIC0gJHtlbnZpcm9ubWVudC5zZXR0aW5ncy5hdXRoZW50aWNhdGlvbi5hdXRob3JpdHl9YXV0aHovcHJvdGVjdGlvbi9wZXJtaXNzaW9uXG5cbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LnBvc3QoYCR7dGhpcy5hdXRob3JpemF0aW9uQ29uZmlnLnVtYUNvbmZpZy5wZXJtaXNzaW9uRW5kcG9pbnR9YCxcbiAgICAgIFtcbiAgICAgICAge1xuICAgICAgICAgIHJlc291cmNlX2lkOiByZXNvdXJjZSxcbiAgICAgICAgICByZXNvdXJjZV9zY29wZXM6IFsgYWN0aW9uIF1cbiAgICAgICAgfVxuICAgICAgXVxuICAgICkucGlwZShzd2l0Y2hNYXAoKHJlc3VsdDogeyB0aWNrZXQ6IHN0cmluZyB9KSA9PiB7XG5cblxuICAgICAgLy8gV1NPMiAtIGAke2Vudmlyb25tZW50LnNldHRpbmdzLmF1dGhlbnRpY2F0aW9uLmF1dGhvcml0eX1vYXV0aDIvdG9rZW5gLFxuICAgICAgLy8gS0VZQ0xPQUsgLSBgJHtlbnZpcm9ubWVudC5zZXR0aW5ncy5hdXRoZW50aWNhdGlvbi5hdXRob3JpdHl9cHJvdG9jb2wvb3BlbmlkLWNvbm5lY3QvdG9rZW5gLFxuXG4gICAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LnBvc3QoYCR7dGhpcy5hdXRob3JpemF0aW9uQ29uZmlnLnVtYUNvbmZpZy50b2tlbkVuZHBvaW50fWAsXG4gICAgICAgIG5ldyBIdHRwUGFyYW1zKClcbiAgICAgICAgLnNldCgnZ3JhbnRfdHlwZScsICd1cm46aWV0ZjpwYXJhbXM6b2F1dGg6Z3JhbnQtdHlwZTp1bWEtdGlja2V0JylcbiAgICAgICAgLy8gLnNldCgnYXVkaWVuY2UnLCBjbGllbnQpXG4gICAgICAgIC5zZXQoJ3Jlc3BvbnNlX21vZGUnLCAnZGVjaXNpb24nKVxuICAgICAgICAuc2V0KCd0aWNrZXQnLCByZXN1bHQudGlja2V0KVxuICAgICAgICAudG9TdHJpbmcoKSxcbiAgICAgICAge1xuICAgICAgICAgIGhlYWRlcnM6IG5ldyBIdHRwSGVhZGVycygpLnNldCgnQ29udGVudC1UeXBlJywgJ2FwcGxpY2F0aW9uL3gtd3d3LWZvcm0tdXJsZW5jb2RlZCcpXG4gICAgICAgIH1cbiAgICAgICk7XG5cbiAgICB9KSkudG9Qcm9taXNlKCkudGhlbigocmVzdWx0OiB7IHJlc3VsdDogYm9vbGVhbiB9KSA9PiB7XG5cbiAgICAgIHJldHVybiByZXN1bHQucmVzdWx0O1xuXG4gICAgfSk7XG5cbiAgfVxufVxuIl19