/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { UserService } from '../../authentication/user.service';
import * as i0 from "@angular/core";
import * as i1 from "../../authentication/user.service";
export class UserAuthorizationManager {
    /**
     * @param {?} userService
     */
    constructor(userService) {
        this.userService = userService;
    }
    /**
     * @param {?} action
     * @param {?} resource
     * @return {?}
     */
    authorize(action, resource) {
        return tslib_1.__awaiter(this, void 0, void 0, /** @this {!UserAuthorizationManager} */ function* () {
            /** @type {?} */
            const userWithAuthorizations = (/** @type {?} */ (this.userService.userValue));
            if (userWithAuthorizations.authenticated) {
                if (userWithAuthorizations.authorizations != null && userWithAuthorizations.authorizations.length > 0) {
                    /** @type {?} */
                    const usuarioPossuiPermissao = userWithAuthorizations.authorizations.some((/**
                     * @param {?} permissao
                     * @return {?}
                     */
                    permissao => {
                        /** @type {?} */
                        const ehMesmoRecurso = permissao.resource === resource;
                        /** @type {?} */
                        const ehMesmaAcao = permissao.action.includes(action);
                        /** @type {?} */
                        const possuiPermissao = ehMesmoRecurso && ehMesmaAcao;
                        return possuiPermissao;
                    }));
                    if (usuarioPossuiPermissao) {
                        return true;
                    }
                }
            }
            return false;
        });
    }
}
UserAuthorizationManager.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
UserAuthorizationManager.ctorParameters = () => [
    { type: UserService }
];
/** @nocollapse */ UserAuthorizationManager.ngInjectableDef = i0.defineInjectable({ factory: function UserAuthorizationManager_Factory() { return new UserAuthorizationManager(i0.inject(i1.UserService)); }, token: UserAuthorizationManager, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @protected
     */
    UserAuthorizationManager.prototype.userService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci1hdXRob3JpemF0aW9uLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9hdXRoL2F1dGhvcml6YXRpb24vdXNlci91c2VyLWF1dGhvcml6YXRpb24uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUNBLE9BQU8sRUFBRSxVQUFVLEVBQVUsTUFBTSxlQUFlLENBQUM7QUFDbkQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLG1DQUFtQyxDQUFDOzs7QUFNaEUsTUFBTSxPQUFPLHdCQUF3Qjs7OztJQUNuQyxZQUFzQixXQUF3QjtRQUF4QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtJQUM5QyxDQUFDOzs7Ozs7SUFDWSxTQUFTLENBQUMsTUFBYyxFQUFFLFFBQWdCOzs7a0JBRS9DLHNCQUFzQixHQUFHLG1CQUFBLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxFQUEwQjtZQUVuRixJQUFJLHNCQUFzQixDQUFDLGFBQWEsRUFBRTtnQkFFeEMsSUFBSSxzQkFBc0IsQ0FBQyxjQUFjLElBQUksSUFBSSxJQUFJLHNCQUFzQixDQUFDLGNBQWMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFOzswQkFFL0Ysc0JBQXNCLEdBQUcsc0JBQXNCLENBQUMsY0FBYyxDQUFDLElBQUk7Ozs7b0JBQUMsU0FBUyxDQUFDLEVBQUU7OzhCQUM5RSxjQUFjLEdBQUcsU0FBUyxDQUFDLFFBQVEsS0FBSyxRQUFROzs4QkFDaEQsV0FBVyxHQUFHLFNBQVMsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQzs7OEJBRS9DLGVBQWUsR0FBRyxjQUFjLElBQUksV0FBVzt3QkFDckQsT0FBTyxlQUFlLENBQUM7b0JBQ3pCLENBQUMsRUFBQztvQkFFRixJQUFJLHNCQUFzQixFQUFFO3dCQUMxQixPQUFPLElBQUksQ0FBQztxQkFDYjtpQkFFRjthQUNGO1lBRUQsT0FBTyxLQUFLLENBQUM7UUFDZixDQUFDO0tBQUE7OztZQTlCRixVQUFVLFNBQUM7Z0JBQ1YsVUFBVSxFQUFFLE1BQU07YUFDbkI7Ozs7WUFMUSxXQUFXOzs7Ozs7OztJQU9OLCtDQUFrQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFVzZXJXaXRoQXV0aG9yaXphdGlvbnMgfSBmcm9tICcuLy4uLy4uL2F1dGhlbnRpY2F0aW9uL3VzZXIubW9kZWwnO1xuaW1wb3J0IHsgSW5qZWN0YWJsZSwgSW5qZWN0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gJy4uLy4uL2F1dGhlbnRpY2F0aW9uL3VzZXIuc2VydmljZSc7XG5pbXBvcnQgeyBJQXV0aG9yaXphdGlvbk1hbmFnZXIgfSBmcm9tICcuLi9hdXRob3JpemF0aW9uLnNlcnZpY2UnO1xuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBVc2VyQXV0aG9yaXphdGlvbk1hbmFnZXIgaW1wbGVtZW50cyBJQXV0aG9yaXphdGlvbk1hbmFnZXIge1xuICBjb25zdHJ1Y3Rvcihwcm90ZWN0ZWQgdXNlclNlcnZpY2U6IFVzZXJTZXJ2aWNlKSB7XG4gIH1cbiAgcHVibGljIGFzeW5jIGF1dGhvcml6ZShhY3Rpb246IHN0cmluZywgcmVzb3VyY2U6IHN0cmluZyk6IFByb21pc2U8Ym9vbGVhbj4ge1xuXG4gICAgY29uc3QgdXNlcldpdGhBdXRob3JpemF0aW9ucyA9IHRoaXMudXNlclNlcnZpY2UudXNlclZhbHVlIGFzIFVzZXJXaXRoQXV0aG9yaXphdGlvbnM7XG5cbiAgICBpZiAodXNlcldpdGhBdXRob3JpemF0aW9ucy5hdXRoZW50aWNhdGVkKSB7XG5cbiAgICAgIGlmICh1c2VyV2l0aEF1dGhvcml6YXRpb25zLmF1dGhvcml6YXRpb25zICE9IG51bGwgJiYgdXNlcldpdGhBdXRob3JpemF0aW9ucy5hdXRob3JpemF0aW9ucy5sZW5ndGggPiAwKSB7XG5cbiAgICAgICAgY29uc3QgdXN1YXJpb1Bvc3N1aVBlcm1pc3NhbyA9IHVzZXJXaXRoQXV0aG9yaXphdGlvbnMuYXV0aG9yaXphdGlvbnMuc29tZShwZXJtaXNzYW8gPT4ge1xuICAgICAgICAgIGNvbnN0IGVoTWVzbW9SZWN1cnNvID0gcGVybWlzc2FvLnJlc291cmNlID09PSByZXNvdXJjZTtcbiAgICAgICAgICBjb25zdCBlaE1lc21hQWNhbyA9IHBlcm1pc3Nhby5hY3Rpb24uaW5jbHVkZXMoYWN0aW9uKTtcblxuICAgICAgICAgIGNvbnN0IHBvc3N1aVBlcm1pc3NhbyA9IGVoTWVzbW9SZWN1cnNvICYmIGVoTWVzbWFBY2FvO1xuICAgICAgICAgIHJldHVybiBwb3NzdWlQZXJtaXNzYW87XG4gICAgICAgIH0pO1xuXG4gICAgICAgIGlmICh1c3VhcmlvUG9zc3VpUGVybWlzc2FvKSB7XG4gICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgIH1cblxuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiBmYWxzZTtcbiAgfVxufVxuIl19