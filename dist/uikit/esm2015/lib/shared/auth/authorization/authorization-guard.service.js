/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Location, PlatformLocation } from '@angular/common';
import { GuardService } from './guard.service';
import { AuthorizationService } from './authorization.service';
/**
 * @abstract
 */
export class AuthorizationGuardService extends GuardService {
    /**
     * @param {?} authorizationService
     * @param {?} router
     * @param {?} angularLocation
     * @param {?} platformLocation
     */
    constructor(authorizationService, router, angularLocation, platformLocation) {
        super(router, angularLocation, platformLocation);
        this.authorizationService = authorizationService;
        this.router = router;
        this.angularLocation = angularLocation;
        this.platformLocation = platformLocation;
    }
}
AuthorizationGuardService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
AuthorizationGuardService.ctorParameters = () => [
    { type: AuthorizationService },
    { type: Router },
    { type: Location },
    { type: PlatformLocation }
];
if (false) {
    /**
     * @type {?}
     * @protected
     */
    AuthorizationGuardService.prototype.authorizationService;
    /**
     * @type {?}
     * @protected
     */
    AuthorizationGuardService.prototype.router;
    /**
     * @type {?}
     * @protected
     */
    AuthorizationGuardService.prototype.angularLocation;
    /**
     * @type {?}
     * @protected
     */
    AuthorizationGuardService.prototype.platformLocation;
    /**
     * @abstract
     * @param {?} route
     * @param {?} state
     * @return {?}
     */
    AuthorizationGuardService.prototype.canActivate = function (route, state) { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aG9yaXphdGlvbi1ndWFyZC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvYXV0aC9hdXRob3JpemF0aW9uL2F1dGhvcml6YXRpb24tZ3VhcmQuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQ0EsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQTRELE1BQU0sRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBR25HLE9BQU8sRUFBRSxRQUFRLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUM3RCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDL0MsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0seUJBQXlCLENBQUM7Ozs7QUFJL0QsTUFBTSxPQUFnQix5QkFBMEIsU0FBUSxZQUFZOzs7Ozs7O0lBQ2xFLFlBQ1ksb0JBQTBDLEVBQzFDLE1BQWMsRUFDZCxlQUF5QixFQUN6QixnQkFBa0M7UUFFNUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxlQUFlLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztRQUx2Qyx5QkFBb0IsR0FBcEIsb0JBQW9CLENBQXNCO1FBQzFDLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxvQkFBZSxHQUFmLGVBQWUsQ0FBVTtRQUN6QixxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO0lBRzlDLENBQUM7OztZQVRGLFVBQVU7Ozs7WUFIRixvQkFBb0I7WUFMc0MsTUFBTTtZQUdoRSxRQUFRO1lBQUUsZ0JBQWdCOzs7Ozs7O0lBUS9CLHlEQUFvRDs7Ozs7SUFDcEQsMkNBQXdCOzs7OztJQUN4QixvREFBbUM7Ozs7O0lBQ25DLHFEQUE0Qzs7Ozs7OztJQUs5Qyw4RUFBd0ciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBBdXRoZW50aWNhdGlvbkd1YXJkU2VydmljZSB9IGZyb20gJy4vYXV0aGVudGljYXRpb24tZ3VhcmQuc2VydmljZSc7XG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDYW5BY3RpdmF0ZSwgQWN0aXZhdGVkUm91dGVTbmFwc2hvdCwgUm91dGVyU3RhdGVTbmFwc2hvdCwgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSAnLi4vYXV0aGVudGljYXRpb24vdXNlci5zZXJ2aWNlJztcbmltcG9ydCB7IExvY2F0aW9uLCBQbGF0Zm9ybUxvY2F0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7IEd1YXJkU2VydmljZSB9IGZyb20gJy4vZ3VhcmQuc2VydmljZSc7XG5pbXBvcnQgeyBBdXRob3JpemF0aW9uU2VydmljZSB9IGZyb20gJy4vYXV0aG9yaXphdGlvbi5zZXJ2aWNlJztcblxuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgQXV0aG9yaXphdGlvbkd1YXJkU2VydmljZSBleHRlbmRzIEd1YXJkU2VydmljZSB7XG4gIGNvbnN0cnVjdG9yKFxuICAgIHByb3RlY3RlZCBhdXRob3JpemF0aW9uU2VydmljZTogQXV0aG9yaXphdGlvblNlcnZpY2UsXG4gICAgcHJvdGVjdGVkIHJvdXRlcjogUm91dGVyLFxuICAgIHByb3RlY3RlZCBhbmd1bGFyTG9jYXRpb246IExvY2F0aW9uLFxuICAgIHByb3RlY3RlZCBwbGF0Zm9ybUxvY2F0aW9uOiBQbGF0Zm9ybUxvY2F0aW9uXG4gICkge1xuICAgIHN1cGVyKHJvdXRlciwgYW5ndWxhckxvY2F0aW9uLCBwbGF0Zm9ybUxvY2F0aW9uKTtcbiAgfVxuXG4gIGFzeW5jIGFic3RyYWN0IGNhbkFjdGl2YXRlKHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LCBzdGF0ZTogUm91dGVyU3RhdGVTbmFwc2hvdCk6IFByb21pc2U8Ym9vbGVhbj47XG5cbn1cbiJdfQ==