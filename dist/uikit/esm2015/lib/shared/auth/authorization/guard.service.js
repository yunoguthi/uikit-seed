/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @abstract
 */
export class GuardService {
    /**
     * @param {?} router
     * @param {?} angularLocation
     * @param {?} platformLocation
     */
    constructor(router, angularLocation, platformLocation) {
        this.router = router;
        this.angularLocation = angularLocation;
        this.platformLocation = platformLocation;
    }
    /**
     * @protected
     * @return {?}
     */
    get routerStateSnapshot() {
        return this.router.routerState.snapshot;
    }
    /**
     * @protected
     * @return {?}
     */
    get activatedRouteSnapshot() {
        return this.router.routerState.root.snapshot;
    }
    // tslint:disable-next-line:max-line-length
    /**
     * @param {?} childRoute
     * @param {?} state
     * @return {?}
     */
    canActivateChild(childRoute, state) {
        return this.canActivate(childRoute, state);
    }
    /**
     * @return {?}
     */
    allow() {
        return true;
    }
    /**
     * @param {?} state
     * @param {?=} login
     * @param {?=} setCallback
     * @return {?}
     */
    deny(state, login = 'manual', setCallback = true) {
        if (setCallback) {
            /** @type {?} */
            const baseHref = this.platformLocation.getBaseHrefFromDOM();
            /** @type {?} */
            const origin = window.location.origin;
            /** @type {?} */
            const completeUrlToBaseHref = origin + baseHref;
            /** @type {?} */
            const angularRouteWithBaseHref = this.angularLocation.prepareExternalUrl(state.url);
            /** @type {?} */
            let angularRouteWithoutBaseHref = angularRouteWithBaseHref;
            if (angularRouteWithBaseHref.startsWith(baseHref)) {
                angularRouteWithoutBaseHref = angularRouteWithBaseHref.replace(baseHref, '');
            }
            /** @type {?} */
            const completeRoute = completeUrlToBaseHref + angularRouteWithoutBaseHref;
            console.log('Setando after callback como: ', completeRoute);
            localStorage.setItem('authentication-callback', completeRoute);
        }
        else {
            localStorage.removeItem('authentication-callback');
        }
        // const uri = this.angularLocation.prepareExternalUrl(state.url);
        // localStorage.setItem(location.host + ':callback', uri);
        /** @type {?} */
        let parametros = { queryParams: { login: login } };
        if (login == 'manual') {
            parametros = undefined;
        }
        this.router.navigate(['unauthorized'], parametros);
        return false;
    }
}
if (false) {
    /**
     * @type {?}
     * @protected
     */
    GuardService.prototype.router;
    /**
     * @type {?}
     * @protected
     */
    GuardService.prototype.angularLocation;
    /**
     * @type {?}
     * @protected
     */
    GuardService.prototype.platformLocation;
    /**
     * @abstract
     * @param {?} route
     * @param {?} state
     * @return {?}
     */
    GuardService.prototype.canActivate = function (route, state) { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ3VhcmQuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2F1dGgvYXV0aG9yaXphdGlvbi9ndWFyZC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFJQSxNQUFNLE9BQWdCLFlBQVk7Ozs7OztJQVdoQyxZQUNZLE1BQWMsRUFDZCxlQUF5QixFQUN6QixnQkFBa0M7UUFGbEMsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLG9CQUFlLEdBQWYsZUFBZSxDQUFVO1FBQ3pCLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7SUFFOUMsQ0FBQzs7Ozs7SUFiRCxJQUFjLG1CQUFtQjtRQUMvQixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQztJQUMxQyxDQUFDOzs7OztJQUVELElBQWMsc0JBQXNCO1FBQ2xDLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztJQUMvQyxDQUFDOzs7Ozs7O0lBVUQsZ0JBQWdCLENBQUMsVUFBa0MsRUFBRSxLQUEwQjtRQUM3RSxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQzdDLENBQUM7Ozs7SUFJRCxLQUFLO1FBQ0gsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDOzs7Ozs7O0lBRUQsSUFBSSxDQUFDLEtBQTBCLEVBQUUsUUFBMkIsUUFBUSxFQUFFLFdBQVcsR0FBRyxJQUFJO1FBRXRGLElBQUksV0FBVyxFQUFFOztrQkFFVCxRQUFRLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGtCQUFrQixFQUFFOztrQkFDckQsTUFBTSxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTTs7a0JBRS9CLHFCQUFxQixHQUFHLE1BQU0sR0FBRyxRQUFROztrQkFHekMsd0JBQXdCLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDOztnQkFFL0UsMkJBQTJCLEdBQUcsd0JBQXdCO1lBQzFELElBQUksd0JBQXdCLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUNqRCwyQkFBMkIsR0FBRyx3QkFBd0IsQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLEVBQUUsQ0FBQyxDQUFDO2FBQzlFOztrQkFFSyxhQUFhLEdBQUcscUJBQXFCLEdBQUcsMkJBQTJCO1lBRXpFLE9BQU8sQ0FBQyxHQUFHLENBQUMsK0JBQStCLEVBQUUsYUFBYSxDQUFDLENBQUM7WUFDNUQsWUFBWSxDQUFDLE9BQU8sQ0FBQyx5QkFBeUIsRUFBRSxhQUFhLENBQUMsQ0FBQztTQUNoRTthQUFJO1lBQ0gsWUFBWSxDQUFDLFVBQVUsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1NBQ3BEOzs7O1lBSUcsVUFBVSxHQUFHLEVBQUUsV0FBVyxFQUFFLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxFQUFFO1FBQ2xELElBQUksS0FBSyxJQUFJLFFBQVEsRUFBRTtZQUNyQixVQUFVLEdBQUcsU0FBUyxDQUFDO1NBQ3hCO1FBRUQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxjQUFjLENBQUMsRUFBRSxVQUFVLENBQUUsQ0FBQztRQUNwRCxPQUFPLEtBQUssQ0FBQztJQUNmLENBQUM7Q0FFRjs7Ozs7O0lBckRHLDhCQUF3Qjs7Ozs7SUFDeEIsdUNBQW1DOzs7OztJQUNuQyx3Q0FBNEM7Ozs7Ozs7SUFTOUMsaUVBQWtJIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ2FuQWN0aXZhdGUsIEFjdGl2YXRlZFJvdXRlU25hcHNob3QsIFJvdXRlclN0YXRlU25hcHNob3QsIFJvdXRlciwgQ2FuQWN0aXZhdGVDaGlsZCwgVXJsVHJlZSB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQgeyBMb2NhdGlvbiwgUGxhdGZvcm1Mb2NhdGlvbiB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5cbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBHdWFyZFNlcnZpY2UgaW1wbGVtZW50cyBDYW5BY3RpdmF0ZSwgQ2FuQWN0aXZhdGVDaGlsZCB7XG5cblxuICBwcm90ZWN0ZWQgZ2V0IHJvdXRlclN0YXRlU25hcHNob3QoKSB7XG4gICAgcmV0dXJuIHRoaXMucm91dGVyLnJvdXRlclN0YXRlLnNuYXBzaG90O1xuICB9XG5cbiAgcHJvdGVjdGVkIGdldCBhY3RpdmF0ZWRSb3V0ZVNuYXBzaG90KCkge1xuICAgIHJldHVybiB0aGlzLnJvdXRlci5yb3V0ZXJTdGF0ZS5yb290LnNuYXBzaG90O1xuICB9XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJvdGVjdGVkIHJvdXRlcjogUm91dGVyLFxuICAgIHByb3RlY3RlZCBhbmd1bGFyTG9jYXRpb246IExvY2F0aW9uLFxuICAgIHByb3RlY3RlZCBwbGF0Zm9ybUxvY2F0aW9uOiBQbGF0Zm9ybUxvY2F0aW9uLFxuICApIHtcbiAgfVxuXG4gIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTptYXgtbGluZS1sZW5ndGhcbiAgY2FuQWN0aXZhdGVDaGlsZChjaGlsZFJvdXRlOiBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LCBzdGF0ZTogUm91dGVyU3RhdGVTbmFwc2hvdCk6IGJvb2xlYW4gfCBVcmxUcmVlIHwgT2JzZXJ2YWJsZTxib29sZWFuIHwgVXJsVHJlZT4gfCBQcm9taXNlPGJvb2xlYW4gfCBVcmxUcmVlPiB7XG4gICAgcmV0dXJuIHRoaXMuY2FuQWN0aXZhdGUoY2hpbGRSb3V0ZSwgc3RhdGUpO1xuICB9XG5cbiAgYWJzdHJhY3QgY2FuQWN0aXZhdGUocm91dGU6IEFjdGl2YXRlZFJvdXRlU25hcHNob3QsIHN0YXRlOiBSb3V0ZXJTdGF0ZVNuYXBzaG90KTogYm9vbGVhbiB8IFByb21pc2U8Ym9vbGVhbj4gfCBPYnNlcnZhYmxlPGJvb2xlYW4+O1xuXG4gIGFsbG93KCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0cnVlO1xuICB9XG5cbiAgZGVueShzdGF0ZTogUm91dGVyU3RhdGVTbmFwc2hvdCwgbG9naW46ICdhdXRvJyB8ICdtYW51YWwnID0gJ21hbnVhbCcsIHNldENhbGxiYWNrID0gdHJ1ZSk6IGJvb2xlYW4ge1xuXG4gICAgaWYgKHNldENhbGxiYWNrKSB7XG5cbiAgICAgIGNvbnN0IGJhc2VIcmVmID0gdGhpcy5wbGF0Zm9ybUxvY2F0aW9uLmdldEJhc2VIcmVmRnJvbURPTSgpO1xuICAgICAgY29uc3Qgb3JpZ2luID0gd2luZG93LmxvY2F0aW9uLm9yaWdpbjtcblxuICAgICAgY29uc3QgY29tcGxldGVVcmxUb0Jhc2VIcmVmID0gb3JpZ2luICsgYmFzZUhyZWY7XG5cblxuICAgICAgY29uc3QgYW5ndWxhclJvdXRlV2l0aEJhc2VIcmVmID0gdGhpcy5hbmd1bGFyTG9jYXRpb24ucHJlcGFyZUV4dGVybmFsVXJsKHN0YXRlLnVybCk7XG5cbiAgICAgIGxldCBhbmd1bGFyUm91dGVXaXRob3V0QmFzZUhyZWYgPSBhbmd1bGFyUm91dGVXaXRoQmFzZUhyZWY7XG4gICAgICBpZiAoYW5ndWxhclJvdXRlV2l0aEJhc2VIcmVmLnN0YXJ0c1dpdGgoYmFzZUhyZWYpKSB7XG4gICAgICAgIGFuZ3VsYXJSb3V0ZVdpdGhvdXRCYXNlSHJlZiA9IGFuZ3VsYXJSb3V0ZVdpdGhCYXNlSHJlZi5yZXBsYWNlKGJhc2VIcmVmLCAnJyk7XG4gICAgICB9XG5cbiAgICAgIGNvbnN0IGNvbXBsZXRlUm91dGUgPSBjb21wbGV0ZVVybFRvQmFzZUhyZWYgKyBhbmd1bGFyUm91dGVXaXRob3V0QmFzZUhyZWY7XG5cbiAgICAgIGNvbnNvbGUubG9nKCdTZXRhbmRvIGFmdGVyIGNhbGxiYWNrIGNvbW86ICcsIGNvbXBsZXRlUm91dGUpO1xuICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oJ2F1dGhlbnRpY2F0aW9uLWNhbGxiYWNrJywgY29tcGxldGVSb3V0ZSk7XG4gICAgfWVsc2V7XG4gICAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbSgnYXV0aGVudGljYXRpb24tY2FsbGJhY2snKTtcbiAgICB9XG5cbiAgICAvLyBjb25zdCB1cmkgPSB0aGlzLmFuZ3VsYXJMb2NhdGlvbi5wcmVwYXJlRXh0ZXJuYWxVcmwoc3RhdGUudXJsKTtcbiAgICAvLyBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShsb2NhdGlvbi5ob3N0ICsgJzpjYWxsYmFjaycsIHVyaSk7XG4gICAgbGV0IHBhcmFtZXRyb3MgPSB7IHF1ZXJ5UGFyYW1zOiB7IGxvZ2luOiBsb2dpbiB9IH07XG4gICAgaWYgKGxvZ2luID09ICdtYW51YWwnKSB7XG4gICAgICBwYXJhbWV0cm9zID0gdW5kZWZpbmVkO1xuICAgIH1cblxuICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFsndW5hdXRob3JpemVkJ10sIHBhcmFtZXRyb3MgKTtcbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cblxufVxuIl19