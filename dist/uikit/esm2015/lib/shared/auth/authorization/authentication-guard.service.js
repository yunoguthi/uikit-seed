/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../authentication/user.service';
import { Location, PlatformLocation } from '@angular/common';
import { GuardService } from './guard.service';
import { map } from 'rxjs/operators';
export class AuthenticationGuardService extends GuardService {
    /**
     * @param {?} identityService
     * @param {?} router
     * @param {?} angularLocation
     * @param {?} platformLocation
     */
    constructor(identityService, router, angularLocation, platformLocation) {
        super(router, angularLocation, platformLocation);
        this.identityService = identityService;
        this.router = router;
        this.angularLocation = angularLocation;
        this.platformLocation = platformLocation;
    }
    /**
     * @param {?} route
     * @param {?} state
     * @return {?}
     */
    canActivate(route, state) {
        console.debug('AuthenticationGuardService!', this.identityService.userValue);
        /** @type {?} */
        const mapped = this.identityService.user$
            .pipe(map((/**
         * @param {?} user
         * @return {?}
         */
        user => {
            console.debug('Verificando se usuário está autenticado!', user);
            if (user.authenticated) {
                return this.allow();
            }
            else {
                /** @type {?} */
                const data = route.data;
                /** @type {?} */
                let login = 'auto';
                if (data && data.login == 'manual') {
                    login = 'manual';
                }
                /** @type {?} */
                let setCallback = true;
                if (data && data.setCallback == false) {
                    setCallback = false;
                }
                return this.deny(state, login, setCallback);
            }
        })));
        // const ret = map.catch(() => {
        //   return this.deny();
        // });
        return mapped;
    }
}
AuthenticationGuardService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
AuthenticationGuardService.ctorParameters = () => [
    { type: UserService },
    { type: Router },
    { type: Location },
    { type: PlatformLocation }
];
if (false) {
    /**
     * @type {?}
     * @protected
     */
    AuthenticationGuardService.prototype.identityService;
    /**
     * @type {?}
     * @protected
     */
    AuthenticationGuardService.prototype.router;
    /**
     * @type {?}
     * @protected
     */
    AuthenticationGuardService.prototype.angularLocation;
    /**
     * @type {?}
     * @protected
     */
    AuthenticationGuardService.prototype.platformLocation;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aGVudGljYXRpb24tZ3VhcmQuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2F1dGgvYXV0aG9yaXphdGlvbi9hdXRoZW50aWNhdGlvbi1ndWFyZC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBNEQsTUFBTSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFFbkcsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBQzdELE9BQU8sRUFBRSxRQUFRLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUM3RCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFFL0MsT0FBTyxFQUFFLEdBQUcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBR3JDLE1BQU0sT0FBTywwQkFBMkIsU0FBUSxZQUFZOzs7Ozs7O0lBRTFELFlBQ1ksZUFBNEIsRUFDNUIsTUFBYyxFQUNkLGVBQXlCLEVBQ3pCLGdCQUFrQztRQUU1QyxLQUFLLENBQUMsTUFBTSxFQUFFLGVBQWUsRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDO1FBTHZDLG9CQUFlLEdBQWYsZUFBZSxDQUFhO1FBQzVCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxvQkFBZSxHQUFmLGVBQWUsQ0FBVTtRQUN6QixxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO0lBRzlDLENBQUM7Ozs7OztJQUVELFdBQVcsQ0FBQyxLQUE2QixFQUFFLEtBQTBCO1FBQ25FLE9BQU8sQ0FBQyxLQUFLLENBQUMsNkJBQTZCLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsQ0FBQzs7Y0FDdkUsTUFBTSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSzthQUN4QyxJQUFJLENBQUMsR0FBRzs7OztRQUFDLElBQUksQ0FBQyxFQUFFO1lBQ2YsT0FBTyxDQUFDLEtBQUssQ0FBQywwQ0FBMEMsRUFBRSxJQUFJLENBQUMsQ0FBQztZQUNoRSxJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7Z0JBQ3RCLE9BQU8sSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO2FBQ3JCO2lCQUFNOztzQkFDQyxJQUFJLEdBQUcsS0FBSyxDQUFDLElBQUk7O29CQUNuQixLQUFLLEdBQXNCLE1BQU07Z0JBQ3JDLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxLQUFLLElBQUksUUFBUSxFQUFFO29CQUNsQyxLQUFLLEdBQUcsUUFBUSxDQUFDO2lCQUNsQjs7b0JBRUcsV0FBVyxHQUFHLElBQUk7Z0JBQ3RCLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxXQUFXLElBQUksS0FBSyxFQUFFO29CQUNyQyxXQUFXLEdBQUcsS0FBSyxDQUFDO2lCQUNyQjtnQkFDRCxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLEtBQUssRUFBRSxXQUFXLENBQUMsQ0FBQzthQUM3QztRQUNILENBQUMsRUFBQyxDQUFDO1FBQ0gsZ0NBQWdDO1FBQ2hDLHdCQUF3QjtRQUN4QixNQUFNO1FBQ04sT0FBTyxNQUFNLENBQUM7SUFDaEIsQ0FBQzs7O1lBckNGLFVBQVU7Ozs7WUFORixXQUFXO1lBRitDLE1BQU07WUFHaEUsUUFBUTtZQUFFLGdCQUFnQjs7Ozs7OztJQVMvQixxREFBc0M7Ozs7O0lBQ3RDLDRDQUF3Qjs7Ozs7SUFDeEIscURBQW1DOzs7OztJQUNuQyxzREFBNEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDYW5BY3RpdmF0ZSwgQWN0aXZhdGVkUm91dGVTbmFwc2hvdCwgUm91dGVyU3RhdGVTbmFwc2hvdCwgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSAnLi4vYXV0aGVudGljYXRpb24vdXNlci5zZXJ2aWNlJztcbmltcG9ydCB7IExvY2F0aW9uLCBQbGF0Zm9ybUxvY2F0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7IEd1YXJkU2VydmljZSB9IGZyb20gJy4vZ3VhcmQuc2VydmljZSc7XG5cbmltcG9ydCB7IG1hcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIEF1dGhlbnRpY2F0aW9uR3VhcmRTZXJ2aWNlIGV4dGVuZHMgR3VhcmRTZXJ2aWNlIHtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcm90ZWN0ZWQgaWRlbnRpdHlTZXJ2aWNlOiBVc2VyU2VydmljZSxcbiAgICBwcm90ZWN0ZWQgcm91dGVyOiBSb3V0ZXIsXG4gICAgcHJvdGVjdGVkIGFuZ3VsYXJMb2NhdGlvbjogTG9jYXRpb24sXG4gICAgcHJvdGVjdGVkIHBsYXRmb3JtTG9jYXRpb246IFBsYXRmb3JtTG9jYXRpb25cbiAgKSB7XG4gICAgc3VwZXIocm91dGVyLCBhbmd1bGFyTG9jYXRpb24sIHBsYXRmb3JtTG9jYXRpb24pO1xuICB9XG5cbiAgY2FuQWN0aXZhdGUocm91dGU6IEFjdGl2YXRlZFJvdXRlU25hcHNob3QsIHN0YXRlOiBSb3V0ZXJTdGF0ZVNuYXBzaG90KTogYm9vbGVhbiB8IFByb21pc2U8Ym9vbGVhbj4gfCBPYnNlcnZhYmxlPGJvb2xlYW4+IHtcbiAgICBjb25zb2xlLmRlYnVnKCdBdXRoZW50aWNhdGlvbkd1YXJkU2VydmljZSEnLCB0aGlzLmlkZW50aXR5U2VydmljZS51c2VyVmFsdWUpO1xuICAgIGNvbnN0IG1hcHBlZCA9IHRoaXMuaWRlbnRpdHlTZXJ2aWNlLnVzZXIkXG4gICAgLnBpcGUobWFwKHVzZXIgPT4ge1xuICAgICAgY29uc29sZS5kZWJ1ZygnVmVyaWZpY2FuZG8gc2UgdXN1w6FyaW8gZXN0w6EgYXV0ZW50aWNhZG8hJywgdXNlcik7XG4gICAgICBpZiAodXNlci5hdXRoZW50aWNhdGVkKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmFsbG93KCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBjb25zdCBkYXRhID0gcm91dGUuZGF0YTtcbiAgICAgICAgbGV0IGxvZ2luOiAnbWFudWFsJyB8ICdhdXRvJyA9ICdhdXRvJztcbiAgICAgICAgaWYgKGRhdGEgJiYgZGF0YS5sb2dpbiA9PSAnbWFudWFsJykge1xuICAgICAgICAgIGxvZ2luID0gJ21hbnVhbCc7XG4gICAgICAgIH1cblxuICAgICAgICBsZXQgc2V0Q2FsbGJhY2sgPSB0cnVlO1xuICAgICAgICBpZiAoZGF0YSAmJiBkYXRhLnNldENhbGxiYWNrID09IGZhbHNlKSB7XG4gICAgICAgICAgc2V0Q2FsbGJhY2sgPSBmYWxzZTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcy5kZW55KHN0YXRlLCBsb2dpbiwgc2V0Q2FsbGJhY2spO1xuICAgICAgfVxuICAgIH0pKTtcbiAgICAvLyBjb25zdCByZXQgPSBtYXAuY2F0Y2goKCkgPT4ge1xuICAgIC8vICAgcmV0dXJuIHRoaXMuZGVueSgpO1xuICAgIC8vIH0pO1xuICAgIHJldHVybiBtYXBwZWQ7XG4gIH1cblxuXG59XG4iXX0=