/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthorizationConfigToken } from '../authorization-config.token';
import { UserService } from '../../authentication/user.service';
import * as i0 from "@angular/core";
import * as i1 from "../../authentication/user.service";
import * as i2 from "@angular/common/http";
import * as i3 from "../authorization-config.token";
export class XacmlAuthorizationManager {
    /**
     * @param {?} userService
     * @param {?} httpClient
     * @param {?} authorizationConfig
     */
    constructor(userService, httpClient, authorizationConfig) {
        this.userService = userService;
        this.httpClient = httpClient;
        this.authorizationConfig = authorizationConfig;
    }
    /**
     * @param {?} action
     * @param {?} resource
     * @return {?}
     */
    authorize(action, resource) {
        return tslib_1.__awaiter(this, void 0, void 0, /** @this {!XacmlAuthorizationManager} */ function* () {
            // https://docs.wso2.com/display/IS570/Entitlement+with+REST+APIs
            // WSO2 - ${environment.settings.authentication.authority}api/identity/entitlement/decision/pdp
            return this.httpClient
                .post(`${this.authorizationConfig.xacmlConfig.policyDecisionEndpoint}`, {
                Request: {
                    AccessSubject: {
                        Attribute: [
                            {
                                AttributeId: 'subject-id',
                                Value: `${this.userService.userValue.sub}`,
                                DataType: 'string',
                                IncludeInResult: true
                            }
                        ]
                    },
                    Resource: {
                        Attribute: [
                            {
                                AttributeId: 'resource-id',
                                Value: resource,
                                DataType: 'string',
                                IncludeInResult: true
                            }
                        ]
                    },
                    Action: {
                        Attribute: [
                            {
                                AttributeId: 'action-id',
                                Value: action,
                                DataType: 'string',
                                IncludeInResult: true
                            }
                        ]
                    }
                }
            })
                .toPromise()
                .then((/**
             * @param {?} result
             * @return {?}
             */
            (result) => {
                if (result && result.Response && result.Response[0]) {
                    /** @type {?} */
                    const resposta = result.Response[0];
                    if (resposta.Decision === 'Permit') {
                        return true;
                    }
                }
                return false;
            }));
        });
    }
}
XacmlAuthorizationManager.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
XacmlAuthorizationManager.ctorParameters = () => [
    { type: UserService },
    { type: HttpClient },
    { type: undefined, decorators: [{ type: Inject, args: [AuthorizationConfigToken,] }] }
];
/** @nocollapse */ XacmlAuthorizationManager.ngInjectableDef = i0.defineInjectable({ factory: function XacmlAuthorizationManager_Factory() { return new XacmlAuthorizationManager(i0.inject(i1.UserService), i0.inject(i2.HttpClient), i0.inject(i3.AuthorizationConfigToken)); }, token: XacmlAuthorizationManager, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @protected
     */
    XacmlAuthorizationManager.prototype.userService;
    /**
     * @type {?}
     * @protected
     */
    XacmlAuthorizationManager.prototype.httpClient;
    /**
     * @type {?}
     * @protected
     */
    XacmlAuthorizationManager.prototype.authorizationConfig;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoieGFjbWwtYXV0aG9yaXphdGlvbi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvYXV0aC9hdXRob3JpemF0aW9uL3hhY21sL3hhY21sLWF1dGhvcml6YXRpb24uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ25ELE9BQU8sRUFBRSxVQUFVLEVBQTJCLE1BQU0sc0JBQXNCLENBQUM7QUFDM0UsT0FBTyxFQUFFLHdCQUF3QixFQUF1QixNQUFNLCtCQUErQixDQUFDO0FBQzlGLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxtQ0FBbUMsQ0FBQzs7Ozs7QUFNaEUsTUFBTSxPQUFPLHlCQUF5Qjs7Ozs7O0lBQ3BDLFlBQ1ksV0FBd0IsRUFDeEIsVUFBc0IsRUFDWSxtQkFBd0M7UUFGMUUsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUNZLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7SUFFdEYsQ0FBQzs7Ozs7O0lBQ1ksU0FBUyxDQUFDLE1BQWMsRUFBRSxRQUFnQjs7WUFFckQsaUVBQWlFO1lBQ2pFLCtGQUErRjtZQUUvRixPQUFPLElBQUksQ0FBQyxVQUFVO2lCQUNuQixJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsV0FBVyxDQUFDLHNCQUFzQixFQUFFLEVBQ3BFO2dCQUNFLE9BQU8sRUFBRTtvQkFDUCxhQUFhLEVBQUU7d0JBQ2IsU0FBUyxFQUFFOzRCQUNUO2dDQUNFLFdBQVcsRUFBRSxZQUFZO2dDQUN6QixLQUFLLEVBQUUsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxHQUFHLEVBQUU7Z0NBQzFDLFFBQVEsRUFBRSxRQUFRO2dDQUNsQixlQUFlLEVBQUUsSUFBSTs2QkFDdEI7eUJBQ0Y7cUJBQ0Y7b0JBQ0QsUUFBUSxFQUFFO3dCQUNSLFNBQVMsRUFBRTs0QkFDVDtnQ0FDRSxXQUFXLEVBQUUsYUFBYTtnQ0FDMUIsS0FBSyxFQUFFLFFBQVE7Z0NBQ2YsUUFBUSxFQUFFLFFBQVE7Z0NBQ2xCLGVBQWUsRUFBRSxJQUFJOzZCQUN0Qjt5QkFDRjtxQkFDRjtvQkFDRCxNQUFNLEVBQUU7d0JBQ04sU0FBUyxFQUFFOzRCQUNUO2dDQUNFLFdBQVcsRUFBRSxXQUFXO2dDQUN4QixLQUFLLEVBQUUsTUFBTTtnQ0FDYixRQUFRLEVBQUUsUUFBUTtnQ0FDbEIsZUFBZSxFQUFFLElBQUk7NkJBQ3RCO3lCQUNGO3FCQUNGO2lCQUNGO2FBQ0YsQ0FDRjtpQkFDQSxTQUFTLEVBQUU7aUJBQ1gsSUFBSTs7OztZQUFDLENBQUMsTUFBdUQsRUFBRSxFQUFFO2dCQUNoRSxJQUFJLE1BQU0sSUFBSSxNQUFNLENBQUMsUUFBUSxJQUFJLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUU7OzBCQUM3QyxRQUFRLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7b0JBQ25DLElBQUksUUFBUSxDQUFDLFFBQVEsS0FBSyxRQUFRLEVBQUU7d0JBQ2xDLE9BQU8sSUFBSSxDQUFDO3FCQUNiO2lCQUNGO2dCQUNELE9BQU8sS0FBSyxDQUFDO1lBQ2YsQ0FBQyxFQUFDLENBQUM7UUFDUCxDQUFDO0tBQUE7OztZQTlERixVQUFVLFNBQUM7Z0JBQ1YsVUFBVSxFQUFFLE1BQU07YUFDbkI7Ozs7WUFMUSxXQUFXO1lBRlgsVUFBVTs0Q0FZZCxNQUFNLFNBQUMsd0JBQXdCOzs7Ozs7OztJQUZoQyxnREFBa0M7Ozs7O0lBQ2xDLCtDQUFnQzs7Ozs7SUFDaEMsd0RBQW9GIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSwgSW5qZWN0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwQ2xpZW50LCBIdHRwSGVhZGVycywgSHR0cFBhcmFtcyB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IEF1dGhvcml6YXRpb25Db25maWdUb2tlbiwgQXV0aG9yaXphdGlvbkNvbmZpZyB9IGZyb20gJy4uL2F1dGhvcml6YXRpb24tY29uZmlnLnRva2VuJztcbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vYXV0aGVudGljYXRpb24vdXNlci5zZXJ2aWNlJztcbmltcG9ydCB7IElBdXRob3JpemF0aW9uTWFuYWdlciB9IGZyb20gJy4uL2F1dGhvcml6YXRpb24uc2VydmljZSc7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIFhhY21sQXV0aG9yaXphdGlvbk1hbmFnZXIgaW1wbGVtZW50cyBJQXV0aG9yaXphdGlvbk1hbmFnZXIge1xuICBjb25zdHJ1Y3RvcihcbiAgICBwcm90ZWN0ZWQgdXNlclNlcnZpY2U6IFVzZXJTZXJ2aWNlLFxuICAgIHByb3RlY3RlZCBodHRwQ2xpZW50OiBIdHRwQ2xpZW50LFxuICAgIEBJbmplY3QoQXV0aG9yaXphdGlvbkNvbmZpZ1Rva2VuKSBwcm90ZWN0ZWQgYXV0aG9yaXphdGlvbkNvbmZpZzogQXV0aG9yaXphdGlvbkNvbmZpZyxcbiAgKSB7XG4gIH1cbiAgcHVibGljIGFzeW5jIGF1dGhvcml6ZShhY3Rpb246IHN0cmluZywgcmVzb3VyY2U6IHN0cmluZyk6IFByb21pc2U8Ym9vbGVhbj4ge1xuXG4gICAgLy8gaHR0cHM6Ly9kb2NzLndzbzIuY29tL2Rpc3BsYXkvSVM1NzAvRW50aXRsZW1lbnQrd2l0aCtSRVNUK0FQSXNcbiAgICAvLyBXU08yIC0gJHtlbnZpcm9ubWVudC5zZXR0aW5ncy5hdXRoZW50aWNhdGlvbi5hdXRob3JpdHl9YXBpL2lkZW50aXR5L2VudGl0bGVtZW50L2RlY2lzaW9uL3BkcFxuXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudFxuICAgICAgLnBvc3QoYCR7dGhpcy5hdXRob3JpemF0aW9uQ29uZmlnLnhhY21sQ29uZmlnLnBvbGljeURlY2lzaW9uRW5kcG9pbnR9YCxcbiAgICAgICAge1xuICAgICAgICAgIFJlcXVlc3Q6IHtcbiAgICAgICAgICAgIEFjY2Vzc1N1YmplY3Q6IHtcbiAgICAgICAgICAgICAgQXR0cmlidXRlOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgQXR0cmlidXRlSWQ6ICdzdWJqZWN0LWlkJyxcbiAgICAgICAgICAgICAgICAgIFZhbHVlOiBgJHt0aGlzLnVzZXJTZXJ2aWNlLnVzZXJWYWx1ZS5zdWJ9YCxcbiAgICAgICAgICAgICAgICAgIERhdGFUeXBlOiAnc3RyaW5nJyxcbiAgICAgICAgICAgICAgICAgIEluY2x1ZGVJblJlc3VsdDogdHJ1ZVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIFJlc291cmNlOiB7XG4gICAgICAgICAgICAgIEF0dHJpYnV0ZTogW1xuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgIEF0dHJpYnV0ZUlkOiAncmVzb3VyY2UtaWQnLFxuICAgICAgICAgICAgICAgICAgVmFsdWU6IHJlc291cmNlLFxuICAgICAgICAgICAgICAgICAgRGF0YVR5cGU6ICdzdHJpbmcnLFxuICAgICAgICAgICAgICAgICAgSW5jbHVkZUluUmVzdWx0OiB0cnVlXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBdXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgQWN0aW9uOiB7XG4gICAgICAgICAgICAgIEF0dHJpYnV0ZTogW1xuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgIEF0dHJpYnV0ZUlkOiAnYWN0aW9uLWlkJyxcbiAgICAgICAgICAgICAgICAgIFZhbHVlOiBhY3Rpb24sXG4gICAgICAgICAgICAgICAgICBEYXRhVHlwZTogJ3N0cmluZycsXG4gICAgICAgICAgICAgICAgICBJbmNsdWRlSW5SZXN1bHQ6IHRydWVcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIF1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIClcbiAgICAgIC50b1Byb21pc2UoKVxuICAgICAgLnRoZW4oKHJlc3VsdDogeyBSZXNwb25zZTogeyBEZWNpc2lvbjogJ1Blcm1pdCcgfCAnRGVueScgfVtdIH0pID0+IHtcbiAgICAgICAgaWYgKHJlc3VsdCAmJiByZXN1bHQuUmVzcG9uc2UgJiYgcmVzdWx0LlJlc3BvbnNlWzBdKSB7XG4gICAgICAgICAgY29uc3QgcmVzcG9zdGEgPSByZXN1bHQuUmVzcG9uc2VbMF07XG4gICAgICAgICAgaWYgKHJlc3Bvc3RhLkRlY2lzaW9uID09PSAnUGVybWl0Jykge1xuICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgIH0pO1xuICB9XG59XG4iXX0=