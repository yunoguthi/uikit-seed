/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { catchError } from 'rxjs/operators';
import { Injectable, Optional } from '@angular/core';
import { throwError } from 'rxjs';
import { UserService } from './authentication/user.service';
import { LogService } from '../../utils/log/log.service';
export class AuthHttpInterceptor {
    /**
     * @param {?} userService
     * @param {?} logService
     */
    constructor(userService, logService) {
        this.userService = userService;
        this.logService = logService;
    }
    /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    intercept(req, next) {
        /** @type {?} */
        let authReq = req;
        if (!req.headers.has('Authorization')) {
            if (this.userService && this.userService.userValue && this.userService.userValue.authenticated) {
                /** @type {?} */
                const token = ((/** @type {?} */ (this.userService.userValue))).access_token ||
                    ((/** @type {?} */ (this.userService.userValue))).id_token;
                /** @type {?} */
                let httpHeaders = req.headers.set('Authorization', `Bearer ${token}`);
                if (!req.headers.has('content-type')) {
                    httpHeaders = httpHeaders.set('content-type', 'application/json');
                }
                authReq = req.clone({
                    headers: httpHeaders
                });
                if (this.logService) {
                    this.logService.debug(`Não possui 'Authorization' header, foi adicionado.`);
                }
            }
        }
        /** @type {?} */
        const httpHandle = next
            .handle(authReq)
            .pipe(catchError((/**
         * @param {?} error
         * @return {?}
         */
        (error) => {
            if (this.logService) {
                this.logService.error(error);
            }
            // return the error to the method that called it
            return throwError(error);
        })));
        return httpHandle;
    }
}
AuthHttpInterceptor.decorators = [
    { type: Injectable }
];
/** @nocollapse */
AuthHttpInterceptor.ctorParameters = () => [
    { type: UserService },
    { type: LogService, decorators: [{ type: Optional }] }
];
if (false) {
    /**
     * @type {?}
     * @protected
     */
    AuthHttpInterceptor.prototype.userService;
    /**
     * @type {?}
     * @protected
     */
    AuthHttpInterceptor.prototype.logService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC1odHRwLmludGVyY2VwdG9yLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvYXV0aC9hdXRoLWh0dHAuaW50ZXJjZXB0b3IudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUM1QyxPQUFPLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUVyRCxPQUFPLEVBQWMsVUFBVSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQzlDLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUU1RCxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFHekQsTUFBTSxPQUFPLG1CQUFtQjs7Ozs7SUFFOUIsWUFDWSxXQUF3QixFQUNaLFVBQXNCO1FBRGxDLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQ1osZUFBVSxHQUFWLFVBQVUsQ0FBWTtJQUc5QyxDQUFDOzs7Ozs7SUFFRCxTQUFTLENBQUMsR0FBcUIsRUFBRSxJQUFpQjs7WUFFNUMsT0FBTyxHQUFHLEdBQUc7UUFFakIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxFQUFFO1lBQ3JDLElBQUksSUFBSSxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxhQUFhLEVBQUU7O3NCQUN4RixLQUFLLEdBQ1QsQ0FBQyxtQkFBQSxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsRUFBYSxDQUFDLENBQUMsWUFBWTtvQkFDdEQsQ0FBQyxtQkFBQSxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsRUFBcUIsQ0FBQyxDQUFDLFFBQVE7O29CQUV4RCxXQUFXLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsZUFBZSxFQUFFLFVBQVUsS0FBSyxFQUFFLENBQUM7Z0JBRXJFLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsRUFBRTtvQkFDcEMsV0FBVyxHQUFHLFdBQVcsQ0FBQyxHQUFHLENBQUMsY0FBYyxFQUFFLGtCQUFrQixDQUFDLENBQUM7aUJBQ25FO2dCQUVELE9BQU8sR0FBRyxHQUFHLENBQUMsS0FBSyxDQUFDO29CQUNsQixPQUFPLEVBQUUsV0FBVztpQkFDckIsQ0FBQyxDQUFDO2dCQUVILElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtvQkFDbkIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsb0RBQW9ELENBQUMsQ0FBQztpQkFDN0U7YUFDRjtTQUNGOztjQUVLLFVBQVUsR0FBRyxJQUFJO2FBQ3BCLE1BQU0sQ0FBQyxPQUFPLENBQUM7YUFDZixJQUFJLENBQUMsVUFBVTs7OztRQUFDLENBQUMsS0FBSyxFQUFFLEVBQUU7WUFDekIsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO2dCQUNuQixJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUM5QjtZQUNELGdEQUFnRDtZQUNoRCxPQUFPLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMzQixDQUFDLEVBQUMsQ0FBQztRQUVMLE9BQU8sVUFBVSxDQUFDO0lBRXBCLENBQUM7OztZQWhERixVQUFVOzs7O1lBSkYsV0FBVztZQUVYLFVBQVUsdUJBT2QsUUFBUTs7Ozs7OztJQURULDBDQUFrQzs7Ozs7SUFDbEMseUNBQTRDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgY2F0Y2hFcnJvciB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcbmltcG9ydCB7IEluamVjdGFibGUsIE9wdGlvbmFsIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwRXZlbnQsIEh0dHBJbnRlcmNlcHRvciwgSHR0cEhhbmRsZXIsIEh0dHBSZXF1ZXN0LCBIdHRwSGVhZGVycyB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IE9ic2VydmFibGUsIHRocm93RXJyb3IgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSAnLi9hdXRoZW50aWNhdGlvbi91c2VyLnNlcnZpY2UnO1xuaW1wb3J0IHsgT0F1dGhVc2VyLCBPcGVuSURDb25uZWN0VXNlciB9IGZyb20gJy4vYXV0aGVudGljYXRpb24vdXNlci5tb2RlbCc7XG5pbXBvcnQgeyBMb2dTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vdXRpbHMvbG9nL2xvZy5zZXJ2aWNlJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIEF1dGhIdHRwSW50ZXJjZXB0b3IgaW1wbGVtZW50cyBIdHRwSW50ZXJjZXB0b3Ige1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByb3RlY3RlZCB1c2VyU2VydmljZTogVXNlclNlcnZpY2UsXG4gICAgQE9wdGlvbmFsKCkgcHJvdGVjdGVkIGxvZ1NlcnZpY2U6IExvZ1NlcnZpY2VcbiAgKSB7XG5cbiAgfVxuXG4gIGludGVyY2VwdChyZXE6IEh0dHBSZXF1ZXN0PGFueT4sIG5leHQ6IEh0dHBIYW5kbGVyKTogT2JzZXJ2YWJsZTxIdHRwRXZlbnQ8YW55Pj4ge1xuXG4gICAgbGV0IGF1dGhSZXEgPSByZXE7XG5cbiAgICBpZiAoIXJlcS5oZWFkZXJzLmhhcygnQXV0aG9yaXphdGlvbicpKSB7XG4gICAgICBpZiAodGhpcy51c2VyU2VydmljZSAmJiB0aGlzLnVzZXJTZXJ2aWNlLnVzZXJWYWx1ZSAmJiB0aGlzLnVzZXJTZXJ2aWNlLnVzZXJWYWx1ZS5hdXRoZW50aWNhdGVkKSB7XG4gICAgICAgIGNvbnN0IHRva2VuID1cbiAgICAgICAgICAodGhpcy51c2VyU2VydmljZS51c2VyVmFsdWUgYXMgT0F1dGhVc2VyKS5hY2Nlc3NfdG9rZW4gfHxcbiAgICAgICAgICAodGhpcy51c2VyU2VydmljZS51c2VyVmFsdWUgYXMgT3BlbklEQ29ubmVjdFVzZXIpLmlkX3Rva2VuO1xuXG4gICAgICAgIGxldCBodHRwSGVhZGVycyA9IHJlcS5oZWFkZXJzLnNldCgnQXV0aG9yaXphdGlvbicsIGBCZWFyZXIgJHt0b2tlbn1gKTtcblxuICAgICAgICBpZiAoIXJlcS5oZWFkZXJzLmhhcygnY29udGVudC10eXBlJykpIHtcbiAgICAgICAgICBodHRwSGVhZGVycyA9IGh0dHBIZWFkZXJzLnNldCgnY29udGVudC10eXBlJywgJ2FwcGxpY2F0aW9uL2pzb24nKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGF1dGhSZXEgPSByZXEuY2xvbmUoe1xuICAgICAgICAgIGhlYWRlcnM6IGh0dHBIZWFkZXJzXG4gICAgICAgIH0pO1xuXG4gICAgICAgIGlmICh0aGlzLmxvZ1NlcnZpY2UpIHtcbiAgICAgICAgICB0aGlzLmxvZ1NlcnZpY2UuZGVidWcoYE7Do28gcG9zc3VpICdBdXRob3JpemF0aW9uJyBoZWFkZXIsIGZvaSBhZGljaW9uYWRvLmApO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgY29uc3QgaHR0cEhhbmRsZSA9IG5leHRcbiAgICAgIC5oYW5kbGUoYXV0aFJlcSlcbiAgICAgIC5waXBlKGNhdGNoRXJyb3IoKGVycm9yKSA9PiB7XG4gICAgICAgIGlmICh0aGlzLmxvZ1NlcnZpY2UpIHtcbiAgICAgICAgICB0aGlzLmxvZ1NlcnZpY2UuZXJyb3IoZXJyb3IpO1xuICAgICAgICB9XG4gICAgICAgIC8vIHJldHVybiB0aGUgZXJyb3IgdG8gdGhlIG1ldGhvZCB0aGF0IGNhbGxlZCBpdFxuICAgICAgICByZXR1cm4gdGhyb3dFcnJvcihlcnJvcik7XG4gICAgICB9KSk7XG5cbiAgICByZXR1cm4gaHR0cEhhbmRsZTtcblxuICB9XG59XG4iXX0=