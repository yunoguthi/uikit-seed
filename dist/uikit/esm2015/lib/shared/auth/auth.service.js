/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, Optional } from '@angular/core';
import { AuthenticationService } from './authentication/authentication.service';
import { AuthorizationService } from './authorization/authorization.service';
import { UserService } from './authentication/user.service';
export class AuthService {
    /**
     * @param {?} user
     * @param {?=} authentication
     * @param {?=} authorization
     */
    constructor(user, authentication, authorization) {
        this.user = user;
        this.authentication = authentication;
        this.authorization = authorization;
    }
}
AuthService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
AuthService.ctorParameters = () => [
    { type: UserService },
    { type: AuthenticationService, decorators: [{ type: Optional }] },
    { type: AuthorizationService, decorators: [{ type: Optional }] }
];
if (false) {
    /** @type {?} */
    AuthService.prototype.user;
    /** @type {?} */
    AuthService.prototype.authentication;
    /** @type {?} */
    AuthService.prototype.authorization;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvYXV0aC9hdXRoLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRXJELE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQ2hGLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQzdFLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUc1RCxNQUFNLE9BQU8sV0FBVzs7Ozs7O0lBRXRCLFlBQ1MsSUFBaUIsRUFDTCxjQUFzQyxFQUN0QyxhQUFvQztRQUZoRCxTQUFJLEdBQUosSUFBSSxDQUFhO1FBQ0wsbUJBQWMsR0FBZCxjQUFjLENBQXdCO1FBQ3RDLGtCQUFhLEdBQWIsYUFBYSxDQUF1QjtJQUNyRCxDQUFDOzs7WUFQTixVQUFVOzs7O1lBRkYsV0FBVztZQUZYLHFCQUFxQix1QkFTekIsUUFBUTtZQVJKLG9CQUFvQix1QkFTeEIsUUFBUTs7OztJQUZULDJCQUF3Qjs7SUFDeEIscUNBQXlEOztJQUN6RCxvQ0FBdUQiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBPcHRpb25hbCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQXV0aE1vZHVsZSB9IGZyb20gJy4vYXV0aC5tb2R1bGUnO1xuaW1wb3J0IHsgQXV0aGVudGljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi9hdXRoZW50aWNhdGlvbi9hdXRoZW50aWNhdGlvbi5zZXJ2aWNlJztcbmltcG9ydCB7IEF1dGhvcml6YXRpb25TZXJ2aWNlIH0gZnJvbSAnLi9hdXRob3JpemF0aW9uL2F1dGhvcml6YXRpb24uc2VydmljZSc7XG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gJy4vYXV0aGVudGljYXRpb24vdXNlci5zZXJ2aWNlJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIEF1dGhTZXJ2aWNlIHtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwdWJsaWMgdXNlcjogVXNlclNlcnZpY2UsXG4gICAgQE9wdGlvbmFsKCkgcHVibGljIGF1dGhlbnRpY2F0aW9uPzogQXV0aGVudGljYXRpb25TZXJ2aWNlLFxuICAgIEBPcHRpb25hbCgpIHB1YmxpYyBhdXRob3JpemF0aW9uPzogQXV0aG9yaXphdGlvblNlcnZpY2UsXG4gICkgeyB9XG5cblxufVxuIl19