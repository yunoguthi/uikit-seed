/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { UserService } from '../../authentication/user.service';
import { Router } from '@angular/router';
import { PlatformLocation, LocationStrategy } from '@angular/common';
export class ProtectedRouteComponent {
    /**
     * @param {?} userService
     * @param {?} router
     * @param {?} platformLocation
     * @param {?} locationStrategy
     */
    constructor(userService, router, platformLocation, locationStrategy) {
        this.userService = userService;
        this.router = router;
        this.platformLocation = platformLocation;
        this.locationStrategy = locationStrategy;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
ProtectedRouteComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-protected-route',
                template: ``
            }] }
];
/** @nocollapse */
ProtectedRouteComponent.ctorParameters = () => [
    { type: UserService },
    { type: Router },
    { type: PlatformLocation },
    { type: LocationStrategy }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    ProtectedRouteComponent.prototype.userService;
    /**
     * @type {?}
     * @protected
     */
    ProtectedRouteComponent.prototype.router;
    /**
     * @type {?}
     * @private
     */
    ProtectedRouteComponent.prototype.platformLocation;
    /**
     * @type {?}
     * @private
     */
    ProtectedRouteComponent.prototype.locationStrategy;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvdGVjdGVkLXJvdXRlLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2F1dGgvYXV0aGVudGljYXRpb24tb2lkYy9jb21wb25lbnRzL3Byb3RlY3RlZC1yb3V0ZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQXNELE1BQU0sZUFBZSxDQUFDO0FBRTlGLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxtQ0FBbUMsQ0FBQztBQUNoRSxPQUFPLEVBQUUsTUFBTSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDekMsT0FBTyxFQUFFLGdCQUFnQixFQUFFLGdCQUFnQixFQUF3QixNQUFNLGlCQUFpQixDQUFDO0FBUTNGLE1BQU0sT0FBTyx1QkFBdUI7Ozs7Ozs7SUFDbEMsWUFBb0IsV0FBd0IsRUFBWSxNQUFjLEVBQVUsZ0JBQWtDLEVBQ3hHLGdCQUFrQztRQUR4QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUFZLFdBQU0sR0FBTixNQUFNLENBQVE7UUFBVSxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBQ3hHLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7SUFBSSxDQUFDOzs7O0lBRWpELFFBQVE7SUFHUixDQUFDOzs7WUFaRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLHVCQUF1QjtnQkFDakMsUUFBUSxFQUFFLEVBQUU7YUFFYjs7OztZQVRRLFdBQVc7WUFDWCxNQUFNO1lBQ04sZ0JBQWdCO1lBQUUsZ0JBQWdCOzs7Ozs7O0lBUzdCLDhDQUFnQzs7Ozs7SUFBRSx5Q0FBd0I7Ozs7O0lBQUUsbURBQTBDOzs7OztJQUNoSCxtREFBMEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgVmlld0VuY2Fwc3VsYXRpb24sIENoYW5nZURldGVjdGlvblN0cmF0ZWd5IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBBdXRoZW50aWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi9hdXRoZW50aWNhdGlvbi9hdXRoZW50aWNhdGlvbi5zZXJ2aWNlJztcbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vYXV0aGVudGljYXRpb24vdXNlci5zZXJ2aWNlJztcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQgeyBQbGF0Zm9ybUxvY2F0aW9uLCBMb2NhdGlvblN0cmF0ZWd5LCBIYXNoTG9jYXRpb25TdHJhdGVneSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5cblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAndWlraXQtcHJvdGVjdGVkLXJvdXRlJyxcbiAgdGVtcGxhdGU6IGBgLFxuICBzdHlsZXM6IFtdLFxufSlcbmV4cG9ydCBjbGFzcyBQcm90ZWN0ZWRSb3V0ZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgdXNlclNlcnZpY2U6IFVzZXJTZXJ2aWNlLCBwcm90ZWN0ZWQgcm91dGVyOiBSb3V0ZXIsIHByaXZhdGUgcGxhdGZvcm1Mb2NhdGlvbjogUGxhdGZvcm1Mb2NhdGlvbixcbiAgICBwcml2YXRlIGxvY2F0aW9uU3RyYXRlZ3k6IExvY2F0aW9uU3RyYXRlZ3kpIHsgfVxuXG4gIG5nT25Jbml0KCkge1xuXG5cbiAgfVxuXG5cblxuXG59XG4iXX0=