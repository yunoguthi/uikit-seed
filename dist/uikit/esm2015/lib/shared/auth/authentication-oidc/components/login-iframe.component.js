/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ApplicationRef, Inject } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ToastService } from '../../../../layout/toast/toast.service';
import { WebStorageStateStore, UserManager } from 'oidc-client';
import { IAuthenticationServiceToken } from '../../authentication/abstraction/provider-authentication-service.token';
import { timer, Subscription, BehaviorSubject } from 'rxjs';
import { UserService } from '../../authentication/user.service';
import { Router } from '@angular/router';
import { HashLocationStrategy, PlatformLocation, LocationStrategy } from '@angular/common';
export class LoginIframeComponent {
    /**
     * @param {?} sanitizer
     * @param {?} applicationRef
     * @param {?} toastService
     * @param {?} providerAuthenticationService
     * @param {?} userService
     * @param {?} router
     * @param {?} platformLocation
     * @param {?} locationStrategy
     */
    constructor(sanitizer, applicationRef, toastService, providerAuthenticationService, userService, router, platformLocation, locationStrategy) {
        this.sanitizer = sanitizer;
        this.applicationRef = applicationRef;
        this.toastService = toastService;
        this.providerAuthenticationService = providerAuthenticationService;
        this.userService = userService;
        this.router = router;
        this.platformLocation = platformLocation;
        this.locationStrategy = locationStrategy;
        this.contador = 0;
        this.isLoading$ = new BehaviorSubject(true);
        this.subscription = new Subscription();
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.url = this.sanitizer.bypassSecurityTrustResourceUrl('./protected-route');
        this.toastService.loading(this.isLoading$);
    }
    /**
     * @param {?} myIframe
     * @return {?}
     */
    onLoad(myIframe) {
        this.isLoading$.next(false);
        ((/** @type {?} */ (myIframe))).onloadstart = (/**
         * @return {?}
         */
        () => {
            // console.log('start loading iframe');
            this.isLoading$.next(true);
        });
        // console.log(`A página do IFrame terminou de carregar!`);
        /** @type {?} */
        const oidcProviderAuthenticationService = (/** @type {?} */ ((/** @type {?} */ (this.providerAuthenticationService))));
        /** @type {?} */
        const userManagerArg = Object.assign({}, oidcProviderAuthenticationService.authenticationSettings, { userStore: new WebStorageStateStore({ store: window.localStorage }) });
        /** @type {?} */
        const userManager = new UserManager(userManagerArg);
        this.subscription.add(timer(0, 100).subscribe((/**
         * @return {?}
         */
        () => {
            userManager.getUser().then((/**
             * @param {?} user
             * @return {?}
             */
            user => {
                // console.log(`Usuário encontrado: `, user);
                if (user) {
                    oidcProviderAuthenticationService.loadOidcUser(user);
                    // console.log(`Setando usuário encontrado: `, user);
                    // const usuario = oidcProviderAuthenticationService.transform(user);
                    // this.userService.load(usuario);
                    // this.goToLastUri();
                    // oidcProviderAuthenticationService.user$.subscribe(usuario => {
                    //   console.log(`Carregando usuário encontrado: `, user);
                    //   this.userService.load(usuario);
                    // });
                }
            }));
        })));
        // if(this.oidcAuthenticationService.user$.subscribe(a => a.authenticated))
        // this.oidcAuthenticationService.renewUser().then(() => this.applicationRef.tick());
        // const currentIframePage = null;
        // try {
        //   const currentIframePage = (myIframe as HTMLIFrameElement).contentWindow.location.href;
        // } catch (error) {
        //   this.toastService.error('Não foi possivel obter a informação da página do IFrame!')
        // }
        // console.log(`A página '${currentIframePage}' do IFrame terminou de carregar!`);
        // // this.logService && this.logService.debug();
        // // Caso seja callback, então deve
        // if (currentIframePage.indexOf('/callback') !== -1) {
        //   // Atualizar a aplicação
        //   // this.logService && this.logService.debug(`Atualizando a aplicação!`);
        //   console.log('Atualizando a aplicação!');
        //   this.applicationRef.tick();
        // }
    }
    // TODO: Este método está repetido, devo criar um service para ele (AuthenticationService)
    /**
     * @protected
     * @return {?}
     */
    goToLastUri() {
        /** @type {?} */
        const lastUri = localStorage.getItem('authentication-callback');
        /** @type {?} */
        const baseHref = this.platformLocation.getBaseHrefFromDOM();
        /** @type {?} */
        const origin = window.location.origin;
        /** @type {?} */
        let hashStrategy = '';
        if (this.locationStrategy instanceof HashLocationStrategy) {
            hashStrategy = '#';
        }
        /** @type {?} */
        const completeUrlToBaseHref = origin + baseHref + hashStrategy;
        if (lastUri && (lastUri.indexOf('protected-route') == -1)) {
            /** @type {?} */
            const uriToNavigate = lastUri.replace(completeUrlToBaseHref, '');
            // let uriToNavigate = lastUri;
            // if (uriToNavigate.startsWith(baseHref)) {
            //   uriToNavigate = uriToNavigate.replace(baseHref, '');
            // }
            localStorage.removeItem('authentication-callback');
            console.debug('Navegando para página: ', lastUri);
            this.router.navigateByUrl(uriToNavigate);
        }
        else {
            this.router.navigateByUrl('');
        }
    }
}
LoginIframeComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-login-iframe',
                template: `
  <div class="row-container">
    <iframe [hidden]="(isLoading$ | async)" #myIframe [src]="url ? url : null" frameBorder="0" (load)="url ? onLoad(myIframe) : null" class="second-row"></iframe>
  </div>
  `,
                styles: ['.iframe { height: 100%; width: 100% border: none; }',
                    '.row-container {display: flex; width: 100%; height: 100%; flex-direction: column; overflow: hidden;}',
                    '.first-row {background-color: lime; }',
                    '.second-row { flex-grow: 1; border: none; margin: 0; padding: 0; }']
            }] }
];
/** @nocollapse */
LoginIframeComponent.ctorParameters = () => [
    { type: DomSanitizer },
    { type: ApplicationRef },
    { type: ToastService },
    { type: undefined, decorators: [{ type: Inject, args: [IAuthenticationServiceToken,] }] },
    { type: UserService },
    { type: Router },
    { type: PlatformLocation },
    { type: LocationStrategy }
];
if (false) {
    /** @type {?} */
    LoginIframeComponent.prototype.url;
    /** @type {?} */
    LoginIframeComponent.prototype.contador;
    /** @type {?} */
    LoginIframeComponent.prototype.isLoading$;
    /**
     * @type {?}
     * @private
     */
    LoginIframeComponent.prototype.subscription;
    /**
     * @type {?}
     * @private
     */
    LoginIframeComponent.prototype.sanitizer;
    /**
     * @type {?}
     * @protected
     */
    LoginIframeComponent.prototype.applicationRef;
    /**
     * @type {?}
     * @protected
     */
    LoginIframeComponent.prototype.toastService;
    /**
     * @type {?}
     * @private
     */
    LoginIframeComponent.prototype.providerAuthenticationService;
    /**
     * @type {?}
     * @protected
     */
    LoginIframeComponent.prototype.userService;
    /**
     * @type {?}
     * @protected
     */
    LoginIframeComponent.prototype.router;
    /**
     * @type {?}
     * @private
     */
    LoginIframeComponent.prototype.platformLocation;
    /**
     * @type {?}
     * @private
     */
    LoginIframeComponent.prototype.locationStrategy;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4taWZyYW1lLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2F1dGgvYXV0aGVudGljYXRpb24tb2lkYy9jb21wb25lbnRzL2xvZ2luLWlmcmFtZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsY0FBYyxFQUFFLE1BQU0sRUFBYSxNQUFNLGVBQWUsQ0FBQztBQUNyRixPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFFekQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBRXRFLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxXQUFXLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFDaEUsT0FBTyxFQUFFLDJCQUEyQixFQUFFLE1BQU0sd0VBQXdFLENBQUM7QUFFckgsT0FBTyxFQUFFLEtBQUssRUFBRSxZQUFZLEVBQUUsZUFBZSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBRTVELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxtQ0FBbUMsQ0FBQztBQUNoRSxPQUFPLEVBQUUsTUFBTSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDekMsT0FBTyxFQUFFLG9CQUFvQixFQUFFLGdCQUFnQixFQUFFLGdCQUFnQixFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFnQjNGLE1BQU0sT0FBTyxvQkFBb0I7Ozs7Ozs7Ozs7O0lBUy9CLFlBQW9CLFNBQXVCLEVBQVksY0FBOEIsRUFBWSxZQUEwQixFQUM1RSw2QkFBNkQsRUFDaEcsV0FBd0IsRUFBWSxNQUFjLEVBQVUsZ0JBQWtDLEVBQ2hHLGdCQUFrQztRQUh4QixjQUFTLEdBQVQsU0FBUyxDQUFjO1FBQVksbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQVksaUJBQVksR0FBWixZQUFZLENBQWM7UUFDNUUsa0NBQTZCLEdBQTdCLDZCQUE2QixDQUFnQztRQUNoRyxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUFZLFdBQU0sR0FBTixNQUFNLENBQVE7UUFBVSxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBQ2hHLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFUNUMsYUFBUSxHQUFHLENBQUMsQ0FBQztRQUVOLGVBQVUsR0FBRyxJQUFJLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUV0QyxpQkFBWSxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7SUFPdEMsQ0FBQzs7OztJQUNMLFdBQVc7UUFDVCxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQ2xDLENBQUM7Ozs7SUFFRCxRQUFRO1FBQ04sSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLDhCQUE4QixDQUFDLG1CQUFtQixDQUFDLENBQUM7UUFDOUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQzdDLENBQUM7Ozs7O0lBRUQsTUFBTSxDQUFDLFFBQVE7UUFDYixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUU1QixDQUFDLG1CQUFBLFFBQVEsRUFBcUIsQ0FBQyxDQUFDLFdBQVc7OztRQUFHLEdBQUcsRUFBRTtZQUNqRCx1Q0FBdUM7WUFDdkMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDN0IsQ0FBQyxDQUFBLENBQUM7OztjQUlJLGlDQUFpQyxHQUFHLG1CQUFBLG1CQUFBLElBQUksQ0FBQyw2QkFBNkIsRUFBVyxFQUE2Qjs7Y0FFOUcsY0FBYyxxQkFDZixpQ0FBaUMsQ0FBQyxzQkFBc0IsSUFDM0QsU0FBUyxFQUFFLElBQUksb0JBQW9CLENBQUMsRUFBRSxLQUFLLEVBQUUsTUFBTSxDQUFDLFlBQVksRUFBRSxDQUFDLEdBQ3BFOztjQUNLLFdBQVcsR0FBRyxJQUFJLFdBQVcsQ0FBQyxjQUFjLENBQUM7UUFDbkQsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxTQUFTOzs7UUFBQyxHQUFHLEVBQUU7WUFDakQsV0FBVyxDQUFDLE9BQU8sRUFBRSxDQUFDLElBQUk7Ozs7WUFBQyxJQUFJLENBQUMsRUFBRTtnQkFDaEMsNkNBQTZDO2dCQUM3QyxJQUFJLElBQUksRUFBRTtvQkFDUixpQ0FBaUMsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ3JELHFEQUFxRDtvQkFDckQscUVBQXFFO29CQUNyRSxrQ0FBa0M7b0JBQ2xDLHNCQUFzQjtvQkFFdEIsaUVBQWlFO29CQUNqRSwwREFBMEQ7b0JBQzFELG9DQUFvQztvQkFDcEMsTUFBTTtpQkFDUDtZQUNILENBQUMsRUFBQyxDQUFDO1FBQ0wsQ0FBQyxFQUFDLENBQUMsQ0FBQztRQUtKLDJFQUEyRTtRQUMzRSxxRkFBcUY7UUFFckYsa0NBQWtDO1FBQ2xDLFFBQVE7UUFDUiwyRkFBMkY7UUFDM0Ysb0JBQW9CO1FBQ3BCLHdGQUF3RjtRQUN4RixJQUFJO1FBRUosa0ZBQWtGO1FBQ2xGLGlEQUFpRDtRQUNqRCxvQ0FBb0M7UUFDcEMsdURBQXVEO1FBQ3ZELDZCQUE2QjtRQUM3Qiw2RUFBNkU7UUFDN0UsNkNBQTZDO1FBQzdDLGdDQUFnQztRQUNoQyxJQUFJO0lBQ04sQ0FBQzs7Ozs7O0lBR1MsV0FBVzs7Y0FDYixPQUFPLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyx5QkFBeUIsQ0FBQzs7Y0FHekQsUUFBUSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxrQkFBa0IsRUFBRTs7Y0FDckQsTUFBTSxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTTs7WUFDakMsWUFBWSxHQUFHLEVBQUU7UUFDckIsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLFlBQVksb0JBQW9CLEVBQUU7WUFDekQsWUFBWSxHQUFHLEdBQUcsQ0FBQztTQUNwQjs7Y0FDSyxxQkFBcUIsR0FBRyxNQUFNLEdBQUcsUUFBUSxHQUFHLFlBQVk7UUFHOUQsSUFBSSxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRTs7a0JBSW5ELGFBQWEsR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLHFCQUFxQixFQUFFLEVBQUUsQ0FBQztZQUdoRSwrQkFBK0I7WUFDL0IsNENBQTRDO1lBQzVDLHlEQUF5RDtZQUN6RCxJQUFJO1lBRUosWUFBWSxDQUFDLFVBQVUsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1lBQ25ELE9BQU8sQ0FBQyxLQUFLLENBQUMseUJBQXlCLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDbEQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLENBQUM7U0FDMUM7YUFBTTtZQUNMLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1NBQy9CO0lBQ0gsQ0FBQzs7O1lBaklGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsb0JBQW9CO2dCQUM5QixRQUFRLEVBQUU7Ozs7R0FJVDt5QkFFQyxxREFBcUQ7b0JBQ3JELHNHQUFzRztvQkFDdEcsdUNBQXVDO29CQUN2QyxvRUFBb0U7YUFFdkU7Ozs7WUExQlEsWUFBWTtZQURPLGNBQWM7WUFHakMsWUFBWTs0Q0FtQ2hCLE1BQU0sU0FBQywyQkFBMkI7WUE1QjlCLFdBQVc7WUFDWCxNQUFNO1lBQ2dCLGdCQUFnQjtZQUFFLGdCQUFnQjs7OztJQWlCL0QsbUNBQWdCOztJQUVoQix3Q0FBYTs7SUFFYiwwQ0FBOEM7Ozs7O0lBRTlDLDRDQUEwQzs7Ozs7SUFFOUIseUNBQStCOzs7OztJQUFFLDhDQUF3Qzs7Ozs7SUFBRSw0Q0FBb0M7Ozs7O0lBQ3pILDZEQUEwRzs7Ozs7SUFDMUcsMkNBQWtDOzs7OztJQUFFLHNDQUF3Qjs7Ozs7SUFBRSxnREFBMEM7Ozs7O0lBQ3hHLGdEQUEwQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBBcHBsaWNhdGlvblJlZiwgSW5qZWN0LCBPbkRlc3Ryb3kgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IERvbVNhbml0aXplciB9IGZyb20gJ0Bhbmd1bGFyL3BsYXRmb3JtLWJyb3dzZXInO1xuaW1wb3J0IHsgTG9nU2VydmljZSB9IGZyb20gJy4vLi4vLi4vLi4vLi4vdXRpbHMvbG9nL2xvZy5zZXJ2aWNlJztcbmltcG9ydCB7IFRvYXN0U2VydmljZSB9IGZyb20gJy4uLy4uLy4uLy4uL2xheW91dC90b2FzdC90b2FzdC5zZXJ2aWNlJztcbmltcG9ydCB7IE9pZGNBdXRoZW50aWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi9vaWRjLWF1dGhlbnRpY2F0aW9uLnNlcnZpY2UnO1xuaW1wb3J0IHsgV2ViU3RvcmFnZVN0YXRlU3RvcmUsIFVzZXJNYW5hZ2VyIH0gZnJvbSAnb2lkYy1jbGllbnQnO1xuaW1wb3J0IHsgSUF1dGhlbnRpY2F0aW9uU2VydmljZVRva2VuIH0gZnJvbSAnLi4vLi4vYXV0aGVudGljYXRpb24vYWJzdHJhY3Rpb24vcHJvdmlkZXItYXV0aGVudGljYXRpb24tc2VydmljZS50b2tlbic7XG5pbXBvcnQgeyBJUHJvdmlkZXJBdXRoZW50aWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi9hdXRoZW50aWNhdGlvbi9hYnN0cmFjdGlvbi9wcm92aWRlci1hdXRoZW50aWNhdGlvbi5zZXJ2aWNlJztcbmltcG9ydCB7IHRpbWVyLCBTdWJzY3JpcHRpb24sIEJlaGF2aW9yU3ViamVjdCB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgQXV0aGVudGljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vYXV0aGVudGljYXRpb24vYXV0aGVudGljYXRpb24uc2VydmljZSc7XG5pbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gJy4uLy4uL2F1dGhlbnRpY2F0aW9uL3VzZXIuc2VydmljZSc7XG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHsgSGFzaExvY2F0aW9uU3RyYXRlZ3ksIFBsYXRmb3JtTG9jYXRpb24sIExvY2F0aW9uU3RyYXRlZ3kgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICd1aWtpdC1sb2dpbi1pZnJhbWUnLFxuICB0ZW1wbGF0ZTogYFxuICA8ZGl2IGNsYXNzPVwicm93LWNvbnRhaW5lclwiPlxuICAgIDxpZnJhbWUgW2hpZGRlbl09XCIoaXNMb2FkaW5nJCB8IGFzeW5jKVwiICNteUlmcmFtZSBbc3JjXT1cInVybCA/IHVybCA6IG51bGxcIiBmcmFtZUJvcmRlcj1cIjBcIiAobG9hZCk9XCJ1cmwgPyBvbkxvYWQobXlJZnJhbWUpIDogbnVsbFwiIGNsYXNzPVwic2Vjb25kLXJvd1wiPjwvaWZyYW1lPlxuICA8L2Rpdj5cbiAgYCxcbiAgc3R5bGVzOiBbXG4gICAgJy5pZnJhbWUgeyBoZWlnaHQ6IDEwMCU7IHdpZHRoOiAxMDAlIGJvcmRlcjogbm9uZTsgfScsXG4gICAgJy5yb3ctY29udGFpbmVyIHtkaXNwbGF5OiBmbGV4OyB3aWR0aDogMTAwJTsgaGVpZ2h0OiAxMDAlOyBmbGV4LWRpcmVjdGlvbjogY29sdW1uOyBvdmVyZmxvdzogaGlkZGVuO30nLFxuICAgICcuZmlyc3Qtcm93IHtiYWNrZ3JvdW5kLWNvbG9yOiBsaW1lOyB9JyxcbiAgICAnLnNlY29uZC1yb3cgeyBmbGV4LWdyb3c6IDE7IGJvcmRlcjogbm9uZTsgbWFyZ2luOiAwOyBwYWRkaW5nOiAwOyB9J1xuICBdLFxufSlcbmV4cG9ydCBjbGFzcyBMb2dpbklmcmFtZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcbiAgcHVibGljIHVybDogYW55O1xuXG4gIGNvbnRhZG9yID0gMDtcblxuICBwdWJsaWMgaXNMb2FkaW5nJCA9IG5ldyBCZWhhdmlvclN1YmplY3QodHJ1ZSk7XG5cbiAgcHJpdmF0ZSBzdWJzY3JpcHRpb24gPSBuZXcgU3Vic2NyaXB0aW9uKCk7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBzYW5pdGl6ZXI6IERvbVNhbml0aXplciwgcHJvdGVjdGVkIGFwcGxpY2F0aW9uUmVmOiBBcHBsaWNhdGlvblJlZiwgcHJvdGVjdGVkIHRvYXN0U2VydmljZTogVG9hc3RTZXJ2aWNlLFxuICAgIEBJbmplY3QoSUF1dGhlbnRpY2F0aW9uU2VydmljZVRva2VuKSBwcml2YXRlIHByb3ZpZGVyQXV0aGVudGljYXRpb25TZXJ2aWNlOiBJUHJvdmlkZXJBdXRoZW50aWNhdGlvblNlcnZpY2UsXG4gICAgcHJvdGVjdGVkIHVzZXJTZXJ2aWNlOiBVc2VyU2VydmljZSwgcHJvdGVjdGVkIHJvdXRlcjogUm91dGVyLCBwcml2YXRlIHBsYXRmb3JtTG9jYXRpb246IFBsYXRmb3JtTG9jYXRpb24sXG4gICAgcHJpdmF0ZSBsb2NhdGlvblN0cmF0ZWd5OiBMb2NhdGlvblN0cmF0ZWd5XG5cbiAgKSB7IH1cbiAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XG4gICAgdGhpcy5zdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIHRoaXMudXJsID0gdGhpcy5zYW5pdGl6ZXIuYnlwYXNzU2VjdXJpdHlUcnVzdFJlc291cmNlVXJsKCcuL3Byb3RlY3RlZC1yb3V0ZScpO1xuICAgIHRoaXMudG9hc3RTZXJ2aWNlLmxvYWRpbmcodGhpcy5pc0xvYWRpbmckKTtcbiAgfVxuXG4gIG9uTG9hZChteUlmcmFtZSkge1xuICAgIHRoaXMuaXNMb2FkaW5nJC5uZXh0KGZhbHNlKTtcblxuICAgIChteUlmcmFtZSBhcyBIVE1MSUZyYW1lRWxlbWVudCkub25sb2Fkc3RhcnQgPSAoKSA9PiB7XG4gICAgICAvLyBjb25zb2xlLmxvZygnc3RhcnQgbG9hZGluZyBpZnJhbWUnKTtcbiAgICAgIHRoaXMuaXNMb2FkaW5nJC5uZXh0KHRydWUpO1xuICAgIH07XG5cblxuICAgIC8vIGNvbnNvbGUubG9nKGBBIHDDoWdpbmEgZG8gSUZyYW1lIHRlcm1pbm91IGRlIGNhcnJlZ2FyIWApO1xuICAgIGNvbnN0IG9pZGNQcm92aWRlckF1dGhlbnRpY2F0aW9uU2VydmljZSA9IHRoaXMucHJvdmlkZXJBdXRoZW50aWNhdGlvblNlcnZpY2UgYXMgdW5rbm93biBhcyBPaWRjQXV0aGVudGljYXRpb25TZXJ2aWNlO1xuXG4gICAgY29uc3QgdXNlck1hbmFnZXJBcmcgPSB7XG4gICAgICAuLi5vaWRjUHJvdmlkZXJBdXRoZW50aWNhdGlvblNlcnZpY2UuYXV0aGVudGljYXRpb25TZXR0aW5ncyxcbiAgICAgIHVzZXJTdG9yZTogbmV3IFdlYlN0b3JhZ2VTdGF0ZVN0b3JlKHsgc3RvcmU6IHdpbmRvdy5sb2NhbFN0b3JhZ2UgfSlcbiAgICB9O1xuICAgIGNvbnN0IHVzZXJNYW5hZ2VyID0gbmV3IFVzZXJNYW5hZ2VyKHVzZXJNYW5hZ2VyQXJnKTtcbiAgICB0aGlzLnN1YnNjcmlwdGlvbi5hZGQodGltZXIoMCwgMTAwKS5zdWJzY3JpYmUoKCkgPT4ge1xuICAgICAgdXNlck1hbmFnZXIuZ2V0VXNlcigpLnRoZW4odXNlciA9PiB7XG4gICAgICAgIC8vIGNvbnNvbGUubG9nKGBVc3XDoXJpbyBlbmNvbnRyYWRvOiBgLCB1c2VyKTtcbiAgICAgICAgaWYgKHVzZXIpIHtcbiAgICAgICAgICBvaWRjUHJvdmlkZXJBdXRoZW50aWNhdGlvblNlcnZpY2UubG9hZE9pZGNVc2VyKHVzZXIpO1xuICAgICAgICAgIC8vIGNvbnNvbGUubG9nKGBTZXRhbmRvIHVzdcOhcmlvIGVuY29udHJhZG86IGAsIHVzZXIpO1xuICAgICAgICAgIC8vIGNvbnN0IHVzdWFyaW8gPSBvaWRjUHJvdmlkZXJBdXRoZW50aWNhdGlvblNlcnZpY2UudHJhbnNmb3JtKHVzZXIpO1xuICAgICAgICAgIC8vIHRoaXMudXNlclNlcnZpY2UubG9hZCh1c3VhcmlvKTtcbiAgICAgICAgICAvLyB0aGlzLmdvVG9MYXN0VXJpKCk7XG5cbiAgICAgICAgICAvLyBvaWRjUHJvdmlkZXJBdXRoZW50aWNhdGlvblNlcnZpY2UudXNlciQuc3Vic2NyaWJlKHVzdWFyaW8gPT4ge1xuICAgICAgICAgIC8vICAgY29uc29sZS5sb2coYENhcnJlZ2FuZG8gdXN1w6FyaW8gZW5jb250cmFkbzogYCwgdXNlcik7XG4gICAgICAgICAgLy8gICB0aGlzLnVzZXJTZXJ2aWNlLmxvYWQodXN1YXJpbyk7XG4gICAgICAgICAgLy8gfSk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH0pKTtcblxuXG5cblxuICAgIC8vIGlmKHRoaXMub2lkY0F1dGhlbnRpY2F0aW9uU2VydmljZS51c2VyJC5zdWJzY3JpYmUoYSA9PiBhLmF1dGhlbnRpY2F0ZWQpKVxuICAgIC8vIHRoaXMub2lkY0F1dGhlbnRpY2F0aW9uU2VydmljZS5yZW5ld1VzZXIoKS50aGVuKCgpID0+IHRoaXMuYXBwbGljYXRpb25SZWYudGljaygpKTtcblxuICAgIC8vIGNvbnN0IGN1cnJlbnRJZnJhbWVQYWdlID0gbnVsbDtcbiAgICAvLyB0cnkge1xuICAgIC8vICAgY29uc3QgY3VycmVudElmcmFtZVBhZ2UgPSAobXlJZnJhbWUgYXMgSFRNTElGcmFtZUVsZW1lbnQpLmNvbnRlbnRXaW5kb3cubG9jYXRpb24uaHJlZjtcbiAgICAvLyB9IGNhdGNoIChlcnJvcikge1xuICAgIC8vICAgdGhpcy50b2FzdFNlcnZpY2UuZXJyb3IoJ07Do28gZm9pIHBvc3NpdmVsIG9idGVyIGEgaW5mb3JtYcOnw6NvIGRhIHDDoWdpbmEgZG8gSUZyYW1lIScpXG4gICAgLy8gfVxuXG4gICAgLy8gY29uc29sZS5sb2coYEEgcMOhZ2luYSAnJHtjdXJyZW50SWZyYW1lUGFnZX0nIGRvIElGcmFtZSB0ZXJtaW5vdSBkZSBjYXJyZWdhciFgKTtcbiAgICAvLyAvLyB0aGlzLmxvZ1NlcnZpY2UgJiYgdGhpcy5sb2dTZXJ2aWNlLmRlYnVnKCk7XG4gICAgLy8gLy8gQ2FzbyBzZWphIGNhbGxiYWNrLCBlbnTDo28gZGV2ZVxuICAgIC8vIGlmIChjdXJyZW50SWZyYW1lUGFnZS5pbmRleE9mKCcvY2FsbGJhY2snKSAhPT0gLTEpIHtcbiAgICAvLyAgIC8vIEF0dWFsaXphciBhIGFwbGljYcOnw6NvXG4gICAgLy8gICAvLyB0aGlzLmxvZ1NlcnZpY2UgJiYgdGhpcy5sb2dTZXJ2aWNlLmRlYnVnKGBBdHVhbGl6YW5kbyBhIGFwbGljYcOnw6NvIWApO1xuICAgIC8vICAgY29uc29sZS5sb2coJ0F0dWFsaXphbmRvIGEgYXBsaWNhw6fDo28hJyk7XG4gICAgLy8gICB0aGlzLmFwcGxpY2F0aW9uUmVmLnRpY2soKTtcbiAgICAvLyB9XG4gIH1cblxuICAvLyBUT0RPOiBFc3RlIG3DqXRvZG8gZXN0w6EgcmVwZXRpZG8sIGRldm8gY3JpYXIgdW0gc2VydmljZSBwYXJhIGVsZSAoQXV0aGVudGljYXRpb25TZXJ2aWNlKVxuICBwcm90ZWN0ZWQgZ29Ub0xhc3RVcmkoKSB7XG4gICAgY29uc3QgbGFzdFVyaSA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdhdXRoZW50aWNhdGlvbi1jYWxsYmFjaycpO1xuXG5cbiAgICBjb25zdCBiYXNlSHJlZiA9IHRoaXMucGxhdGZvcm1Mb2NhdGlvbi5nZXRCYXNlSHJlZkZyb21ET00oKTtcbiAgICBjb25zdCBvcmlnaW4gPSB3aW5kb3cubG9jYXRpb24ub3JpZ2luO1xuICAgIGxldCBoYXNoU3RyYXRlZ3kgPSAnJztcbiAgICBpZiAodGhpcy5sb2NhdGlvblN0cmF0ZWd5IGluc3RhbmNlb2YgSGFzaExvY2F0aW9uU3RyYXRlZ3kpIHtcbiAgICAgIGhhc2hTdHJhdGVneSA9ICcjJztcbiAgICB9XG4gICAgY29uc3QgY29tcGxldGVVcmxUb0Jhc2VIcmVmID0gb3JpZ2luICsgYmFzZUhyZWYgKyBoYXNoU3RyYXRlZ3k7XG5cblxuICAgIGlmIChsYXN0VXJpICYmIChsYXN0VXJpLmluZGV4T2YoJ3Byb3RlY3RlZC1yb3V0ZScpID09IC0xKSkge1xuXG5cblxuICAgICAgY29uc3QgdXJpVG9OYXZpZ2F0ZSA9IGxhc3RVcmkucmVwbGFjZShjb21wbGV0ZVVybFRvQmFzZUhyZWYsICcnKTtcblxuXG4gICAgICAvLyBsZXQgdXJpVG9OYXZpZ2F0ZSA9IGxhc3RVcmk7XG4gICAgICAvLyBpZiAodXJpVG9OYXZpZ2F0ZS5zdGFydHNXaXRoKGJhc2VIcmVmKSkge1xuICAgICAgLy8gICB1cmlUb05hdmlnYXRlID0gdXJpVG9OYXZpZ2F0ZS5yZXBsYWNlKGJhc2VIcmVmLCAnJyk7XG4gICAgICAvLyB9XG5cbiAgICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKCdhdXRoZW50aWNhdGlvbi1jYWxsYmFjaycpO1xuICAgICAgY29uc29sZS5kZWJ1ZygnTmF2ZWdhbmRvIHBhcmEgcMOhZ2luYTogJywgbGFzdFVyaSk7XG4gICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZUJ5VXJsKHVyaVRvTmF2aWdhdGUpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZUJ5VXJsKCcnKTtcbiAgICB9XG4gIH1cblxufVxuIl19