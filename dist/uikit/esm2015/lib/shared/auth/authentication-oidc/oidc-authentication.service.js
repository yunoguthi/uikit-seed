/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { Router, NavigationStart, ActivatedRoute } from '@angular/router';
import { filter } from 'rxjs/operators';
import { UserManager, WebStorageStateStore } from 'oidc-client';
import { ProviderAuthenticationService } from '../authentication/provider-authentication.service';
import { PlatformLocation, LocationStrategy, } from '@angular/common';
// @dynamic
export class OidcAuthenticationService extends ProviderAuthenticationService {
    /**
     * @param {?} authenticationSettings
     * @param {?} router
     * @param {?} activatedRoute
     * @param {?} platformLocation
     * @param {?} locationStrategy
     */
    constructor(authenticationSettings, router, activatedRoute, platformLocation, locationStrategy) {
        super();
        this.authenticationSettings = authenticationSettings;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.platformLocation = platformLocation;
        this.locationStrategy = locationStrategy;
        this.userManager = null;
        this.userValue = null;
        /** @type {?} */
        const userManagerArg = Object.assign({}, authenticationSettings, { userStore: new WebStorageStateStore({ store: window.localStorage }) });
        this.userManager = new UserManager(userManagerArg);
        this.userManager.events.addUserLoaded((/**
         * @param {?} user
         * @return {?}
         */
        (user) => {
            this.loadOidcUser(user);
        }));
        this.userManager.events.addAccessTokenExpired((/**
         * @param {?} event
         * @return {?}
         */
        (event) => {
            this.userManager.signinSilent().catch((/**
             * @param {?} error
             * @return {?}
             */
            (error) => {
                console.error(error);
                this.userManager.removeUser();
                super.logout();
            }));
        }));
        this.userManager.events.addSilentRenewError((/**
         * @param {?} error
         * @return {?}
         */
        (error) => {
            console.error(error);
            this.userManager.removeUser();
            super.logout();
        }));
        this.userManager.getUser().then((/**
         * @param {?} user
         * @return {?}
         */
        (user) => {
            this.loadOidcUser(user);
        }));
        if (this.authenticationSettings.automaticSilentRenew) {
            this.router.events
                .pipe(filter((/**
             * @param {?} event
             * @return {?}
             */
            (event) => event instanceof NavigationStart)))
                .pipe(filter((/**
             * @param {?} event
             * @return {?}
             */
            (event) => new RegExp(this.authenticationSettings.silent_redirect_uri).test(location.href))))
                .subscribe((/**
             * @param {?} event
             * @return {?}
             */
            (event) => {
                this.userManager.signinSilentCallback().catch((/**
                 * @param {?} err
                 * @return {?}
                 */
                (err) => {
                    console.error(err);
                    this.userManager.startSilentRenew();
                }));
            }));
        }
        this.router.events
            .pipe(filter((/**
         * @param {?} event
         * @return {?}
         */
        (event) => new RegExp(this.authenticationSettings.redirect_uri).test(location.href))))
            .subscribe((/**
         * @param {?} event
         * @return {?}
         */
        (event) => {
            this.userManager.signinRedirectCallback();
        }));
    }
    /**
     * @return {?}
     */
    get needRenewUser() {
        return this.userValue != null && this.userValue.expired === true;
    }
    /**
     * @return {?}
     */
    renewUser() {
        return this.userManager
            .signinSilent()
            .then((/**
         * @param {?} user
         * @return {?}
         */
        (user) => (/**
         * @return {?}
         */
        () => this.loadOidcUser(user))));
    }
    /**
     * @param {?} user
     * @return {?}
     */
    loadOidcUser(user) {
        this.userValue = user;
        this.loadProviderUser(user);
    }
    /**
     * @param {?} user
     * @return {?}
     */
    transform(user) {
        /** @type {?} */
        const destinationUser = (/** @type {?} */ ({}));
        /** @type {?} */
        let combinedUser = Object.assign(destinationUser, user);
        // O OIDC_Client usa a propriedade profile para colocar claims,
        // portanto devemos fazer o assign deste com o objetivo de manter
        // no usuário também as informações 'não mapeadas'
        if (user.profile) {
            combinedUser = Object.assign(combinedUser, user.profile);
        }
        combinedUser.authenticated = true;
        combinedUser.sub = user.profile.sub || null;
        combinedUser.name = user.profile.name || null;
        combinedUser.preferred_username = user.profile.preferred_username || null;
        combinedUser.given_name = user.profile.given_name || null;
        combinedUser.nickname = user.profile.nickname || null;
        combinedUser.email = user.profile.email || null;
        combinedUser.picture =
            (user.profile.picture && user.profile.picture[0]) || null;
        combinedUser.client_id = user.profile.client_id || null;
        combinedUser.access_token = user.access_token || null;
        combinedUser.id_token = user.id_token || null;
        combinedUser.roles = user.profile.roles || null;
        return combinedUser;
    }
    /**
     * @param {?=} args
     * @return {?}
     */
    login(args) {
        /** @type {?} */
        const baseHref = this.platformLocation.getBaseHrefFromDOM();
        /** @type {?} */
        const routeActiveIsUnauthorized = this.router.isActive(baseHref + 'unauthorized', false);
        if (this.authenticationSettings.login_mode === 'redirect' ||
            (this.authenticationSettings.login_mode === 'iframe' &&
                routeActiveIsUnauthorized)) {
            return this.userManager.signinRedirect(args).catch((/**
             * @param {?} error
             * @return {?}
             */
            (error) => {
                /** @type {?} */
                const erro = (/** @type {?} */ (error));
                if ('Network Error' === erro.message) {
                    /** @type {?} */
                    const message = 'Não foi possível se conectar ao servidor de autenticação!';
                    /** @type {?} */
                    const errorToThrow = new Error(message);
                    errorToThrow.stack = erro.stack;
                    errorToThrow.name = 'Erro de conexão';
                    throw errorToThrow;
                }
                else {
                    throw erro;
                }
                // console.error(error);
            }));
        }
        else if (this.authenticationSettings.login_mode === 'iframe' &&
            routeActiveIsUnauthorized === false) {
            return this.router.navigateByUrl(baseHref + 'login');
        }
        else if (this.authenticationSettings.login_mode === 'popup') {
            return this.userManager.signinPopup(args).catch((/**
             * @param {?} error
             * @return {?}
             */
            (error) => {
                /** @type {?} */
                const erro = (/** @type {?} */ (error));
                if ('Network Error' === erro.message) {
                    /** @type {?} */
                    const message = 'Não foi possível se conectar ao servidor de autenticação!';
                    /** @type {?} */
                    const errorToThrow = new Error(message);
                    errorToThrow.stack = erro.stack;
                    errorToThrow.name = 'Erro de conexão';
                    throw errorToThrow;
                }
                else {
                    throw erro;
                }
                // console.error(error);
            }));
        }
    }
    /**
     * @param {?=} ssoLogout
     * @return {?}
     */
    logout(ssoLogout = true) {
        if (ssoLogout) {
            /** @type {?} */
            const isOnline = navigator.onLine;
            if (isOnline) {
                return this.userManager.createSignoutRequest().then((/**
                 * @param {?} req
                 * @return {?}
                 */
                (req) => {
                    console.log('Request logout: ', req);
                    /** @type {?} */
                    const ifrm = document.createElement('iframe');
                    ifrm.setAttribute('src', req.url);
                    ifrm.style.width = '640px';
                    ifrm.style.height = '480px';
                    ifrm.style.display = 'none';
                    ifrm.onload = (/**
                     * @return {?}
                     */
                    () => {
                        console.log('IFrame do logout terminou de carregar!');
                        super.logout().then((/**
                         * @return {?}
                         */
                        () => {
                            this.userManager.removeUser();
                            ifrm.parentElement.removeChild(ifrm);
                        }));
                    });
                    document.body.appendChild(ifrm);
                }));
                // return this.userManager.signoutRedirect()
                //   .then(() => super.logout())
                //   .catch((error) => {
                //     console.error(error);
                //     super.logout();
                //     throw error;
                //   });
            }
            else {
                /** @type {?} */
                const message = 'Não foi possível se conectar ao servidor de autenticação!';
                /** @type {?} */
                const errorToThrow = new Error(message);
                errorToThrow.name = 'Erro de conexão';
                throw errorToThrow;
            }
        }
        else {
            super.logout();
        }
    }
}
OidcAuthenticationService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
OidcAuthenticationService.ctorParameters = () => [
    { type: undefined },
    { type: Router },
    { type: ActivatedRoute },
    { type: PlatformLocation },
    { type: LocationStrategy }
];
if (false) {
    /**
     * @type {?}
     * @protected
     */
    OidcAuthenticationService.prototype.userManager;
    /** @type {?} */
    OidcAuthenticationService.prototype.userValue;
    /** @type {?} */
    OidcAuthenticationService.prototype.authenticationSettings;
    /**
     * @type {?}
     * @protected
     */
    OidcAuthenticationService.prototype.router;
    /**
     * @type {?}
     * @protected
     */
    OidcAuthenticationService.prototype.activatedRoute;
    /**
     * @type {?}
     * @protected
     */
    OidcAuthenticationService.prototype.platformLocation;
    /**
     * @type {?}
     * @protected
     */
    OidcAuthenticationService.prototype.locationStrategy;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib2lkYy1hdXRoZW50aWNhdGlvbi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvYXV0aC9hdXRoZW50aWNhdGlvbi1vaWRjL29pZGMtYXV0aGVudGljYXRpb24uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQ0EsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsTUFBTSxFQUFFLGVBQWUsRUFBRSxjQUFjLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMxRSxPQUFPLEVBQUUsTUFBTSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDeEMsT0FBTyxFQUFFLFdBQVcsRUFBRSxvQkFBb0IsRUFBRSxNQUFNLGFBQWEsQ0FBQztBQUNoRSxPQUFPLEVBQUUsNkJBQTZCLEVBQUUsTUFBTSxtREFBbUQsQ0FBQztBQUVsRyxPQUFPLEVBQ0wsZ0JBQWdCLEVBRWhCLGdCQUFnQixHQUNqQixNQUFNLGlCQUFpQixDQUFDOztBQUl6QixNQUFNLE9BQU8seUJBQTBCLFNBQVEsNkJBSzlDOzs7Ozs7OztJQWVDLFlBQ1Msc0JBQ2dCLEVBQ2IsTUFBYyxFQUNkLGNBQThCLEVBQzlCLGdCQUFrQyxFQUNsQyxnQkFBa0M7UUFFNUMsS0FBSyxFQUFFLENBQUM7UUFQRCwyQkFBc0IsR0FBdEIsc0JBQXNCLENBQ047UUFDYixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFDbEMscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQXBCcEMsZ0JBQVcsR0FBcUIsSUFBSSxDQUFDO1FBRXhDLGNBQVMsR0FBYyxJQUFJLENBQUM7O2NBc0IzQixjQUFjLHFCQUNmLHNCQUFzQixJQUN6QixTQUFTLEVBQUUsSUFBSSxvQkFBb0IsQ0FBQyxFQUFFLEtBQUssRUFBRSxNQUFNLENBQUMsWUFBWSxFQUFFLENBQUMsR0FDcEU7UUFDRCxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksV0FBVyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ25ELElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLGFBQWE7Ozs7UUFBQyxDQUFDLElBQUksRUFBRSxFQUFFO1lBQzdDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDMUIsQ0FBQyxFQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxxQkFBcUI7Ozs7UUFBQyxDQUFDLEtBQUssRUFBRSxFQUFFO1lBQ3RELElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxFQUFFLENBQUMsS0FBSzs7OztZQUFDLENBQUMsS0FBSyxFQUFFLEVBQUU7Z0JBQzlDLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3JCLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxFQUFFLENBQUM7Z0JBQzlCLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUNqQixDQUFDLEVBQUMsQ0FBQztRQUNMLENBQUMsRUFBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsbUJBQW1COzs7O1FBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRTtZQUNwRCxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3JCLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxFQUFFLENBQUM7WUFDOUIsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ2pCLENBQUMsRUFBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxJQUFJOzs7O1FBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRTtZQUN2QyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzFCLENBQUMsRUFBQyxDQUFDO1FBRUgsSUFBSSxJQUFJLENBQUMsc0JBQXNCLENBQUMsb0JBQW9CLEVBQUU7WUFDcEQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNO2lCQUNmLElBQUksQ0FBQyxNQUFNOzs7O1lBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLEtBQUssWUFBWSxlQUFlLEVBQUMsQ0FBQztpQkFDekQsSUFBSSxDQUNILE1BQU07Ozs7WUFBQyxDQUFDLEtBQXNCLEVBQUUsRUFBRSxDQUNoQyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxJQUFJLENBQzlELFFBQVEsQ0FBQyxJQUFJLENBQ2QsRUFDRixDQUNGO2lCQUNBLFNBQVM7Ozs7WUFBQyxDQUFDLEtBQXNCLEVBQUUsRUFBRTtnQkFDcEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxvQkFBb0IsRUFBRSxDQUFDLEtBQUs7Ozs7Z0JBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRTtvQkFDcEQsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDbkIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO2dCQUN0QyxDQUFDLEVBQUMsQ0FBQztZQUNMLENBQUMsRUFBQyxDQUFDO1NBQ047UUFFRCxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU07YUFDZixJQUFJLENBQ0gsTUFBTTs7OztRQUFDLENBQUMsS0FBc0IsRUFBRSxFQUFFLENBQ2hDLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxZQUFZLENBQUMsQ0FBQyxJQUFJLENBQ3ZELFFBQVEsQ0FBQyxJQUFJLENBQ2QsRUFDRixDQUNGO2FBQ0EsU0FBUzs7OztRQUFDLENBQUMsS0FBc0IsRUFBRSxFQUFFO1lBQ3BDLElBQUksQ0FBQyxXQUFXLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztRQUM1QyxDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7SUF6RUQsSUFBSSxhQUFhO1FBQ2YsT0FBTyxJQUFJLENBQUMsU0FBUyxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sS0FBSyxJQUFJLENBQUM7SUFDbkUsQ0FBQzs7OztJQUVELFNBQVM7UUFDUCxPQUFPLElBQUksQ0FBQyxXQUFXO2FBQ3BCLFlBQVksRUFBRTthQUNkLElBQUk7Ozs7UUFBQyxDQUFDLElBQUksRUFBRSxFQUFFOzs7UUFBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFBLEVBQUMsQ0FBQztJQUNuRCxDQUFDOzs7OztJQW1FTSxZQUFZLENBQUMsSUFBSTtRQUN0QixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUN0QixJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDOUIsQ0FBQzs7Ozs7SUFFTSxTQUFTLENBQUMsSUFBZTs7Y0FDeEIsZUFBZSxHQUFHLG1CQUFBLEVBQUUsRUFBcUI7O1lBQzNDLFlBQVksR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLGVBQWUsRUFBRSxJQUFJLENBQUM7UUFDdkQsK0RBQStEO1FBQy9ELGlFQUFpRTtRQUNqRSxrREFBa0Q7UUFDbEQsSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO1lBQ2hCLFlBQVksR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7U0FDMUQ7UUFFRCxZQUFZLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztRQUVsQyxZQUFZLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxJQUFJLElBQUksQ0FBQztRQUM1QyxZQUFZLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQztRQUM5QyxZQUFZLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsSUFBSSxJQUFJLENBQUM7UUFDMUUsWUFBWSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUM7UUFDMUQsWUFBWSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUM7UUFDdEQsWUFBWSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUM7UUFDaEQsWUFBWSxDQUFDLE9BQU87WUFDbEIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQztRQUM1RCxZQUFZLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQztRQUN4RCxZQUFZLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxZQUFZLElBQUksSUFBSSxDQUFDO1FBQ3RELFlBQVksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUM7UUFDOUMsWUFBWSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUM7UUFFaEQsT0FBTyxZQUFZLENBQUM7SUFDdEIsQ0FBQzs7Ozs7SUFFTSxLQUFLLENBQUMsSUFBSzs7Y0FDVixRQUFRLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGtCQUFrQixFQUFFOztjQUNyRCx5QkFBeUIsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FDcEQsUUFBUSxHQUFHLGNBQWMsRUFDekIsS0FBSyxDQUNOO1FBQ0QsSUFDRSxJQUFJLENBQUMsc0JBQXNCLENBQUMsVUFBVSxLQUFLLFVBQVU7WUFDckQsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsVUFBVSxLQUFLLFFBQVE7Z0JBQ2xELHlCQUF5QixDQUFDLEVBQzVCO1lBQ0EsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLOzs7O1lBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRTs7c0JBQ3JELElBQUksR0FBRyxtQkFBQSxLQUFLLEVBQVM7Z0JBQzNCLElBQUksZUFBZSxLQUFLLElBQUksQ0FBQyxPQUFPLEVBQUU7OzBCQUM5QixPQUFPLEdBQ1gsMkRBQTJEOzswQkFDdkQsWUFBWSxHQUFHLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQztvQkFDdkMsWUFBWSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO29CQUNoQyxZQUFZLENBQUMsSUFBSSxHQUFHLGlCQUFpQixDQUFDO29CQUN0QyxNQUFNLFlBQVksQ0FBQztpQkFDcEI7cUJBQU07b0JBQ0wsTUFBTSxJQUFJLENBQUM7aUJBQ1o7Z0JBQ0Qsd0JBQXdCO1lBQzFCLENBQUMsRUFBQyxDQUFDO1NBQ0o7YUFBTSxJQUNMLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxVQUFVLEtBQUssUUFBUTtZQUNuRCx5QkFBeUIsS0FBSyxLQUFLLEVBQ25DO1lBQ0EsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDLENBQUM7U0FDdEQ7YUFBTSxJQUFJLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxVQUFVLEtBQUssT0FBTyxFQUFFO1lBQzdELE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSzs7OztZQUFDLENBQUMsS0FBSyxFQUFFLEVBQUU7O3NCQUNsRCxJQUFJLEdBQUcsbUJBQUEsS0FBSyxFQUFTO2dCQUMzQixJQUFJLGVBQWUsS0FBSyxJQUFJLENBQUMsT0FBTyxFQUFFOzswQkFDOUIsT0FBTyxHQUNYLDJEQUEyRDs7MEJBQ3ZELFlBQVksR0FBRyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUM7b0JBQ3ZDLFlBQVksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztvQkFDaEMsWUFBWSxDQUFDLElBQUksR0FBRyxpQkFBaUIsQ0FBQztvQkFDdEMsTUFBTSxZQUFZLENBQUM7aUJBQ3BCO3FCQUFNO29CQUNMLE1BQU0sSUFBSSxDQUFDO2lCQUNaO2dCQUNELHdCQUF3QjtZQUMxQixDQUFDLEVBQUMsQ0FBQztTQUNKO0lBQ0gsQ0FBQzs7Ozs7SUFFTSxNQUFNLENBQUMsU0FBUyxHQUFHLElBQUk7UUFDNUIsSUFBSSxTQUFTLEVBQUU7O2tCQUNQLFFBQVEsR0FBRyxTQUFTLENBQUMsTUFBTTtZQUNqQyxJQUFJLFFBQVEsRUFBRTtnQkFDWixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsb0JBQW9CLEVBQUUsQ0FBQyxJQUFJOzs7O2dCQUFDLENBQUMsR0FBRyxFQUFFLEVBQUU7b0JBQzFELE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLEVBQUUsR0FBRyxDQUFDLENBQUM7OzBCQUMvQixJQUFJLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUM7b0JBQzdDLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDbEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsT0FBTyxDQUFDO29CQUMzQixJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxPQUFPLENBQUM7b0JBQzVCLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztvQkFDNUIsSUFBSSxDQUFDLE1BQU07OztvQkFBRyxHQUFHLEVBQUU7d0JBQ2pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsd0NBQXdDLENBQUMsQ0FBQzt3QkFDdEQsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLElBQUk7Ozt3QkFBQyxHQUFHLEVBQUU7NEJBQ3ZCLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxFQUFFLENBQUM7NEJBQzlCLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUN2QyxDQUFDLEVBQUMsQ0FBQztvQkFDTCxDQUFDLENBQUEsQ0FBQztvQkFDRixRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDbEMsQ0FBQyxFQUFDLENBQUM7Z0JBQ0gsNENBQTRDO2dCQUM1QyxnQ0FBZ0M7Z0JBQ2hDLHdCQUF3QjtnQkFDeEIsNEJBQTRCO2dCQUM1QixzQkFBc0I7Z0JBQ3RCLG1CQUFtQjtnQkFDbkIsUUFBUTthQUNUO2lCQUFNOztzQkFDQyxPQUFPLEdBQ1gsMkRBQTJEOztzQkFDdkQsWUFBWSxHQUFHLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQztnQkFDdkMsWUFBWSxDQUFDLElBQUksR0FBRyxpQkFBaUIsQ0FBQztnQkFDdEMsTUFBTSxZQUFZLENBQUM7YUFDcEI7U0FDRjthQUFNO1lBQ0wsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDO1NBQ2hCO0lBQ0gsQ0FBQzs7O1lBNU1GLFVBQVU7Ozs7O1lBWkYsTUFBTTtZQUFtQixjQUFjO1lBTTlDLGdCQUFnQjtZQUVoQixnQkFBZ0I7Ozs7Ozs7SUFXaEIsZ0RBQStDOztJQUUvQyw4Q0FBbUM7O0lBYWpDLDJEQUN1Qjs7Ozs7SUFDdkIsMkNBQXdCOzs7OztJQUN4QixtREFBd0M7Ozs7O0lBQ3hDLHFEQUE0Qzs7Ozs7SUFDNUMscURBQTRDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgT3BlbklEQ29ubmVjdFVzZXIgfSBmcm9tICcuLy4uL2F1dGhlbnRpY2F0aW9uL3VzZXIubW9kZWwnO1xuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgUm91dGVyLCBOYXZpZ2F0aW9uU3RhcnQsIEFjdGl2YXRlZFJvdXRlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IGZpbHRlciB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcbmltcG9ydCB7IFVzZXJNYW5hZ2VyLCBXZWJTdG9yYWdlU3RhdGVTdG9yZSB9IGZyb20gJ29pZGMtY2xpZW50JztcbmltcG9ydCB7IFByb3ZpZGVyQXV0aGVudGljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vYXV0aGVudGljYXRpb24vcHJvdmlkZXItYXV0aGVudGljYXRpb24uc2VydmljZSc7XG5pbXBvcnQgeyBPcGVuSURDb25uZWN0U2V0dGluZ3MgfSBmcm9tICcuL29pZGMtbW9kdWxlJztcbmltcG9ydCB7XG4gIFBsYXRmb3JtTG9jYXRpb24sXG4gIEhhc2hMb2NhdGlvblN0cmF0ZWd5LFxuICBMb2NhdGlvblN0cmF0ZWd5LFxufSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuLy8gQGR5bmFtaWNcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIE9pZGNBdXRoZW50aWNhdGlvblNlcnZpY2UgZXh0ZW5kcyBQcm92aWRlckF1dGhlbnRpY2F0aW9uU2VydmljZTxcbiAgT2lkYy5Vc2VyLFxuICBPcGVuSURDb25uZWN0VXNlcixcbiAgdm9pZCxcbiAgYm9vbGVhblxuPiB7XG4gIHByb3RlY3RlZCB1c2VyTWFuYWdlcjogT2lkYy5Vc2VyTWFuYWdlciA9IG51bGw7XG5cbiAgcHVibGljIHVzZXJWYWx1ZTogT2lkYy5Vc2VyID0gbnVsbDtcblxuICBnZXQgbmVlZFJlbmV3VXNlcigpIHtcbiAgICByZXR1cm4gdGhpcy51c2VyVmFsdWUgIT0gbnVsbCAmJiB0aGlzLnVzZXJWYWx1ZS5leHBpcmVkID09PSB0cnVlO1xuICB9XG5cbiAgcmVuZXdVc2VyKCkge1xuICAgIHJldHVybiB0aGlzLnVzZXJNYW5hZ2VyXG4gICAgICAuc2lnbmluU2lsZW50KClcbiAgICAgIC50aGVuKCh1c2VyKSA9PiAoKSA9PiB0aGlzLmxvYWRPaWRjVXNlcih1c2VyKSk7XG4gIH1cblxuICBjb25zdHJ1Y3RvcihcbiAgICBwdWJsaWMgYXV0aGVudGljYXRpb25TZXR0aW5nczogT2lkYy5Vc2VyTWFuYWdlclNldHRpbmdzICZcbiAgICAgIE9wZW5JRENvbm5lY3RTZXR0aW5ncyxcbiAgICBwcm90ZWN0ZWQgcm91dGVyOiBSb3V0ZXIsXG4gICAgcHJvdGVjdGVkIGFjdGl2YXRlZFJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSxcbiAgICBwcm90ZWN0ZWQgcGxhdGZvcm1Mb2NhdGlvbjogUGxhdGZvcm1Mb2NhdGlvbixcbiAgICBwcm90ZWN0ZWQgbG9jYXRpb25TdHJhdGVneTogTG9jYXRpb25TdHJhdGVneVxuICApIHtcbiAgICBzdXBlcigpO1xuXG4gICAgY29uc3QgdXNlck1hbmFnZXJBcmcgPSB7XG4gICAgICAuLi5hdXRoZW50aWNhdGlvblNldHRpbmdzLFxuICAgICAgdXNlclN0b3JlOiBuZXcgV2ViU3RvcmFnZVN0YXRlU3RvcmUoeyBzdG9yZTogd2luZG93LmxvY2FsU3RvcmFnZSB9KSxcbiAgICB9O1xuICAgIHRoaXMudXNlck1hbmFnZXIgPSBuZXcgVXNlck1hbmFnZXIodXNlck1hbmFnZXJBcmcpO1xuICAgIHRoaXMudXNlck1hbmFnZXIuZXZlbnRzLmFkZFVzZXJMb2FkZWQoKHVzZXIpID0+IHtcbiAgICAgIHRoaXMubG9hZE9pZGNVc2VyKHVzZXIpO1xuICAgIH0pO1xuICAgIHRoaXMudXNlck1hbmFnZXIuZXZlbnRzLmFkZEFjY2Vzc1Rva2VuRXhwaXJlZCgoZXZlbnQpID0+IHtcbiAgICAgIHRoaXMudXNlck1hbmFnZXIuc2lnbmluU2lsZW50KCkuY2F0Y2goKGVycm9yKSA9PiB7XG4gICAgICAgIGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xuICAgICAgICB0aGlzLnVzZXJNYW5hZ2VyLnJlbW92ZVVzZXIoKTtcbiAgICAgICAgc3VwZXIubG9nb3V0KCk7XG4gICAgICB9KTtcbiAgICB9KTtcbiAgICB0aGlzLnVzZXJNYW5hZ2VyLmV2ZW50cy5hZGRTaWxlbnRSZW5ld0Vycm9yKChlcnJvcikgPT4ge1xuICAgICAgY29uc29sZS5lcnJvcihlcnJvcik7XG4gICAgICB0aGlzLnVzZXJNYW5hZ2VyLnJlbW92ZVVzZXIoKTtcbiAgICAgIHN1cGVyLmxvZ291dCgpO1xuICAgIH0pO1xuICAgIHRoaXMudXNlck1hbmFnZXIuZ2V0VXNlcigpLnRoZW4oKHVzZXIpID0+IHtcbiAgICAgIHRoaXMubG9hZE9pZGNVc2VyKHVzZXIpO1xuICAgIH0pO1xuXG4gICAgaWYgKHRoaXMuYXV0aGVudGljYXRpb25TZXR0aW5ncy5hdXRvbWF0aWNTaWxlbnRSZW5ldykge1xuICAgICAgdGhpcy5yb3V0ZXIuZXZlbnRzXG4gICAgICAgIC5waXBlKGZpbHRlcigoZXZlbnQpID0+IGV2ZW50IGluc3RhbmNlb2YgTmF2aWdhdGlvblN0YXJ0KSlcbiAgICAgICAgLnBpcGUoXG4gICAgICAgICAgZmlsdGVyKChldmVudDogTmF2aWdhdGlvblN0YXJ0KSA9PlxuICAgICAgICAgICAgbmV3IFJlZ0V4cCh0aGlzLmF1dGhlbnRpY2F0aW9uU2V0dGluZ3Muc2lsZW50X3JlZGlyZWN0X3VyaSkudGVzdChcbiAgICAgICAgICAgICAgbG9jYXRpb24uaHJlZlxuICAgICAgICAgICAgKVxuICAgICAgICAgIClcbiAgICAgICAgKVxuICAgICAgICAuc3Vic2NyaWJlKChldmVudDogTmF2aWdhdGlvblN0YXJ0KSA9PiB7XG4gICAgICAgICAgdGhpcy51c2VyTWFuYWdlci5zaWduaW5TaWxlbnRDYWxsYmFjaygpLmNhdGNoKChlcnIpID0+IHtcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZXJyKTtcbiAgICAgICAgICAgIHRoaXMudXNlck1hbmFnZXIuc3RhcnRTaWxlbnRSZW5ldygpO1xuICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICB0aGlzLnJvdXRlci5ldmVudHNcbiAgICAgIC5waXBlKFxuICAgICAgICBmaWx0ZXIoKGV2ZW50OiBOYXZpZ2F0aW9uU3RhcnQpID0+XG4gICAgICAgICAgbmV3IFJlZ0V4cCh0aGlzLmF1dGhlbnRpY2F0aW9uU2V0dGluZ3MucmVkaXJlY3RfdXJpKS50ZXN0KFxuICAgICAgICAgICAgbG9jYXRpb24uaHJlZlxuICAgICAgICAgIClcbiAgICAgICAgKVxuICAgICAgKVxuICAgICAgLnN1YnNjcmliZSgoZXZlbnQ6IE5hdmlnYXRpb25TdGFydCkgPT4ge1xuICAgICAgICB0aGlzLnVzZXJNYW5hZ2VyLnNpZ25pblJlZGlyZWN0Q2FsbGJhY2soKTtcbiAgICAgIH0pO1xuICB9XG5cbiAgcHVibGljIGxvYWRPaWRjVXNlcih1c2VyKSB7XG4gICAgdGhpcy51c2VyVmFsdWUgPSB1c2VyO1xuICAgIHRoaXMubG9hZFByb3ZpZGVyVXNlcih1c2VyKTtcbiAgfVxuXG4gIHB1YmxpYyB0cmFuc2Zvcm0odXNlcjogT2lkYy5Vc2VyKTogT3BlbklEQ29ubmVjdFVzZXIge1xuICAgIGNvbnN0IGRlc3RpbmF0aW9uVXNlciA9IHt9IGFzIE9wZW5JRENvbm5lY3RVc2VyO1xuICAgIGxldCBjb21iaW5lZFVzZXIgPSBPYmplY3QuYXNzaWduKGRlc3RpbmF0aW9uVXNlciwgdXNlcik7XG4gICAgLy8gTyBPSURDX0NsaWVudCB1c2EgYSBwcm9wcmllZGFkZSBwcm9maWxlIHBhcmEgY29sb2NhciBjbGFpbXMsXG4gICAgLy8gcG9ydGFudG8gZGV2ZW1vcyBmYXplciBvIGFzc2lnbiBkZXN0ZSBjb20gbyBvYmpldGl2byBkZSBtYW50ZXJcbiAgICAvLyBubyB1c3XDoXJpbyB0YW1iw6ltIGFzIGluZm9ybWHDp8O1ZXMgJ27Do28gbWFwZWFkYXMnXG4gICAgaWYgKHVzZXIucHJvZmlsZSkge1xuICAgICAgY29tYmluZWRVc2VyID0gT2JqZWN0LmFzc2lnbihjb21iaW5lZFVzZXIsIHVzZXIucHJvZmlsZSk7XG4gICAgfVxuXG4gICAgY29tYmluZWRVc2VyLmF1dGhlbnRpY2F0ZWQgPSB0cnVlO1xuXG4gICAgY29tYmluZWRVc2VyLnN1YiA9IHVzZXIucHJvZmlsZS5zdWIgfHwgbnVsbDtcbiAgICBjb21iaW5lZFVzZXIubmFtZSA9IHVzZXIucHJvZmlsZS5uYW1lIHx8IG51bGw7XG4gICAgY29tYmluZWRVc2VyLnByZWZlcnJlZF91c2VybmFtZSA9IHVzZXIucHJvZmlsZS5wcmVmZXJyZWRfdXNlcm5hbWUgfHwgbnVsbDtcbiAgICBjb21iaW5lZFVzZXIuZ2l2ZW5fbmFtZSA9IHVzZXIucHJvZmlsZS5naXZlbl9uYW1lIHx8IG51bGw7XG4gICAgY29tYmluZWRVc2VyLm5pY2tuYW1lID0gdXNlci5wcm9maWxlLm5pY2tuYW1lIHx8IG51bGw7XG4gICAgY29tYmluZWRVc2VyLmVtYWlsID0gdXNlci5wcm9maWxlLmVtYWlsIHx8IG51bGw7XG4gICAgY29tYmluZWRVc2VyLnBpY3R1cmUgPVxuICAgICAgKHVzZXIucHJvZmlsZS5waWN0dXJlICYmIHVzZXIucHJvZmlsZS5waWN0dXJlWzBdKSB8fCBudWxsO1xuICAgIGNvbWJpbmVkVXNlci5jbGllbnRfaWQgPSB1c2VyLnByb2ZpbGUuY2xpZW50X2lkIHx8IG51bGw7XG4gICAgY29tYmluZWRVc2VyLmFjY2Vzc190b2tlbiA9IHVzZXIuYWNjZXNzX3Rva2VuIHx8IG51bGw7XG4gICAgY29tYmluZWRVc2VyLmlkX3Rva2VuID0gdXNlci5pZF90b2tlbiB8fCBudWxsO1xuICAgIGNvbWJpbmVkVXNlci5yb2xlcyA9IHVzZXIucHJvZmlsZS5yb2xlcyB8fCBudWxsO1xuXG4gICAgcmV0dXJuIGNvbWJpbmVkVXNlcjtcbiAgfVxuXG4gIHB1YmxpYyBsb2dpbihhcmdzPykge1xuICAgIGNvbnN0IGJhc2VIcmVmID0gdGhpcy5wbGF0Zm9ybUxvY2F0aW9uLmdldEJhc2VIcmVmRnJvbURPTSgpO1xuICAgIGNvbnN0IHJvdXRlQWN0aXZlSXNVbmF1dGhvcml6ZWQgPSB0aGlzLnJvdXRlci5pc0FjdGl2ZShcbiAgICAgIGJhc2VIcmVmICsgJ3VuYXV0aG9yaXplZCcsXG4gICAgICBmYWxzZVxuICAgICk7XG4gICAgaWYgKFxuICAgICAgdGhpcy5hdXRoZW50aWNhdGlvblNldHRpbmdzLmxvZ2luX21vZGUgPT09ICdyZWRpcmVjdCcgfHxcbiAgICAgICh0aGlzLmF1dGhlbnRpY2F0aW9uU2V0dGluZ3MubG9naW5fbW9kZSA9PT0gJ2lmcmFtZScgJiZcbiAgICAgICAgcm91dGVBY3RpdmVJc1VuYXV0aG9yaXplZClcbiAgICApIHtcbiAgICAgIHJldHVybiB0aGlzLnVzZXJNYW5hZ2VyLnNpZ25pblJlZGlyZWN0KGFyZ3MpLmNhdGNoKChlcnJvcikgPT4ge1xuICAgICAgICBjb25zdCBlcnJvID0gZXJyb3IgYXMgRXJyb3I7XG4gICAgICAgIGlmICgnTmV0d29yayBFcnJvcicgPT09IGVycm8ubWVzc2FnZSkge1xuICAgICAgICAgIGNvbnN0IG1lc3NhZ2UgPVxuICAgICAgICAgICAgJ07Do28gZm9pIHBvc3PDrXZlbCBzZSBjb25lY3RhciBhbyBzZXJ2aWRvciBkZSBhdXRlbnRpY2HDp8OjbyEnO1xuICAgICAgICAgIGNvbnN0IGVycm9yVG9UaHJvdyA9IG5ldyBFcnJvcihtZXNzYWdlKTtcbiAgICAgICAgICBlcnJvclRvVGhyb3cuc3RhY2sgPSBlcnJvLnN0YWNrO1xuICAgICAgICAgIGVycm9yVG9UaHJvdy5uYW1lID0gJ0Vycm8gZGUgY29uZXjDo28nO1xuICAgICAgICAgIHRocm93IGVycm9yVG9UaHJvdztcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB0aHJvdyBlcnJvO1xuICAgICAgICB9XG4gICAgICAgIC8vIGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xuICAgICAgfSk7XG4gICAgfSBlbHNlIGlmIChcbiAgICAgIHRoaXMuYXV0aGVudGljYXRpb25TZXR0aW5ncy5sb2dpbl9tb2RlID09PSAnaWZyYW1lJyAmJlxuICAgICAgcm91dGVBY3RpdmVJc1VuYXV0aG9yaXplZCA9PT0gZmFsc2VcbiAgICApIHtcbiAgICAgIHJldHVybiB0aGlzLnJvdXRlci5uYXZpZ2F0ZUJ5VXJsKGJhc2VIcmVmICsgJ2xvZ2luJyk7XG4gICAgfSBlbHNlIGlmICh0aGlzLmF1dGhlbnRpY2F0aW9uU2V0dGluZ3MubG9naW5fbW9kZSA9PT0gJ3BvcHVwJykge1xuICAgICAgcmV0dXJuIHRoaXMudXNlck1hbmFnZXIuc2lnbmluUG9wdXAoYXJncykuY2F0Y2goKGVycm9yKSA9PiB7XG4gICAgICAgIGNvbnN0IGVycm8gPSBlcnJvciBhcyBFcnJvcjtcbiAgICAgICAgaWYgKCdOZXR3b3JrIEVycm9yJyA9PT0gZXJyby5tZXNzYWdlKSB7XG4gICAgICAgICAgY29uc3QgbWVzc2FnZSA9XG4gICAgICAgICAgICAnTsOjbyBmb2kgcG9zc8OtdmVsIHNlIGNvbmVjdGFyIGFvIHNlcnZpZG9yIGRlIGF1dGVudGljYcOnw6NvISc7XG4gICAgICAgICAgY29uc3QgZXJyb3JUb1Rocm93ID0gbmV3IEVycm9yKG1lc3NhZ2UpO1xuICAgICAgICAgIGVycm9yVG9UaHJvdy5zdGFjayA9IGVycm8uc3RhY2s7XG4gICAgICAgICAgZXJyb3JUb1Rocm93Lm5hbWUgPSAnRXJybyBkZSBjb25leMOjbyc7XG4gICAgICAgICAgdGhyb3cgZXJyb3JUb1Rocm93O1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHRocm93IGVycm87XG4gICAgICAgIH1cbiAgICAgICAgLy8gY29uc29sZS5lcnJvcihlcnJvcik7XG4gICAgICB9KTtcbiAgICB9XG4gIH1cblxuICBwdWJsaWMgbG9nb3V0KHNzb0xvZ291dCA9IHRydWUpIHtcbiAgICBpZiAoc3NvTG9nb3V0KSB7XG4gICAgICBjb25zdCBpc09ubGluZSA9IG5hdmlnYXRvci5vbkxpbmU7XG4gICAgICBpZiAoaXNPbmxpbmUpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMudXNlck1hbmFnZXIuY3JlYXRlU2lnbm91dFJlcXVlc3QoKS50aGVuKChyZXEpID0+IHtcbiAgICAgICAgICBjb25zb2xlLmxvZygnUmVxdWVzdCBsb2dvdXQ6ICcsIHJlcSk7XG4gICAgICAgICAgY29uc3QgaWZybSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2lmcmFtZScpO1xuICAgICAgICAgIGlmcm0uc2V0QXR0cmlidXRlKCdzcmMnLCByZXEudXJsKTtcbiAgICAgICAgICBpZnJtLnN0eWxlLndpZHRoID0gJzY0MHB4JztcbiAgICAgICAgICBpZnJtLnN0eWxlLmhlaWdodCA9ICc0ODBweCc7XG4gICAgICAgICAgaWZybS5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnO1xuICAgICAgICAgIGlmcm0ub25sb2FkID0gKCkgPT4ge1xuICAgICAgICAgICAgY29uc29sZS5sb2coJ0lGcmFtZSBkbyBsb2dvdXQgdGVybWlub3UgZGUgY2FycmVnYXIhJyk7XG4gICAgICAgICAgICBzdXBlci5sb2dvdXQoKS50aGVuKCgpID0+IHtcbiAgICAgICAgICAgICAgdGhpcy51c2VyTWFuYWdlci5yZW1vdmVVc2VyKCk7XG4gICAgICAgICAgICAgIGlmcm0ucGFyZW50RWxlbWVudC5yZW1vdmVDaGlsZChpZnJtKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH07XG4gICAgICAgICAgZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZChpZnJtKTtcbiAgICAgICAgfSk7XG4gICAgICAgIC8vIHJldHVybiB0aGlzLnVzZXJNYW5hZ2VyLnNpZ25vdXRSZWRpcmVjdCgpXG4gICAgICAgIC8vICAgLnRoZW4oKCkgPT4gc3VwZXIubG9nb3V0KCkpXG4gICAgICAgIC8vICAgLmNhdGNoKChlcnJvcikgPT4ge1xuICAgICAgICAvLyAgICAgY29uc29sZS5lcnJvcihlcnJvcik7XG4gICAgICAgIC8vICAgICBzdXBlci5sb2dvdXQoKTtcbiAgICAgICAgLy8gICAgIHRocm93IGVycm9yO1xuICAgICAgICAvLyAgIH0pO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgY29uc3QgbWVzc2FnZSA9XG4gICAgICAgICAgJ07Do28gZm9pIHBvc3PDrXZlbCBzZSBjb25lY3RhciBhbyBzZXJ2aWRvciBkZSBhdXRlbnRpY2HDp8OjbyEnO1xuICAgICAgICBjb25zdCBlcnJvclRvVGhyb3cgPSBuZXcgRXJyb3IobWVzc2FnZSk7XG4gICAgICAgIGVycm9yVG9UaHJvdy5uYW1lID0gJ0Vycm8gZGUgY29uZXjDo28nO1xuICAgICAgICB0aHJvdyBlcnJvclRvVGhyb3c7XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIHN1cGVyLmxvZ291dCgpO1xuICAgIH1cbiAgfVxufVxuIl19