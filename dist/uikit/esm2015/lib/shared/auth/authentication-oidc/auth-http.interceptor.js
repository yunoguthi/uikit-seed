/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { catchError, mergeMap } from 'rxjs/operators';
import { Injectable, Optional, Injector } from '@angular/core';
import { throwError, from } from 'rxjs';
import { LogService } from '../../../utils/log/log.service';
import { UserService } from '../authentication/user.service';
import { IAuthenticationServiceToken } from '../authentication/abstraction/provider-authentication-service.token';
export class OidcAuthHttpInterceptor {
    /**
     * @param {?} userService
     * @param {?} injector
     * @param {?=} logService
     */
    constructor(userService, injector, logService) {
        this.userService = userService;
        this.injector = injector;
        this.logService = logService;
    }
    /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    intercept(req, next) {
        /** @type {?} */
        let authReq = req;
        if (!req.headers.has('Authorization')) {
            if (this.userService && this.userService.userValue && this.userService.userValue.authenticated) {
                /** @type {?} */
                const token = ((/** @type {?} */ (this.userService.userValue))).access_token ||
                    ((/** @type {?} */ (this.userService.userValue))).id_token;
                /** @type {?} */
                let httpHeaders = req.headers.set('Authorization', `Bearer ${token}`);
                if (!req.headers.has('content-type')) {
                    httpHeaders = httpHeaders.set('content-type', 'application/json');
                }
                authReq = req.clone({
                    headers: httpHeaders
                });
                if (this.logService) {
                    this.logService.debug(`Não possui 'Authorization' header, foi adicionado.`);
                }
            }
        }
        /** @type {?} */
        const httpHandle = next
            .handle(authReq)
            .pipe(catchError((/**
         * @param {?} error
         * @return {?}
         */
        (error) => {
            if (error.status === 401) {
                /** @type {?} */
                const authenticationService = (/** @type {?} */ (this.injector.get(IAuthenticationServiceToken)));
                if (authenticationService.needRenewUser) {
                    return from(authenticationService.renewUser()).pipe(mergeMap((/**
                     * @return {?}
                     */
                    () => {
                        return this.intercept(req, next);
                    })));
                }
            }
            if (this.logService) {
                this.logService.error(JSON.stringify(error));
            }
            return throwError(error);
        })));
        return httpHandle;
    }
}
OidcAuthHttpInterceptor.decorators = [
    { type: Injectable }
];
/** @nocollapse */
OidcAuthHttpInterceptor.ctorParameters = () => [
    { type: UserService },
    { type: Injector },
    { type: LogService, decorators: [{ type: Optional }] }
];
if (false) {
    /**
     * @type {?}
     * @protected
     */
    OidcAuthHttpInterceptor.prototype.userService;
    /**
     * @type {?}
     * @private
     */
    OidcAuthHttpInterceptor.prototype.injector;
    /**
     * @type {?}
     * @protected
     */
    OidcAuthHttpInterceptor.prototype.logService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC1odHRwLmludGVyY2VwdG9yLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvYXV0aC9hdXRoZW50aWNhdGlvbi1vaWRjL2F1dGgtaHR0cC5pbnRlcmNlcHRvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUN0RCxPQUFPLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBc0MsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRW5HLE9BQU8sRUFBYyxVQUFVLEVBQUUsSUFBSSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBRXBELE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUM1RCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0NBQWdDLENBQUM7QUFFN0QsT0FBTyxFQUFFLDJCQUEyQixFQUFFLE1BQU0scUVBQXFFLENBQUM7QUFJbEgsTUFBTSxPQUFPLHVCQUF1Qjs7Ozs7O0lBRWxDLFlBQ1ksV0FBd0IsRUFDMUIsUUFBa0IsRUFDSixVQUF1QjtRQUZuQyxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUMxQixhQUFRLEdBQVIsUUFBUSxDQUFVO1FBQ0osZUFBVSxHQUFWLFVBQVUsQ0FBYTtJQUcvQyxDQUFDOzs7Ozs7SUFFRCxTQUFTLENBQUMsR0FBcUIsRUFBRSxJQUFpQjs7WUFFNUMsT0FBTyxHQUFHLEdBQUc7UUFFakIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxFQUFFO1lBQ3JDLElBQUksSUFBSSxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxhQUFhLEVBQUU7O3NCQUV4RixLQUFLLEdBQ1QsQ0FBQyxtQkFBQSxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsRUFBYSxDQUFDLENBQUMsWUFBWTtvQkFDdEQsQ0FBQyxtQkFBQSxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsRUFBcUIsQ0FBQyxDQUFDLFFBQVE7O29CQUV4RCxXQUFXLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsZUFBZSxFQUFFLFVBQVUsS0FBSyxFQUFFLENBQUM7Z0JBRXJFLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsRUFBRTtvQkFDcEMsV0FBVyxHQUFHLFdBQVcsQ0FBQyxHQUFHLENBQUMsY0FBYyxFQUFFLGtCQUFrQixDQUFDLENBQUM7aUJBQ25FO2dCQUVELE9BQU8sR0FBRyxHQUFHLENBQUMsS0FBSyxDQUFDO29CQUNsQixPQUFPLEVBQUUsV0FBVztpQkFDckIsQ0FBQyxDQUFDO2dCQUVILElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtvQkFDbkIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsb0RBQW9ELENBQUMsQ0FBQztpQkFDN0U7YUFDRjtTQUNGOztjQUVLLFVBQVUsR0FBRyxJQUFJO2FBQ3BCLE1BQU0sQ0FBQyxPQUFPLENBQUM7YUFDZixJQUFJLENBQUMsVUFBVTs7OztRQUFDLENBQUMsS0FBd0IsRUFBRSxFQUFFO1lBQzVDLElBQUksS0FBSyxDQUFDLE1BQU0sS0FBSyxHQUFHLEVBQUU7O3NCQUNsQixxQkFBcUIsR0FBRyxtQkFBQSxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQywyQkFBMkIsQ0FBQyxFQUE2QjtnQkFDekcsSUFBSSxxQkFBcUIsQ0FBQyxhQUFhLEVBQUU7b0JBQ3ZDLE9BQU8sSUFBSSxDQUFDLHFCQUFxQixDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVE7OztvQkFBQyxHQUFHLEVBQUU7d0JBQ2hFLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUM7b0JBQ25DLENBQUMsRUFBQyxDQUFDLENBQUM7aUJBQ0w7YUFDRjtZQUNELElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtnQkFDbkIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO2FBQzlDO1lBQ0QsT0FBTyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDM0IsQ0FBQyxFQUFDLENBQUM7UUFFTCxPQUFPLFVBQVUsQ0FBQztJQUVwQixDQUFDOzs7WUF6REYsVUFBVTs7OztZQUxGLFdBQVc7WUFMK0MsUUFBUTtZQUlsRSxVQUFVLHVCQVlkLFFBQVE7Ozs7Ozs7SUFGVCw4Q0FBa0M7Ozs7O0lBQ2xDLDJDQUEwQjs7Ozs7SUFDMUIsNkNBQTZDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgY2F0Y2hFcnJvciwgbWVyZ2VNYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQgeyBJbmplY3RhYmxlLCBPcHRpb25hbCwgSW5qZWN0LCBpbmplY3QsIFJlZmxlY3RpdmVJbmplY3RvciwgSW5qZWN0b3IgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEh0dHBFdmVudCwgSHR0cEludGVyY2VwdG9yLCBIdHRwSGFuZGxlciwgSHR0cFJlcXVlc3QsIEh0dHBIZWFkZXJzLCBIdHRwRXJyb3JSZXNwb25zZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IE9ic2VydmFibGUsIHRocm93RXJyb3IsIGZyb20gfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IE9pZGNBdXRoZW50aWNhdGlvblNlcnZpY2UgfSBmcm9tICcuL29pZGMtYXV0aGVudGljYXRpb24uc2VydmljZSc7XG5pbXBvcnQgeyBMb2dTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vdXRpbHMvbG9nL2xvZy5zZXJ2aWNlJztcbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSAnLi4vYXV0aGVudGljYXRpb24vdXNlci5zZXJ2aWNlJztcbmltcG9ydCB7IE9BdXRoVXNlciwgT3BlbklEQ29ubmVjdFVzZXIsIFVzZXIgfSBmcm9tICcuLi9hdXRoZW50aWNhdGlvbi91c2VyLm1vZGVsJztcbmltcG9ydCB7IElBdXRoZW50aWNhdGlvblNlcnZpY2VUb2tlbiB9IGZyb20gJy4uL2F1dGhlbnRpY2F0aW9uL2Fic3RyYWN0aW9uL3Byb3ZpZGVyLWF1dGhlbnRpY2F0aW9uLXNlcnZpY2UudG9rZW4nO1xuaW1wb3J0IHsgSVByb3ZpZGVyQXV0aGVudGljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vYXV0aGVudGljYXRpb24vYWJzdHJhY3Rpb24vcHJvdmlkZXItYXV0aGVudGljYXRpb24uc2VydmljZSc7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBPaWRjQXV0aEh0dHBJbnRlcmNlcHRvciBpbXBsZW1lbnRzIEh0dHBJbnRlcmNlcHRvciB7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJvdGVjdGVkIHVzZXJTZXJ2aWNlOiBVc2VyU2VydmljZSxcbiAgICBwcml2YXRlIGluamVjdG9yOiBJbmplY3RvcixcbiAgICBAT3B0aW9uYWwoKSBwcm90ZWN0ZWQgbG9nU2VydmljZT86IExvZ1NlcnZpY2UsXG4gICkge1xuXG4gIH1cblxuICBpbnRlcmNlcHQocmVxOiBIdHRwUmVxdWVzdDxhbnk+LCBuZXh0OiBIdHRwSGFuZGxlcik6IE9ic2VydmFibGU8SHR0cEV2ZW50PGFueT4+IHtcblxuICAgIGxldCBhdXRoUmVxID0gcmVxO1xuXG4gICAgaWYgKCFyZXEuaGVhZGVycy5oYXMoJ0F1dGhvcml6YXRpb24nKSkge1xuICAgICAgaWYgKHRoaXMudXNlclNlcnZpY2UgJiYgdGhpcy51c2VyU2VydmljZS51c2VyVmFsdWUgJiYgdGhpcy51c2VyU2VydmljZS51c2VyVmFsdWUuYXV0aGVudGljYXRlZCkge1xuXG4gICAgICAgIGNvbnN0IHRva2VuID1cbiAgICAgICAgICAodGhpcy51c2VyU2VydmljZS51c2VyVmFsdWUgYXMgT0F1dGhVc2VyKS5hY2Nlc3NfdG9rZW4gfHxcbiAgICAgICAgICAodGhpcy51c2VyU2VydmljZS51c2VyVmFsdWUgYXMgT3BlbklEQ29ubmVjdFVzZXIpLmlkX3Rva2VuO1xuXG4gICAgICAgIGxldCBodHRwSGVhZGVycyA9IHJlcS5oZWFkZXJzLnNldCgnQXV0aG9yaXphdGlvbicsIGBCZWFyZXIgJHt0b2tlbn1gKTtcblxuICAgICAgICBpZiAoIXJlcS5oZWFkZXJzLmhhcygnY29udGVudC10eXBlJykpIHtcbiAgICAgICAgICBodHRwSGVhZGVycyA9IGh0dHBIZWFkZXJzLnNldCgnY29udGVudC10eXBlJywgJ2FwcGxpY2F0aW9uL2pzb24nKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGF1dGhSZXEgPSByZXEuY2xvbmUoe1xuICAgICAgICAgIGhlYWRlcnM6IGh0dHBIZWFkZXJzXG4gICAgICAgIH0pO1xuXG4gICAgICAgIGlmICh0aGlzLmxvZ1NlcnZpY2UpIHtcbiAgICAgICAgICB0aGlzLmxvZ1NlcnZpY2UuZGVidWcoYE7Do28gcG9zc3VpICdBdXRob3JpemF0aW9uJyBoZWFkZXIsIGZvaSBhZGljaW9uYWRvLmApO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgY29uc3QgaHR0cEhhbmRsZSA9IG5leHRcbiAgICAgIC5oYW5kbGUoYXV0aFJlcSlcbiAgICAgIC5waXBlKGNhdGNoRXJyb3IoKGVycm9yOiBIdHRwRXJyb3JSZXNwb25zZSkgPT4ge1xuICAgICAgICBpZiAoZXJyb3Iuc3RhdHVzID09PSA0MDEpIHtcbiAgICAgICAgICBjb25zdCBhdXRoZW50aWNhdGlvblNlcnZpY2UgPSB0aGlzLmluamVjdG9yLmdldChJQXV0aGVudGljYXRpb25TZXJ2aWNlVG9rZW4pIGFzIE9pZGNBdXRoZW50aWNhdGlvblNlcnZpY2U7XG4gICAgICAgICAgaWYgKGF1dGhlbnRpY2F0aW9uU2VydmljZS5uZWVkUmVuZXdVc2VyKSB7XG4gICAgICAgICAgICByZXR1cm4gZnJvbShhdXRoZW50aWNhdGlvblNlcnZpY2UucmVuZXdVc2VyKCkpLnBpcGUobWVyZ2VNYXAoKCkgPT4ge1xuICAgICAgICAgICAgICByZXR1cm4gdGhpcy5pbnRlcmNlcHQocmVxLCBuZXh0KTtcbiAgICAgICAgICAgIH0pKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMubG9nU2VydmljZSkge1xuICAgICAgICAgIHRoaXMubG9nU2VydmljZS5lcnJvcihKU09OLnN0cmluZ2lmeShlcnJvcikpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0aHJvd0Vycm9yKGVycm9yKTtcbiAgICAgIH0pKTtcblxuICAgIHJldHVybiBodHRwSGFuZGxlO1xuXG4gIH1cblxufVxuIl19