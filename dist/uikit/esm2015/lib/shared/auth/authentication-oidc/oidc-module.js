/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { IAuthenticationServiceToken } from './../authentication/abstraction/provider-authentication-service.token';
import { OidcAuthenticationService } from './oidc-authentication.service';
import { NgModule, InjectionToken, Inject, Injectable } from '@angular/core';
import { OidcRoutingModule } from './oidc-router.module';
import { CallbackComponent } from './components/callback.component';
import { SilentRefreshComponent } from './components/silentrefresh.component';
import { ActivatedRoute, Router } from '@angular/router';
import { PlatformLocation, LocationStrategy, CommonModule } from '@angular/common';
import { LoginIframeComponent } from './components/login-iframe.component';
import { ProtectedRouteComponent } from './components/protected-route.component';
/**
 * @record
 */
export function OpenIDConnectSettings() { }
if (false) {
    /** @type {?} */
    OpenIDConnectSettings.prototype.client_uri;
    /**
     * Modo de como efetuar o login. (Padrão é 'iframe' quando não informado)
     * @type {?|undefined}
     */
    OpenIDConnectSettings.prototype.login_mode;
}
/** @type {?} */
export const OIDC_CONFIG = new InjectionToken(`OIDC_CONFIG`);
export class OidcConfigDepHolder {
    /**
     * @param {?} openIDConnectSettings
     */
    constructor(openIDConnectSettings) {
        this.openIDConnectSettings = openIDConnectSettings;
    }
}
OidcConfigDepHolder.decorators = [
    { type: Injectable }
];
/** @nocollapse */
OidcConfigDepHolder.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [OIDC_CONFIG,] }] }
];
if (false) {
    /** @type {?} */
    OidcConfigDepHolder.prototype.openIDConnectSettings;
}
// @Injectable()
// export class AuthManagerDepHolder {
//   constructor(
//     @Inject(IAuthenticationManagerToken) @Optional() public authenticationManager: IAuthenticationManager
//   ) {
//   }
// }
/**
 * @param {?} oidcConfigDepHolder
 * @param {?} router
 * @param {?} platformLocation
 * @param {?} activatedRoute
 * @param {?} locationStrategy
 * @return {?}
 */
export function InitOidcAuthenticationService(oidcConfigDepHolder, router, platformLocation, activatedRoute, locationStrategy) {
    /** @type {?} */
    const clientSettings = oidcConfigDepHolder.openIDConnectSettings;
    clientSettings.login_mode = clientSettings.login_mode ? clientSettings.login_mode : 'iframe';
    /** @type {?} */
    const baseHref = platformLocation.getBaseHrefFromDOM();
    /** @type {?} */
    const origin = window.location.origin;
    /** @type {?} */
    const completeUrlToBaseHref = origin + baseHref;
    /** @type {?} */
    const baseClientUri = clientSettings.client_uri || completeUrlToBaseHref;
    /** @type {?} */
    const defaults = {
        redirect_uri: `${baseClientUri}callback`,
        post_logout_redirect_uri: `${baseClientUri}`,
        response_type: `id_token token`,
        scope: `openid profile email roles offline_access`,
        silent_redirect_uri: `${baseClientUri}silentrefresh`,
        automaticSilentRenew: true,
        filterProtocolClaims: false,
        loadUserInfo: true
    };
    /** @type {?} */
    const finalSettings = Object.assign(defaults, clientSettings);
    return new OidcAuthenticationService(finalSettings, router, activatedRoute, platformLocation, locationStrategy);
}
export class OidcAuthModule {
}
OidcAuthModule.decorators = [
    { type: NgModule, args: [{
                declarations: [CallbackComponent, SilentRefreshComponent, ProtectedRouteComponent, LoginIframeComponent],
                imports: [OidcRoutingModule, CommonModule],
                providers: [
                    OidcConfigDepHolder,
                    // { provide: APP_INITIALIZER, useFactory: initializeUser, deps: [], multi: true },
                    {
                        // tslint:disable-next-line:object-literal-shorthand
                        provide: IAuthenticationServiceToken,
                        useFactory: InitOidcAuthenticationService,
                        deps: [
                            OidcConfigDepHolder,
                            // ActivatedRoute,
                            Router,
                            PlatformLocation,
                            ActivatedRoute,
                            LocationStrategy,
                        ]
                    },
                ]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib2lkYy1tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9hdXRoL2F1dGhlbnRpY2F0aW9uLW9pZGMvb2lkYy1tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSwyQkFBMkIsRUFBRSxNQUFNLHVFQUF1RSxDQUFDO0FBR3BILE9BQU8sRUFBRSx5QkFBeUIsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBQzFFLE9BQU8sRUFDTCxRQUFRLEVBQXVCLGNBQWMsRUFBaUIsTUFBTSxFQUNwRSxVQUFVLEVBQ1gsTUFBTSxlQUFlLENBQUM7QUFDdkIsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDekQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0saUNBQWlDLENBQUM7QUFDcEUsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFDOUUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUl6RCxPQUFPLEVBQUMsZ0JBQWdCLEVBQUUsZ0JBQWdCLEVBQUUsWUFBWSxFQUFDLE1BQU0saUJBQWlCLENBQUM7QUFDakYsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0scUNBQXFDLENBQUM7QUFDM0UsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sd0NBQXdDLENBQUM7Ozs7QUFHakYsMkNBSUM7OztJQUhDLDJDQUFtQjs7Ozs7SUFFbkIsMkNBQTZDOzs7QUFFL0MsTUFBTSxPQUFPLFdBQVcsR0FBRyxJQUFJLGNBQWMsQ0FBd0IsYUFBYSxDQUFDO0FBSW5GLE1BQU0sT0FBTyxtQkFBbUI7Ozs7SUFDOUIsWUFDOEIscUJBQTRDO1FBQTVDLDBCQUFxQixHQUFyQixxQkFBcUIsQ0FBdUI7SUFHMUUsQ0FBQzs7O1lBTkYsVUFBVTs7Ozs0Q0FHTixNQUFNLFNBQUMsV0FBVzs7OztJQUFuQixvREFBd0U7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBYzVFLE1BQU0sVUFBVSw2QkFBNkIsQ0FDM0MsbUJBQXdDLEVBQUUsTUFBYyxFQUFFLGdCQUFrQyxFQUM1RixjQUE4QixFQUFFLGdCQUFrQzs7VUFFMUQsY0FBYyxHQUFHLG1CQUFtQixDQUFDLHFCQUFxQjtJQUVoRSxjQUFjLENBQUMsVUFBVSxHQUFHLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQzs7VUFHdkYsUUFBUSxHQUFHLGdCQUFnQixDQUFDLGtCQUFrQixFQUFFOztVQUNoRCxNQUFNLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxNQUFNOztVQUMvQixxQkFBcUIsR0FBRyxNQUFNLEdBQUcsUUFBUTs7VUFHekMsYUFBYSxHQUFHLGNBQWMsQ0FBQyxVQUFVLElBQUkscUJBQXFCOztVQUVsRSxRQUFRLEdBQXdCO1FBQ3BDLFlBQVksRUFBRSxHQUFHLGFBQWEsVUFBVTtRQUN4Qyx3QkFBd0IsRUFBRSxHQUFHLGFBQWEsRUFBRTtRQUM1QyxhQUFhLEVBQUUsZ0JBQWdCO1FBQy9CLEtBQUssRUFBRSwyQ0FBMkM7UUFDbEQsbUJBQW1CLEVBQUUsR0FBRyxhQUFhLGVBQWU7UUFDcEQsb0JBQW9CLEVBQUUsSUFBSTtRQUMxQixvQkFBb0IsRUFBRSxLQUFLO1FBQzNCLFlBQVksRUFBRSxJQUFJO0tBQ25COztVQUVLLGFBQWEsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxjQUFjLENBQUM7SUFFN0QsT0FBTyxJQUFJLHlCQUF5QixDQUFDLGFBQWEsRUFBRSxNQUFNLEVBQUUsY0FBYyxFQUFFLGdCQUFnQixFQUFFLGdCQUFnQixDQUFDLENBQUM7QUFDcEgsQ0FBQztBQTBCRCxNQUFNLE9BQU8sY0FBYzs7O1lBeEIxQixRQUFRLFNBQUM7Z0JBQ1IsWUFBWSxFQUFFLENBQUMsaUJBQWlCLEVBQUUsc0JBQXNCLEVBQUUsdUJBQXVCLEVBQUUsb0JBQW9CLENBQUM7Z0JBQ3hHLE9BQU8sRUFBRSxDQUFDLGlCQUFpQixFQUFFLFlBQVksQ0FBQztnQkFDMUMsU0FBUyxFQUFFO29CQUNULG1CQUFtQjtvQkFDbkIsbUZBQW1GO29CQUVuRjs7d0JBRUUsT0FBTyxFQUFFLDJCQUEyQjt3QkFDcEMsVUFBVSxFQUFFLDZCQUE2Qjt3QkFDekMsSUFBSSxFQUNGOzRCQUNFLG1CQUFtQjs0QkFDbkIsa0JBQWtCOzRCQUNsQixNQUFNOzRCQUNOLGdCQUFnQjs0QkFDaEIsY0FBYzs0QkFDZCxnQkFBZ0I7eUJBQ2pCO3FCQUNKO2lCQUVGO2FBQ0YiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJQXV0aGVudGljYXRpb25TZXJ2aWNlVG9rZW4gfSBmcm9tICcuLy4uL2F1dGhlbnRpY2F0aW9uL2Fic3RyYWN0aW9uL3Byb3ZpZGVyLWF1dGhlbnRpY2F0aW9uLXNlcnZpY2UudG9rZW4nO1xuLy8gaW1wb3J0IHsgT2lkY1VzZXJUcmFuc2Zvcm1hdGlvblNlcnZpY2UgfSBmcm9tIGAuL29pZGMtdXNlci10cmFuc2Zvcm1hdGlvbi5zZXJ2aWNlYDtcbmltcG9ydCB7IFVzZXJTZXJ2aWNlIH0gZnJvbSAnLi8uLi9hdXRoZW50aWNhdGlvbi91c2VyLnNlcnZpY2UnO1xuaW1wb3J0IHsgT2lkY0F1dGhlbnRpY2F0aW9uU2VydmljZSB9IGZyb20gJy4vb2lkYy1hdXRoZW50aWNhdGlvbi5zZXJ2aWNlJztcbmltcG9ydCB7XG4gIE5nTW9kdWxlLCBNb2R1bGVXaXRoUHJvdmlkZXJzLCBJbmplY3Rpb25Ub2tlbiwgVmFsdWVQcm92aWRlciwgSW5qZWN0LFxuICBJbmplY3RhYmxlLCBUeXBlLCBPcHRpb25hbCwgZm9yd2FyZFJlZiwgUHJvdmlkZXIsIEFQUF9JTklUSUFMSVpFUlxufSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE9pZGNSb3V0aW5nTW9kdWxlIH0gZnJvbSAnLi9vaWRjLXJvdXRlci5tb2R1bGUnO1xuaW1wb3J0IHsgQ2FsbGJhY2tDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvY2FsbGJhY2suY29tcG9uZW50JztcbmltcG9ydCB7IFNpbGVudFJlZnJlc2hDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvc2lsZW50cmVmcmVzaC5jb21wb25lbnQnO1xuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGUsIFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQgeyBJQXV0aGVudGljYXRpb25NYW5hZ2VyIH0gZnJvbSAnLi4vYXV0aGVudGljYXRpb24vYXV0aGVudGljYXRpb24tbWFuYWdlcic7XG5pbXBvcnQgeyBJQXV0aGVudGljYXRpb25NYW5hZ2VyVG9rZW4gfSBmcm9tICcuLi9hdXRoZW50aWNhdGlvbi9hdXRoZW50aWNhdGlvbi1tYW5hZ2VyLnRva2VuJztcbmltcG9ydCB7IFVzZXJNYW5hZ2VyU2V0dGluZ3MsIFVzZXJNYW5hZ2VyLCBXZWJTdG9yYWdlU3RhdGVTdG9yZSB9IGZyb20gJ29pZGMtY2xpZW50JztcbmltcG9ydCB7UGxhdGZvcm1Mb2NhdGlvbiwgTG9jYXRpb25TdHJhdGVneSwgQ29tbW9uTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHsgTG9naW5JZnJhbWVDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvbG9naW4taWZyYW1lLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBQcm90ZWN0ZWRSb3V0ZUNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9wcm90ZWN0ZWQtcm91dGUuY29tcG9uZW50JztcblxuLy8gZXhwb3J0IHR5cGUgT3BlbklEQ29ubmVjdFNldHRpbmdzID0gT2lkYy5Vc2VyTWFuYWdlclNldHRpbmdzICYgeyBjbGllbnRfdXJpOiBzdHJpbmcgfTtcbmV4cG9ydCBpbnRlcmZhY2UgT3BlbklEQ29ubmVjdFNldHRpbmdzIGV4dGVuZHMgVXNlck1hbmFnZXJTZXR0aW5ncyB7XG4gIGNsaWVudF91cmk6IHN0cmluZztcbiAgLyoqIE1vZG8gZGUgY29tbyBlZmV0dWFyIG8gbG9naW4uIChQYWRyw6NvIMOpICdpZnJhbWUnIHF1YW5kbyBuw6NvIGluZm9ybWFkbykgKi9cbiAgbG9naW5fbW9kZT86ICdyZWRpcmVjdCcgfCAncG9wdXAnIHwgJ2lmcmFtZSc7XG59XG5leHBvcnQgY29uc3QgT0lEQ19DT05GSUcgPSBuZXcgSW5qZWN0aW9uVG9rZW48T3BlbklEQ29ubmVjdFNldHRpbmdzPihgT0lEQ19DT05GSUdgKTtcblxuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgT2lkY0NvbmZpZ0RlcEhvbGRlciB7XG4gIGNvbnN0cnVjdG9yKFxuICAgIEBJbmplY3QoT0lEQ19DT05GSUcpIHB1YmxpYyBvcGVuSURDb25uZWN0U2V0dGluZ3M6IE9wZW5JRENvbm5lY3RTZXR0aW5nc1xuICApIHtcblxuICB9XG59XG5cbi8vIEBJbmplY3RhYmxlKClcbi8vIGV4cG9ydCBjbGFzcyBBdXRoTWFuYWdlckRlcEhvbGRlciB7XG4vLyAgIGNvbnN0cnVjdG9yKFxuLy8gICAgIEBJbmplY3QoSUF1dGhlbnRpY2F0aW9uTWFuYWdlclRva2VuKSBAT3B0aW9uYWwoKSBwdWJsaWMgYXV0aGVudGljYXRpb25NYW5hZ2VyOiBJQXV0aGVudGljYXRpb25NYW5hZ2VyXG4vLyAgICkge1xuLy8gICB9XG4vLyB9XG5cbmV4cG9ydCBmdW5jdGlvbiBJbml0T2lkY0F1dGhlbnRpY2F0aW9uU2VydmljZShcbiAgb2lkY0NvbmZpZ0RlcEhvbGRlcjogT2lkY0NvbmZpZ0RlcEhvbGRlciwgcm91dGVyOiBSb3V0ZXIsIHBsYXRmb3JtTG9jYXRpb246IFBsYXRmb3JtTG9jYXRpb24sXG4gIGFjdGl2YXRlZFJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSwgbG9jYXRpb25TdHJhdGVneTogTG9jYXRpb25TdHJhdGVneSkge1xuXG4gICAgY29uc3QgY2xpZW50U2V0dGluZ3MgPSBvaWRjQ29uZmlnRGVwSG9sZGVyLm9wZW5JRENvbm5lY3RTZXR0aW5ncztcblxuICAgIGNsaWVudFNldHRpbmdzLmxvZ2luX21vZGUgPSBjbGllbnRTZXR0aW5ncy5sb2dpbl9tb2RlID8gY2xpZW50U2V0dGluZ3MubG9naW5fbW9kZSA6ICdpZnJhbWUnO1xuXG5cbiAgICBjb25zdCBiYXNlSHJlZiA9IHBsYXRmb3JtTG9jYXRpb24uZ2V0QmFzZUhyZWZGcm9tRE9NKCk7XG4gICAgY29uc3Qgb3JpZ2luID0gd2luZG93LmxvY2F0aW9uLm9yaWdpbjtcbiAgICBjb25zdCBjb21wbGV0ZVVybFRvQmFzZUhyZWYgPSBvcmlnaW4gKyBiYXNlSHJlZjtcblxuXG4gICAgY29uc3QgYmFzZUNsaWVudFVyaSA9IGNsaWVudFNldHRpbmdzLmNsaWVudF91cmkgfHwgY29tcGxldGVVcmxUb0Jhc2VIcmVmO1xuXG4gICAgY29uc3QgZGVmYXVsdHM6IFVzZXJNYW5hZ2VyU2V0dGluZ3MgPSB7XG4gICAgICByZWRpcmVjdF91cmk6IGAke2Jhc2VDbGllbnRVcml9Y2FsbGJhY2tgLFxuICAgICAgcG9zdF9sb2dvdXRfcmVkaXJlY3RfdXJpOiBgJHtiYXNlQ2xpZW50VXJpfWAsXG4gICAgICByZXNwb25zZV90eXBlOiBgaWRfdG9rZW4gdG9rZW5gLFxuICAgICAgc2NvcGU6IGBvcGVuaWQgcHJvZmlsZSBlbWFpbCByb2xlcyBvZmZsaW5lX2FjY2Vzc2AsXG4gICAgICBzaWxlbnRfcmVkaXJlY3RfdXJpOiBgJHtiYXNlQ2xpZW50VXJpfXNpbGVudHJlZnJlc2hgLFxuICAgICAgYXV0b21hdGljU2lsZW50UmVuZXc6IHRydWUsXG4gICAgICBmaWx0ZXJQcm90b2NvbENsYWltczogZmFsc2UsXG4gICAgICBsb2FkVXNlckluZm86IHRydWVcbiAgICB9O1xuXG4gICAgY29uc3QgZmluYWxTZXR0aW5ncyA9IE9iamVjdC5hc3NpZ24oZGVmYXVsdHMsIGNsaWVudFNldHRpbmdzKTtcblxuICAgIHJldHVybiBuZXcgT2lkY0F1dGhlbnRpY2F0aW9uU2VydmljZShmaW5hbFNldHRpbmdzLCByb3V0ZXIsIGFjdGl2YXRlZFJvdXRlLCBwbGF0Zm9ybUxvY2F0aW9uLCBsb2NhdGlvblN0cmF0ZWd5KTtcbn1cblxuQE5nTW9kdWxlKHtcbiAgZGVjbGFyYXRpb25zOiBbQ2FsbGJhY2tDb21wb25lbnQsIFNpbGVudFJlZnJlc2hDb21wb25lbnQsIFByb3RlY3RlZFJvdXRlQ29tcG9uZW50LCBMb2dpbklmcmFtZUNvbXBvbmVudF0sXG4gIGltcG9ydHM6IFtPaWRjUm91dGluZ01vZHVsZSwgQ29tbW9uTW9kdWxlXSxcbiAgcHJvdmlkZXJzOiBbXG4gICAgT2lkY0NvbmZpZ0RlcEhvbGRlcixcbiAgICAvLyB7IHByb3ZpZGU6IEFQUF9JTklUSUFMSVpFUiwgdXNlRmFjdG9yeTogaW5pdGlhbGl6ZVVzZXIsIGRlcHM6IFtdLCBtdWx0aTogdHJ1ZSB9LFxuXG4gICAge1xuICAgICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOm9iamVjdC1saXRlcmFsLXNob3J0aGFuZFxuICAgICAgcHJvdmlkZTogSUF1dGhlbnRpY2F0aW9uU2VydmljZVRva2VuLFxuICAgICAgdXNlRmFjdG9yeTogSW5pdE9pZGNBdXRoZW50aWNhdGlvblNlcnZpY2UsXG4gICAgICBkZXBzOlxuICAgICAgICBbXG4gICAgICAgICAgT2lkY0NvbmZpZ0RlcEhvbGRlcixcbiAgICAgICAgICAvLyBBY3RpdmF0ZWRSb3V0ZSxcbiAgICAgICAgICBSb3V0ZXIsXG4gICAgICAgICAgUGxhdGZvcm1Mb2NhdGlvbixcbiAgICAgICAgICBBY3RpdmF0ZWRSb3V0ZSxcbiAgICAgICAgICBMb2NhdGlvblN0cmF0ZWd5LFxuICAgICAgICBdXG4gICAgfSxcbiAgICAvLyBPaWRjVXNlclRyYW5zZm9ybWF0aW9uU2VydmljZVxuICBdXG59KVxuZXhwb3J0IGNsYXNzIE9pZGNBdXRoTW9kdWxlIHtcbn1cblxuIl19