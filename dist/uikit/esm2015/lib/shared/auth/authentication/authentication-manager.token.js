/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { InjectionToken } from '@angular/core';
/** @type {?} */
export const IAuthenticationManagerToken = new InjectionToken('IAuthenticationManager');
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aGVudGljYXRpb24tbWFuYWdlci50b2tlbi5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2F1dGgvYXV0aGVudGljYXRpb24vYXV0aGVudGljYXRpb24tbWFuYWdlci50b2tlbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLGVBQWUsQ0FBQzs7QUFHL0MsTUFBTSxPQUFPLDJCQUEyQixHQUFHLElBQUksY0FBYyxDQUF5Qix3QkFBd0IsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGlvblRva2VuIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBJQXV0aGVudGljYXRpb25NYW5hZ2VyIH0gZnJvbSAnLi9hdXRoZW50aWNhdGlvbi1tYW5hZ2VyJztcblxuZXhwb3J0IGNvbnN0IElBdXRoZW50aWNhdGlvbk1hbmFnZXJUb2tlbiA9IG5ldyBJbmplY3Rpb25Ub2tlbjxJQXV0aGVudGljYXRpb25NYW5hZ2VyPignSUF1dGhlbnRpY2F0aW9uTWFuYWdlcicpO1xuIl19