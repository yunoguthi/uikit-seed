/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';
import { map } from 'rxjs/operators';
import { AuthorizationConfigToken } from '../../authorization/authorization-config.token';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
import * as i2 from "../../authorization/authorization-config.token";
/** @type {?} */
const groupBy = (/**
 * @template T
 * @param {?} items
 * @param {?} keyFunction
 * @return {?}
 */
function (items, keyFunction) {
    /** @type {?} */
    const groups = {};
    items.forEach((/**
     * @param {?} el
     * @return {?}
     */
    function (el) {
        /** @type {?} */
        var key = keyFunction(el);
        if (key in groups == false) {
            groups[key] = [];
        }
        groups[key].push(el);
    }));
    return Object.keys(groups).map((/**
     * @param {?} key
     * @return {?}
     */
    function (key) {
        return {
            key: key,
            values: (/** @type {?} */ (groups[key]))
        };
    }));
});
const ɵ0 = groupBy;
/**
 * @record
 */
function EntitledAttributesDTOs() { }
if (false) {
    /** @type {?} */
    EntitledAttributesDTOs.prototype.resourceName;
    /** @type {?} */
    EntitledAttributesDTOs.prototype.action;
    /** @type {?} */
    EntitledAttributesDTOs.prototype.environment;
    /** @type {?} */
    EntitledAttributesDTOs.prototype.allActions;
    /** @type {?} */
    EntitledAttributesDTOs.prototype.allResources;
    /** @type {?} */
    EntitledAttributesDTOs.prototype.attributeDTOs;
}
/**
 * @record
 */
function IResultEntitlementAllRequest() { }
if (false) {
    /** @type {?} */
    IResultEntitlementAllRequest.prototype.entitledResultSetDTO;
}
export class Wso2AuthenticationManager {
    /**
     * @param {?} httpClient
     * @param {?} authorizationConfig
     */
    constructor(httpClient, authorizationConfig) {
        this.httpClient = httpClient;
        this.authorizationConfig = authorizationConfig;
    }
    /**
     * @param {?} user
     * @return {?}
     */
    transform(user) {
        return tslib_1.__awaiter(this, void 0, void 0, /** @this {!Wso2AuthenticationManager} */ function* () {
            // Faço request para buscar todas as permissões do usuário (resource owner) para um client específico
            // Neste caso uso o padrão User Managed Access para retornar as informações
            // https://localhost:9443/api/identity/entitlement/decision/entitlements-all
            return this.httpClient
                .post(`${this.authorizationConfig.xacmlConfig.entitlementsAllEndpoint}`, {
                identifier: '',
                givenAttributes: []
            }, {
                headers: new HttpHeaders()
                    .set('Content-Type', 'application/json')
                    .set('Accept', 'application/json')
                    .set('Authorization', `Bearer ${user.access_token}`)
            })
                .pipe(map((/**
             * @param {?} result
             * @return {?}
             */
            (result) => result.entitledResultSetDTO.entitledAttributesDTOs)))
                .pipe(map((/**
             * @param {?} result
             * @return {?}
             */
            (result) => {
                /** @type {?} */
                const authorizations = groupBy(result, (
                // tslint:disable-next-line:max-line-length
                /**
                 * @param {?} item
                 * @return {?}
                 */
                (item) => item.attributeDTOs.find((/**
                 * @param {?} attr
                 * @return {?}
                 */
                attr => attr.attributeId === 'urn:oasis:names:tc:xacml:1.0:resource:resource-id')).attributeValue));
                /** @type {?} */
                const permissions = authorizations.map((/**
                 * @param {?} perm
                 * @return {?}
                 */
                perm => ({
                    resource: perm.key,
                    action: perm.values.map((/**
                     * @param {?} r
                     * @return {?}
                     */
                    r => r.attributeDTOs.find((/**
                     * @param {?} a
                     * @return {?}
                     */
                    a => a.attributeId === 'urn:oasis:names:tc:xacml:1.0:action:action-id')).attributeValue))
                })));
                /** @type {?} */
                const returnUser = (/** @type {?} */ (Object.assign({}, user)));
                returnUser.authorizations = permissions;
                return returnUser;
            })))
                .toPromise();
        });
    }
}
Wso2AuthenticationManager.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
Wso2AuthenticationManager.ctorParameters = () => [
    { type: HttpClient },
    { type: undefined, decorators: [{ type: Inject, args: [AuthorizationConfigToken,] }] }
];
/** @nocollapse */ Wso2AuthenticationManager.ngInjectableDef = i0.defineInjectable({ factory: function Wso2AuthenticationManager_Factory() { return new Wso2AuthenticationManager(i0.inject(i1.HttpClient), i0.inject(i2.AuthorizationConfigToken)); }, token: Wso2AuthenticationManager, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @protected
     */
    Wso2AuthenticationManager.prototype.httpClient;
    /**
     * @type {?}
     * @protected
     */
    Wso2AuthenticationManager.prototype.authorizationConfig;
}
export { ɵ0 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid3NvMi1wcm92aWRlci1hdXRoZW50aWNhdGlvbi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvYXV0aC9hdXRoZW50aWNhdGlvbi93c28yL3dzbzItcHJvdmlkZXItYXV0aGVudGljYXRpb24uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQWMsV0FBVyxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFFM0UsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDbkQsT0FBTyxFQUFFLEdBQUcsRUFBVyxNQUFNLGdCQUFnQixDQUFDO0FBRTlDLE9BQU8sRUFBdUIsd0JBQXdCLEVBQUUsTUFBTSxnREFBZ0QsQ0FBQzs7Ozs7TUFLekcsT0FBTzs7Ozs7O0FBQUcsVUFBWSxLQUFlLEVBQUUsV0FBOEI7O1VBQ25FLE1BQU0sR0FBRyxFQUFFO0lBQ2pCLEtBQUssQ0FBQyxPQUFPOzs7O0lBQUMsVUFBUyxFQUFFOztZQUNqQixHQUFHLEdBQUcsV0FBVyxDQUFDLEVBQUUsQ0FBQztRQUN6QixJQUFJLEdBQUcsSUFBSSxNQUFNLElBQUksS0FBSyxFQUFFO1lBQ3hCLE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLENBQUM7U0FDcEI7UUFDRCxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ3pCLENBQUMsRUFBQyxDQUFDO0lBQ0gsT0FBTyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEdBQUc7Ozs7SUFBQyxVQUFTLEdBQUc7UUFDdkMsT0FBTztZQUNILEdBQUcsRUFBRSxHQUFHO1lBQ1IsTUFBTSxFQUFFLG1CQUFBLE1BQU0sQ0FBQyxHQUFHLENBQUMsRUFBTztTQUM3QixDQUFDO0lBQ04sQ0FBQyxFQUFDLENBQUM7QUFDTCxDQUFDLENBQUE7Ozs7O0FBVUQscUNBWUc7OztJQVhELDhDQUFtQjs7SUFDakIsd0NBQWE7O0lBQ2IsNkNBQWtCOztJQUNsQiw0Q0FBa0I7O0lBQ2xCLDhDQUFvQjs7SUFDcEIsK0NBS0c7Ozs7O0FBR1AsMkNBSUM7OztJQUhDLDREQUVFOztBQU1KLE1BQU0sT0FBTyx5QkFBeUI7Ozs7O0lBQ3BDLFlBQ1ksVUFBc0IsRUFFdEIsbUJBQXdDO1FBRnhDLGVBQVUsR0FBVixVQUFVLENBQVk7UUFFdEIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtJQUNqRCxDQUFDOzs7OztJQUNTLFNBQVMsQ0FDcEIsSUFBdUI7O1lBR3ZCLHFHQUFxRztZQUNyRywyRUFBMkU7WUFFM0UsNEVBQTRFO1lBQzVFLE9BQU8sSUFBSSxDQUFDLFVBQVU7aUJBQ25CLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLENBQUMsdUJBQXVCLEVBQUUsRUFDckU7Z0JBQ0UsVUFBVSxFQUFFLEVBQUU7Z0JBQ2QsZUFBZSxFQUFFLEVBQUU7YUFDcEIsRUFDRDtnQkFDRSxPQUFPLEVBQUUsSUFBSSxXQUFXLEVBQUU7cUJBQ3pCLEdBQUcsQ0FBQyxjQUFjLEVBQUUsa0JBQWtCLENBQUM7cUJBQ3ZDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsa0JBQWtCLENBQUM7cUJBQ2pDLEdBQUcsQ0FBQyxlQUFlLEVBQUUsVUFBVSxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7YUFDckQsQ0FDRjtpQkFDQSxJQUFJLENBQUMsR0FBRzs7OztZQUFDLENBQUMsTUFBb0MsRUFBRSxFQUFFLENBQUMsTUFBTSxDQUFDLG9CQUFvQixDQUFDLHNCQUFzQixFQUFDLENBQUM7aUJBQ3ZHLElBQUksQ0FBQyxHQUFHOzs7O1lBQUMsQ0FBQyxNQUFNLEVBQUUsRUFBRTs7c0JBQ2IsY0FBYyxHQUFHLE9BQU8sQ0FBQyxNQUFNOzs7Ozs7Z0JBRW5DLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUk7Ozs7Z0JBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxLQUFLLG1EQUFtRCxFQUFDLENBQUMsY0FBYyxFQUNuSTs7c0JBRUssV0FBVyxHQUFHLGNBQWMsQ0FBQyxHQUFHOzs7O2dCQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztvQkFDOUMsUUFBUSxFQUFFLElBQUksQ0FBQyxHQUFHO29CQUNsQixNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHOzs7O29CQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxJQUFJOzs7O29CQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFdBQVcsS0FBSywrQ0FBK0MsRUFBQyxDQUFDLGNBQWMsRUFBQztpQkFDMUksQ0FBQyxFQUFDOztzQkFHRyxVQUFVLEdBQUcscUNBQUssSUFBSSxHQUFnRDtnQkFDNUUsVUFBVSxDQUFDLGNBQWMsR0FBRyxXQUFXLENBQUM7Z0JBR3hDLE9BQU8sVUFBVSxDQUFDO1lBRXBCLENBQUMsRUFBQyxDQUFDO2lCQUNGLFNBQVMsRUFBRSxDQUFDO1FBRWpCLENBQUM7S0FBQTs7O1lBcERGLFVBQVUsU0FBQztnQkFDVixVQUFVLEVBQUUsTUFBTTthQUNuQjs7OztZQXpEUSxVQUFVOzRDQTZEZCxNQUFNLFNBQUMsd0JBQXdCOzs7Ozs7OztJQURoQywrQ0FBZ0M7Ozs7O0lBQ2hDLHdEQUNrRCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEh0dHBDbGllbnQsIEh0dHBQYXJhbXMsIEh0dHBIZWFkZXJzIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuXG5pbXBvcnQgeyBJbmplY3RhYmxlLCBJbmplY3QgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IG1hcCwgZmxhdE1hcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcbmltcG9ydCB7IElBdXRoZW50aWNhdGlvbk1hbmFnZXIgfSBmcm9tICcuLi9hdXRoZW50aWNhdGlvbi1tYW5hZ2VyJztcbmltcG9ydCB7IEF1dGhvcml6YXRpb25Db25maWcsIEF1dGhvcml6YXRpb25Db25maWdUb2tlbiB9IGZyb20gJy4uLy4uL2F1dGhvcml6YXRpb24vYXV0aG9yaXphdGlvbi1jb25maWcudG9rZW4nO1xuaW1wb3J0IHsgT3BlbklEQ29ubmVjdFVzZXIsIFVzZXJXaXRoQXV0aG9yaXphdGlvbnMgfSBmcm9tICcuLi91c2VyLm1vZGVsJztcblxuXG5cbmNvbnN0IGdyb3VwQnkgPSBmdW5jdGlvbjxUPihpdGVtczogQXJyYXk8VD4sIGtleUZ1bmN0aW9uOiAoaXRlbXM6IFQpID0+IGFueSkge1xuICBjb25zdCBncm91cHMgPSB7fTtcbiAgaXRlbXMuZm9yRWFjaChmdW5jdGlvbihlbCkge1xuICAgICAgdmFyIGtleSA9IGtleUZ1bmN0aW9uKGVsKTtcbiAgICAgIGlmIChrZXkgaW4gZ3JvdXBzID09IGZhbHNlKSB7XG4gICAgICAgICAgZ3JvdXBzW2tleV0gPSBbXTtcbiAgICAgIH1cbiAgICAgIGdyb3Vwc1trZXldLnB1c2goZWwpO1xuICB9KTtcbiAgcmV0dXJuIE9iamVjdC5rZXlzKGdyb3VwcykubWFwKGZ1bmN0aW9uKGtleSkge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgICBrZXk6IGtleSxcbiAgICAgICAgICB2YWx1ZXM6IGdyb3Vwc1trZXldIGFzIFRbXVxuICAgICAgfTtcbiAgfSk7XG59O1xuXG5cblxuXG5cblxuXG5cblxuaW50ZXJmYWNlIEVudGl0bGVkQXR0cmlidXRlc0RUT3Mge1xuICByZXNvdXJjZU5hbWU6IG51bGw7XG4gICAgYWN0aW9uOiBudWxsO1xuICAgIGVudmlyb25tZW50OiBudWxsO1xuICAgIGFsbEFjdGlvbnM6IGZhbHNlO1xuICAgIGFsbFJlc291cmNlczogZmFsc2U7XG4gICAgYXR0cmlidXRlRFRPczogQXJyYXk8e1xuICAgICAgYXR0cmlidXRlVmFsdWU6IHN0cmluZyxcbiAgICAgIGF0dHJpYnV0ZURhdGFUeXBlOiAnaHR0cDovL3d3dy53My5vcmcvMjAwMS9YTUxTY2hlbWEjc3RyaW5nJyxcbiAgICAgIGF0dHJpYnV0ZUlkOiAndXJuOm9hc2lzOm5hbWVzOnRjOnhhY21sOjEuMDpyZXNvdXJjZTpyZXNvdXJjZS1pZCcgfCAndXJuOm9hc2lzOm5hbWVzOnRjOnhhY21sOjEuMDpzdWJqZWN0OnN1YmplY3QtaWQnIHwgJ3VybjpvYXNpczpuYW1lczp0Yzp4YWNtbDoxLjA6YWN0aW9uOmFjdGlvbi1pZCcsXG4gICAgICBjYXRlZ29yeTogJ3VybjpvYXNpczpuYW1lczp0Yzp4YWNtbDozLjA6YXR0cmlidXRlLWNhdGVnb3J5OnJlc291cmNlJyB8ICd1cm46b2FzaXM6bmFtZXM6dGM6eGFjbWw6MS4wOnN1YmplY3QtY2F0ZWdvcnk6YWNjZXNzLXN1YmplY3QnIHwgJ3VybjpvYXNpczpuYW1lczp0Yzp4YWNtbDozLjA6YXR0cmlidXRlLWNhdGVnb3J5OmFjdGlvbicsXG4gICAgfT47XG4gIH1cblxuaW50ZXJmYWNlIElSZXN1bHRFbnRpdGxlbWVudEFsbFJlcXVlc3Qge1xuICBlbnRpdGxlZFJlc3VsdFNldERUTzoge1xuICAgIGVudGl0bGVkQXR0cmlidXRlc0RUT3M6IEFycmF5PEVudGl0bGVkQXR0cmlidXRlc0RUT3M+LFxuICB9O1xufVxuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBXc28yQXV0aGVudGljYXRpb25NYW5hZ2VyIGltcGxlbWVudHMgSUF1dGhlbnRpY2F0aW9uTWFuYWdlciB7XG4gIGNvbnN0cnVjdG9yKFxuICAgIHByb3RlY3RlZCBodHRwQ2xpZW50OiBIdHRwQ2xpZW50LFxuICAgIEBJbmplY3QoQXV0aG9yaXphdGlvbkNvbmZpZ1Rva2VuKVxuICAgIHByb3RlY3RlZCBhdXRob3JpemF0aW9uQ29uZmlnOiBBdXRob3JpemF0aW9uQ29uZmlnXG4gICkge31cbiAgcHVibGljIGFzeW5jIHRyYW5zZm9ybShcbiAgICB1c2VyOiBPcGVuSURDb25uZWN0VXNlclxuICApOiBQcm9taXNlPE9wZW5JRENvbm5lY3RVc2VyICYgVXNlcldpdGhBdXRob3JpemF0aW9ucz4ge1xuXG4gICAgLy8gRmHDp28gcmVxdWVzdCBwYXJhIGJ1c2NhciB0b2RhcyBhcyBwZXJtaXNzw7VlcyBkbyB1c3XDoXJpbyAocmVzb3VyY2Ugb3duZXIpIHBhcmEgdW0gY2xpZW50IGVzcGVjw61maWNvXG4gICAgLy8gTmVzdGUgY2FzbyB1c28gbyBwYWRyw6NvIFVzZXIgTWFuYWdlZCBBY2Nlc3MgcGFyYSByZXRvcm5hciBhcyBpbmZvcm1hw6fDtWVzXG5cbiAgICAvLyBodHRwczovL2xvY2FsaG9zdDo5NDQzL2FwaS9pZGVudGl0eS9lbnRpdGxlbWVudC9kZWNpc2lvbi9lbnRpdGxlbWVudHMtYWxsXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudFxuICAgICAgLnBvc3QoYCR7dGhpcy5hdXRob3JpemF0aW9uQ29uZmlnLnhhY21sQ29uZmlnLmVudGl0bGVtZW50c0FsbEVuZHBvaW50fWAsXG4gICAgICAgIHtcbiAgICAgICAgICBpZGVudGlmaWVyOiAnJyxcbiAgICAgICAgICBnaXZlbkF0dHJpYnV0ZXM6IFtdXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICBoZWFkZXJzOiBuZXcgSHR0cEhlYWRlcnMoKVxuICAgICAgICAgIC5zZXQoJ0NvbnRlbnQtVHlwZScsICdhcHBsaWNhdGlvbi9qc29uJylcbiAgICAgICAgICAuc2V0KCdBY2NlcHQnLCAnYXBwbGljYXRpb24vanNvbicpXG4gICAgICAgICAgLnNldCgnQXV0aG9yaXphdGlvbicsIGBCZWFyZXIgJHt1c2VyLmFjY2Vzc190b2tlbn1gKVxuICAgICAgICB9XG4gICAgICApXG4gICAgICAucGlwZShtYXAoKHJlc3VsdDogSVJlc3VsdEVudGl0bGVtZW50QWxsUmVxdWVzdCkgPT4gcmVzdWx0LmVudGl0bGVkUmVzdWx0U2V0RFRPLmVudGl0bGVkQXR0cmlidXRlc0RUT3MpKVxuICAgICAgLnBpcGUobWFwKChyZXN1bHQpID0+IHtcbiAgICAgICAgY29uc3QgYXV0aG9yaXphdGlvbnMgPSBncm91cEJ5KHJlc3VsdCxcbiAgICAgICAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6bWF4LWxpbmUtbGVuZ3RoXG4gICAgICAgICAgKGl0ZW0pID0+IGl0ZW0uYXR0cmlidXRlRFRPcy5maW5kKGF0dHIgPT4gYXR0ci5hdHRyaWJ1dGVJZCA9PT0gJ3VybjpvYXNpczpuYW1lczp0Yzp4YWNtbDoxLjA6cmVzb3VyY2U6cmVzb3VyY2UtaWQnKS5hdHRyaWJ1dGVWYWx1ZVxuICAgICAgICApO1xuXG4gICAgICAgIGNvbnN0IHBlcm1pc3Npb25zID0gYXV0aG9yaXphdGlvbnMubWFwKHBlcm0gPT4gKHtcbiAgICAgICAgICByZXNvdXJjZTogcGVybS5rZXksXG4gICAgICAgICAgYWN0aW9uOiBwZXJtLnZhbHVlcy5tYXAociA9PiByLmF0dHJpYnV0ZURUT3MuZmluZChhID0+IGEuYXR0cmlidXRlSWQgPT09ICd1cm46b2FzaXM6bmFtZXM6dGM6eGFjbWw6MS4wOmFjdGlvbjphY3Rpb24taWQnKS5hdHRyaWJ1dGVWYWx1ZSlcbiAgICAgICAgfSkpO1xuXG5cbiAgICAgICAgY29uc3QgcmV0dXJuVXNlciA9IHsgLi4udXNlciB9IGFzIE9wZW5JRENvbm5lY3RVc2VyICYgVXNlcldpdGhBdXRob3JpemF0aW9ucztcbiAgICAgICAgcmV0dXJuVXNlci5hdXRob3JpemF0aW9ucyA9IHBlcm1pc3Npb25zO1xuXG5cbiAgICAgICAgcmV0dXJuIHJldHVyblVzZXI7XG5cbiAgICAgIH0pKVxuICAgICAgLnRvUHJvbWlzZSgpO1xuXG4gIH1cbn1cblxuXG4iXX0=