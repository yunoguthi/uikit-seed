/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Inject, Injectable, Optional } from '@angular/core';
import { Location, PlatformLocation, LocationStrategy, HashLocationStrategy } from '@angular/common';
import { Router } from '@angular/router';
import { IAuthenticationServiceToken } from './abstraction/provider-authentication-service.token';
import { IAuthenticationManagerToken } from './authentication-manager.token';
import { UserService } from './user.service';
/**
 * @template TLoginParam, TLogoutParam
 */
export class AuthenticationService {
    /**
     * @param {?} userService
     * @param {?} ngLocation
     * @param {?} platformLocation
     * @param {?} locationStrategy
     * @param {?} router
     * @param {?=} providerAuthenticationService
     * @param {?=} authenticationManager
     */
    constructor(userService, ngLocation, platformLocation, locationStrategy, router, providerAuthenticationService, authenticationManager) {
        this.userService = userService;
        this.ngLocation = ngLocation;
        this.platformLocation = platformLocation;
        this.locationStrategy = locationStrategy;
        this.router = router;
        this.providerAuthenticationService = providerAuthenticationService;
        this.authenticationManager = authenticationManager;
        if (this.providerAuthenticationService) {
            this.providerAuthenticationService.user$.subscribe((/**
             * @param {?} user
             * @return {?}
             */
            user => {
                // Transforma do Framework para o do Produto (caso exista)
                if (this.authenticationManager && user) {
                    this.authenticationManager.transform(user).then((/**
                     * @param {?} userAuthenticated
                     * @return {?}
                     */
                    (userAuthenticated) => this.load(userAuthenticated)));
                }
                else {
                    this.load(user);
                }
            }));
        }
    }
    /**
     * @param {?=} args
     * @return {?}
     */
    login(args) {
        /** @type {?} */
        const isOnline = navigator.onLine;
        if (isOnline) {
            if (args && args.enderecoParaVoltar) {
                localStorage.setItem('authentication-callback', args.enderecoParaVoltar);
            }
            /** @type {?} */
            const lastUri = localStorage.getItem('authentication-callback');
            if (!lastUri) {
                localStorage.setItem('authentication-callback', window.location.href);
            }
            /** @type {?} */
            const param = args && args.param;
            return this.providerAuthenticationService.login(param);
        }
        else {
            throw new Error('Não é possível se autenticar sem conexão de rede!');
        }
    }
    /**
     * @private
     * @param {?} providerTransformedUser
     * @return {?}
     */
    load(providerTransformedUser) {
        if (providerTransformedUser != null && providerTransformedUser.authenticated) {
            this.userService.load(providerTransformedUser);
            this.goToLastUri();
        }
        else {
            this.userService.unload();
        }
    }
    /**
     * @protected
     * @return {?}
     */
    goToLastUri() {
        /** @type {?} */
        const lastUri = localStorage.getItem('authentication-callback');
        /** @type {?} */
        const baseHref = this.platformLocation.getBaseHrefFromDOM();
        /** @type {?} */
        const origin = window.location.origin;
        /** @type {?} */
        let hashStrategy = '';
        if (this.locationStrategy instanceof HashLocationStrategy) {
            hashStrategy = '#';
        }
        /** @type {?} */
        const completeUrlToBaseHref = origin + baseHref + hashStrategy;
        if (lastUri && (lastUri.indexOf('protected-route') == -1)) {
            /** @type {?} */
            const uriToNavigate = lastUri.replace(completeUrlToBaseHref, '');
            // let uriToNavigate = lastUri;
            // if (uriToNavigate.startsWith(baseHref)) {
            //   uriToNavigate = uriToNavigate.replace(baseHref, '');
            // }
            localStorage.removeItem('authentication-callback');
            console.debug('Navegando para página: ', lastUri);
            this.router.navigateByUrl(uriToNavigate);
        }
        else {
            this.router.navigateByUrl('');
        }
    }
    /**
     * @param {?=} param
     * @return {?}
     */
    logout(param) {
        return this.providerAuthenticationService.logout(param);
    }
}
AuthenticationService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
AuthenticationService.ctorParameters = () => [
    { type: UserService },
    { type: Location },
    { type: PlatformLocation },
    { type: LocationStrategy },
    { type: Router },
    { type: undefined, decorators: [{ type: Inject, args: [IAuthenticationServiceToken,] }, { type: Optional }] },
    { type: undefined, decorators: [{ type: Inject, args: [IAuthenticationManagerToken,] }, { type: Optional }] }
];
if (false) {
    /**
     * @type {?}
     * @protected
     */
    AuthenticationService.prototype.userService;
    /**
     * @type {?}
     * @private
     */
    AuthenticationService.prototype.ngLocation;
    /**
     * @type {?}
     * @private
     */
    AuthenticationService.prototype.platformLocation;
    /**
     * @type {?}
     * @private
     */
    AuthenticationService.prototype.locationStrategy;
    /**
     * @type {?}
     * @private
     */
    AuthenticationService.prototype.router;
    /**
     * @type {?}
     * @private
     */
    AuthenticationService.prototype.providerAuthenticationService;
    /**
     * @type {?}
     * @protected
     */
    AuthenticationService.prototype.authenticationManager;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aGVudGljYXRpb24uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2F1dGgvYXV0aGVudGljYXRpb24vYXV0aGVudGljYXRpb24uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLE1BQU0sRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQzNELE9BQU8sRUFBRSxRQUFRLEVBQUUsZ0JBQWdCLEVBQUUsZ0JBQWdCLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUNyRyxPQUFPLEVBQUMsTUFBTSxFQUFDLE1BQU0saUJBQWlCLENBQUM7QUFFdkMsT0FBTyxFQUFDLDJCQUEyQixFQUFDLE1BQU0scURBQXFELENBQUM7QUFDaEcsT0FBTyxFQUFDLDJCQUEyQixFQUFDLE1BQU0sZ0NBQWdDLENBQUM7QUFFM0UsT0FBTyxFQUFDLFdBQVcsRUFBQyxNQUFNLGdCQUFnQixDQUFDOzs7O0FBSTNDLE1BQU0sT0FBTyxxQkFBcUI7Ozs7Ozs7Ozs7SUFHaEMsWUFDWSxXQUF3QixFQUMxQixVQUFvQixFQUNwQixnQkFBa0MsRUFDbEMsZ0JBQWtDLEVBQ2xDLE1BQWMsRUFFbUMsNkJBQW9FLEVBQ2xFLHFCQUE4QztRQVAvRixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUMxQixlQUFVLEdBQVYsVUFBVSxDQUFVO1FBQ3BCLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFDbEMscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUNsQyxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBRW1DLGtDQUE2QixHQUE3Qiw2QkFBNkIsQ0FBdUM7UUFDbEUsMEJBQXFCLEdBQXJCLHFCQUFxQixDQUF5QjtRQUV6RyxJQUFJLElBQUksQ0FBQyw2QkFBNkIsRUFBRTtZQUN0QyxJQUFJLENBQUMsNkJBQTZCLENBQUMsS0FBSyxDQUFDLFNBQVM7Ozs7WUFBQyxJQUFJLENBQUMsRUFBRTtnQkFDeEQsMERBQTBEO2dCQUMxRCxJQUFJLElBQUksQ0FBQyxxQkFBcUIsSUFBSSxJQUFJLEVBQUU7b0JBQ3RDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSTs7OztvQkFBQyxDQUFDLGlCQUFpQixFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEVBQUMsQ0FBQztpQkFDdEc7cUJBQU07b0JBQ0wsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDakI7WUFDSCxDQUFDLEVBQUMsQ0FBQztTQUNKO0lBQ0gsQ0FBQzs7Ozs7SUFFRCxLQUFLLENBQUMsSUFBMkQ7O2NBQ3pELFFBQVEsR0FBRyxTQUFTLENBQUMsTUFBTTtRQUNqQyxJQUFJLFFBQVEsRUFBRTtZQUVaLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxrQkFBa0IsRUFBRTtnQkFDbkMsWUFBWSxDQUFDLE9BQU8sQ0FBQyx5QkFBeUIsRUFBRSxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQzthQUMxRTs7a0JBRUssT0FBTyxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMseUJBQXlCLENBQUM7WUFDL0QsSUFBSSxDQUFDLE9BQU8sRUFBRTtnQkFDWixZQUFZLENBQUMsT0FBTyxDQUFDLHlCQUF5QixFQUFFLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDdkU7O2tCQUNLLEtBQUssR0FBRyxJQUFJLElBQUksSUFBSSxDQUFDLEtBQUs7WUFDaEMsT0FBTyxJQUFJLENBQUMsNkJBQTZCLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3hEO2FBQU07WUFDTCxNQUFNLElBQUksS0FBSyxDQUFDLG1EQUFtRCxDQUFDLENBQUM7U0FDdEU7SUFDSCxDQUFDOzs7Ozs7SUFFTyxJQUFJLENBQUMsdUJBQTZCO1FBQ3hDLElBQUksdUJBQXVCLElBQUksSUFBSSxJQUFJLHVCQUF1QixDQUFDLGFBQWEsRUFBRTtZQUM1RSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDO1lBQy9DLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUNwQjthQUFNO1lBQ0wsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsQ0FBQztTQUMzQjtJQUNILENBQUM7Ozs7O0lBRVMsV0FBVzs7Y0FDYixPQUFPLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyx5QkFBeUIsQ0FBQzs7Y0FHekQsUUFBUSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxrQkFBa0IsRUFBRTs7Y0FDckQsTUFBTSxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTTs7WUFDakMsWUFBWSxHQUFHLEVBQUU7UUFDckIsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLFlBQVksb0JBQW9CLEVBQUU7WUFDekQsWUFBWSxHQUFHLEdBQUcsQ0FBQztTQUNwQjs7Y0FDSyxxQkFBcUIsR0FBRyxNQUFNLEdBQUcsUUFBUSxHQUFHLFlBQVk7UUFHOUQsSUFBSSxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRTs7a0JBSW5ELGFBQWEsR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLHFCQUFxQixFQUFFLEVBQUUsQ0FBQztZQUdoRSwrQkFBK0I7WUFDL0IsNENBQTRDO1lBQzVDLHlEQUF5RDtZQUN6RCxJQUFJO1lBRUosWUFBWSxDQUFDLFVBQVUsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1lBQ25ELE9BQU8sQ0FBQyxLQUFLLENBQUMseUJBQXlCLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDbEQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLENBQUM7U0FDMUM7YUFBTTtZQUNMLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1NBQy9CO0lBQ0gsQ0FBQzs7Ozs7SUFFRCxNQUFNLENBQUMsS0FBb0I7UUFDekIsT0FBTyxJQUFJLENBQUMsNkJBQTZCLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzFELENBQUM7OztZQXpGRixVQUFVOzs7O1lBSEgsV0FBVztZQU5WLFFBQVE7WUFBRSxnQkFBZ0I7WUFBRSxnQkFBZ0I7WUFDN0MsTUFBTTs0Q0FtQlQsTUFBTSxTQUFDLDJCQUEyQixjQUFHLFFBQVE7NENBQzdDLE1BQU0sU0FBQywyQkFBMkIsY0FBRyxRQUFROzs7Ozs7O0lBUDlDLDRDQUFrQzs7Ozs7SUFDbEMsMkNBQTRCOzs7OztJQUM1QixpREFBMEM7Ozs7O0lBQzFDLGlEQUEwQzs7Ozs7SUFDMUMsdUNBQXNCOzs7OztJQUV0Qiw4REFBNkg7Ozs7O0lBQzdILHNEQUF5RyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SW5qZWN0LCBJbmplY3RhYmxlLCBPcHRpb25hbH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBMb2NhdGlvbiwgUGxhdGZvcm1Mb2NhdGlvbiwgTG9jYXRpb25TdHJhdGVneSwgSGFzaExvY2F0aW9uU3RyYXRlZ3kgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHtSb3V0ZXJ9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQge1VzZXJ9IGZyb20gJy4vdXNlci5tb2RlbCc7XG5pbXBvcnQge0lBdXRoZW50aWNhdGlvblNlcnZpY2VUb2tlbn0gZnJvbSAnLi9hYnN0cmFjdGlvbi9wcm92aWRlci1hdXRoZW50aWNhdGlvbi1zZXJ2aWNlLnRva2VuJztcbmltcG9ydCB7SUF1dGhlbnRpY2F0aW9uTWFuYWdlclRva2VufSBmcm9tICcuL2F1dGhlbnRpY2F0aW9uLW1hbmFnZXIudG9rZW4nO1xuaW1wb3J0IHtJQXV0aGVudGljYXRpb25NYW5hZ2VyfSBmcm9tICcuL2F1dGhlbnRpY2F0aW9uLW1hbmFnZXInO1xuaW1wb3J0IHtVc2VyU2VydmljZX0gZnJvbSAnLi91c2VyLnNlcnZpY2UnO1xuaW1wb3J0IHtJUHJvdmlkZXJBdXRoZW50aWNhdGlvblNlcnZpY2V9IGZyb20gJy4vYWJzdHJhY3Rpb24vcHJvdmlkZXItYXV0aGVudGljYXRpb24uc2VydmljZSc7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBBdXRoZW50aWNhdGlvblNlcnZpY2U8VExvZ2luUGFyYW0gPSBhbnksIFRMb2dvdXRQYXJhbSA9IGFueT4ge1xuXG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJvdGVjdGVkIHVzZXJTZXJ2aWNlOiBVc2VyU2VydmljZSxcbiAgICBwcml2YXRlIG5nTG9jYXRpb246IExvY2F0aW9uLFxuICAgIHByaXZhdGUgcGxhdGZvcm1Mb2NhdGlvbjogUGxhdGZvcm1Mb2NhdGlvbixcbiAgICBwcml2YXRlIGxvY2F0aW9uU3RyYXRlZ3k6IExvY2F0aW9uU3RyYXRlZ3ksXG4gICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcbiAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6bWF4LWxpbmUtbGVuZ3RoXG4gICAgQEluamVjdChJQXV0aGVudGljYXRpb25TZXJ2aWNlVG9rZW4pIEBPcHRpb25hbCgpIHByaXZhdGUgcHJvdmlkZXJBdXRoZW50aWNhdGlvblNlcnZpY2U/OiBJUHJvdmlkZXJBdXRoZW50aWNhdGlvblNlcnZpY2U8VXNlcj4sXG4gICAgQEluamVjdChJQXV0aGVudGljYXRpb25NYW5hZ2VyVG9rZW4pIEBPcHRpb25hbCgpIHByb3RlY3RlZCBhdXRoZW50aWNhdGlvbk1hbmFnZXI/OiBJQXV0aGVudGljYXRpb25NYW5hZ2VyLFxuICApIHtcbiAgICBpZiAodGhpcy5wcm92aWRlckF1dGhlbnRpY2F0aW9uU2VydmljZSkge1xuICAgICAgdGhpcy5wcm92aWRlckF1dGhlbnRpY2F0aW9uU2VydmljZS51c2VyJC5zdWJzY3JpYmUodXNlciA9PiB7XG4gICAgICAgIC8vIFRyYW5zZm9ybWEgZG8gRnJhbWV3b3JrIHBhcmEgbyBkbyBQcm9kdXRvIChjYXNvIGV4aXN0YSlcbiAgICAgICAgaWYgKHRoaXMuYXV0aGVudGljYXRpb25NYW5hZ2VyICYmIHVzZXIpIHtcbiAgICAgICAgICB0aGlzLmF1dGhlbnRpY2F0aW9uTWFuYWdlci50cmFuc2Zvcm0odXNlcikudGhlbigodXNlckF1dGhlbnRpY2F0ZWQpID0+IHRoaXMubG9hZCh1c2VyQXV0aGVudGljYXRlZCkpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHRoaXMubG9hZCh1c2VyKTtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgfVxuICB9XG5cbiAgbG9naW4oYXJncz86IHsgcGFyYW0/OiBUTG9naW5QYXJhbSwgZW5kZXJlY29QYXJhVm9sdGFyPzogc3RyaW5nIH0pOiBQcm9taXNlPHZvaWQ+IHtcbiAgICBjb25zdCBpc09ubGluZSA9IG5hdmlnYXRvci5vbkxpbmU7XG4gICAgaWYgKGlzT25saW5lKSB7XG5cbiAgICAgIGlmIChhcmdzICYmIGFyZ3MuZW5kZXJlY29QYXJhVm9sdGFyKSB7XG4gICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdhdXRoZW50aWNhdGlvbi1jYWxsYmFjaycsIGFyZ3MuZW5kZXJlY29QYXJhVm9sdGFyKTtcbiAgICAgIH1cblxuICAgICAgY29uc3QgbGFzdFVyaSA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdhdXRoZW50aWNhdGlvbi1jYWxsYmFjaycpO1xuICAgICAgaWYgKCFsYXN0VXJpKSB7XG4gICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdhdXRoZW50aWNhdGlvbi1jYWxsYmFjaycsIHdpbmRvdy5sb2NhdGlvbi5ocmVmKTtcbiAgICAgIH1cbiAgICAgIGNvbnN0IHBhcmFtID0gYXJncyAmJiBhcmdzLnBhcmFtO1xuICAgICAgcmV0dXJuIHRoaXMucHJvdmlkZXJBdXRoZW50aWNhdGlvblNlcnZpY2UubG9naW4ocGFyYW0pO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ07Do28gw6kgcG9zc8OtdmVsIHNlIGF1dGVudGljYXIgc2VtIGNvbmV4w6NvIGRlIHJlZGUhJyk7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBsb2FkKHByb3ZpZGVyVHJhbnNmb3JtZWRVc2VyOiBVc2VyKSB7XG4gICAgaWYgKHByb3ZpZGVyVHJhbnNmb3JtZWRVc2VyICE9IG51bGwgJiYgcHJvdmlkZXJUcmFuc2Zvcm1lZFVzZXIuYXV0aGVudGljYXRlZCkge1xuICAgICAgdGhpcy51c2VyU2VydmljZS5sb2FkKHByb3ZpZGVyVHJhbnNmb3JtZWRVc2VyKTtcbiAgICAgIHRoaXMuZ29Ub0xhc3RVcmkoKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy51c2VyU2VydmljZS51bmxvYWQoKTtcbiAgICB9XG4gIH1cblxuICBwcm90ZWN0ZWQgZ29Ub0xhc3RVcmkoKSB7XG4gICAgY29uc3QgbGFzdFVyaSA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdhdXRoZW50aWNhdGlvbi1jYWxsYmFjaycpO1xuXG5cbiAgICBjb25zdCBiYXNlSHJlZiA9IHRoaXMucGxhdGZvcm1Mb2NhdGlvbi5nZXRCYXNlSHJlZkZyb21ET00oKTtcbiAgICBjb25zdCBvcmlnaW4gPSB3aW5kb3cubG9jYXRpb24ub3JpZ2luO1xuICAgIGxldCBoYXNoU3RyYXRlZ3kgPSAnJztcbiAgICBpZiAodGhpcy5sb2NhdGlvblN0cmF0ZWd5IGluc3RhbmNlb2YgSGFzaExvY2F0aW9uU3RyYXRlZ3kpIHtcbiAgICAgIGhhc2hTdHJhdGVneSA9ICcjJztcbiAgICB9XG4gICAgY29uc3QgY29tcGxldGVVcmxUb0Jhc2VIcmVmID0gb3JpZ2luICsgYmFzZUhyZWYgKyBoYXNoU3RyYXRlZ3k7XG5cblxuICAgIGlmIChsYXN0VXJpICYmIChsYXN0VXJpLmluZGV4T2YoJ3Byb3RlY3RlZC1yb3V0ZScpID09IC0xKSkge1xuXG5cblxuICAgICAgY29uc3QgdXJpVG9OYXZpZ2F0ZSA9IGxhc3RVcmkucmVwbGFjZShjb21wbGV0ZVVybFRvQmFzZUhyZWYsICcnKTtcblxuXG4gICAgICAvLyBsZXQgdXJpVG9OYXZpZ2F0ZSA9IGxhc3RVcmk7XG4gICAgICAvLyBpZiAodXJpVG9OYXZpZ2F0ZS5zdGFydHNXaXRoKGJhc2VIcmVmKSkge1xuICAgICAgLy8gICB1cmlUb05hdmlnYXRlID0gdXJpVG9OYXZpZ2F0ZS5yZXBsYWNlKGJhc2VIcmVmLCAnJyk7XG4gICAgICAvLyB9XG5cbiAgICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKCdhdXRoZW50aWNhdGlvbi1jYWxsYmFjaycpO1xuICAgICAgY29uc29sZS5kZWJ1ZygnTmF2ZWdhbmRvIHBhcmEgcMOhZ2luYTogJywgbGFzdFVyaSk7XG4gICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZUJ5VXJsKHVyaVRvTmF2aWdhdGUpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZUJ5VXJsKCcnKTtcbiAgICB9XG4gIH1cblxuICBsb2dvdXQocGFyYW0/OiBUTG9nb3V0UGFyYW0pOiBQcm9taXNlPHZvaWQ+IHtcbiAgICByZXR1cm4gdGhpcy5wcm92aWRlckF1dGhlbnRpY2F0aW9uU2VydmljZS5sb2dvdXQocGFyYW0pO1xuICB9XG5cbn1cbiJdfQ==