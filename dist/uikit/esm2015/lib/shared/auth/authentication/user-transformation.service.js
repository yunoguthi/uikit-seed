/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
/**
 * @template SourceUser, DestinationUser
 */
export class ProviderUserTransformationService {
    constructor() { }
    /**
     * @param {?} user
     * @return {?}
     */
    transform(user) {
        /** @type {?} */
        const destinationUser = (/** @type {?} */ ({}));
        /** @type {?} */
        const combinedUser = Object.assign(destinationUser, user);
        combinedUser.authenticated = true;
        return combinedUser;
    }
}
ProviderUserTransformationService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
ProviderUserTransformationService.ctorParameters = () => [];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci10cmFuc2Zvcm1hdGlvbi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvYXV0aC9hdXRoZW50aWNhdGlvbi91c2VyLXRyYW5zZm9ybWF0aW9uLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7Ozs7QUFJM0MsTUFBTSxPQUFPLGlDQUFpQztJQUU1QyxnQkFBZ0IsQ0FBQzs7Ozs7SUFFVixTQUFTLENBQUMsSUFBZ0I7O2NBQ3pCLGVBQWUsR0FBb0IsbUJBQUssRUFBRSxFQUFBOztjQUMxQyxZQUFZLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDO1FBQ3pELFlBQVksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1FBQ2xDLE9BQU8sWUFBWSxDQUFDO0lBQ3RCLENBQUM7OztZQVZGLFVBQVUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBVc2VyIH0gZnJvbSAnLi91c2VyLm1vZGVsJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIFByb3ZpZGVyVXNlclRyYW5zZm9ybWF0aW9uU2VydmljZTxTb3VyY2VVc2VyLCBEZXN0aW5hdGlvblVzZXIgZXh0ZW5kcyBVc2VyPiB7XG5cbiAgY29uc3RydWN0b3IoKSB7IH1cblxuICBwdWJsaWMgdHJhbnNmb3JtKHVzZXI6IFNvdXJjZVVzZXIpOiBEZXN0aW5hdGlvblVzZXIge1xuICAgIGNvbnN0IGRlc3RpbmF0aW9uVXNlcjogRGVzdGluYXRpb25Vc2VyID0gPGFueT57fTtcbiAgICBjb25zdCBjb21iaW5lZFVzZXIgPSBPYmplY3QuYXNzaWduKGRlc3RpbmF0aW9uVXNlciwgdXNlcik7XG4gICAgY29tYmluZWRVc2VyLmF1dGhlbnRpY2F0ZWQgPSB0cnVlO1xuICAgIHJldHVybiBjb21iaW5lZFVzZXI7XG4gIH1cbn1cblxuIl19