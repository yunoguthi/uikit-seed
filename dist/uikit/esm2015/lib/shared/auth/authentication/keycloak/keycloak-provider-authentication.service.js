/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';
import { map } from 'rxjs/operators';
import { AuthorizationConfigToken } from '../../authorization/authorization-config.token';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
import * as i2 from "../../authorization/authorization-config.token";
export class KeycloakAuthenticationManager {
    /**
     * @param {?} httpClient
     * @param {?} authorizationConfig
     */
    constructor(httpClient, authorizationConfig) {
        this.httpClient = httpClient;
        this.authorizationConfig = authorizationConfig;
    }
    /**
     * @param {?} user
     * @return {?}
     */
    transform(user) {
        return tslib_1.__awaiter(this, void 0, void 0, /** @this {!KeycloakAuthenticationManager} */ function* () {
            // Faço request para buscar todas as permissões do usuário (resource owner) para um client específico
            // Neste caso uso o padrão User Managed Access para retornar as informações
            return this.httpClient
                .post(`${this.authorizationConfig.umaConfig.tokenEndpoint}`, new HttpParams()
                .set('grant_type', 'urn:ietf:params:oauth:grant-type:uma-ticket')
                .set('audience', `${this.authorizationConfig.clientId}`)
                .toString(), {
                headers: new HttpHeaders()
                    .set('Content-Type', 'application/x-www-form-urlencoded')
                    .set('Authorization', `Bearer ${user.access_token}`)
            })
                // tslint:disable-next-line:max-line-length
                .pipe(map((/**
             * @param {?} result
             * @return {?}
             */
            (result) => {
                // Devo então buscar o payload do access_token e incluir (substituindo o access_token anterior) as informações de autorização
                /** @type {?} */
                const accessToken = result.access_token.split('.')[1].replace('-', '+').replace('_', '/');
                /** @type {?} */
                const accessTokenPayload1 = JSON.parse(atob(accessToken));
                /** @type {?} */
                const usuarioFinal1 = (/** @type {?} */ (Object.assign(user, accessTokenPayload1)));
                usuarioFinal1.access_token = result.access_token;
                ((/** @type {?} */ (usuarioFinal1))).access_token = result.access_token;
                return (/** @type {?} */ (usuarioFinal1));
            })))
                // tslint:disable-next-line:max-line-length
                .pipe(map((/**
             * @param {?} user
             * @return {?}
             */
            (user) => {
                // Devo agora retornar as informações do usuário no formado esperado (OpenIDConnectUser & UserWithAuthorizations)
                /** @type {?} */
                let userToReturn = (/** @type {?} */ (JSON.parse(JSON.stringify(user))));
                delete userToReturn['authorization'];
                userToReturn.authorizations = user.authorization.permissions.map((/**
                 * @param {?} permission
                 * @return {?}
                 */
                permission => {
                    return { action: permission.scopes, resource: permission.rsname, resourceId: permission.rsid };
                }));
                return userToReturn;
            })))
                .toPromise();
        });
    }
}
KeycloakAuthenticationManager.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
KeycloakAuthenticationManager.ctorParameters = () => [
    { type: HttpClient },
    { type: undefined, decorators: [{ type: Inject, args: [AuthorizationConfigToken,] }] }
];
/** @nocollapse */ KeycloakAuthenticationManager.ngInjectableDef = i0.defineInjectable({ factory: function KeycloakAuthenticationManager_Factory() { return new KeycloakAuthenticationManager(i0.inject(i1.HttpClient), i0.inject(i2.AuthorizationConfigToken)); }, token: KeycloakAuthenticationManager, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @protected
     */
    KeycloakAuthenticationManager.prototype.httpClient;
    /**
     * @type {?}
     * @protected
     */
    KeycloakAuthenticationManager.prototype.authorizationConfig;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoia2V5Y2xvYWstcHJvdmlkZXItYXV0aGVudGljYXRpb24uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2F1dGgvYXV0aGVudGljYXRpb24va2V5Y2xvYWsva2V5Y2xvYWstcHJvdmlkZXItYXV0aGVudGljYXRpb24uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQzNFLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ25ELE9BQU8sRUFBRSxHQUFHLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNyQyxPQUFPLEVBQXVCLHdCQUF3QixFQUFFLE1BQU0sZ0RBQWdELENBQUM7Ozs7QUFPL0csTUFBTSxPQUFPLDZCQUE2Qjs7Ozs7SUFDeEMsWUFDWSxVQUFzQixFQUV0QixtQkFBd0M7UUFGeEMsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUV0Qix3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO0lBQ2pELENBQUM7Ozs7O0lBQ1MsU0FBUyxDQUNwQixJQUF1Qjs7WUFHdkIscUdBQXFHO1lBQ3JHLDJFQUEyRTtZQUMzRSxPQUFPLElBQUksQ0FBQyxVQUFVO2lCQUNuQixJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsU0FBUyxDQUFDLGFBQWEsRUFBRSxFQUN6RCxJQUFJLFVBQVUsRUFBRTtpQkFDZixHQUFHLENBQUMsWUFBWSxFQUFFLDZDQUE2QyxDQUFDO2lCQUNoRSxHQUFHLENBQUMsVUFBVSxFQUFFLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUN2RCxRQUFRLEVBQUUsRUFDWDtnQkFDRSxPQUFPLEVBQUUsSUFBSSxXQUFXLEVBQUU7cUJBQ3pCLEdBQUcsQ0FBQyxjQUFjLEVBQUUsbUNBQW1DLENBQUM7cUJBQ3hELEdBQUcsQ0FBQyxlQUFlLEVBQUUsVUFBVSxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7YUFDckQsQ0FDRjtnQkFDRCwyQ0FBMkM7aUJBQzFDLElBQUksQ0FBQyxHQUFHOzs7O1lBQUMsQ0FBQyxNQUErSSxFQUFFLEVBQUU7OztzQkFFdEosV0FBVyxHQUFHLE1BQU0sQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUM7O3NCQUNuRixtQkFBbUIsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQzs7c0JBRW5ELGFBQWEsR0FBRyxtQkFBQSxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxtQkFBbUIsQ0FBQyxFQUFxQjtnQkFDbkYsYUFBYSxDQUFDLFlBQVksR0FBRyxNQUFNLENBQUMsWUFBWSxDQUFDO2dCQUNqRCxDQUFDLG1CQUFBLGFBQWEsRUFBTyxDQUFDLENBQUMsWUFBWSxHQUFHLE1BQU0sQ0FBQyxZQUFZLENBQUM7Z0JBRTFELE9BQU8sbUJBQUEsYUFBYSxFQUFPLENBQUM7WUFDOUIsQ0FBQyxFQUFDLENBQUM7Z0JBQ0gsMkNBQTJDO2lCQUMxQyxJQUFJLENBQUMsR0FBRzs7OztZQUFDLENBQUMsSUFBNEgsRUFBRSxFQUFFOzs7b0JBRXJJLFlBQVksR0FBRyxtQkFBQSxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsRUFDaUQ7Z0JBRXBHLE9BQU8sWUFBWSxDQUFDLGVBQWUsQ0FBQyxDQUFDO2dCQUVyQyxZQUFZLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDLEdBQUc7Ozs7Z0JBQUMsVUFBVSxDQUFDLEVBQUU7b0JBQzVFLE9BQU8sRUFBRSxNQUFNLEVBQUUsVUFBVSxDQUFDLE1BQU0sRUFBRSxRQUFRLEVBQUUsVUFBVSxDQUFDLE1BQU0sRUFBRSxVQUFVLEVBQUUsVUFBVSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUNqRyxDQUFDLEVBQUMsQ0FBQztnQkFFSCxPQUFPLFlBQVksQ0FBQztZQUN0QixDQUFDLEVBQUMsQ0FBQztpQkFDRixTQUFTLEVBQUUsQ0FBQztRQUVqQixDQUFDO0tBQUE7OztZQXZERixVQUFVLFNBQUM7Z0JBQ1YsVUFBVSxFQUFFLE1BQU07YUFDbkI7Ozs7WUFUUSxVQUFVOzRDQWFkLE1BQU0sU0FBQyx3QkFBd0I7Ozs7Ozs7O0lBRGhDLG1EQUFnQzs7Ozs7SUFDaEMsNERBQ2tEIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSHR0cENsaWVudCwgSHR0cFBhcmFtcywgSHR0cEhlYWRlcnMgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBJbmplY3RhYmxlLCBJbmplY3QgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IG1hcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcbmltcG9ydCB7IEF1dGhvcml6YXRpb25Db25maWcsIEF1dGhvcml6YXRpb25Db25maWdUb2tlbiB9IGZyb20gJy4uLy4uL2F1dGhvcml6YXRpb24vYXV0aG9yaXphdGlvbi1jb25maWcudG9rZW4nO1xuaW1wb3J0IHsgT3BlbklEQ29ubmVjdFVzZXIsIFVzZXJXaXRoQXV0aG9yaXphdGlvbnMgfSBmcm9tICcuLi91c2VyLm1vZGVsJztcbmltcG9ydCB7IElBdXRoZW50aWNhdGlvbk1hbmFnZXIgfSBmcm9tICcuLi9hdXRoZW50aWNhdGlvbi1tYW5hZ2VyJztcblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgS2V5Y2xvYWtBdXRoZW50aWNhdGlvbk1hbmFnZXIgaW1wbGVtZW50cyBJQXV0aGVudGljYXRpb25NYW5hZ2VyIHtcbiAgY29uc3RydWN0b3IoXG4gICAgcHJvdGVjdGVkIGh0dHBDbGllbnQ6IEh0dHBDbGllbnQsXG4gICAgQEluamVjdChBdXRob3JpemF0aW9uQ29uZmlnVG9rZW4pXG4gICAgcHJvdGVjdGVkIGF1dGhvcml6YXRpb25Db25maWc6IEF1dGhvcml6YXRpb25Db25maWdcbiAgKSB7fVxuICBwdWJsaWMgYXN5bmMgdHJhbnNmb3JtKFxuICAgIHVzZXI6IE9wZW5JRENvbm5lY3RVc2VyXG4gICk6IFByb21pc2U8T3BlbklEQ29ubmVjdFVzZXIgJiBVc2VyV2l0aEF1dGhvcml6YXRpb25zPiB7XG5cbiAgICAvLyBGYcOnbyByZXF1ZXN0IHBhcmEgYnVzY2FyIHRvZGFzIGFzIHBlcm1pc3PDtWVzIGRvIHVzdcOhcmlvIChyZXNvdXJjZSBvd25lcikgcGFyYSB1bSBjbGllbnQgZXNwZWPDrWZpY29cbiAgICAvLyBOZXN0ZSBjYXNvIHVzbyBvIHBhZHLDo28gVXNlciBNYW5hZ2VkIEFjY2VzcyBwYXJhIHJldG9ybmFyIGFzIGluZm9ybWHDp8O1ZXNcbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50XG4gICAgICAucG9zdChgJHt0aGlzLmF1dGhvcml6YXRpb25Db25maWcudW1hQ29uZmlnLnRva2VuRW5kcG9pbnR9YCxcbiAgICAgICAgbmV3IEh0dHBQYXJhbXMoKVxuICAgICAgICAuc2V0KCdncmFudF90eXBlJywgJ3VybjppZXRmOnBhcmFtczpvYXV0aDpncmFudC10eXBlOnVtYS10aWNrZXQnKVxuICAgICAgICAuc2V0KCdhdWRpZW5jZScsIGAke3RoaXMuYXV0aG9yaXphdGlvbkNvbmZpZy5jbGllbnRJZH1gKVxuICAgICAgICAudG9TdHJpbmcoKSxcbiAgICAgICAge1xuICAgICAgICAgIGhlYWRlcnM6IG5ldyBIdHRwSGVhZGVycygpXG4gICAgICAgICAgLnNldCgnQ29udGVudC1UeXBlJywgJ2FwcGxpY2F0aW9uL3gtd3d3LWZvcm0tdXJsZW5jb2RlZCcpXG4gICAgICAgICAgLnNldCgnQXV0aG9yaXphdGlvbicsIGBCZWFyZXIgJHt1c2VyLmFjY2Vzc190b2tlbn1gKVxuICAgICAgICB9XG4gICAgICApXG4gICAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6bWF4LWxpbmUtbGVuZ3RoXG4gICAgICAucGlwZShtYXAoKHJlc3VsdDogeyB1cGdyYWRlZDogYm9vbGVhbjsgYWNjZXNzX3Rva2VuOiBzdHJpbmc7IGV4cGlyZXNfaW46IG51bWJlcjsgcmVmcmVzaF9leHBpcmVzX2luOiBudW1iZXI7IHJlZnJlc2hfdG9rZW46IHN0cmluZzsgdG9rZW5fdHlwZTogc3RyaW5nOyB9KSA9PiB7XG4gICAgICAgIC8vIERldm8gZW50w6NvIGJ1c2NhciBvIHBheWxvYWQgZG8gYWNjZXNzX3Rva2VuIGUgaW5jbHVpciAoc3Vic3RpdHVpbmRvIG8gYWNjZXNzX3Rva2VuIGFudGVyaW9yKSBhcyBpbmZvcm1hw6fDtWVzIGRlIGF1dG9yaXphw6fDo29cbiAgICAgICAgY29uc3QgYWNjZXNzVG9rZW4gPSByZXN1bHQuYWNjZXNzX3Rva2VuLnNwbGl0KCcuJylbMV0ucmVwbGFjZSgnLScsICcrJykucmVwbGFjZSgnXycsICcvJyk7XG4gICAgICAgIGNvbnN0IGFjY2Vzc1Rva2VuUGF5bG9hZDEgPSBKU09OLnBhcnNlKGF0b2IoYWNjZXNzVG9rZW4pKTtcblxuICAgICAgICBjb25zdCB1c3VhcmlvRmluYWwxID0gT2JqZWN0LmFzc2lnbih1c2VyLCBhY2Nlc3NUb2tlblBheWxvYWQxKSBhcyBPcGVuSURDb25uZWN0VXNlcjtcbiAgICAgICAgdXN1YXJpb0ZpbmFsMS5hY2Nlc3NfdG9rZW4gPSByZXN1bHQuYWNjZXNzX3Rva2VuO1xuICAgICAgICAodXN1YXJpb0ZpbmFsMSBhcyBhbnkpLmFjY2Vzc190b2tlbiA9IHJlc3VsdC5hY2Nlc3NfdG9rZW47XG5cbiAgICAgICAgcmV0dXJuIHVzdWFyaW9GaW5hbDEgYXMgYW55O1xuICAgICAgfSkpXG4gICAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6bWF4LWxpbmUtbGVuZ3RoXG4gICAgICAucGlwZShtYXAoKHVzZXI6IE9wZW5JRENvbm5lY3RVc2VyICYgeyBhdXRob3JpemF0aW9uOiB7IHBlcm1pc3Npb25zOiBBcnJheTx7IHNjb3BlczogQXJyYXk8c3RyaW5nPiwgcnNpZDogc3RyaW5nLCByc25hbWU6IHN0cmluZyB9PiB9IH0pID0+IHtcbiAgICAgICAgLy8gRGV2byBhZ29yYSByZXRvcm5hciBhcyBpbmZvcm1hw6fDtWVzIGRvIHVzdcOhcmlvIG5vIGZvcm1hZG8gZXNwZXJhZG8gKE9wZW5JRENvbm5lY3RVc2VyICYgVXNlcldpdGhBdXRob3JpemF0aW9ucylcbiAgICAgICAgbGV0IHVzZXJUb1JldHVybiA9IEpTT04ucGFyc2UoSlNPTi5zdHJpbmdpZnkodXNlcikpIGFzIE9wZW5JRENvbm5lY3RVc2VyICYgVXNlcldpdGhBdXRob3JpemF0aW9ucyAmXG4gICAgICAgICAgeyBhdXRob3JpemF0aW9uOiB7IHBlcm1pc3Npb25zOiBBcnJheTx7IHNjb3BlczogQXJyYXk8c3RyaW5nPiwgcnNpZDogc3RyaW5nLCByc25hbWU6IHN0cmluZyB9PiB9IH07XG5cbiAgICAgICAgZGVsZXRlIHVzZXJUb1JldHVyblsnYXV0aG9yaXphdGlvbiddO1xuXG4gICAgICAgIHVzZXJUb1JldHVybi5hdXRob3JpemF0aW9ucyA9IHVzZXIuYXV0aG9yaXphdGlvbi5wZXJtaXNzaW9ucy5tYXAocGVybWlzc2lvbiA9PiB7XG4gICAgICAgICAgcmV0dXJuIHsgYWN0aW9uOiBwZXJtaXNzaW9uLnNjb3BlcywgcmVzb3VyY2U6IHBlcm1pc3Npb24ucnNuYW1lLCByZXNvdXJjZUlkOiBwZXJtaXNzaW9uLnJzaWQgfTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgcmV0dXJuIHVzZXJUb1JldHVybjtcbiAgICAgIH0pKVxuICAgICAgLnRvUHJvbWlzZSgpO1xuXG4gIH1cbn1cbiJdfQ==