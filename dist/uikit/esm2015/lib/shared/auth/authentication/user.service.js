/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
export class AnonymousUser {
    constructor() {
        this.sub = null;
        this.authenticated = false;
        this.name = 'Anônimo';
        this.email = null;
    }
}
if (false) {
    /** @type {?} */
    AnonymousUser.prototype.sub;
    /** @type {?} */
    AnonymousUser.prototype.authenticated;
    /** @type {?} */
    AnonymousUser.prototype.name;
    /** @type {?} */
    AnonymousUser.prototype.email;
}
/**
 * @template TUser
 */
export class UserService {
    constructor() {
        this.anonymousUser = new AnonymousUser();
        this.userSubject$ = new BehaviorSubject((/** @type {?} */ (this.anonymousUser)));
        this.user$ = this.userSubject$.asObservable();
    }
    /**
     * @return {?}
     */
    get userValue() {
        return this.userSubject$.getValue();
    }
    /**
     * Seta o usuário (não deve ser usado na aplicação [somente em testes]).
     * Para login e logout utilize o serviço 'AuthenticationService'
     * @param {?} user
     * @return {?}
     */
    load(user) {
        if (!user) {
            throw Error('É obrigatório informar um usuário!');
        }
        this.userSubject$.next(user);
    }
    /**
     * Seta o usuário como o anônimo (não deve ser usado na aplicação [somente em testes]).
     * Para login e logout utilize o serviço 'AuthenticationService'
     * @return {?}
     */
    unload() {
        this.userSubject$.next((/** @type {?} */ (this.anonymousUser)));
    }
}
UserService.decorators = [
    { type: Injectable }
];
if (false) {
    /**
     * @type {?}
     * @protected
     */
    UserService.prototype.anonymousUser;
    /**
     * @type {?}
     * @private
     */
    UserService.prototype.userSubject$;
    /** @type {?} */
    UserService.prototype.user$;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvYXV0aC9hdXRoZW50aWNhdGlvbi91c2VyLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUNBLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFFdkMsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUUzQyxNQUFNLE9BQU8sYUFBYTtJQUExQjtRQUNFLFFBQUcsR0FBVyxJQUFJLENBQUM7UUFDbkIsa0JBQWEsR0FBRyxLQUFLLENBQUM7UUFDdEIsU0FBSSxHQUFHLFNBQVMsQ0FBQztRQUNqQixVQUFLLEdBQVcsSUFBSSxDQUFDO0lBQ3ZCLENBQUM7Q0FBQTs7O0lBSkMsNEJBQW1COztJQUNuQixzQ0FBc0I7O0lBQ3RCLDZCQUFpQjs7SUFDakIsOEJBQXFCOzs7OztBQUl2QixNQUFNLE9BQU8sV0FBVztJQUR4QjtRQUVZLGtCQUFhLEdBQUcsSUFBSSxhQUFhLEVBQUUsQ0FBQztRQUV0QyxpQkFBWSxHQUEyQixJQUFJLGVBQWUsQ0FBUSxtQkFBQSxJQUFJLENBQUMsYUFBYSxFQUFTLENBQUMsQ0FBQztRQUNoRyxVQUFLLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQXFCbEQsQ0FBQzs7OztJQXBCQyxJQUFXLFNBQVM7UUFDbEIsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQ3RDLENBQUM7Ozs7Ozs7SUFLTSxJQUFJLENBQUMsSUFBVztRQUNyQixJQUFJLENBQUMsSUFBSSxFQUFFO1lBQ1QsTUFBTSxLQUFLLENBQUMsb0NBQW9DLENBQUMsQ0FBQztTQUNuRDtRQUNELElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQy9CLENBQUM7Ozs7OztJQUtNLE1BQU07UUFDWCxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxtQkFBQSxJQUFJLENBQUMsYUFBYSxFQUFTLENBQUMsQ0FBQztJQUN0RCxDQUFDOzs7WUF6QkYsVUFBVTs7Ozs7OztJQUVULG9DQUE4Qzs7Ozs7SUFFOUMsbUNBQXVHOztJQUN2Ryw0QkFBZ0QiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBCZWhhdmlvclN1YmplY3QgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IFVzZXIsIFJvbGUgfSBmcm9tICcuL3VzZXIubW9kZWwnO1xuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5leHBvcnQgY2xhc3MgQW5vbnltb3VzVXNlciBpbXBsZW1lbnRzIFVzZXIge1xuICBzdWI6IHN0cmluZyA9IG51bGw7XG4gIGF1dGhlbnRpY2F0ZWQgPSBmYWxzZTtcbiAgbmFtZSA9ICdBbsO0bmltbyc7XG4gIGVtYWlsOiBzdHJpbmcgPSBudWxsO1xufVxuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgVXNlclNlcnZpY2U8VFVzZXIgZXh0ZW5kcyBVc2VyID0gVXNlcj4ge1xuICBwcm90ZWN0ZWQgYW5vbnltb3VzVXNlciA9IG5ldyBBbm9ueW1vdXNVc2VyKCk7XG5cbiAgcHJpdmF0ZSB1c2VyU3ViamVjdCQ6IEJlaGF2aW9yU3ViamVjdDxUVXNlcj4gPSBuZXcgQmVoYXZpb3JTdWJqZWN0PFRVc2VyPih0aGlzLmFub255bW91c1VzZXIgYXMgVFVzZXIpO1xuICBwdWJsaWMgdXNlciQgPSB0aGlzLnVzZXJTdWJqZWN0JC5hc09ic2VydmFibGUoKTtcbiAgcHVibGljIGdldCB1c2VyVmFsdWUoKSB7XG4gICAgcmV0dXJuIHRoaXMudXNlclN1YmplY3QkLmdldFZhbHVlKCk7XG4gIH1cblxuICAvKiogU2V0YSBvIHVzdcOhcmlvIChuw6NvIGRldmUgc2VyIHVzYWRvIG5hIGFwbGljYcOnw6NvIFtzb21lbnRlIGVtIHRlc3Rlc10pLlxuICAgKiBQYXJhIGxvZ2luIGUgbG9nb3V0IHV0aWxpemUgbyBzZXJ2acOnbyAnQXV0aGVudGljYXRpb25TZXJ2aWNlJ1xuICAgKi9cbiAgcHVibGljIGxvYWQodXNlcjogVFVzZXIpIHtcbiAgICBpZiAoIXVzZXIpIHtcbiAgICAgIHRocm93IEVycm9yKCfDiSBvYnJpZ2F0w7NyaW8gaW5mb3JtYXIgdW0gdXN1w6FyaW8hJyk7XG4gICAgfVxuICAgIHRoaXMudXNlclN1YmplY3QkLm5leHQodXNlcik7XG4gIH1cblxuICAvKiogU2V0YSBvIHVzdcOhcmlvIGNvbW8gbyBhbsO0bmltbyAobsOjbyBkZXZlIHNlciB1c2FkbyBuYSBhcGxpY2HDp8OjbyBbc29tZW50ZSBlbSB0ZXN0ZXNdKS5cbiAgICogUGFyYSBsb2dpbiBlIGxvZ291dCB1dGlsaXplIG8gc2VydmnDp28gJ0F1dGhlbnRpY2F0aW9uU2VydmljZSdcbiAgICovXG4gIHB1YmxpYyB1bmxvYWQoKSB7XG4gICAgdGhpcy51c2VyU3ViamVjdCQubmV4dCh0aGlzLmFub255bW91c1VzZXIgYXMgVFVzZXIpO1xuICB9XG59XG4iXX0=