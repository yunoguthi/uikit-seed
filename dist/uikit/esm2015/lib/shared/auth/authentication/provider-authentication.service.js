/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { BehaviorSubject } from 'rxjs';
// tslint:disable-next-line:max-line-length
/**
 * @abstract
 * @template TUserProvider, TUser, TLoginParam, TLogoutParam
 */
export class ProviderAuthenticationService {
    constructor() {
        this.userProviderSubject = new BehaviorSubject(null);
        // protected userProvider$ = this.userProviderSubject.asObservable();
        this.userSubject = new BehaviorSubject(null);
        this.user$ = this.userSubject.asObservable();
        this.userProviderSubject.subscribe((/**
         * @param {?} userProvider
         * @return {?}
         */
        userProvider => {
            if (userProvider) {
                /** @type {?} */
                const user = this.transform(userProvider);
                this.loadUser(user);
            }
            else {
                this.loadUser(null);
            }
        }));
    }
    /**
     * @param {?=} param
     * @return {?}
     */
    logout(param) {
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            this.loadProviderUser(null);
            resolve();
        }));
    }
    /**
     * @private
     * @param {?} user
     * @return {?}
     */
    loadUser(user) {
        this.userSubject.next(user);
    }
    /**
     * @protected
     * @param {?} user
     * @return {?}
     */
    loadProviderUser(user) {
        this.userProviderSubject.next(user);
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    ProviderAuthenticationService.prototype.userProviderSubject;
    /**
     * @type {?}
     * @private
     */
    ProviderAuthenticationService.prototype.userSubject;
    /** @type {?} */
    ProviderAuthenticationService.prototype.user$;
    /**
     * @abstract
     * @param {?=} param
     * @return {?}
     */
    ProviderAuthenticationService.prototype.login = function (param) { };
    /**
     * @abstract
     * @protected
     * @param {?} providerUser
     * @return {?}
     */
    ProviderAuthenticationService.prototype.transform = function (providerUser) { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvdmlkZXItYXV0aGVudGljYXRpb24uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2F1dGgvYXV0aGVudGljYXRpb24vcHJvdmlkZXItYXV0aGVudGljYXRpb24uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBR0EsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLE1BQU0sQ0FBQzs7Ozs7O0FBTXZDLE1BQU0sT0FBZ0IsNkJBQTZCO0lBZWpEO1FBYlEsd0JBQW1CLEdBQW1DLElBQUksZUFBZSxDQUFnQixJQUFJLENBQUMsQ0FBQzs7UUFHL0YsZ0JBQVcsR0FBMkIsSUFBSSxlQUFlLENBQVEsSUFBSSxDQUFDLENBQUM7UUFDeEUsVUFBSyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxFQUFFLENBQUM7UUFVN0MsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVM7Ozs7UUFBQyxZQUFZLENBQUMsRUFBRTtZQUNoRCxJQUFJLFlBQVksRUFBRTs7c0JBQ1YsSUFBSSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDO2dCQUN6QyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ3JCO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDckI7UUFDSCxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBZkQsTUFBTSxDQUFDLEtBQW9CO1FBQ3pCLE9BQU8sSUFBSSxPQUFPOzs7OztRQUFDLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFO1lBQ3JDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM1QixPQUFPLEVBQUUsQ0FBQztRQUNaLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7O0lBWU8sUUFBUSxDQUFDLElBQVc7UUFDMUIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDOUIsQ0FBQzs7Ozs7O0lBQ1MsZ0JBQWdCLENBQUMsSUFBbUI7UUFDNUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN0QyxDQUFDO0NBQ0Y7Ozs7OztJQTlCQyw0REFBdUc7Ozs7O0lBR3ZHLG9EQUErRTs7SUFDL0UsOENBQStDOzs7Ozs7SUFFL0MscUVBQW1EOzs7Ozs7O0lBaUJuRCxnRkFBaUUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBVc2VyU2VydmljZSB9IGZyb20gJy4vdXNlci5zZXJ2aWNlJztcbmltcG9ydCB7IElQcm92aWRlckF1dGhlbnRpY2F0aW9uU2VydmljZSB9IGZyb20gJy4vYWJzdHJhY3Rpb24vcHJvdmlkZXItYXV0aGVudGljYXRpb24uc2VydmljZSc7XG5pbXBvcnQgeyBVc2VyIH0gZnJvbSAnLi91c2VyLm1vZGVsJztcbmltcG9ydCB7IEJlaGF2aW9yU3ViamVjdCB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgbWFwLCB0YXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5cblxuXG4vLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6bWF4LWxpbmUtbGVuZ3RoXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgUHJvdmlkZXJBdXRoZW50aWNhdGlvblNlcnZpY2U8VFVzZXJQcm92aWRlciBleHRlbmRzIGFueSA9IGFueSwgVFVzZXIgZXh0ZW5kcyBVc2VyID0gVXNlciwgVExvZ2luUGFyYW0gZXh0ZW5kcyBhbnkgPSBhbnksIFRMb2dvdXRQYXJhbSBleHRlbmRzIGFueSA9IGFueT4gaW1wbGVtZW50cyBJUHJvdmlkZXJBdXRoZW50aWNhdGlvblNlcnZpY2Uge1xuXG4gIHByaXZhdGUgdXNlclByb3ZpZGVyU3ViamVjdDogQmVoYXZpb3JTdWJqZWN0PFRVc2VyUHJvdmlkZXI+ID0gbmV3IEJlaGF2aW9yU3ViamVjdDxUVXNlclByb3ZpZGVyPihudWxsKTtcbiAgLy8gcHJvdGVjdGVkIHVzZXJQcm92aWRlciQgPSB0aGlzLnVzZXJQcm92aWRlclN1YmplY3QuYXNPYnNlcnZhYmxlKCk7XG5cbiAgcHJpdmF0ZSB1c2VyU3ViamVjdDogQmVoYXZpb3JTdWJqZWN0PFRVc2VyPiA9IG5ldyBCZWhhdmlvclN1YmplY3Q8VFVzZXI+KG51bGwpO1xuICBwdWJsaWMgdXNlciQgPSB0aGlzLnVzZXJTdWJqZWN0LmFzT2JzZXJ2YWJsZSgpO1xuXG4gIGFic3RyYWN0IGxvZ2luKHBhcmFtPzogVExvZ2luUGFyYW0pOiBQcm9taXNlPHZvaWQ+O1xuICBsb2dvdXQocGFyYW0/OiBUTG9nb3V0UGFyYW0pOiBQcm9taXNlPHZvaWQ+IHtcbiAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgdGhpcy5sb2FkUHJvdmlkZXJVc2VyKG51bGwpO1xuICAgICAgcmVzb2x2ZSgpO1xuICAgIH0pO1xuICB9XG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMudXNlclByb3ZpZGVyU3ViamVjdC5zdWJzY3JpYmUodXNlclByb3ZpZGVyID0+IHtcbiAgICAgIGlmICh1c2VyUHJvdmlkZXIpIHtcbiAgICAgICAgY29uc3QgdXNlciA9IHRoaXMudHJhbnNmb3JtKHVzZXJQcm92aWRlcik7XG4gICAgICAgIHRoaXMubG9hZFVzZXIodXNlcik7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLmxvYWRVc2VyKG51bGwpO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG4gIHByb3RlY3RlZCBhYnN0cmFjdCB0cmFuc2Zvcm0ocHJvdmlkZXJVc2VyOiBUVXNlclByb3ZpZGVyKTogVFVzZXI7XG4gIHByaXZhdGUgbG9hZFVzZXIodXNlcjogVFVzZXIpIHtcbiAgICB0aGlzLnVzZXJTdWJqZWN0Lm5leHQodXNlcik7XG4gIH1cbiAgcHJvdGVjdGVkIGxvYWRQcm92aWRlclVzZXIodXNlcjogVFVzZXJQcm92aWRlcikge1xuICAgIHRoaXMudXNlclByb3ZpZGVyU3ViamVjdC5uZXh0KHVzZXIpO1xuICB9XG59XG5cblxuLy8gLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOm1heC1saW5lLWxlbmd0aFxuLy8gZXhwb3J0IGFic3RyYWN0IGNsYXNzIEJhc2VQcm92aWRlckF1dGhlbnRpY2F0aW9uU2VydmljZTxUTG9naW5QYXJhbXMgZXh0ZW5kcyBhbnksIFRMb2dvdXRQYXJhbXMgZXh0ZW5kcyBhbnk+IGltcGxlbWVudHMgSUF1dGhlbnRpY2F0aW9uU2VydmljZTxUTG9naW5QYXJhbXMsIFRMb2dvdXRQYXJhbXM+IHtcbi8vICAgcHVibGljIGFic3RyYWN0IGxvZ2luKHBhcmFtOiBUTG9naW5QYXJhbXMpOiBQcm9taXNlPFVzZXI+O1xuLy8gICBjb25zdHJ1Y3Rvcihwcm90ZWN0ZWQgdXNlclNlcnZpY2U6IFVzZXJTZXJ2aWNlKSB7fVxuLy8gICBwdWJsaWMgbG9nb3V0KHBhcmFtOiBUTG9nb3V0UGFyYW1zKTogUHJvbWlzZTx2b2lkPiB7XG4vLyAgICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbi8vICAgICAgIHRyeSB7XG4vLyAgICAgICAgIHRoaXMudXNlclNlcnZpY2UudW5sb2FkKCk7XG4vLyAgICAgICAgIHJlc29sdmUoKTtcbi8vICAgICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4vLyAgICAgICAgIHJlamVjdChlcnJvcik7XG4vLyAgICAgICB9XG4vLyAgICAgfSk7XG4vLyAgIH1cbi8vIH1cbiJdfQ==