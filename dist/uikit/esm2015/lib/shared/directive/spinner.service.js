/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { MatSpinner } from "@angular/material/progress-spinner";
export class SpinnerService {
    /**
     * @param {?} el
     * @param {?} renderer
     * @param {?} viewContainerRef
     * @param {?} componentFactoryResolver
     */
    constructor(el, renderer, viewContainerRef, componentFactoryResolver) {
        this.el = el;
        this.renderer = renderer;
        this.viewContainerRef = viewContainerRef;
        this.componentFactoryResolver = componentFactoryResolver;
        this.Init();
    }
    /**
     * @return {?}
     */
    Init() {
        /** @type {?} */
        const factory = this.componentFactoryResolver.resolveComponentFactory(MatSpinner);
        /** @type {?} */
        const componentRef = this.viewContainerRef.createComponent(factory);
        this.divCenter = this.renderer.createElement('div');
        this.spinner = componentRef.instance;
        this.spinner.strokeWidth = 3;
        this.spinner.diameter = 24;
        this.renderer.addClass(this.divCenter, 'uikit-container-spinner');
        this.renderer.addClass(this.spinner._elementRef.nativeElement, 'uikit-spinner');
        this.renderer.setStyle(this.spinner._elementRef.nativeElement, 'display', 'none');
        /** @type {?} */
        const spanButton = (/** @type {?} */ (this.el.nativeElement.querySelector('.mat-button-wrapper')));
        if (spanButton) {
            this.renderer.setStyle(spanButton, 'display', 'flex');
            this.renderer.setStyle(spanButton, 'align-items', 'center');
            this.renderer.setStyle(spanButton, 'justify-content', 'center');
        }
    }
    /**
     * @return {?}
     */
    hide() {
        this.renderer.setStyle(this.spinner._elementRef.nativeElement, 'display', 'none');
        this.renderer.removeChild(this.el.nativeElement.firstChild, this.spinner._elementRef.nativeElement);
        this.renderer.removeChild(this.el.nativeElement.firstChild, this.divCenter);
        this.el.nativeElement.disabled = false;
    }
    /**
     * @return {?}
     */
    show() {
        this.renderer.appendChild(this.el.nativeElement.firstChild, this.divCenter);
        this.renderer.appendChild(this.el.nativeElement.firstChild, this.spinner._elementRef.nativeElement);
        this.renderer.setStyle(this.spinner._elementRef.nativeElement, 'display', 'inherit');
        this.el.nativeElement.disabled = true;
    }
}
if (false) {
    /**
     * @type {?}
     * @protected
     */
    SpinnerService.prototype.spinner;
    /**
     * @type {?}
     * @protected
     */
    SpinnerService.prototype.divCenter;
    /**
     * @type {?}
     * @protected
     */
    SpinnerService.prototype.el;
    /**
     * @type {?}
     * @protected
     */
    SpinnerService.prototype.renderer;
    /**
     * @type {?}
     * @protected
     */
    SpinnerService.prototype.viewContainerRef;
    /**
     * @type {?}
     * @protected
     */
    SpinnerService.prototype.componentFactoryResolver;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3Bpbm5lci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvZGlyZWN0aXZlL3NwaW5uZXIuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQ0EsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLG9DQUFvQyxDQUFDO0FBRTlELE1BQU0sT0FBTyxjQUFjOzs7Ozs7O0lBSXpCLFlBQ1ksRUFBYyxFQUNkLFFBQW1CLEVBQ25CLGdCQUFrQyxFQUNsQyx3QkFBa0Q7UUFIbEQsT0FBRSxHQUFGLEVBQUUsQ0FBWTtRQUNkLGFBQVEsR0FBUixRQUFRLENBQVc7UUFDbkIscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUNsQyw2QkFBd0IsR0FBeEIsd0JBQXdCLENBQTBCO1FBRTVELElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUNkLENBQUM7Ozs7SUFFTSxJQUFJOztjQUNILE9BQU8sR0FBRyxJQUFJLENBQUMsd0JBQXdCLENBQUMsdUJBQXVCLENBQUMsVUFBVSxDQUFDOztjQUMzRSxZQUFZLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUM7UUFDbkUsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUVwRCxJQUFJLENBQUMsT0FBTyxHQUFHLFlBQVksQ0FBQyxRQUFRLENBQUM7UUFDckMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFDO1FBQzdCLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztRQUMzQixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLHlCQUF5QixDQUFDLENBQUM7UUFDbEUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLGVBQWUsQ0FBQyxDQUFDO1FBQ2hGLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxTQUFTLEVBQUUsTUFBTSxDQUFDLENBQUM7O2NBRTVFLFVBQVUsR0FBRyxtQkFBQSxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMscUJBQXFCLENBQUMsRUFBbUI7UUFDaEcsSUFBSSxVQUFVLEVBQUU7WUFDZCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsU0FBUyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBQ3RELElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSxhQUFhLEVBQUUsUUFBUSxDQUFDLENBQUM7WUFDNUQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLGlCQUFpQixFQUFFLFFBQVEsQ0FBQyxDQUFDO1NBQ2pFO0lBQ0gsQ0FBQzs7OztJQUVNLElBQUk7UUFDVCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsU0FBUyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQ2xGLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNwRyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQzVFLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7SUFDekMsQ0FBQzs7OztJQUVNLElBQUk7UUFDVCxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQzVFLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNwRyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsU0FBUyxFQUFFLFNBQVMsQ0FBQyxDQUFDO1FBQ3JGLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7SUFDeEMsQ0FBQztDQUNGOzs7Ozs7SUE3Q0MsaUNBQThCOzs7OztJQUM5QixtQ0FBeUI7Ozs7O0lBR3ZCLDRCQUF3Qjs7Ozs7SUFDeEIsa0NBQTZCOzs7OztJQUM3QiwwQ0FBNEM7Ozs7O0lBQzVDLGtEQUE0RCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50RmFjdG9yeVJlc29sdmVyLCBFbGVtZW50UmVmLCBSZW5kZXJlcjIsIFZpZXdDb250YWluZXJSZWZ9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtNYXRTcGlubmVyfSBmcm9tIFwiQGFuZ3VsYXIvbWF0ZXJpYWwvcHJvZ3Jlc3Mtc3Bpbm5lclwiO1xuXG5leHBvcnQgY2xhc3MgU3Bpbm5lclNlcnZpY2Uge1xuICBwcm90ZWN0ZWQgc3Bpbm5lcjogTWF0U3Bpbm5lcjtcbiAgcHJvdGVjdGVkIGRpdkNlbnRlcjogYW55O1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByb3RlY3RlZCBlbDogRWxlbWVudFJlZixcbiAgICBwcm90ZWN0ZWQgcmVuZGVyZXI6IFJlbmRlcmVyMixcbiAgICBwcm90ZWN0ZWQgdmlld0NvbnRhaW5lclJlZjogVmlld0NvbnRhaW5lclJlZixcbiAgICBwcm90ZWN0ZWQgY29tcG9uZW50RmFjdG9yeVJlc29sdmVyOiBDb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIsXG4gICkge1xuICAgIHRoaXMuSW5pdCgpO1xuICB9XG5cbiAgcHVibGljIEluaXQoKSB7XG4gICAgY29uc3QgZmFjdG9yeSA9IHRoaXMuY29tcG9uZW50RmFjdG9yeVJlc29sdmVyLnJlc29sdmVDb21wb25lbnRGYWN0b3J5KE1hdFNwaW5uZXIpO1xuICAgIGNvbnN0IGNvbXBvbmVudFJlZiA9IHRoaXMudmlld0NvbnRhaW5lclJlZi5jcmVhdGVDb21wb25lbnQoZmFjdG9yeSk7XG4gICAgdGhpcy5kaXZDZW50ZXIgPSB0aGlzLnJlbmRlcmVyLmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xuXG4gICAgdGhpcy5zcGlubmVyID0gY29tcG9uZW50UmVmLmluc3RhbmNlO1xuICAgIHRoaXMuc3Bpbm5lci5zdHJva2VXaWR0aCA9IDM7XG4gICAgdGhpcy5zcGlubmVyLmRpYW1ldGVyID0gMjQ7XG4gICAgdGhpcy5yZW5kZXJlci5hZGRDbGFzcyh0aGlzLmRpdkNlbnRlciwgJ3Vpa2l0LWNvbnRhaW5lci1zcGlubmVyJyk7XG4gICAgdGhpcy5yZW5kZXJlci5hZGRDbGFzcyh0aGlzLnNwaW5uZXIuX2VsZW1lbnRSZWYubmF0aXZlRWxlbWVudCwgJ3Vpa2l0LXNwaW5uZXInKTtcbiAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKHRoaXMuc3Bpbm5lci5fZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LCAnZGlzcGxheScsICdub25lJyk7XG5cbiAgICBjb25zdCBzcGFuQnV0dG9uID0gdGhpcy5lbC5uYXRpdmVFbGVtZW50LnF1ZXJ5U2VsZWN0b3IoJy5tYXQtYnV0dG9uLXdyYXBwZXInKSBhcyBIVE1MU3BhbkVsZW1lbnQ7XG4gICAgaWYgKHNwYW5CdXR0b24pIHtcbiAgICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUoc3BhbkJ1dHRvbiwgJ2Rpc3BsYXknLCAnZmxleCcpO1xuICAgICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZShzcGFuQnV0dG9uLCAnYWxpZ24taXRlbXMnLCAnY2VudGVyJyk7XG4gICAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKHNwYW5CdXR0b24sICdqdXN0aWZ5LWNvbnRlbnQnLCAnY2VudGVyJyk7XG4gICAgfVxuICB9XG5cbiAgcHVibGljIGhpZGUoKTogdm9pZCB7XG4gICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0aGlzLnNwaW5uZXIuX2VsZW1lbnRSZWYubmF0aXZlRWxlbWVudCwgJ2Rpc3BsYXknLCAnbm9uZScpO1xuICAgIHRoaXMucmVuZGVyZXIucmVtb3ZlQ2hpbGQodGhpcy5lbC5uYXRpdmVFbGVtZW50LmZpcnN0Q2hpbGQsIHRoaXMuc3Bpbm5lci5fZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50KTtcbiAgICB0aGlzLnJlbmRlcmVyLnJlbW92ZUNoaWxkKHRoaXMuZWwubmF0aXZlRWxlbWVudC5maXJzdENoaWxkLCB0aGlzLmRpdkNlbnRlcik7XG4gICAgdGhpcy5lbC5uYXRpdmVFbGVtZW50LmRpc2FibGVkID0gZmFsc2U7XG4gIH1cblxuICBwdWJsaWMgc2hvdygpOiB2b2lkIHtcbiAgICB0aGlzLnJlbmRlcmVyLmFwcGVuZENoaWxkKHRoaXMuZWwubmF0aXZlRWxlbWVudC5maXJzdENoaWxkLCB0aGlzLmRpdkNlbnRlcik7XG4gICAgdGhpcy5yZW5kZXJlci5hcHBlbmRDaGlsZCh0aGlzLmVsLm5hdGl2ZUVsZW1lbnQuZmlyc3RDaGlsZCwgdGhpcy5zcGlubmVyLl9lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQpO1xuICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUodGhpcy5zcGlubmVyLl9lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQsICdkaXNwbGF5JywgJ2luaGVyaXQnKTtcbiAgICB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQuZGlzYWJsZWQgPSB0cnVlO1xuICB9XG59XG4iXX0=