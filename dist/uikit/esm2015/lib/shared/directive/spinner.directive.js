/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, Input, ElementRef, Renderer2, ViewContainerRef, ComponentFactoryResolver, } from '@angular/core';
import { SpinnerService } from "./spinner.service";
export class SpinnerDirective {
    /**
     * @param {?} el
     * @param {?} renderer
     * @param {?} viewContainerRef
     * @param {?} componentFactoryResolver
     */
    constructor(el, renderer, viewContainerRef, componentFactoryResolver) {
        this.el = el;
        this.renderer = renderer;
        this.viewContainerRef = viewContainerRef;
        this.componentFactoryResolver = componentFactoryResolver;
        this.spinnerService = new SpinnerService(el, renderer, viewContainerRef, componentFactoryResolver);
    }
    /**
     * @param {?} condition
     * @return {?}
     */
    set isLoading(condition) {
        if (condition) {
            this.spinnerService.show();
        }
        else {
            this.spinnerService.hide();
        }
    }
}
SpinnerDirective.decorators = [
    { type: Directive, args: [{
                selector: '[uikitSpinner]'
            },] }
];
/** @nocollapse */
SpinnerDirective.ctorParameters = () => [
    { type: ElementRef },
    { type: Renderer2 },
    { type: ViewContainerRef },
    { type: ComponentFactoryResolver }
];
SpinnerDirective.propDecorators = {
    isLoading: [{ type: Input, args: ['uikitSpinner',] }]
};
if (false) {
    /**
     * @type {?}
     * @protected
     */
    SpinnerDirective.prototype.spinnerService;
    /**
     * @type {?}
     * @protected
     */
    SpinnerDirective.prototype.el;
    /**
     * @type {?}
     * @protected
     */
    SpinnerDirective.prototype.renderer;
    /**
     * @type {?}
     * @protected
     */
    SpinnerDirective.prototype.viewContainerRef;
    /**
     * @type {?}
     * @protected
     */
    SpinnerDirective.prototype.componentFactoryResolver;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3Bpbm5lci5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9kaXJlY3RpdmUvc3Bpbm5lci5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFDTCxTQUFTLEVBQ1QsS0FBSyxFQUNMLFVBQVUsRUFDVixTQUFTLEVBQ1QsZ0JBQWdCLEVBQ2hCLHdCQUF3QixHQUN6QixNQUFNLGVBQWUsQ0FBQztBQUN2QixPQUFPLEVBQUMsY0FBYyxFQUFDLE1BQU0sbUJBQW1CLENBQUM7QUFLakQsTUFBTSxPQUFPLGdCQUFnQjs7Ozs7OztJQVczQixZQUNZLEVBQWMsRUFDZCxRQUFtQixFQUNuQixnQkFBa0MsRUFDbEMsd0JBQWtEO1FBSGxELE9BQUUsR0FBRixFQUFFLENBQVk7UUFDZCxhQUFRLEdBQVIsUUFBUSxDQUFXO1FBQ25CLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFDbEMsNkJBQXdCLEdBQXhCLHdCQUF3QixDQUEwQjtRQUU1RCxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksY0FBYyxDQUFDLEVBQUUsRUFBRSxRQUFRLEVBQUUsZ0JBQWdCLEVBQUUsd0JBQXdCLENBQUMsQ0FBQztJQUNyRyxDQUFDOzs7OztJQWpCRCxJQUEyQixTQUFTLENBQUMsU0FBUztRQUM1QyxJQUFJLFNBQVMsRUFBRTtZQUNiLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFFLENBQUM7U0FDNUI7YUFBTTtZQUNMLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFFLENBQUM7U0FDNUI7SUFDSCxDQUFDOzs7WUFWRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLGdCQUFnQjthQUMzQjs7OztZQVRDLFVBQVU7WUFDVixTQUFTO1lBQ1QsZ0JBQWdCO1lBQ2hCLHdCQUF3Qjs7O3dCQVF2QixLQUFLLFNBQUMsY0FBYzs7Ozs7OztJQVFyQiwwQ0FBeUM7Ozs7O0lBR3ZDLDhCQUF3Qjs7Ozs7SUFDeEIsb0NBQTZCOzs7OztJQUM3Qiw0Q0FBNEM7Ozs7O0lBQzVDLG9EQUE0RCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XG4gIERpcmVjdGl2ZSxcbiAgSW5wdXQsXG4gIEVsZW1lbnRSZWYsXG4gIFJlbmRlcmVyMixcbiAgVmlld0NvbnRhaW5lclJlZixcbiAgQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyLFxufSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7U3Bpbm5lclNlcnZpY2V9IGZyb20gXCIuL3NwaW5uZXIuc2VydmljZVwiO1xuXG5ARGlyZWN0aXZlKHtcbiAgc2VsZWN0b3I6ICdbdWlraXRTcGlubmVyXSdcbn0pXG5leHBvcnQgY2xhc3MgU3Bpbm5lckRpcmVjdGl2ZSB7XG4gIEBJbnB1dCgndWlraXRTcGlubmVyJykgc2V0IGlzTG9hZGluZyhjb25kaXRpb24pIHtcbiAgICBpZiAoY29uZGl0aW9uKSB7XG4gICAgICB0aGlzLnNwaW5uZXJTZXJ2aWNlLnNob3coKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5zcGlubmVyU2VydmljZS5oaWRlKCk7XG4gICAgfVxuICB9XG5cbiAgcHJvdGVjdGVkIHNwaW5uZXJTZXJ2aWNlOiBTcGlubmVyU2VydmljZTtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcm90ZWN0ZWQgZWw6IEVsZW1lbnRSZWYsXG4gICAgcHJvdGVjdGVkIHJlbmRlcmVyOiBSZW5kZXJlcjIsXG4gICAgcHJvdGVjdGVkIHZpZXdDb250YWluZXJSZWY6IFZpZXdDb250YWluZXJSZWYsXG4gICAgcHJvdGVjdGVkIGNvbXBvbmVudEZhY3RvcnlSZXNvbHZlcjogQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyLFxuICApIHtcbiAgICB0aGlzLnNwaW5uZXJTZXJ2aWNlID0gbmV3IFNwaW5uZXJTZXJ2aWNlKGVsLCByZW5kZXJlciwgdmlld0NvbnRhaW5lclJlZiwgY29tcG9uZW50RmFjdG9yeVJlc29sdmVyKTtcbiAgfVxufVxuIl19