/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
export class HighlightPipe {
    /**
     * @param {?} value
     * @param {?} term
     * @return {?}
     */
    transform(value, term) {
        if (!value) {
            return '';
        }
        /** @type {?} */
        const regex = this.createRegex(term);
        if (!regex) {
            return value;
        }
        // tslint:disable-next-line:only-arrow-functions
        value = value.replace(regex, (/**
         * @param {?} matched
         * @param {?} group1
         * @return {?}
         */
        function (matched, group1) {
            return '<strong>' + group1 + '</strong>';
        }));
        return value;
    }
    /**
     * @param {?} input
     * @return {?}
     */
    createRegex(input) {
        input = input || '';
        input = this.createReplacements(input);
        input = input.replace(/^[^A-z\u00C0-\u00ff]+|[^A-z\u00C0-\u00ff]+$/g, '');
        input = input.replace(/^\||\|$/g, '');
        if (input) {
            /** @type {?} */
            const re = '(' + input + ')';
            return new RegExp(re, 'i');
        }
        return null;
    }
    /**
     * @param {?} str
     * @return {?}
     */
    createReplacements(str) {
        /** @type {?} */
        const replacements = [
            '[aàáâãäå]',
            '(æ|oe)',
            '[cç]',
            '[eèéêë]',
            '[iìíîï]',
            '[nñ]',
            '[oòóôõö]',
            '[uùúûü]',
            '[yýÿ]'
        ];
        return replacements.reduce((/**
         * @param {?} item
         * @param {?} regexStr
         * @return {?}
         */
        (item, regexStr) => {
            return item.replace(new RegExp(regexStr, 'gi'), regexStr);
        }), str);
    }
}
HighlightPipe.decorators = [
    { type: Pipe, args: [{ name: 'highlight' },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGlnaGxpZ2h0LnBpcGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9waXBlcy9oaWdobGlnaHQucGlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLElBQUksRUFBZ0IsTUFBTSxlQUFlLENBQUM7QUFHbEQsTUFBTSxPQUFPLGFBQWE7Ozs7OztJQUV4QixTQUFTLENBQUMsS0FBYSxFQUFFLElBQVk7UUFFbkMsSUFBSSxDQUFDLEtBQUssRUFBRTtZQUNWLE9BQU8sRUFBRSxDQUFDO1NBQ1g7O2NBRUssS0FBSyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDO1FBQ3BDLElBQUksQ0FBQyxLQUFLLEVBQUU7WUFDVixPQUFPLEtBQUssQ0FBQztTQUNkO1FBRUQsZ0RBQWdEO1FBQ2hELEtBQUssR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUs7Ozs7O1FBQUUsVUFBVSxPQUFPLEVBQUUsTUFBTTtZQUNwRCxPQUFPLFVBQVUsR0FBRyxNQUFNLEdBQUcsV0FBVyxDQUFDO1FBQzNDLENBQUMsRUFBQyxDQUFDO1FBRUgsT0FBTyxLQUFLLENBQUM7SUFDZixDQUFDOzs7OztJQUVELFdBQVcsQ0FBQyxLQUFLO1FBQ2YsS0FBSyxHQUFHLEtBQUssSUFBSSxFQUFFLENBQUM7UUFDcEIsS0FBSyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN2QyxLQUFLLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyw4Q0FBOEMsRUFBRSxFQUFFLENBQUMsQ0FBQztRQUMxRSxLQUFLLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDdEMsSUFBSSxLQUFLLEVBQUU7O2tCQUNILEVBQUUsR0FBRyxHQUFHLEdBQUcsS0FBSyxHQUFHLEdBQUc7WUFDNUIsT0FBTyxJQUFJLE1BQU0sQ0FBQyxFQUFFLEVBQUUsR0FBRyxDQUFDLENBQUM7U0FDNUI7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7Ozs7O0lBR0Qsa0JBQWtCLENBQUMsR0FBRzs7Y0FDZCxZQUFZLEdBQUc7WUFDbkIsV0FBVztZQUNYLFFBQVE7WUFDUixNQUFNO1lBQ04sU0FBUztZQUNULFNBQVM7WUFDVCxNQUFNO1lBQ04sVUFBVTtZQUNWLFNBQVM7WUFDVCxPQUFPO1NBQ1I7UUFFRCxPQUFPLFlBQVksQ0FBQyxNQUFNOzs7OztRQUFDLENBQUMsSUFBSSxFQUFFLFFBQVEsRUFBRSxFQUFFO1lBQzVDLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLE1BQU0sQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFDNUQsQ0FBQyxHQUFFLEdBQUcsQ0FBQyxDQUFDO0lBRVYsQ0FBQzs7O1lBcERGLElBQUksU0FBQyxFQUFDLElBQUksRUFBRSxXQUFXLEVBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1BpcGUsIFBpcGVUcmFuc2Zvcm19IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AUGlwZSh7bmFtZTogJ2hpZ2hsaWdodCd9KVxuZXhwb3J0IGNsYXNzIEhpZ2hsaWdodFBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcblxuICB0cmFuc2Zvcm0odmFsdWU6IHN0cmluZywgdGVybTogc3RyaW5nKTogc3RyaW5nIHtcblxuICAgIGlmICghdmFsdWUpIHtcbiAgICAgIHJldHVybiAnJztcbiAgICB9XG5cbiAgICBjb25zdCByZWdleCA9IHRoaXMuY3JlYXRlUmVnZXgodGVybSk7XG4gICAgaWYgKCFyZWdleCkge1xuICAgICAgcmV0dXJuIHZhbHVlO1xuICAgIH1cblxuICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTpvbmx5LWFycm93LWZ1bmN0aW9uc1xuICAgIHZhbHVlID0gdmFsdWUucmVwbGFjZShyZWdleCwgZnVuY3Rpb24gKG1hdGNoZWQsIGdyb3VwMSkge1xuICAgICAgcmV0dXJuICc8c3Ryb25nPicgKyBncm91cDEgKyAnPC9zdHJvbmc+JztcbiAgICB9KTtcblxuICAgIHJldHVybiB2YWx1ZTtcbiAgfVxuXG4gIGNyZWF0ZVJlZ2V4KGlucHV0KSB7XG4gICAgaW5wdXQgPSBpbnB1dCB8fCAnJztcbiAgICBpbnB1dCA9IHRoaXMuY3JlYXRlUmVwbGFjZW1lbnRzKGlucHV0KTtcbiAgICBpbnB1dCA9IGlucHV0LnJlcGxhY2UoL15bXkEtelxcdTAwQzAtXFx1MDBmZl0rfFteQS16XFx1MDBDMC1cXHUwMGZmXSskL2csICcnKTtcbiAgICBpbnB1dCA9IGlucHV0LnJlcGxhY2UoL15cXHx8XFx8JC9nLCAnJyk7XG4gICAgaWYgKGlucHV0KSB7XG4gICAgICBjb25zdCByZSA9ICcoJyArIGlucHV0ICsgJyknO1xuICAgICAgcmV0dXJuIG5ldyBSZWdFeHAocmUsICdpJyk7XG4gICAgfVxuICAgIHJldHVybiBudWxsO1xuICB9XG5cblxuICBjcmVhdGVSZXBsYWNlbWVudHMoc3RyKSB7XG4gICAgY29uc3QgcmVwbGFjZW1lbnRzID0gW1xuICAgICAgJ1thw6DDocOiw6PDpMOlXScsXG4gICAgICAnKMOmfG9lKScsXG4gICAgICAnW2PDp10nLFxuICAgICAgJ1tlw6jDqcOqw6tdJyxcbiAgICAgICdbacOsw63DrsOvXScsXG4gICAgICAnW27DsV0nLFxuICAgICAgJ1tvw7LDs8O0w7XDtl0nLFxuICAgICAgJ1t1w7nDusO7w7xdJyxcbiAgICAgICdbecO9w79dJ1xuICAgIF07XG5cbiAgICByZXR1cm4gcmVwbGFjZW1lbnRzLnJlZHVjZSgoaXRlbSwgcmVnZXhTdHIpID0+IHtcbiAgICAgIHJldHVybiBpdGVtLnJlcGxhY2UobmV3IFJlZ0V4cChyZWdleFN0ciwgJ2dpJyksIHJlZ2V4U3RyKTtcbiAgICB9LCBzdHIpO1xuXG4gIH1cbn1cbiJdfQ==