/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @param {?=} blackList
 * @return {?}
 */
export function AutoUnsubscribe(blackList = []) {
    return (/**
     * @param {?} constructor
     * @return {?}
     */
    function (constructor) {
        /** @type {?} */
        const original = constructor.prototype.ngOnDestroy;
        constructor.prototype.ngOnDestroy = (/**
         * @return {?}
         */
        function () {
            for (let prop in this) {
                /** @type {?} */
                const property = this[prop];
                if (!blackList.includes(prop)) {
                    if (property && (typeof property.unsubscribe === "function")) {
                        property.unsubscribe();
                    }
                }
            }
            original && typeof original === 'function' && original.apply(this, arguments);
        });
    });
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0by11bnN1YnNjcmliZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2F1dG8tdW5zdWJzY3JpYmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQSxNQUFNLFVBQVUsZUFBZSxDQUFDLFNBQVMsR0FBRyxFQUFFO0lBRTVDOzs7O0lBQU8sVUFBUyxXQUFXOztjQUNuQixRQUFRLEdBQUcsV0FBVyxDQUFDLFNBQVMsQ0FBQyxXQUFXO1FBRWxELFdBQVcsQ0FBQyxTQUFTLENBQUMsV0FBVzs7O1FBQUc7WUFDbEMsS0FBSyxJQUFJLElBQUksSUFBSSxJQUFJLEVBQUU7O3NCQUNmLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO2dCQUMzQixJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDN0IsSUFBSSxRQUFRLElBQUksQ0FBQyxPQUFPLFFBQVEsQ0FBQyxXQUFXLEtBQUssVUFBVSxDQUFDLEVBQUU7d0JBQzVELFFBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQztxQkFDeEI7aUJBQ0Y7YUFDRjtZQUNELFFBQVEsSUFBSSxPQUFPLFFBQVEsS0FBSyxVQUFVLElBQUksUUFBUSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsU0FBUyxDQUFDLENBQUM7UUFDaEYsQ0FBQyxDQUFBLENBQUM7SUFDSixDQUFDLEVBQUE7QUFFSCxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGZ1bmN0aW9uIEF1dG9VbnN1YnNjcmliZShibGFja0xpc3QgPSBbXSkge1xuXG4gIHJldHVybiBmdW5jdGlvbihjb25zdHJ1Y3Rvcikge1xuICAgIGNvbnN0IG9yaWdpbmFsID0gY29uc3RydWN0b3IucHJvdG90eXBlLm5nT25EZXN0cm95O1xuXG4gICAgY29uc3RydWN0b3IucHJvdG90eXBlLm5nT25EZXN0cm95ID0gZnVuY3Rpb24oKSB7XG4gICAgICBmb3IgKGxldCBwcm9wIGluIHRoaXMpIHtcbiAgICAgICAgY29uc3QgcHJvcGVydHkgPSB0aGlzW3Byb3BdO1xuICAgICAgICBpZiAoIWJsYWNrTGlzdC5pbmNsdWRlcyhwcm9wKSkge1xuICAgICAgICAgIGlmIChwcm9wZXJ0eSAmJiAodHlwZW9mIHByb3BlcnR5LnVuc3Vic2NyaWJlID09PSBcImZ1bmN0aW9uXCIpKSB7XG4gICAgICAgICAgICBwcm9wZXJ0eS51bnN1YnNjcmliZSgpO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgb3JpZ2luYWwgJiYgdHlwZW9mIG9yaWdpbmFsID09PSAnZnVuY3Rpb24nICYmIG9yaWdpbmFsLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XG4gICAgfTtcbiAgfVxuXG59XG4iXX0=