/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule, Optional, SkipSelf } from '@angular/core';
import { LayoutModule } from './layout/layout.module';
import { MatPaginatorIntl } from '@angular/material';
import { MatPaginatorIntlPtBr } from './utils/mat-paginator-ptbr';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { OfflineInterceptor } from './offline/offline-interceptor';
export class UikitModule {
    /**
     * @param {?} parentModule
     */
    constructor(parentModule) {
        if (parentModule) {
            throw new Error('UikitModule já foi carregado! Importe ele somente no AppModule.');
        }
    }
}
UikitModule.decorators = [
    { type: NgModule, args: [{
                entryComponents: [],
                declarations: [],
                imports: [
                // OperationsModule,
                ],
                exports: [
                    // OperationsModule,
                    LayoutModule,
                ],
                providers: [
                    { provide: HTTP_INTERCEPTORS, useClass: OfflineInterceptor, multi: true },
                    { provide: MatPaginatorIntl, useClass: MatPaginatorIntlPtBr }
                ],
            },] }
];
/** @nocollapse */
UikitModule.ctorParameters = () => [
    { type: UikitModule, decorators: [{ type: Optional }, { type: SkipSelf }] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWlraXQubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi91aWtpdC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM3RCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDdEQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDckQsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFFbEUsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDekQsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sK0JBQStCLENBQUM7QUFrQm5FLE1BQU0sT0FBTyxXQUFXOzs7O0lBQ3RCLFlBQW9DLFlBQXlCO1FBQzNELElBQUksWUFBWSxFQUFFO1lBQ2hCLE1BQU0sSUFBSSxLQUFLLENBQUMsaUVBQWlFLENBQUMsQ0FBQztTQUNwRjtJQUNILENBQUM7OztZQXJCRixRQUFRLFNBQUM7Z0JBQ1IsZUFBZSxFQUFFLEVBQUU7Z0JBQ25CLFlBQVksRUFBRSxFQUFFO2dCQUNoQixPQUFPLEVBQUU7Z0JBQ1Asb0JBQW9CO2lCQUNyQjtnQkFDRCxPQUFPLEVBQUU7b0JBQ1Asb0JBQW9CO29CQUNwQixZQUFZO2lCQUNiO2dCQUNELFNBQVMsRUFBRTtvQkFDVCxFQUFFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxRQUFRLEVBQUUsa0JBQWtCLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRTtvQkFDekUsRUFBRSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsUUFBUSxFQUFFLG9CQUFvQixFQUFFO2lCQUM5RDthQUVGOzs7O1lBRW1ELFdBQVcsdUJBQWhELFFBQVEsWUFBSSxRQUFRIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUsIE9wdGlvbmFsLCBTa2lwU2VsZiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTGF5b3V0TW9kdWxlIH0gZnJvbSAnLi9sYXlvdXQvbGF5b3V0Lm1vZHVsZSc7XG5pbXBvcnQgeyBNYXRQYWdpbmF0b3JJbnRsIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xuaW1wb3J0IHsgTWF0UGFnaW5hdG9ySW50bFB0QnIgfSBmcm9tICcuL3V0aWxzL21hdC1wYWdpbmF0b3ItcHRicic7XG5pbXBvcnQgeyBPcGVyYXRpb25zTW9kdWxlIH0gZnJvbSAnLi9vcGVyYXRpb25zL29wZXJhdGlvbnMubW9kdWxlJztcbmltcG9ydCB7IEhUVFBfSU5URVJDRVBUT1JTIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgT2ZmbGluZUludGVyY2VwdG9yIH0gZnJvbSAnLi9vZmZsaW5lL29mZmxpbmUtaW50ZXJjZXB0b3InO1xuXG5ATmdNb2R1bGUoe1xuICBlbnRyeUNvbXBvbmVudHM6IFtdLFxuICBkZWNsYXJhdGlvbnM6IFtdLFxuICBpbXBvcnRzOiBbXG4gICAgLy8gT3BlcmF0aW9uc01vZHVsZSxcbiAgXSxcbiAgZXhwb3J0czogW1xuICAgIC8vIE9wZXJhdGlvbnNNb2R1bGUsXG4gICAgTGF5b3V0TW9kdWxlLFxuICBdLFxuICBwcm92aWRlcnM6IFtcbiAgICB7IHByb3ZpZGU6IEhUVFBfSU5URVJDRVBUT1JTLCB1c2VDbGFzczogT2ZmbGluZUludGVyY2VwdG9yLCBtdWx0aTogdHJ1ZSB9LFxuICAgIHsgcHJvdmlkZTogTWF0UGFnaW5hdG9ySW50bCwgdXNlQ2xhc3M6IE1hdFBhZ2luYXRvckludGxQdEJyIH1cbiAgXSxcblxufSlcbmV4cG9ydCBjbGFzcyBVaWtpdE1vZHVsZSB7XG4gIGNvbnN0cnVjdG9yKEBPcHRpb25hbCgpIEBTa2lwU2VsZigpIHBhcmVudE1vZHVsZTogVWlraXRNb2R1bGUpIHtcbiAgICBpZiAocGFyZW50TW9kdWxlKSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ1Vpa2l0TW9kdWxlIGrDoSBmb2kgY2FycmVnYWRvISBJbXBvcnRlIGVsZSBzb21lbnRlIG5vIEFwcE1vZHVsZS4nKTtcbiAgICB9XG4gIH1cbn1cbiJdfQ==