/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SaveButtonComponent } from './save-button/save-button.component';
import { MatBadgeModule, MatButtonModule, MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule, MatMenuModule } from '@angular/material';
import { FilterComponent } from './filter/filter.component';
export class UikitComponentsModule {
}
UikitComponentsModule.decorators = [
    { type: NgModule, args: [{
                providers: [],
                declarations: [SaveButtonComponent, FilterComponent],
                exports: [
                    SaveButtonComponent,
                    FilterComponent,
                ],
                imports: [
                    CommonModule,
                    MatButtonModule,
                    MatIconModule,
                    MatMenuModule,
                    MatFormFieldModule,
                    MatInputModule,
                    MatBadgeModule,
                    MatDialogModule
                ],
                entryComponents: [SaveButtonComponent, FilterComponent]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hhcmVkLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9zaGFyZWQubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsUUFBUSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3ZDLE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUM3QyxPQUFPLEVBQUMsbUJBQW1CLEVBQUMsTUFBTSxxQ0FBcUMsQ0FBQztBQUN4RSxPQUFPLEVBQ0wsY0FBYyxFQUNkLGVBQWUsRUFDZixlQUFlLEVBQ2Ysa0JBQWtCLEVBQ2xCLGFBQWEsRUFDYixjQUFjLEVBQ2QsYUFBYSxFQUNkLE1BQU0sbUJBQW1CLENBQUM7QUFDM0IsT0FBTyxFQUFDLGVBQWUsRUFBQyxNQUFNLDJCQUEyQixDQUFDO0FBcUIxRCxNQUFNLE9BQU8scUJBQXFCOzs7WUFuQmpDLFFBQVEsU0FBQztnQkFDUixTQUFTLEVBQUUsRUFBRTtnQkFDYixZQUFZLEVBQUUsQ0FBQyxtQkFBbUIsRUFBRSxlQUFlLENBQUM7Z0JBQ3BELE9BQU8sRUFBRTtvQkFDUCxtQkFBbUI7b0JBQ25CLGVBQWU7aUJBQ2hCO2dCQUNELE9BQU8sRUFBRTtvQkFDUCxZQUFZO29CQUNaLGVBQWU7b0JBQ2YsYUFBYTtvQkFDYixhQUFhO29CQUNiLGtCQUFrQjtvQkFDbEIsY0FBYztvQkFDZCxjQUFjO29CQUNkLGVBQWU7aUJBQ2hCO2dCQUNELGVBQWUsRUFBRSxDQUFDLG1CQUFtQixFQUFFLGVBQWUsQ0FBQzthQUN4RCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7TmdNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtDb21tb25Nb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQge1NhdmVCdXR0b25Db21wb25lbnR9IGZyb20gJy4vc2F2ZS1idXR0b24vc2F2ZS1idXR0b24uY29tcG9uZW50JztcbmltcG9ydCB7XG4gIE1hdEJhZGdlTW9kdWxlLFxuICBNYXRCdXR0b25Nb2R1bGUsXG4gIE1hdERpYWxvZ01vZHVsZSxcbiAgTWF0Rm9ybUZpZWxkTW9kdWxlLFxuICBNYXRJY29uTW9kdWxlLFxuICBNYXRJbnB1dE1vZHVsZSxcbiAgTWF0TWVudU1vZHVsZVxufSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XG5pbXBvcnQge0ZpbHRlckNvbXBvbmVudH0gZnJvbSAnLi9maWx0ZXIvZmlsdGVyLmNvbXBvbmVudCc7XG5cbkBOZ01vZHVsZSh7XG4gIHByb3ZpZGVyczogW10sXG4gIGRlY2xhcmF0aW9uczogW1NhdmVCdXR0b25Db21wb25lbnQsIEZpbHRlckNvbXBvbmVudF0sXG4gIGV4cG9ydHM6IFtcbiAgICBTYXZlQnV0dG9uQ29tcG9uZW50LFxuICAgIEZpbHRlckNvbXBvbmVudCxcbiAgXSxcbiAgaW1wb3J0czogW1xuICAgIENvbW1vbk1vZHVsZSxcbiAgICBNYXRCdXR0b25Nb2R1bGUsXG4gICAgTWF0SWNvbk1vZHVsZSxcbiAgICBNYXRNZW51TW9kdWxlLFxuICAgIE1hdEZvcm1GaWVsZE1vZHVsZSxcbiAgICBNYXRJbnB1dE1vZHVsZSxcbiAgICBNYXRCYWRnZU1vZHVsZSxcbiAgICBNYXREaWFsb2dNb2R1bGVcbiAgXSxcbiAgZW50cnlDb21wb25lbnRzOiBbU2F2ZUJ1dHRvbkNvbXBvbmVudCwgRmlsdGVyQ29tcG9uZW50XVxufSlcbmV4cG9ydCBjbGFzcyBVaWtpdENvbXBvbmVudHNNb2R1bGUge1xufVxuIl19