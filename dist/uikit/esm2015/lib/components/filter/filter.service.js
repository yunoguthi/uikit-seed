/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import * as i0 from "@angular/core";
export class FilterService {
    constructor() {
        this.filterBadgeSubject = new BehaviorSubject(0);
        this.filterBadge$ = this.filterBadgeSubject.asObservable();
        this.showFiltersSubject = new BehaviorSubject(false);
        this.showFilters$ = this.showFiltersSubject.asObservable();
    }
    /**
     * @param {?} count
     * @return {?}
     */
    setCountFilter(count) {
        this.filterBadgeSubject.next(count);
    }
    /**
     * @return {?}
     */
    setShowFilters() {
        this.showFiltersSubject.next(!this.showFiltersSubject.getValue());
    }
}
FilterService.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */
FilterService.ctorParameters = () => [];
/** @nocollapse */ FilterService.ngInjectableDef = i0.defineInjectable({ factory: function FilterService_Factory() { return new FilterService(); }, token: FilterService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @protected
     */
    FilterService.prototype.filterBadgeSubject;
    /** @type {?} */
    FilterService.prototype.filterBadge$;
    /**
     * @type {?}
     * @protected
     */
    FilterService.prototype.showFiltersSubject;
    /** @type {?} */
    FilterService.prototype.showFilters$;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsdGVyLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvZmlsdGVyL2ZpbHRlci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsVUFBVSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBQyxlQUFlLEVBQUMsTUFBTSxNQUFNLENBQUM7O0FBR3JDLE1BQU0sT0FBTyxhQUFhO0lBT3hCO1FBTFUsdUJBQWtCLEdBQUcsSUFBSSxlQUFlLENBQVMsQ0FBQyxDQUFDLENBQUM7UUFDdkQsaUJBQVksR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDbkQsdUJBQWtCLEdBQUcsSUFBSSxlQUFlLENBQVUsS0FBSyxDQUFDLENBQUM7UUFDNUQsaUJBQVksR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsWUFBWSxFQUFFLENBQUM7SUFHN0QsQ0FBQzs7Ozs7SUFFRCxjQUFjLENBQUMsS0FBYTtRQUMxQixJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3RDLENBQUM7Ozs7SUFFRCxjQUFjO1FBQ1osSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO0lBQ3BFLENBQUM7OztZQWpCRixVQUFVLFNBQUMsRUFBQyxVQUFVLEVBQUUsTUFBTSxFQUFDOzs7Ozs7Ozs7O0lBRzlCLDJDQUE4RDs7SUFDOUQscUNBQTZEOzs7OztJQUM3RCwyQ0FBbUU7O0lBQ25FLHFDQUE2RCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SW5qZWN0YWJsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0JlaGF2aW9yU3ViamVjdH0gZnJvbSAncnhqcyc7XG5cbkBJbmplY3RhYmxlKHtwcm92aWRlZEluOiAncm9vdCd9KVxuZXhwb3J0IGNsYXNzIEZpbHRlclNlcnZpY2Uge1xuXG4gIHByb3RlY3RlZCBmaWx0ZXJCYWRnZVN1YmplY3QgPSBuZXcgQmVoYXZpb3JTdWJqZWN0PG51bWJlcj4oMCk7XG4gIHB1YmxpYyBmaWx0ZXJCYWRnZSQgPSB0aGlzLmZpbHRlckJhZGdlU3ViamVjdC5hc09ic2VydmFibGUoKTtcbiAgcHJvdGVjdGVkIHNob3dGaWx0ZXJzU3ViamVjdCA9IG5ldyBCZWhhdmlvclN1YmplY3Q8Ym9vbGVhbj4oZmFsc2UpO1xuICBwdWJsaWMgc2hvd0ZpbHRlcnMkID0gdGhpcy5zaG93RmlsdGVyc1N1YmplY3QuYXNPYnNlcnZhYmxlKCk7XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gIH1cblxuICBzZXRDb3VudEZpbHRlcihjb3VudDogbnVtYmVyKSB7XG4gICAgdGhpcy5maWx0ZXJCYWRnZVN1YmplY3QubmV4dChjb3VudCk7XG4gIH1cblxuICBzZXRTaG93RmlsdGVycygpIHtcbiAgICB0aGlzLnNob3dGaWx0ZXJzU3ViamVjdC5uZXh0KCF0aGlzLnNob3dGaWx0ZXJzU3ViamVjdC5nZXRWYWx1ZSgpKTtcbiAgfVxufVxuIl19