/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { ContentChild, ViewChild, EventEmitter, Output, Component, Input, TemplateRef } from '@angular/core';
import { FormGroupDirective } from '@angular/forms';
import { MatDialog, MatInput } from '@angular/material';
import { Hotkey, HotkeysService } from 'angular2-hotkeys';
import { FilterService } from './filter.service';
export class FilterComponent {
    /**
     * @param {?} hotkeysService
     * @param {?} filterService
     * @param {?} dialog
     */
    constructor(hotkeysService, filterService, dialog) {
        this.hotkeysService = hotkeysService;
        this.filterService = filterService;
        this.dialog = dialog;
        this.clickPesquisa = new EventEmitter();
        this.filterBadge$ = this.filterService.filterBadge$;
        this.showFilters$ = this.filterService.showFilters$;
        this.subscriptions = [];
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @return {?}
     */
    ngAfterViewInit() {
        this.hotkeysService.add(new Hotkey('alt+3', (/**
         * @param {?} event
         * @return {?}
         */
        (event) => {
            this.searchInput.focus();
            return false;
        })));
        this.hotkeysService.add(new Hotkey('ctrl+alt+3', (/**
         * @param {?} event
         * @return {?}
         */
        (event) => {
            this.toggleFilters();
            return false;
        })));
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.subscriptions) {
            this.subscriptions.forEach((/**
             * @param {?} subscription
             * @return {?}
             */
            subscription => {
                if (subscription && subscription.closed === false) {
                    subscription.unsubscribe();
                }
            }));
        }
    }
    /**
     * @return {?}
     */
    pesquisar() {
        this.clickPesquisa.emit(this.searchInput.value);
        this.clickPesquisa.subscribe((/**
         * @return {?}
         */
        () => {
            this.searchInput.focus();
        }));
    }
    /**
     * @return {?}
     */
    toggleFilters() {
        this.dialogRef = this.dialog.open(this.template);
    }
    /**
     * @return {?}
     */
    onNoClick() {
        this.dialogRef.close();
    }
}
FilterComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-filter',
                template: "<div class=\"uikit-filter\">\n  <div class=\"uikit-finder\">\n    <mat-form-field>\n      <input #search=\"matInput\" matInput placeholder=\"Pesquisar\" autofocus/>\n      <button type='button' mat-icon-button class=\"btn-search\" (click)=\"pesquisar()\" [attr.disabled]=\"!disabled\">\n        <mat-icon class=\"fa-2x fas fa-search\"></mat-icon>\n      </button>\n    </mat-form-field>\n\n    <button type=\"button\" (click)=\"toggleFilters()\" #moreFilters mat-flat-button color=\"primary\"\n            class=\"btn-more-filters\" matTooltip=\"Mostrar ou esconder mais op\u00E7\u00F5es de filtros\"\n            aria-label=\"Mostrar ou esconder mais op\u00E7\u00F5es de filtros\"\n            [matBadge]=\"(filterBadge$ | async)\" [matBadgeHidden]=\"(filterBadge$ | async) == 0\"\n            matBadgeColor=\"accent\"\n            matBadgeSize=\"medium\">\n      mais filtros\n    </button>\n  </div>\n</div>\n\n<ng-template #filterDialog>\n  <div>\n    <div class=\"row line\">\n      <div class=\"col-10\">\n        <h1 mat-dialog-title>Filtros</h1>\n      </div>\n\n      <div class=\"col-2\">\n        <div mat-dialog-actions class=\"close-position\">\n          <button class=\"close\" mat-button (click)=\"onNoClick()\"><i class=\"fas fa-times\"></i></button>\n        </div>\n      </div>\n     </div>\n    <div mat-dialog-content>\n      <ng-content></ng-content>\n    </div>\n  </div>\n</ng-template>\n",
                styles: [""]
            }] }
];
/** @nocollapse */
FilterComponent.ctorParameters = () => [
    { type: HotkeysService },
    { type: FilterService },
    { type: MatDialog }
];
FilterComponent.propDecorators = {
    clickPesquisa: [{ type: Output }],
    disabled: [{ type: Input }],
    searchInput: [{ type: ViewChild, args: ['search',] }],
    template: [{ type: ViewChild, args: ['filterDialog',] }],
    filtersForm: [{ type: ContentChild, args: [FormGroupDirective,] }]
};
if (false) {
    /** @type {?} */
    FilterComponent.prototype.clickPesquisa;
    /** @type {?} */
    FilterComponent.prototype.disabled;
    /** @type {?} */
    FilterComponent.prototype.searchInput;
    /** @type {?} */
    FilterComponent.prototype.template;
    /** @type {?} */
    FilterComponent.prototype.filtersForm;
    /** @type {?} */
    FilterComponent.prototype.filterBadge$;
    /** @type {?} */
    FilterComponent.prototype.showFilters$;
    /**
     * @type {?}
     * @protected
     */
    FilterComponent.prototype.subscriptions;
    /** @type {?} */
    FilterComponent.prototype.dialogRef;
    /**
     * @type {?}
     * @protected
     */
    FilterComponent.prototype.hotkeysService;
    /**
     * @type {?}
     * @protected
     */
    FilterComponent.prototype.filterService;
    /** @type {?} */
    FilterComponent.prototype.dialog;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsdGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9maWx0ZXIvZmlsdGVyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUlMLFlBQVksRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLFdBQVcsRUFDN0UsTUFBTSxlQUFlLENBQUM7QUFFdkIsT0FBTyxFQUFDLGtCQUFrQixFQUFDLE1BQU0sZ0JBQWdCLENBQUM7QUFDbEQsT0FBTyxFQUFDLFNBQVMsRUFBZ0IsUUFBUSxFQUFDLE1BQU0sbUJBQW1CLENBQUM7QUFDcEUsT0FBTyxFQUFDLE1BQU0sRUFBRSxjQUFjLEVBQUMsTUFBTSxrQkFBa0IsQ0FBQztBQUN4RCxPQUFPLEVBQUMsYUFBYSxFQUFDLE1BQU0sa0JBQWtCLENBQUM7QUFPL0MsTUFBTSxPQUFPLGVBQWU7Ozs7OztJQVkxQixZQUFzQixjQUE4QixFQUM5QixhQUE0QixFQUMvQixNQUFpQjtRQUZkLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUMvQixXQUFNLEdBQU4sTUFBTSxDQUFXO1FBWjFCLGtCQUFhLEdBQXNCLElBQUksWUFBWSxFQUFFLENBQUM7UUFLekQsaUJBQVksR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBQztRQUMvQyxpQkFBWSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDO1FBQzVDLGtCQUFhLEdBQW1CLEVBQUUsQ0FBQztJQU03QyxDQUFDOzs7O0lBRUQsUUFBUTtJQUNSLENBQUM7Ozs7SUFFRCxlQUFlO1FBQ2IsSUFBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsSUFBSSxNQUFNLENBQUMsT0FBTzs7OztRQUFFLENBQUMsS0FBb0IsRUFBVyxFQUFFO1lBQzVFLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDekIsT0FBTyxLQUFLLENBQUM7UUFDZixDQUFDLEVBQUMsQ0FBQyxDQUFDO1FBRUosSUFBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsSUFBSSxNQUFNLENBQUMsWUFBWTs7OztRQUFFLENBQUMsS0FBb0IsRUFBVyxFQUFFO1lBQ2pGLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztZQUNyQixPQUFPLEtBQUssQ0FBQztRQUNmLENBQUMsRUFBQyxDQUFDLENBQUM7SUFDTixDQUFDOzs7O0lBRUQsV0FBVztRQUNULElBQUksSUFBSSxDQUFDLGFBQWEsRUFBRTtZQUN0QixJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU87Ozs7WUFBQyxZQUFZLENBQUMsRUFBRTtnQkFDeEMsSUFBSSxZQUFZLElBQUksWUFBWSxDQUFDLE1BQU0sS0FBSyxLQUFLLEVBQUU7b0JBQ2pELFlBQVksQ0FBQyxXQUFXLEVBQUUsQ0FBQztpQkFDNUI7WUFDSCxDQUFDLEVBQUMsQ0FBQztTQUNKO0lBQ0gsQ0FBQzs7OztJQUVELFNBQVM7UUFDUCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ2hELElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUzs7O1FBQUMsR0FBRyxFQUFFO1lBQ2hDLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDM0IsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDOzs7O0lBRUQsYUFBYTtRQUNYLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ25ELENBQUM7Ozs7SUFFRCxTQUFTO1FBQ1AsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUN6QixDQUFDOzs7WUE1REYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxjQUFjO2dCQUN4QixpNUNBQXNDOzthQUV2Qzs7OztZQVBlLGNBQWM7WUFDdEIsYUFBYTtZQUZiLFNBQVM7Ozs0QkFXZCxNQUFNO3VCQUNOLEtBQUs7MEJBQ0wsU0FBUyxTQUFDLFFBQVE7dUJBQ2xCLFNBQVMsU0FBQyxjQUFjOzBCQUN4QixZQUFZLFNBQUMsa0JBQWtCOzs7O0lBSmhDLHdDQUFnRTs7SUFDaEUsbUNBQTJCOztJQUMzQixzQ0FBMkM7O0lBQzNDLG1DQUFxRDs7SUFDckQsc0NBQWtFOztJQUNsRSx1Q0FBc0Q7O0lBQ3RELHVDQUFzRDs7Ozs7SUFDdEQsd0NBQTZDOztJQUM3QyxvQ0FBNkI7Ozs7O0lBRWpCLHlDQUF3Qzs7Ozs7SUFDeEMsd0NBQXNDOztJQUN0QyxpQ0FBd0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xuICBPbkluaXQsXG4gIE9uRGVzdHJveSxcbiAgQWZ0ZXJWaWV3SW5pdCxcbiAgQ29udGVudENoaWxkLCBWaWV3Q2hpbGQsIEV2ZW50RW1pdHRlciwgT3V0cHV0LCBDb21wb25lbnQsIElucHV0LCBUZW1wbGF0ZVJlZlxufSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7U3Vic2NyaXB0aW9ufSBmcm9tICdyeGpzJztcbmltcG9ydCB7Rm9ybUdyb3VwRGlyZWN0aXZlfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQge01hdERpYWxvZywgTWF0RGlhbG9nUmVmLCBNYXRJbnB1dH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xuaW1wb3J0IHtIb3RrZXksIEhvdGtleXNTZXJ2aWNlfSBmcm9tICdhbmd1bGFyMi1ob3RrZXlzJztcbmltcG9ydCB7RmlsdGVyU2VydmljZX0gZnJvbSAnLi9maWx0ZXIuc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ3Vpa2l0LWZpbHRlcicsXG4gIHRlbXBsYXRlVXJsOiAnLi9maWx0ZXIuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9maWx0ZXIuY29tcG9uZW50LnNjc3MnXSxcbn0pXG5leHBvcnQgY2xhc3MgRmlsdGVyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBBZnRlclZpZXdJbml0LCBPbkRlc3Ryb3kge1xuXG4gIEBPdXRwdXQoKSBjbGlja1Blc3F1aXNhOiBFdmVudEVtaXR0ZXI8YW55PiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgQElucHV0KCkgZGlzYWJsZWQ6IGJvb2xlYW47XG4gIEBWaWV3Q2hpbGQoJ3NlYXJjaCcpIHNlYXJjaElucHV0OiBNYXRJbnB1dDtcbiAgQFZpZXdDaGlsZCgnZmlsdGVyRGlhbG9nJykgdGVtcGxhdGU6IFRlbXBsYXRlUmVmPHt9PjtcbiAgQENvbnRlbnRDaGlsZChGb3JtR3JvdXBEaXJlY3RpdmUpIGZpbHRlcnNGb3JtOiBGb3JtR3JvdXBEaXJlY3RpdmU7XG4gIHB1YmxpYyBmaWx0ZXJCYWRnZSQgPSB0aGlzLmZpbHRlclNlcnZpY2UuZmlsdGVyQmFkZ2UkO1xuICBwdWJsaWMgc2hvd0ZpbHRlcnMkID0gdGhpcy5maWx0ZXJTZXJ2aWNlLnNob3dGaWx0ZXJzJDtcbiAgcHJvdGVjdGVkIHN1YnNjcmlwdGlvbnM6IFN1YnNjcmlwdGlvbltdID0gW107XG4gIGRpYWxvZ1JlZjogTWF0RGlhbG9nUmVmPGFueT47XG5cbiAgY29uc3RydWN0b3IocHJvdGVjdGVkIGhvdGtleXNTZXJ2aWNlOiBIb3RrZXlzU2VydmljZSxcbiAgICAgICAgICAgICAgcHJvdGVjdGVkIGZpbHRlclNlcnZpY2U6IEZpbHRlclNlcnZpY2UsXG4gICAgICAgICAgICAgIHB1YmxpYyBkaWFsb2c6IE1hdERpYWxvZykge1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gIH1cblxuICBuZ0FmdGVyVmlld0luaXQoKTogdm9pZCB7XG4gICAgdGhpcy5ob3RrZXlzU2VydmljZS5hZGQobmV3IEhvdGtleSgnYWx0KzMnLCAoZXZlbnQ6IEtleWJvYXJkRXZlbnQpOiBib29sZWFuID0+IHtcbiAgICAgIHRoaXMuc2VhcmNoSW5wdXQuZm9jdXMoKTtcbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9KSk7XG5cbiAgICB0aGlzLmhvdGtleXNTZXJ2aWNlLmFkZChuZXcgSG90a2V5KCdjdHJsK2FsdCszJywgKGV2ZW50OiBLZXlib2FyZEV2ZW50KTogYm9vbGVhbiA9PiB7XG4gICAgICB0aGlzLnRvZ2dsZUZpbHRlcnMoKTtcbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9KSk7XG4gIH1cblxuICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcbiAgICBpZiAodGhpcy5zdWJzY3JpcHRpb25zKSB7XG4gICAgICB0aGlzLnN1YnNjcmlwdGlvbnMuZm9yRWFjaChzdWJzY3JpcHRpb24gPT4ge1xuICAgICAgICBpZiAoc3Vic2NyaXB0aW9uICYmIHN1YnNjcmlwdGlvbi5jbG9zZWQgPT09IGZhbHNlKSB7XG4gICAgICAgICAgc3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH1cbiAgfVxuXG4gIHBlc3F1aXNhcigpIHtcbiAgICB0aGlzLmNsaWNrUGVzcXVpc2EuZW1pdCh0aGlzLnNlYXJjaElucHV0LnZhbHVlKTtcbiAgICB0aGlzLmNsaWNrUGVzcXVpc2Euc3Vic2NyaWJlKCgpID0+IHtcbiAgICAgIHRoaXMuc2VhcmNoSW5wdXQuZm9jdXMoKTtcbiAgICB9KTtcbiAgfVxuXG4gIHRvZ2dsZUZpbHRlcnMoKSB7XG4gICAgdGhpcy5kaWFsb2dSZWYgPSB0aGlzLmRpYWxvZy5vcGVuKHRoaXMudGVtcGxhdGUpO1xuICB9XG5cbiAgb25Ob0NsaWNrKCk6IHZvaWQge1xuICAgIHRoaXMuZGlhbG9nUmVmLmNsb3NlKCk7XG4gIH1cbn1cbiJdfQ==