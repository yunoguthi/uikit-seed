/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { SimpleDialogComponent } from './simple-dialog.component';
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/dialog";
export class DialogService {
    /**
     * @param {?} dialog
     */
    constructor(dialog) {
        this.dialog = dialog;
    }
    /**
     * @param {?} description
     * @param {?=} items
     * @param {?=} title
     * @param {?=} width
     * @return {?}
     */
    show(description, items, title = 'Confirmação', width = '450px') {
        /** @type {?} */
        const dialogRef = this.dialog.open(SimpleDialogComponent, {
            width: '450px',
            data: {
                title: title,
                description: description,
                items: items
            }
        });
        return dialogRef.afterClosed();
    }
}
DialogService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
DialogService.ctorParameters = () => [
    { type: MatDialog }
];
/** @nocollapse */ DialogService.ngInjectableDef = i0.defineInjectable({ factory: function DialogService_Factory() { return new DialogService(i0.inject(i1.MatDialog)); }, token: DialogService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @protected
     */
    DialogService.prototype.dialog;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGlhbG9nLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvc2ltcGxlLWRpYWxvZy9kaWFsb2cuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDOUMsT0FBTyxFQUFFLHFCQUFxQixFQUErQixNQUFNLDJCQUEyQixDQUFDOzs7QUFNL0YsTUFBTSxPQUFPLGFBQWE7Ozs7SUFFeEIsWUFBc0IsTUFBaUI7UUFBakIsV0FBTSxHQUFOLE1BQU0sQ0FBVztJQUFJLENBQUM7Ozs7Ozs7O0lBRTVDLElBQUksQ0FBQyxXQUFtQixFQUFFLEtBQWEsRUFBRSxRQUFnQixhQUFhLEVBQUUsUUFBZ0IsT0FBTzs7Y0FDdkYsU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFxRCxxQkFBcUIsRUFBRTtZQUM1RyxLQUFLLEVBQUUsT0FBTztZQUNkLElBQUksRUFBRTtnQkFDSixLQUFLLEVBQUUsS0FBSztnQkFDWixXQUFXLEVBQUUsV0FBVztnQkFDeEIsS0FBSyxFQUFFLEtBQUs7YUFDYjtTQUNGLENBQUM7UUFFRixPQUFPLFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUVqQyxDQUFDOzs7WUFuQkYsVUFBVSxTQUFDO2dCQUNWLFVBQVUsRUFBRSxNQUFNO2FBQ25COzs7O1lBTlEsU0FBUzs7Ozs7Ozs7SUFTSiwrQkFBMkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBNYXREaWFsb2cgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XG5pbXBvcnQgeyBTaW1wbGVEaWFsb2dDb21wb25lbnQsIERpYWxvZ1Jlc3VsdCwgRGlhbG9nT3B0aW9ucyB9IGZyb20gJy4vc2ltcGxlLWRpYWxvZy5jb21wb25lbnQnO1xuaW1wb3J0IHsgSUQgfSBmcm9tICdAZGF0b3JhbWEvYWtpdGEnO1xuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBEaWFsb2dTZXJ2aWNlIHtcblxuICBjb25zdHJ1Y3Rvcihwcm90ZWN0ZWQgZGlhbG9nOiBNYXREaWFsb2cpIHsgfVxuXG4gIHNob3coZGVzY3JpcHRpb246IHN0cmluZywgaXRlbXM/OiBhbnlbXSwgdGl0bGU6IHN0cmluZyA9ICdDb25maXJtYcOnw6NvJywgd2lkdGg6IHN0cmluZyA9ICc0NTBweCcpIHtcbiAgICBjb25zdCBkaWFsb2dSZWYgPSB0aGlzLmRpYWxvZy5vcGVuPFNpbXBsZURpYWxvZ0NvbXBvbmVudCwgRGlhbG9nT3B0aW9ucywgRGlhbG9nUmVzdWx0PihTaW1wbGVEaWFsb2dDb21wb25lbnQsIHtcbiAgICAgIHdpZHRoOiAnNDUwcHgnLFxuICAgICAgZGF0YToge1xuICAgICAgICB0aXRsZTogdGl0bGUsXG4gICAgICAgIGRlc2NyaXB0aW9uOiBkZXNjcmlwdGlvbixcbiAgICAgICAgaXRlbXM6IGl0ZW1zXG4gICAgICB9XG4gICAgfSk7XG5cbiAgICByZXR1cm4gZGlhbG9nUmVmLmFmdGVyQ2xvc2VkKCk7XG5cbiAgfVxufVxuIl19