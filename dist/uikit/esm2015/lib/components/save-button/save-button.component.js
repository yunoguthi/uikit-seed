/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, ViewChild, Renderer2, Output, EventEmitter } from '@angular/core';
import { Location } from '@angular/common';
import { MatButton } from '@angular/material';
/** @enum {string} */
const SaveButtonSelection = {
    Salvar: 'Salvar',
    SalvarVoltar: 'Salvar e voltar',
    SalvarNovo: 'Salvar e novo',
};
export { SaveButtonSelection };
export class SaveButtonComponent {
    /**
     * @param {?} renderer
     * @param {?} ngLocation
     */
    constructor(renderer, ngLocation) {
        this.renderer = renderer;
        this.ngLocation = ngLocation;
        // @ViewChild(MatButton) saveButton: MatButton;
        // @ViewChild('uikitSubmitButton', { read: ElementRef }) protected submitButton: ElementRef;
        // @ViewChild('uikitResetButton', { read: ElementRef }) protected resetButton: ElementRef;
        /**
         * Use the parent form to save (using submit), new (reset) and back (history)
         */
        // @Input() automaticTrigger = false;
        this.save = new EventEmitter();
        this.selection = SaveButtonSelection.Salvar;
        this.disabled = false;
    }
    /**
     * @return {?}
     */
    ngAfterViewInit() {
    }
    /**
     * @return {?}
     */
    saveAction() {
        console.log(SaveButtonSelection.Salvar);
        this.selection = SaveButtonSelection.Salvar;
        // if (this.automaticTrigger) {
        //   this.action();
        // }
        this.save.emit(SaveButtonSelection.Salvar);
    }
    /**
     * @return {?}
     */
    saveBackAction() {
        console.log(SaveButtonSelection.SalvarVoltar);
        this.selection = SaveButtonSelection.SalvarVoltar;
        // if (this.automaticTrigger) {
        //   this.action();
        // }
        this.save.emit(SaveButtonSelection.SalvarVoltar);
    }
    /**
     * @return {?}
     */
    saveNewAction() {
        console.log(SaveButtonSelection.SalvarNovo);
        this.selection = SaveButtonSelection.SalvarNovo;
        // if (this.automaticTrigger) {
        //   this.action();
        // }
        this.save.emit(SaveButtonSelection.SalvarNovo);
    }
    // submit() {
    //   const saveButtonElement = this.saveButton.nativeElement as HTMLButtonElement;
    //   this.renderer.setAttribute(saveButtonElement, `type`, `submit`);
    //   saveButtonElement.click();
    //   if (this.selection === SaveButtonSelection.SalvarNovo) {
    //   }
    // }
    /**
     * @return {?}
     */
    action() {
        if (this.selection === SaveButtonSelection.Salvar) {
            return this.saveAction();
        }
        else if (this.selection === SaveButtonSelection.SalvarNovo) {
            return this.saveNewAction();
        }
        else if (this.selection === SaveButtonSelection.SalvarVoltar) {
            return this.saveBackAction();
        }
    }
}
SaveButtonComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-save-button',
                template: "<div class=\"btn-group\">\n  <button type=\"button\" (click)=\"action()\" [disabled]=\"disabled\" mat-fab color=\"primary\">\n    <mat-icon class=\"fas fa-save\"></mat-icon> {{ selection }}\n  </button>\n  <div [hidden]=\"true\">\n    <button mat-button #uikitSubmitButton type=\"submit\"></button>\n    <button mat-button #uikitResetButton type=\"reset\"></button>\n  </div>\n  <mat-menu #menu=\"matMenu\">\n    <button mat-menu-item [disabled]=\"disabled\" (click)=\"saveAction()\">Salvar</button>\n    <button mat-menu-item [disabled]=\"disabled\" (click)=\"saveBackAction()\">Salvar e Voltar</button>\n    <button mat-menu-item [disabled]=\"disabled\" (click)=\"saveNewAction()\">Salvar e Novo</button>\n  </mat-menu>\n\n  <button type=\"button\" [disabled]=\"disabled\" [matMenuTriggerFor]=\"menu\" mat-fab color=\"primary\">\n    <mat-icon class=\"fas fa-caret-down\"></mat-icon>\n  </button>\n</div>\n",
                styles: [""]
            }] }
];
/** @nocollapse */
SaveButtonComponent.ctorParameters = () => [
    { type: Renderer2 },
    { type: Location }
];
SaveButtonComponent.propDecorators = {
    save: [{ type: Output }],
    submitButton: [{ type: ViewChild, args: ['uikitSubmitButton',] }],
    resetButton: [{ type: ViewChild, args: ['uikitResetButton',] }],
    selection: [{ type: Input }],
    disabled: [{ type: Input }]
};
if (false) {
    /**
     * Use the parent form to save (using submit), new (reset) and back (history)
     * @type {?}
     */
    SaveButtonComponent.prototype.save;
    /**
     * @type {?}
     * @protected
     */
    SaveButtonComponent.prototype.submitButton;
    /**
     * @type {?}
     * @protected
     */
    SaveButtonComponent.prototype.resetButton;
    /** @type {?} */
    SaveButtonComponent.prototype.selection;
    /** @type {?} */
    SaveButtonComponent.prototype.disabled;
    /**
     * @type {?}
     * @protected
     */
    SaveButtonComponent.prototype.renderer;
    /**
     * @type {?}
     * @protected
     */
    SaveButtonComponent.prototype.ngLocation;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2F2ZS1idXR0b24uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL3NhdmUtYnV0dG9uL3NhdmUtYnV0dG9uLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsU0FBUyxFQUE2QixTQUFTLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNoSSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDM0MsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLG1CQUFtQixDQUFDOzs7SUFHNUMsUUFBUyxRQUFRO0lBQ2pCLGNBQWUsaUJBQWlCO0lBQ2hDLFlBQWEsZUFBZTs7O0FBUTlCLE1BQU0sT0FBTyxtQkFBbUI7Ozs7O0lBcUI5QixZQUNZLFFBQW1CLEVBQ25CLFVBQW9CO1FBRHBCLGFBQVEsR0FBUixRQUFRLENBQVc7UUFDbkIsZUFBVSxHQUFWLFVBQVUsQ0FBVTs7Ozs7Ozs7UUFkdEIsU0FBSSxHQUFHLElBQUksWUFBWSxFQUF1QixDQUFDO1FBUXpDLGNBQVMsR0FBd0IsbUJBQW1CLENBQUMsTUFBTSxDQUFDO1FBRTVELGFBQVEsR0FBRyxLQUFLLENBQUM7SUFLN0IsQ0FBQzs7OztJQUVMLGVBQWU7SUFJZixDQUFDOzs7O0lBRUQsVUFBVTtRQUNSLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDeEMsSUFBSSxDQUFDLFNBQVMsR0FBRyxtQkFBbUIsQ0FBQyxNQUFNLENBQUM7UUFDNUMsK0JBQStCO1FBQy9CLG1CQUFtQjtRQUNuQixJQUFJO1FBQ0osSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDN0MsQ0FBQzs7OztJQUVELGNBQWM7UUFDWixPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQzlDLElBQUksQ0FBQyxTQUFTLEdBQUcsbUJBQW1CLENBQUMsWUFBWSxDQUFDO1FBQ2xELCtCQUErQjtRQUMvQixtQkFBbUI7UUFDbkIsSUFBSTtRQUNKLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQ25ELENBQUM7Ozs7SUFFRCxhQUFhO1FBQ1gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUM1QyxJQUFJLENBQUMsU0FBUyxHQUFHLG1CQUFtQixDQUFDLFVBQVUsQ0FBQztRQUNoRCwrQkFBK0I7UUFDL0IsbUJBQW1CO1FBQ25CLElBQUk7UUFDSixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUNqRCxDQUFDOzs7Ozs7Ozs7OztJQVdELE1BQU07UUFDSixJQUFJLElBQUksQ0FBQyxTQUFTLEtBQUssbUJBQW1CLENBQUMsTUFBTSxFQUFFO1lBQ2pELE9BQU8sSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO1NBQzFCO2FBQU0sSUFBSSxJQUFJLENBQUMsU0FBUyxLQUFLLG1CQUFtQixDQUFDLFVBQVUsRUFBRTtZQUM1RCxPQUFPLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztTQUM3QjthQUFNLElBQUksSUFBSSxDQUFDLFNBQVMsS0FBSyxtQkFBbUIsQ0FBQyxZQUFZLEVBQUU7WUFDOUQsT0FBTyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7U0FDOUI7SUFDSCxDQUFDOzs7WUFqRkYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxtQkFBbUI7Z0JBQzdCLHc1QkFBMkM7O2FBRTVDOzs7O1lBZHdFLFNBQVM7WUFDekUsUUFBUTs7O21CQXVCZCxNQUFNOzJCQUtOLFNBQVMsU0FBQyxtQkFBbUI7MEJBQzdCLFNBQVMsU0FBQyxrQkFBa0I7d0JBRTVCLEtBQUs7dUJBRUwsS0FBSzs7Ozs7OztJQVZOLG1DQUF5RDs7Ozs7SUFLekQsMkNBQWtFOzs7OztJQUNsRSwwQ0FBZ0U7O0lBRWhFLHdDQUE0RTs7SUFFNUUsdUNBQWlDOzs7OztJQUcvQix1Q0FBNkI7Ozs7O0lBQzdCLHlDQUE4QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgVmlld0NoaWxkLCBBZnRlclZpZXdJbml0LCBFbGVtZW50UmVmLCBSZW5kZXJlcjIsIE91dHB1dCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBMb2NhdGlvbiB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQgeyBNYXRCdXR0b24gfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XG5cbmV4cG9ydCBlbnVtIFNhdmVCdXR0b25TZWxlY3Rpb24ge1xuICBTYWx2YXIgPSAnU2FsdmFyJyxcbiAgU2FsdmFyVm9sdGFyID0gJ1NhbHZhciBlIHZvbHRhcicsXG4gIFNhbHZhck5vdm8gPSAnU2FsdmFyIGUgbm92bycsXG59XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ3Vpa2l0LXNhdmUtYnV0dG9uJyxcbiAgdGVtcGxhdGVVcmw6ICcuL3NhdmUtYnV0dG9uLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vc2F2ZS1idXR0b24uY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBTYXZlQnV0dG9uQ29tcG9uZW50IGltcGxlbWVudHMgQWZ0ZXJWaWV3SW5pdCB7XG5cbiAgLy8gQFZpZXdDaGlsZChNYXRCdXR0b24pIHNhdmVCdXR0b246IE1hdEJ1dHRvbjtcbiAgLy8gQFZpZXdDaGlsZCgndWlraXRTdWJtaXRCdXR0b24nLCB7IHJlYWQ6IEVsZW1lbnRSZWYgfSkgcHJvdGVjdGVkIHN1Ym1pdEJ1dHRvbjogRWxlbWVudFJlZjtcbiAgLy8gQFZpZXdDaGlsZCgndWlraXRSZXNldEJ1dHRvbicsIHsgcmVhZDogRWxlbWVudFJlZiB9KSBwcm90ZWN0ZWQgcmVzZXRCdXR0b246IEVsZW1lbnRSZWY7XG5cbiAgLyoqIFVzZSB0aGUgcGFyZW50IGZvcm0gdG8gc2F2ZSAodXNpbmcgc3VibWl0KSwgbmV3IChyZXNldCkgYW5kIGJhY2sgKGhpc3RvcnkpICovXG4gIC8vIEBJbnB1dCgpIGF1dG9tYXRpY1RyaWdnZXIgPSBmYWxzZTtcblxuICBAT3V0cHV0KCkgc2F2ZSA9IG5ldyBFdmVudEVtaXR0ZXI8U2F2ZUJ1dHRvblNlbGVjdGlvbj4oKTtcbiAgLy8gQE91dHB1dCgpIGNsaWNrQXRTYXZlID0gbmV3IEV2ZW50RW1pdHRlcjxTYXZlQnV0dG9uU2VsZWN0aW9uPigpO1xuICAvLyBAT3V0cHV0KCkgY2xpY2tBdFNhdmVBbmRCYWNrID0gbmV3IEV2ZW50RW1pdHRlcjxTYXZlQnV0dG9uU2VsZWN0aW9uPigpO1xuICAvLyBAT3V0cHV0KCkgY2xpY2tBdFNhdmVBbmROZXcgPSBuZXcgRXZlbnRFbWl0dGVyPFNhdmVCdXR0b25TZWxlY3Rpb24+KCk7XG5cbiAgQFZpZXdDaGlsZCgndWlraXRTdWJtaXRCdXR0b24nKSBwcm90ZWN0ZWQgc3VibWl0QnV0dG9uOiBNYXRCdXR0b247XG4gIEBWaWV3Q2hpbGQoJ3Vpa2l0UmVzZXRCdXR0b24nKSBwcm90ZWN0ZWQgcmVzZXRCdXR0b246IE1hdEJ1dHRvbjtcblxuICBASW5wdXQoKSBwdWJsaWMgc2VsZWN0aW9uOiBTYXZlQnV0dG9uU2VsZWN0aW9uID0gU2F2ZUJ1dHRvblNlbGVjdGlvbi5TYWx2YXI7XG5cbiAgQElucHV0KCkgcHVibGljIGRpc2FibGVkID0gZmFsc2U7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJvdGVjdGVkIHJlbmRlcmVyOiBSZW5kZXJlcjIsXG4gICAgcHJvdGVjdGVkIG5nTG9jYXRpb246IExvY2F0aW9uLFxuICApIHsgfVxuXG4gIG5nQWZ0ZXJWaWV3SW5pdCgpIHtcblxuXG5cbiAgfVxuXG4gIHNhdmVBY3Rpb24oKSB7XG4gICAgY29uc29sZS5sb2coU2F2ZUJ1dHRvblNlbGVjdGlvbi5TYWx2YXIpO1xuICAgIHRoaXMuc2VsZWN0aW9uID0gU2F2ZUJ1dHRvblNlbGVjdGlvbi5TYWx2YXI7XG4gICAgLy8gaWYgKHRoaXMuYXV0b21hdGljVHJpZ2dlcikge1xuICAgIC8vICAgdGhpcy5hY3Rpb24oKTtcbiAgICAvLyB9XG4gICAgdGhpcy5zYXZlLmVtaXQoU2F2ZUJ1dHRvblNlbGVjdGlvbi5TYWx2YXIpO1xuICB9XG5cbiAgc2F2ZUJhY2tBY3Rpb24oKSB7XG4gICAgY29uc29sZS5sb2coU2F2ZUJ1dHRvblNlbGVjdGlvbi5TYWx2YXJWb2x0YXIpO1xuICAgIHRoaXMuc2VsZWN0aW9uID0gU2F2ZUJ1dHRvblNlbGVjdGlvbi5TYWx2YXJWb2x0YXI7XG4gICAgLy8gaWYgKHRoaXMuYXV0b21hdGljVHJpZ2dlcikge1xuICAgIC8vICAgdGhpcy5hY3Rpb24oKTtcbiAgICAvLyB9XG4gICAgdGhpcy5zYXZlLmVtaXQoU2F2ZUJ1dHRvblNlbGVjdGlvbi5TYWx2YXJWb2x0YXIpO1xuICB9XG5cbiAgc2F2ZU5ld0FjdGlvbigpIHtcbiAgICBjb25zb2xlLmxvZyhTYXZlQnV0dG9uU2VsZWN0aW9uLlNhbHZhck5vdm8pO1xuICAgIHRoaXMuc2VsZWN0aW9uID0gU2F2ZUJ1dHRvblNlbGVjdGlvbi5TYWx2YXJOb3ZvO1xuICAgIC8vIGlmICh0aGlzLmF1dG9tYXRpY1RyaWdnZXIpIHtcbiAgICAvLyAgIHRoaXMuYWN0aW9uKCk7XG4gICAgLy8gfVxuICAgIHRoaXMuc2F2ZS5lbWl0KFNhdmVCdXR0b25TZWxlY3Rpb24uU2FsdmFyTm92byk7XG4gIH1cblxuICAvLyBzdWJtaXQoKSB7XG4gIC8vICAgY29uc3Qgc2F2ZUJ1dHRvbkVsZW1lbnQgPSB0aGlzLnNhdmVCdXR0b24ubmF0aXZlRWxlbWVudCBhcyBIVE1MQnV0dG9uRWxlbWVudDtcbiAgLy8gICB0aGlzLnJlbmRlcmVyLnNldEF0dHJpYnV0ZShzYXZlQnV0dG9uRWxlbWVudCwgYHR5cGVgLCBgc3VibWl0YCk7XG4gIC8vICAgc2F2ZUJ1dHRvbkVsZW1lbnQuY2xpY2soKTtcbiAgLy8gICBpZiAodGhpcy5zZWxlY3Rpb24gPT09IFNhdmVCdXR0b25TZWxlY3Rpb24uU2FsdmFyTm92bykge1xuXG4gIC8vICAgfVxuICAvLyB9XG5cbiAgYWN0aW9uKCkge1xuICAgIGlmICh0aGlzLnNlbGVjdGlvbiA9PT0gU2F2ZUJ1dHRvblNlbGVjdGlvbi5TYWx2YXIpIHtcbiAgICAgIHJldHVybiB0aGlzLnNhdmVBY3Rpb24oKTtcbiAgICB9IGVsc2UgaWYgKHRoaXMuc2VsZWN0aW9uID09PSBTYXZlQnV0dG9uU2VsZWN0aW9uLlNhbHZhck5vdm8pIHtcbiAgICAgIHJldHVybiB0aGlzLnNhdmVOZXdBY3Rpb24oKTtcbiAgICB9IGVsc2UgaWYgKHRoaXMuc2VsZWN0aW9uID09PSBTYXZlQnV0dG9uU2VsZWN0aW9uLlNhbHZhclZvbHRhcikge1xuICAgICAgcmV0dXJuIHRoaXMuc2F2ZUJhY2tBY3Rpb24oKTtcbiAgICB9XG4gIH1cblxuICAvLyBhY3Rpb24oKSB7XG4gIC8vICAgY29uc3Qgc3VibWl0QnV0dG9uRWxlbWVudFJlZiA9IHRoaXMuc3VibWl0QnV0dG9uLl9lbGVtZW50UmVmO1xuICAvLyAgIGNvbnN0IHN1Ym1pdEJ1dHRvbkVsZW1lbnQgPSBzdWJtaXRCdXR0b25FbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQgYXMgSFRNTEJ1dHRvbkVsZW1lbnQ7XG4gIC8vICAgc3VibWl0QnV0dG9uRWxlbWVudC5jbGljaygpO1xuICAvLyAgIGlmICh0aGlzLnNlbGVjdGlvbiA9PT0gU2F2ZUJ1dHRvblNlbGVjdGlvbi5TYWx2YXJOb3ZvKSB7XG4gIC8vICAgICBjb25zdCByZXNldEJ1dHRvbkVsZW1lbnRSZWYgPSB0aGlzLnJlc2V0QnV0dG9uLl9lbGVtZW50UmVmO1xuICAvLyAgICAgY29uc3QgcmVzZXRCdXR0b25FbGVtZW50ID0gcmVzZXRCdXR0b25FbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQgYXMgSFRNTEJ1dHRvbkVsZW1lbnQ7XG4gIC8vICAgICByZXNldEJ1dHRvbkVsZW1lbnQuY2xpY2soKTtcbiAgLy8gICB9IGVsc2UgaWYgKHRoaXMuc2VsZWN0aW9uID09PSBTYXZlQnV0dG9uU2VsZWN0aW9uLlNhbHZhclZvbHRhcikge1xuICAvLyAgICAgdGhpcy5uZ0xvY2F0aW9uLmJhY2soKTtcbiAgLy8gICB9XG4gIC8vIH1cblxufVxuIl19