/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { filter } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
export class PreviousRouteService {
    /**
     * @param {?} router
     */
    constructor(router) {
        this.router = router;
        this.history = [];
        this.rotasAcessadas = [];
        this.removerRotaAcessada = (/**
         * @return {?}
         */
        () => this.rotasAcessadas.splice(this.rotasAcessadas.length - 2, 2));
        console.log('constructor PreviousRouteService');
        this.desabilitarBotaoVoltar = new BehaviorSubject(true);
        this.router.events
            .pipe(filter((/**
         * @param {?} event
         * @return {?}
         */
        event => event instanceof NavigationEnd)))
            .subscribe((/**
         * @param {?} __0
         * @return {?}
         */
        ({ urlAfterRedirects }) => {
            this.history = [...this.history, urlAfterRedirects];
            this.desabilitarBotaoVoltar.next(this.rotasAcessadas.length === 0);
            this.rotasAcessadas = [...this.rotasAcessadas, urlAfterRedirects];
        }));
        // this.router.events
        //   .pipe(filter((event: RouterEvent) => event instanceof NavigationEnd))
        //   .subscribe((event: NavigationEnd) => {
        //     console.log(`prev -> ${this.currentUrl}`);
        //     console.log(`curr -> ${event.urlAfterRedirects}`);
        //     this.previousUrl.next(this.currentUrl);
        //     this.currentUrl = event.urlAfterRedirects;
        //   });
        // this.previousUrl$.subscribe((previousUrl) => {
        //   this.previousUrl = previousUrl;
        // });
        // this.currentUrl$.subscribe((currentUrl) => {
        //   this.currentUrl = currentUrl;
        // });
        // this.currentUrlSubject.next(this.router.url);
        // router.events.subscribe(event => {
        //   if (event instanceof NavigationEnd) {
        //     this.previousUrlSubject.next(this.currentUrlSubject.getValue());
        //     this.currentUrlSubject.next(event.url);
        //   }
        // });
    }
    /**
     * @return {?}
     */
    getHistory() {
        return this.history;
    }
    /**
     * @return {?}
     */
    getPreviousUrl() {
        return this.history[this.history.length - 2];
    }
}
PreviousRouteService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
PreviousRouteService.ctorParameters = () => [
    { type: Router }
];
/** @nocollapse */ PreviousRouteService.ngInjectableDef = i0.defineInjectable({ factory: function PreviousRouteService_Factory() { return new PreviousRouteService(i0.inject(i1.Router)); }, token: PreviousRouteService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    PreviousRouteService.prototype.history;
    /** @type {?} */
    PreviousRouteService.prototype.desabilitarBotaoVoltar;
    /**
     * @type {?}
     * @private
     */
    PreviousRouteService.prototype.rotasAcessadas;
    /** @type {?} */
    PreviousRouteService.prototype.removerRotaAcessada;
    /**
     * @type {?}
     * @private
     */
    PreviousRouteService.prototype.router;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJldmlvdXMtcm91dGUuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9wcmV2aW91cy1yb3V0ZS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsVUFBVSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBa0IsTUFBTSxFQUFlLGFBQWEsRUFBQyxNQUFNLGlCQUFpQixDQUFDO0FBRXBGLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDdkMsT0FBTyxFQUFFLE1BQU0sRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7QUFLeEMsTUFBTSxPQUFPLG9CQUFvQjs7OztJQWUvQixZQUFvQixNQUFjO1FBQWQsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQWIxQixZQUFPLEdBQUcsRUFBRSxDQUFDO1FBV2IsbUJBQWMsR0FBRyxFQUFFLENBQUM7UUFnRHJCLHdCQUFtQjs7O1FBQUcsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFDO1FBN0MvRixPQUFPLENBQUMsR0FBRyxDQUFDLGtDQUFrQyxDQUFDLENBQUM7UUFFaEQsSUFBSSxDQUFDLHNCQUFzQixHQUFHLElBQUksZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBRXhELElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTTthQUNmLElBQUksQ0FBQyxNQUFNOzs7O1FBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxLQUFLLFlBQVksYUFBYSxFQUFDLENBQUM7YUFDckQsU0FBUzs7OztRQUFDLENBQUMsRUFBRSxpQkFBaUIsRUFBaUIsRUFBRSxFQUFFO1lBQ2xELElBQUksQ0FBQyxPQUFPLEdBQUcsQ0FBRSxHQUFHLElBQUksQ0FBQyxPQUFPLEVBQUUsaUJBQWlCLENBQUUsQ0FBQztZQUV0RCxJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ25FLElBQUksQ0FBQyxjQUFjLEdBQUcsQ0FBRSxHQUFHLElBQUksQ0FBQyxjQUFjLEVBQUUsaUJBQWlCLENBQUUsQ0FBQztRQUN0RSxDQUFDLEVBQUMsQ0FBQztRQUVMLHFCQUFxQjtRQUNyQiwwRUFBMEU7UUFDMUUsMkNBQTJDO1FBQzNDLGlEQUFpRDtRQUNqRCx5REFBeUQ7UUFDekQsOENBQThDO1FBQzlDLGlEQUFpRDtRQUNqRCxRQUFRO1FBRVIsaURBQWlEO1FBQ2pELG9DQUFvQztRQUNwQyxNQUFNO1FBQ04sK0NBQStDO1FBQy9DLGtDQUFrQztRQUNsQyxNQUFNO1FBQ04sZ0RBQWdEO1FBQ2hELHFDQUFxQztRQUNyQywwQ0FBMEM7UUFDMUMsdUVBQXVFO1FBQ3ZFLDhDQUE4QztRQUM5QyxNQUFNO1FBQ04sTUFBTTtJQUNSLENBQUM7Ozs7SUFFTSxVQUFVO1FBQ2YsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO0lBQ3RCLENBQUM7Ozs7SUFFTSxjQUFjO1FBQ25CLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQztJQUMvQyxDQUFDOzs7WUE5REYsVUFBVSxTQUFDO2dCQUNWLFVBQVUsRUFBRSxNQUFNO2FBQ25COzs7O1lBUHdCLE1BQU07Ozs7Ozs7O0lBVTdCLHVDQUFxQjs7SUFVckIsc0RBQXdEOzs7OztJQUN4RCw4Q0FBNEI7O0lBZ0Q1QixtREFBaUc7Ozs7O0lBOUNyRixzQ0FBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0luamVjdGFibGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtOYXZpZ2F0aW9uU3RhcnQsIFJvdXRlciwgUm91dGVyRXZlbnQsIE5hdmlnYXRpb25FbmR9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQge1BsYXRmb3JtTG9jYXRpb259IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQgeyBCZWhhdmlvclN1YmplY3QgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IGZpbHRlciB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgUHJldmlvdXNSb3V0ZVNlcnZpY2Uge1xuXG4gIHByaXZhdGUgaGlzdG9yeSA9IFtdO1xuXG4gIC8vIHByb3RlY3RlZCBwcmV2aW91c1VybFN1YmplY3QgPSBuZXcgQmVoYXZpb3JTdWJqZWN0PHN0cmluZz4obnVsbCk7XG4gIC8vIHB1YmxpYyBwcmV2aW91c1VybCQgPSB0aGlzLnByZXZpb3VzVXJsU3ViamVjdC5hc09ic2VydmFibGUoKS5waXBlKGRpc3RpbmN0VW50aWxDaGFuZ2VkKCkpO1xuICAvLyBwdWJsaWMgcHJldmlvdXNVcmwgPSBudWxsO1xuXG4gIC8vIHByb3RlY3RlZCBjdXJyZW50VXJsU3ViamVjdCA9IG5ldyBCZWhhdmlvclN1YmplY3Q8c3RyaW5nPihudWxsKTtcbiAgLy8gcHVibGljIGN1cnJlbnRVcmwkID0gdGhpcy5jdXJyZW50VXJsU3ViamVjdC5hc09ic2VydmFibGUoKS5waXBlKGRpc3RpbmN0VW50aWxDaGFuZ2VkKCkpO1xuICAvLyBwdWJsaWMgY3VycmVudFVybCA9IG51bGw7XG5cbiAgcHVibGljIGRlc2FiaWxpdGFyQm90YW9Wb2x0YXI6IEJlaGF2aW9yU3ViamVjdDxib29sZWFuPjtcbiAgcHJpdmF0ZSByb3Rhc0FjZXNzYWRhcyA9IFtdO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgcm91dGVyOiBSb3V0ZXIpIHtcbiAgICBjb25zb2xlLmxvZygnY29uc3RydWN0b3IgUHJldmlvdXNSb3V0ZVNlcnZpY2UnKTtcblxuICAgIHRoaXMuZGVzYWJpbGl0YXJCb3Rhb1ZvbHRhciA9IG5ldyBCZWhhdmlvclN1YmplY3QodHJ1ZSk7XG5cbiAgICB0aGlzLnJvdXRlci5ldmVudHNcbiAgICAgIC5waXBlKGZpbHRlcihldmVudCA9PiBldmVudCBpbnN0YW5jZW9mIE5hdmlnYXRpb25FbmQpKVxuICAgICAgLnN1YnNjcmliZSgoeyB1cmxBZnRlclJlZGlyZWN0cyB9OiBOYXZpZ2F0aW9uRW5kKSA9PiB7XG4gICAgICAgIHRoaXMuaGlzdG9yeSA9IFsgLi4udGhpcy5oaXN0b3J5LCB1cmxBZnRlclJlZGlyZWN0cyBdO1xuXG4gICAgICAgIHRoaXMuZGVzYWJpbGl0YXJCb3Rhb1ZvbHRhci5uZXh0KHRoaXMucm90YXNBY2Vzc2FkYXMubGVuZ3RoID09PSAwKTtcbiAgICAgICAgdGhpcy5yb3Rhc0FjZXNzYWRhcyA9IFsgLi4udGhpcy5yb3Rhc0FjZXNzYWRhcywgdXJsQWZ0ZXJSZWRpcmVjdHMgXTtcbiAgICAgIH0pO1xuXG4gICAgLy8gdGhpcy5yb3V0ZXIuZXZlbnRzXG4gICAgLy8gICAucGlwZShmaWx0ZXIoKGV2ZW50OiBSb3V0ZXJFdmVudCkgPT4gZXZlbnQgaW5zdGFuY2VvZiBOYXZpZ2F0aW9uRW5kKSlcbiAgICAvLyAgIC5zdWJzY3JpYmUoKGV2ZW50OiBOYXZpZ2F0aW9uRW5kKSA9PiB7XG4gICAgLy8gICAgIGNvbnNvbGUubG9nKGBwcmV2IC0+ICR7dGhpcy5jdXJyZW50VXJsfWApO1xuICAgIC8vICAgICBjb25zb2xlLmxvZyhgY3VyciAtPiAke2V2ZW50LnVybEFmdGVyUmVkaXJlY3RzfWApO1xuICAgIC8vICAgICB0aGlzLnByZXZpb3VzVXJsLm5leHQodGhpcy5jdXJyZW50VXJsKTtcbiAgICAvLyAgICAgdGhpcy5jdXJyZW50VXJsID0gZXZlbnQudXJsQWZ0ZXJSZWRpcmVjdHM7XG4gICAgLy8gICB9KTtcblxuICAgIC8vIHRoaXMucHJldmlvdXNVcmwkLnN1YnNjcmliZSgocHJldmlvdXNVcmwpID0+IHtcbiAgICAvLyAgIHRoaXMucHJldmlvdXNVcmwgPSBwcmV2aW91c1VybDtcbiAgICAvLyB9KTtcbiAgICAvLyB0aGlzLmN1cnJlbnRVcmwkLnN1YnNjcmliZSgoY3VycmVudFVybCkgPT4ge1xuICAgIC8vICAgdGhpcy5jdXJyZW50VXJsID0gY3VycmVudFVybDtcbiAgICAvLyB9KTtcbiAgICAvLyB0aGlzLmN1cnJlbnRVcmxTdWJqZWN0Lm5leHQodGhpcy5yb3V0ZXIudXJsKTtcbiAgICAvLyByb3V0ZXIuZXZlbnRzLnN1YnNjcmliZShldmVudCA9PiB7XG4gICAgLy8gICBpZiAoZXZlbnQgaW5zdGFuY2VvZiBOYXZpZ2F0aW9uRW5kKSB7XG4gICAgLy8gICAgIHRoaXMucHJldmlvdXNVcmxTdWJqZWN0Lm5leHQodGhpcy5jdXJyZW50VXJsU3ViamVjdC5nZXRWYWx1ZSgpKTtcbiAgICAvLyAgICAgdGhpcy5jdXJyZW50VXJsU3ViamVjdC5uZXh0KGV2ZW50LnVybCk7XG4gICAgLy8gICB9XG4gICAgLy8gfSk7XG4gIH1cblxuICBwdWJsaWMgZ2V0SGlzdG9yeSgpOiBzdHJpbmdbXSB7XG4gICAgcmV0dXJuIHRoaXMuaGlzdG9yeTtcbiAgfVxuXG4gIHB1YmxpYyBnZXRQcmV2aW91c1VybCgpOiBzdHJpbmcge1xuICAgIHJldHVybiB0aGlzLmhpc3RvcnlbdGhpcy5oaXN0b3J5Lmxlbmd0aCAtIDJdO1xuICB9XG5cbiAgcHVibGljIHJlbW92ZXJSb3RhQWNlc3NhZGEgPSAoKSA9PiB0aGlzLnJvdGFzQWNlc3NhZGFzLnNwbGljZSh0aGlzLnJvdGFzQWNlc3NhZGFzLmxlbmd0aCAtIDIsIDIpO1xuXG4gIC8vIHB1YmxpYyBnZXQgZ2V0UHJldmlvdXNVcmwoKSB7XG4gIC8vICAgcmV0dXJuIHRoaXMucHJldmlvdXNVcmw7XG4gIC8vIH1cblxuICAvLyBwdWJsaWMgZ2V0IGdldEN1cnJlbnRVcmwoKSB7XG4gIC8vICAgcmV0dXJuIHRoaXMuY3VycmVudFVybDtcbiAgLy8gfVxufVxuIl19