/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { fromEvent, throwError } from 'rxjs';
import { mapTo, retryWhen, switchMap } from 'rxjs/operators';
export class OfflineInterceptor {
    constructor() {
        this.onlineChanges$ = fromEvent(window, 'online').pipe(mapTo(true));
    }
    /**
     * @return {?}
     */
    get isOnline() {
        return navigator.onLine;
    }
    /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    intercept(req, next) {
        return next.handle(req)
            .pipe(retryWhen((/**
         * @param {?} errors
         * @return {?}
         */
        errors => {
            if (this.isOnline) {
                return errors.pipe(switchMap((/**
                 * @param {?} err
                 * @return {?}
                 */
                err => throwError(err))));
            }
            else if (req.method === 'GET') {
                // Não vamos fazer retry se não for GET (imutabilidade)
                return this.onlineChanges$;
            }
        })));
    }
}
OfflineInterceptor.decorators = [
    { type: Injectable }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    OfflineInterceptor.prototype.onlineChanges$;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib2ZmbGluZS1pbnRlcmNlcHRvci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvb2ZmbGluZS9vZmZsaW5lLWludGVyY2VwdG9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFjLE1BQU0sTUFBTSxDQUFDO0FBQ3pELE9BQU8sRUFBRSxLQUFLLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBVSxNQUFNLGdCQUFnQixDQUFDO0FBSXJFLE1BQU0sT0FBTyxrQkFBa0I7SUFEL0I7UUFFVSxtQkFBYyxHQUFHLFNBQVMsQ0FBQyxNQUFNLEVBQUUsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO0lBbUJ6RSxDQUFDOzs7O0lBakJDLElBQUksUUFBUTtRQUNWLE9BQU8sU0FBUyxDQUFDLE1BQU0sQ0FBQztJQUMxQixDQUFDOzs7Ozs7SUFFRCxTQUFTLENBQUMsR0FBcUIsRUFBRSxJQUFpQjtRQUNoRCxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDO2FBQ3RCLElBQUksQ0FDSCxTQUFTOzs7O1FBQUMsTUFBTSxDQUFDLEVBQUU7WUFDakIsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO2dCQUNqQixPQUFPLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUzs7OztnQkFBQyxHQUFHLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsRUFBQyxDQUFDLENBQUM7YUFDdkQ7aUJBQU0sSUFBSSxHQUFHLENBQUMsTUFBTSxLQUFLLEtBQUssRUFBRTtnQkFDL0IsdURBQXVEO2dCQUN2RCxPQUFPLElBQUksQ0FBQyxjQUFjLENBQUM7YUFDNUI7UUFDSCxDQUFDLEVBQUMsQ0FDSCxDQUFDO0lBQ0osQ0FBQzs7O1lBcEJGLFVBQVU7Ozs7Ozs7SUFFVCw0Q0FBdUUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBmcm9tRXZlbnQsIHRocm93RXJyb3IsIE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IG1hcFRvLCByZXRyeVdoZW4sIHN3aXRjaE1hcCwgZmlsdGVyIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHsgSHR0cEludGVyY2VwdG9yLCBIdHRwUmVxdWVzdCwgSHR0cEhhbmRsZXIsIEh0dHBFdmVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIE9mZmxpbmVJbnRlcmNlcHRvciBpbXBsZW1lbnRzIEh0dHBJbnRlcmNlcHRvciB7XG4gIHByaXZhdGUgb25saW5lQ2hhbmdlcyQgPSBmcm9tRXZlbnQod2luZG93LCAnb25saW5lJykucGlwZShtYXBUbyh0cnVlKSk7XG5cbiAgZ2V0IGlzT25saW5lKCkge1xuICAgIHJldHVybiBuYXZpZ2F0b3Iub25MaW5lO1xuICB9XG5cbiAgaW50ZXJjZXB0KHJlcTogSHR0cFJlcXVlc3Q8YW55PiwgbmV4dDogSHR0cEhhbmRsZXIpOiBPYnNlcnZhYmxlPEh0dHBFdmVudDxhbnk+PiB7XG4gICAgcmV0dXJuIG5leHQuaGFuZGxlKHJlcSlcbiAgICAucGlwZShcbiAgICAgIHJldHJ5V2hlbihlcnJvcnMgPT4ge1xuICAgICAgICBpZiAodGhpcy5pc09ubGluZSkge1xuICAgICAgICAgIHJldHVybiBlcnJvcnMucGlwZShzd2l0Y2hNYXAoZXJyID0+IHRocm93RXJyb3IoZXJyKSkpO1xuICAgICAgICB9IGVsc2UgaWYgKHJlcS5tZXRob2QgPT09ICdHRVQnKSB7XG4gICAgICAgICAgLy8gTsOjbyB2YW1vcyBmYXplciByZXRyeSBzZSBuw6NvIGZvciBHRVQgKGltdXRhYmlsaWRhZGUpXG4gICAgICAgICAgcmV0dXJuIHRoaXMub25saW5lQ2hhbmdlcyQ7XG4gICAgICAgIH1cbiAgICAgIH0pXG4gICAgKTtcbiAgfVxufVxuIl19