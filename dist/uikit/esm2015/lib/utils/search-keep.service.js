/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { SearchQuery } from '../layout/header/search/state/search.query';
import { SearchStore } from '../layout/header/search/state/search.store';
import * as _ from 'lodash';
import { map } from 'rxjs/operators';
import { LayoutService } from '../layout/layout.service';
import { guid } from '@datorama/akita';
import * as Fuse from 'fuse.js';
export class SearchKeepService {
    /**
     * @param {?} layoutService
     * @param {?} searchQuery
     * @param {?} searchStore
     */
    constructor(layoutService, searchQuery, searchStore) {
        this.layoutService = layoutService;
        this.searchQuery = searchQuery;
        this.searchStore = searchStore;
        this.searchGroups = this.layoutService.menuItemsFlattened$;
        this.searchGroupsAll = this.layoutService.menuItems$;
    }
    /*
    * Método responsável por adicionar um novo grupo na pesquisas.
    * */
    /**
     * @param {?} groups
     * @return {?}
     */
    addGroup(groups) {
        /** @type {?} */
        const itensGroups = this.searchQuery.getAll();
        if (_.head(itensGroups) !== undefined) {
            this.searchStore.adicionarGrupoPesquisa(itensGroups, groups);
        }
        else {
            this.searchStore.addGroupBase(groups);
        }
    }
    /*
        * Método responsável por remover um grupo da pesquisa.
        * */
    /**
     * @param {?} group
     * @return {?}
     */
    removeGroup(group) {
        this.searchStore.removerGrupoPesquisa(group.title);
    }
    /*
       * Método responsável por realizar a pesquisa dos grupo na pesquisa.
       * */
    /**
     * @param {?} term
     * @return {?}
     */
    searchGroup(term) {
        return this._filterGroup(term);
    }
    /**
     * @private
     * @param {?} value
     * @return {?}
     */
    _filterGroup(value) {
        /** @type {?} */
        const options = {
            keys: ['title', 'tags',
                'children.title', 'children.tags',
                'children.children.title', 'children.children.title.tags',
                'children.children.children.title', 'children.children.children.title.tags']
        };
        return this.searchGroupsAll.pipe(map((/**
         * @param {?} listMenuItem
         * @return {?}
         */
        listMenuItem => this.applyFilter(listMenuItem, value, options))), map((/**
         * @param {?} itens
         * @return {?}
         */
        itens => this.groupReturnFilter(itens))));
    }
    /**
     * @private
     * @param {?} listMenuItem
     * @param {?} value
     * @param {?} options
     * @return {?}
     */
    applyFilter(listMenuItem, value, options) {
        /** @type {?} */
        const fuse = new Fuse(listMenuItem, options);
        return fuse.search(value);
    }
    /**
     * @private
     * @param {?} itens
     * @return {?}
     */
    groupReturnFilter(itens) {
        /** @type {?} */
        const functionMapItem = (/**
         * @param {?} group
         * @return {?}
         */
        (group) => {
            return (/** @type {?} */ ({
                id: group.id ? group.id : guid(),
                title: group.title,
                icon: group.icon,
                link: group.link,
                tags: group.tags
            }));
        });
        /** @type {?} */
        const functionMapGroup = (/**
         * @param {?} group
         * @return {?}
         */
        (group) => {
            /** @type {?} */
            let groupMenu;
            if (group.link) {
                groupMenu = functionMapItem(group);
            }
            else {
                groupMenu = (/** @type {?} */ ({
                    id: group.id ? group.id : guid(),
                    title: group.title,
                    icon: group.icon,
                    isHeader: true,
                }));
                if (group.children && group.children.length > 1) {
                    /** @type {?} */
                    const itemFilhos = group.children
                        .filter((/**
                     * @param {?} b
                     * @return {?}
                     */
                    b => b !== group))
                        .map((/**
                     * @param {?} itemIt
                     * @return {?}
                     */
                    (itemIt) => {
                        if (itemIt.children && itemIt.children.length > 1) {
                            return functionMapGroup(itemIt);
                        }
                        else {
                            return functionMapItem(itemIt);
                        }
                    }));
                    if ((/** @type {?} */ (itemFilhos))) {
                        groupMenu.items = (/** @type {?} */ (itemFilhos));
                    }
                    else {
                        groupMenu.items = (/** @type {?} */ (itemFilhos));
                    }
                }
            }
            return groupMenu;
        });
        return (/** @type {?} */ ({
            id: guid(),
            title: 'Menu',
            isHeader: true,
            items: itens.map(functionMapGroup)
        }));
    }
}
SearchKeepService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
SearchKeepService.ctorParameters = () => [
    { type: LayoutService },
    { type: SearchQuery },
    { type: SearchStore }
];
if (false) {
    /**
     * @type {?}
     * @protected
     */
    SearchKeepService.prototype.searchGroups;
    /**
     * @type {?}
     * @protected
     */
    SearchKeepService.prototype.searchGroupsAll;
    /**
     * @type {?}
     * @protected
     */
    SearchKeepService.prototype.layoutService;
    /**
     * @type {?}
     * @private
     */
    SearchKeepService.prototype.searchQuery;
    /**
     * @type {?}
     * @private
     */
    SearchKeepService.prototype.searchStore;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLWtlZXAuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvdXRpbHMvc2VhcmNoLWtlZXAuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUV6QyxPQUFPLEVBQUMsV0FBVyxFQUFDLE1BQU0sNENBQTRDLENBQUM7QUFDdkUsT0FBTyxFQUFDLFdBQVcsRUFBQyxNQUFNLDRDQUE0QyxDQUFDO0FBQ3ZFLE9BQU8sS0FBSyxDQUFDLE1BQU0sUUFBUSxDQUFDO0FBRzVCLE9BQU8sRUFBQyxHQUFHLEVBQUMsTUFBTSxnQkFBZ0IsQ0FBQztBQUVuQyxPQUFPLEVBQUMsYUFBYSxFQUFDLE1BQU0sMEJBQTBCLENBQUM7QUFDdkQsT0FBTyxFQUFDLElBQUksRUFBQyxNQUFNLGlCQUFpQixDQUFDO0FBQ3JDLE9BQU8sS0FBSyxJQUFJLE1BQU0sU0FBUyxDQUFDO0FBSWhDLE1BQU0sT0FBTyxpQkFBaUI7Ozs7OztJQUs1QixZQUFzQixhQUE0QixFQUM5QixXQUF3QixFQUN4QixXQUF3QjtRQUZ0QixrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUM5QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUxsQyxpQkFBWSxHQUEyQixJQUFJLENBQUMsYUFBYSxDQUFDLG1CQUFtQixDQUFDO1FBQzlFLG9CQUFlLEdBQTJCLElBQUksQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDO0lBS2xGLENBQUM7Ozs7Ozs7O0lBS0QsUUFBUSxDQUFDLE1BQXVCOztjQUN4QixXQUFXLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUU7UUFDN0MsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLFNBQVMsRUFBRTtZQUNyQyxJQUFJLENBQUMsV0FBVyxDQUFDLHNCQUFzQixDQUFDLFdBQVcsRUFBRSxNQUFNLENBQUMsQ0FBQztTQUM5RDthQUFNO1lBQ0wsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDdkM7SUFDSCxDQUFDOzs7Ozs7OztJQUtELFdBQVcsQ0FBQyxLQUFvQjtRQUM5QixJQUFJLENBQUMsV0FBVyxDQUFDLG9CQUFvQixDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNyRCxDQUFDOzs7Ozs7OztJQUtELFdBQVcsQ0FBQyxJQUFZO1FBQ3RCLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNqQyxDQUFDOzs7Ozs7SUFFTyxZQUFZLENBQUMsS0FBYTs7Y0FDMUIsT0FBTyxHQUFHO1lBQ2QsSUFBSSxFQUFFLENBQUMsT0FBTyxFQUFFLE1BQU07Z0JBQ3BCLGdCQUFnQixFQUFFLGVBQWU7Z0JBQ2pDLHlCQUF5QixFQUFFLDhCQUE4QjtnQkFDekQsa0NBQWtDLEVBQUUsdUNBQXVDLENBQUM7U0FDL0U7UUFFRCxPQUFPLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUM5QixHQUFHOzs7O1FBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksRUFBRSxLQUFLLEVBQUUsT0FBTyxDQUFDLEVBQUMsRUFDbkUsR0FBRzs7OztRQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxFQUFDLENBQUMsQ0FBQztJQUNqRCxDQUFDOzs7Ozs7OztJQUVPLFdBQVcsQ0FBQyxZQUF3QixFQUFFLEtBQWEsRUFBRSxPQUEyQjs7Y0FDaEYsSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLFlBQVksRUFBRSxPQUFPLENBQUM7UUFDNUMsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzVCLENBQUM7Ozs7OztJQUVPLGlCQUFpQixDQUFDLEtBQWlCOztjQUVuQyxlQUFlOzs7O1FBQUcsQ0FBQyxLQUFlLEVBQUUsRUFBRTtZQUMxQyxPQUFPLG1CQUFBO2dCQUNMLEVBQUUsRUFBRSxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUU7Z0JBQ2hDLEtBQUssRUFBRSxLQUFLLENBQUMsS0FBSztnQkFDbEIsSUFBSSxFQUFFLEtBQUssQ0FBQyxJQUFJO2dCQUNoQixJQUFJLEVBQUUsS0FBSyxDQUFDLElBQUk7Z0JBQ2hCLElBQUksRUFBRSxLQUFLLENBQUMsSUFBSTthQUNqQixFQUFnQixDQUFDO1FBQ3BCLENBQUMsQ0FBQTs7Y0FFSyxnQkFBZ0I7Ozs7UUFBRyxDQUFDLEtBQWUsRUFBRSxFQUFFOztnQkFFdkMsU0FBdUM7WUFFM0MsSUFBSSxLQUFLLENBQUMsSUFBSSxFQUFFO2dCQUNkLFNBQVMsR0FBRyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDcEM7aUJBQU07Z0JBQ0wsU0FBUyxHQUFHLG1CQUFBO29CQUNWLEVBQUUsRUFBRSxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUU7b0JBQ2hDLEtBQUssRUFBRSxLQUFLLENBQUMsS0FBSztvQkFDbEIsSUFBSSxFQUFFLEtBQUssQ0FBQyxJQUFJO29CQUNoQixRQUFRLEVBQUUsSUFBSTtpQkFDZixFQUFpQixDQUFDO2dCQUVuQixJQUFJLEtBQUssQ0FBQyxRQUFRLElBQUksS0FBSyxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFOzswQkFFekMsVUFBVSxHQUFHLEtBQUssQ0FBQyxRQUFRO3lCQUM5QixNQUFNOzs7O29CQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxLQUFLLEtBQUssRUFBQzt5QkFDeEIsR0FBRzs7OztvQkFBQyxDQUFDLE1BQU0sRUFBRSxFQUFFO3dCQUNkLElBQUksTUFBTSxDQUFDLFFBQVEsSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7NEJBQ2pELE9BQU8sZ0JBQWdCLENBQUMsTUFBTSxDQUFDLENBQUM7eUJBQ2pDOzZCQUFNOzRCQUNMLE9BQU8sZUFBZSxDQUFDLE1BQU0sQ0FBQyxDQUFDO3lCQUNoQztvQkFDSCxDQUFDLEVBQUM7b0JBRUosSUFBSSxtQkFBQSxVQUFVLEVBQWtCLEVBQUU7d0JBQ2hDLFNBQVMsQ0FBQyxLQUFLLEdBQUcsbUJBQUEsVUFBVSxFQUFrQixDQUFDO3FCQUNoRDt5QkFBTTt3QkFDTCxTQUFTLENBQUMsS0FBSyxHQUFHLG1CQUFBLFVBQVUsRUFBbUIsQ0FBQztxQkFDakQ7aUJBQ0Y7YUFDRjtZQUVELE9BQU8sU0FBUyxDQUFDO1FBQ25CLENBQUMsQ0FBQTtRQUVELE9BQU8sbUJBQUE7WUFDTCxFQUFFLEVBQUUsSUFBSSxFQUFFO1lBQ1YsS0FBSyxFQUFFLE1BQU07WUFDYixRQUFRLEVBQUUsSUFBSTtZQUNkLEtBQUssRUFBRSxLQUFLLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDO1NBQ25DLEVBQWlCLENBQUM7SUFDckIsQ0FBQzs7O1lBOUdGLFVBQVU7Ozs7WUFMSCxhQUFhO1lBUGIsV0FBVztZQUNYLFdBQVc7Ozs7Ozs7SUFjakIseUNBQXdGOzs7OztJQUN4Riw0Q0FBa0Y7Ozs7O0lBRXRFLDBDQUFzQzs7Ozs7SUFDdEMsd0NBQWdDOzs7OztJQUNoQyx3Q0FBZ0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0luamVjdGFibGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtHcnVwb1Blc3F1aXNhLCBJdGVtUGVzcXVpc2F9IGZyb20gJy4uL2xheW91dC9oZWFkZXIvc2VhcmNoL3NlYXJjaC5tb2RlbCc7XG5pbXBvcnQge1NlYXJjaFF1ZXJ5fSBmcm9tICcuLi9sYXlvdXQvaGVhZGVyL3NlYXJjaC9zdGF0ZS9zZWFyY2gucXVlcnknO1xuaW1wb3J0IHtTZWFyY2hTdG9yZX0gZnJvbSAnLi4vbGF5b3V0L2hlYWRlci9zZWFyY2gvc3RhdGUvc2VhcmNoLnN0b3JlJztcbmltcG9ydCAqIGFzIF8gZnJvbSAnbG9kYXNoJztcbmltcG9ydCB7U2VhcmNoQWJzdHJhY3RTZXJ2aWNlfSBmcm9tICcuL2Fic3RyYWN0L3NlYXJjaC1hYnN0cmFjdC5zZXJ2aWNlJztcbmltcG9ydCB7T2JzZXJ2YWJsZX0gZnJvbSAncnhqcyc7XG5pbXBvcnQge21hcH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHtNZW51SXRlbX0gZnJvbSAnLi4vbGF5b3V0L25hdi9tZW51L21lbnUtaXRlbS9tZW51LWl0ZW0ubW9kZWwnO1xuaW1wb3J0IHtMYXlvdXRTZXJ2aWNlfSBmcm9tICcuLi9sYXlvdXQvbGF5b3V0LnNlcnZpY2UnO1xuaW1wb3J0IHtndWlkfSBmcm9tICdAZGF0b3JhbWEvYWtpdGEnO1xuaW1wb3J0ICogYXMgRnVzZSBmcm9tICdmdXNlLmpzJztcblxuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgU2VhcmNoS2VlcFNlcnZpY2UgaW1wbGVtZW50cyBTZWFyY2hBYnN0cmFjdFNlcnZpY2Uge1xuXG4gIHByb3RlY3RlZCBzZWFyY2hHcm91cHM6IE9ic2VydmFibGU8TWVudUl0ZW1bXT4gPSB0aGlzLmxheW91dFNlcnZpY2UubWVudUl0ZW1zRmxhdHRlbmVkJDtcbiAgcHJvdGVjdGVkIHNlYXJjaEdyb3Vwc0FsbDogT2JzZXJ2YWJsZTxNZW51SXRlbVtdPiA9IHRoaXMubGF5b3V0U2VydmljZS5tZW51SXRlbXMkO1xuXG4gIGNvbnN0cnVjdG9yKHByb3RlY3RlZCBsYXlvdXRTZXJ2aWNlOiBMYXlvdXRTZXJ2aWNlLFxuICAgICAgICAgICAgICBwcml2YXRlIHNlYXJjaFF1ZXJ5OiBTZWFyY2hRdWVyeSxcbiAgICAgICAgICAgICAgcHJpdmF0ZSBzZWFyY2hTdG9yZTogU2VhcmNoU3RvcmUpIHtcbiAgfVxuXG4gIC8qXG4qIE3DqXRvZG8gcmVzcG9uc8OhdmVsIHBvciBhZGljaW9uYXIgdW0gbm92byBncnVwbyBuYSBwZXNxdWlzYXMuXG4qICovXG4gIGFkZEdyb3VwKGdyb3VwczogR3J1cG9QZXNxdWlzYVtdKSB7XG4gICAgY29uc3QgaXRlbnNHcm91cHMgPSB0aGlzLnNlYXJjaFF1ZXJ5LmdldEFsbCgpO1xuICAgIGlmIChfLmhlYWQoaXRlbnNHcm91cHMpICE9PSB1bmRlZmluZWQpIHtcbiAgICAgIHRoaXMuc2VhcmNoU3RvcmUuYWRpY2lvbmFyR3J1cG9QZXNxdWlzYShpdGVuc0dyb3VwcywgZ3JvdXBzKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5zZWFyY2hTdG9yZS5hZGRHcm91cEJhc2UoZ3JvdXBzKTtcbiAgICB9XG4gIH1cblxuICAvKlxuICAgICogTcOpdG9kbyByZXNwb25zw6F2ZWwgcG9yIHJlbW92ZXIgdW0gZ3J1cG8gZGEgcGVzcXVpc2EuXG4gICAgKiAqL1xuICByZW1vdmVHcm91cChncm91cDogR3J1cG9QZXNxdWlzYSkge1xuICAgIHRoaXMuc2VhcmNoU3RvcmUucmVtb3ZlckdydXBvUGVzcXVpc2EoZ3JvdXAudGl0bGUpO1xuICB9XG5cbiAgLypcbiAgICogTcOpdG9kbyByZXNwb25zw6F2ZWwgcG9yIHJlYWxpemFyIGEgcGVzcXVpc2EgZG9zIGdydXBvIG5hIHBlc3F1aXNhLlxuICAgKiAqL1xuICBzZWFyY2hHcm91cCh0ZXJtOiBzdHJpbmcpOiBPYnNlcnZhYmxlPEdydXBvUGVzcXVpc2EgfCBJdGVtUGVzcXVpc2E+IHtcbiAgICByZXR1cm4gdGhpcy5fZmlsdGVyR3JvdXAodGVybSk7XG4gIH1cblxuICBwcml2YXRlIF9maWx0ZXJHcm91cCh2YWx1ZTogc3RyaW5nKTogT2JzZXJ2YWJsZTxHcnVwb1Blc3F1aXNhIHwgSXRlbVBlc3F1aXNhPiB7XG4gICAgY29uc3Qgb3B0aW9ucyA9IHtcbiAgICAgIGtleXM6IFsndGl0bGUnLCAndGFncycsXG4gICAgICAgICdjaGlsZHJlbi50aXRsZScsICdjaGlsZHJlbi50YWdzJyxcbiAgICAgICAgJ2NoaWxkcmVuLmNoaWxkcmVuLnRpdGxlJywgJ2NoaWxkcmVuLmNoaWxkcmVuLnRpdGxlLnRhZ3MnLFxuICAgICAgICAnY2hpbGRyZW4uY2hpbGRyZW4uY2hpbGRyZW4udGl0bGUnLCAnY2hpbGRyZW4uY2hpbGRyZW4uY2hpbGRyZW4udGl0bGUudGFncyddXG4gICAgfTtcblxuICAgIHJldHVybiB0aGlzLnNlYXJjaEdyb3Vwc0FsbC5waXBlKFxuICAgICAgbWFwKGxpc3RNZW51SXRlbSA9PiB0aGlzLmFwcGx5RmlsdGVyKGxpc3RNZW51SXRlbSwgdmFsdWUsIG9wdGlvbnMpKSxcbiAgICAgIG1hcChpdGVucyA9PiB0aGlzLmdyb3VwUmV0dXJuRmlsdGVyKGl0ZW5zKSkpO1xuICB9XG5cbiAgcHJpdmF0ZSBhcHBseUZpbHRlcihsaXN0TWVudUl0ZW06IE1lbnVJdGVtW10sIHZhbHVlOiBzdHJpbmcsIG9wdGlvbnM6IHsga2V5czogc3RyaW5nW10gfSkge1xuICAgIGNvbnN0IGZ1c2UgPSBuZXcgRnVzZShsaXN0TWVudUl0ZW0sIG9wdGlvbnMpO1xuICAgIHJldHVybiBmdXNlLnNlYXJjaCh2YWx1ZSk7XG4gIH1cblxuICBwcml2YXRlIGdyb3VwUmV0dXJuRmlsdGVyKGl0ZW5zOiBNZW51SXRlbVtdKTogR3J1cG9QZXNxdWlzYSB8IEl0ZW1QZXNxdWlzYSB7XG5cbiAgICBjb25zdCBmdW5jdGlvbk1hcEl0ZW0gPSAoZ3JvdXA6IE1lbnVJdGVtKSA9PiB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICBpZDogZ3JvdXAuaWQgPyBncm91cC5pZCA6IGd1aWQoKSxcbiAgICAgICAgdGl0bGU6IGdyb3VwLnRpdGxlLFxuICAgICAgICBpY29uOiBncm91cC5pY29uLFxuICAgICAgICBsaW5rOiBncm91cC5saW5rLFxuICAgICAgICB0YWdzOiBncm91cC50YWdzXG4gICAgICB9IGFzIEl0ZW1QZXNxdWlzYTtcbiAgICB9O1xuXG4gICAgY29uc3QgZnVuY3Rpb25NYXBHcm91cCA9IChncm91cDogTWVudUl0ZW0pID0+IHtcblxuICAgICAgbGV0IGdyb3VwTWVudTogR3J1cG9QZXNxdWlzYSB8IEl0ZW1QZXNxdWlzYTtcblxuICAgICAgaWYgKGdyb3VwLmxpbmspIHtcbiAgICAgICAgZ3JvdXBNZW51ID0gZnVuY3Rpb25NYXBJdGVtKGdyb3VwKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGdyb3VwTWVudSA9IHtcbiAgICAgICAgICBpZDogZ3JvdXAuaWQgPyBncm91cC5pZCA6IGd1aWQoKSxcbiAgICAgICAgICB0aXRsZTogZ3JvdXAudGl0bGUsXG4gICAgICAgICAgaWNvbjogZ3JvdXAuaWNvbixcbiAgICAgICAgICBpc0hlYWRlcjogdHJ1ZSxcbiAgICAgICAgfSBhcyBHcnVwb1Blc3F1aXNhO1xuXG4gICAgICAgIGlmIChncm91cC5jaGlsZHJlbiAmJiBncm91cC5jaGlsZHJlbi5sZW5ndGggPiAxKSB7XG5cbiAgICAgICAgICBjb25zdCBpdGVtRmlsaG9zID0gZ3JvdXAuY2hpbGRyZW5cbiAgICAgICAgICAgIC5maWx0ZXIoYiA9PiBiICE9PSBncm91cClcbiAgICAgICAgICAgIC5tYXAoKGl0ZW1JdCkgPT4ge1xuICAgICAgICAgICAgICBpZiAoaXRlbUl0LmNoaWxkcmVuICYmIGl0ZW1JdC5jaGlsZHJlbi5sZW5ndGggPiAxKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZ1bmN0aW9uTWFwR3JvdXAoaXRlbUl0KTtcbiAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gZnVuY3Rpb25NYXBJdGVtKGl0ZW1JdCk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgaWYgKGl0ZW1GaWxob3MgYXMgSXRlbVBlc3F1aXNhW10pIHtcbiAgICAgICAgICAgIGdyb3VwTWVudS5pdGVtcyA9IGl0ZW1GaWxob3MgYXMgSXRlbVBlc3F1aXNhW107XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGdyb3VwTWVudS5pdGVtcyA9IGl0ZW1GaWxob3MgYXMgR3J1cG9QZXNxdWlzYVtdO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICByZXR1cm4gZ3JvdXBNZW51O1xuICAgIH07XG5cbiAgICByZXR1cm4ge1xuICAgICAgaWQ6IGd1aWQoKSxcbiAgICAgIHRpdGxlOiAnTWVudScsXG4gICAgICBpc0hlYWRlcjogdHJ1ZSxcbiAgICAgIGl0ZW1zOiBpdGVucy5tYXAoZnVuY3Rpb25NYXBHcm91cClcbiAgICB9IGFzIEdydXBvUGVzcXVpc2E7XG4gIH1cbn1cbiJdfQ==