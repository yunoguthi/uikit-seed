/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { RouteReuseStrategy } from '@angular/router';
export class NoRouteReuseStrategy extends RouteReuseStrategy {
    /**
     * @param {?} route
     * @return {?}
     */
    shouldDetach(route) {
        return false;
    }
    /**
     * @param {?} route
     * @param {?} handle
     * @return {?}
     */
    store(route, handle) {
    }
    /**
     * @param {?} route
     * @return {?}
     */
    shouldAttach(route) {
        return false;
    }
    /**
     * @param {?} route
     * @return {?}
     */
    retrieve(route) {
        return null;
    }
    /**
     * @param {?} future
     * @param {?} curr
     * @return {?}
     */
    shouldReuseRoute(future, curr) {
        return false; // default is true if configuration of current and future route are the same
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicm91dGUtcmV1c2Utc3RyYXRlZ3kuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL3V0aWxzL3JvdXRlLXJldXNlLXN0cmF0ZWd5LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsa0JBQWtCLEVBQTBCLE1BQU0saUJBQWlCLENBQUM7QUFFN0UsTUFBTSxPQUFPLG9CQUFxQixTQUFRLGtCQUFrQjs7Ozs7SUFDMUQsWUFBWSxDQUFDLEtBQTZCO1FBQ3hDLE9BQU8sS0FBSyxDQUFDO0lBQ2YsQ0FBQzs7Ozs7O0lBQ0QsS0FBSyxDQUFDLEtBQTZCLEVBQUUsTUFBVTtJQUUvQyxDQUFDOzs7OztJQUNELFlBQVksQ0FBQyxLQUE2QjtRQUN4QyxPQUFPLEtBQUssQ0FBQztJQUNmLENBQUM7Ozs7O0lBQ0QsUUFBUSxDQUFDLEtBQTZCO1FBQ3BDLE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQzs7Ozs7O0lBQ0QsZ0JBQWdCLENBQUMsTUFBOEIsRUFBRSxJQUE0QjtRQUMzRSxPQUFPLEtBQUssQ0FBQyxDQUFDLDRFQUE0RTtJQUM1RixDQUFDO0NBQ0YiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBSb3V0ZVJldXNlU3RyYXRlZ3ksIEFjdGl2YXRlZFJvdXRlU25hcHNob3QgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuXG5leHBvcnQgY2xhc3MgTm9Sb3V0ZVJldXNlU3RyYXRlZ3kgZXh0ZW5kcyBSb3V0ZVJldXNlU3RyYXRlZ3kge1xuICBzaG91bGREZXRhY2gocm91dGU6IEFjdGl2YXRlZFJvdXRlU25hcHNob3QpOiBib29sZWFuIHtcbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cbiAgc3RvcmUocm91dGU6IEFjdGl2YXRlZFJvdXRlU25hcHNob3QsIGhhbmRsZToge30pOiB2b2lkIHtcblxuICB9XG4gIHNob3VsZEF0dGFjaChyb3V0ZTogQWN0aXZhdGVkUm91dGVTbmFwc2hvdCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuICByZXRyaWV2ZShyb3V0ZTogQWN0aXZhdGVkUm91dGVTbmFwc2hvdCk6IHt9IHtcbiAgICByZXR1cm4gbnVsbDtcbiAgfVxuICBzaG91bGRSZXVzZVJvdXRlKGZ1dHVyZTogQWN0aXZhdGVkUm91dGVTbmFwc2hvdCwgY3VycjogQWN0aXZhdGVkUm91dGVTbmFwc2hvdCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiBmYWxzZTsgLy8gZGVmYXVsdCBpcyB0cnVlIGlmIGNvbmZpZ3VyYXRpb24gb2YgY3VycmVudCBhbmQgZnV0dXJlIHJvdXRlIGFyZSB0aGUgc2FtZVxuICB9XG59XG4iXX0=