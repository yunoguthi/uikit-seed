/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, InjectionToken } from '@angular/core';
/** @type {?} */
export const SEARCH_TOKEN = new InjectionToken('SearchAbstractService');
/**
 * @abstract
 */
export class SearchAbstractService {
    /**
     * @protected
     */
    constructor() {
    }
}
SearchAbstractService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
SearchAbstractService.ctorParameters = () => [];
if (false) {
    /**
     * @abstract
     * @param {?} term
     * @return {?}
     */
    SearchAbstractService.prototype.searchGroup = function (term) { };
    /**
     * @abstract
     * @param {?} group
     * @return {?}
     */
    SearchAbstractService.prototype.addGroup = function (group) { };
    /**
     * @abstract
     * @param {?} group
     * @return {?}
     */
    SearchAbstractService.prototype.removeGroup = function (group) { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLWFic3RyYWN0LnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL3V0aWxzL2Fic3RyYWN0L3NlYXJjaC1hYnN0cmFjdC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsVUFBVSxFQUFFLGNBQWMsRUFBQyxNQUFNLGVBQWUsQ0FBQzs7QUFJekQsTUFBTSxPQUFPLFlBQVksR0FBRyxJQUFJLGNBQWMsQ0FBd0IsdUJBQXVCLENBQUM7Ozs7QUFHOUYsTUFBTSxPQUFnQixxQkFBcUI7Ozs7SUFFekM7SUFDQSxDQUFDOzs7WUFKRixVQUFVOzs7Ozs7Ozs7O0lBTVQsa0VBQTZFOzs7Ozs7SUFFN0UsZ0VBQTBDOzs7Ozs7SUFFMUMsbUVBQTJDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtJbmplY3RhYmxlLCBJbmplY3Rpb25Ub2tlbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0dydXBvUGVzcXVpc2EsIEl0ZW1QZXNxdWlzYX0gZnJvbSAnLi4vLi4vbGF5b3V0L2hlYWRlci9zZWFyY2gvc2VhcmNoLm1vZGVsJztcbmltcG9ydCB7T2JzZXJ2YWJsZX0gZnJvbSAncnhqcyc7XG5cbmV4cG9ydCBjb25zdCBTRUFSQ0hfVE9LRU4gPSBuZXcgSW5qZWN0aW9uVG9rZW48U2VhcmNoQWJzdHJhY3RTZXJ2aWNlPignU2VhcmNoQWJzdHJhY3RTZXJ2aWNlJyk7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBTZWFyY2hBYnN0cmFjdFNlcnZpY2Uge1xuXG4gIHByb3RlY3RlZCBjb25zdHJ1Y3RvcigpIHtcbiAgfVxuXG4gIGFic3RyYWN0IHNlYXJjaEdyb3VwKHRlcm06IHN0cmluZyk6IE9ic2VydmFibGU8R3J1cG9QZXNxdWlzYSB8IEl0ZW1QZXNxdWlzYT47XG5cbiAgYWJzdHJhY3QgYWRkR3JvdXAoZ3JvdXA6IEdydXBvUGVzcXVpc2FbXSk7XG5cbiAgYWJzdHJhY3QgcmVtb3ZlR3JvdXAoZ3JvdXA6IEdydXBvUGVzcXVpc2EpO1xufVxuIl19