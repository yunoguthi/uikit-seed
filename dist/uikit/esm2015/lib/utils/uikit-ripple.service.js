/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
export class UikitRrippleService {
    constructor() {
    }
    /**
     * @return {?}
     */
    init() {
        document.addEventListener('mousedown', this.rippleMouseDown, false);
    }
    /**
     * @param {?} e
     * @return {?}
     */
    rippleMouseDown(e) {
        /** @type {?} */
        const parentNode = 'parentNode';
        /** @type {?} */
        const isVisible = (/**
         * @param {?} el
         * @return {?}
         */
        (el) => {
            return !!(el.offsetWidth || el.offsetHeight);
        });
        /** @type {?} */
        const selectorMatches = (/**
         * @param {?} el
         * @param {?} selector
         * @return {?}
         */
        (el, selector) => {
            /** @type {?} */
            const matches = 'matches';
            /** @type {?} */
            const webkitMatchesSelector = 'webkitMatchesSelector';
            /** @type {?} */
            const mozMatchesSelector = 'mozMatchesSelector';
            /** @type {?} */
            const msMatchesSelector = 'msMatchesSelector';
            /** @type {?} */
            const p = Element.prototype;
            /** @type {?} */
            const f = p[matches] || p[webkitMatchesSelector] || p[mozMatchesSelector] || p[msMatchesSelector] || (/**
             * @param {?} s
             * @return {?}
             */
            function (s) {
                return [].indexOf.call(document.querySelectorAll(s), this) !== -1;
            });
            return f.call(el, selector);
        });
        /** @type {?} */
        const addClass = (/**
         * @param {?} element
         * @param {?} className
         * @return {?}
         */
        (element, className) => {
            if (element.classList) {
                element.classList.add(className);
            }
            else {
                element.className += ' ' + className;
            }
        });
        /** @type {?} */
        const hasClass = (/**
         * @param {?} element
         * @param {?} className
         * @return {?}
         */
        (element, className) => {
            if (element.classList) {
                return element.classList.contains(className);
            }
            else {
                return new RegExp('(^| )' + className + '( |$)', 'gi').test(element.className);
            }
        });
        /** @type {?} */
        const removeClass = (/**
         * @param {?} element
         * @param {?} className
         * @return {?}
         */
        (element, className) => {
            if (element.classList) {
                element.classList.remove(className);
            }
            else {
                element.className = element.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
            }
        });
        /** @type {?} */
        const getOffset = (/**
         * @param {?} el
         * @return {?}
         */
        (el) => {
            /** @type {?} */
            const rect = el.getBoundingClientRect();
            return {
                top: rect.top + (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0),
                left: rect.left + (window.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft || 0),
            };
        });
        /** @type {?} */
        const rippleEffect = (/**
         * @param {?} element
         * @param {?} e
         * @return {?}
         */
        (element, e) => {
            if (element.querySelector('.ink') === null) {
                /** @type {?} */
                const inkEl = document.createElement('span');
                addClass(inkEl, 'ink');
                if (hasClass(element, 'ripplelink') && element.querySelector('span')) {
                    element.querySelector('span').insertAdjacentHTML('afterend', '<span class=\'ink\'></span>');
                }
                else {
                    element.appendChild(inkEl);
                }
            }
            /** @type {?} */
            const ink = element.querySelector('.ink');
            removeClass(ink, 'ripple-animate');
            if (!ink.offsetHeight && !ink.offsetWidth) {
                /** @type {?} */
                const d = Math.max(element.offsetWidth, element.offsetHeight);
                ink.style.height = d + 'px';
                ink.style.width = d + 'px';
            }
            /** @type {?} */
            const x = e.pageX - getOffset(element).left - (ink.offsetWidth / 2);
            /** @type {?} */
            const y = e.pageY - getOffset(element).top - (ink.offsetHeight / 2);
            ink.style.top = y + 'px';
            ink.style.left = x + 'px';
            ink.style.pointerEvents = 'none';
            addClass(ink, 'ripple-animate');
        });
        for (let target = e.target; target && target !== this; target = target[parentNode]) {
            if (!isVisible(target)) {
                continue;
            }
            if (selectorMatches(target, '.ripplelink, .ui-button, .ui-listbox-item, .ui-multiselect-item, .ui-fieldset-toggler')) {
                /** @type {?} */
                const element = target;
                rippleEffect(element, e);
                break;
            }
        }
    }
}
UikitRrippleService.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */
UikitRrippleService.ctorParameters = () => [];
/** @nocollapse */ UikitRrippleService.ngInjectableDef = i0.defineInjectable({ factory: function UikitRrippleService_Factory() { return new UikitRrippleService(); }, token: UikitRrippleService, providedIn: "root" });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWlraXQtcmlwcGxlLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL3V0aWxzL3Vpa2l0LXJpcHBsZS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsVUFBVSxFQUFTLE1BQU0sZUFBZSxDQUFDOztBQUdqRCxNQUFNLE9BQU8sbUJBQW1CO0lBQzlCO0lBQ0EsQ0FBQzs7OztJQUVELElBQUk7UUFDRixRQUFRLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxlQUFlLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDdEUsQ0FBQzs7Ozs7SUFFRCxlQUFlLENBQUMsQ0FBQzs7Y0FDVCxVQUFVLEdBQUcsWUFBWTs7Y0FFekIsU0FBUzs7OztRQUFHLENBQUMsRUFBRSxFQUFFLEVBQUU7WUFDdkIsT0FBTyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsV0FBVyxJQUFJLEVBQUUsQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUMvQyxDQUFDLENBQUE7O2NBRUssZUFBZTs7Ozs7UUFBRyxDQUFDLEVBQUUsRUFBRSxRQUFRLEVBQUUsRUFBRTs7a0JBQ2pDLE9BQU8sR0FBRyxTQUFTOztrQkFDbkIscUJBQXFCLEdBQUcsdUJBQXVCOztrQkFDL0Msa0JBQWtCLEdBQUcsb0JBQW9COztrQkFDekMsaUJBQWlCLEdBQUcsbUJBQW1COztrQkFDdkMsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxTQUFTOztrQkFDckIsQ0FBQyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLENBQUMsaUJBQWlCLENBQUM7Ozs7WUFBSSxVQUFVLENBQUM7Z0JBQzlHLE9BQU8sRUFBRSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ3BFLENBQUMsQ0FBQTtZQUNELE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFDOUIsQ0FBQyxDQUFBOztjQUVLLFFBQVE7Ozs7O1FBQUcsQ0FBQyxPQUFPLEVBQUUsU0FBUyxFQUFFLEVBQUU7WUFDdEMsSUFBSSxPQUFPLENBQUMsU0FBUyxFQUFFO2dCQUNyQixPQUFPLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUNsQztpQkFBTTtnQkFDTCxPQUFPLENBQUMsU0FBUyxJQUFJLEdBQUcsR0FBRyxTQUFTLENBQUM7YUFDdEM7UUFDSCxDQUFDLENBQUE7O2NBRUssUUFBUTs7Ozs7UUFBRyxDQUFDLE9BQU8sRUFBRSxTQUFTLEVBQUUsRUFBRTtZQUN0QyxJQUFJLE9BQU8sQ0FBQyxTQUFTLEVBQUU7Z0JBQ3JCLE9BQU8sT0FBTyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUM7YUFDOUM7aUJBQU07Z0JBQ0wsT0FBTyxJQUFJLE1BQU0sQ0FBQyxPQUFPLEdBQUcsU0FBUyxHQUFHLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDO2FBQ2hGO1FBQ0gsQ0FBQyxDQUFBOztjQUVLLFdBQVc7Ozs7O1FBQUcsQ0FBQyxPQUFPLEVBQUUsU0FBUyxFQUFFLEVBQUU7WUFDekMsSUFBSSxPQUFPLENBQUMsU0FBUyxFQUFFO2dCQUNyQixPQUFPLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUNyQztpQkFBTTtnQkFDTCxPQUFPLENBQUMsU0FBUyxHQUFHLE9BQU8sQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLElBQUksTUFBTSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxTQUFTLEVBQUUsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7YUFDOUg7UUFDSCxDQUFDLENBQUE7O2NBRUssU0FBUzs7OztRQUFHLENBQUMsRUFBRSxFQUFFLEVBQUU7O2tCQUNqQixJQUFJLEdBQUcsRUFBRSxDQUFDLHFCQUFxQixFQUFFO1lBRXZDLE9BQU87Z0JBQ0wsR0FBRyxFQUFFLElBQUksQ0FBQyxHQUFHLEdBQUcsQ0FBQyxNQUFNLENBQUMsV0FBVyxJQUFJLFFBQVEsQ0FBQyxlQUFlLENBQUMsU0FBUyxJQUFJLFFBQVEsQ0FBQyxJQUFJLENBQUMsU0FBUyxJQUFJLENBQUMsQ0FBQztnQkFDMUcsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxNQUFNLENBQUMsV0FBVyxJQUFJLFFBQVEsQ0FBQyxlQUFlLENBQUMsVUFBVSxJQUFJLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBVSxJQUFJLENBQUMsQ0FBQzthQUMvRyxDQUFDO1FBQ0osQ0FBQyxDQUFBOztjQUVLLFlBQVk7Ozs7O1FBQUcsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDbEMsSUFBSSxPQUFPLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxLQUFLLElBQUksRUFBRTs7c0JBQ3BDLEtBQUssR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQztnQkFDNUMsUUFBUSxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztnQkFFdkIsSUFBSSxRQUFRLENBQUMsT0FBTyxFQUFFLFlBQVksQ0FBQyxJQUFJLE9BQU8sQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLEVBQUU7b0JBQ3BFLE9BQU8sQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUMsa0JBQWtCLENBQUMsVUFBVSxFQUFFLDZCQUE2QixDQUFDLENBQUM7aUJBQzdGO3FCQUFNO29CQUNMLE9BQU8sQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQzVCO2FBQ0Y7O2tCQUVLLEdBQUcsR0FBRyxPQUFPLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQztZQUN6QyxXQUFXLENBQUMsR0FBRyxFQUFFLGdCQUFnQixDQUFDLENBQUM7WUFFbkMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxZQUFZLElBQUksQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFFOztzQkFDbkMsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxPQUFPLENBQUMsWUFBWSxDQUFDO2dCQUM3RCxHQUFHLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDO2dCQUM1QixHQUFHLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDO2FBQzVCOztrQkFFSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssR0FBRyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxHQUFHLENBQUMsR0FBRyxDQUFDLFdBQVcsR0FBRyxDQUFDLENBQUM7O2tCQUM3RCxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssR0FBRyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUMsR0FBRyxDQUFDLFlBQVksR0FBRyxDQUFDLENBQUM7WUFFbkUsR0FBRyxDQUFDLEtBQUssQ0FBQyxHQUFHLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQztZQUN6QixHQUFHLENBQUMsS0FBSyxDQUFDLElBQUksR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDO1lBQzFCLEdBQUcsQ0FBQyxLQUFLLENBQUMsYUFBYSxHQUFHLE1BQU0sQ0FBQztZQUNqQyxRQUFRLENBQUMsR0FBRyxFQUFFLGdCQUFnQixDQUFDLENBQUM7UUFDbEMsQ0FBQyxDQUFBO1FBRUQsS0FBSyxJQUFJLE1BQU0sR0FBRyxDQUFDLENBQUMsTUFBTSxFQUFFLE1BQU0sSUFBSSxNQUFNLEtBQUssSUFBSSxFQUFFLE1BQU0sR0FBRyxNQUFNLENBQUMsVUFBVSxDQUFDLEVBQUU7WUFDbEYsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsRUFBRTtnQkFDdEIsU0FBUzthQUNWO1lBRUQsSUFBSSxlQUFlLENBQUMsTUFBTSxFQUFFLHVGQUF1RixDQUFDLEVBQUU7O3NCQUM5RyxPQUFPLEdBQUcsTUFBTTtnQkFDdEIsWUFBWSxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDekIsTUFBTTthQUNQO1NBQ0Y7SUFDSCxDQUFDOzs7WUF0R0YsVUFBVSxTQUFDLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SW5qZWN0YWJsZSwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQEluamVjdGFibGUoeyBwcm92aWRlZEluOiAncm9vdCcgfSlcbmV4cG9ydCBjbGFzcyBVaWtpdFJyaXBwbGVTZXJ2aWNlIHtcbiAgY29uc3RydWN0b3IoKSB7XG4gIH1cblxuICBpbml0KCkge1xuICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ21vdXNlZG93bicsIHRoaXMucmlwcGxlTW91c2VEb3duLCBmYWxzZSk7XG4gIH1cblxuICByaXBwbGVNb3VzZURvd24oZSkge1xuICAgIGNvbnN0IHBhcmVudE5vZGUgPSAncGFyZW50Tm9kZSc7XG5cbiAgICBjb25zdCBpc1Zpc2libGUgPSAoZWwpID0+IHtcbiAgICAgIHJldHVybiAhIShlbC5vZmZzZXRXaWR0aCB8fCBlbC5vZmZzZXRIZWlnaHQpO1xuICAgIH1cblxuICAgIGNvbnN0IHNlbGVjdG9yTWF0Y2hlcyA9IChlbCwgc2VsZWN0b3IpID0+IHtcbiAgICAgIGNvbnN0IG1hdGNoZXMgPSAnbWF0Y2hlcyc7XG4gICAgICBjb25zdCB3ZWJraXRNYXRjaGVzU2VsZWN0b3IgPSAnd2Via2l0TWF0Y2hlc1NlbGVjdG9yJztcbiAgICAgIGNvbnN0IG1vek1hdGNoZXNTZWxlY3RvciA9ICdtb3pNYXRjaGVzU2VsZWN0b3InO1xuICAgICAgY29uc3QgbXNNYXRjaGVzU2VsZWN0b3IgPSAnbXNNYXRjaGVzU2VsZWN0b3InO1xuICAgICAgY29uc3QgcCA9IEVsZW1lbnQucHJvdG90eXBlO1xuICAgICAgY29uc3QgZiA9IHBbbWF0Y2hlc10gfHwgcFt3ZWJraXRNYXRjaGVzU2VsZWN0b3JdIHx8IHBbbW96TWF0Y2hlc1NlbGVjdG9yXSB8fCBwW21zTWF0Y2hlc1NlbGVjdG9yXSB8fCBmdW5jdGlvbiAocykge1xuICAgICAgICByZXR1cm4gW10uaW5kZXhPZi5jYWxsKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwocyksIHRoaXMpICE9PSAtMTtcbiAgICAgIH07XG4gICAgICByZXR1cm4gZi5jYWxsKGVsLCBzZWxlY3Rvcik7XG4gICAgfVxuXG4gICAgY29uc3QgYWRkQ2xhc3MgPSAoZWxlbWVudCwgY2xhc3NOYW1lKSA9PiB7XG4gICAgICBpZiAoZWxlbWVudC5jbGFzc0xpc3QpIHtcbiAgICAgICAgZWxlbWVudC5jbGFzc0xpc3QuYWRkKGNsYXNzTmFtZSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBlbGVtZW50LmNsYXNzTmFtZSArPSAnICcgKyBjbGFzc05hbWU7XG4gICAgICB9XG4gICAgfVxuXG4gICAgY29uc3QgaGFzQ2xhc3MgPSAoZWxlbWVudCwgY2xhc3NOYW1lKSA9PiB7XG4gICAgICBpZiAoZWxlbWVudC5jbGFzc0xpc3QpIHtcbiAgICAgICAgcmV0dXJuIGVsZW1lbnQuY2xhc3NMaXN0LmNvbnRhaW5zKGNsYXNzTmFtZSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICByZXR1cm4gbmV3IFJlZ0V4cCgnKF58ICknICsgY2xhc3NOYW1lICsgJyggfCQpJywgJ2dpJykudGVzdChlbGVtZW50LmNsYXNzTmFtZSk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgY29uc3QgcmVtb3ZlQ2xhc3MgPSAoZWxlbWVudCwgY2xhc3NOYW1lKSA9PiB7XG4gICAgICBpZiAoZWxlbWVudC5jbGFzc0xpc3QpIHtcbiAgICAgICAgZWxlbWVudC5jbGFzc0xpc3QucmVtb3ZlKGNsYXNzTmFtZSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBlbGVtZW50LmNsYXNzTmFtZSA9IGVsZW1lbnQuY2xhc3NOYW1lLnJlcGxhY2UobmV3IFJlZ0V4cCgnKF58XFxcXGIpJyArIGNsYXNzTmFtZS5zcGxpdCgnICcpLmpvaW4oJ3wnKSArICcoXFxcXGJ8JCknLCAnZ2knKSwgJyAnKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBjb25zdCBnZXRPZmZzZXQgPSAoZWwpID0+IHtcbiAgICAgIGNvbnN0IHJlY3QgPSBlbC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKTtcblxuICAgICAgcmV0dXJuIHtcbiAgICAgICAgdG9wOiByZWN0LnRvcCArICh3aW5kb3cucGFnZVlPZmZzZXQgfHwgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnNjcm9sbFRvcCB8fCBkb2N1bWVudC5ib2R5LnNjcm9sbFRvcCB8fCAwKSxcbiAgICAgICAgbGVmdDogcmVjdC5sZWZ0ICsgKHdpbmRvdy5wYWdlWE9mZnNldCB8fCBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsTGVmdCB8fCBkb2N1bWVudC5ib2R5LnNjcm9sbExlZnQgfHwgMCksXG4gICAgICB9O1xuICAgIH1cblxuICAgIGNvbnN0IHJpcHBsZUVmZmVjdCA9IChlbGVtZW50LCBlKSA9PiB7XG4gICAgICBpZiAoZWxlbWVudC5xdWVyeVNlbGVjdG9yKCcuaW5rJykgPT09IG51bGwpIHtcbiAgICAgICAgY29uc3QgaW5rRWwgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzcGFuJyk7XG4gICAgICAgIGFkZENsYXNzKGlua0VsLCAnaW5rJyk7XG5cbiAgICAgICAgaWYgKGhhc0NsYXNzKGVsZW1lbnQsICdyaXBwbGVsaW5rJykgJiYgZWxlbWVudC5xdWVyeVNlbGVjdG9yKCdzcGFuJykpIHtcbiAgICAgICAgICBlbGVtZW50LnF1ZXJ5U2VsZWN0b3IoJ3NwYW4nKS5pbnNlcnRBZGphY2VudEhUTUwoJ2FmdGVyZW5kJywgJzxzcGFuIGNsYXNzPVxcJ2lua1xcJz48L3NwYW4+Jyk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgZWxlbWVudC5hcHBlbmRDaGlsZChpbmtFbCk7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgY29uc3QgaW5rID0gZWxlbWVudC5xdWVyeVNlbGVjdG9yKCcuaW5rJyk7XG4gICAgICByZW1vdmVDbGFzcyhpbmssICdyaXBwbGUtYW5pbWF0ZScpO1xuXG4gICAgICBpZiAoIWluay5vZmZzZXRIZWlnaHQgJiYgIWluay5vZmZzZXRXaWR0aCkge1xuICAgICAgICBjb25zdCBkID0gTWF0aC5tYXgoZWxlbWVudC5vZmZzZXRXaWR0aCwgZWxlbWVudC5vZmZzZXRIZWlnaHQpO1xuICAgICAgICBpbmsuc3R5bGUuaGVpZ2h0ID0gZCArICdweCc7XG4gICAgICAgIGluay5zdHlsZS53aWR0aCA9IGQgKyAncHgnO1xuICAgICAgfVxuXG4gICAgICBjb25zdCB4ID0gZS5wYWdlWCAtIGdldE9mZnNldChlbGVtZW50KS5sZWZ0IC0gKGluay5vZmZzZXRXaWR0aCAvIDIpO1xuICAgICAgY29uc3QgeSA9IGUucGFnZVkgLSBnZXRPZmZzZXQoZWxlbWVudCkudG9wIC0gKGluay5vZmZzZXRIZWlnaHQgLyAyKTtcblxuICAgICAgaW5rLnN0eWxlLnRvcCA9IHkgKyAncHgnO1xuICAgICAgaW5rLnN0eWxlLmxlZnQgPSB4ICsgJ3B4JztcbiAgICAgIGluay5zdHlsZS5wb2ludGVyRXZlbnRzID0gJ25vbmUnO1xuICAgICAgYWRkQ2xhc3MoaW5rLCAncmlwcGxlLWFuaW1hdGUnKTtcbiAgICB9XG5cbiAgICBmb3IgKGxldCB0YXJnZXQgPSBlLnRhcmdldDsgdGFyZ2V0ICYmIHRhcmdldCAhPT0gdGhpczsgdGFyZ2V0ID0gdGFyZ2V0W3BhcmVudE5vZGVdKSB7XG4gICAgICBpZiAoIWlzVmlzaWJsZSh0YXJnZXQpKSB7XG4gICAgICAgIGNvbnRpbnVlO1xuICAgICAgfVxuXG4gICAgICBpZiAoc2VsZWN0b3JNYXRjaGVzKHRhcmdldCwgJy5yaXBwbGVsaW5rLCAudWktYnV0dG9uLCAudWktbGlzdGJveC1pdGVtLCAudWktbXVsdGlzZWxlY3QtaXRlbSwgLnVpLWZpZWxkc2V0LXRvZ2dsZXInKSkge1xuICAgICAgICBjb25zdCBlbGVtZW50ID0gdGFyZ2V0O1xuICAgICAgICByaXBwbGVFZmZlY3QoZWxlbWVudCwgZSk7XG4gICAgICAgIGJyZWFrO1xuICAgICAgfVxuICAgIH1cbiAgfVxufVxuIl19