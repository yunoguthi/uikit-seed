/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, InjectionToken } from '@angular/core';
/** @type {?} */
export const FAVORITOS_SERVICE_TOKEN = new InjectionToken('FavoritosService');
/**
 * @abstract
 */
export class FavoritosService {
    constructor() { }
}
FavoritosService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
FavoritosService.ctorParameters = () => [];
if (false) {
    /**
     * Salva os favoritos do usuário
     * @abstract
     * @param {?} itens Itens da lista de favoritos
     * @return {?}
     */
    FavoritosService.prototype.salvar = function (itens) { };
    /**
     * Retorna os favoritos do usuário
     * @abstract
     * @return {?}
     */
    FavoritosService.prototype.buscar = function () { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmF2b3JpdG9zLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL3V0aWxzL2Zhdm9yaXRvcy5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLGNBQWMsRUFBRSxNQUFNLGVBQWUsQ0FBQzs7QUFJM0QsTUFBTSxPQUFPLHVCQUF1QixHQUFHLElBQUksY0FBYyxDQUFtQixrQkFBa0IsQ0FBQzs7OztBQUcvRixNQUFNLE9BQWdCLGdCQUFnQjtJQUNwQyxnQkFBZ0IsQ0FBQzs7O1lBRmxCLFVBQVU7Ozs7Ozs7Ozs7O0lBT1QseURBQXFEOzs7Ozs7SUFLckQsb0RBQTBDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSwgSW5qZWN0aW9uVG9rZW4gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE1lbnVJdGVtIH0gZnJvbSAnLi4vbGF5b3V0L25hdi9tZW51L21lbnUtaXRlbS9tZW51LWl0ZW0ubW9kZWwnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xuXG5leHBvcnQgY29uc3QgRkFWT1JJVE9TX1NFUlZJQ0VfVE9LRU4gPSBuZXcgSW5qZWN0aW9uVG9rZW48RmF2b3JpdG9zU2VydmljZT4oJ0Zhdm9yaXRvc1NlcnZpY2UnKTtcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIEZhdm9yaXRvc1NlcnZpY2Uge1xuICBjb25zdHJ1Y3RvcigpIHsgfVxuICAvKipcbiAgICogU2FsdmEgb3MgZmF2b3JpdG9zIGRvIHVzdcOhcmlvXG4gICAqIEBwYXJhbSBpdGVucyBJdGVucyBkYSBsaXN0YSBkZSBmYXZvcml0b3NcbiAgICovXG4gIGFic3RyYWN0IHNhbHZhcihpdGVuczogTWVudUl0ZW1bXSk6IE9ic2VydmFibGU8dm9pZD47XG5cbiAgLyoqXG4gICAqIFJldG9ybmEgb3MgZmF2b3JpdG9zIGRvIHVzdcOhcmlvXG4gICAqL1xuICBhYnN0cmFjdCBidXNjYXIoKTogT2JzZXJ2YWJsZTxNZW51SXRlbVtdPjtcbn1cbiJdfQ==