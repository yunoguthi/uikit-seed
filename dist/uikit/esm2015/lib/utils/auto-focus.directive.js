/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, ElementRef, Input } from '@angular/core';
export class AutoFocusDirective {
    /**
     * @param {?} el
     */
    constructor(el) {
        this.el = el;
    }
    /**
     * @return {?}
     */
    ngAfterContentInit() {
        setTimeout((/**
         * @return {?}
         */
        () => {
            this.hideKeyboard(this.el.nativeElement);
        }), 500);
    }
    /**
     * @param {?} el
     * @return {?}
     */
    hideKeyboard(el) {
        /** @type {?} */
        const agent = window.navigator.userAgent.toLowerCase();
        /** @type {?} */
        const att = document.createAttribute('readonly');
        el.setAttributeNode(att);
        setTimeout((/**
         * @return {?}
         */
        () => {
            el.blur();
            /** @type {?} */
            const isSafari = (agent.indexOf('safari') !== -1) && (!(agent.indexOf('chrome') > -1));
            if (!isSafari) {
                el.focus();
            }
            el.removeAttribute('readonly');
        }), 100);
    }
}
AutoFocusDirective.decorators = [
    { type: Directive, args: [{
                selector: '[autofocus]'
            },] }
];
/** @nocollapse */
AutoFocusDirective.ctorParameters = () => [
    { type: ElementRef }
];
AutoFocusDirective.propDecorators = {
    appAutoFocus: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    AutoFocusDirective.prototype.appAutoFocus;
    /**
     * @type {?}
     * @private
     */
    AutoFocusDirective.prototype.el;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0by1mb2N1cy5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL3V0aWxzL2F1dG8tZm9jdXMuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQW1CLFNBQVMsRUFBRSxVQUFVLEVBQUUsS0FBSyxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBSzdFLE1BQU0sT0FBTyxrQkFBa0I7Ozs7SUFJN0IsWUFBMkIsRUFBYztRQUFkLE9BQUUsR0FBRixFQUFFLENBQVk7SUFFekMsQ0FBQzs7OztJQUVNLGtCQUFrQjtRQUV2QixVQUFVOzs7UUFBQyxHQUFHLEVBQUU7WUFDZCxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDM0MsQ0FBQyxHQUFFLEdBQUcsQ0FBQyxDQUFDO0lBRVYsQ0FBQzs7Ozs7SUFFRCxZQUFZLENBQUMsRUFBTzs7Y0FDWixLQUFLLEdBQUcsTUFBTSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFOztjQUNoRCxHQUFHLEdBQUcsUUFBUSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUM7UUFDaEQsRUFBRSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3pCLFVBQVU7OztRQUFDLEdBQUcsRUFBRTtZQUNkLEVBQUUsQ0FBQyxJQUFJLEVBQUUsQ0FBQzs7a0JBQ0osUUFBUSxHQUFHLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN0RixJQUFJLENBQUMsUUFBUSxFQUFFO2dCQUNiLEVBQUUsQ0FBQyxLQUFLLEVBQUUsQ0FBQzthQUNaO1lBQ0QsRUFBRSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNqQyxDQUFDLEdBQUUsR0FBRyxDQUFDLENBQUM7SUFDVixDQUFDOzs7WUEvQkYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxhQUFhO2FBQ3hCOzs7O1lBSm9DLFVBQVU7OzsyQkFPNUMsS0FBSzs7OztJQUFOLDBDQUFzQzs7Ozs7SUFFbkIsZ0NBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtBZnRlckNvbnRlbnRJbml0LCBEaXJlY3RpdmUsIEVsZW1lbnRSZWYsIElucHV0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQERpcmVjdGl2ZSh7XG4gIHNlbGVjdG9yOiAnW2F1dG9mb2N1c10nXG59KVxuZXhwb3J0IGNsYXNzIEF1dG9Gb2N1c0RpcmVjdGl2ZSBpbXBsZW1lbnRzIEFmdGVyQ29udGVudEluaXQge1xuXG4gIEBJbnB1dCgpIHB1YmxpYyBhcHBBdXRvRm9jdXM6IGJvb2xlYW47XG5cbiAgcHVibGljIGNvbnN0cnVjdG9yKHByaXZhdGUgZWw6IEVsZW1lbnRSZWYpIHtcblxuICB9XG5cbiAgcHVibGljIG5nQWZ0ZXJDb250ZW50SW5pdCgpIHtcblxuICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgdGhpcy5oaWRlS2V5Ym9hcmQodGhpcy5lbC5uYXRpdmVFbGVtZW50KTtcbiAgICB9LCA1MDApO1xuXG4gIH1cblxuICBoaWRlS2V5Ym9hcmQoZWw6IGFueSkge1xuICAgIGNvbnN0IGFnZW50ID0gd2luZG93Lm5hdmlnYXRvci51c2VyQWdlbnQudG9Mb3dlckNhc2UoKTtcbiAgICBjb25zdCBhdHQgPSBkb2N1bWVudC5jcmVhdGVBdHRyaWJ1dGUoJ3JlYWRvbmx5Jyk7XG4gICAgZWwuc2V0QXR0cmlidXRlTm9kZShhdHQpO1xuICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgZWwuYmx1cigpO1xuICAgICAgY29uc3QgaXNTYWZhcmkgPSAoYWdlbnQuaW5kZXhPZignc2FmYXJpJykgIT09IC0xKSAmJiAoIShhZ2VudC5pbmRleE9mKCdjaHJvbWUnKSA+IC0xKSk7XG4gICAgICBpZiAoIWlzU2FmYXJpKSB7XG4gICAgICAgIGVsLmZvY3VzKCk7XG4gICAgICB9XG4gICAgICBlbC5yZW1vdmVBdHRyaWJ1dGUoJ3JlYWRvbmx5Jyk7XG4gICAgfSwgMTAwKTtcbiAgfVxufVxuIl19