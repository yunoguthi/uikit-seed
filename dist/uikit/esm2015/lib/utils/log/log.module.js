/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule, SkipSelf, Optional } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LogService, LogConsumersService } from './log.service';
export class LogModule {
    /**
     * @param {?} logConsumersService
     * @param {?=} parentModule
     */
    constructor(logConsumersService, parentModule) {
        this.logConsumersService = logConsumersService;
        if (parentModule) {
            throw new Error('LogModule is already loaded. Import it in the AppModule only');
        }
    }
}
LogModule.decorators = [
    { type: NgModule, args: [{
                declarations: [],
                imports: [
                    CommonModule
                ],
                providers: [
                    LogService,
                    LogConsumersService
                ],
                exports: []
            },] }
];
/** @nocollapse */
LogModule.ctorParameters = () => [
    { type: LogConsumersService },
    { type: LogModule, decorators: [{ type: Optional }, { type: SkipSelf }] }
];
if (false) {
    /** @type {?} */
    LogModule.prototype.logConsumersService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9nLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvdXRpbHMvbG9nL2xvZy5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM3RCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDL0MsT0FBTyxFQUFFLFVBQVUsRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQWlCaEUsTUFBTSxPQUFPLFNBQVM7Ozs7O0lBQ3BCLFlBQ1MsbUJBQXdDLEVBQ3ZCLFlBQXdCO1FBRHpDLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFHN0MsSUFBSSxZQUFZLEVBQUU7WUFDaEIsTUFBTSxJQUFJLEtBQUssQ0FDYiw4REFBOEQsQ0FBQyxDQUFDO1NBQ25FO0lBQ0wsQ0FBQzs7O1lBeEJGLFFBQVEsU0FBQztnQkFDUixZQUFZLEVBQUUsRUFFYjtnQkFDRCxPQUFPLEVBQUU7b0JBQ1AsWUFBWTtpQkFDYjtnQkFDRCxTQUFTLEVBQUU7b0JBQ1QsVUFBVTtvQkFDVixtQkFBbUI7aUJBQ3BCO2dCQUNELE9BQU8sRUFBRSxFQUVSO2FBQ0Y7Ozs7WUFoQm9CLG1CQUFtQjtZQW9CRyxTQUFTLHVCQUEvQyxRQUFRLFlBQUksUUFBUTs7OztJQURyQix3Q0FBK0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSwgU2tpcFNlbGYsIE9wdGlvbmFsIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHsgTG9nU2VydmljZSwgTG9nQ29uc3VtZXJzU2VydmljZSB9IGZyb20gJy4vbG9nLnNlcnZpY2UnO1xuXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtcblxuICBdLFxuICBpbXBvcnRzOiBbXG4gICAgQ29tbW9uTW9kdWxlXG4gIF0sXG4gIHByb3ZpZGVyczogW1xuICAgIExvZ1NlcnZpY2UsXG4gICAgTG9nQ29uc3VtZXJzU2VydmljZVxuICBdLFxuICBleHBvcnRzOiBbXG5cbiAgXVxufSlcbmV4cG9ydCBjbGFzcyBMb2dNb2R1bGUge1xuICBjb25zdHJ1Y3RvcihcbiAgICBwdWJsaWMgbG9nQ29uc3VtZXJzU2VydmljZTogTG9nQ29uc3VtZXJzU2VydmljZSxcbiAgICBAT3B0aW9uYWwoKSBAU2tpcFNlbGYoKSBwYXJlbnRNb2R1bGU/OiBMb2dNb2R1bGUsXG4gICAgKSB7XG4gICAgICBpZiAocGFyZW50TW9kdWxlKSB7XG4gICAgICAgIHRocm93IG5ldyBFcnJvcihcbiAgICAgICAgICAnTG9nTW9kdWxlIGlzIGFscmVhZHkgbG9hZGVkLiBJbXBvcnQgaXQgaW4gdGhlIEFwcE1vZHVsZSBvbmx5Jyk7XG4gICAgICB9XG4gIH1cbn1cbiJdfQ==