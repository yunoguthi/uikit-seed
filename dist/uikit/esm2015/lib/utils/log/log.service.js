/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { BehaviorSubject, Subscription } from 'rxjs';
import { Injectable, Inject, InjectionToken, Optional } from '@angular/core';
/** @enum {number} */
const LogLevel = {
    Debug: 0,
    Info: 1,
    Warn: 2,
    Error: 3,
};
export { LogLevel };
LogLevel[LogLevel.Debug] = 'Debug';
LogLevel[LogLevel.Info] = 'Info';
LogLevel[LogLevel.Warn] = 'Warn';
LogLevel[LogLevel.Error] = 'Error';
/**
 * @record
 */
export function Log() { }
if (false) {
    /** @type {?} */
    Log.prototype.message;
    /** @type {?} */
    Log.prototype.level;
}
export class LogService {
    constructor() {
        this.loggerSubject$ = new BehaviorSubject({ message: 'Log iniciado!', level: LogLevel.Debug });
        this.logger$ = this.loggerSubject$.asObservable();
    }
    /**
     * @private
     * @param {...?} infos
     * @return {?}
     */
    infosToMessage(...infos) {
        return infos.join(' ');
    }
    /**
     * @protected
     * @param {?=} level
     * @param {...?} infos
     * @return {?}
     */
    log(level = LogLevel.Debug, ...infos) {
        /** @type {?} */
        const message = this.infosToMessage(...infos);
        this.loggerSubject$.next({ message, level });
    }
    /**
     * @param {...?} data
     * @return {?}
     */
    debug(...data) {
        this.log(LogLevel.Debug, ...data);
    }
    /**
     * @param {...?} data
     * @return {?}
     */
    info(...data) {
        this.log(LogLevel.Info, ...data);
    }
    /**
     * @param {...?} data
     * @return {?}
     */
    warn(...data) {
        this.log(LogLevel.Warn, ...data);
    }
    /**
     * @param {...?} data
     * @return {?}
     */
    error(...data) {
        this.log(LogLevel.Error, ...data);
    }
}
LogService.decorators = [
    { type: Injectable }
];
if (false) {
    /**
     * @type {?}
     * @protected
     */
    LogService.prototype.loggerSubject$;
    /** @type {?} */
    LogService.prototype.logger$;
}
/** @type {?} */
export const LOG_CONSUMER_SERVICE = new InjectionToken('LOG_CONSUMER_SERVICE');
export class LogConsumersService {
    /**
     * @param {?} logService
     * @param {?=} services
     */
    constructor(logService, services) {
        this.logService = logService;
        if (services && services.length > 0) {
            services.forEach((/**
             * @param {?} service
             * @return {?}
             */
            service => service.configure(logService)));
        }
    }
}
LogConsumersService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
LogConsumersService.ctorParameters = () => [
    { type: LogService },
    { type: Array, decorators: [{ type: Inject, args: [LOG_CONSUMER_SERVICE,] }, { type: Optional }] }
];
if (false) {
    /**
     * @type {?}
     * @protected
     */
    LogConsumersService.prototype.logService;
}
/**
 * @abstract
 */
export class LogConsumer {
    constructor() {
        this.logSubscription = new Subscription();
        this.minimalConsumerLevel = LogLevel.Debug;
    }
    /**
     * @protected
     * @param {?} log
     * @return {?}
     */
    consume(log) {
        if (log.level >= this.minimalConsumerLevel) {
            if (log.level > 0) {
                this.consumeDebug(log.message);
            }
            else if (log.level > 1) {
                this.consumeInfo(log.message);
            }
            else if (log.level > 2) {
                this.consumeWarn(log.message);
            }
            else if (log.level > 3) {
                this.consumeError(log.message);
            }
        }
    }
    /**
     * @param {?} logService
     * @return {?}
     */
    configure(logService) {
        this.logSubscription.add(logService.logger$.subscribe((/**
         * @param {?} log
         * @return {?}
         */
        (log) => this.consume(log))));
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.logSubscription && !this.logSubscription.closed) {
            this.logSubscription.unsubscribe();
        }
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    LogConsumer.prototype.logSubscription;
    /** @type {?} */
    LogConsumer.prototype.minimalConsumerLevel;
    /**
     * @abstract
     * @protected
     * @param {?} message
     * @return {?}
     */
    LogConsumer.prototype.consumeDebug = function (message) { };
    /**
     * @abstract
     * @protected
     * @param {?} message
     * @return {?}
     */
    LogConsumer.prototype.consumeInfo = function (message) { };
    /**
     * @abstract
     * @protected
     * @param {?} message
     * @return {?}
     */
    LogConsumer.prototype.consumeWarn = function (message) { };
    /**
     * @abstract
     * @protected
     * @param {?} message
     * @return {?}
     */
    LogConsumer.prototype.consumeError = function (message) { };
}
export class LogConsoleConsumerService extends LogConsumer {
    /**
     * @protected
     * @param {?} message
     * @return {?}
     */
    consumeDebug(message) {
        console.debug(message);
    }
    /**
     * @protected
     * @param {?} message
     * @return {?}
     */
    consumeInfo(message) {
        console.info(message);
    }
    /**
     * @protected
     * @param {?} message
     * @return {?}
     */
    consumeWarn(message) {
        console.warn(message);
    }
    /**
     * @protected
     * @param {?} message
     * @return {?}
     */
    consumeError(message) {
        console.error(message);
    }
}
LogConsoleConsumerService.decorators = [
    { type: Injectable }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9nLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL3V0aWxzL2xvZy9sb2cuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLGVBQWUsRUFBRSxZQUFZLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDckQsT0FBTyxFQUFFLFVBQVUsRUFBYSxNQUFNLEVBQUUsY0FBYyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQzs7O0lBR3RGLFFBQVM7SUFDVCxPQUFRO0lBQ1IsT0FBUTtJQUNSLFFBQVM7Ozs7Ozs7Ozs7QUFHWCx5QkFHQzs7O0lBRkMsc0JBQWdCOztJQUNoQixvQkFBZ0I7O0FBSWxCLE1BQU0sT0FBTyxVQUFVO0lBRHZCO1FBR1ksbUJBQWMsR0FBRyxJQUFJLGVBQWUsQ0FBTSxFQUFFLE9BQU8sRUFBRSxlQUFlLEVBQUUsS0FBSyxFQUFFLFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDO1FBRWxHLFlBQU8sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksRUFBRSxDQUFDO0lBMkJ0RCxDQUFDOzs7Ozs7SUF6QlMsY0FBYyxDQUFDLEdBQUcsS0FBWTtRQUNwQyxPQUFPLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDekIsQ0FBQzs7Ozs7OztJQUVTLEdBQUcsQ0FBQyxLQUFLLEdBQUcsUUFBUSxDQUFDLEtBQUssRUFBRSxHQUFHLEtBQVk7O2NBQzdDLE9BQU8sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsS0FBSyxDQUFDO1FBQzdDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7SUFDL0MsQ0FBQzs7Ozs7SUFFTSxLQUFLLENBQUMsR0FBRyxJQUFXO1FBQ3pCLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRSxHQUFHLElBQUksQ0FBQyxDQUFDO0lBQ3BDLENBQUM7Ozs7O0lBRU0sSUFBSSxDQUFDLEdBQUcsSUFBVztRQUN4QixJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUMsQ0FBQztJQUNuQyxDQUFDOzs7OztJQUVNLElBQUksQ0FBQyxHQUFHLElBQVc7UUFDeEIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLEdBQUcsSUFBSSxDQUFDLENBQUM7SUFDbkMsQ0FBQzs7Ozs7SUFFTSxLQUFLLENBQUMsR0FBRyxJQUFXO1FBQ3pCLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRSxHQUFHLElBQUksQ0FBQyxDQUFDO0lBQ3BDLENBQUM7OztZQTlCRixVQUFVOzs7Ozs7O0lBR1Qsb0NBQXlHOztJQUV6Ryw2QkFBb0Q7OztBQThCdEQsTUFBTSxPQUFPLG9CQUFvQixHQUFHLElBQUksY0FBYyxDQUFjLHNCQUFzQixDQUFDO0FBRzNGLE1BQU0sT0FBTyxtQkFBbUI7Ozs7O0lBQzlCLFlBQ1ksVUFBc0IsRUFDVSxRQUF3QjtRQUR4RCxlQUFVLEdBQVYsVUFBVSxDQUFZO1FBR2hDLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ25DLFFBQVEsQ0FBQyxPQUFPOzs7O1lBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxFQUFDLENBQUM7U0FDNUQ7SUFDSCxDQUFDOzs7WUFURixVQUFVOzs7O1lBR2UsVUFBVTt3Q0FDL0IsTUFBTSxTQUFDLG9CQUFvQixjQUFHLFFBQVE7Ozs7Ozs7SUFEdkMseUNBQWdDOzs7OztBQVNwQyxNQUFNLE9BQWdCLFdBQVc7SUFBakM7UUFFVSxvQkFBZSxHQUFpQixJQUFJLFlBQVksRUFBRSxDQUFDO1FBRXBELHlCQUFvQixHQUFhLFFBQVEsQ0FBQyxLQUFLLENBQUM7SUErQnpELENBQUM7Ozs7OztJQTdCVyxPQUFPLENBQUMsR0FBUTtRQUN4QixJQUFJLEdBQUcsQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLG9CQUFvQixFQUFFO1lBQzFDLElBQUksR0FBRyxDQUFDLEtBQUssR0FBRyxDQUFDLEVBQUU7Z0JBQ2pCLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2FBQ2hDO2lCQUFNLElBQUksR0FBRyxDQUFDLEtBQUssR0FBRyxDQUFDLEVBQUU7Z0JBQ3hCLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2FBQy9CO2lCQUFNLElBQUksR0FBRyxDQUFDLEtBQUssR0FBRyxDQUFDLEVBQUU7Z0JBQ3hCLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2FBQy9CO2lCQUFNLElBQUksR0FBRyxDQUFDLEtBQUssR0FBRyxDQUFDLEVBQUU7Z0JBQ3hCLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2FBQ2hDO1NBQ0Y7SUFDSCxDQUFDOzs7OztJQUVNLFNBQVMsQ0FBQyxVQUFzQjtRQUNyQyxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLFNBQVM7Ozs7UUFBQyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsRUFBQyxDQUFDLENBQUM7SUFDckYsQ0FBQzs7OztJQVFELFdBQVc7UUFDVCxJQUFJLElBQUksQ0FBQyxlQUFlLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sRUFBRTtZQUN4RCxJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQ3BDO0lBQ0gsQ0FBQztDQUNGOzs7Ozs7SUFqQ0Msc0NBQTJEOztJQUUzRCwyQ0FBdUQ7Ozs7Ozs7SUFvQnZELDREQUFpRDs7Ozs7OztJQUNqRCwyREFBZ0Q7Ozs7Ozs7SUFDaEQsMkRBQWdEOzs7Ozs7O0lBQ2hELDREQUFpRDs7QUFXbkQsTUFBTSxPQUFPLHlCQUEwQixTQUFRLFdBQVc7Ozs7OztJQUM5QyxZQUFZLENBQUMsT0FBZTtRQUNwQyxPQUFPLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ3pCLENBQUM7Ozs7OztJQUNTLFdBQVcsQ0FBQyxPQUFlO1FBQ25DLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDeEIsQ0FBQzs7Ozs7O0lBQ1MsV0FBVyxDQUFDLE9BQWU7UUFDbkMsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUN4QixDQUFDOzs7Ozs7SUFDUyxZQUFZLENBQUMsT0FBZTtRQUNwQyxPQUFPLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ3pCLENBQUM7OztZQWJGLFVBQVUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBCZWhhdmlvclN1YmplY3QsIFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgSW5qZWN0YWJsZSwgT25EZXN0cm95LCBJbmplY3QsIEluamVjdGlvblRva2VuLCBPcHRpb25hbCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5leHBvcnQgZW51bSBMb2dMZXZlbCB7XG4gIERlYnVnID0gMCxcbiAgSW5mbyA9IDEsXG4gIFdhcm4gPSAyLFxuICBFcnJvciA9IDNcbn1cblxuZXhwb3J0IGludGVyZmFjZSBMb2cge1xuICBtZXNzYWdlOiBzdHJpbmc7XG4gIGxldmVsOiBMb2dMZXZlbDtcbn1cblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIExvZ1NlcnZpY2Uge1xuXG4gIHByb3RlY3RlZCBsb2dnZXJTdWJqZWN0JCA9IG5ldyBCZWhhdmlvclN1YmplY3Q8TG9nPih7IG1lc3NhZ2U6ICdMb2cgaW5pY2lhZG8hJywgbGV2ZWw6IExvZ0xldmVsLkRlYnVnIH0pO1xuXG4gIHB1YmxpYyBsb2dnZXIkID0gdGhpcy5sb2dnZXJTdWJqZWN0JC5hc09ic2VydmFibGUoKTtcblxuICBwcml2YXRlIGluZm9zVG9NZXNzYWdlKC4uLmluZm9zOiBhbnlbXSkge1xuICAgIHJldHVybiBpbmZvcy5qb2luKCcgJyk7XG4gIH1cblxuICBwcm90ZWN0ZWQgbG9nKGxldmVsID0gTG9nTGV2ZWwuRGVidWcsIC4uLmluZm9zOiBhbnlbXSkge1xuICAgIGNvbnN0IG1lc3NhZ2UgPSB0aGlzLmluZm9zVG9NZXNzYWdlKC4uLmluZm9zKTtcbiAgICB0aGlzLmxvZ2dlclN1YmplY3QkLm5leHQoeyBtZXNzYWdlLCBsZXZlbCB9KTtcbiAgfVxuXG4gIHB1YmxpYyBkZWJ1ZyguLi5kYXRhOiBhbnlbXSk6IHZvaWQge1xuICAgIHRoaXMubG9nKExvZ0xldmVsLkRlYnVnLCAuLi5kYXRhKTtcbiAgfVxuXG4gIHB1YmxpYyBpbmZvKC4uLmRhdGE6IGFueVtdKTogdm9pZCB7XG4gICAgdGhpcy5sb2coTG9nTGV2ZWwuSW5mbywgLi4uZGF0YSk7XG4gIH1cblxuICBwdWJsaWMgd2FybiguLi5kYXRhOiBhbnlbXSk6IHZvaWQge1xuICAgIHRoaXMubG9nKExvZ0xldmVsLldhcm4sIC4uLmRhdGEpO1xuICB9XG5cbiAgcHVibGljIGVycm9yKC4uLmRhdGE6IGFueVtdKTogdm9pZCB7XG4gICAgdGhpcy5sb2coTG9nTGV2ZWwuRXJyb3IsIC4uLmRhdGEpO1xuICB9XG5cbn1cblxuXG5leHBvcnQgY29uc3QgTE9HX0NPTlNVTUVSX1NFUlZJQ0UgPSBuZXcgSW5qZWN0aW9uVG9rZW48TG9nQ29uc3VtZXI+KCdMT0dfQ09OU1VNRVJfU0VSVklDRScpO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgTG9nQ29uc3VtZXJzU2VydmljZSB7XG4gIGNvbnN0cnVjdG9yKFxuICAgIHByb3RlY3RlZCBsb2dTZXJ2aWNlOiBMb2dTZXJ2aWNlLFxuICAgIEBJbmplY3QoTE9HX0NPTlNVTUVSX1NFUlZJQ0UpIEBPcHRpb25hbCgpIHNlcnZpY2VzPzogTG9nQ29uc3VtZXJbXSxcbiAgKSB7XG4gICAgaWYgKHNlcnZpY2VzICYmIHNlcnZpY2VzLmxlbmd0aCA+IDApIHtcbiAgICAgIHNlcnZpY2VzLmZvckVhY2goc2VydmljZSA9PiBzZXJ2aWNlLmNvbmZpZ3VyZShsb2dTZXJ2aWNlKSk7XG4gICAgfVxuICB9XG59XG5cbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBMb2dDb25zdW1lciBpbXBsZW1lbnRzIE9uRGVzdHJveSB7XG5cbiAgcHJpdmF0ZSBsb2dTdWJzY3JpcHRpb246IFN1YnNjcmlwdGlvbiA9IG5ldyBTdWJzY3JpcHRpb24oKTtcblxuICBwdWJsaWMgbWluaW1hbENvbnN1bWVyTGV2ZWw6IExvZ0xldmVsID0gTG9nTGV2ZWwuRGVidWc7XG5cbiAgcHJvdGVjdGVkIGNvbnN1bWUobG9nOiBMb2cpIHtcbiAgICBpZiAobG9nLmxldmVsID49IHRoaXMubWluaW1hbENvbnN1bWVyTGV2ZWwpIHtcbiAgICAgIGlmIChsb2cubGV2ZWwgPiAwKSB7XG4gICAgICAgIHRoaXMuY29uc3VtZURlYnVnKGxvZy5tZXNzYWdlKTtcbiAgICAgIH0gZWxzZSBpZiAobG9nLmxldmVsID4gMSkge1xuICAgICAgICB0aGlzLmNvbnN1bWVJbmZvKGxvZy5tZXNzYWdlKTtcbiAgICAgIH0gZWxzZSBpZiAobG9nLmxldmVsID4gMikge1xuICAgICAgICB0aGlzLmNvbnN1bWVXYXJuKGxvZy5tZXNzYWdlKTtcbiAgICAgIH0gZWxzZSBpZiAobG9nLmxldmVsID4gMykge1xuICAgICAgICB0aGlzLmNvbnN1bWVFcnJvcihsb2cubWVzc2FnZSk7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgcHVibGljIGNvbmZpZ3VyZShsb2dTZXJ2aWNlOiBMb2dTZXJ2aWNlKSB7XG4gICAgdGhpcy5sb2dTdWJzY3JpcHRpb24uYWRkKGxvZ1NlcnZpY2UubG9nZ2VyJC5zdWJzY3JpYmUoKGxvZykgPT4gdGhpcy5jb25zdW1lKGxvZykpKTtcbiAgfVxuXG4gIHByb3RlY3RlZCBhYnN0cmFjdCBjb25zdW1lRGVidWcobWVzc2FnZTogc3RyaW5nKTtcbiAgcHJvdGVjdGVkIGFic3RyYWN0IGNvbnN1bWVJbmZvKG1lc3NhZ2U6IHN0cmluZyk7XG4gIHByb3RlY3RlZCBhYnN0cmFjdCBjb25zdW1lV2FybihtZXNzYWdlOiBzdHJpbmcpO1xuICBwcm90ZWN0ZWQgYWJzdHJhY3QgY29uc3VtZUVycm9yKG1lc3NhZ2U6IHN0cmluZyk7XG5cblxuICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcbiAgICBpZiAodGhpcy5sb2dTdWJzY3JpcHRpb24gJiYgIXRoaXMubG9nU3Vic2NyaXB0aW9uLmNsb3NlZCkge1xuICAgICAgdGhpcy5sb2dTdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcbiAgICB9XG4gIH1cbn1cblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIExvZ0NvbnNvbGVDb25zdW1lclNlcnZpY2UgZXh0ZW5kcyBMb2dDb25zdW1lciB7XG4gIHByb3RlY3RlZCBjb25zdW1lRGVidWcobWVzc2FnZTogc3RyaW5nKSB7XG4gICAgY29uc29sZS5kZWJ1ZyhtZXNzYWdlKTtcbiAgfVxuICBwcm90ZWN0ZWQgY29uc3VtZUluZm8obWVzc2FnZTogc3RyaW5nKSB7XG4gICAgY29uc29sZS5pbmZvKG1lc3NhZ2UpO1xuICB9XG4gIHByb3RlY3RlZCBjb25zdW1lV2FybihtZXNzYWdlOiBzdHJpbmcpIHtcbiAgICBjb25zb2xlLndhcm4obWVzc2FnZSk7XG4gIH1cbiAgcHJvdGVjdGVkIGNvbnN1bWVFcnJvcihtZXNzYWdlOiBzdHJpbmcpIHtcbiAgICBjb25zb2xlLmVycm9yKG1lc3NhZ2UpO1xuICB9XG59XG4iXX0=