/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { MatPaginatorIntl } from '@angular/material';
export class MatPaginatorIntlPtBr extends MatPaginatorIntl {
    constructor() {
        super(...arguments);
        this.itemsPerPageLabel = 'Items por página';
        this.nextPageLabel = 'Próxima página';
        this.previousPageLabel = 'Página anterior';
        this.getRangeLabel = (/**
         * @param {?} page
         * @param {?} pageSize
         * @param {?} length
         * @return {?}
         */
        (page, pageSize, length) => {
            if (length === 0 || pageSize === 0) {
                return '0 de ' + length;
            }
            length = Math.max(length, 0);
            /** @type {?} */
            const startIndex = page * pageSize;
            /** @type {?} */
            const endIndex = startIndex < length ?
                Math.min(startIndex + pageSize, length) :
                startIndex + pageSize;
            return startIndex + 1 + ' - ' + endIndex + ' de  ' + length;
        });
    }
}
if (false) {
    /** @type {?} */
    MatPaginatorIntlPtBr.prototype.itemsPerPageLabel;
    /** @type {?} */
    MatPaginatorIntlPtBr.prototype.nextPageLabel;
    /** @type {?} */
    MatPaginatorIntlPtBr.prototype.previousPageLabel;
    /** @type {?} */
    MatPaginatorIntlPtBr.prototype.getRangeLabel;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWF0LXBhZ2luYXRvci1wdGJyLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi91dGlscy9tYXQtcGFnaW5hdG9yLXB0YnIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxnQkFBZ0IsRUFBQyxNQUFNLG1CQUFtQixDQUFDO0FBQ25ELE1BQU0sT0FBTyxvQkFBcUIsU0FBUSxnQkFBZ0I7SUFBMUQ7O1FBQ0Usc0JBQWlCLEdBQUcsa0JBQWtCLENBQUM7UUFDdkMsa0JBQWEsR0FBTyxnQkFBZ0IsQ0FBQztRQUNyQyxzQkFBaUIsR0FBRyxpQkFBaUIsQ0FBQztRQUV0QyxrQkFBYTs7Ozs7O1FBQUcsQ0FBQyxJQUFJLEVBQUUsUUFBUSxFQUFFLE1BQU0sRUFBRSxFQUFFO1lBQ3pDLElBQUksTUFBTSxLQUFLLENBQUMsSUFBSSxRQUFRLEtBQUssQ0FBQyxFQUFFO2dCQUNsQyxPQUFPLE9BQU8sR0FBRyxNQUFNLENBQUM7YUFDekI7WUFDRCxNQUFNLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUM7O2tCQUN2QixVQUFVLEdBQUcsSUFBSSxHQUFHLFFBQVE7O2tCQUM1QixRQUFRLEdBQUcsVUFBVSxHQUFHLE1BQU0sQ0FBQyxDQUFDO2dCQUNwQyxJQUFJLENBQUMsR0FBRyxDQUFDLFVBQVUsR0FBRyxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUMsQ0FBQztnQkFDekMsVUFBVSxHQUFHLFFBQVE7WUFDdkIsT0FBTyxVQUFVLEdBQUcsQ0FBQyxHQUFHLEtBQUssR0FBRyxRQUFRLEdBQUcsT0FBTyxHQUFHLE1BQU0sQ0FBQztRQUM5RCxDQUFDLEVBQUM7SUFDSixDQUFDO0NBQUE7OztJQWZDLGlEQUF1Qzs7SUFDdkMsNkNBQXFDOztJQUNyQyxpREFBc0M7O0lBRXRDLDZDQVVFIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtNYXRQYWdpbmF0b3JJbnRsfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XG5leHBvcnQgY2xhc3MgTWF0UGFnaW5hdG9ySW50bFB0QnIgZXh0ZW5kcyBNYXRQYWdpbmF0b3JJbnRsIHtcbiAgaXRlbXNQZXJQYWdlTGFiZWwgPSAnSXRlbXMgcG9yIHDDoWdpbmEnO1xuICBuZXh0UGFnZUxhYmVsICAgICA9ICdQcsOzeGltYSBww6FnaW5hJztcbiAgcHJldmlvdXNQYWdlTGFiZWwgPSAnUMOhZ2luYSBhbnRlcmlvcic7XG5cbiAgZ2V0UmFuZ2VMYWJlbCA9IChwYWdlLCBwYWdlU2l6ZSwgbGVuZ3RoKSA9PiB7XG4gICAgaWYgKGxlbmd0aCA9PT0gMCB8fCBwYWdlU2l6ZSA9PT0gMCkge1xuICAgICAgcmV0dXJuICcwIGRlICcgKyBsZW5ndGg7XG4gICAgfVxuICAgIGxlbmd0aCA9IE1hdGgubWF4KGxlbmd0aCwgMCk7XG4gICAgY29uc3Qgc3RhcnRJbmRleCA9IHBhZ2UgKiBwYWdlU2l6ZTtcbiAgICBjb25zdCBlbmRJbmRleCA9IHN0YXJ0SW5kZXggPCBsZW5ndGggP1xuICAgICAgTWF0aC5taW4oc3RhcnRJbmRleCArIHBhZ2VTaXplLCBsZW5ndGgpIDpcbiAgICAgIHN0YXJ0SW5kZXggKyBwYWdlU2l6ZTtcbiAgICByZXR1cm4gc3RhcnRJbmRleCArIDEgKyAnIC0gJyArIGVuZEluZGV4ICsgJyBkZSAgJyArIGxlbmd0aDtcbiAgfTtcbn1cbiJdfQ==