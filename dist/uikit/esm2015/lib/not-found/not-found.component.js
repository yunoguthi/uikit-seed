/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Location } from '@angular/common';
export class NotFoundComponent {
    /**
     * @param {?} ngLocation
     */
    constructor(ngLocation) {
        this.ngLocation = ngLocation;
        this.imageSrc = require('@cnj/uikit/lib/theme/uikit/images/error_404.png');
    }
    /**
     * @return {?}
     */
    ngOnInit() { }
    /**
     * @return {?}
     */
    goBack() {
        this.ngLocation.back();
    }
}
NotFoundComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-not-found',
                template: "<div class=\"container-fluid\">\n  <div class=\"row\">\n    <div class=\"col\">\n      <div class=\"error\">\n        <img src=\"{{ imageSrc }}\" />\n        <h3><b>Oops!</b> P\u00E1gina n\u00E3o encontrada</h3>\n        <button mat-raised-button color=\"primary\" (click)=\"goBack()\">Clique aqui para voltar</button>\n      </div>\n    </div>\n  </div>\n</div>\n",
                changeDetection: ChangeDetectionStrategy.OnPush,
                styles: [""]
            }] }
];
/** @nocollapse */
NotFoundComponent.ctorParameters = () => [
    { type: Location }
];
if (false) {
    /** @type {?} */
    NotFoundComponent.prototype.imageSrc;
    /**
     * @type {?}
     * @private
     */
    NotFoundComponent.prototype.ngLocation;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90LWZvdW5kLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvbm90LWZvdW5kL25vdC1mb3VuZC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFDTCxTQUFTLEVBR1QsdUJBQXVCLEVBQ3hCLE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQVUzQyxNQUFNLE9BQU8saUJBQWlCOzs7O0lBRTVCLFlBQ1UsVUFBb0I7UUFBcEIsZUFBVSxHQUFWLFVBQVUsQ0FBVTtRQUY5QixhQUFRLEdBQUcsT0FBTyxDQUFDLGlEQUFpRCxDQUFDLENBQUM7SUFHbEUsQ0FBQzs7OztJQUVMLFFBQVEsS0FBSSxDQUFDOzs7O0lBRU4sTUFBTTtRQUNYLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDekIsQ0FBQzs7O1lBaEJGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsaUJBQWlCO2dCQUMzQix3WEFBeUM7Z0JBRXpDLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNOzthQUNoRDs7OztZQVRRLFFBQVE7Ozs7SUFXZixxQ0FBc0U7Ozs7O0lBRXBFLHVDQUE0QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XG4gIENvbXBvbmVudCxcbiAgT25Jbml0LFxuICBWaWV3RW5jYXBzdWxhdGlvbixcbiAgQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3lcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBMb2NhdGlvbiB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5cbmRlY2xhcmUgZnVuY3Rpb24gcmVxdWlyZShwYXRoOiBzdHJpbmcpO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICd1aWtpdC1ub3QtZm91bmQnLFxuICB0ZW1wbGF0ZVVybDogJy4vbm90LWZvdW5kLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vbm90LWZvdW5kLmNvbXBvbmVudC5zY3NzJ10sXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoXG59KVxuZXhwb3J0IGNsYXNzIE5vdEZvdW5kQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgaW1hZ2VTcmMgPSByZXF1aXJlKCdAY25qL3Vpa2l0L2xpYi90aGVtZS91aWtpdC9pbWFnZXMvZXJyb3JfNDA0LnBuZycpO1xuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIG5nTG9jYXRpb246IExvY2F0aW9uXG4gICkgeyB9XG5cbiAgbmdPbkluaXQoKSB7fVxuXG4gIHB1YmxpYyBnb0JhY2soKSB7XG4gICAgdGhpcy5uZ0xvY2F0aW9uLmJhY2soKTtcbiAgfVxuXG59XG4iXX0=