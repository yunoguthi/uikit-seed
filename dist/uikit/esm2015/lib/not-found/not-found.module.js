/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotFoundRoutingModule } from './not-found-routing.module';
import { NotFoundComponent } from './not-found.component';
import { MatButtonModule } from '@angular/material';
export class NotFoundModule {
}
NotFoundModule.decorators = [
    { type: NgModule, args: [{
                entryComponents: [],
                declarations: [NotFoundComponent],
                imports: [
                    CommonModule,
                    NotFoundRoutingModule,
                    MatButtonModule
                ],
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90LWZvdW5kLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvbm90LWZvdW5kL25vdC1mb3VuZC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRS9DLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQ25FLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQzFELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQVdwRCxNQUFNLE9BQU8sY0FBYzs7O1lBVDFCLFFBQVEsU0FBQztnQkFDUixlQUFlLEVBQUUsRUFBRTtnQkFDbkIsWUFBWSxFQUFFLENBQUMsaUJBQWlCLENBQUM7Z0JBQ2pDLE9BQU8sRUFBRTtvQkFDUCxZQUFZO29CQUNaLHFCQUFxQjtvQkFDckIsZUFBZTtpQkFDaEI7YUFDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuXG5pbXBvcnQgeyBOb3RGb3VuZFJvdXRpbmdNb2R1bGUgfSBmcm9tICcuL25vdC1mb3VuZC1yb3V0aW5nLm1vZHVsZSc7XG5pbXBvcnQgeyBOb3RGb3VuZENvbXBvbmVudCB9IGZyb20gJy4vbm90LWZvdW5kLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBNYXRCdXR0b25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XG5cbkBOZ01vZHVsZSh7XG4gIGVudHJ5Q29tcG9uZW50czogW10sXG4gIGRlY2xhcmF0aW9uczogW05vdEZvdW5kQ29tcG9uZW50XSxcbiAgaW1wb3J0czogW1xuICAgIENvbW1vbk1vZHVsZSxcbiAgICBOb3RGb3VuZFJvdXRpbmdNb2R1bGUsXG4gICAgTWF0QnV0dG9uTW9kdWxlXG4gIF0sXG59KVxuZXhwb3J0IGNsYXNzIE5vdEZvdW5kTW9kdWxlIHsgfVxuIl19