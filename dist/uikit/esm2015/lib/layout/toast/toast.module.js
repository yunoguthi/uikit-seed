/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToastComponent } from '../toast/toast.component';
import { ToastService } from '../toast/toast.service';
import { ToastrModule, ToastNoAnimationModule, ToastContainerModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
export class ToastModule {
}
ToastModule.decorators = [
    { type: NgModule, args: [{
                entryComponents: [ToastComponent],
                declarations: [ToastComponent],
                imports: [
                    CommonModule,
                    ToastNoAnimationModule,
                    ToastrModule.forRoot(),
                    ToastContainerModule,
                    BrowserAnimationsModule,
                ],
                exports: [ToastComponent],
                providers: [ToastService]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9hc3QubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9sYXlvdXQvdG9hc3QvdG9hc3QubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsUUFBUSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3ZDLE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUM3QyxPQUFPLEVBQUMsY0FBYyxFQUFDLE1BQU0sMEJBQTBCLENBQUM7QUFDeEQsT0FBTyxFQUFDLFlBQVksRUFBQyxNQUFNLHdCQUF3QixDQUFDO0FBQ3BELE9BQU8sRUFDTCxZQUFZLEVBQ1osc0JBQXNCLEVBQ3RCLG9CQUFvQixFQUNyQixNQUFNLFlBQVksQ0FBQztBQUVwQixPQUFPLEVBQUMsdUJBQXVCLEVBQUMsTUFBTSxzQ0FBc0MsQ0FBQztBQWU3RSxNQUFNLE9BQU8sV0FBVzs7O1lBYnZCLFFBQVEsU0FBQztnQkFDUixlQUFlLEVBQUUsQ0FBQyxjQUFjLENBQUM7Z0JBQ2pDLFlBQVksRUFBRSxDQUFDLGNBQWMsQ0FBQztnQkFDOUIsT0FBTyxFQUFFO29CQUNQLFlBQVk7b0JBQ1osc0JBQXNCO29CQUN0QixZQUFZLENBQUMsT0FBTyxFQUFFO29CQUN0QixvQkFBb0I7b0JBQ3BCLHVCQUF1QjtpQkFDeEI7Z0JBQ0QsT0FBTyxFQUFFLENBQUMsY0FBYyxDQUFDO2dCQUN6QixTQUFTLEVBQUUsQ0FBQyxZQUFZLENBQUM7YUFDMUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge05nTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7Q29tbW9uTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHtUb2FzdENvbXBvbmVudH0gZnJvbSAnLi4vdG9hc3QvdG9hc3QuY29tcG9uZW50JztcbmltcG9ydCB7VG9hc3RTZXJ2aWNlfSBmcm9tICcuLi90b2FzdC90b2FzdC5zZXJ2aWNlJztcbmltcG9ydCB7XG4gIFRvYXN0ck1vZHVsZSxcbiAgVG9hc3ROb0FuaW1hdGlvbk1vZHVsZSxcbiAgVG9hc3RDb250YWluZXJNb2R1bGVcbn0gZnJvbSAnbmd4LXRvYXN0cic7XG5cbmltcG9ydCB7QnJvd3NlckFuaW1hdGlvbnNNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL3BsYXRmb3JtLWJyb3dzZXIvYW5pbWF0aW9ucyc7XG5cbkBOZ01vZHVsZSh7XG4gIGVudHJ5Q29tcG9uZW50czogW1RvYXN0Q29tcG9uZW50XSxcbiAgZGVjbGFyYXRpb25zOiBbVG9hc3RDb21wb25lbnRdLFxuICBpbXBvcnRzOiBbXG4gICAgQ29tbW9uTW9kdWxlLFxuICAgIFRvYXN0Tm9BbmltYXRpb25Nb2R1bGUsXG4gICAgVG9hc3RyTW9kdWxlLmZvclJvb3QoKSxcbiAgICBUb2FzdENvbnRhaW5lck1vZHVsZSxcbiAgICBCcm93c2VyQW5pbWF0aW9uc01vZHVsZSxcbiAgXSxcbiAgZXhwb3J0czogW1RvYXN0Q29tcG9uZW50XSxcbiAgcHJvdmlkZXJzOiBbVG9hc3RTZXJ2aWNlXVxufSlcbmV4cG9ydCBjbGFzcyBUb2FzdE1vZHVsZSB7XG59XG4iXX0=