/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ToastComponent } from './toast.component';
import { timer } from 'rxjs';
import * as i0 from "@angular/core";
import * as i1 from "ngx-toastr";
export class ToastService {
    /**
     * @param {?} toastrService
     */
    constructor(toastrService) {
        this.toastrService = toastrService;
        this.defaultConfig = {
            timeOut: 10000,
            toastComponent: ToastComponent,
            progressBar: true,
            positionClass: 'toast-bottom-right',
            extendedTimeOut: 5000,
        };
    }
    /**
     * @param {?} message
     * @param {?=} title
     * @param {?=} objectAction
     * @return {?}
     */
    success(message, title, objectAction) {
        /** @type {?} */
        const config = this.applyConfig({ message, title, objectAction });
        return this.toastrService.success(config.message, config.title, config);
    }
    /**
     * @param {?} message
     * @param {?} error
     * @param {?=} title
     * @param {?=} objectAction
     * @return {?}
     */
    error(message, error, title, objectAction) {
        /** @type {?} */
        const config = this.applyConfig({
            message,
            title,
            error,
            objectAction,
            disableTimeOut: true,
            preventDuplicates: true,
            progressBar: false,
            autoDismiss: false,
            positionClass: 'toast-center-center',
            tapToDismiss: false,
            closeButton: true
        });
        return this.toastrService.error(config.message, config.title, config);
    }
    /**
     * @param {?} message
     * @param {?=} title
     * @param {?=} objectAction
     * @return {?}
     */
    info(message, title, objectAction) {
        /** @type {?} */
        const disableTimeOut = objectAction != null;
        /** @type {?} */
        const config = this.applyConfig({ message, title, objectAction, disableTimeOut });
        return this.toastrService.info(config.message, config.title, config);
    }
    /**
     * @param {?} message
     * @param {?=} title
     * @param {?=} objectAction
     * @return {?}
     */
    warning(message, title, objectAction) {
        /** @type {?} */
        const disableTimeOut = objectAction != null;
        /** @type {?} */
        const config = this.applyConfig({ message, title, objectAction, disableTimeOut });
        return this.toastrService.warning(config.message, config.title, config);
    }
    /**
     * @param {?} isLoading$
     * @param {?=} message
     * @param {?=} title
     * @param {?=} objectAction
     * @return {?}
     */
    loading(isLoading$, message = 'Carregando...', title, objectAction) {
        if (this.loadingSubscription && this.loadingSubscription.closed === false) {
            if (this.loadingToaster && this.loadingToaster.toastRef) {
                try {
                    this.loadingToaster.toastRef.close();
                }
                catch (error) {
                }
            }
            this.loadingSubscription.unsubscribe();
        }
        this.loadingSubscription = isLoading$.subscribe((/**
         * @param {?} isLoading
         * @return {?}
         */
        isLoading => {
            timer().subscribe((/**
             * @return {?}
             */
            () => {
                if (isLoading) {
                    /** @type {?} */
                    const config = this.applyConfig({
                        message: message === '' ? 'Carregando...' : message,
                        title,
                        objectAction,
                        disableTimeOut: true,
                        preventDuplicates: true,
                        progressBar: false,
                        autoDismiss: false,
                        positionClass: 'toast-top-center',
                        tapToDismiss: false
                    });
                    this.loadingToaster = this.toastrService.show(config.message, config.title, config, 'toast-loading');
                }
                else {
                    if (this.loadingToaster) {
                        this.loadingToaster.toastRef.close();
                    }
                }
            }));
        }));
    }
    /**
     * @param {?=} toastId
     * @return {?}
     */
    clear(toastId) {
        this.toastrService.clear(toastId);
    }
    /**
     * @param {?} config
     * @return {?}
     */
    show(config) {
        config = this.applyConfig(config);
        return this.toastrService.show(config.message, config.title, config);
    }
    /**
     * @private
     * @param {?=} override
     * @return {?}
     */
    applyConfig(override = {}) {
        if (override.objectAction && override.objectAction.display.length > 17) {
            override.objectAction.display = override.objectAction.display.substring(0, 17);
        }
        return Object.assign({}, this.defaultConfig, override);
    }
}
ToastService.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */
ToastService.ctorParameters = () => [
    { type: ToastrService }
];
/** @nocollapse */ ToastService.ngInjectableDef = i0.defineInjectable({ factory: function ToastService_Factory() { return new ToastService(i0.inject(i1.ToastrService)); }, token: ToastService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    ToastService.prototype.loadingSubscription;
    /**
     * @type {?}
     * @private
     */
    ToastService.prototype.loadingToaster;
    /**
     * @type {?}
     * @private
     */
    ToastService.prototype.defaultConfig;
    /**
     * @type {?}
     * @protected
     */
    ToastService.prototype.toastrService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9hc3Quc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvbGF5b3V0L3RvYXN0L3RvYXN0LnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFDLGFBQWEsRUFBQyxNQUFNLFlBQVksQ0FBQztBQUV6QyxPQUFPLEVBQUMsY0FBYyxFQUFDLE1BQU0sbUJBQW1CLENBQUM7QUFFakQsT0FBTyxFQUErQixLQUFLLEVBQUMsTUFBTSxNQUFNLENBQUM7OztBQUd6RCxNQUFNLE9BQU8sWUFBWTs7OztJQVl2QixZQUFzQixhQUE0QjtRQUE1QixrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQVIxQyxrQkFBYSxHQUF5QjtZQUM1QyxPQUFPLEVBQUUsS0FBSztZQUNkLGNBQWMsRUFBRSxjQUFjO1lBQzlCLFdBQVcsRUFBRSxJQUFJO1lBQ2pCLGFBQWEsRUFBRSxvQkFBb0I7WUFDbkMsZUFBZSxFQUFFLElBQUk7U0FDdEIsQ0FBQztJQUdGLENBQUM7Ozs7Ozs7SUFFTSxPQUFPLENBQUMsT0FBZSxFQUFFLEtBQWMsRUFBRSxZQUEwQjs7Y0FDbEUsTUFBTSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsRUFBQyxPQUFPLEVBQUUsS0FBSyxFQUFFLFlBQVksRUFBQyxDQUFDO1FBQy9ELE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQzFFLENBQUM7Ozs7Ozs7O0lBRU0sS0FBSyxDQUFDLE9BQWUsRUFBRSxLQUFZLEVBQUUsS0FBYyxFQUFFLFlBQTBCOztjQUM5RSxNQUFNLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQztZQUM5QixPQUFPO1lBQ1AsS0FBSztZQUNMLEtBQUs7WUFDTCxZQUFZO1lBQ1osY0FBYyxFQUFFLElBQUk7WUFDcEIsaUJBQWlCLEVBQUUsSUFBSTtZQUN2QixXQUFXLEVBQUUsS0FBSztZQUNsQixXQUFXLEVBQUUsS0FBSztZQUNsQixhQUFhLEVBQUUscUJBQXFCO1lBQ3BDLFlBQVksRUFBRSxLQUFLO1lBQ25CLFdBQVcsRUFBRSxJQUFJO1NBQ2xCLENBQUM7UUFFRixPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLEtBQUssRUFBRSxNQUFNLENBQUMsQ0FBQztJQUN4RSxDQUFDOzs7Ozs7O0lBRU0sSUFBSSxDQUFDLE9BQWUsRUFBRSxLQUFjLEVBQUUsWUFBMEI7O2NBQy9ELGNBQWMsR0FBRyxZQUFZLElBQUksSUFBSTs7Y0FDckMsTUFBTSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsRUFBQyxPQUFPLEVBQUUsS0FBSyxFQUFFLFlBQVksRUFBRSxjQUFjLEVBQUMsQ0FBQztRQUMvRSxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLEtBQUssRUFBRSxNQUFNLENBQUMsQ0FBQztJQUN2RSxDQUFDOzs7Ozs7O0lBRU0sT0FBTyxDQUFDLE9BQWUsRUFBRSxLQUFjLEVBQUUsWUFBMEI7O2NBQ2xFLGNBQWMsR0FBRyxZQUFZLElBQUksSUFBSTs7Y0FDckMsTUFBTSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsRUFBQyxPQUFPLEVBQUUsS0FBSyxFQUFFLFlBQVksRUFBRSxjQUFjLEVBQUMsQ0FBQztRQUMvRSxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLEtBQUssRUFBRSxNQUFNLENBQUMsQ0FBQztJQUMxRSxDQUFDOzs7Ozs7OztJQUVNLE9BQU8sQ0FBQyxVQUErQixFQUFFLFVBQWtCLGVBQWUsRUFBRSxLQUFjLEVBQUUsWUFBMEI7UUFDM0gsSUFBSSxJQUFJLENBQUMsbUJBQW1CLElBQUksSUFBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sS0FBSyxLQUFLLEVBQUU7WUFDekUsSUFBSSxJQUFJLENBQUMsY0FBYyxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxFQUFFO2dCQUN2RCxJQUFJO29CQUNGLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRSxDQUFDO2lCQUN0QztnQkFBQyxPQUFPLEtBQUssRUFBRTtpQkFDZjthQUNGO1lBRUQsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQ3hDO1FBRUQsSUFBSSxDQUFDLG1CQUFtQixHQUFHLFVBQVUsQ0FBQyxTQUFTOzs7O1FBQUMsU0FBUyxDQUFDLEVBQUU7WUFDMUQsS0FBSyxFQUFFLENBQUMsU0FBUzs7O1lBQUMsR0FBRyxFQUFFO2dCQUNyQixJQUFJLFNBQVMsRUFBRTs7MEJBRVAsTUFBTSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7d0JBQzlCLE9BQU8sRUFBRSxPQUFPLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLE9BQU87d0JBQ25ELEtBQUs7d0JBQ0wsWUFBWTt3QkFDWixjQUFjLEVBQUUsSUFBSTt3QkFDcEIsaUJBQWlCLEVBQUUsSUFBSTt3QkFDdkIsV0FBVyxFQUFFLEtBQUs7d0JBQ2xCLFdBQVcsRUFBRSxLQUFLO3dCQUNsQixhQUFhLEVBQUUsa0JBQWtCO3dCQUNqQyxZQUFZLEVBQUUsS0FBSztxQkFDcEIsQ0FBQztvQkFFRixJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLEtBQUssRUFBRSxNQUFNLEVBQUUsZUFBZSxDQUFDLENBQUM7aUJBRXRHO3FCQUFNO29CQUNMLElBQUksSUFBSSxDQUFDLGNBQWMsRUFBRTt3QkFDdkIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFLENBQUM7cUJBQ3RDO2lCQUNGO1lBQ0gsQ0FBQyxFQUFDLENBQUM7UUFDTCxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRU0sS0FBSyxDQUFDLE9BQWdCO1FBQzNCLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ3BDLENBQUM7Ozs7O0lBRU0sSUFBSSxDQUFDLE1BQTRCO1FBQ3RDLE1BQU0sR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ2xDLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ3ZFLENBQUM7Ozs7OztJQUVPLFdBQVcsQ0FBQyxXQUFpQyxFQUFFO1FBQ3JELElBQUksUUFBUSxDQUFDLFlBQVksSUFBSSxRQUFRLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsRUFBRSxFQUFFO1lBQ3RFLFFBQVEsQ0FBQyxZQUFZLENBQUMsT0FBTyxHQUFHLFFBQVEsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUM7U0FDaEY7UUFFRCx5QkFBVyxJQUFJLENBQUMsYUFBYSxFQUFLLFFBQVEsRUFBRTtJQUM5QyxDQUFDOzs7WUF6R0YsVUFBVSxTQUFDLEVBQUMsVUFBVSxFQUFFLE1BQU0sRUFBQzs7OztZQU54QixhQUFhOzs7Ozs7OztJQVNuQiwyQ0FBMEM7Ozs7O0lBQzFDLHNDQUFxQzs7Ozs7SUFDckMscUNBTUU7Ozs7O0lBRVUscUNBQXNDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtJbmplY3RhYmxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7VG9hc3RyU2VydmljZX0gZnJvbSAnbmd4LXRvYXN0cic7XG5pbXBvcnQge1RvYXN0Q29uZmlnfSBmcm9tICcuL3RvYXN0LmNvbmZpZyc7XG5pbXBvcnQge1RvYXN0Q29tcG9uZW50fSBmcm9tICcuL3RvYXN0LmNvbXBvbmVudCc7XG5pbXBvcnQge1RvYXN0ZXIsIFRvYXN0QWN0aW9ufSBmcm9tICcuL3RvYXN0LmFjdGlvbic7XG5pbXBvcnQge1N1YnNjcmlwdGlvbiwgT2JzZXJ2YWJsZSwgb2YsIHRpbWVyfSBmcm9tICdyeGpzJztcblxuQEluamVjdGFibGUoe3Byb3ZpZGVkSW46ICdyb290J30pXG5leHBvcnQgY2xhc3MgVG9hc3RTZXJ2aWNlIHtcblxuICBwcml2YXRlIGxvYWRpbmdTdWJzY3JpcHRpb246IFN1YnNjcmlwdGlvbjtcbiAgcHJpdmF0ZSBsb2FkaW5nVG9hc3RlcjogVG9hc3Rlcjxhbnk+O1xuICBwcml2YXRlIGRlZmF1bHRDb25maWc6IFBhcnRpYWw8VG9hc3RDb25maWc+ID0ge1xuICAgIHRpbWVPdXQ6IDEwMDAwLFxuICAgIHRvYXN0Q29tcG9uZW50OiBUb2FzdENvbXBvbmVudCxcbiAgICBwcm9ncmVzc0JhcjogdHJ1ZSxcbiAgICBwb3NpdGlvbkNsYXNzOiAndG9hc3QtYm90dG9tLXJpZ2h0JyxcbiAgICBleHRlbmRlZFRpbWVPdXQ6IDUwMDAsXG4gIH07XG5cbiAgY29uc3RydWN0b3IocHJvdGVjdGVkIHRvYXN0clNlcnZpY2U6IFRvYXN0clNlcnZpY2UpIHtcbiAgfVxuXG4gIHB1YmxpYyBzdWNjZXNzKG1lc3NhZ2U6IHN0cmluZywgdGl0bGU/OiBzdHJpbmcsIG9iamVjdEFjdGlvbj86IFRvYXN0QWN0aW9uKTogVG9hc3Rlcjxhbnk+IHtcbiAgICBjb25zdCBjb25maWcgPSB0aGlzLmFwcGx5Q29uZmlnKHttZXNzYWdlLCB0aXRsZSwgb2JqZWN0QWN0aW9ufSk7XG4gICAgcmV0dXJuIHRoaXMudG9hc3RyU2VydmljZS5zdWNjZXNzKGNvbmZpZy5tZXNzYWdlLCBjb25maWcudGl0bGUsIGNvbmZpZyk7XG4gIH1cblxuICBwdWJsaWMgZXJyb3IobWVzc2FnZTogc3RyaW5nLCBlcnJvcjogRXJyb3IsIHRpdGxlPzogc3RyaW5nLCBvYmplY3RBY3Rpb24/OiBUb2FzdEFjdGlvbik6IFRvYXN0ZXI8YW55PiB7XG4gICAgY29uc3QgY29uZmlnID0gdGhpcy5hcHBseUNvbmZpZyh7XG4gICAgICBtZXNzYWdlLFxuICAgICAgdGl0bGUsXG4gICAgICBlcnJvcixcbiAgICAgIG9iamVjdEFjdGlvbixcbiAgICAgIGRpc2FibGVUaW1lT3V0OiB0cnVlLFxuICAgICAgcHJldmVudER1cGxpY2F0ZXM6IHRydWUsXG4gICAgICBwcm9ncmVzc0JhcjogZmFsc2UsXG4gICAgICBhdXRvRGlzbWlzczogZmFsc2UsXG4gICAgICBwb3NpdGlvbkNsYXNzOiAndG9hc3QtY2VudGVyLWNlbnRlcicsXG4gICAgICB0YXBUb0Rpc21pc3M6IGZhbHNlLFxuICAgICAgY2xvc2VCdXR0b246IHRydWVcbiAgICB9KTtcblxuICAgIHJldHVybiB0aGlzLnRvYXN0clNlcnZpY2UuZXJyb3IoY29uZmlnLm1lc3NhZ2UsIGNvbmZpZy50aXRsZSwgY29uZmlnKTtcbiAgfVxuXG4gIHB1YmxpYyBpbmZvKG1lc3NhZ2U6IHN0cmluZywgdGl0bGU/OiBzdHJpbmcsIG9iamVjdEFjdGlvbj86IFRvYXN0QWN0aW9uKTogVG9hc3Rlcjxhbnk+IHtcbiAgICBjb25zdCBkaXNhYmxlVGltZU91dCA9IG9iamVjdEFjdGlvbiAhPSBudWxsO1xuICAgIGNvbnN0IGNvbmZpZyA9IHRoaXMuYXBwbHlDb25maWcoe21lc3NhZ2UsIHRpdGxlLCBvYmplY3RBY3Rpb24sIGRpc2FibGVUaW1lT3V0fSk7XG4gICAgcmV0dXJuIHRoaXMudG9hc3RyU2VydmljZS5pbmZvKGNvbmZpZy5tZXNzYWdlLCBjb25maWcudGl0bGUsIGNvbmZpZyk7XG4gIH1cblxuICBwdWJsaWMgd2FybmluZyhtZXNzYWdlOiBzdHJpbmcsIHRpdGxlPzogc3RyaW5nLCBvYmplY3RBY3Rpb24/OiBUb2FzdEFjdGlvbik6IFRvYXN0ZXI8YW55PiB7XG4gICAgY29uc3QgZGlzYWJsZVRpbWVPdXQgPSBvYmplY3RBY3Rpb24gIT0gbnVsbDtcbiAgICBjb25zdCBjb25maWcgPSB0aGlzLmFwcGx5Q29uZmlnKHttZXNzYWdlLCB0aXRsZSwgb2JqZWN0QWN0aW9uLCBkaXNhYmxlVGltZU91dH0pO1xuICAgIHJldHVybiB0aGlzLnRvYXN0clNlcnZpY2Uud2FybmluZyhjb25maWcubWVzc2FnZSwgY29uZmlnLnRpdGxlLCBjb25maWcpO1xuICB9XG5cbiAgcHVibGljIGxvYWRpbmcoaXNMb2FkaW5nJDogT2JzZXJ2YWJsZTxib29sZWFuPiwgbWVzc2FnZTogc3RyaW5nID0gJ0NhcnJlZ2FuZG8uLi4nLCB0aXRsZT86IHN0cmluZywgb2JqZWN0QWN0aW9uPzogVG9hc3RBY3Rpb24pOiB2b2lkIHtcbiAgICBpZiAodGhpcy5sb2FkaW5nU3Vic2NyaXB0aW9uICYmIHRoaXMubG9hZGluZ1N1YnNjcmlwdGlvbi5jbG9zZWQgPT09IGZhbHNlKSB7XG4gICAgICBpZiAodGhpcy5sb2FkaW5nVG9hc3RlciAmJiB0aGlzLmxvYWRpbmdUb2FzdGVyLnRvYXN0UmVmKSB7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgdGhpcy5sb2FkaW5nVG9hc3Rlci50b2FzdFJlZi5jbG9zZSgpO1xuICAgICAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIHRoaXMubG9hZGluZ1N1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xuICAgIH1cblxuICAgIHRoaXMubG9hZGluZ1N1YnNjcmlwdGlvbiA9IGlzTG9hZGluZyQuc3Vic2NyaWJlKGlzTG9hZGluZyA9PiB7XG4gICAgICB0aW1lcigpLnN1YnNjcmliZSgoKSA9PiB7XG4gICAgICAgIGlmIChpc0xvYWRpbmcpIHtcblxuICAgICAgICAgIGNvbnN0IGNvbmZpZyA9IHRoaXMuYXBwbHlDb25maWcoe1xuICAgICAgICAgICAgbWVzc2FnZTogbWVzc2FnZSA9PT0gJycgPyAnQ2FycmVnYW5kby4uLicgOiBtZXNzYWdlLFxuICAgICAgICAgICAgdGl0bGUsXG4gICAgICAgICAgICBvYmplY3RBY3Rpb24sXG4gICAgICAgICAgICBkaXNhYmxlVGltZU91dDogdHJ1ZSxcbiAgICAgICAgICAgIHByZXZlbnREdXBsaWNhdGVzOiB0cnVlLFxuICAgICAgICAgICAgcHJvZ3Jlc3NCYXI6IGZhbHNlLFxuICAgICAgICAgICAgYXV0b0Rpc21pc3M6IGZhbHNlLFxuICAgICAgICAgICAgcG9zaXRpb25DbGFzczogJ3RvYXN0LXRvcC1jZW50ZXInLFxuICAgICAgICAgICAgdGFwVG9EaXNtaXNzOiBmYWxzZVxuICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgdGhpcy5sb2FkaW5nVG9hc3RlciA9IHRoaXMudG9hc3RyU2VydmljZS5zaG93KGNvbmZpZy5tZXNzYWdlLCBjb25maWcudGl0bGUsIGNvbmZpZywgJ3RvYXN0LWxvYWRpbmcnKTtcblxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGlmICh0aGlzLmxvYWRpbmdUb2FzdGVyKSB7XG4gICAgICAgICAgICB0aGlzLmxvYWRpbmdUb2FzdGVyLnRvYXN0UmVmLmNsb3NlKCk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxuXG4gIHB1YmxpYyBjbGVhcih0b2FzdElkPzogbnVtYmVyKTogdm9pZCB7XG4gICAgdGhpcy50b2FzdHJTZXJ2aWNlLmNsZWFyKHRvYXN0SWQpO1xuICB9XG5cbiAgcHVibGljIHNob3coY29uZmlnOiBQYXJ0aWFsPFRvYXN0Q29uZmlnPik6IFRvYXN0ZXI8YW55PiB7XG4gICAgY29uZmlnID0gdGhpcy5hcHBseUNvbmZpZyhjb25maWcpO1xuICAgIHJldHVybiB0aGlzLnRvYXN0clNlcnZpY2Uuc2hvdyhjb25maWcubWVzc2FnZSwgY29uZmlnLnRpdGxlLCBjb25maWcpO1xuICB9XG5cbiAgcHJpdmF0ZSBhcHBseUNvbmZpZyhvdmVycmlkZTogUGFydGlhbDxUb2FzdENvbmZpZz4gPSB7fSk6IFBhcnRpYWw8VG9hc3RDb25maWc+IHtcbiAgICBpZiAob3ZlcnJpZGUub2JqZWN0QWN0aW9uICYmIG92ZXJyaWRlLm9iamVjdEFjdGlvbi5kaXNwbGF5Lmxlbmd0aCA+IDE3KSB7XG4gICAgICBvdmVycmlkZS5vYmplY3RBY3Rpb24uZGlzcGxheSA9IG92ZXJyaWRlLm9iamVjdEFjdGlvbi5kaXNwbGF5LnN1YnN0cmluZygwLCAxNyk7XG4gICAgfVxuXG4gICAgcmV0dXJuIHsuLi50aGlzLmRlZmF1bHRDb25maWcsIC4uLm92ZXJyaWRlfTtcbiAgfVxufVxuIl19