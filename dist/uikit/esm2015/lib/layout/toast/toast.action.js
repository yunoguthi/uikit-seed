/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
export class ToastAction {
}
if (false) {
    /** @type {?} */
    ToastAction.prototype.display;
    /** @type {?} */
    ToastAction.prototype.action;
}
/**
 * @record
 * @template T
 */
export function Toaster() { }
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9hc3QuYWN0aW9uLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9sYXlvdXQvdG9hc3QvdG9hc3QuYWN0aW9uLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFFQSxNQUFNLE9BQU8sV0FBVztDQVV2Qjs7O0lBTkMsOEJBQWdCOztJQUtoQiw2QkFBK0I7Ozs7OztBQUdqQyw2QkFFQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEFjdGl2ZVRvYXN0IH0gZnJvbSAnbmd4LXRvYXN0cic7XG5cbmV4cG9ydCBjbGFzcyBUb2FzdEFjdGlvbiB7XG4gIC8qXG4gICAgRGVzY3Jpw6fDo28gZG8gbGluayBkbyBhw6fDo28gbm8gdG9hc3QgLS1Nw6F4aW1vIGRlIGNhcmFjdGVyZXMgMTdcbiAgKi9cbiAgZGlzcGxheTogc3RyaW5nO1xuXG4gIC8qXG4gICAgQcOnw6NvIGEgc2VyIGV4ZWN1dGFkYSBhbyBjbGljYXIgbm8gbGluayBhdHJpYnVpZGEgbm8gZGlzcGxheVxuICAqL1xuICBhY3Rpb246ICgodmFsdWU/OiBhbnkpID0+IGFueSk7XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgVG9hc3RlcjxUPiBleHRlbmRzIEFjdGl2ZVRvYXN0PFQ+IHtcblxufVxuIl19