/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, Renderer2 } from '@angular/core';
import { Toast, ToastrService, ToastPackage } from 'ngx-toastr';
import { Hotkey, HotkeysService } from 'angular2-hotkeys';
export class ToastComponent extends Toast {
    /**
     * @param {?} toastrService
     * @param {?} toastPackage
     * @param {?} renderer
     * @param {?} hotkeysService
     */
    constructor(toastrService, toastPackage, renderer, hotkeysService) {
        super(toastrService, toastPackage);
        this.toastrService = toastrService;
        this.toastPackage = toastPackage;
        this.renderer = renderer;
        this.hotkeysService = hotkeysService;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    action(event) {
        event.stopPropagation();
        this.options.objectAction.action();
        this.toastPackage.triggerAction();
        return false;
    }
    /**
     * @return {?}
     */
    ngAfterViewInit() {
        if (this.options.error) {
            this.hotkeysService.add(new Hotkey('esc', (/**
             * @param {?} event
             * @return {?}
             */
            (event) => {
                this.toastrService.clear(this.toastrService.currentlyActive);
                this.remove();
                return false;
            })));
            /** @type {?} */
            const toast = this.toastrService.toasts.find((/**
             * @param {?} c
             * @return {?}
             */
            c => c.toastId === this.toastPackage.toastId && !c.toastRef.isInactive()));
            if (toast) {
                this.divCenter = this.renderer.createElement('div');
                this.renderer.addClass(this.divCenter, 'block-toast');
                /** @type {?} */
                const existBlockToast = this.toastrService.toasts.some((/**
                 * @param {?} c
                 * @return {?}
                 */
                (c) => this.isBlockBodyToast(c.toastRef)));
                if (!existBlockToast) {
                    this.blockBodyToast = true;
                    document.body.appendChild(this.divCenter);
                }
                toast.onHidden.subscribe((/**
                 * @return {?}
                 */
                () => {
                    if (this.blockBodyToast) {
                        document.body.removeChild(this.divCenter);
                    }
                }));
            }
        }
    }
    /**
     * @param {?} toast
     * @return {?}
     */
    isBlockBodyToast(toast) {
        if (toast && (toast.componentInstance instanceof ToastComponent)) {
            return toast.componentInstance.blockBodyToast;
        }
        return false;
    }
}
ToastComponent.decorators = [
    { type: Component, args: [{
                selector: '[uikit-toast-component]',
                template: "<div *ngIf=\"!options.error\" class=\"toast-content-left\">\n  <div class=\"toast-content-icon\" *ngIf=\"options.icon\">\n    <i [class]=\"options.icon\"></i>\n  </div>\n</div>\n<div *ngIf=\"options.error\" class=\"toast-content-topo\">\n  <p>Ops! Algo aconteceu.</p>\n</div>\n<button *ngIf=\"options.closeButton\" (click)=\"remove()\" class=\"toast-close-button\" aria-label=\"Close\">\n  <span aria-hidden=\"true\">&times;</span>\n</button>\n<div class=\"row\" [style.display]=\"state.value === 'inactive' ? 'none' : ''\">\n  <div class=\"col-9\">\n    <div *ngIf=\"options.title\" [class]=\"options.titleClass\" [attr.aria-label]=\"title\">\n      {{ options.title }}\n    </div>\n    <div *ngIf=\"options.message && options.enableHtml\" role=\"alert\" aria-live=\"polite\" [class]=\"options.messageClass\"\n         [innerHTML]=\"message\">\n    </div>\n    <div *ngIf=\"options.message && !options.enableHtml\" role=\"alert\" aria-live=\"polite\" [class]=\"options.messageClass\"\n         [attr.aria-label]=\"message\">\n      {{ options.message }}\n    </div>\n  </div>\n  <div *ngIf=\"options.objectAction && options.objectAction.display && !options.error\"\n       class=\"col-3 text-right toast-action\">\n    <a class=\"btn btn-link btn-sm\" (click)=\"action($event)\">\n      {{ options.objectAction.display }}\n    </a>\n  </div>\n</div>\n<div *ngIf=\"options.error\">\n  <div class=\"icone\"><i [class]=\"options.icon\"></i></div>\n  <details class=\"toast-error-details\">\n\n    <summary>{{options.error.name}}</summary>\n    <div class=\"toast-details-error\" id=\"style-3\">\n      <p>Message: {{options.error.message}}</p>\n      <p>Stack: {{options.error.stack}}</p>\n    </div>\n  </details>\n</div>\n\n<div *ngIf=\"options.progressBar\">\n  <div class=\"toast-progress\" [style.width]=\"width + '%'\"></div>\n</div>\n",
                animations: [
                    trigger('flyInOut', [
                        state('inactive', style({ opacity: 0 })),
                        state('active', style({ opacity: 1 })),
                        state('removed', style({ opacity: 0 })),
                        transition('inactive => active', animate('{{ easeTime }}ms {{ easing }}')),
                        transition('active => removed', animate('{{ easeTime }}ms {{ easing }}'))
                    ])
                ],
                preserveWhitespaces: false,
                styles: [".btn-link{font-size:11px}.toast-action{text-align:center;text-transform:uppercase}"]
            }] }
];
/** @nocollapse */
ToastComponent.ctorParameters = () => [
    { type: ToastrService },
    { type: ToastPackage },
    { type: Renderer2 },
    { type: HotkeysService }
];
if (false) {
    /** @type {?} */
    ToastComponent.prototype.options;
    /**
     * @type {?}
     * @protected
     */
    ToastComponent.prototype.divCenter;
    /** @type {?} */
    ToastComponent.prototype.blockBodyToast;
    /**
     * @type {?}
     * @protected
     */
    ToastComponent.prototype.toastrService;
    /** @type {?} */
    ToastComponent.prototype.toastPackage;
    /**
     * @type {?}
     * @protected
     */
    ToastComponent.prototype.renderer;
    /** @type {?} */
    ToastComponent.prototype.hotkeysService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9hc3QuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9sYXlvdXQvdG9hc3QvdG9hc3QuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQ0wsT0FBTyxFQUNQLEtBQUssRUFDTCxLQUFLLEVBQ0wsVUFBVSxFQUNWLE9BQU8sRUFDUixNQUFNLHFCQUFxQixDQUFDO0FBQzdCLE9BQU8sRUFBZ0IsU0FBUyxFQUFFLFNBQVMsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUNsRSxPQUFPLEVBQUMsS0FBSyxFQUFFLGFBQWEsRUFBRSxZQUFZLEVBQUMsTUFBTSxZQUFZLENBQUM7QUFHOUQsT0FBTyxFQUFDLE1BQU0sRUFBRSxjQUFjLEVBQUMsTUFBTSxrQkFBa0IsQ0FBQztBQXVCeEQsTUFBTSxPQUFPLGNBQWUsU0FBUSxLQUFLOzs7Ozs7O0lBS3ZDLFlBQ1ksYUFBNEIsRUFDL0IsWUFBMEIsRUFDdkIsUUFBbUIsRUFDdEIsY0FBOEI7UUFFckMsS0FBSyxDQUFDLGFBQWEsRUFBRSxZQUFZLENBQUMsQ0FBQztRQUx6QixrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUMvQixpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUN2QixhQUFRLEdBQVIsUUFBUSxDQUFXO1FBQ3RCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtJQUd2QyxDQUFDOzs7OztJQUVELE1BQU0sQ0FBQyxLQUFZO1FBQ2pCLEtBQUssQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUN4QixJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNuQyxJQUFJLENBQUMsWUFBWSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ2xDLE9BQU8sS0FBSyxDQUFDO0lBQ2YsQ0FBQzs7OztJQUVELGVBQWU7UUFDYixJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFO1lBRXRCLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLElBQUksTUFBTSxDQUFDLEtBQUs7Ozs7WUFBRSxDQUFDLEtBQW9CLEVBQVcsRUFBRTtnQkFDMUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxlQUFlLENBQUMsQ0FBQztnQkFDN0QsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO2dCQUNkLE9BQU8sS0FBSyxDQUFDO1lBQ2YsQ0FBQyxFQUFDLENBQUMsQ0FBQzs7a0JBRUUsS0FBSyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLElBQUk7Ozs7WUFBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLEtBQUssSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLElBQUksQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSxFQUFDO1lBRXRILElBQUksS0FBSyxFQUFFO2dCQUNULElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3BELElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsYUFBYSxDQUFDLENBQUM7O3NCQUVoRCxlQUFlLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsSUFBSTs7OztnQkFBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsRUFBQztnQkFFaEcsSUFBSSxDQUFDLGVBQWUsRUFBRTtvQkFDcEIsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7b0JBQzNCLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztpQkFDM0M7Z0JBRUQsS0FBSyxDQUFDLFFBQVEsQ0FBQyxTQUFTOzs7Z0JBQUMsR0FBRyxFQUFFO29CQUM1QixJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUU7d0JBQ3ZCLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztxQkFDM0M7Z0JBQ0gsQ0FBQyxFQUFDLENBQUM7YUFDSjtTQUNGO0lBQ0gsQ0FBQzs7Ozs7SUFFRCxnQkFBZ0IsQ0FBQyxLQUFvQjtRQUNuQyxJQUFJLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsWUFBWSxjQUFjLENBQUMsRUFBRTtZQUNoRSxPQUFPLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLENBQUM7U0FDL0M7UUFFRCxPQUFPLEtBQUssQ0FBQztJQUNmLENBQUM7OztZQS9FRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLHlCQUF5QjtnQkFDbkMsMnpEQUFxQztnQkFFckMsVUFBVSxFQUFFO29CQUNWLE9BQU8sQ0FBQyxVQUFVLEVBQUU7d0JBQ2xCLEtBQUssQ0FBQyxVQUFVLEVBQUUsS0FBSyxDQUFDLEVBQUMsT0FBTyxFQUFFLENBQUMsRUFBQyxDQUFDLENBQUM7d0JBQ3RDLEtBQUssQ0FBQyxRQUFRLEVBQUUsS0FBSyxDQUFDLEVBQUMsT0FBTyxFQUFFLENBQUMsRUFBQyxDQUFDLENBQUM7d0JBQ3BDLEtBQUssQ0FBQyxTQUFTLEVBQUUsS0FBSyxDQUFDLEVBQUMsT0FBTyxFQUFFLENBQUMsRUFBQyxDQUFDLENBQUM7d0JBQ3JDLFVBQVUsQ0FDUixvQkFBb0IsRUFDcEIsT0FBTyxDQUFDLCtCQUErQixDQUFDLENBQ3pDO3dCQUNELFVBQVUsQ0FDUixtQkFBbUIsRUFDbkIsT0FBTyxDQUFDLCtCQUErQixDQUFDLENBQ3pDO3FCQUNGLENBQUM7aUJBQ0g7Z0JBQ0QsbUJBQW1CLEVBQUUsS0FBSzs7YUFDM0I7Ozs7WUF6QmMsYUFBYTtZQUFFLFlBQVk7WUFEUixTQUFTO1lBSTNCLGNBQWM7Ozs7SUF3QjVCLGlDQUFxQjs7Ozs7SUFDckIsbUNBQXlCOztJQUN6Qix3Q0FBd0I7Ozs7O0lBR3RCLHVDQUFzQzs7SUFDdEMsc0NBQWlDOzs7OztJQUNqQyxrQ0FBNkI7O0lBQzdCLHdDQUFxQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XG4gIGFuaW1hdGUsXG4gIHN0YXRlLFxuICBzdHlsZSxcbiAgdHJhbnNpdGlvbixcbiAgdHJpZ2dlclxufSBmcm9tICdAYW5ndWxhci9hbmltYXRpb25zJztcbmltcG9ydCB7QWZ0ZXJWaWV3SW5pdCwgQ29tcG9uZW50LCBSZW5kZXJlcjJ9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtUb2FzdCwgVG9hc3RyU2VydmljZSwgVG9hc3RQYWNrYWdlfSBmcm9tICduZ3gtdG9hc3RyJztcbmltcG9ydCB7VG9hc3RDb25maWd9IGZyb20gJy4vdG9hc3QuY29uZmlnJztcbmltcG9ydCB7VG9hc3RSZWZ9IGZyb20gJ25neC10b2FzdHIvdG9hc3RyL3RvYXN0LWluamVjdG9yJztcbmltcG9ydCB7SG90a2V5LCBIb3RrZXlzU2VydmljZX0gZnJvbSAnYW5ndWxhcjItaG90a2V5cyc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ1t1aWtpdC10b2FzdC1jb21wb25lbnRdJyxcbiAgdGVtcGxhdGVVcmw6ICcuL3RvYXN0LmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vdG9hc3QuY29tcG9uZW50LnNjc3MnXSxcbiAgYW5pbWF0aW9uczogW1xuICAgIHRyaWdnZXIoJ2ZseUluT3V0JywgW1xuICAgICAgc3RhdGUoJ2luYWN0aXZlJywgc3R5bGUoe29wYWNpdHk6IDB9KSksXG4gICAgICBzdGF0ZSgnYWN0aXZlJywgc3R5bGUoe29wYWNpdHk6IDF9KSksXG4gICAgICBzdGF0ZSgncmVtb3ZlZCcsIHN0eWxlKHtvcGFjaXR5OiAwfSkpLFxuICAgICAgdHJhbnNpdGlvbihcbiAgICAgICAgJ2luYWN0aXZlID0+IGFjdGl2ZScsXG4gICAgICAgIGFuaW1hdGUoJ3t7IGVhc2VUaW1lIH19bXMge3sgZWFzaW5nIH19JylcbiAgICAgICksXG4gICAgICB0cmFuc2l0aW9uKFxuICAgICAgICAnYWN0aXZlID0+IHJlbW92ZWQnLFxuICAgICAgICBhbmltYXRlKCd7eyBlYXNlVGltZSB9fW1zIHt7IGVhc2luZyB9fScpXG4gICAgICApXG4gICAgXSlcbiAgXSxcbiAgcHJlc2VydmVXaGl0ZXNwYWNlczogZmFsc2UsXG59KVxuZXhwb3J0IGNsYXNzIFRvYXN0Q29tcG9uZW50IGV4dGVuZHMgVG9hc3QgaW1wbGVtZW50cyBBZnRlclZpZXdJbml0IHtcbiAgb3B0aW9uczogVG9hc3RDb25maWc7XG4gIHByb3RlY3RlZCBkaXZDZW50ZXI6IGFueTtcbiAgYmxvY2tCb2R5VG9hc3Q6IGJvb2xlYW47XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJvdGVjdGVkIHRvYXN0clNlcnZpY2U6IFRvYXN0clNlcnZpY2UsXG4gICAgcHVibGljIHRvYXN0UGFja2FnZTogVG9hc3RQYWNrYWdlLFxuICAgIHByb3RlY3RlZCByZW5kZXJlcjogUmVuZGVyZXIyLFxuICAgIHB1YmxpYyBob3RrZXlzU2VydmljZTogSG90a2V5c1NlcnZpY2UsXG4gICkge1xuICAgIHN1cGVyKHRvYXN0clNlcnZpY2UsIHRvYXN0UGFja2FnZSk7XG4gIH1cblxuICBhY3Rpb24oZXZlbnQ6IEV2ZW50KSB7XG4gICAgZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgdGhpcy5vcHRpb25zLm9iamVjdEFjdGlvbi5hY3Rpb24oKTtcbiAgICB0aGlzLnRvYXN0UGFja2FnZS50cmlnZ2VyQWN0aW9uKCk7XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG5cbiAgbmdBZnRlclZpZXdJbml0KCk6IHZvaWQge1xuICAgIGlmICh0aGlzLm9wdGlvbnMuZXJyb3IpIHtcblxuICAgICAgdGhpcy5ob3RrZXlzU2VydmljZS5hZGQobmV3IEhvdGtleSgnZXNjJywgKGV2ZW50OiBLZXlib2FyZEV2ZW50KTogYm9vbGVhbiA9PiB7XG4gICAgICAgIHRoaXMudG9hc3RyU2VydmljZS5jbGVhcih0aGlzLnRvYXN0clNlcnZpY2UuY3VycmVudGx5QWN0aXZlKTtcbiAgICAgICAgdGhpcy5yZW1vdmUoKTtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgfSkpO1xuXG4gICAgICBjb25zdCB0b2FzdCA9IHRoaXMudG9hc3RyU2VydmljZS50b2FzdHMuZmluZChjID0+IGMudG9hc3RJZCA9PT0gdGhpcy50b2FzdFBhY2thZ2UudG9hc3RJZCAmJiAhYy50b2FzdFJlZi5pc0luYWN0aXZlKCkpO1xuXG4gICAgICBpZiAodG9hc3QpIHtcbiAgICAgICAgdGhpcy5kaXZDZW50ZXIgPSB0aGlzLnJlbmRlcmVyLmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xuICAgICAgICB0aGlzLnJlbmRlcmVyLmFkZENsYXNzKHRoaXMuZGl2Q2VudGVyLCAnYmxvY2stdG9hc3QnKTtcblxuICAgICAgICBjb25zdCBleGlzdEJsb2NrVG9hc3QgPSB0aGlzLnRvYXN0clNlcnZpY2UudG9hc3RzLnNvbWUoKGMpID0+IHRoaXMuaXNCbG9ja0JvZHlUb2FzdChjLnRvYXN0UmVmKSk7XG5cbiAgICAgICAgaWYgKCFleGlzdEJsb2NrVG9hc3QpIHtcbiAgICAgICAgICB0aGlzLmJsb2NrQm9keVRvYXN0ID0gdHJ1ZTtcbiAgICAgICAgICBkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKHRoaXMuZGl2Q2VudGVyKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHRvYXN0Lm9uSGlkZGVuLnN1YnNjcmliZSgoKSA9PiB7XG4gICAgICAgICAgaWYgKHRoaXMuYmxvY2tCb2R5VG9hc3QpIHtcbiAgICAgICAgICAgIGRvY3VtZW50LmJvZHkucmVtb3ZlQ2hpbGQodGhpcy5kaXZDZW50ZXIpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgaXNCbG9ja0JvZHlUb2FzdCh0b2FzdDogVG9hc3RSZWY8YW55Pik6IGJvb2xlYW4ge1xuICAgIGlmICh0b2FzdCAmJiAodG9hc3QuY29tcG9uZW50SW5zdGFuY2UgaW5zdGFuY2VvZiBUb2FzdENvbXBvbmVudCkpIHtcbiAgICAgIHJldHVybiB0b2FzdC5jb21wb25lbnRJbnN0YW5jZS5ibG9ja0JvZHlUb2FzdDtcbiAgICB9XG5cbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cbn1cbiJdfQ==