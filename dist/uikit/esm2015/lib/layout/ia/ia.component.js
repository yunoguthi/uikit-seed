/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ChangeDetectionStrategy } from '@angular/core';
import { NavQuery } from './../nav/state/nav.query';
import { Subscription } from 'rxjs';
import { NavService } from './../nav/state/nav.service';
export class IaComponent {
    /**
     * @param {?} navQuery
     * @param {?} navService
     */
    constructor(navQuery, navService) {
        this.navQuery = navQuery;
        this.navService = navService;
        this.rightnav$ = this.navQuery.rightnav$;
        this.subscription = new Subscription();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.subscription.add(this.rightnav$.subscribe((/**
         * @param {?} rightnav
         * @return {?}
         */
        rightnav => {
            this.rightnav = rightnav;
        })));
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
    /**
     * @return {?}
     */
    rightNavToggle() {
        this.navService.toggleRightNav();
    }
    /**
     * @return {?}
     */
    rightNavOpen() {
        this.navService.openRightNav();
    }
    /**
     * @return {?}
     */
    rightNavClose() {
        this.navService.closeRightNav();
    }
    /**
     * @return {?}
     */
    togglePinRightNav() {
        this.navService.togglePinRightNav();
    }
}
IaComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-ia',
                template: "<mat-toolbar>\n  <button\n    class=\"aikit-button\"\n    mat-flat-button\n    type=\"button\"\n    aria-label=\"Abri menu de navega\u00E7\u00E3o\"\n    aria-controls=\"navigation\"\n    matTooltip=\"Abri menu de navega\u00E7\u00E3o\"\n    (click)=\"rightNavToggle()\"\n    tabindex=\"-1\"\n  >\n    <mat-icon class=\"fas fa-times\" aria-label=\"Fixar menu na tela\"></mat-icon>\n  </button>\n\n  <button\n    mat-icon-button\n    color=\"primary\"\n    class=\"btn-attach-chat\"\n    (click)=\"togglePinRightNav()\"\n    [class.is-active]=\"(rightnav$ | async).opened\"\n    [class.is-pinned]=\"(rightnav$ | async).pinned\"\n    tabindex=\"-1\"\n  >\n    <mat-icon\n      class=\"fas fa-thumbtack\"\n      aria-label=\"Fixar menu na tela\"\n    ></mat-icon>\n  </button>\n\n  <img src=\"../../assets/images/iakit-logotipo-mobile.svg\" class=\"is-mobile\" />\n  <img src=\"../../assets/images/iakit-logotipo.svg\" class=\"is-desktop\" />\n</mat-toolbar>\n\n<div class=\"chat\">\n  <div class=\"messages\">\n    <div class=\"message left\">\n      <picture> </picture>\n      <div>Oi, meu nome \u00E9 Judi, em que posso ajudar?</div>\n    </div>\n\n    <div class=\"message right\">\n      <picture>\n        <mat-icon\n          aria-label=\"Example icon-button with a heart icon\"\n          class=\"fas fa-user\"\n        ></mat-icon>\n      </picture>\n      <div>Mensagem do Usu\u00E1rio</div>\n    </div>\n  </div>\n</div>\n\n<div class=\"actions\">\n  <form>\n    <mat-form-field color=\"primary\" appearance=\"fill\">\n      <mat-label>Criar uma nova anota\u00E7\u00E3o</mat-label>\n      <input matInput #message maxlength=\"256\" placeholder=\"Message\" cdkFocusInitial />\n    </mat-form-field>\n    <button mat-flat-button color=\"primary\">ENVIAR</button>\n  </form>\n</div>\n",
                changeDetection: ChangeDetectionStrategy.OnPush,
                styles: [""]
            }] }
];
/** @nocollapse */
IaComponent.ctorParameters = () => [
    { type: NavQuery },
    { type: NavService }
];
if (false) {
    /** @type {?} */
    IaComponent.prototype.rightnav$;
    /** @type {?} */
    IaComponent.prototype.rightnav;
    /**
     * @type {?}
     * @protected
     */
    IaComponent.prototype.subscription;
    /**
     * @type {?}
     * @protected
     */
    IaComponent.prototype.navQuery;
    /**
     * @type {?}
     * @protected
     */
    IaComponent.prototype.navService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaWEuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9sYXlvdXQvaWEvaWEuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLHVCQUF1QixFQUFhLE1BQU0sZUFBZSxDQUFDO0FBQ3RGLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUNwRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ3BDLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQVF4RCxNQUFNLE9BQU8sV0FBVzs7Ozs7SUFNdEIsWUFDWSxRQUFrQixFQUNsQixVQUFzQjtRQUR0QixhQUFRLEdBQVIsUUFBUSxDQUFVO1FBQ2xCLGVBQVUsR0FBVixVQUFVLENBQVk7UUFOM0IsY0FBUyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDO1FBRWpDLGlCQUFZLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztJQUlOLENBQUM7Ozs7SUFFdkMsUUFBUTtRQUNOLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUNuQixJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVM7Ozs7UUFBQyxRQUFRLENBQUMsRUFBRTtZQUNsQyxJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztRQUMzQixDQUFDLEVBQUMsQ0FDSCxDQUFDO0lBQ0osQ0FBQzs7OztJQUdELFdBQVc7UUFDVCxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQ2xDLENBQUM7Ozs7SUFFRCxjQUFjO1FBQ1osSUFBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLEVBQUUsQ0FBQztJQUNuQyxDQUFDOzs7O0lBQ0QsWUFBWTtRQUNWLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDakMsQ0FBQzs7OztJQUNELGFBQWE7UUFDWCxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ2xDLENBQUM7Ozs7SUFDRCxpQkFBaUI7UUFDZixJQUFJLENBQUMsVUFBVSxDQUFDLGlCQUFpQixFQUFFLENBQUM7SUFDdEMsQ0FBQzs7O1lBeENGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsVUFBVTtnQkFDcEIsd3dEQUFrQztnQkFFbEMsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07O2FBQ2hEOzs7O1lBVFEsUUFBUTtZQUVSLFVBQVU7Ozs7SUFVakIsZ0NBQTJDOztJQUMzQywrQkFBc0Q7Ozs7O0lBQ3RELG1DQUE0Qzs7Ozs7SUFHMUMsK0JBQTRCOzs7OztJQUM1QixpQ0FBZ0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3ksIE9uRGVzdHJveSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTmF2UXVlcnkgfSBmcm9tICcuLy4uL25hdi9zdGF0ZS9uYXYucXVlcnknO1xuaW1wb3J0IHsgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBOYXZTZXJ2aWNlIH0gZnJvbSAnLi8uLi9uYXYvc3RhdGUvbmF2LnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICd1aWtpdC1pYScsXG4gIHRlbXBsYXRlVXJsOiAnLi9pYS5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2lhLmNvbXBvbmVudC5zY3NzJ10sXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoXG59KVxuZXhwb3J0IGNsYXNzIElhQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xuXG4gIHB1YmxpYyByaWdodG5hdiQgPSB0aGlzLm5hdlF1ZXJ5LnJpZ2h0bmF2JDtcbiAgcHVibGljIHJpZ2h0bmF2OiB7IG9wZW5lZDogYm9vbGVhbjsgcGlubmVkOiBib29sZWFuIH07XG4gIHByb3RlY3RlZCBzdWJzY3JpcHRpb24gPSBuZXcgU3Vic2NyaXB0aW9uKCk7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJvdGVjdGVkIG5hdlF1ZXJ5OiBOYXZRdWVyeSxcbiAgICBwcm90ZWN0ZWQgbmF2U2VydmljZTogTmF2U2VydmljZSkgeyB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgdGhpcy5zdWJzY3JpcHRpb24uYWRkKFxuICAgICAgdGhpcy5yaWdodG5hdiQuc3Vic2NyaWJlKHJpZ2h0bmF2ID0+IHtcbiAgICAgICAgdGhpcy5yaWdodG5hdiA9IHJpZ2h0bmF2O1xuICAgICAgfSlcbiAgICApO1xuICB9XG5cblxuICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcbiAgICB0aGlzLnN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xuICB9XG5cbiAgcmlnaHROYXZUb2dnbGUoKSB7XG4gICAgdGhpcy5uYXZTZXJ2aWNlLnRvZ2dsZVJpZ2h0TmF2KCk7XG4gIH1cbiAgcmlnaHROYXZPcGVuKCkge1xuICAgIHRoaXMubmF2U2VydmljZS5vcGVuUmlnaHROYXYoKTtcbiAgfVxuICByaWdodE5hdkNsb3NlKCkge1xuICAgIHRoaXMubmF2U2VydmljZS5jbG9zZVJpZ2h0TmF2KCk7XG4gIH1cbiAgdG9nZ2xlUGluUmlnaHROYXYoKSB7XG4gICAgdGhpcy5uYXZTZXJ2aWNlLnRvZ2dsZVBpblJpZ2h0TmF2KCk7XG4gIH1cblxufVxuIl19