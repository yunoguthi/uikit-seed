/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { ScrollDispatcher } from '@angular/cdk/overlay';
import { DOCUMENT } from '@angular/common';
import { ChangeDetectionStrategy, Component, ContentChildren, ElementRef, Inject, Input, QueryList, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material';
import { ActivatedRoute, NavigationEnd, NavigationStart, Router } from '@angular/router';
import { Hotkey, HotkeysService } from 'angular2-hotkeys';
import { BehaviorSubject, merge, Subscription, timer } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { UpdateInfoService } from '../update/update-info.service';
import { UpdateService } from '../update/update.service';
import { NotificacaoComponent } from './header/notification/notificacao/notificacao.component';
import { LayoutType } from './layout-type.model';
import { LayoutService } from './layout.service';
import { BreadcrumbService } from './nav/breadcrumb/breadcrumbs/breadcrumb.service';
import { FavNavsQuery } from './nav/favnav/state/favnavs.query';
import { FavNavsService } from './nav/favnav/state/favnavs.service';
import { NavQuery } from './nav/state/nav.query';
import { NavService } from './nav/state/nav.service';
import { ToastService } from './toast/toast.service';
import { FormBuilder } from '@angular/forms';
import { UikitRrippleService } from "../utils/uikit-ripple.service";
import { PreviousRouteService } from '../components/previous-route.service';
export class LayoutComponent {
    /**
     * @param {?} navQuery
     * @param {?} favNavsQuery
     * @param {?} navService
     * @param {?} scroll
     * @param {?} layoutService
     * @param {?} router
     * @param {?} activatedRoute
     * @param {?} updateInfoService
     * @param {?} updateService
     * @param {?} favNavsService
     * @param {?} hotkeysService
     * @param {?} breadcrumbService
     * @param {?} toastService
     * @param {?} uikitRrippleService
     * @param {?} fb
     * @param {?} document
     * @param {?} previousRouteService
     */
    constructor(navQuery, favNavsQuery, navService, scroll, layoutService, router, activatedRoute, updateInfoService, updateService, favNavsService, hotkeysService, breadcrumbService, toastService, uikitRrippleService, fb, document, previousRouteService) {
        this.navQuery = navQuery;
        this.favNavsQuery = favNavsQuery;
        this.navService = navService;
        this.scroll = scroll;
        this.layoutService = layoutService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.updateInfoService = updateInfoService;
        this.updateService = updateService;
        this.favNavsService = favNavsService;
        this.hotkeysService = hotkeysService;
        this.breadcrumbService = breadcrumbService;
        this.toastService = toastService;
        this.uikitRrippleService = uikitRrippleService;
        this.fb = fb;
        this.document = document;
        this.previousRouteService = previousRouteService;
        this.hasActionsSubject = new BehaviorSubject(false);
        this.hasActions$ = this.hasActionsSubject.asObservable();
        this.showShell = this.layoutService.showShell$;
        this.hiddenBreadcrumb = this.layoutService.showBreadcrumb$;
        this.showFullScrren = this.layoutService.showFullScreen$;
        this.hiddenShell = this.layoutService.hiddenShell$;
        this.showNotifications = false;
        this.showSystemInfo = false;
        this.showUserInfo = false;
        this.showEnvironment = false;
        this.showIa = false;
        this.environment = null;
        this.allowedTypes = [
            'normal',
            'fullscreen',
            'noshell',
            'noshellfullscreen',
            'noshellnobreadcrumb',
            'noshellnobreadcrumbfullscreen'
        ];
        this.defaultType = 'normal';
        this.showShortcuts = true;
        this.subscription = new Subscription();
        this.isFixed$ = this.layoutService.isFixed$;
        this.isMobile$ = this.layoutService.isMobile$;
        this.rightnav$ = this.navQuery.rightnav$;
        this.leftnav$ = this.navQuery.leftnav$;
        this.data$ = new BehaviorSubject(null);
        this.possuiRecursoDeFavoritos = this.favNavsService.possuiRecursoDeFavoritos;
        this.notificacoes$ = this.layoutService.notificacoes$;
        this.hasInstallOption$ = this.updateInfoService.hasInstallOption$;
        this.hasUpdate$ = this.updateInfoService.hasUpdate$;
        this.favorites$ = this.favNavsQuery.favorites$;
    }
    /**
     * @return {?}
     */
    ngAfterContentInit() {
        this.notificacoesProjetadas.changes.subscribe((/**
         * @return {?}
         */
        () => {
            /** @type {?} */
            const notificacoesModel = this.notificacoesProjetadas.toArray().map((/**
             * @param {?} r
             * @return {?}
             */
            r => {
                return {
                    nome: r.nome,
                    icone: r.icone,
                    descricao: r.descricao,
                    data: r.data,
                    read: r.read
                };
            }));
            this.layoutService.setNotificacoes(notificacoesModel);
        }));
        this.notificacoesProjetadas.notifyOnChanges();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.uikitRrippleService.init();
        this.subscription.add(this.leftnav$.subscribe((/**
         * @param {?} leftnav
         * @return {?}
         */
        leftnav => {
            this.leftnav = leftnav;
        })));
        this.subscription.add((this.closeWhenNotPinnedLeftSubscription = this.router.events
            .pipe(filter((/**
         * @param {?} event
         * @return {?}
         */
        event => event instanceof NavigationStart)))
            .subscribe((/**
         * @param {?} event
         * @return {?}
         */
        event => {
            if (this.leftnav.pinned === false) {
                this.navService.closeLeftNav();
            }
        }))));
        this.subscription.add(this.rightnav$.subscribe((/**
         * @param {?} rightnav
         * @return {?}
         */
        rightnav => {
            this.rightnav = rightnav;
        })));
        this.subscription.add((this.closeWhenNotPinnedRightSubscription = this.router.events
            .pipe(filter((/**
         * @param {?} event
         * @return {?}
         */
        event => event instanceof NavigationStart)))
            .subscribe((/**
         * @param {?} event
         * @return {?}
         */
        event => {
            if (this.rightnav.pinned === false) {
                this.navService.closeRightNav();
            }
        }))));
        /** @type {?} */
        const nagivate = this.router.events.pipe(filter((/**
         * @param {?} event
         * @return {?}
         */
        event => event instanceof NavigationEnd)));
        /** @type {?} */
        const params = this.activatedRoute.params;
        merge(nagivate, params).pipe(map((/**
         * @return {?}
         */
        () => {
            /** @type {?} */
            let child = this.activatedRoute.firstChild;
            while (child) {
                if (child.firstChild) {
                    child = child.firstChild;
                }
                else if (child.snapshot.data) {
                    return child.snapshot.data;
                }
                else {
                    return null;
                }
            }
            return null;
        })))
            .subscribe((/**
         * @param {?} customData
         * @return {?}
         */
        customData => {
            /** @type {?} */
            let route = this.router.routerState.root.snapshot;
            while (route.firstChild != null) {
                route = route.firstChild;
            }
            /** @type {?} */
            const data = route.data;
            /** @type {?} */
            const favItem = this.getFavItem(data);
            // console.log('layout - nagivate, params', favItem);
            // favItem.icon = customData ? customData.icon : null;
            this.data$.next(favItem);
        }));
        // When connection become offline, then put the entire app with grayscale (0.8)
        window.addEventListener('online', (/**
         * @return {?}
         */
        () => {
            document.querySelector('body').style.filter = '';
            if (localStorage.getItem('offline') === 'true') {
                timer(600).subscribe((/**
                 * @return {?}
                 */
                () => this.toastService.success('Conexão de rede restabelecida!')));
                localStorage.removeItem('offline');
            }
        }));
        window.addEventListener('offline', (/**
         * @return {?}
         */
        () => {
            document.querySelector('body').style.filter = 'grayscale(0.8)';
            localStorage.setItem('offline', 'true');
            timer(600).subscribe((/**
             * @return {?}
             */
            () => this.toastService.warning('Sem conexão de rede!')));
        }));
        this.subscription.add((this.scrollSubscription = this.scroll
            .scrolled()
            .pipe(map((/**
         * @param {?} data
         * @return {?}
         */
        (data) => {
            if (data) {
                /** @type {?} */
                const element = data.getElementRef();
                if (element) {
                    return element.nativeElement.scrollTop;
                }
            }
        })))
            .subscribe((/**
         * @param {?} scrollY
         * @return {?}
         */
        (scrollY) => {
            if (scrollY) {
                this.layoutService.setFixed(scrollY > 0);
            }
        }))));
        this.router.events
            .pipe(filter((/**
         * @param {?} event
         * @return {?}
         */
        event => event instanceof NavigationEnd)))
            .subscribe((/**
         * @return {?}
         */
        () => {
            this.setLayoutTypeByRouteEvent();
            this.updateIfHasActions();
        }));
        setTimeout((/**
         * @return {?}
         */
        () => {
            this.updateIfHasActions();
        }), 0);
    }
    /**
     * @return {?}
     */
    toggleMobile() {
        this.layoutService.toggleMobile();
    }
    /**
     * @return {?}
     */
    updateIfHasActions() {
        /** @type {?} */
        const elements = document.getElementsByClassName('uikit-actions');
        /** @type {?} */
        const element = elements.item(0);
        if (element != null) {
            /** @type {?} */
            const hasChildNodes = element.hasChildNodes();
            this.hasActionsSubject.next(hasChildNodes);
        }
        else {
            this.hasActionsSubject.next(false);
        }
    }
    /**
     * @return {?}
     */
    ngAfterViewInit() {
        this.hotkeysService.add(new Hotkey('ctrl+m', (/**
         * @param {?} event
         * @return {?}
         */
        (event) => {
            this.leftNavToggle();
            return false;
        })));
        this.hotkeysService.add(new Hotkey('ctrl+alt+j', (/**
         * @param {?} event
         * @return {?}
         */
        (event) => {
            this.rightNavToggle();
            return false;
        })));
        this.isVerifyTypeShellApply();
    }
    /**
     * @private
     * @param {?} customData
     * @return {?}
     */
    getFavItem(customData) {
        /** @type {?} */
        const data = customData;
        /** @type {?} */
        const lastBreadCrumb = this.breadcrumbService.breadcrumbs[this.breadcrumbService.breadcrumbs.length - 1];
        if (lastBreadCrumb != null) {
            /** @type {?} */
            const title = lastBreadCrumb.title;
            return { link: this.router.url, title, icon: data ? data.icon : null };
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
    /**
     * @return {?}
     */
    leftNavToggle() {
        this.navService.toggleLeftNav();
    }
    /**
     * @return {?}
     */
    leftNavClose() {
        this.navService.closeLeftNav();
    }
    /**
     * @return {?}
     */
    rightNavToggle() {
        this.navService.toggleRightNav();
    }
    /**
     * @return {?}
     */
    rightNavClose() {
        this.navService.closeRightNav();
    }
    /**
     * @return {?}
     */
    navsClose() {
        if (this.leftnav.opened && !this.leftnav.pinned) {
            this.navService.toggleLeftNav();
        }
        if (this.rightnav.opened && !this.rightnav.pinned) {
            this.navService.toggleRightNav();
        }
    }
    /**
     * @return {?}
     */
    install() {
        this.updateInfoService.install();
    }
    /**
     * @return {?}
     */
    update() {
        this.updateService.update();
    }
    /**
     * @return {?}
     */
    isVerifyTypeShellApply() {
        this.showFullScrren.subscribe((/**
         * @param {?} isShowFullScreen
         * @return {?}
         */
        (isShowFullScreen) => {
            if (isShowFullScreen) {
                /** @type {?} */
                const fsDocElem = (/** @type {?} */ (this.document.documentElement));
                // document.documentElement as FsDocumentElement;
                this.openFullscreen(fsDocElem);
            }
        }));
    }
    /**
     * @param {?} fsDocElem
     * @return {?}
     */
    openFullscreen(fsDocElem) {
        /*debugger;*/
        if (fsDocElem.requestFullscreen) {
            timer(600).subscribe((/**
             * @return {?}
             */
            () => {
                fsDocElem.requestFullscreen().catch((/**
                 * @param {?} error
                 * @return {?}
                 */
                error => console.error(error)));
            }));
        }
        else if (fsDocElem.msRequestFullscreen) {
            try {
                timer(600).subscribe((/**
                 * @return {?}
                 */
                () => {
                    fsDocElem.msRequestFullscreen().catch((/**
                     * @param {?} error
                     * @return {?}
                     */
                    error => console.error(error)));
                }));
            }
            catch (error) {
                console.error(error);
            }
        }
        else if (fsDocElem.mozRequestFullScreen) {
            try {
                timer(600).subscribe((/**
                 * @return {?}
                 */
                () => {
                    fsDocElem.mozRequestFullScreen().catch((/**
                     * @param {?} error
                     * @return {?}
                     */
                    error => console.error(error)));
                }));
            }
            catch (error) {
                console.error(error);
            }
        }
        else if (fsDocElem.webkitRequestFullscreen) {
            try {
                timer(600).subscribe((/**
                 * @return {?}
                 */
                () => {
                    fsDocElem.webkitRequestFullscreen().catch((/**
                     * @param {?} error
                     * @return {?}
                     */
                    error => console.error(error)));
                }));
            }
            catch (error) {
                console.error(error);
            }
        }
        timer(900).subscribe((/**
         * @return {?}
         */
        () => {
            ((/** @type {?} */ (this.myClickFullscreen.nativeElement))).click();
        }));
    }
    /**
     * @private
     * @return {?}
     */
    closeFullscreen() {
        if (this.document.exitFullscreen) {
            this.document.exitFullscreen();
        }
        else if (this.document.mozCancelFullScreen) {
            /* Firefox */
            this.document.mozCancelFullScreen();
        }
        else if (this.document.webkitExitFullscreen) {
            /* Chrome, Safari and Opera */
            this.document.webkitExitFullscreen();
        }
        else if (this.document.msExitFullscreen) {
            /* IE/Edge */
            this.document.msExitFullscreen();
        }
    }
    /**
     * Region RouteLayoutApplication
     * Modo de apresentação das tela aplicado de forma totalmente configurável a forma geral e por rota.
     * **
     * @return {?}
     */
    setLayoutTypeByRouteEvent() {
        /** @type {?} */
        const queryType = this.router.routerState.root.snapshot.queryParamMap.get('type');
        /** @type {?} */
        const dataRota = this.getData();
        /** @type {?} */
        let typeDesejado = 'normal';
        if (queryType) {
            // usuario informou via url (tem prioridade sobre todos)
            typeDesejado = queryType;
        }
        else if (dataRota && dataRota.defaultType) {
            // esta configurado na rota (tem prioridade somente sobre o global - sobrescrita)
            typeDesejado = dataRota.defaultType;
        }
        else {
            typeDesejado = this.defaultType;
            // busca o global
        }
        if (typeDesejado === 'noshell') {
            this.navService.toggleLeftNav();
        }
        /** @type {?} */
        const defaultTypeRoute = (dataRota && dataRota.defaultType) ? dataRota.defaultType : this.defaultType;
        if (dataRota && dataRota.allowedTypes && dataRota.allowedTypes.length > 0) {
            // existe configuracao de tipos permitidos na rota
            /** @type {?} */
            const ehPermitidoPelaRota = dataRota.allowedTypes.some((/**
             * @param {?} type
             * @return {?}
             */
            type => type === typeDesejado));
            if (ehPermitidoPelaRota) {
                this.layoutService.setType(LayoutType[typeDesejado]);
            }
            else {
                console.error(`O parametro ${typeDesejado} não está definido como permitido, favor verificar!`);
                this.layoutService.setType(LayoutType[defaultTypeRoute]);
            }
        }
        else {
            // não existe configuração de tipos permitidos na rota devo então ver do componente layout (global da aplicação)
            /** @type {?} */
            const allowedTypesLayout = this.allowedTypes;
            /** @type {?} */
            const ehPermitidoGlobalmente = allowedTypesLayout.some((/**
             * @param {?} type
             * @return {?}
             */
            type => type === typeDesejado));
            if (ehPermitidoGlobalmente) {
                this.layoutService.setType(LayoutType[typeDesejado]);
            }
            else {
                console.error(`O parametro ${typeDesejado} não está definido como permitido, favor verificar!`);
                this.layoutService.setType(LayoutType[defaultTypeRoute]);
            }
        }
        if ((typeDesejado === 'normal' || defaultTypeRoute === 'normal') && this.document.fullscreenElement) {
            this.closeFullscreen();
        }
    }
    /**
     * @private
     * @return {?}
     */
    getData() {
        /** @type {?} */
        let route = this.router.routerState.root.snapshot;
        while (route.firstChild != null) {
            route = route.firstChild;
            if (route.routeConfig === null) {
                continue;
            }
            if (!route.routeConfig.path) {
                continue;
            }
            if (!route.data.defaultType) {
                continue;
            }
            return (/** @type {?} */ (route.data));
        }
    }
}
LayoutComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-layout',
                template: "<button #myClickFullscreen style=\"display: none;\"></button>\n<mat-sidenav-container\n  attr.data-environment=\"{{ environment }}\" [class.environment]=\"!showEnvironment\" *ngIf=\"(showShell | async)\"\n  [hasBackdrop]=\"(!(leftnav$ | async).pinned && (leftnav$ | async).opened) || (!(rightnav$ | async).pinned && (rightnav$ | async).opened)\"\n  (backdropClick)=\"navsClose();\"\n  [class.is-fixed]=\"(isFixed$ | async)\"\n  [class.is-mobileAction]=\"(isMobile$ | async)\"\n  [class.is-rightPinned]=\"(rightnav$ | async).pinned\" [class.is-leftPinned]=\"(leftnav$ | async).pinned\"\n  [class.is-favorite]=\"(possuiRecursoDeFavoritos) && (favorites$ | async).length > 0\">\n  <mat-sidenav class=\"uikit-nav\" #sidenav position=\"start\" disableClose=\"true\"\n               *ngIf=\"(hiddenShell | async)\"\n               [mode]=\"(leftnav$ | async).pinned ? 'side' : 'over'\" [opened]=\"(leftnav$ | async).opened\"\n               (keydown.escape)=\"leftNavClose()\" [fixedInViewport]=\"true\">\n    <uikit-nav>\n      <div nav-menu>\n        <ng-content select=\"[uikit-menu]\"></ng-content>\n        <ng-content select=\"[layout-nav-menu-extras]\"></ng-content>\n      </div>\n    </uikit-nav>\n  </mat-sidenav>\n\n  <mat-sidenav *ngIf=\"showIa\"\n               #aikitSideNav class=\"aikit-nav\" disableClose=\"true\" position=\"end\"\n               [opened]=\"(rightnav$ | async).opened\"\n               [mode]=\"(rightnav$ | async).pinned ? 'side' : 'over'\"\n               (keydown.escape)=\"rightNavClose()\" [fixedInViewport]=\"false\">\n\n    <uikit-ia></uikit-ia>\n\n  </mat-sidenav>\n  <mat-sidenav-content cdkScrollable [ngClass]=\"{\n  'noshell': !(hiddenShell | async),\n  'noshellnobreadcrumb': !(hiddenBreadcrumb | async)\n  }\">\n    <uikit-header [showNotifications]=\"showNotifications\"\n                  [showSystemInfo]=\"showSystemInfo\"\n                  [showUserInfo]=\"showUserInfo\" [showIa]=\"showIa\" [showShortcuts]=\"showShortcuts\">\n\n      <div header-notifications>\n        <ng-container *ngTemplateOutlet=\"notificacoes\"></ng-container>\n      </div>\n\n      <div header-systeminfo>\n        <ng-content select=\"[layout-header-systeminfo]\"></ng-content>\n\n        <mat-action-list *ngIf=\"(hasInstallOption$ | async) || (hasUpdate$ | async)\">\n          <!-- <mat-divider></mat-divider>-->\n          <a mat-list-item (click)=\"install()\" *ngIf=\"(hasInstallOption$ | async)\">Instalar sistema</a>\n          <a mat-list-item (click)=\"update()\" *ngIf=\"(hasUpdate$ | async)\">Atualizar vers\u00E3o</a>\n        </mat-action-list>\n      </div>\n\n      <div header-userinfo>\n\n        <ng-content select=\"[layout-header-userinfo]\"></ng-content>\n\n      </div>\n\n    </uikit-header>\n    <uikit-breadcrumb *ngIf=\"(hiddenBreadcrumb | async)\"></uikit-breadcrumb>\n\n    <div [hidden]=\"!(hasActions$ | async)\">\n      <button class=\"nav-mobile is-mobile\" mat-fab (click)=\"toggleMobile()\">\n        <mat-icon class=\"fas fa-ellipsis-h\" [class.fa-times]=\"(isMobile$ | async)\"></mat-icon>\n      </button>\n    </div>\n\n    <ng-container *ngTemplateOutlet=\"projecao\"></ng-container>\n  </mat-sidenav-content>\n</mat-sidenav-container>\n\n<ng-template *ngIf=\"(showShell | async)\">\n  <ng-container *ngTemplateOutlet=\"projecao\"></ng-container>\n</ng-template>\n\n<ng-template #projecao>\n  <div style=\"height: 100%\" [ngClass]=\"{'noshell' : !(showShell | async),\n                   'noshellnobreadcrumb': !(hiddenBreadcrumb | async)}\">\n    <ng-content></ng-content>\n  </div>\n</ng-template>\n\n<div hidden>\n  <ng-content select=\"[layout-nav-menu]\"></ng-content>\n</div>\n<div hidden>\n  <ng-content select=\"[layout-header-notifications]\"></ng-content>\n</div>\n\n<ng-template #notificacoes>\n  <ng-container *ngFor=\"let notificacao of (notificacoes$ | async)\">\n    <uikit-notificacao [nome]=\"notificacao.nome\" [icone]=\"notificacao.icone\" [descricao]=\"notificacao.descricao\"\n                       [data]=\"notificacao.data\" [read]=\"notificacao.read\"></uikit-notificacao>\n  </ng-container>\n</ng-template>\n\n",
                changeDetection: ChangeDetectionStrategy.OnPush,
                styles: [""]
            }] }
];
/** @nocollapse */
LayoutComponent.ctorParameters = () => [
    { type: NavQuery },
    { type: FavNavsQuery },
    { type: NavService },
    { type: ScrollDispatcher },
    { type: LayoutService },
    { type: Router },
    { type: ActivatedRoute },
    { type: UpdateInfoService },
    { type: UpdateService },
    { type: FavNavsService },
    { type: HotkeysService },
    { type: BreadcrumbService },
    { type: ToastService },
    { type: UikitRrippleService },
    { type: FormBuilder },
    { type: undefined, decorators: [{ type: Inject, args: [DOCUMENT,] }] },
    { type: PreviousRouteService }
];
LayoutComponent.propDecorators = {
    myClickFullscreen: [{ type: ViewChild, args: ['myClickFullscreen',] }],
    showNotifications: [{ type: Input }],
    showSystemInfo: [{ type: Input }],
    showUserInfo: [{ type: Input }],
    showEnvironment: [{ type: Input }],
    showIa: [{ type: Input }],
    environment: [{ type: Input }],
    allowedTypes: [{ type: Input }],
    defaultType: [{ type: Input }],
    showShortcuts: [{ type: Input }],
    notificacoesProjetadas: [{ type: ContentChildren, args: [NotificacaoComponent, { descendants: false },] }],
    sidenav: [{ type: ViewChild, args: [MatSidenav,] }]
};
if (false) {
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.hasActionsSubject;
    /** @type {?} */
    LayoutComponent.prototype.hasActions$;
    /** @type {?} */
    LayoutComponent.prototype.myClickFullscreen;
    /** @type {?} */
    LayoutComponent.prototype.showShell;
    /** @type {?} */
    LayoutComponent.prototype.hiddenBreadcrumb;
    /** @type {?} */
    LayoutComponent.prototype.showFullScrren;
    /** @type {?} */
    LayoutComponent.prototype.hiddenShell;
    /** @type {?} */
    LayoutComponent.prototype.showNotifications;
    /** @type {?} */
    LayoutComponent.prototype.showSystemInfo;
    /** @type {?} */
    LayoutComponent.prototype.showUserInfo;
    /** @type {?} */
    LayoutComponent.prototype.showEnvironment;
    /** @type {?} */
    LayoutComponent.prototype.showIa;
    /** @type {?} */
    LayoutComponent.prototype.environment;
    /** @type {?} */
    LayoutComponent.prototype.allowedTypes;
    /** @type {?} */
    LayoutComponent.prototype.defaultType;
    /** @type {?} */
    LayoutComponent.prototype.showShortcuts;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.subscription;
    /** @type {?} */
    LayoutComponent.prototype.isFixed$;
    /** @type {?} */
    LayoutComponent.prototype.isMobile$;
    /** @type {?} */
    LayoutComponent.prototype.rightnav$;
    /** @type {?} */
    LayoutComponent.prototype.rightnav;
    /** @type {?} */
    LayoutComponent.prototype.leftnav$;
    /** @type {?} */
    LayoutComponent.prototype.leftnav;
    /** @type {?} */
    LayoutComponent.prototype.data$;
    /** @type {?} */
    LayoutComponent.prototype.possuiRecursoDeFavoritos;
    /**
     * @type {?}
     * @private
     */
    LayoutComponent.prototype.scrollSubscription;
    /** @type {?} */
    LayoutComponent.prototype.notificacoesProjetadas;
    /** @type {?} */
    LayoutComponent.prototype.sidenav;
    /** @type {?} */
    LayoutComponent.prototype.notificacoes$;
    /**
     * @type {?}
     * @private
     */
    LayoutComponent.prototype.closeWhenNotPinnedLeftSubscription;
    /**
     * @type {?}
     * @private
     */
    LayoutComponent.prototype.closeWhenNotPinnedRightSubscription;
    /** @type {?} */
    LayoutComponent.prototype.hasInstallOption$;
    /** @type {?} */
    LayoutComponent.prototype.hasUpdate$;
    /** @type {?} */
    LayoutComponent.prototype.favorites$;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.navQuery;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.favNavsQuery;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.navService;
    /** @type {?} */
    LayoutComponent.prototype.scroll;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.layoutService;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.router;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.activatedRoute;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.updateInfoService;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.updateService;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.favNavsService;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.hotkeysService;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.breadcrumbService;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.toastService;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.uikitRrippleService;
    /**
     * @type {?}
     * @private
     */
    LayoutComponent.prototype.fb;
    /**
     * @type {?}
     * @private
     */
    LayoutComponent.prototype.document;
    /**
     * @type {?}
     * @protected
     */
    LayoutComponent.prototype.previousRouteService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGF5b3V0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvbGF5b3V0L2xheW91dC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBZ0IsZ0JBQWdCLEVBQUMsTUFBTSxzQkFBc0IsQ0FBQztBQUNyRSxPQUFPLEVBQUMsUUFBUSxFQUFDLE1BQU0saUJBQWlCLENBQUM7QUFDekMsT0FBTyxFQUdMLHVCQUF1QixFQUN2QixTQUFTLEVBQ1QsZUFBZSxFQUNmLFVBQVUsRUFDVixNQUFNLEVBQ04sS0FBSyxFQUdMLFNBQVMsRUFDVCxTQUFTLEVBQ1YsTUFBTSxlQUFlLENBQUM7QUFDdkIsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLG1CQUFtQixDQUFDO0FBQzdDLE9BQU8sRUFBQyxjQUFjLEVBQUUsYUFBYSxFQUFFLGVBQWUsRUFBRSxNQUFNLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUV2RixPQUFPLEVBQUMsTUFBTSxFQUFFLGNBQWMsRUFBQyxNQUFNLGtCQUFrQixDQUFDO0FBQ3hELE9BQU8sRUFBQyxlQUFlLEVBQUUsS0FBSyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUMsTUFBTSxNQUFNLENBQUM7QUFDakUsT0FBTyxFQUFDLE1BQU0sRUFBRSxHQUFHLEVBQUMsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzQyxPQUFPLEVBQUMsaUJBQWlCLEVBQUMsTUFBTSwrQkFBK0IsQ0FBQztBQUNoRSxPQUFPLEVBQUMsYUFBYSxFQUFDLE1BQU0sMEJBQTBCLENBQUM7QUFDdkQsT0FBTyxFQUFDLG9CQUFvQixFQUFDLE1BQU0seURBQXlELENBQUM7QUFDN0YsT0FBTyxFQUFvQixVQUFVLEVBQUMsTUFBTSxxQkFBcUIsQ0FBQztBQUNsRSxPQUFPLEVBQUMsYUFBYSxFQUFDLE1BQU0sa0JBQWtCLENBQUM7QUFDL0MsT0FBTyxFQUFDLGlCQUFpQixFQUFDLE1BQU0saURBQWlELENBQUM7QUFDbEYsT0FBTyxFQUFDLFlBQVksRUFBQyxNQUFNLGtDQUFrQyxDQUFDO0FBQzlELE9BQU8sRUFBQyxjQUFjLEVBQUMsTUFBTSxvQ0FBb0MsQ0FBQztBQUVsRSxPQUFPLEVBQUMsUUFBUSxFQUFDLE1BQU0sdUJBQXVCLENBQUM7QUFDL0MsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLHlCQUF5QixDQUFDO0FBQ25ELE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSx1QkFBdUIsQ0FBQztBQUNuRCxPQUFPLEVBQUMsV0FBVyxFQUFDLE1BQU0sZ0JBQWdCLENBQUM7QUFDM0MsT0FBTyxFQUFDLG1CQUFtQixFQUFDLE1BQU0sK0JBQStCLENBQUM7QUFFbEUsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFRNUUsTUFBTSxPQUFPLGVBQWU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lBMkQxQixZQUNZLFFBQWtCLEVBQ2xCLFlBQTBCLEVBQzFCLFVBQXNCLEVBQ3pCLE1BQXdCLEVBQ3JCLGFBQTRCLEVBQzVCLE1BQWMsRUFDZCxjQUE4QixFQUM5QixpQkFBb0MsRUFDcEMsYUFBNEIsRUFDNUIsY0FBOEIsRUFDOUIsY0FBOEIsRUFDOUIsaUJBQW9DLEVBQ3BDLFlBQTBCLEVBQzFCLG1CQUF3QyxFQUMxQyxFQUFlLEVBQ0csUUFBYSxFQUM3QixvQkFBMEM7UUFoQjFDLGFBQVEsR0FBUixRQUFRLENBQVU7UUFDbEIsaUJBQVksR0FBWixZQUFZLENBQWM7UUFDMUIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUN6QixXQUFNLEdBQU4sTUFBTSxDQUFrQjtRQUNyQixrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUM1QixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBbUI7UUFDcEMsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFDNUIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixzQkFBaUIsR0FBakIsaUJBQWlCLENBQW1CO1FBQ3BDLGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBQzFCLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFDMUMsT0FBRSxHQUFGLEVBQUUsQ0FBYTtRQUNHLGFBQVEsR0FBUixRQUFRLENBQUs7UUFDN0IseUJBQW9CLEdBQXBCLG9CQUFvQixDQUFzQjtRQTFFNUMsc0JBQWlCLEdBQUcsSUFBSSxlQUFlLENBQVUsS0FBSyxDQUFDLENBQUM7UUFDM0QsZ0JBQVcsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsWUFBWSxFQUFFLENBQUM7UUFJcEQsY0FBUyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDO1FBQzFDLHFCQUFnQixHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsZUFBZSxDQUFDO1FBQ3RELG1CQUFjLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxlQUFlLENBQUM7UUFDcEQsZ0JBQVcsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBQztRQUVyQyxzQkFBaUIsR0FBRyxLQUFLLENBQUM7UUFDMUIsbUJBQWMsR0FBRyxLQUFLLENBQUM7UUFDdkIsaUJBQVksR0FBRyxLQUFLLENBQUM7UUFDckIsb0JBQWUsR0FBRyxLQUFLLENBQUM7UUFDeEIsV0FBTSxHQUFHLEtBQUssQ0FBQztRQUNmLGdCQUFXLEdBQVcsSUFBSSxDQUFDO1FBQzNCLGlCQUFZLEdBQUc7WUFDN0IsUUFBUTtZQUNSLFlBQVk7WUFDWixTQUFTO1lBQ1QsbUJBQW1CO1lBQ25CLHFCQUFxQjtZQUNyQiwrQkFBK0I7U0FDaEMsQ0FBQztRQUNjLGdCQUFXLEdBQUcsUUFBUSxDQUFDO1FBQ3ZCLGtCQUFhLEdBQUcsSUFBSSxDQUFDO1FBRTNCLGlCQUFZLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUVyQyxhQUFRLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUM7UUFDdkMsY0FBUyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDO1FBRXpDLGNBQVMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQztRQUVwQyxhQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUM7UUFHbEMsVUFBSyxHQUFHLElBQUksZUFBZSxDQUFNLElBQUksQ0FBQyxDQUFDO1FBRTlDLDZCQUF3QixHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsd0JBQXdCLENBQUM7UUFTakUsa0JBQWEsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQztRQUtqRCxzQkFBaUIsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsaUJBQWlCLENBQUM7UUFDN0QsZUFBVSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUM7UUFDL0MsZUFBVSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDO0lBcUJqRCxDQUFDOzs7O0lBRUQsa0JBQWtCO1FBRWhCLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxPQUFPLENBQUMsU0FBUzs7O1FBQUMsR0FBRyxFQUFFOztrQkFDM0MsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixDQUFDLE9BQU8sRUFBRSxDQUFDLEdBQUc7Ozs7WUFBQyxDQUFDLENBQUMsRUFBRTtnQkFDdEUsT0FBTztvQkFDTCxJQUFJLEVBQUUsQ0FBQyxDQUFDLElBQUk7b0JBQ1osS0FBSyxFQUFFLENBQUMsQ0FBQyxLQUFLO29CQUNkLFNBQVMsRUFBRSxDQUFDLENBQUMsU0FBUztvQkFDdEIsSUFBSSxFQUFFLENBQUMsQ0FBQyxJQUFJO29CQUNaLElBQUksRUFBRSxDQUFDLENBQUMsSUFBSTtpQkFDYixDQUFDO1lBQ0osQ0FBQyxFQUFDO1lBQ0YsSUFBSSxDQUFDLGFBQWEsQ0FBQyxlQUFlLENBQUMsaUJBQWlCLENBQUMsQ0FBQztRQUN4RCxDQUFDLEVBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxlQUFlLEVBQUUsQ0FBQztJQUNoRCxDQUFDOzs7O0lBRUQsUUFBUTtRQUNOLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUVoQyxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FDbkIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTOzs7O1FBQUMsT0FBTyxDQUFDLEVBQUU7WUFDaEMsSUFBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7UUFDekIsQ0FBQyxFQUFDLENBQ0gsQ0FBQztRQUVGLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUNuQixDQUFDLElBQUksQ0FBQyxrQ0FBa0MsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU07YUFDMUQsSUFBSSxDQUFDLE1BQU07Ozs7UUFBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEtBQUssWUFBWSxlQUFlLEVBQUMsQ0FBQzthQUN2RCxTQUFTOzs7O1FBQUMsS0FBSyxDQUFDLEVBQUU7WUFDakIsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sS0FBSyxLQUFLLEVBQUU7Z0JBQ2pDLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxFQUFFLENBQUM7YUFDaEM7UUFDSCxDQUFDLEVBQUMsQ0FBQyxDQUNOLENBQUM7UUFFRixJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FDbkIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTOzs7O1FBQUMsUUFBUSxDQUFDLEVBQUU7WUFDbEMsSUFBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7UUFDM0IsQ0FBQyxFQUFDLENBQ0gsQ0FBQztRQUVGLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUNuQixDQUFDLElBQUksQ0FBQyxtQ0FBbUMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU07YUFDM0QsSUFBSSxDQUFDLE1BQU07Ozs7UUFBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEtBQUssWUFBWSxlQUFlLEVBQUMsQ0FBQzthQUN2RCxTQUFTOzs7O1FBQUMsS0FBSyxDQUFDLEVBQUU7WUFDakIsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sS0FBSyxLQUFLLEVBQUU7Z0JBQ2xDLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxFQUFFLENBQUM7YUFDakM7UUFDSCxDQUFDLEVBQUMsQ0FBQyxDQUNOLENBQUM7O2NBRUksUUFBUSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNOzs7O1FBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxLQUFLLFlBQVksYUFBYSxFQUFDLENBQUM7O2NBQ25GLE1BQU0sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU07UUFFekMsS0FBSyxDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQzFCLEdBQUc7OztRQUFDLEdBQUcsRUFBRTs7Z0JBQ0gsS0FBSyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsVUFBVTtZQUMxQyxPQUFPLEtBQUssRUFBRTtnQkFDWixJQUFJLEtBQUssQ0FBQyxVQUFVLEVBQUU7b0JBQ3BCLEtBQUssR0FBRyxLQUFLLENBQUMsVUFBVSxDQUFDO2lCQUMxQjtxQkFBTSxJQUFJLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFO29CQUM5QixPQUFPLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDO2lCQUM1QjtxQkFBTTtvQkFDTCxPQUFPLElBQUksQ0FBQztpQkFDYjthQUNGO1lBQ0QsT0FBTyxJQUFJLENBQUM7UUFDZCxDQUFDLEVBQUMsQ0FBQzthQUNGLFNBQVM7Ozs7UUFBQyxVQUFVLENBQUMsRUFBRTs7Z0JBQ2xCLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUTtZQUNqRCxPQUFPLEtBQUssQ0FBQyxVQUFVLElBQUksSUFBSSxFQUFFO2dCQUMvQixLQUFLLEdBQUcsS0FBSyxDQUFDLFVBQVUsQ0FBQzthQUMxQjs7a0JBQ0ssSUFBSSxHQUFHLEtBQUssQ0FBQyxJQUFJOztrQkFHakIsT0FBTyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDO1lBQ3JDLHFEQUFxRDtZQUNyRCxzREFBc0Q7WUFDdEQsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDM0IsQ0FBQyxFQUFDLENBQUM7UUFFTCwrRUFBK0U7UUFDL0UsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFFBQVE7OztRQUFFLEdBQUcsRUFBRTtZQUNyQyxRQUFRLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDO1lBQ2pELElBQUksWUFBWSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsS0FBSyxNQUFNLEVBQUU7Z0JBQzlDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxTQUFTOzs7Z0JBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsZ0NBQWdDLENBQUMsRUFBQyxDQUFDO2dCQUN4RixZQUFZLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2FBQ3BDO1FBQ0gsQ0FBQyxFQUFDLENBQUM7UUFDSCxNQUFNLENBQUMsZ0JBQWdCLENBQUMsU0FBUzs7O1FBQUUsR0FBRyxFQUFFO1lBQ3RDLFFBQVEsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxnQkFBZ0IsQ0FBQztZQUMvRCxZQUFZLENBQUMsT0FBTyxDQUFDLFNBQVMsRUFBRSxNQUFNLENBQUMsQ0FBQztZQUN4QyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsU0FBUzs7O1lBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsc0JBQXNCLENBQUMsRUFBQyxDQUFDO1FBQ2hGLENBQUMsRUFBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQ25CLENBQUMsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxNQUFNO2FBQ25DLFFBQVEsRUFBRTthQUNWLElBQUksQ0FDSCxHQUFHOzs7O1FBQ0QsQ0FBQyxJQUFtQixFQUFFLEVBQUU7WUFDdEIsSUFBSSxJQUFJLEVBQUU7O3NCQUNGLE9BQU8sR0FBRyxJQUFJLENBQUMsYUFBYSxFQUFFO2dCQUNwQyxJQUFJLE9BQU8sRUFBRTtvQkFDWCxPQUFPLE9BQU8sQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDO2lCQUN4QzthQUNGO1FBQ0gsQ0FBQyxFQUNGLENBQ0Y7YUFDQSxTQUFTOzs7O1FBQUMsQ0FBQyxPQUFlLEVBQUUsRUFBRTtZQUM3QixJQUFJLE9BQU8sRUFBRTtnQkFDWCxJQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDLENBQUM7YUFDMUM7UUFDSCxDQUFDLEVBQUMsQ0FBQyxDQUNOLENBQUM7UUFFRixJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU07YUFDZixJQUFJLENBQUMsTUFBTTs7OztRQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsS0FBSyxZQUFZLGFBQWEsRUFBQyxDQUFDO2FBQ3JELFNBQVM7OztRQUFDLEdBQUcsRUFBRTtZQUNkLElBQUksQ0FBQyx5QkFBeUIsRUFBRSxDQUFDO1lBQ2pDLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1FBQzVCLENBQUMsRUFBQyxDQUFDO1FBQ0wsVUFBVTs7O1FBQUMsR0FBRyxFQUFFO1lBQ2QsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7UUFDNUIsQ0FBQyxHQUFFLENBQUMsQ0FBQyxDQUFDO0lBQ1IsQ0FBQzs7OztJQUVELFlBQVk7UUFDVixJQUFJLENBQUMsYUFBYSxDQUFDLFlBQVksRUFBRSxDQUFDO0lBQ3BDLENBQUM7Ozs7SUFFRCxrQkFBa0I7O2NBQ1YsUUFBUSxHQUFHLFFBQVEsQ0FBQyxzQkFBc0IsQ0FBQyxlQUFlLENBQUM7O2NBQzNELE9BQU8sR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztRQUNoQyxJQUFJLE9BQU8sSUFBSSxJQUFJLEVBQUU7O2tCQUNiLGFBQWEsR0FBRyxPQUFPLENBQUMsYUFBYSxFQUFFO1lBQzdDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7U0FDNUM7YUFBTTtZQUNMLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDcEM7SUFDSCxDQUFDOzs7O0lBRUQsZUFBZTtRQUViLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLElBQUksTUFBTSxDQUFDLFFBQVE7Ozs7UUFBRSxDQUFDLEtBQW9CLEVBQVcsRUFBRTtZQUM3RSxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7WUFDckIsT0FBTyxLQUFLLENBQUM7UUFDZixDQUFDLEVBQUMsQ0FBQyxDQUFDO1FBQ0osSUFBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsSUFBSSxNQUFNLENBQUMsWUFBWTs7OztRQUFFLENBQUMsS0FBb0IsRUFBVyxFQUFFO1lBQ2pGLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUN0QixPQUFPLEtBQUssQ0FBQztRQUNmLENBQUMsRUFBQyxDQUFDLENBQUM7UUFFSixJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztJQUNoQyxDQUFDOzs7Ozs7SUFFTyxVQUFVLENBQUMsVUFBVTs7Y0FDckIsSUFBSSxHQUFHLFVBQVU7O2NBQ2pCLGNBQWMsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztRQUN4RyxJQUFJLGNBQWMsSUFBSSxJQUFJLEVBQUU7O2tCQUNwQixLQUFLLEdBQUcsY0FBYyxDQUFDLEtBQUs7WUFDbEMsT0FBTyxFQUFDLElBQUksRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFDLENBQUM7U0FDdEU7SUFDSCxDQUFDOzs7O0lBRUQsV0FBVztRQUNULElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDbEMsQ0FBQzs7OztJQUVELGFBQWE7UUFDWCxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ2xDLENBQUM7Ozs7SUFFRCxZQUFZO1FBQ1YsSUFBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUNqQyxDQUFDOzs7O0lBRUQsY0FBYztRQUNaLElBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxFQUFFLENBQUM7SUFDbkMsQ0FBQzs7OztJQUVELGFBQWE7UUFDWCxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ2xDLENBQUM7Ozs7SUFFRCxTQUFTO1FBQ1AsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFO1lBQy9DLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxFQUFFLENBQUM7U0FDakM7UUFFRCxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUU7WUFDakQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLEVBQUUsQ0FBQztTQUNsQztJQUNILENBQUM7Ozs7SUFFRCxPQUFPO1FBQ0wsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQ25DLENBQUM7Ozs7SUFFRCxNQUFNO1FBQ0osSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEVBQUUsQ0FBQztJQUM5QixDQUFDOzs7O0lBRU0sc0JBQXNCO1FBRTNCLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUzs7OztRQUFDLENBQUMsZ0JBQXlCLEVBQUUsRUFBRTtZQUMxRCxJQUFJLGdCQUFnQixFQUFFOztzQkFDZCxTQUFTLEdBQUcsbUJBQUEsSUFBSSxDQUFDLFFBQVEsQ0FBQyxlQUFlLEVBQXFCO2dCQUNwRSxpREFBaUQ7Z0JBQ2pELElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLENBQUM7YUFDaEM7UUFDSCxDQUFDLEVBQUMsQ0FBQztJQUVMLENBQUM7Ozs7O0lBRU0sY0FBYyxDQUFDLFNBQWM7UUFDbEMsYUFBYTtRQUNiLElBQUksU0FBUyxDQUFDLGlCQUFpQixFQUFFO1lBQy9CLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxTQUFTOzs7WUFBQyxHQUFHLEVBQUU7Z0JBQ3hCLFNBQVMsQ0FBQyxpQkFBaUIsRUFBRSxDQUFDLEtBQUs7Ozs7Z0JBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxFQUFDLENBQUM7WUFDckUsQ0FBQyxFQUFDLENBQUM7U0FDSjthQUFNLElBQUksU0FBUyxDQUFDLG1CQUFtQixFQUFFO1lBQ3hDLElBQUk7Z0JBQ0YsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLFNBQVM7OztnQkFBQyxHQUFHLEVBQUU7b0JBQ3hCLFNBQVMsQ0FBQyxtQkFBbUIsRUFBRSxDQUFDLEtBQUs7Ozs7b0JBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxFQUFDLENBQUM7Z0JBQ3ZFLENBQUMsRUFBQyxDQUFDO2FBQ0o7WUFBQyxPQUFPLEtBQUssRUFBRTtnQkFDZCxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ3RCO1NBQ0Y7YUFBTSxJQUFJLFNBQVMsQ0FBQyxvQkFBb0IsRUFBRTtZQUN6QyxJQUFJO2dCQUNGLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxTQUFTOzs7Z0JBQUMsR0FBRyxFQUFFO29CQUN4QixTQUFTLENBQUMsb0JBQW9CLEVBQUUsQ0FBQyxLQUFLOzs7O29CQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsRUFBQyxDQUFDO2dCQUN4RSxDQUFDLEVBQUMsQ0FBQzthQUNKO1lBQUMsT0FBTyxLQUFLLEVBQUU7Z0JBQ2QsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUN0QjtTQUNGO2FBQU0sSUFBSSxTQUFTLENBQUMsdUJBQXVCLEVBQUU7WUFDNUMsSUFBSTtnQkFDRixLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsU0FBUzs7O2dCQUFDLEdBQUcsRUFBRTtvQkFDeEIsU0FBUyxDQUFDLHVCQUF1QixFQUFFLENBQUMsS0FBSzs7OztvQkFBQyxLQUFLLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEVBQUMsQ0FBQztnQkFDM0UsQ0FBQyxFQUFDLENBQUM7YUFDSjtZQUFDLE9BQU8sS0FBSyxFQUFFO2dCQUNkLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDdEI7U0FDRjtRQUNELEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxTQUFTOzs7UUFBQyxHQUFHLEVBQUU7WUFDeEIsQ0FBQyxtQkFBQSxJQUFJLENBQUMsaUJBQWlCLENBQUMsYUFBYSxFQUFlLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNoRSxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRU8sZUFBZTtRQUNyQixJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsY0FBYyxFQUFFO1lBQ2hDLElBQUksQ0FBQyxRQUFRLENBQUMsY0FBYyxFQUFFLENBQUM7U0FDaEM7YUFBTSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsbUJBQW1CLEVBQUU7WUFDNUMsYUFBYTtZQUNiLElBQUksQ0FBQyxRQUFRLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztTQUNyQzthQUFNLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxvQkFBb0IsRUFBRTtZQUM3Qyw4QkFBOEI7WUFDOUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO1NBQ3RDO2FBQU0sSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLGdCQUFnQixFQUFFO1lBQ3pDLGFBQWE7WUFDYixJQUFJLENBQUMsUUFBUSxDQUFDLGdCQUFnQixFQUFFLENBQUM7U0FDbEM7SUFDSCxDQUFDOzs7Ozs7O0lBS00seUJBQXlCOztjQUN4QixTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQzs7Y0FDM0UsUUFBUSxHQUFHLElBQUksQ0FBQyxPQUFPLEVBQUU7O1lBRTNCLFlBQVksR0FBRyxRQUFRO1FBQzNCLElBQUksU0FBUyxFQUFFO1lBQ2Isd0RBQXdEO1lBQ3hELFlBQVksR0FBRyxTQUFTLENBQUM7U0FDMUI7YUFBTSxJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsV0FBVyxFQUFFO1lBQzNDLGlGQUFpRjtZQUNqRixZQUFZLEdBQUcsUUFBUSxDQUFDLFdBQVcsQ0FBQztTQUNyQzthQUFNO1lBQ0wsWUFBWSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7WUFDaEMsaUJBQWlCO1NBQ2xCO1FBRUQsSUFBSSxZQUFZLEtBQUssU0FBUyxFQUFFO1lBQzlCLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxFQUFFLENBQUM7U0FDakM7O2NBRUssZ0JBQWdCLEdBQUcsQ0FBQyxRQUFRLElBQUksUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVztRQUNyRyxJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsWUFBWSxJQUFJLFFBQVEsQ0FBQyxZQUFZLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTs7O2tCQUVuRSxtQkFBbUIsR0FBRyxRQUFRLENBQUMsWUFBWSxDQUFDLElBQUk7Ozs7WUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksS0FBSyxZQUFZLEVBQUM7WUFDckYsSUFBSSxtQkFBbUIsRUFBRTtnQkFDdkIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7YUFDdEQ7aUJBQU07Z0JBQ0wsT0FBTyxDQUFDLEtBQUssQ0FBQyxlQUFlLFlBQVkscURBQXFELENBQUMsQ0FBQztnQkFDaEcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQzthQUMxRDtTQUNGO2FBQU07OztrQkFFQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsWUFBWTs7a0JBQ3RDLHNCQUFzQixHQUFHLGtCQUFrQixDQUFDLElBQUk7Ozs7WUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksS0FBSyxZQUFZLEVBQUM7WUFDckYsSUFBSSxzQkFBc0IsRUFBRTtnQkFDMUIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7YUFDdEQ7aUJBQU07Z0JBQ0wsT0FBTyxDQUFDLEtBQUssQ0FBQyxlQUFlLFlBQVkscURBQXFELENBQUMsQ0FBQztnQkFDaEcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQzthQUMxRDtTQUNGO1FBQ0QsSUFBSSxDQUFDLFlBQVksS0FBSyxRQUFRLElBQUksZ0JBQWdCLEtBQUssUUFBUSxDQUFDLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsRUFBRTtZQUNuRyxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7U0FDeEI7SUFDSCxDQUFDOzs7OztJQUVPLE9BQU87O1lBQ1QsS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxRQUFRO1FBQ2pELE9BQU8sS0FBSyxDQUFDLFVBQVUsSUFBSSxJQUFJLEVBQUU7WUFDL0IsS0FBSyxHQUFHLEtBQUssQ0FBQyxVQUFVLENBQUM7WUFDekIsSUFBSSxLQUFLLENBQUMsV0FBVyxLQUFLLElBQUksRUFBRTtnQkFDOUIsU0FBUzthQUNWO1lBQ0QsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFO2dCQUMzQixTQUFTO2FBQ1Y7WUFDRCxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUU7Z0JBQzNCLFNBQVM7YUFDVjtZQUNELE9BQU8sbUJBQUEsS0FBSyxDQUFDLElBQUksRUFBbUQsQ0FBQztTQUN0RTtJQUNILENBQUM7OztZQW5hRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLGNBQWM7Z0JBQ3hCLG1oSUFBc0M7Z0JBRXRDLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNOzthQUNoRDs7OztZQWJPLFFBQVE7WUFIUixZQUFZO1lBSVosVUFBVTtZQWhDSyxnQkFBZ0I7WUEwQi9CLGFBQWE7WUFUbUMsTUFBTTtZQUF0RCxjQUFjO1lBS2QsaUJBQWlCO1lBQ2pCLGFBQWE7WUFNYixjQUFjO1lBVk4sY0FBYztZQVF0QixpQkFBaUI7WUFNakIsWUFBWTtZQUVaLG1CQUFtQjtZQURuQixXQUFXOzRDQXNGZCxNQUFNLFNBQUMsUUFBUTtZQW5GWCxvQkFBb0I7OztnQ0FhMUIsU0FBUyxTQUFDLG1CQUFtQjtnQ0FPN0IsS0FBSzs2QkFDTCxLQUFLOzJCQUNMLEtBQUs7OEJBQ0wsS0FBSztxQkFDTCxLQUFLOzBCQUNMLEtBQUs7MkJBQ0wsS0FBSzswQkFRTCxLQUFLOzRCQUNMLEtBQUs7cUNBa0JMLGVBQWUsU0FBQyxvQkFBb0IsRUFBRSxFQUFDLFdBQVcsRUFBRSxLQUFLLEVBQUM7c0JBRzFELFNBQVMsU0FBQyxVQUFVOzs7Ozs7O0lBOUNyQiw0Q0FBa0U7O0lBQ2xFLHNDQUEyRDs7SUFFM0QsNENBQTJFOztJQUUzRSxvQ0FBaUQ7O0lBQ2pELDJDQUE2RDs7SUFDN0QseUNBQTJEOztJQUMzRCxzQ0FBcUQ7O0lBRXJELDRDQUEwQzs7SUFDMUMseUNBQXVDOztJQUN2Qyx1Q0FBcUM7O0lBQ3JDLDBDQUF3Qzs7SUFDeEMsaUNBQStCOztJQUMvQixzQ0FBMkM7O0lBQzNDLHVDQU9FOztJQUNGLHNDQUF1Qzs7SUFDdkMsd0NBQXFDOzs7OztJQUVyQyx1Q0FBNEM7O0lBRTVDLG1DQUE4Qzs7SUFDOUMsb0NBQWdEOztJQUVoRCxvQ0FBMkM7O0lBQzNDLG1DQUFzRDs7SUFDdEQsbUNBQXlDOztJQUN6QyxrQ0FBcUQ7O0lBRXJELGdDQUE4Qzs7SUFFOUMsbURBQXdFOzs7OztJQUV4RSw2Q0FBeUM7O0lBRXpDLGlEQUMrRDs7SUFFL0Qsa0NBQTJDOztJQUUzQyx3Q0FBd0Q7Ozs7O0lBRXhELDZEQUF5RDs7Ozs7SUFDekQsOERBQTBEOztJQUUxRCw0Q0FBb0U7O0lBQ3BFLHFDQUFzRDs7SUFDdEQscUNBQWlEOzs7OztJQUcvQyxtQ0FBNEI7Ozs7O0lBQzVCLHVDQUFvQzs7Ozs7SUFDcEMscUNBQWdDOztJQUNoQyxpQ0FBK0I7Ozs7O0lBQy9CLHdDQUFzQzs7Ozs7SUFDdEMsaUNBQXdCOzs7OztJQUN4Qix5Q0FBd0M7Ozs7O0lBQ3hDLDRDQUE4Qzs7Ozs7SUFDOUMsd0NBQXNDOzs7OztJQUN0Qyx5Q0FBd0M7Ozs7O0lBQ3hDLHlDQUF3Qzs7Ozs7SUFDeEMsNENBQThDOzs7OztJQUM5Qyx1Q0FBb0M7Ozs7O0lBQ3BDLDhDQUFrRDs7Ozs7SUFDbEQsNkJBQXVCOzs7OztJQUN2QixtQ0FBdUM7Ozs7O0lBQ3ZDLCtDQUFvRCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q2RrU2Nyb2xsYWJsZSwgU2Nyb2xsRGlzcGF0Y2hlcn0gZnJvbSAnQGFuZ3VsYXIvY2RrL292ZXJsYXknO1xuaW1wb3J0IHtET0NVTUVOVH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7XG4gIEFmdGVyQ29udGVudEluaXQsXG4gIEFmdGVyVmlld0luaXQsXG4gIENoYW5nZURldGVjdGlvblN0cmF0ZWd5LFxuICBDb21wb25lbnQsXG4gIENvbnRlbnRDaGlsZHJlbixcbiAgRWxlbWVudFJlZixcbiAgSW5qZWN0LFxuICBJbnB1dCxcbiAgT25EZXN0cm95LFxuICBPbkluaXQsXG4gIFF1ZXJ5TGlzdCxcbiAgVmlld0NoaWxkXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtNYXRTaWRlbmF2fSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XG5pbXBvcnQge0FjdGl2YXRlZFJvdXRlLCBOYXZpZ2F0aW9uRW5kLCBOYXZpZ2F0aW9uU3RhcnQsIFJvdXRlcn0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcblxuaW1wb3J0IHtIb3RrZXksIEhvdGtleXNTZXJ2aWNlfSBmcm9tICdhbmd1bGFyMi1ob3RrZXlzJztcbmltcG9ydCB7QmVoYXZpb3JTdWJqZWN0LCBtZXJnZSwgU3Vic2NyaXB0aW9uLCB0aW1lcn0gZnJvbSAncnhqcyc7XG5pbXBvcnQge2ZpbHRlciwgbWFwfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQge1VwZGF0ZUluZm9TZXJ2aWNlfSBmcm9tICcuLi91cGRhdGUvdXBkYXRlLWluZm8uc2VydmljZSc7XG5pbXBvcnQge1VwZGF0ZVNlcnZpY2V9IGZyb20gJy4uL3VwZGF0ZS91cGRhdGUuc2VydmljZSc7XG5pbXBvcnQge05vdGlmaWNhY2FvQ29tcG9uZW50fSBmcm9tICcuL2hlYWRlci9ub3RpZmljYXRpb24vbm90aWZpY2FjYW8vbm90aWZpY2FjYW8uY29tcG9uZW50JztcbmltcG9ydCB7RnNEb2N1bWVudEVsZW1lbnQsIExheW91dFR5cGV9IGZyb20gJy4vbGF5b3V0LXR5cGUubW9kZWwnO1xuaW1wb3J0IHtMYXlvdXRTZXJ2aWNlfSBmcm9tICcuL2xheW91dC5zZXJ2aWNlJztcbmltcG9ydCB7QnJlYWRjcnVtYlNlcnZpY2V9IGZyb20gJy4vbmF2L2JyZWFkY3J1bWIvYnJlYWRjcnVtYnMvYnJlYWRjcnVtYi5zZXJ2aWNlJztcbmltcG9ydCB7RmF2TmF2c1F1ZXJ5fSBmcm9tICcuL25hdi9mYXZuYXYvc3RhdGUvZmF2bmF2cy5xdWVyeSc7XG5pbXBvcnQge0Zhdk5hdnNTZXJ2aWNlfSBmcm9tICcuL25hdi9mYXZuYXYvc3RhdGUvZmF2bmF2cy5zZXJ2aWNlJztcblxuaW1wb3J0IHtOYXZRdWVyeX0gZnJvbSAnLi9uYXYvc3RhdGUvbmF2LnF1ZXJ5JztcbmltcG9ydCB7TmF2U2VydmljZX0gZnJvbSAnLi9uYXYvc3RhdGUvbmF2LnNlcnZpY2UnO1xuaW1wb3J0IHtUb2FzdFNlcnZpY2V9IGZyb20gJy4vdG9hc3QvdG9hc3Quc2VydmljZSc7XG5pbXBvcnQge0Zvcm1CdWlsZGVyfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQge1Vpa2l0UnJpcHBsZVNlcnZpY2V9IGZyb20gXCIuLi91dGlscy91aWtpdC1yaXBwbGUuc2VydmljZVwiO1xuXG5pbXBvcnQgeyBQcmV2aW91c1JvdXRlU2VydmljZSB9IGZyb20gJy4uL2NvbXBvbmVudHMvcHJldmlvdXMtcm91dGUuc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ3Vpa2l0LWxheW91dCcsXG4gIHRlbXBsYXRlVXJsOiAnLi9sYXlvdXQuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9sYXlvdXQuY29tcG9uZW50LnNjc3MnXSxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2hcbn0pXG5leHBvcnQgY2xhc3MgTGF5b3V0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3ksIEFmdGVyVmlld0luaXQsIEFmdGVyQ29udGVudEluaXQge1xuXG4gIHByb3RlY3RlZCBoYXNBY3Rpb25zU3ViamVjdCA9IG5ldyBCZWhhdmlvclN1YmplY3Q8Ym9vbGVhbj4oZmFsc2UpO1xuICBwdWJsaWMgaGFzQWN0aW9ucyQgPSB0aGlzLmhhc0FjdGlvbnNTdWJqZWN0LmFzT2JzZXJ2YWJsZSgpO1xuXG4gIEBWaWV3Q2hpbGQoJ215Q2xpY2tGdWxsc2NyZWVuJykgbXlDbGlja0Z1bGxzY3JlZW46IEVsZW1lbnRSZWY8SFRNTEVsZW1lbnQ+O1xuXG4gIHB1YmxpYyBzaG93U2hlbGwgPSB0aGlzLmxheW91dFNlcnZpY2Uuc2hvd1NoZWxsJDtcbiAgcHVibGljIGhpZGRlbkJyZWFkY3J1bWIgPSB0aGlzLmxheW91dFNlcnZpY2Uuc2hvd0JyZWFkY3J1bWIkO1xuICBwdWJsaWMgc2hvd0Z1bGxTY3JyZW4gPSB0aGlzLmxheW91dFNlcnZpY2Uuc2hvd0Z1bGxTY3JlZW4kO1xuICBwdWJsaWMgaGlkZGVuU2hlbGwgPSB0aGlzLmxheW91dFNlcnZpY2UuaGlkZGVuU2hlbGwkO1xuXG4gIEBJbnB1dCgpIHB1YmxpYyBzaG93Tm90aWZpY2F0aW9ucyA9IGZhbHNlO1xuICBASW5wdXQoKSBwdWJsaWMgc2hvd1N5c3RlbUluZm8gPSBmYWxzZTtcbiAgQElucHV0KCkgcHVibGljIHNob3dVc2VySW5mbyA9IGZhbHNlO1xuICBASW5wdXQoKSBwdWJsaWMgc2hvd0Vudmlyb25tZW50ID0gZmFsc2U7XG4gIEBJbnB1dCgpIHB1YmxpYyBzaG93SWEgPSBmYWxzZTtcbiAgQElucHV0KCkgcHVibGljIGVudmlyb25tZW50OiBzdHJpbmcgPSBudWxsO1xuICBASW5wdXQoKSBwdWJsaWMgYWxsb3dlZFR5cGVzID0gW1xuICAgICdub3JtYWwnLFxuICAgICdmdWxsc2NyZWVuJyxcbiAgICAnbm9zaGVsbCcsXG4gICAgJ25vc2hlbGxmdWxsc2NyZWVuJyxcbiAgICAnbm9zaGVsbG5vYnJlYWRjcnVtYicsXG4gICAgJ25vc2hlbGxub2JyZWFkY3J1bWJmdWxsc2NyZWVuJ1xuICBdO1xuICBASW5wdXQoKSBwdWJsaWMgZGVmYXVsdFR5cGUgPSAnbm9ybWFsJztcbiAgQElucHV0KCkgcHVibGljIHNob3dTaG9ydGN1dHMgPSB0cnVlO1xuXG4gIHByb3RlY3RlZCBzdWJzY3JpcHRpb24gPSBuZXcgU3Vic2NyaXB0aW9uKCk7XG5cbiAgcHVibGljIGlzRml4ZWQkID0gdGhpcy5sYXlvdXRTZXJ2aWNlLmlzRml4ZWQkO1xuICBwdWJsaWMgaXNNb2JpbGUkID0gdGhpcy5sYXlvdXRTZXJ2aWNlLmlzTW9iaWxlJDtcblxuICBwdWJsaWMgcmlnaHRuYXYkID0gdGhpcy5uYXZRdWVyeS5yaWdodG5hdiQ7XG4gIHB1YmxpYyByaWdodG5hdjogeyBvcGVuZWQ6IGJvb2xlYW47IHBpbm5lZDogYm9vbGVhbiB9O1xuICBwdWJsaWMgbGVmdG5hdiQgPSB0aGlzLm5hdlF1ZXJ5LmxlZnRuYXYkO1xuICBwdWJsaWMgbGVmdG5hdjogeyBvcGVuZWQ6IGJvb2xlYW47IHBpbm5lZDogYm9vbGVhbiB9O1xuXG4gIHB1YmxpYyBkYXRhJCA9IG5ldyBCZWhhdmlvclN1YmplY3Q8YW55PihudWxsKTtcblxuICBwb3NzdWlSZWN1cnNvRGVGYXZvcml0b3MgPSB0aGlzLmZhdk5hdnNTZXJ2aWNlLnBvc3N1aVJlY3Vyc29EZUZhdm9yaXRvcztcblxuICBwcml2YXRlIHNjcm9sbFN1YnNjcmlwdGlvbjogU3Vic2NyaXB0aW9uO1xuXG4gIEBDb250ZW50Q2hpbGRyZW4oTm90aWZpY2FjYW9Db21wb25lbnQsIHtkZXNjZW5kYW50czogZmFsc2V9KVxuICBwdWJsaWMgbm90aWZpY2Fjb2VzUHJvamV0YWRhczogUXVlcnlMaXN0PE5vdGlmaWNhY2FvQ29tcG9uZW50PjtcblxuICBAVmlld0NoaWxkKE1hdFNpZGVuYXYpIHNpZGVuYXY6IE1hdFNpZGVuYXY7XG5cbiAgcHVibGljIG5vdGlmaWNhY29lcyQgPSB0aGlzLmxheW91dFNlcnZpY2Uubm90aWZpY2Fjb2VzJDtcblxuICBwcml2YXRlIGNsb3NlV2hlbk5vdFBpbm5lZExlZnRTdWJzY3JpcHRpb246IFN1YnNjcmlwdGlvbjtcbiAgcHJpdmF0ZSBjbG9zZVdoZW5Ob3RQaW5uZWRSaWdodFN1YnNjcmlwdGlvbjogU3Vic2NyaXB0aW9uO1xuXG4gIHB1YmxpYyBoYXNJbnN0YWxsT3B0aW9uJCA9IHRoaXMudXBkYXRlSW5mb1NlcnZpY2UuaGFzSW5zdGFsbE9wdGlvbiQ7XG4gIHB1YmxpYyBoYXNVcGRhdGUkID0gdGhpcy51cGRhdGVJbmZvU2VydmljZS5oYXNVcGRhdGUkO1xuICBwdWJsaWMgZmF2b3JpdGVzJCA9IHRoaXMuZmF2TmF2c1F1ZXJ5LmZhdm9yaXRlcyQ7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJvdGVjdGVkIG5hdlF1ZXJ5OiBOYXZRdWVyeSxcbiAgICBwcm90ZWN0ZWQgZmF2TmF2c1F1ZXJ5OiBGYXZOYXZzUXVlcnksXG4gICAgcHJvdGVjdGVkIG5hdlNlcnZpY2U6IE5hdlNlcnZpY2UsXG4gICAgcHVibGljIHNjcm9sbDogU2Nyb2xsRGlzcGF0Y2hlcixcbiAgICBwcm90ZWN0ZWQgbGF5b3V0U2VydmljZTogTGF5b3V0U2VydmljZSxcbiAgICBwcm90ZWN0ZWQgcm91dGVyOiBSb3V0ZXIsXG4gICAgcHJvdGVjdGVkIGFjdGl2YXRlZFJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSxcbiAgICBwcm90ZWN0ZWQgdXBkYXRlSW5mb1NlcnZpY2U6IFVwZGF0ZUluZm9TZXJ2aWNlLFxuICAgIHByb3RlY3RlZCB1cGRhdGVTZXJ2aWNlOiBVcGRhdGVTZXJ2aWNlLFxuICAgIHByb3RlY3RlZCBmYXZOYXZzU2VydmljZTogRmF2TmF2c1NlcnZpY2UsXG4gICAgcHJvdGVjdGVkIGhvdGtleXNTZXJ2aWNlOiBIb3RrZXlzU2VydmljZSxcbiAgICBwcm90ZWN0ZWQgYnJlYWRjcnVtYlNlcnZpY2U6IEJyZWFkY3J1bWJTZXJ2aWNlLFxuICAgIHByb3RlY3RlZCB0b2FzdFNlcnZpY2U6IFRvYXN0U2VydmljZSxcbiAgICBwcm90ZWN0ZWQgdWlraXRScmlwcGxlU2VydmljZTogVWlraXRScmlwcGxlU2VydmljZSxcbiAgICBwcml2YXRlIGZiOiBGb3JtQnVpbGRlcixcbiAgICBASW5qZWN0KERPQ1VNRU5UKSBwcml2YXRlIGRvY3VtZW50OiBhbnksXG4gICAgcHJvdGVjdGVkIHByZXZpb3VzUm91dGVTZXJ2aWNlOiBQcmV2aW91c1JvdXRlU2VydmljZSxcbiAgKSB7XG4gIH1cblxuICBuZ0FmdGVyQ29udGVudEluaXQoKTogdm9pZCB7XG5cbiAgICB0aGlzLm5vdGlmaWNhY29lc1Byb2pldGFkYXMuY2hhbmdlcy5zdWJzY3JpYmUoKCkgPT4ge1xuICAgICAgY29uc3Qgbm90aWZpY2Fjb2VzTW9kZWwgPSB0aGlzLm5vdGlmaWNhY29lc1Byb2pldGFkYXMudG9BcnJheSgpLm1hcChyID0+IHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICBub21lOiByLm5vbWUsXG4gICAgICAgICAgaWNvbmU6IHIuaWNvbmUsXG4gICAgICAgICAgZGVzY3JpY2FvOiByLmRlc2NyaWNhbyxcbiAgICAgICAgICBkYXRhOiByLmRhdGEsXG4gICAgICAgICAgcmVhZDogci5yZWFkXG4gICAgICAgIH07XG4gICAgICB9KTtcbiAgICAgIHRoaXMubGF5b3V0U2VydmljZS5zZXROb3RpZmljYWNvZXMobm90aWZpY2Fjb2VzTW9kZWwpO1xuICAgIH0pO1xuICAgIHRoaXMubm90aWZpY2Fjb2VzUHJvamV0YWRhcy5ub3RpZnlPbkNoYW5nZXMoKTtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIHRoaXMudWlraXRScmlwcGxlU2VydmljZS5pbml0KCk7XG5cbiAgICB0aGlzLnN1YnNjcmlwdGlvbi5hZGQoXG4gICAgICB0aGlzLmxlZnRuYXYkLnN1YnNjcmliZShsZWZ0bmF2ID0+IHtcbiAgICAgICAgdGhpcy5sZWZ0bmF2ID0gbGVmdG5hdjtcbiAgICAgIH0pXG4gICAgKTtcblxuICAgIHRoaXMuc3Vic2NyaXB0aW9uLmFkZChcbiAgICAgICh0aGlzLmNsb3NlV2hlbk5vdFBpbm5lZExlZnRTdWJzY3JpcHRpb24gPSB0aGlzLnJvdXRlci5ldmVudHNcbiAgICAgICAgLnBpcGUoZmlsdGVyKGV2ZW50ID0+IGV2ZW50IGluc3RhbmNlb2YgTmF2aWdhdGlvblN0YXJ0KSlcbiAgICAgICAgLnN1YnNjcmliZShldmVudCA9PiB7XG4gICAgICAgICAgaWYgKHRoaXMubGVmdG5hdi5waW5uZWQgPT09IGZhbHNlKSB7XG4gICAgICAgICAgICB0aGlzLm5hdlNlcnZpY2UuY2xvc2VMZWZ0TmF2KCk7XG4gICAgICAgICAgfVxuICAgICAgICB9KSlcbiAgICApO1xuXG4gICAgdGhpcy5zdWJzY3JpcHRpb24uYWRkKFxuICAgICAgdGhpcy5yaWdodG5hdiQuc3Vic2NyaWJlKHJpZ2h0bmF2ID0+IHtcbiAgICAgICAgdGhpcy5yaWdodG5hdiA9IHJpZ2h0bmF2O1xuICAgICAgfSlcbiAgICApO1xuXG4gICAgdGhpcy5zdWJzY3JpcHRpb24uYWRkKFxuICAgICAgKHRoaXMuY2xvc2VXaGVuTm90UGlubmVkUmlnaHRTdWJzY3JpcHRpb24gPSB0aGlzLnJvdXRlci5ldmVudHNcbiAgICAgICAgLnBpcGUoZmlsdGVyKGV2ZW50ID0+IGV2ZW50IGluc3RhbmNlb2YgTmF2aWdhdGlvblN0YXJ0KSlcbiAgICAgICAgLnN1YnNjcmliZShldmVudCA9PiB7XG4gICAgICAgICAgaWYgKHRoaXMucmlnaHRuYXYucGlubmVkID09PSBmYWxzZSkge1xuICAgICAgICAgICAgdGhpcy5uYXZTZXJ2aWNlLmNsb3NlUmlnaHROYXYoKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0pKVxuICAgICk7XG5cbiAgICBjb25zdCBuYWdpdmF0ZSA9IHRoaXMucm91dGVyLmV2ZW50cy5waXBlKGZpbHRlcihldmVudCA9PiBldmVudCBpbnN0YW5jZW9mIE5hdmlnYXRpb25FbmQpKTtcbiAgICBjb25zdCBwYXJhbXMgPSB0aGlzLmFjdGl2YXRlZFJvdXRlLnBhcmFtcztcblxuICAgIG1lcmdlKG5hZ2l2YXRlLCBwYXJhbXMpLnBpcGUoXG4gICAgICBtYXAoKCkgPT4ge1xuICAgICAgICBsZXQgY2hpbGQgPSB0aGlzLmFjdGl2YXRlZFJvdXRlLmZpcnN0Q2hpbGQ7XG4gICAgICAgIHdoaWxlIChjaGlsZCkge1xuICAgICAgICAgIGlmIChjaGlsZC5maXJzdENoaWxkKSB7XG4gICAgICAgICAgICBjaGlsZCA9IGNoaWxkLmZpcnN0Q2hpbGQ7XG4gICAgICAgICAgfSBlbHNlIGlmIChjaGlsZC5zbmFwc2hvdC5kYXRhKSB7XG4gICAgICAgICAgICByZXR1cm4gY2hpbGQuc25hcHNob3QuZGF0YTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgICAgfSkpXG4gICAgICAuc3Vic2NyaWJlKGN1c3RvbURhdGEgPT4ge1xuICAgICAgICBsZXQgcm91dGUgPSB0aGlzLnJvdXRlci5yb3V0ZXJTdGF0ZS5yb290LnNuYXBzaG90O1xuICAgICAgICB3aGlsZSAocm91dGUuZmlyc3RDaGlsZCAhPSBudWxsKSB7XG4gICAgICAgICAgcm91dGUgPSByb3V0ZS5maXJzdENoaWxkO1xuICAgICAgICB9XG4gICAgICAgIGNvbnN0IGRhdGEgPSByb3V0ZS5kYXRhO1xuXG5cbiAgICAgICAgY29uc3QgZmF2SXRlbSA9IHRoaXMuZ2V0RmF2SXRlbShkYXRhKTtcbiAgICAgICAgLy8gY29uc29sZS5sb2coJ2xheW91dCAtIG5hZ2l2YXRlLCBwYXJhbXMnLCBmYXZJdGVtKTtcbiAgICAgICAgLy8gZmF2SXRlbS5pY29uID0gY3VzdG9tRGF0YSA/IGN1c3RvbURhdGEuaWNvbiA6IG51bGw7XG4gICAgICAgIHRoaXMuZGF0YSQubmV4dChmYXZJdGVtKTtcbiAgICAgIH0pO1xuXG4gICAgLy8gV2hlbiBjb25uZWN0aW9uIGJlY29tZSBvZmZsaW5lLCB0aGVuIHB1dCB0aGUgZW50aXJlIGFwcCB3aXRoIGdyYXlzY2FsZSAoMC44KVxuICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdvbmxpbmUnLCAoKSA9PiB7XG4gICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCdib2R5Jykuc3R5bGUuZmlsdGVyID0gJyc7XG4gICAgICBpZiAobG9jYWxTdG9yYWdlLmdldEl0ZW0oJ29mZmxpbmUnKSA9PT0gJ3RydWUnKSB7XG4gICAgICAgIHRpbWVyKDYwMCkuc3Vic2NyaWJlKCgpID0+IHRoaXMudG9hc3RTZXJ2aWNlLnN1Y2Nlc3MoJ0NvbmV4w6NvIGRlIHJlZGUgcmVzdGFiZWxlY2lkYSEnKSk7XG4gICAgICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKCdvZmZsaW5lJyk7XG4gICAgICB9XG4gICAgfSk7XG4gICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ29mZmxpbmUnLCAoKSA9PiB7XG4gICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCdib2R5Jykuc3R5bGUuZmlsdGVyID0gJ2dyYXlzY2FsZSgwLjgpJztcbiAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdvZmZsaW5lJywgJ3RydWUnKTtcbiAgICAgIHRpbWVyKDYwMCkuc3Vic2NyaWJlKCgpID0+IHRoaXMudG9hc3RTZXJ2aWNlLndhcm5pbmcoJ1NlbSBjb25leMOjbyBkZSByZWRlIScpKTtcbiAgICB9KTtcblxuICAgIHRoaXMuc3Vic2NyaXB0aW9uLmFkZChcbiAgICAgICh0aGlzLnNjcm9sbFN1YnNjcmlwdGlvbiA9IHRoaXMuc2Nyb2xsXG4gICAgICAgIC5zY3JvbGxlZCgpXG4gICAgICAgIC5waXBlKFxuICAgICAgICAgIG1hcChcbiAgICAgICAgICAgIChkYXRhOiBDZGtTY3JvbGxhYmxlKSA9PiB7XG4gICAgICAgICAgICAgIGlmIChkYXRhKSB7XG4gICAgICAgICAgICAgICAgY29uc3QgZWxlbWVudCA9IGRhdGEuZ2V0RWxlbWVudFJlZigpO1xuICAgICAgICAgICAgICAgIGlmIChlbGVtZW50KSB7XG4gICAgICAgICAgICAgICAgICByZXR1cm4gZWxlbWVudC5uYXRpdmVFbGVtZW50LnNjcm9sbFRvcDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICApXG4gICAgICAgIClcbiAgICAgICAgLnN1YnNjcmliZSgoc2Nyb2xsWTogbnVtYmVyKSA9PiB7XG4gICAgICAgICAgaWYgKHNjcm9sbFkpIHtcbiAgICAgICAgICAgIHRoaXMubGF5b3V0U2VydmljZS5zZXRGaXhlZChzY3JvbGxZID4gMCk7XG4gICAgICAgICAgfVxuICAgICAgICB9KSlcbiAgICApO1xuXG4gICAgdGhpcy5yb3V0ZXIuZXZlbnRzXG4gICAgICAucGlwZShmaWx0ZXIoZXZlbnQgPT4gZXZlbnQgaW5zdGFuY2VvZiBOYXZpZ2F0aW9uRW5kKSlcbiAgICAgIC5zdWJzY3JpYmUoKCkgPT4ge1xuICAgICAgICB0aGlzLnNldExheW91dFR5cGVCeVJvdXRlRXZlbnQoKTtcbiAgICAgICAgdGhpcy51cGRhdGVJZkhhc0FjdGlvbnMoKTtcbiAgICAgIH0pO1xuICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgdGhpcy51cGRhdGVJZkhhc0FjdGlvbnMoKTtcbiAgICB9LCAwKTtcbiAgfVxuXG4gIHRvZ2dsZU1vYmlsZSgpIHtcbiAgICB0aGlzLmxheW91dFNlcnZpY2UudG9nZ2xlTW9iaWxlKCk7XG4gIH1cblxuICB1cGRhdGVJZkhhc0FjdGlvbnMoKSB7XG4gICAgY29uc3QgZWxlbWVudHMgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCd1aWtpdC1hY3Rpb25zJyk7XG4gICAgY29uc3QgZWxlbWVudCA9IGVsZW1lbnRzLml0ZW0oMCk7XG4gICAgaWYgKGVsZW1lbnQgIT0gbnVsbCkge1xuICAgICAgY29uc3QgaGFzQ2hpbGROb2RlcyA9IGVsZW1lbnQuaGFzQ2hpbGROb2RlcygpO1xuICAgICAgdGhpcy5oYXNBY3Rpb25zU3ViamVjdC5uZXh0KGhhc0NoaWxkTm9kZXMpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmhhc0FjdGlvbnNTdWJqZWN0Lm5leHQoZmFsc2UpO1xuICAgIH1cbiAgfVxuXG4gIG5nQWZ0ZXJWaWV3SW5pdCgpIHtcblxuICAgIHRoaXMuaG90a2V5c1NlcnZpY2UuYWRkKG5ldyBIb3RrZXkoJ2N0cmwrbScsIChldmVudDogS2V5Ym9hcmRFdmVudCk6IGJvb2xlYW4gPT4ge1xuICAgICAgdGhpcy5sZWZ0TmF2VG9nZ2xlKCk7XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfSkpO1xuICAgIHRoaXMuaG90a2V5c1NlcnZpY2UuYWRkKG5ldyBIb3RrZXkoJ2N0cmwrYWx0K2onLCAoZXZlbnQ6IEtleWJvYXJkRXZlbnQpOiBib29sZWFuID0+IHtcbiAgICAgIHRoaXMucmlnaHROYXZUb2dnbGUoKTtcbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9KSk7XG5cbiAgICB0aGlzLmlzVmVyaWZ5VHlwZVNoZWxsQXBwbHkoKTtcbiAgfVxuXG4gIHByaXZhdGUgZ2V0RmF2SXRlbShjdXN0b21EYXRhKSB7XG4gICAgY29uc3QgZGF0YSA9IGN1c3RvbURhdGE7XG4gICAgY29uc3QgbGFzdEJyZWFkQ3J1bWIgPSB0aGlzLmJyZWFkY3J1bWJTZXJ2aWNlLmJyZWFkY3J1bWJzW3RoaXMuYnJlYWRjcnVtYlNlcnZpY2UuYnJlYWRjcnVtYnMubGVuZ3RoIC0gMV07XG4gICAgaWYgKGxhc3RCcmVhZENydW1iICE9IG51bGwpIHtcbiAgICAgIGNvbnN0IHRpdGxlID0gbGFzdEJyZWFkQ3J1bWIudGl0bGU7IC8vIHRoaXMuYnJlYWRjcnVtYlNlcnZpY2UuZ2V0VGl0bGVGb3JtYXR0ZWQoZGF0YS5icmVhZGNydW1iLCB0aGlzLmFjdGl2YXRlZFJvdXRlLnNuYXBzaG90KTtcbiAgICAgIHJldHVybiB7bGluazogdGhpcy5yb3V0ZXIudXJsLCB0aXRsZSwgaWNvbjogZGF0YSA/IGRhdGEuaWNvbiA6IG51bGx9O1xuICAgIH1cbiAgfVxuXG4gIG5nT25EZXN0cm95KCk6IHZvaWQge1xuICAgIHRoaXMuc3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XG4gIH1cblxuICBsZWZ0TmF2VG9nZ2xlKCkge1xuICAgIHRoaXMubmF2U2VydmljZS50b2dnbGVMZWZ0TmF2KCk7XG4gIH1cblxuICBsZWZ0TmF2Q2xvc2UoKSB7XG4gICAgdGhpcy5uYXZTZXJ2aWNlLmNsb3NlTGVmdE5hdigpO1xuICB9XG5cbiAgcmlnaHROYXZUb2dnbGUoKSB7XG4gICAgdGhpcy5uYXZTZXJ2aWNlLnRvZ2dsZVJpZ2h0TmF2KCk7XG4gIH1cblxuICByaWdodE5hdkNsb3NlKCkge1xuICAgIHRoaXMubmF2U2VydmljZS5jbG9zZVJpZ2h0TmF2KCk7XG4gIH1cblxuICBuYXZzQ2xvc2UoKSB7XG4gICAgaWYgKHRoaXMubGVmdG5hdi5vcGVuZWQgJiYgIXRoaXMubGVmdG5hdi5waW5uZWQpIHtcbiAgICAgIHRoaXMubmF2U2VydmljZS50b2dnbGVMZWZ0TmF2KCk7XG4gICAgfVxuXG4gICAgaWYgKHRoaXMucmlnaHRuYXYub3BlbmVkICYmICF0aGlzLnJpZ2h0bmF2LnBpbm5lZCkge1xuICAgICAgdGhpcy5uYXZTZXJ2aWNlLnRvZ2dsZVJpZ2h0TmF2KCk7XG4gICAgfVxuICB9XG5cbiAgaW5zdGFsbCgpIHtcbiAgICB0aGlzLnVwZGF0ZUluZm9TZXJ2aWNlLmluc3RhbGwoKTtcbiAgfVxuXG4gIHVwZGF0ZSgpIHtcbiAgICB0aGlzLnVwZGF0ZVNlcnZpY2UudXBkYXRlKCk7XG4gIH1cblxuICBwdWJsaWMgaXNWZXJpZnlUeXBlU2hlbGxBcHBseSgpIHtcblxuICAgIHRoaXMuc2hvd0Z1bGxTY3JyZW4uc3Vic2NyaWJlKChpc1Nob3dGdWxsU2NyZWVuOiBib29sZWFuKSA9PiB7XG4gICAgICBpZiAoaXNTaG93RnVsbFNjcmVlbikge1xuICAgICAgICBjb25zdCBmc0RvY0VsZW0gPSB0aGlzLmRvY3VtZW50LmRvY3VtZW50RWxlbWVudCBhcyBGc0RvY3VtZW50RWxlbWVudDtcbiAgICAgICAgLy8gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50IGFzIEZzRG9jdW1lbnRFbGVtZW50O1xuICAgICAgICB0aGlzLm9wZW5GdWxsc2NyZWVuKGZzRG9jRWxlbSk7XG4gICAgICB9XG4gICAgfSk7XG5cbiAgfVxuXG4gIHB1YmxpYyBvcGVuRnVsbHNjcmVlbihmc0RvY0VsZW06IGFueSk6IHZvaWQge1xuICAgIC8qZGVidWdnZXI7Ki9cbiAgICBpZiAoZnNEb2NFbGVtLnJlcXVlc3RGdWxsc2NyZWVuKSB7XG4gICAgICB0aW1lcig2MDApLnN1YnNjcmliZSgoKSA9PiB7XG4gICAgICAgIGZzRG9jRWxlbS5yZXF1ZXN0RnVsbHNjcmVlbigpLmNhdGNoKGVycm9yID0+IGNvbnNvbGUuZXJyb3IoZXJyb3IpKTtcbiAgICAgIH0pO1xuICAgIH0gZWxzZSBpZiAoZnNEb2NFbGVtLm1zUmVxdWVzdEZ1bGxzY3JlZW4pIHtcbiAgICAgIHRyeSB7XG4gICAgICAgIHRpbWVyKDYwMCkuc3Vic2NyaWJlKCgpID0+IHtcbiAgICAgICAgICBmc0RvY0VsZW0ubXNSZXF1ZXN0RnVsbHNjcmVlbigpLmNhdGNoKGVycm9yID0+IGNvbnNvbGUuZXJyb3IoZXJyb3IpKTtcbiAgICAgICAgfSk7XG4gICAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcbiAgICAgIH1cbiAgICB9IGVsc2UgaWYgKGZzRG9jRWxlbS5tb3pSZXF1ZXN0RnVsbFNjcmVlbikge1xuICAgICAgdHJ5IHtcbiAgICAgICAgdGltZXIoNjAwKS5zdWJzY3JpYmUoKCkgPT4ge1xuICAgICAgICAgIGZzRG9jRWxlbS5tb3pSZXF1ZXN0RnVsbFNjcmVlbigpLmNhdGNoKGVycm9yID0+IGNvbnNvbGUuZXJyb3IoZXJyb3IpKTtcbiAgICAgICAgfSk7XG4gICAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcbiAgICAgIH1cbiAgICB9IGVsc2UgaWYgKGZzRG9jRWxlbS53ZWJraXRSZXF1ZXN0RnVsbHNjcmVlbikge1xuICAgICAgdHJ5IHtcbiAgICAgICAgdGltZXIoNjAwKS5zdWJzY3JpYmUoKCkgPT4ge1xuICAgICAgICAgIGZzRG9jRWxlbS53ZWJraXRSZXF1ZXN0RnVsbHNjcmVlbigpLmNhdGNoKGVycm9yID0+IGNvbnNvbGUuZXJyb3IoZXJyb3IpKTtcbiAgICAgICAgfSk7XG4gICAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcbiAgICAgIH1cbiAgICB9XG4gICAgdGltZXIoOTAwKS5zdWJzY3JpYmUoKCkgPT4ge1xuICAgICAgKHRoaXMubXlDbGlja0Z1bGxzY3JlZW4ubmF0aXZlRWxlbWVudCBhcyBIVE1MRWxlbWVudCkuY2xpY2soKTtcbiAgICB9KTtcbiAgfVxuXG4gIHByaXZhdGUgY2xvc2VGdWxsc2NyZWVuKCk6IHZvaWQge1xuICAgIGlmICh0aGlzLmRvY3VtZW50LmV4aXRGdWxsc2NyZWVuKSB7XG4gICAgICB0aGlzLmRvY3VtZW50LmV4aXRGdWxsc2NyZWVuKCk7XG4gICAgfSBlbHNlIGlmICh0aGlzLmRvY3VtZW50Lm1vekNhbmNlbEZ1bGxTY3JlZW4pIHtcbiAgICAgIC8qIEZpcmVmb3ggKi9cbiAgICAgIHRoaXMuZG9jdW1lbnQubW96Q2FuY2VsRnVsbFNjcmVlbigpO1xuICAgIH0gZWxzZSBpZiAodGhpcy5kb2N1bWVudC53ZWJraXRFeGl0RnVsbHNjcmVlbikge1xuICAgICAgLyogQ2hyb21lLCBTYWZhcmkgYW5kIE9wZXJhICovXG4gICAgICB0aGlzLmRvY3VtZW50LndlYmtpdEV4aXRGdWxsc2NyZWVuKCk7XG4gICAgfSBlbHNlIGlmICh0aGlzLmRvY3VtZW50Lm1zRXhpdEZ1bGxzY3JlZW4pIHtcbiAgICAgIC8qIElFL0VkZ2UgKi9cbiAgICAgIHRoaXMuZG9jdW1lbnQubXNFeGl0RnVsbHNjcmVlbigpO1xuICAgIH1cbiAgfVxuXG4gIC8qKiogUmVnaW9uIFJvdXRlTGF5b3V0QXBwbGljYXRpb25cbiAgICogTW9kbyBkZSBhcHJlc2VudGHDp8OjbyBkYXMgdGVsYSBhcGxpY2FkbyBkZSBmb3JtYSB0b3RhbG1lbnRlIGNvbmZpZ3Vyw6F2ZWwgYSBmb3JtYSBnZXJhbCBlIHBvciByb3RhLlxuICAgKiAqKiovXG4gIHB1YmxpYyBzZXRMYXlvdXRUeXBlQnlSb3V0ZUV2ZW50KCkge1xuICAgIGNvbnN0IHF1ZXJ5VHlwZSA9IHRoaXMucm91dGVyLnJvdXRlclN0YXRlLnJvb3Quc25hcHNob3QucXVlcnlQYXJhbU1hcC5nZXQoJ3R5cGUnKTtcbiAgICBjb25zdCBkYXRhUm90YSA9IHRoaXMuZ2V0RGF0YSgpO1xuXG4gICAgbGV0IHR5cGVEZXNlamFkbyA9ICdub3JtYWwnO1xuICAgIGlmIChxdWVyeVR5cGUpIHtcbiAgICAgIC8vIHVzdWFyaW8gaW5mb3Jtb3UgdmlhIHVybCAodGVtIHByaW9yaWRhZGUgc29icmUgdG9kb3MpXG4gICAgICB0eXBlRGVzZWphZG8gPSBxdWVyeVR5cGU7XG4gICAgfSBlbHNlIGlmIChkYXRhUm90YSAmJiBkYXRhUm90YS5kZWZhdWx0VHlwZSkge1xuICAgICAgLy8gZXN0YSBjb25maWd1cmFkbyBuYSByb3RhICh0ZW0gcHJpb3JpZGFkZSBzb21lbnRlIHNvYnJlIG8gZ2xvYmFsIC0gc29icmVzY3JpdGEpXG4gICAgICB0eXBlRGVzZWphZG8gPSBkYXRhUm90YS5kZWZhdWx0VHlwZTtcbiAgICB9IGVsc2Uge1xuICAgICAgdHlwZURlc2VqYWRvID0gdGhpcy5kZWZhdWx0VHlwZTtcbiAgICAgIC8vIGJ1c2NhIG8gZ2xvYmFsXG4gICAgfVxuXG4gICAgaWYgKHR5cGVEZXNlamFkbyA9PT0gJ25vc2hlbGwnKSB7XG4gICAgICB0aGlzLm5hdlNlcnZpY2UudG9nZ2xlTGVmdE5hdigpO1xuICAgIH1cblxuICAgIGNvbnN0IGRlZmF1bHRUeXBlUm91dGUgPSAoZGF0YVJvdGEgJiYgZGF0YVJvdGEuZGVmYXVsdFR5cGUpID8gZGF0YVJvdGEuZGVmYXVsdFR5cGUgOiB0aGlzLmRlZmF1bHRUeXBlO1xuICAgIGlmIChkYXRhUm90YSAmJiBkYXRhUm90YS5hbGxvd2VkVHlwZXMgJiYgZGF0YVJvdGEuYWxsb3dlZFR5cGVzLmxlbmd0aCA+IDApIHtcbiAgICAgIC8vIGV4aXN0ZSBjb25maWd1cmFjYW8gZGUgdGlwb3MgcGVybWl0aWRvcyBuYSByb3RhXG4gICAgICBjb25zdCBlaFBlcm1pdGlkb1BlbGFSb3RhID0gZGF0YVJvdGEuYWxsb3dlZFR5cGVzLnNvbWUodHlwZSA9PiB0eXBlID09PSB0eXBlRGVzZWphZG8pO1xuICAgICAgaWYgKGVoUGVybWl0aWRvUGVsYVJvdGEpIHtcbiAgICAgICAgdGhpcy5sYXlvdXRTZXJ2aWNlLnNldFR5cGUoTGF5b3V0VHlwZVt0eXBlRGVzZWphZG9dKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGNvbnNvbGUuZXJyb3IoYE8gcGFyYW1ldHJvICR7dHlwZURlc2VqYWRvfSBuw6NvIGVzdMOhIGRlZmluaWRvIGNvbW8gcGVybWl0aWRvLCBmYXZvciB2ZXJpZmljYXIhYCk7XG4gICAgICAgIHRoaXMubGF5b3V0U2VydmljZS5zZXRUeXBlKExheW91dFR5cGVbZGVmYXVsdFR5cGVSb3V0ZV0pO1xuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICAvLyBuw6NvIGV4aXN0ZSBjb25maWd1cmHDp8OjbyBkZSB0aXBvcyBwZXJtaXRpZG9zIG5hIHJvdGEgZGV2byBlbnTDo28gdmVyIGRvIGNvbXBvbmVudGUgbGF5b3V0IChnbG9iYWwgZGEgYXBsaWNhw6fDo28pXG4gICAgICBjb25zdCBhbGxvd2VkVHlwZXNMYXlvdXQgPSB0aGlzLmFsbG93ZWRUeXBlcztcbiAgICAgIGNvbnN0IGVoUGVybWl0aWRvR2xvYmFsbWVudGUgPSBhbGxvd2VkVHlwZXNMYXlvdXQuc29tZSh0eXBlID0+IHR5cGUgPT09IHR5cGVEZXNlamFkbyk7XG4gICAgICBpZiAoZWhQZXJtaXRpZG9HbG9iYWxtZW50ZSkge1xuICAgICAgICB0aGlzLmxheW91dFNlcnZpY2Uuc2V0VHlwZShMYXlvdXRUeXBlW3R5cGVEZXNlamFkb10pO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgY29uc29sZS5lcnJvcihgTyBwYXJhbWV0cm8gJHt0eXBlRGVzZWphZG99IG7Do28gZXN0w6EgZGVmaW5pZG8gY29tbyBwZXJtaXRpZG8sIGZhdm9yIHZlcmlmaWNhciFgKTtcbiAgICAgICAgdGhpcy5sYXlvdXRTZXJ2aWNlLnNldFR5cGUoTGF5b3V0VHlwZVtkZWZhdWx0VHlwZVJvdXRlXSk7XG4gICAgICB9XG4gICAgfVxuICAgIGlmICgodHlwZURlc2VqYWRvID09PSAnbm9ybWFsJyB8fCBkZWZhdWx0VHlwZVJvdXRlID09PSAnbm9ybWFsJykgJiYgdGhpcy5kb2N1bWVudC5mdWxsc2NyZWVuRWxlbWVudCkge1xuICAgICAgdGhpcy5jbG9zZUZ1bGxzY3JlZW4oKTtcbiAgICB9XG4gIH1cblxuICBwcml2YXRlIGdldERhdGEoKSB7XG4gICAgbGV0IHJvdXRlID0gdGhpcy5yb3V0ZXIucm91dGVyU3RhdGUucm9vdC5zbmFwc2hvdDtcbiAgICB3aGlsZSAocm91dGUuZmlyc3RDaGlsZCAhPSBudWxsKSB7XG4gICAgICByb3V0ZSA9IHJvdXRlLmZpcnN0Q2hpbGQ7XG4gICAgICBpZiAocm91dGUucm91dGVDb25maWcgPT09IG51bGwpIHtcbiAgICAgICAgY29udGludWU7XG4gICAgICB9XG4gICAgICBpZiAoIXJvdXRlLnJvdXRlQ29uZmlnLnBhdGgpIHtcbiAgICAgICAgY29udGludWU7XG4gICAgICB9XG4gICAgICBpZiAoIXJvdXRlLmRhdGEuZGVmYXVsdFR5cGUpIHtcbiAgICAgICAgY29udGludWU7XG4gICAgICB9XG4gICAgICByZXR1cm4gcm91dGUuZGF0YSBhcyB7IGRlZmF1bHRUeXBlOiBzdHJpbmcsIGFsbG93ZWRUeXBlczogc3RyaW5nW10gfTtcbiAgICB9XG4gIH1cblxuICAvKioqIEVuZFJlZ2lvbiBSb3V0ZUxheW91dEFwcGxpY2F0aW9uICoqKi9cbn1cbiJdfQ==