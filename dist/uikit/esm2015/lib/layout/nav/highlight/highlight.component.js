/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, HostBinding, Input } from '@angular/core';
export class HighlightComponent {
    constructor() {
        this.disabled = false;
        this._isActive = false;
    }
    /**
     * @return {?}
     */
    get isActive() {
        return this._isActive;
    }
    /**
     * @return {?}
     */
    setActiveStyles() {
        this._isActive = true;
    }
    /**
     * @return {?}
     */
    setInactiveStyles() {
        this._isActive = false;
    }
    /**
     * @return {?}
     */
    getLabel() {
        return '';
    }
}
HighlightComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-highlight',
                template: `
    <li [class.disabled]='disabled'>
      <ng-content></ng-content>
    </li>
  `
            }] }
];
HighlightComponent.propDecorators = {
    item: [{ type: Input }],
    disabled: [{ type: Input }],
    isActive: [{ type: HostBinding, args: ['class.active',] }]
};
if (false) {
    /** @type {?} */
    HighlightComponent.prototype.item;
    /** @type {?} */
    HighlightComponent.prototype.disabled;
    /**
     * @type {?}
     * @private
     */
    HighlightComponent.prototype._isActive;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGlnaGxpZ2h0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvbGF5b3V0L25hdi9oaWdobGlnaHQvaGlnaGxpZ2h0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBWTlELE1BQU0sT0FBTyxrQkFBa0I7SUFUL0I7UUFXVyxhQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ2xCLGNBQVMsR0FBRyxLQUFLLENBQUM7SUFpQjVCLENBQUM7Ozs7SUFmQyxJQUFpQyxRQUFRO1FBQ3ZDLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztJQUN4QixDQUFDOzs7O0lBRUQsZUFBZTtRQUNiLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO0lBQ3hCLENBQUM7Ozs7SUFFRCxpQkFBaUI7UUFDZixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztJQUN6QixDQUFDOzs7O0lBRUQsUUFBUTtRQUNOLE9BQU8sRUFBRSxDQUFDO0lBQ1osQ0FBQzs7O1lBNUJGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsaUJBQWlCO2dCQUMzQixRQUFRLEVBQUU7Ozs7R0FJVDthQUVGOzs7bUJBRUUsS0FBSzt1QkFDTCxLQUFLO3VCQUdMLFdBQVcsU0FBQyxjQUFjOzs7O0lBSjNCLGtDQUFjOztJQUNkLHNDQUEwQjs7Ozs7SUFDMUIsdUNBQTBCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBIb3N0QmluZGluZywgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEhpZ2hsaWdodGFibGUgfSBmcm9tICdAYW5ndWxhci9jZGsvYTExeSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ3Vpa2l0LWhpZ2hsaWdodCcsXG4gIHRlbXBsYXRlOiBgXG4gICAgPGxpIFtjbGFzcy5kaXNhYmxlZF09J2Rpc2FibGVkJz5cbiAgICAgIDxuZy1jb250ZW50PjwvbmctY29udGVudD5cbiAgICA8L2xpPlxuICBgLFxuICBzdHlsZXM6IFtdLFxufSlcbmV4cG9ydCBjbGFzcyBIaWdobGlnaHRDb21wb25lbnQgaW1wbGVtZW50cyBIaWdobGlnaHRhYmxlIHtcbiAgQElucHV0KCkgaXRlbTtcbiAgQElucHV0KCkgZGlzYWJsZWQgPSBmYWxzZTtcbiAgcHJpdmF0ZSBfaXNBY3RpdmUgPSBmYWxzZTtcblxuICBASG9zdEJpbmRpbmcoJ2NsYXNzLmFjdGl2ZScpIGdldCBpc0FjdGl2ZSgpIHtcbiAgICByZXR1cm4gdGhpcy5faXNBY3RpdmU7XG4gIH1cblxuICBzZXRBY3RpdmVTdHlsZXMoKSB7XG4gICAgdGhpcy5faXNBY3RpdmUgPSB0cnVlO1xuICB9XG5cbiAgc2V0SW5hY3RpdmVTdHlsZXMoKSB7XG4gICAgdGhpcy5faXNBY3RpdmUgPSBmYWxzZTtcbiAgfVxuXG4gIGdldExhYmVsKCk6IHN0cmluZyB7XG4gICAgcmV0dXJuICcnO1xuICB9XG59XG4iXX0=