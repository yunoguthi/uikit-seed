/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ChangeDetectionStrategy } from '@angular/core';
import { NavQuery } from './state/nav.query';
import { NavService } from './state/nav.service';
export class NavComponent {
    /**
     * @param {?} navService
     * @param {?} navQuery
     */
    constructor(navService, navQuery) {
        this.navService = navService;
        this.navQuery = navQuery;
        this.leftNav$ = this.navQuery.leftnav$;
        this.hasFavoritosService = this.navQuery.hasFavoritosService;
    }
    /**
     * @return {?}
     */
    ngOnInit() { }
    /**
     * @return {?}
     */
    togglePinLeftNav() {
        this.navService.togglePinLeftNav();
    }
}
NavComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-nav',
                template: "<button tabindex=\"-1\" mat-icon-button color=\"primary\" class=\"btn-attach\" (click)=\"togglePinLeftNav()\"\n        [class.is-active]=\"(leftNav$ | async).opened\" [class.is-pinned]=\"(leftNav$ | async).pinned\">\n  <mat-icon class=\"fas fa-thumbtack\" aria-label=\"Fixar menu na tela\"></mat-icon>\n</button>\n\n<uikit-menu>\n  <div menu>\n\n      <ng-content select=\"[nav-menu]\"></ng-content>\n\n  </div>\n</uikit-menu>\n<uikit-favnav *ngIf=\"hasFavoritosService\"></uikit-favnav>\n",
                changeDetection: ChangeDetectionStrategy.OnPush,
                styles: [""]
            }] }
];
/** @nocollapse */
NavComponent.ctorParameters = () => [
    { type: NavService },
    { type: NavQuery }
];
if (false) {
    /** @type {?} */
    NavComponent.prototype.leftNav$;
    /** @type {?} */
    NavComponent.prototype.hasFavoritosService;
    /**
     * @type {?}
     * @protected
     */
    NavComponent.prototype.navService;
    /**
     * @type {?}
     * @protected
     */
    NavComponent.prototype.navQuery;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvbGF5b3V0L25hdi9uYXYuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLHVCQUF1QixFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNFLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUM3QyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFRakQsTUFBTSxPQUFPLFlBQVk7Ozs7O0lBS3ZCLFlBQ1ksVUFBc0IsRUFDdEIsUUFBa0I7UUFEbEIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUN0QixhQUFRLEdBQVIsUUFBUSxDQUFVO1FBTnZCLGFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQztRQUVsQyx3QkFBbUIsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDO0lBSzFELENBQUM7Ozs7SUFFTixRQUFRLEtBQUksQ0FBQzs7OztJQUViLGdCQUFnQjtRQUNkLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztJQUNyQyxDQUFDOzs7WUFwQkYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxXQUFXO2dCQUNyQixxZkFBbUM7Z0JBRW5DLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNOzthQUNoRDs7OztZQVBRLFVBQVU7WUFEVixRQUFROzs7O0lBVWYsZ0NBQXlDOztJQUV6QywyQ0FBK0Q7Ozs7O0lBRzdELGtDQUFnQzs7Ozs7SUFDaEMsZ0NBQTRCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIENoYW5nZURldGVjdGlvblN0cmF0ZWd5IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBOYXZRdWVyeSB9IGZyb20gJy4vc3RhdGUvbmF2LnF1ZXJ5JztcbmltcG9ydCB7IE5hdlNlcnZpY2UgfSBmcm9tICcuL3N0YXRlL25hdi5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAndWlraXQtbmF2JyxcbiAgdGVtcGxhdGVVcmw6ICcuL25hdi5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL25hdi5jb21wb25lbnQuc2NzcyddLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaFxufSlcbmV4cG9ydCBjbGFzcyBOYXZDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBwdWJsaWMgbGVmdE5hdiQgPSB0aGlzLm5hdlF1ZXJ5LmxlZnRuYXYkO1xuXG4gIHB1YmxpYyBoYXNGYXZvcml0b3NTZXJ2aWNlID0gdGhpcy5uYXZRdWVyeS5oYXNGYXZvcml0b3NTZXJ2aWNlO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByb3RlY3RlZCBuYXZTZXJ2aWNlOiBOYXZTZXJ2aWNlLFxuICAgIHByb3RlY3RlZCBuYXZRdWVyeTogTmF2UXVlcnksXG4gICAgKSB7fVxuXG4gIG5nT25Jbml0KCkge31cblxuICB0b2dnbGVQaW5MZWZ0TmF2KCkge1xuICAgIHRoaXMubmF2U2VydmljZS50b2dnbGVQaW5MZWZ0TmF2KCk7XG4gIH1cbn1cbiJdfQ==