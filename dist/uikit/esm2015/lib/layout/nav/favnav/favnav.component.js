/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ChangeDetectionStrategy } from '@angular/core';
import { NavQuery } from '../state/nav.query';
import { FavNavsQuery } from './state/favnavs.query';
import { FavNavsService } from './state/favnavs.service';
export class FavnavComponent {
    // favorites = [
    //   'fas fa-info-circle',
    //   'fas fa-file-contract',
    //   'fas fa-hard-hat',
    //   'fas fa-plug',
    //   'fas fa-moon',
    //   'fas fa-sms',
    //   'fas fa-comments',
    //   'fas fa-ethernet'
    // ];
    /**
     * @param {?} navQuery
     * @param {?} favNavsQuery
     * @param {?} favNavsService
     */
    constructor(navQuery, favNavsQuery, favNavsService) {
        this.navQuery = navQuery;
        this.favNavsQuery = favNavsQuery;
        this.favNavsService = favNavsService;
        this.leftNav$ = this.navQuery.leftnav$;
        this.favorites$ = this.favNavsQuery.favorites$;
        this.favorites = [
        // { title: 'aaa', icon: 'fas fa-info-circle' },
        // { title: 'bbb', icon: 'fas fa-info-circle' },
        // { title: 'ccc', icon: 'fas fa-info-circle' },
        // { title: 'ddd', icon: 'fas fa-info-circle' },
        ];
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.favoritesSubscription = this.favorites$.subscribe((/**
         * @param {?} favs
         * @return {?}
         */
        (favs) => {
            /** @type {?} */
            const newArrary = favs.slice();
            this.favorites.length = 0;
            this.favorites.push(...newArrary);
        }));
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.favoritesSubscription && !this.favoritesSubscription.closed) {
            this.favoritesSubscription.unsubscribe();
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    drop(event) {
        this.favNavsService.moverItens(event.previousIndex, event.currentIndex);
    }
}
FavnavComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-favnav',
                template: "<!-- favnav -->\n<div class=\"favnav\" [class.is-closed]=\"(favorites$ | async).length === 0\" [class.is-active]=\"(leftNav$ | async).opened\">\n  <div cdkDropList (cdkDropListDropped)=\"drop($event)\">\n    <a [routerLinkActive]=\"'active'\" [routerLink]=\"favorite.link\" [routerLinkActiveOptions]=\"{exact:true}\" mat-icon-button\n      *ngFor=\"let favorite of (favorites$ | async)\" cdkDrag matTooltip=\"Ir para '{{ favorite.title }}'\"\n      attr.aria-label=\"Ir para '{{ favorite.title }}'\" [matTooltipPosition]=\"'right'\">\n\n      <div class=\"favnav-placeholder\" *cdkDragPlaceholder></div>\n      <mat-icon class=\"fa-lg {{ favorite.icon }}\"></mat-icon>\n    </a>\n  </div>\n</div>\n",
                changeDetection: ChangeDetectionStrategy.OnPush,
                styles: [""]
            }] }
];
/** @nocollapse */
FavnavComponent.ctorParameters = () => [
    { type: NavQuery },
    { type: FavNavsQuery },
    { type: FavNavsService }
];
if (false) {
    /** @type {?} */
    FavnavComponent.prototype.leftNav$;
    /** @type {?} */
    FavnavComponent.prototype.favorites$;
    /** @type {?} */
    FavnavComponent.prototype.favorites;
    /** @type {?} */
    FavnavComponent.prototype.favoritesSubscription;
    /**
     * @type {?}
     * @protected
     */
    FavnavComponent.prototype.navQuery;
    /**
     * @type {?}
     * @protected
     */
    FavnavComponent.prototype.favNavsQuery;
    /**
     * @type {?}
     * @protected
     */
    FavnavComponent.prototype.favNavsService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmF2bmF2LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvbGF5b3V0L25hdi9mYXZuYXYvZmF2bmF2LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSx1QkFBdUIsRUFBYSxNQUFNLGVBQWUsQ0FBQztBQUV0RixPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFDOUMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBRXJELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQVN6RCxNQUFNLE9BQU8sZUFBZTs7Ozs7Ozs7Ozs7Ozs7OztJQXlCMUIsWUFDWSxRQUFrQixFQUNsQixZQUEwQixFQUMxQixjQUE4QjtRQUY5QixhQUFRLEdBQVIsUUFBUSxDQUFVO1FBQ2xCLGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBQzFCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQXpCbkMsYUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDO1FBRXpDLGVBQVUsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQztRQUMxQyxjQUFTLEdBQWU7UUFDdEIsZ0RBQWdEO1FBQ2hELGdEQUFnRDtRQUNoRCxnREFBZ0Q7UUFDaEQsZ0RBQWdEO1NBQ2pELENBQUM7SUFrQkUsQ0FBQzs7OztJQUVMLFFBQVE7UUFDTixJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTOzs7O1FBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRTs7a0JBQ3hELFNBQVMsR0FBRyxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQzlCLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztZQUMxQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxHQUFHLFNBQVMsQ0FBQyxDQUFDO1FBQ3BDLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7OztJQUVELFdBQVc7UUFDVCxJQUFJLElBQUksQ0FBQyxxQkFBcUIsSUFBSSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLEVBQUU7WUFDcEUsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQzFDO0lBQ0gsQ0FBQzs7Ozs7SUFFRCxJQUFJLENBQUMsS0FBOEI7UUFDakMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLGFBQWEsRUFBRSxLQUFLLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDMUUsQ0FBQzs7O1lBckRGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsY0FBYztnQkFDeEIsc3NCQUFzQztnQkFFdEMsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07O2FBQ2hEOzs7O1lBWFEsUUFBUTtZQUNSLFlBQVk7WUFFWixjQUFjOzs7O0lBWXJCLG1DQUF5Qzs7SUFFekMscUNBQTBDOztJQUMxQyxvQ0FLRTs7SUFFRixnREFBb0M7Ozs7O0lBYWxDLG1DQUE0Qjs7Ozs7SUFDNUIsdUNBQW9DOzs7OztJQUNwQyx5Q0FBd0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3ksIE9uRGVzdHJveSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ2RrRHJhZ0Ryb3AgfSBmcm9tICdAYW5ndWxhci9jZGsvZHJhZy1kcm9wJztcbmltcG9ydCB7IE5hdlF1ZXJ5IH0gZnJvbSAnLi4vc3RhdGUvbmF2LnF1ZXJ5JztcbmltcG9ydCB7IEZhdk5hdnNRdWVyeSB9IGZyb20gJy4vc3RhdGUvZmF2bmF2cy5xdWVyeSc7XG5pbXBvcnQgeyBTdWJzY3JpcHRpb24gfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IEZhdk5hdnNTZXJ2aWNlIH0gZnJvbSAnLi9zdGF0ZS9mYXZuYXZzLnNlcnZpY2UnO1xuaW1wb3J0IHsgTWVudUl0ZW0gfSBmcm9tICcuLi9tZW51L21lbnUtaXRlbS9tZW51LWl0ZW0ubW9kZWwnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICd1aWtpdC1mYXZuYXYnLFxuICB0ZW1wbGF0ZVVybDogJy4vZmF2bmF2LmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vZmF2bmF2LmNvbXBvbmVudC5zY3NzJ10sXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoXG59KVxuZXhwb3J0IGNsYXNzIEZhdm5hdkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcblxuXG4gIHB1YmxpYyBsZWZ0TmF2JCA9IHRoaXMubmF2UXVlcnkubGVmdG5hdiQ7XG5cbiAgZmF2b3JpdGVzJCA9IHRoaXMuZmF2TmF2c1F1ZXJ5LmZhdm9yaXRlcyQ7XG4gIGZhdm9yaXRlczogTWVudUl0ZW1bXSA9IFtcbiAgICAvLyB7IHRpdGxlOiAnYWFhJywgaWNvbjogJ2ZhcyBmYS1pbmZvLWNpcmNsZScgfSxcbiAgICAvLyB7IHRpdGxlOiAnYmJiJywgaWNvbjogJ2ZhcyBmYS1pbmZvLWNpcmNsZScgfSxcbiAgICAvLyB7IHRpdGxlOiAnY2NjJywgaWNvbjogJ2ZhcyBmYS1pbmZvLWNpcmNsZScgfSxcbiAgICAvLyB7IHRpdGxlOiAnZGRkJywgaWNvbjogJ2ZhcyBmYS1pbmZvLWNpcmNsZScgfSxcbiAgXTtcblxuICBmYXZvcml0ZXNTdWJzY3JpcHRpb246IFN1YnNjcmlwdGlvbjtcbiAgLy8gZmF2b3JpdGVzID0gW1xuICAvLyAgICdmYXMgZmEtaW5mby1jaXJjbGUnLFxuICAvLyAgICdmYXMgZmEtZmlsZS1jb250cmFjdCcsXG4gIC8vICAgJ2ZhcyBmYS1oYXJkLWhhdCcsXG4gIC8vICAgJ2ZhcyBmYS1wbHVnJyxcbiAgLy8gICAnZmFzIGZhLW1vb24nLFxuICAvLyAgICdmYXMgZmEtc21zJyxcbiAgLy8gICAnZmFzIGZhLWNvbW1lbnRzJyxcbiAgLy8gICAnZmFzIGZhLWV0aGVybmV0J1xuICAvLyBdO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByb3RlY3RlZCBuYXZRdWVyeTogTmF2UXVlcnksXG4gICAgcHJvdGVjdGVkIGZhdk5hdnNRdWVyeTogRmF2TmF2c1F1ZXJ5LFxuICAgIHByb3RlY3RlZCBmYXZOYXZzU2VydmljZTogRmF2TmF2c1NlcnZpY2VcbiAgKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICB0aGlzLmZhdm9yaXRlc1N1YnNjcmlwdGlvbiA9IHRoaXMuZmF2b3JpdGVzJC5zdWJzY3JpYmUoKGZhdnMpID0+IHtcbiAgICAgIGNvbnN0IG5ld0FycmFyeSA9IGZhdnMuc2xpY2UoKTtcbiAgICAgIHRoaXMuZmF2b3JpdGVzLmxlbmd0aCA9IDA7XG4gICAgICB0aGlzLmZhdm9yaXRlcy5wdXNoKC4uLm5ld0FycmFyeSk7XG4gICAgfSk7XG4gIH1cblxuICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcbiAgICBpZiAodGhpcy5mYXZvcml0ZXNTdWJzY3JpcHRpb24gJiYgIXRoaXMuZmF2b3JpdGVzU3Vic2NyaXB0aW9uLmNsb3NlZCkge1xuICAgICAgdGhpcy5mYXZvcml0ZXNTdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcbiAgICB9XG4gIH1cblxuICBkcm9wKGV2ZW50OiBDZGtEcmFnRHJvcDxNZW51SXRlbVtdPikge1xuICAgIHRoaXMuZmF2TmF2c1NlcnZpY2UubW92ZXJJdGVucyhldmVudC5wcmV2aW91c0luZGV4LCBldmVudC5jdXJyZW50SW5kZXgpO1xuICB9XG59XG4iXX0=