/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { EntityStore, StoreConfig } from '@datorama/akita';
import * as i0 from "@angular/core";
/**
 * @record
 */
export function FavNavsState() { }
let FavNavsStore = class FavNavsStore extends EntityStore {
    constructor() {
        super();
    }
};
FavNavsStore.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */
FavNavsStore.ctorParameters = () => [];
/** @nocollapse */ FavNavsStore.ngInjectableDef = i0.defineInjectable({ factory: function FavNavsStore_Factory() { return new FavNavsStore(); }, token: FavNavsStore, providedIn: "root" });
FavNavsStore = tslib_1.__decorate([
    StoreConfig({ name: 'favnavs' }),
    tslib_1.__metadata("design:paramtypes", [])
], FavNavsStore);
export { FavNavsStore };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmF2bmF2cy5zdG9yZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvbGF5b3V0L25hdi9mYXZuYXYvc3RhdGUvZmF2bmF2cy5zdG9yZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFlLFdBQVcsRUFBRSxXQUFXLEVBQWUsTUFBTSxpQkFBaUIsQ0FBQzs7Ozs7QUFHckYsa0NBQStEO0lBSWxELFlBQVksU0FBWixZQUFhLFNBQVEsV0FBbUM7SUFDbkU7UUFDRSxLQUFLLEVBQUUsQ0FBQztJQUNWLENBQUM7Q0FDRixDQUFBOztZQU5BLFVBQVUsU0FBQyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUU7Ozs7O0FBRXJCLFlBQVk7SUFEeEIsV0FBVyxDQUFDLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxDQUFDOztHQUNwQixZQUFZLENBSXhCO1NBSlksWUFBWSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEVudGl0eVN0YXRlLCBFbnRpdHlTdG9yZSwgU3RvcmVDb25maWcsIHRyYW5zYWN0aW9uIH0gZnJvbSAnQGRhdG9yYW1hL2FraXRhJztcbmltcG9ydCB7IE1lbnVJdGVtIH0gZnJvbSAnLi4vLi4vbWVudS9tZW51LWl0ZW0vbWVudS1pdGVtLm1vZGVsJztcblxuZXhwb3J0IGludGVyZmFjZSBGYXZOYXZzU3RhdGUgZXh0ZW5kcyBFbnRpdHlTdGF0ZTxNZW51SXRlbT4geyB9XG5cbkBJbmplY3RhYmxlKHsgcHJvdmlkZWRJbjogJ3Jvb3QnIH0pXG5AU3RvcmVDb25maWcoeyBuYW1lOiAnZmF2bmF2cycgfSlcbmV4cG9ydCBjbGFzcyBGYXZOYXZzU3RvcmUgZXh0ZW5kcyBFbnRpdHlTdG9yZTxGYXZOYXZzU3RhdGUsIE1lbnVJdGVtPiB7XG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHN1cGVyKCk7XG4gIH1cbn1cbiJdfQ==