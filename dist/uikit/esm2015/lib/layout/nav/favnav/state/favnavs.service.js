/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Inject, Injectable, Optional } from '@angular/core';
import { FavNavsStore } from './favnavs.store';
import { createItem } from '../../menu/menu-item/menu-item.model';
import { FavNavsQuery } from './favnavs.query';
import { moveItemInArray } from '@angular/cdk/drag-drop';
import { FAVORITOS_SERVICE_TOKEN, FavoritosService } from '../../../../utils/favoritos.service';
import { Router } from '@angular/router';
import { isDefined } from '@datorama/akita';
import { UserService } from '../../../../shared/auth/authentication/user.service';
import * as i0 from "@angular/core";
import * as i1 from "./favnavs.store";
import * as i2 from "./favnavs.query";
import * as i3 from "@angular/router";
import * as i4 from "../../../../shared/auth/authentication/user.service";
import * as i5 from "../../../../utils/favoritos.service";
export class FavNavsService {
    /**
     * @param {?} favNavsStore
     * @param {?} favNavsQuery
     * @param {?} router
     * @param {?} userService
     * @param {?} favoritosService
     */
    constructor(favNavsStore, favNavsQuery, router, userService, favoritosService) {
        this.favNavsStore = favNavsStore;
        this.favNavsQuery = favNavsQuery;
        this.router = router;
        this.userService = userService;
        this.favoritosService = favoritosService;
        this.possuiRecursoDeFavoritos = this.favoritosService != null;
        this.possuiRecursoDeFavoritos = this.favoritosService != null;
        if (this.possuiRecursoDeFavoritos) {
            // Para cada alteração do usuário, devo então buscar os favoritos deste
            this.userService.user$.subscribe((/**
             * @param {?} user
             * @return {?}
             */
            user => {
                this.favoritosService.buscar()
                    .subscribe((/**
                 * @param {?} menuItens
                 * @return {?}
                 */
                (menuItens) => {
                    this.favNavsStore.set(menuItens);
                }));
            }));
            this.favoritosService.buscar()
                .subscribe((/**
             * @param {?} menuItens
             * @return {?}
             */
            (menuItens) => {
                this.favNavsStore.set(menuItens);
            }));
        }
    }
    /**
     * @param {?} item
     * @return {?}
     */
    toggleItem(item) {
        /** @type {?} */
        const itemFavorited = this.favNavsQuery.getFavorited(item);
        if (isDefined(itemFavorited)) {
            this.remover(itemFavorited);
        }
        else {
            if (isDefined(item.link)) {
                /** @type {?} */
                const parsedUrl = this.router.createUrlTree(Array.isArray(item.link) ? item.link : [item.link]);
                item.link = parsedUrl.toString();
            }
            /** @type {?} */
            const newItem = createItem(item);
            this.adicionar(newItem);
        }
    }
    /**
     * @param {?} indiceOrigem
     * @param {?} indiceDestino
     * @return {?}
     */
    moverItens(indiceOrigem, indiceDestino) {
        /** @type {?} */
        const itens = this.favNavsQuery.getAll();
        moveItemInArray(itens, indiceOrigem, indiceDestino);
        this.substituirColecao(itens);
    }
    /**
     * @private
     * @param {?} item
     * @return {?}
     */
    adicionar(item) {
        this.favNavsStore.add(item);
        this.atualizar();
    }
    /**
     * @private
     * @param {?} item
     * @return {?}
     */
    remover(item) {
        this.favNavsStore.remove(item.id);
        this.atualizar();
    }
    /**
     * @private
     * @param {?} itens
     * @return {?}
     */
    substituirColecao(itens) {
        this.favNavsStore.set(itens);
        this.atualizar();
    }
    /**
     * @private
     * @return {?}
     */
    atualizar() {
        if (this.possuiRecursoDeFavoritos) {
            /** @type {?} */
            const itens = this.favNavsQuery.getAll();
            this.favoritosService.salvar(itens)
                // .pipe(
                //   retryWhen(errors => errors.pipe(delay(1000), take(5)))
                // )
                .subscribe();
        }
    }
}
FavNavsService.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */
FavNavsService.ctorParameters = () => [
    { type: FavNavsStore },
    { type: FavNavsQuery },
    { type: Router },
    { type: UserService },
    { type: FavoritosService, decorators: [{ type: Optional }, { type: Inject, args: [FAVORITOS_SERVICE_TOKEN,] }] }
];
/** @nocollapse */ FavNavsService.ngInjectableDef = i0.defineInjectable({ factory: function FavNavsService_Factory() { return new FavNavsService(i0.inject(i1.FavNavsStore), i0.inject(i2.FavNavsQuery), i0.inject(i3.Router), i0.inject(i4.UserService), i0.inject(i5.FAVORITOS_SERVICE_TOKEN, 8)); }, token: FavNavsService, providedIn: "root" });
if (false) {
    /** @type {?} */
    FavNavsService.prototype.possuiRecursoDeFavoritos;
    /**
     * @type {?}
     * @private
     */
    FavNavsService.prototype.favNavsStore;
    /**
     * @type {?}
     * @private
     */
    FavNavsService.prototype.favNavsQuery;
    /**
     * @type {?}
     * @private
     */
    FavNavsService.prototype.router;
    /**
     * @type {?}
     * @private
     */
    FavNavsService.prototype.userService;
    /**
     * @type {?}
     * @private
     */
    FavNavsService.prototype.favoritosService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmF2bmF2cy5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9sYXlvdXQvbmF2L2Zhdm5hdi9zdGF0ZS9mYXZuYXZzLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxNQUFNLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUMzRCxPQUFPLEVBQUMsWUFBWSxFQUFDLE1BQU0saUJBQWlCLENBQUM7QUFDN0MsT0FBTyxFQUFDLFVBQVUsRUFBVyxNQUFNLHNDQUFzQyxDQUFDO0FBQzFFLE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUM3QyxPQUFPLEVBQUMsZUFBZSxFQUFDLE1BQU0sd0JBQXdCLENBQUM7QUFDdkQsT0FBTyxFQUFDLHVCQUF1QixFQUFFLGdCQUFnQixFQUFDLE1BQU0scUNBQXFDLENBQUM7QUFDOUYsT0FBTyxFQUFDLE1BQU0sRUFBQyxNQUFNLGlCQUFpQixDQUFDO0FBQ3ZDLE9BQU8sRUFBQyxTQUFTLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUMxQyxPQUFPLEVBQUMsV0FBVyxFQUFDLE1BQU0scURBQXFELENBQUM7Ozs7Ozs7QUFJaEYsTUFBTSxPQUFPLGNBQWM7Ozs7Ozs7O0lBR3pCLFlBQ1UsWUFBMEIsRUFDMUIsWUFBMEIsRUFDMUIsTUFBYyxFQUNkLFdBQXdCLEVBQ3FCLGdCQUFrQztRQUovRSxpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUMxQixpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUMxQixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDcUIscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQVB6Riw2QkFBd0IsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLElBQUksSUFBSSxDQUFDO1FBUXJELElBQUksQ0FBQyx3QkFBd0IsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLElBQUksSUFBSSxDQUFDO1FBRTlELElBQUksSUFBSSxDQUFDLHdCQUF3QixFQUFFO1lBQ2pDLHVFQUF1RTtZQUN2RSxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxTQUFTOzs7O1lBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBRXRDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUU7cUJBQzdCLFNBQVM7Ozs7Z0JBQUMsQ0FBQyxTQUFTLEVBQUUsRUFBRTtvQkFDdkIsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQ25DLENBQUMsRUFBQyxDQUFDO1lBRUwsQ0FBQyxFQUFDLENBQUM7WUFFSCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxFQUFFO2lCQUMzQixTQUFTOzs7O1lBQUMsQ0FBQyxTQUFTLEVBQUUsRUFBRTtnQkFDdkIsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDbkMsQ0FBQyxFQUFDLENBQUM7U0FDTjtJQUVILENBQUM7Ozs7O0lBRUgsVUFBVSxDQUFDLElBQWM7O2NBQ2pCLGFBQWEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUM7UUFFMUQsSUFBSSxTQUFTLENBQUMsYUFBYSxDQUFDLEVBQUU7WUFDNUIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsQ0FBQztTQUM3QjthQUFNO1lBQ0wsSUFBSSxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFOztzQkFDbEIsU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDL0YsSUFBSSxDQUFDLElBQUksR0FBRyxTQUFTLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDbEM7O2tCQUVLLE9BQU8sR0FBRyxVQUFVLENBQUMsSUFBSSxDQUFDO1lBQ2hDLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUM7U0FDekI7SUFDSCxDQUFDOzs7Ozs7SUFFRCxVQUFVLENBQUMsWUFBb0IsRUFBRSxhQUFxQjs7Y0FDOUMsS0FBSyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFO1FBQ3hDLGVBQWUsQ0FBQyxLQUFLLEVBQUUsWUFBWSxFQUFFLGFBQWEsQ0FBQyxDQUFDO1FBQ3BELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNoQyxDQUFDOzs7Ozs7SUFFTyxTQUFTLENBQUMsSUFBYztRQUM5QixJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM1QixJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7SUFDbkIsQ0FBQzs7Ozs7O0lBRU8sT0FBTyxDQUFDLElBQWM7UUFDNUIsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztJQUNuQixDQUFDOzs7Ozs7SUFFTyxpQkFBaUIsQ0FBQyxLQUFpQjtRQUN6QyxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM3QixJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7SUFDbkIsQ0FBQzs7Ozs7SUFFTyxTQUFTO1FBQ2YsSUFBSSxJQUFJLENBQUMsd0JBQXdCLEVBQUU7O2tCQUMzQixLQUFLLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUU7WUFDeEMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUM7Z0JBQ2pDLFNBQVM7Z0JBQ1QsMkRBQTJEO2dCQUMzRCxJQUFJO2lCQUNMLFNBQVMsRUFBRSxDQUFDO1NBQ2Q7SUFDSCxDQUFDOzs7WUE3RUYsVUFBVSxTQUFDLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRTs7OztZQVYxQixZQUFZO1lBRVosWUFBWTtZQUdaLE1BQU07WUFFTixXQUFXO1lBSGMsZ0JBQWdCLHVCQWU1QyxRQUFRLFlBQUksTUFBTSxTQUFDLHVCQUF1Qjs7Ozs7SUFQN0Msa0RBQXlEOzs7OztJQUd2RCxzQ0FBa0M7Ozs7O0lBQ2xDLHNDQUFrQzs7Ozs7SUFDbEMsZ0NBQXNCOzs7OztJQUN0QixxQ0FBZ0M7Ozs7O0lBQ2hDLDBDQUF1RiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SW5qZWN0LCBJbmplY3RhYmxlLCBPcHRpb25hbH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0Zhdk5hdnNTdG9yZX0gZnJvbSAnLi9mYXZuYXZzLnN0b3JlJztcbmltcG9ydCB7Y3JlYXRlSXRlbSwgTWVudUl0ZW19IGZyb20gJy4uLy4uL21lbnUvbWVudS1pdGVtL21lbnUtaXRlbS5tb2RlbCc7XG5pbXBvcnQge0Zhdk5hdnNRdWVyeX0gZnJvbSAnLi9mYXZuYXZzLnF1ZXJ5JztcbmltcG9ydCB7bW92ZUl0ZW1JbkFycmF5fSBmcm9tICdAYW5ndWxhci9jZGsvZHJhZy1kcm9wJztcbmltcG9ydCB7RkFWT1JJVE9TX1NFUlZJQ0VfVE9LRU4sIEZhdm9yaXRvc1NlcnZpY2V9IGZyb20gJy4uLy4uLy4uLy4uL3V0aWxzL2Zhdm9yaXRvcy5zZXJ2aWNlJztcbmltcG9ydCB7Um91dGVyfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHtpc0RlZmluZWR9IGZyb20gJ0BkYXRvcmFtYS9ha2l0YSc7XG5pbXBvcnQge1VzZXJTZXJ2aWNlfSBmcm9tICcuLi8uLi8uLi8uLi9zaGFyZWQvYXV0aC9hdXRoZW50aWNhdGlvbi91c2VyLnNlcnZpY2UnO1xuXG5cbkBJbmplY3RhYmxlKHsgcHJvdmlkZWRJbjogJ3Jvb3QnIH0pXG5leHBvcnQgY2xhc3MgRmF2TmF2c1NlcnZpY2Uge1xuICBwb3NzdWlSZWN1cnNvRGVGYXZvcml0b3MgPSB0aGlzLmZhdm9yaXRvc1NlcnZpY2UgIT0gbnVsbDtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIGZhdk5hdnNTdG9yZTogRmF2TmF2c1N0b3JlLFxuICAgIHByaXZhdGUgZmF2TmF2c1F1ZXJ5OiBGYXZOYXZzUXVlcnksXG4gICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcbiAgICBwcml2YXRlIHVzZXJTZXJ2aWNlOiBVc2VyU2VydmljZSxcbiAgICBAT3B0aW9uYWwoKSBASW5qZWN0KEZBVk9SSVRPU19TRVJWSUNFX1RPS0VOKSBwcml2YXRlIGZhdm9yaXRvc1NlcnZpY2U6IEZhdm9yaXRvc1NlcnZpY2UpIHtcbiAgICAgIHRoaXMucG9zc3VpUmVjdXJzb0RlRmF2b3JpdG9zID0gdGhpcy5mYXZvcml0b3NTZXJ2aWNlICE9IG51bGw7XG5cbiAgICAgIGlmICh0aGlzLnBvc3N1aVJlY3Vyc29EZUZhdm9yaXRvcykge1xuICAgICAgICAvLyBQYXJhIGNhZGEgYWx0ZXJhw6fDo28gZG8gdXN1w6FyaW8sIGRldm8gZW50w6NvIGJ1c2NhciBvcyBmYXZvcml0b3MgZGVzdGVcbiAgICAgICAgdGhpcy51c2VyU2VydmljZS51c2VyJC5zdWJzY3JpYmUodXNlciA9PiB7XG5cbiAgICAgICAgICB0aGlzLmZhdm9yaXRvc1NlcnZpY2UuYnVzY2FyKClcbiAgICAgICAgICAuc3Vic2NyaWJlKChtZW51SXRlbnMpID0+IHtcbiAgICAgICAgICAgIHRoaXMuZmF2TmF2c1N0b3JlLnNldChtZW51SXRlbnMpO1xuICAgICAgICAgIH0pO1xuXG4gICAgICAgIH0pO1xuXG4gICAgICAgIHRoaXMuZmF2b3JpdG9zU2VydmljZS5idXNjYXIoKVxuICAgICAgICAgIC5zdWJzY3JpYmUoKG1lbnVJdGVucykgPT4ge1xuICAgICAgICAgICAgdGhpcy5mYXZOYXZzU3RvcmUuc2V0KG1lbnVJdGVucyk7XG4gICAgICAgICAgfSk7XG4gICAgICB9XG5cbiAgICB9XG5cbiAgdG9nZ2xlSXRlbShpdGVtOiBNZW51SXRlbSkge1xuICAgIGNvbnN0IGl0ZW1GYXZvcml0ZWQgPSB0aGlzLmZhdk5hdnNRdWVyeS5nZXRGYXZvcml0ZWQoaXRlbSk7XG5cbiAgICBpZiAoaXNEZWZpbmVkKGl0ZW1GYXZvcml0ZWQpKSB7XG4gICAgICB0aGlzLnJlbW92ZXIoaXRlbUZhdm9yaXRlZCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGlmIChpc0RlZmluZWQoaXRlbS5saW5rKSkge1xuICAgICAgICBjb25zdCBwYXJzZWRVcmwgPSB0aGlzLnJvdXRlci5jcmVhdGVVcmxUcmVlKEFycmF5LmlzQXJyYXkoaXRlbS5saW5rKSA/IGl0ZW0ubGluayA6IFtpdGVtLmxpbmtdKTtcbiAgICAgICAgaXRlbS5saW5rID0gcGFyc2VkVXJsLnRvU3RyaW5nKCk7XG4gICAgICB9XG5cbiAgICAgIGNvbnN0IG5ld0l0ZW0gPSBjcmVhdGVJdGVtKGl0ZW0pO1xuICAgICAgdGhpcy5hZGljaW9uYXIobmV3SXRlbSk7XG4gICAgfVxuICB9XG5cbiAgbW92ZXJJdGVucyhpbmRpY2VPcmlnZW06IG51bWJlciwgaW5kaWNlRGVzdGlubzogbnVtYmVyKSB7XG4gICAgY29uc3QgaXRlbnMgPSB0aGlzLmZhdk5hdnNRdWVyeS5nZXRBbGwoKTtcbiAgICBtb3ZlSXRlbUluQXJyYXkoaXRlbnMsIGluZGljZU9yaWdlbSwgaW5kaWNlRGVzdGlubyk7XG4gICAgdGhpcy5zdWJzdGl0dWlyQ29sZWNhbyhpdGVucyk7XG4gIH1cblxuICBwcml2YXRlIGFkaWNpb25hcihpdGVtOiBNZW51SXRlbSkge1xuICAgIHRoaXMuZmF2TmF2c1N0b3JlLmFkZChpdGVtKTtcbiAgICB0aGlzLmF0dWFsaXphcigpO1xuICB9XG5cbiAgcHJpdmF0ZSByZW1vdmVyKGl0ZW06IE1lbnVJdGVtKSB7XG4gICAgdGhpcy5mYXZOYXZzU3RvcmUucmVtb3ZlKGl0ZW0uaWQpO1xuICAgIHRoaXMuYXR1YWxpemFyKCk7XG4gIH1cblxuICBwcml2YXRlIHN1YnN0aXR1aXJDb2xlY2FvKGl0ZW5zOiBNZW51SXRlbVtdKSB7XG4gICAgdGhpcy5mYXZOYXZzU3RvcmUuc2V0KGl0ZW5zKTtcbiAgICB0aGlzLmF0dWFsaXphcigpO1xuICB9XG5cbiAgcHJpdmF0ZSBhdHVhbGl6YXIoKSB7XG4gICAgaWYgKHRoaXMucG9zc3VpUmVjdXJzb0RlRmF2b3JpdG9zKSB7XG4gICAgICBjb25zdCBpdGVucyA9IHRoaXMuZmF2TmF2c1F1ZXJ5LmdldEFsbCgpO1xuICAgICAgdGhpcy5mYXZvcml0b3NTZXJ2aWNlLnNhbHZhcihpdGVucylcbiAgICAgICAgLy8gLnBpcGUoXG4gICAgICAgIC8vICAgcmV0cnlXaGVuKGVycm9ycyA9PiBlcnJvcnMucGlwZShkZWxheSgxMDAwKSwgdGFrZSg1KSkpXG4gICAgICAgIC8vIClcbiAgICAgIC5zdWJzY3JpYmUoKTtcbiAgICB9XG4gIH1cbn1cbiJdfQ==