/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { QueryEntity, isDefined, toBoolean } from '@datorama/akita';
import { FavNavsStore } from './favnavs.store';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { map } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "./favnavs.store";
import * as i2 from "@angular/router";
import * as i3 from "@angular/common";
export class FavNavsQuery extends QueryEntity {
    /**
     * @param {?} store
     * @param {?} router
     * @param {?} ngLocation
     */
    constructor(store, router, ngLocation) {
        super(store);
        this.store = store;
        this.router = router;
        this.ngLocation = ngLocation;
        this.favorites$ = this.selectAll();
    }
    /**
     * @param {?} menuItem
     * @return {?}
     */
    isFavorited(menuItem) {
        return this.selectAll({
            filterBy: (/**
             * @param {?} item
             * @return {?}
             */
            item => {
                /** @type {?} */
                const menuItemLink = this.router.createUrlTree(Array.isArray(menuItem.link) ? menuItem.link : [menuItem.link]).toString();
                return item.link === menuItemLink;
            })
        }).pipe(map((/**
         * @param {?} value
         * @return {?}
         */
        value => toBoolean(value))));
    }
    /**
     * @param {?} menuItem
     * @return {?}
     */
    getIsFavorited(menuItem) {
        return this.getAll({
            filterBy: (/**
             * @param {?} item
             * @return {?}
             */
            item => {
                /** @type {?} */
                const menuItemLink = this.router.createUrlTree(Array.isArray(menuItem.link) ? menuItem.link : [menuItem.link]).toString();
                return item.link === menuItemLink;
            })
        }).length > 0;
    }
    /**
     * @param {?} menuItem
     * @return {?}
     */
    getFavorited(menuItem) {
        return this.getAll({
            filterBy: (/**
             * @param {?} item
             * @return {?}
             */
            item => {
                /** @type {?} */
                const menuItemLink = this.router.createUrlTree(Array.isArray(menuItem.link) ? menuItem.link : [menuItem.link]).toString();
                return item.link === menuItemLink;
            })
        }).filter((/**
         * @param {?} item
         * @return {?}
         */
        item => isDefined(item)))[0];
    }
}
FavNavsQuery.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */
FavNavsQuery.ctorParameters = () => [
    { type: FavNavsStore },
    { type: Router },
    { type: Location }
];
/** @nocollapse */ FavNavsQuery.ngInjectableDef = i0.defineInjectable({ factory: function FavNavsQuery_Factory() { return new FavNavsQuery(i0.inject(i1.FavNavsStore), i0.inject(i2.Router), i0.inject(i3.Location)); }, token: FavNavsQuery, providedIn: "root" });
if (false) {
    /** @type {?} */
    FavNavsQuery.prototype.favorites$;
    /**
     * @type {?}
     * @protected
     */
    FavNavsQuery.prototype.store;
    /**
     * @type {?}
     * @protected
     */
    FavNavsQuery.prototype.router;
    /**
     * @type {?}
     * @protected
     */
    FavNavsQuery.prototype.ngLocation;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmF2bmF2cy5xdWVyeS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvbGF5b3V0L25hdi9mYXZuYXYvc3RhdGUvZmF2bmF2cy5xdWVyeS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQU0sV0FBVyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQVMsTUFBTSxpQkFBaUIsQ0FBQztBQUMvRSxPQUFPLEVBQUUsWUFBWSxFQUFnQixNQUFNLGlCQUFpQixDQUFDO0FBRTdELE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUN6QyxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFFM0MsT0FBTyxFQUFFLEdBQUcsRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7OztBQUdyQyxNQUFNLE9BQU8sWUFBYSxTQUFRLFdBQW1DOzs7Ozs7SUFHbkUsWUFDWSxLQUFtQixFQUNuQixNQUFjLEVBQ2QsVUFBb0I7UUFDOUIsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBSEgsVUFBSyxHQUFMLEtBQUssQ0FBYztRQUNuQixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsZUFBVSxHQUFWLFVBQVUsQ0FBVTtRQUxoQyxlQUFVLEdBQUcsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO0lBTzlCLENBQUM7Ozs7O0lBRUQsV0FBVyxDQUFDLFFBQWtCO1FBQzVCLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztZQUNwQixRQUFROzs7O1lBQUUsSUFBSSxDQUFDLEVBQUU7O3NCQUNULFlBQVksR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxRQUFRLEVBQUU7Z0JBQ3pILE9BQU8sSUFBSSxDQUFDLElBQUksS0FBSyxZQUFZLENBQUM7WUFDcEMsQ0FBQyxDQUFBO1NBQ0YsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHOzs7O1FBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEVBQUMsQ0FBQyxDQUFDO0lBQzFDLENBQUM7Ozs7O0lBRUQsY0FBYyxDQUFDLFFBQWtCO1FBQy9CLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQztZQUNqQixRQUFROzs7O1lBQUUsSUFBSSxDQUFDLEVBQUU7O3NCQUNULFlBQVksR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxRQUFRLEVBQUU7Z0JBQ3pILE9BQU8sSUFBSSxDQUFDLElBQUksS0FBSyxZQUFZLENBQUM7WUFDcEMsQ0FBQyxDQUFBO1NBQ0YsQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7SUFDaEIsQ0FBQzs7Ozs7SUFFRCxZQUFZLENBQUMsUUFBa0I7UUFDN0IsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDO1lBQ2pCLFFBQVE7Ozs7WUFBRSxJQUFJLENBQUMsRUFBRTs7c0JBQ1QsWUFBWSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLFFBQVEsRUFBRTtnQkFDekgsT0FBTyxJQUFJLENBQUMsSUFBSSxLQUFLLFlBQVksQ0FBQztZQUNwQyxDQUFDLENBQUE7U0FDRixDQUFDLENBQUMsTUFBTTs7OztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDeEMsQ0FBQzs7O1lBcENGLFVBQVUsU0FBQyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUU7Ozs7WUFQekIsWUFBWTtZQUVaLE1BQU07WUFDTixRQUFROzs7OztJQU1mLGtDQUE4Qjs7Ozs7SUFHNUIsNkJBQTZCOzs7OztJQUM3Qiw4QkFBd0I7Ozs7O0lBQ3hCLGtDQUE4QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IElELCBRdWVyeUVudGl0eSwgaXNEZWZpbmVkLCB0b0Jvb2xlYW4sIGlzTmlsIH0gZnJvbSAnQGRhdG9yYW1hL2FraXRhJztcbmltcG9ydCB7IEZhdk5hdnNTdG9yZSwgRmF2TmF2c1N0YXRlIH0gZnJvbSAnLi9mYXZuYXZzLnN0b3JlJztcbmltcG9ydCB7IE1lbnVJdGVtIH0gZnJvbSAnLi4vLi4vbWVudS9tZW51LWl0ZW0vbWVudS1pdGVtLm1vZGVsJztcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQgeyBMb2NhdGlvbiB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBtYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5cbkBJbmplY3RhYmxlKHsgcHJvdmlkZWRJbjogJ3Jvb3QnIH0pXG5leHBvcnQgY2xhc3MgRmF2TmF2c1F1ZXJ5IGV4dGVuZHMgUXVlcnlFbnRpdHk8RmF2TmF2c1N0YXRlLCBNZW51SXRlbT4ge1xuICBmYXZvcml0ZXMkID0gdGhpcy5zZWxlY3RBbGwoKTtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcm90ZWN0ZWQgc3RvcmU6IEZhdk5hdnNTdG9yZSxcbiAgICBwcm90ZWN0ZWQgcm91dGVyOiBSb3V0ZXIsXG4gICAgcHJvdGVjdGVkIG5nTG9jYXRpb246IExvY2F0aW9uKSB7XG4gICAgc3VwZXIoc3RvcmUpO1xuICB9XG5cbiAgaXNGYXZvcml0ZWQobWVudUl0ZW06IE1lbnVJdGVtKTogT2JzZXJ2YWJsZTxib29sZWFuPiB7XG4gICAgcmV0dXJuIHRoaXMuc2VsZWN0QWxsKHtcbiAgICAgIGZpbHRlckJ5OiBpdGVtID0+IHtcbiAgICAgICAgY29uc3QgbWVudUl0ZW1MaW5rID0gdGhpcy5yb3V0ZXIuY3JlYXRlVXJsVHJlZShBcnJheS5pc0FycmF5KG1lbnVJdGVtLmxpbmspID8gbWVudUl0ZW0ubGluayA6IFttZW51SXRlbS5saW5rXSkudG9TdHJpbmcoKTtcbiAgICAgICAgcmV0dXJuIGl0ZW0ubGluayA9PT0gbWVudUl0ZW1MaW5rO1xuICAgICAgfVxuICAgIH0pLnBpcGUobWFwKHZhbHVlID0+IHRvQm9vbGVhbih2YWx1ZSkpKTtcbiAgfVxuXG4gIGdldElzRmF2b3JpdGVkKG1lbnVJdGVtOiBNZW51SXRlbSk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLmdldEFsbCh7XG4gICAgICBmaWx0ZXJCeTogaXRlbSA9PiB7XG4gICAgICAgIGNvbnN0IG1lbnVJdGVtTGluayA9IHRoaXMucm91dGVyLmNyZWF0ZVVybFRyZWUoQXJyYXkuaXNBcnJheShtZW51SXRlbS5saW5rKSA/IG1lbnVJdGVtLmxpbmsgOiBbbWVudUl0ZW0ubGlua10pLnRvU3RyaW5nKCk7XG4gICAgICAgIHJldHVybiBpdGVtLmxpbmsgPT09IG1lbnVJdGVtTGluaztcbiAgICAgIH1cbiAgICB9KS5sZW5ndGggPiAwO1xuICB9XG5cbiAgZ2V0RmF2b3JpdGVkKG1lbnVJdGVtOiBNZW51SXRlbSk6IE1lbnVJdGVtIHtcbiAgICByZXR1cm4gdGhpcy5nZXRBbGwoe1xuICAgICAgZmlsdGVyQnk6IGl0ZW0gPT4ge1xuICAgICAgICBjb25zdCBtZW51SXRlbUxpbmsgPSB0aGlzLnJvdXRlci5jcmVhdGVVcmxUcmVlKEFycmF5LmlzQXJyYXkobWVudUl0ZW0ubGluaykgPyBtZW51SXRlbS5saW5rIDogW21lbnVJdGVtLmxpbmtdKS50b1N0cmluZygpO1xuICAgICAgICByZXR1cm4gaXRlbS5saW5rID09PSBtZW51SXRlbUxpbms7XG4gICAgICB9XG4gICAgfSkuZmlsdGVyKGl0ZW0gPT4gaXNEZWZpbmVkKGl0ZW0pKVswXTtcbiAgfVxufVxuIl19