/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { NavStore } from './nav.store';
import * as i0 from "@angular/core";
import * as i1 from "./nav.store";
export class NavService {
    /**
     * @param {?} navStore
     */
    constructor(navStore) {
        this.navStore = navStore;
    }
    /**
     * @return {?}
     */
    toggleLeftNav() {
        this.navStore.update((/**
         * @param {?} state
         * @return {?}
         */
        state => ({
            leftnav: Object.assign({}, state.leftnav, { opened: !state.leftnav.opened, pinned: false })
        })));
    }
    /**
     * @return {?}
     */
    openLeftNav() {
        this.navStore.update((/**
         * @param {?} state
         * @return {?}
         */
        state => ({
            leftnav: Object.assign({}, state.leftnav, { opened: true })
        })));
    }
    /**
     * @return {?}
     */
    closeLeftNav() {
        this.navStore.update((/**
         * @param {?} state
         * @return {?}
         */
        state => ({
            leftnav: Object.assign({}, state.leftnav, { opened: false, pinned: false })
        })));
    }
    /**
     * @return {?}
     */
    pinLeftNav() {
        this.navStore.update((/**
         * @param {?} state
         * @return {?}
         */
        state => ({
            leftnav: Object.assign({}, state.leftnav, { pinned: true })
        })));
    }
    /**
     * @return {?}
     */
    unpinLeftNav() {
        this.navStore.update((/**
         * @param {?} state
         * @return {?}
         */
        state => ({
            leftnav: Object.assign({}, state.leftnav, { pinned: false })
        })));
    }
    /**
     * @return {?}
     */
    togglePinLeftNav() {
        this.navStore.update((/**
         * @param {?} state
         * @return {?}
         */
        state => ({
            leftnav: Object.assign({}, state.leftnav, { pinned: !state.leftnav.pinned })
        })));
    }
    /**
     * @return {?}
     */
    toggleRightNav() {
        this.navStore.update((/**
         * @param {?} state
         * @return {?}
         */
        state => ({
            rightnav: Object.assign({}, state.rightnav, { opened: !state.rightnav.opened, pinned: false })
        })));
    }
    /**
     * @return {?}
     */
    openRightNav() {
        this.navStore.update((/**
         * @param {?} state
         * @return {?}
         */
        state => ({
            rightnav: Object.assign({}, state.rightnav, { opened: true })
        })));
    }
    /**
     * @return {?}
     */
    closeRightNav() {
        this.navStore.update((/**
         * @param {?} state
         * @return {?}
         */
        state => ({
            rightnav: Object.assign({}, state.rightnav, { opened: false, pinned: false })
        })));
    }
    /**
     * @return {?}
     */
    pinRightNav() {
        this.navStore.update((/**
         * @param {?} state
         * @return {?}
         */
        state => ({
            rightnav: Object.assign({}, state.rightnav, { pinned: true })
        })));
    }
    /**
     * @return {?}
     */
    unpinRightNav() {
        this.navStore.update((/**
         * @param {?} state
         * @return {?}
         */
        state => ({
            rightnav: Object.assign({}, state.rightnav, { pinned: false })
        })));
    }
    /**
     * @return {?}
     */
    togglePinRightNav() {
        this.navStore.update((/**
         * @param {?} state
         * @return {?}
         */
        state => ({
            rightnav: Object.assign({}, state.rightnav, { pinned: !state.rightnav.pinned })
        })));
    }
}
NavService.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */
NavService.ctorParameters = () => [
    { type: NavStore }
];
/** @nocollapse */ NavService.ngInjectableDef = i0.defineInjectable({ factory: function NavService_Factory() { return new NavService(i0.inject(i1.NavStore)); }, token: NavService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    NavService.prototype.navStore;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2LnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL2xheW91dC9uYXYvc3RhdGUvbmF2LnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGFBQWEsQ0FBQzs7O0FBR3ZDLE1BQU0sT0FBTyxVQUFVOzs7O0lBQ3JCLFlBQW9CLFFBQWtCO1FBQWxCLGFBQVEsR0FBUixRQUFRLENBQVU7SUFBRyxDQUFDOzs7O0lBRTFDLGFBQWE7UUFDWCxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU07Ozs7UUFBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDN0IsT0FBTyxvQkFDRixLQUFLLENBQUMsT0FBTyxJQUNoQixNQUFNLEVBQUUsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFDN0IsTUFBTSxFQUFFLEtBQUssR0FDZDtTQUNGLENBQUMsRUFBQyxDQUFDO0lBQ04sQ0FBQzs7OztJQUVELFdBQVc7UUFDVCxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU07Ozs7UUFBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDN0IsT0FBTyxvQkFDRixLQUFLLENBQUMsT0FBTyxJQUNoQixNQUFNLEVBQUUsSUFBSSxHQUNiO1NBQ0YsQ0FBQyxFQUFDLENBQUM7SUFDTixDQUFDOzs7O0lBRUQsWUFBWTtRQUNWLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTTs7OztRQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUM3QixPQUFPLG9CQUNGLEtBQUssQ0FBQyxPQUFPLElBQ2hCLE1BQU0sRUFBRSxLQUFLLEVBQ2IsTUFBTSxFQUFFLEtBQUssR0FDZDtTQUNGLENBQUMsRUFBQyxDQUFDO0lBQ04sQ0FBQzs7OztJQUVELFVBQVU7UUFDUixJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU07Ozs7UUFBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDN0IsT0FBTyxvQkFDRixLQUFLLENBQUMsT0FBTyxJQUNoQixNQUFNLEVBQUUsSUFBSSxHQUNiO1NBQ0YsQ0FBQyxFQUFDLENBQUM7SUFDTixDQUFDOzs7O0lBRUQsWUFBWTtRQUNWLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTTs7OztRQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUM3QixPQUFPLG9CQUNGLEtBQUssQ0FBQyxPQUFPLElBQ2hCLE1BQU0sRUFBRSxLQUFLLEdBQ2Q7U0FDRixDQUFDLEVBQUMsQ0FBQztJQUNOLENBQUM7Ozs7SUFFRCxnQkFBZ0I7UUFDZCxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU07Ozs7UUFBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDN0IsT0FBTyxvQkFDRixLQUFLLENBQUMsT0FBTyxJQUNoQixNQUFNLEVBQUUsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FDOUI7U0FDRixDQUFDLEVBQUMsQ0FBQztJQUNOLENBQUM7Ozs7SUFXRCxjQUFjO1FBQ1osSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNOzs7O1FBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQzdCLFFBQVEsb0JBQ0gsS0FBSyxDQUFDLFFBQVEsSUFDakIsTUFBTSxFQUFFLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQzlCLE1BQU0sRUFBRSxLQUFLLEdBQ2Q7U0FDRixDQUFDLEVBQUMsQ0FBQztJQUNOLENBQUM7Ozs7SUFFRCxZQUFZO1FBQ1YsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNOzs7O1FBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQzdCLFFBQVEsb0JBQ0gsS0FBSyxDQUFDLFFBQVEsSUFDakIsTUFBTSxFQUFFLElBQUksR0FDYjtTQUNGLENBQUMsRUFBQyxDQUFDO0lBQ04sQ0FBQzs7OztJQUVELGFBQWE7UUFDWCxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU07Ozs7UUFBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDN0IsUUFBUSxvQkFDSCxLQUFLLENBQUMsUUFBUSxJQUNqQixNQUFNLEVBQUUsS0FBSyxFQUNiLE1BQU0sRUFBRSxLQUFLLEdBQ2Q7U0FDRixDQUFDLEVBQUMsQ0FBQztJQUNOLENBQUM7Ozs7SUFFRCxXQUFXO1FBQ1QsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNOzs7O1FBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQzdCLFFBQVEsb0JBQ0gsS0FBSyxDQUFDLFFBQVEsSUFDakIsTUFBTSxFQUFFLElBQUksR0FDYjtTQUNGLENBQUMsRUFBQyxDQUFDO0lBQ04sQ0FBQzs7OztJQUVELGFBQWE7UUFDWCxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU07Ozs7UUFBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDN0IsUUFBUSxvQkFDSCxLQUFLLENBQUMsUUFBUSxJQUNqQixNQUFNLEVBQUUsS0FBSyxHQUNkO1NBQ0YsQ0FBQyxFQUFDLENBQUM7SUFDTixDQUFDOzs7O0lBRUQsaUJBQWlCO1FBQ2YsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNOzs7O1FBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQzdCLFFBQVEsb0JBQ0gsS0FBSyxDQUFDLFFBQVEsSUFDakIsTUFBTSxFQUFFLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQy9CO1NBQ0YsQ0FBQyxFQUFDLENBQUM7SUFDTixDQUFDOzs7WUEzSEYsVUFBVSxTQUFDLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRTs7OztZQUZ6QixRQUFROzs7Ozs7OztJQUlILDhCQUEwQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE5hdlN0b3JlIH0gZnJvbSAnLi9uYXYuc3RvcmUnO1xuXG5ASW5qZWN0YWJsZSh7IHByb3ZpZGVkSW46ICdyb290JyB9KVxuZXhwb3J0IGNsYXNzIE5hdlNlcnZpY2Uge1xuICBjb25zdHJ1Y3Rvcihwcml2YXRlIG5hdlN0b3JlOiBOYXZTdG9yZSkge31cblxuICB0b2dnbGVMZWZ0TmF2KCkge1xuICAgIHRoaXMubmF2U3RvcmUudXBkYXRlKHN0YXRlID0+ICh7XG4gICAgICBsZWZ0bmF2OiB7XG4gICAgICAgIC4uLnN0YXRlLmxlZnRuYXYsXG4gICAgICAgIG9wZW5lZDogIXN0YXRlLmxlZnRuYXYub3BlbmVkLFxuICAgICAgICBwaW5uZWQ6IGZhbHNlXG4gICAgICB9XG4gICAgfSkpO1xuICB9XG5cbiAgb3BlbkxlZnROYXYoKSB7XG4gICAgdGhpcy5uYXZTdG9yZS51cGRhdGUoc3RhdGUgPT4gKHtcbiAgICAgIGxlZnRuYXY6IHtcbiAgICAgICAgLi4uc3RhdGUubGVmdG5hdixcbiAgICAgICAgb3BlbmVkOiB0cnVlXG4gICAgICB9XG4gICAgfSkpO1xuICB9XG5cbiAgY2xvc2VMZWZ0TmF2KCkge1xuICAgIHRoaXMubmF2U3RvcmUudXBkYXRlKHN0YXRlID0+ICh7XG4gICAgICBsZWZ0bmF2OiB7XG4gICAgICAgIC4uLnN0YXRlLmxlZnRuYXYsXG4gICAgICAgIG9wZW5lZDogZmFsc2UsXG4gICAgICAgIHBpbm5lZDogZmFsc2VcbiAgICAgIH1cbiAgICB9KSk7XG4gIH1cblxuICBwaW5MZWZ0TmF2KCkge1xuICAgIHRoaXMubmF2U3RvcmUudXBkYXRlKHN0YXRlID0+ICh7XG4gICAgICBsZWZ0bmF2OiB7XG4gICAgICAgIC4uLnN0YXRlLmxlZnRuYXYsXG4gICAgICAgIHBpbm5lZDogdHJ1ZVxuICAgICAgfVxuICAgIH0pKTtcbiAgfVxuXG4gIHVucGluTGVmdE5hdigpIHtcbiAgICB0aGlzLm5hdlN0b3JlLnVwZGF0ZShzdGF0ZSA9PiAoe1xuICAgICAgbGVmdG5hdjoge1xuICAgICAgICAuLi5zdGF0ZS5sZWZ0bmF2LFxuICAgICAgICBwaW5uZWQ6IGZhbHNlXG4gICAgICB9XG4gICAgfSkpO1xuICB9XG5cbiAgdG9nZ2xlUGluTGVmdE5hdigpIHtcbiAgICB0aGlzLm5hdlN0b3JlLnVwZGF0ZShzdGF0ZSA9PiAoe1xuICAgICAgbGVmdG5hdjoge1xuICAgICAgICAuLi5zdGF0ZS5sZWZ0bmF2LFxuICAgICAgICBwaW5uZWQ6ICFzdGF0ZS5sZWZ0bmF2LnBpbm5lZFxuICAgICAgfVxuICAgIH0pKTtcbiAgfVxuXG5cblxuXG5cblxuXG5cblxuXG4gIHRvZ2dsZVJpZ2h0TmF2KCkge1xuICAgIHRoaXMubmF2U3RvcmUudXBkYXRlKHN0YXRlID0+ICh7XG4gICAgICByaWdodG5hdjoge1xuICAgICAgICAuLi5zdGF0ZS5yaWdodG5hdixcbiAgICAgICAgb3BlbmVkOiAhc3RhdGUucmlnaHRuYXYub3BlbmVkLFxuICAgICAgICBwaW5uZWQ6IGZhbHNlXG4gICAgICB9XG4gICAgfSkpO1xuICB9XG5cbiAgb3BlblJpZ2h0TmF2KCkge1xuICAgIHRoaXMubmF2U3RvcmUudXBkYXRlKHN0YXRlID0+ICh7XG4gICAgICByaWdodG5hdjoge1xuICAgICAgICAuLi5zdGF0ZS5yaWdodG5hdixcbiAgICAgICAgb3BlbmVkOiB0cnVlXG4gICAgICB9XG4gICAgfSkpO1xuICB9XG5cbiAgY2xvc2VSaWdodE5hdigpIHtcbiAgICB0aGlzLm5hdlN0b3JlLnVwZGF0ZShzdGF0ZSA9PiAoe1xuICAgICAgcmlnaHRuYXY6IHtcbiAgICAgICAgLi4uc3RhdGUucmlnaHRuYXYsXG4gICAgICAgIG9wZW5lZDogZmFsc2UsXG4gICAgICAgIHBpbm5lZDogZmFsc2VcbiAgICAgIH1cbiAgICB9KSk7XG4gIH1cblxuICBwaW5SaWdodE5hdigpIHtcbiAgICB0aGlzLm5hdlN0b3JlLnVwZGF0ZShzdGF0ZSA9PiAoe1xuICAgICAgcmlnaHRuYXY6IHtcbiAgICAgICAgLi4uc3RhdGUucmlnaHRuYXYsXG4gICAgICAgIHBpbm5lZDogdHJ1ZVxuICAgICAgfVxuICAgIH0pKTtcbiAgfVxuXG4gIHVucGluUmlnaHROYXYoKSB7XG4gICAgdGhpcy5uYXZTdG9yZS51cGRhdGUoc3RhdGUgPT4gKHtcbiAgICAgIHJpZ2h0bmF2OiB7XG4gICAgICAgIC4uLnN0YXRlLnJpZ2h0bmF2LFxuICAgICAgICBwaW5uZWQ6IGZhbHNlXG4gICAgICB9XG4gICAgfSkpO1xuICB9XG5cbiAgdG9nZ2xlUGluUmlnaHROYXYoKSB7XG4gICAgdGhpcy5uYXZTdG9yZS51cGRhdGUoc3RhdGUgPT4gKHtcbiAgICAgIHJpZ2h0bmF2OiB7XG4gICAgICAgIC4uLnN0YXRlLnJpZ2h0bmF2LFxuICAgICAgICBwaW5uZWQ6ICFzdGF0ZS5yaWdodG5hdi5waW5uZWRcbiAgICAgIH1cbiAgICB9KSk7XG4gIH1cbn1cbiJdfQ==