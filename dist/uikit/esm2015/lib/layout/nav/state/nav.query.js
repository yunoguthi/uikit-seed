/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, Optional, Inject } from '@angular/core';
import { Query } from '@datorama/akita';
import { NavStore } from './nav.store';
import { FavoritosService, FAVORITOS_SERVICE_TOKEN } from '../../../utils/favoritos.service';
import * as i0 from "@angular/core";
import * as i1 from "./nav.store";
import * as i2 from "../../../utils/favoritos.service";
export class NavQuery extends Query {
    /**
     * @param {?} store
     * @param {?} favoritosService
     */
    constructor(store, favoritosService) {
        super(store);
        this.store = store;
        this.favoritosService = favoritosService;
        this.leftnav$ = this.select((/**
         * @param {?} session
         * @return {?}
         */
        session => session.leftnav));
        this.rightnav$ = this.select((/**
         * @param {?} session
         * @return {?}
         */
        session => session.rightnav));
        this.isLeftPinned$ = this.select((/**
         * @param {?} session
         * @return {?}
         */
        session => session.leftnav.pinned));
        this.isRightPinned$ = this.select((/**
         * @param {?} session
         * @return {?}
         */
        session => session.rightnav.pinned));
        this.hasFavoritosService = this.favoritosService != null;
    }
}
NavQuery.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
NavQuery.ctorParameters = () => [
    { type: NavStore },
    { type: FavoritosService, decorators: [{ type: Optional }, { type: Inject, args: [FAVORITOS_SERVICE_TOKEN,] }] }
];
/** @nocollapse */ NavQuery.ngInjectableDef = i0.defineInjectable({ factory: function NavQuery_Factory() { return new NavQuery(i0.inject(i1.NavStore), i0.inject(i2.FAVORITOS_SERVICE_TOKEN, 8)); }, token: NavQuery, providedIn: "root" });
if (false) {
    /** @type {?} */
    NavQuery.prototype.leftnav$;
    /** @type {?} */
    NavQuery.prototype.rightnav$;
    /** @type {?} */
    NavQuery.prototype.isLeftPinned$;
    /** @type {?} */
    NavQuery.prototype.isRightPinned$;
    /** @type {?} */
    NavQuery.prototype.hasFavoritosService;
    /**
     * @type {?}
     * @protected
     */
    NavQuery.prototype.store;
    /**
     * @type {?}
     * @protected
     */
    NavQuery.prototype.favoritosService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2LnF1ZXJ5LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9sYXlvdXQvbmF2L3N0YXRlL25hdi5xdWVyeS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzdELE9BQU8sRUFBZSxLQUFLLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUNyRCxPQUFPLEVBQUUsUUFBUSxFQUFZLE1BQU0sYUFBYSxDQUFDO0FBQ2pELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSx1QkFBdUIsRUFBRSxNQUFNLGtDQUFrQyxDQUFDOzs7O0FBSzdGLE1BQU0sT0FBTyxRQUFTLFNBQVEsS0FBZTs7Ozs7SUFVM0MsWUFDWSxLQUFlLEVBQzhCLGdCQUFrQztRQUV6RixLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7UUFISCxVQUFLLEdBQUwsS0FBSyxDQUFVO1FBQzhCLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFWcEYsYUFBUSxHQUFHLElBQUksQ0FBQyxNQUFNOzs7O1FBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFDLENBQUM7UUFDbkQsY0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNOzs7O1FBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFDLENBQUM7UUFFckQsa0JBQWEsR0FBRyxJQUFJLENBQUMsTUFBTTs7OztRQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUMsQ0FBQztRQUMvRCxtQkFBYyxHQUFHLElBQUksQ0FBQyxNQUFNOzs7O1FBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBQyxDQUFDO1FBRWpFLHdCQUFtQixHQUFHLElBQUksQ0FBQyxnQkFBZ0IsSUFBSSxJQUFJLENBQUM7SUFPM0QsQ0FBQzs7O1lBbEJGLFVBQVUsU0FBQztnQkFDVixVQUFVLEVBQUUsTUFBTTthQUNuQjs7OztZQUxRLFFBQVE7WUFDUixnQkFBZ0IsdUJBaUJwQixRQUFRLFlBQUksTUFBTSxTQUFDLHVCQUF1Qjs7Ozs7SUFWN0MsNEJBQTBEOztJQUMxRCw2QkFBNEQ7O0lBRTVELGlDQUFzRTs7SUFDdEUsa0NBQXdFOztJQUV4RSx1Q0FBMkQ7Ozs7O0lBR3pELHlCQUF5Qjs7Ozs7SUFDekIsb0NBQXlGIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSwgT3B0aW9uYWwsIEluamVjdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgUXVlcnlFbnRpdHksIFF1ZXJ5IH0gZnJvbSAnQGRhdG9yYW1hL2FraXRhJztcbmltcG9ydCB7IE5hdlN0b3JlLCBOYXZTdGF0ZSB9IGZyb20gJy4vbmF2LnN0b3JlJztcbmltcG9ydCB7IEZhdm9yaXRvc1NlcnZpY2UsIEZBVk9SSVRPU19TRVJWSUNFX1RPS0VOIH0gZnJvbSAnLi4vLi4vLi4vdXRpbHMvZmF2b3JpdG9zLnNlcnZpY2UnO1xuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBOYXZRdWVyeSBleHRlbmRzIFF1ZXJ5PE5hdlN0YXRlPiB7XG5cbiAgcHVibGljIGxlZnRuYXYkID0gdGhpcy5zZWxlY3Qoc2Vzc2lvbiA9PiBzZXNzaW9uLmxlZnRuYXYpO1xuICBwdWJsaWMgcmlnaHRuYXYkID0gdGhpcy5zZWxlY3Qoc2Vzc2lvbiA9PiBzZXNzaW9uLnJpZ2h0bmF2KTtcblxuICBwdWJsaWMgaXNMZWZ0UGlubmVkJCA9IHRoaXMuc2VsZWN0KHNlc3Npb24gPT4gc2Vzc2lvbi5sZWZ0bmF2LnBpbm5lZCk7XG4gIHB1YmxpYyBpc1JpZ2h0UGlubmVkJCA9IHRoaXMuc2VsZWN0KHNlc3Npb24gPT4gc2Vzc2lvbi5yaWdodG5hdi5waW5uZWQpO1xuXG4gIHB1YmxpYyBoYXNGYXZvcml0b3NTZXJ2aWNlID0gdGhpcy5mYXZvcml0b3NTZXJ2aWNlICE9IG51bGw7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJvdGVjdGVkIHN0b3JlOiBOYXZTdG9yZSxcbiAgICBAT3B0aW9uYWwoKSBASW5qZWN0KEZBVk9SSVRPU19TRVJWSUNFX1RPS0VOKSBwcm90ZWN0ZWQgZmF2b3JpdG9zU2VydmljZTogRmF2b3JpdG9zU2VydmljZVxuICAgICkge1xuICAgIHN1cGVyKHN0b3JlKTtcbiAgfVxuXG5cblxufVxuIl19