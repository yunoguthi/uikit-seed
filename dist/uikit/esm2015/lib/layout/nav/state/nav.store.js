/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { StoreConfig, Store } from '@datorama/akita';
import * as i0 from "@angular/core";
/**
 * @record
 */
export function NavState() { }
if (false) {
    /** @type {?} */
    NavState.prototype.leftnav;
    /** @type {?} */
    NavState.prototype.rightnav;
}
/**
 * @return {?}
 */
export function createInitialState() {
    return {
        leftnav: {
            opened: false,
            pinned: false
        },
        rightnav: {
            opened: false,
            pinned: false
        }
    };
}
let NavStore = class NavStore extends Store {
    constructor() {
        super(createInitialState());
    }
};
NavStore.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */
NavStore.ctorParameters = () => [];
/** @nocollapse */ NavStore.ngInjectableDef = i0.defineInjectable({ factory: function NavStore_Factory() { return new NavStore(); }, token: NavStore, providedIn: "root" });
NavStore = tslib_1.__decorate([
    StoreConfig({ name: 'nav' }),
    tslib_1.__metadata("design:paramtypes", [])
], NavStore);
export { NavStore };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2LnN0b3JlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9sYXlvdXQvbmF2L3N0YXRlL25hdi5zdG9yZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUE0QixXQUFXLEVBQUUsS0FBSyxFQUFFLE1BQU0saUJBQWlCLENBQUM7Ozs7O0FBSS9FLDhCQVNDOzs7SUFSQywyQkFHRTs7SUFDRiw0QkFHRTs7Ozs7QUFHSixNQUFNLFVBQVUsa0JBQWtCO0lBQ2hDLE9BQU87UUFDTCxPQUFPLEVBQUU7WUFDUCxNQUFNLEVBQUUsS0FBSztZQUNiLE1BQU0sRUFBRSxLQUFLO1NBQ2Q7UUFDRCxRQUFRLEVBQUU7WUFDUixNQUFNLEVBQUUsS0FBSztZQUNiLE1BQU0sRUFBRSxLQUFLO1NBQ2Q7S0FDRixDQUFDO0FBQ0osQ0FBQztJQUlZLFFBQVEsU0FBUixRQUFTLFNBQVEsS0FBZTtJQUMzQztRQUNFLEtBQUssQ0FBQyxrQkFBa0IsRUFBRSxDQUFDLENBQUM7SUFDOUIsQ0FBQztDQUNGLENBQUE7O1lBTkEsVUFBVSxTQUFDLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRTs7Ozs7QUFFckIsUUFBUTtJQURwQixXQUFXLENBQUMsRUFBRSxJQUFJLEVBQUUsS0FBSyxFQUFFLENBQUM7O0dBQ2hCLFFBQVEsQ0FJcEI7U0FKWSxRQUFRIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRW50aXR5U3RhdGUsIEVudGl0eVN0b3JlLCBTdG9yZUNvbmZpZywgU3RvcmUgfSBmcm9tICdAZGF0b3JhbWEvYWtpdGEnO1xuXG5cblxuZXhwb3J0IGludGVyZmFjZSBOYXZTdGF0ZSB7XG4gIGxlZnRuYXY6IHtcbiAgICBvcGVuZWQ6IGJvb2xlYW47XG4gICAgcGlubmVkOiBib29sZWFuO1xuICB9O1xuICByaWdodG5hdjoge1xuICAgIG9wZW5lZDogYm9vbGVhbjtcbiAgICBwaW5uZWQ6IGJvb2xlYW47XG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBjcmVhdGVJbml0aWFsU3RhdGUoKTogTmF2U3RhdGUge1xuICByZXR1cm4ge1xuICAgIGxlZnRuYXY6IHtcbiAgICAgIG9wZW5lZDogZmFsc2UsXG4gICAgICBwaW5uZWQ6IGZhbHNlXG4gICAgfSxcbiAgICByaWdodG5hdjoge1xuICAgICAgb3BlbmVkOiBmYWxzZSxcbiAgICAgIHBpbm5lZDogZmFsc2VcbiAgICB9XG4gIH07XG59XG5cbkBJbmplY3RhYmxlKHsgcHJvdmlkZWRJbjogJ3Jvb3QnIH0pXG5AU3RvcmVDb25maWcoeyBuYW1lOiAnbmF2JyB9KVxuZXhwb3J0IGNsYXNzIE5hdlN0b3JlIGV4dGVuZHMgU3RvcmU8TmF2U3RhdGU+IHtcbiAgY29uc3RydWN0b3IoKSB7XG4gICAgc3VwZXIoY3JlYXRlSW5pdGlhbFN0YXRlKCkpO1xuICB9XG59XG4iXX0=