/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { ChangeDetectionStrategy, Component, ContentChildren, Input, QueryList } from '@angular/core';
import { MatExpansionPanel } from '@angular/material';
import { FavNavsQuery } from '../favnav/state/favnavs.query';
import { Observable } from 'rxjs';
import { LayoutService } from '../../layout.service';
import { MenuItemComponent } from './menu-item/menu-item.component';
import { guid } from '@datorama/akita';
import { map } from 'rxjs/operators';
export class MenuComponent {
    /**
     * @param {?} favNavsQuery
     * @param {?} layoutService
     */
    constructor(favNavsQuery, layoutService) {
        this.favNavsQuery = favNavsQuery;
        this.layoutService = layoutService;
        this.favorites$ = this.favNavsQuery.favorites$;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.favoritesMobile$ = this.favorites$.pipe(map((/**
         * @param {?} itens
         * @return {?}
         */
        (itens) => {
            /** @type {?} */
            const favoritesMobile = [(/** @type {?} */ ({ title: 'Meus Favoritos', icon: 'fas fa-star', children: itens }))];
            return favoritesMobile;
        })));
    }
    /**
     * @return {?}
     */
    ngAfterContentInit() {
        /** @type {?} */
        const functionMap = (/**
         * @param {?} r
         * @return {?}
         */
        (r) => {
            /** @type {?} */
            const menuItem = (/** @type {?} */ ({
                id: r.id,
                title: r.title,
                link: r.link,
                icon: r.icon,
                tags: r.tags
            }));
            if (r.children.length > 1) {
                if (!r.id) {
                    r.id = guid();
                }
                menuItem.nodeId = r.id;
                menuItem.children = r.children.filter((/**
                 * @param {?} b
                 * @return {?}
                 */
                b => b !== r)).map(functionMap);
            }
            return menuItem;
        });
        /** @type {?} */
        const menuItems = this.children.toArray().map(functionMap);
        if (this.itensRecursivo) {
            this.itensRecursivo.subscribe((/**
             * @param {?} c
             * @return {?}
             */
            (c) => menuItems.push(...c)));
        }
        if (menuItems && menuItems.length > 0) {
            this.layoutService.setMenuItems(menuItems);
        }
    }
}
MenuComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-menu',
                template: "<mat-list tabindex=\"-1\" role=\"list\" (click)=\"$event.stopPropagation()\">\n  <uikit-menu-search></uikit-menu-search>\n  <ng-content select=\"[menu]\"></ng-content>\n</mat-list>\n",
                changeDetection: ChangeDetectionStrategy.OnPush,
                viewProviders: [MatExpansionPanel],
                styles: [""]
            }] }
];
/** @nocollapse */
MenuComponent.ctorParameters = () => [
    { type: FavNavsQuery },
    { type: LayoutService }
];
MenuComponent.propDecorators = {
    itensRecursivo: [{ type: Input }],
    children: [{ type: ContentChildren, args: [MenuItemComponent, { descendants: false },] }]
};
if (false) {
    /** @type {?} */
    MenuComponent.prototype.itensRecursivo;
    /**
     * @type {?}
     * @private
     */
    MenuComponent.prototype.children;
    /** @type {?} */
    MenuComponent.prototype.favorites$;
    /** @type {?} */
    MenuComponent.prototype.favoritesMobile$;
    /**
     * @type {?}
     * @protected
     */
    MenuComponent.prototype.favNavsQuery;
    /**
     * @type {?}
     * @protected
     */
    MenuComponent.prototype.layoutService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVudS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL2xheW91dC9uYXYvbWVudS9tZW51LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUVMLHVCQUF1QixFQUN2QixTQUFTLEVBQ1QsZUFBZSxFQUNmLEtBQUssRUFFTCxTQUFTLEVBQ1YsTUFBTSxlQUFlLENBQUM7QUFDdkIsT0FBTyxFQUFDLGlCQUFpQixFQUFDLE1BQU0sbUJBQW1CLENBQUM7QUFDcEQsT0FBTyxFQUFDLFlBQVksRUFBQyxNQUFNLCtCQUErQixDQUFDO0FBQzNELE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxNQUFNLENBQUM7QUFFaEMsT0FBTyxFQUFDLGFBQWEsRUFBQyxNQUFNLHNCQUFzQixDQUFDO0FBQ25ELE9BQU8sRUFBQyxpQkFBaUIsRUFBQyxNQUFNLGlDQUFpQyxDQUFDO0FBQ2xFLE9BQU8sRUFBQyxJQUFJLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUNyQyxPQUFPLEVBQUMsR0FBRyxFQUFDLE1BQU0sZ0JBQWdCLENBQUM7QUFTbkMsTUFBTSxPQUFPLGFBQWE7Ozs7O0lBU3hCLFlBQ1ksWUFBMEIsRUFDMUIsYUFBNEI7UUFENUIsaUJBQVksR0FBWixZQUFZLENBQWM7UUFDMUIsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFMakMsZUFBVSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDO0lBTWpELENBQUM7Ozs7SUFFRCxRQUFRO1FBQ04sSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEdBQUc7Ozs7UUFBQyxDQUFDLEtBQXNCLEVBQUUsRUFBRTs7a0JBQ3BFLGVBQWUsR0FBb0IsQ0FBQyxtQkFBQSxFQUFDLEtBQUssRUFBRSxnQkFBZ0IsRUFBRSxJQUFJLEVBQUUsYUFBYSxFQUFFLFFBQVEsRUFBRSxLQUFLLEVBQUMsRUFBTyxDQUFDO1lBQ2pILE9BQU8sZUFBZSxDQUFDO1FBQ3pCLENBQUMsRUFBQyxDQUFDLENBQUM7SUFDTixDQUFDOzs7O0lBRUQsa0JBQWtCOztjQUNWLFdBQVc7Ozs7UUFBRyxDQUFDLENBQW9CLEVBQUUsRUFBRTs7a0JBQ3JDLFFBQVEsR0FBRyxtQkFBQTtnQkFDZixFQUFFLEVBQUUsQ0FBQyxDQUFDLEVBQUU7Z0JBQ1IsS0FBSyxFQUFFLENBQUMsQ0FBQyxLQUFLO2dCQUNkLElBQUksRUFBRSxDQUFDLENBQUMsSUFBSTtnQkFDWixJQUFJLEVBQUUsQ0FBQyxDQUFDLElBQUk7Z0JBQ1osSUFBSSxFQUFFLENBQUMsQ0FBQyxJQUFJO2FBQ2IsRUFBWTtZQUViLElBQUksQ0FBQyxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dCQUN6QixJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRTtvQkFDVCxDQUFDLENBQUMsRUFBRSxHQUFHLElBQUksRUFBRSxDQUFDO2lCQUNmO2dCQUNELFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLEVBQUUsQ0FBQztnQkFDdkIsUUFBUSxDQUFDLFFBQVEsR0FBRyxDQUFDLENBQUMsUUFBUSxDQUFDLE1BQU07Ozs7Z0JBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxFQUFDLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDO2FBQ3RFO1lBRUQsT0FBTyxRQUFRLENBQUM7UUFDbEIsQ0FBQyxDQUFBOztjQUVLLFNBQVMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUM7UUFFMUQsSUFBSSxJQUFJLENBQUMsY0FBYyxFQUFFO1lBQ3ZCLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUzs7OztZQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUMsQ0FBQztTQUM1RDtRQUVELElBQUksU0FBUyxJQUFJLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3JDLElBQUksQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1NBQzVDO0lBQ0gsQ0FBQzs7O1lBMURGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsWUFBWTtnQkFDdEIsa01BQW9DO2dCQUVwQyxlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtnQkFDL0MsYUFBYSxFQUFFLENBQUMsaUJBQWlCLENBQUM7O2FBQ25DOzs7O1lBZE8sWUFBWTtZQUdaLGFBQWE7Ozs2QkFjbEIsS0FBSzt1QkFFTCxlQUFlLFNBQUMsaUJBQWlCLEVBQUUsRUFBQyxXQUFXLEVBQUUsS0FBSyxFQUFDOzs7O0lBRnhELHVDQUFnRDs7Ozs7SUFFaEQsaUNBQytDOztJQUMvQyxtQ0FBaUQ7O0lBQ2pELHlDQUF3Qjs7Ozs7SUFHdEIscUNBQW9DOzs7OztJQUNwQyxzQ0FBc0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xuICBBZnRlckNvbnRlbnRJbml0LFxuICBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneSxcbiAgQ29tcG9uZW50LFxuICBDb250ZW50Q2hpbGRyZW4sXG4gIElucHV0LFxuICBPbkluaXQsXG4gIFF1ZXJ5TGlzdFxufSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7TWF0RXhwYW5zaW9uUGFuZWx9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcbmltcG9ydCB7RmF2TmF2c1F1ZXJ5fSBmcm9tICcuLi9mYXZuYXYvc3RhdGUvZmF2bmF2cy5xdWVyeSc7XG5pbXBvcnQge09ic2VydmFibGV9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHtNZW51SXRlbX0gZnJvbSAnLi9tZW51LWl0ZW0vbWVudS1pdGVtLm1vZGVsJztcbmltcG9ydCB7TGF5b3V0U2VydmljZX0gZnJvbSAnLi4vLi4vbGF5b3V0LnNlcnZpY2UnO1xuaW1wb3J0IHtNZW51SXRlbUNvbXBvbmVudH0gZnJvbSAnLi9tZW51LWl0ZW0vbWVudS1pdGVtLmNvbXBvbmVudCc7XG5pbXBvcnQge2d1aWR9IGZyb20gJ0BkYXRvcmFtYS9ha2l0YSc7XG5pbXBvcnQge21hcH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICd1aWtpdC1tZW51JyxcbiAgdGVtcGxhdGVVcmw6ICcuL21lbnUuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9tZW51LmNvbXBvbmVudC5zY3NzJ10sXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxuICB2aWV3UHJvdmlkZXJzOiBbTWF0RXhwYW5zaW9uUGFuZWxdXG59KVxuZXhwb3J0IGNsYXNzIE1lbnVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIEFmdGVyQ29udGVudEluaXQge1xuXG4gIEBJbnB1dCgpIGl0ZW5zUmVjdXJzaXZvOiBPYnNlcnZhYmxlPE1lbnVJdGVtW10+O1xuXG4gIEBDb250ZW50Q2hpbGRyZW4oTWVudUl0ZW1Db21wb25lbnQsIHtkZXNjZW5kYW50czogZmFsc2V9KVxuICBwcml2YXRlIGNoaWxkcmVuOiBRdWVyeUxpc3Q8TWVudUl0ZW1Db21wb25lbnQ+O1xuICBwdWJsaWMgZmF2b3JpdGVzJCA9IHRoaXMuZmF2TmF2c1F1ZXJ5LmZhdm9yaXRlcyQ7XG4gIHB1YmxpYyBmYXZvcml0ZXNNb2JpbGUkO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByb3RlY3RlZCBmYXZOYXZzUXVlcnk6IEZhdk5hdnNRdWVyeSxcbiAgICBwcm90ZWN0ZWQgbGF5b3V0U2VydmljZTogTGF5b3V0U2VydmljZSwgKSB7XG4gIH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICB0aGlzLmZhdm9yaXRlc01vYmlsZSQgPSB0aGlzLmZhdm9yaXRlcyQucGlwZShtYXAoKGl0ZW5zOiBBcnJheTxNZW51SXRlbT4pID0+IHtcbiAgICAgIGNvbnN0IGZhdm9yaXRlc01vYmlsZTogQXJyYXk8TWVudUl0ZW0+ID0gW3t0aXRsZTogJ01ldXMgRmF2b3JpdG9zJywgaWNvbjogJ2ZhcyBmYS1zdGFyJywgY2hpbGRyZW46IGl0ZW5zfSBhcyBhbnldO1xuICAgICAgcmV0dXJuIGZhdm9yaXRlc01vYmlsZTtcbiAgICB9KSk7XG4gIH1cblxuICBuZ0FmdGVyQ29udGVudEluaXQoKTogdm9pZCB7XG4gICAgY29uc3QgZnVuY3Rpb25NYXAgPSAocjogTWVudUl0ZW1Db21wb25lbnQpID0+IHtcbiAgICAgIGNvbnN0IG1lbnVJdGVtID0ge1xuICAgICAgICBpZDogci5pZCxcbiAgICAgICAgdGl0bGU6IHIudGl0bGUsXG4gICAgICAgIGxpbms6IHIubGluayxcbiAgICAgICAgaWNvbjogci5pY29uLFxuICAgICAgICB0YWdzOiByLnRhZ3NcbiAgICAgIH0gYXMgTWVudUl0ZW07XG5cbiAgICAgIGlmIChyLmNoaWxkcmVuLmxlbmd0aCA+IDEpIHtcbiAgICAgICAgaWYgKCFyLmlkKSB7XG4gICAgICAgICAgci5pZCA9IGd1aWQoKTtcbiAgICAgICAgfVxuICAgICAgICBtZW51SXRlbS5ub2RlSWQgPSByLmlkO1xuICAgICAgICBtZW51SXRlbS5jaGlsZHJlbiA9IHIuY2hpbGRyZW4uZmlsdGVyKGIgPT4gYiAhPT0gcikubWFwKGZ1bmN0aW9uTWFwKTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIG1lbnVJdGVtO1xuICAgIH07XG5cbiAgICBjb25zdCBtZW51SXRlbXMgPSB0aGlzLmNoaWxkcmVuLnRvQXJyYXkoKS5tYXAoZnVuY3Rpb25NYXApO1xuXG4gICAgaWYgKHRoaXMuaXRlbnNSZWN1cnNpdm8pIHtcbiAgICAgIHRoaXMuaXRlbnNSZWN1cnNpdm8uc3Vic2NyaWJlKChjKSA9PiBtZW51SXRlbXMucHVzaCguLi5jKSk7XG4gICAgfVxuXG4gICAgaWYgKG1lbnVJdGVtcyAmJiBtZW51SXRlbXMubGVuZ3RoID4gMCkge1xuICAgICAgdGhpcy5sYXlvdXRTZXJ2aWNlLnNldE1lbnVJdGVtcyhtZW51SXRlbXMpO1xuICAgIH1cbiAgfVxufVxuIl19