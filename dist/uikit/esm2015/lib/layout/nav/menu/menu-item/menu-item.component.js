/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ContentChildren, Input, QueryList } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { FavNavsService } from '../../favnav/state/favnavs.service';
import { FavNavsQuery } from '../../favnav/state/favnavs.query';
import { NestedTreeControl } from '@angular/cdk/tree';
import { map } from 'rxjs/operators';
import { MenuSearchService } from '../../menu-search/menu-search.service';
export class MenuItemComponent {
    /**
     * @param {?} router
     * @param {?} favNavsService
     * @param {?} favNavsQuery
     * @param {?} menuSearchService
     */
    constructor(router, favNavsService, favNavsQuery, menuSearchService) {
        this.router = router;
        this.favNavsService = favNavsService;
        this.favNavsQuery = favNavsQuery;
        this.menuSearchService = menuSearchService;
        this.hasItems$ = new BehaviorSubject(false);
        this.isFavorited = false;
        this.treeControlMobile = new NestedTreeControl((/**
         * @param {?} node
         * @return {?}
         */
        node => node.children));
        this.possuiRecursoDeFavoritos = this.favNavsService.possuiRecursoDeFavoritos;
        this.treeControl = new NestedTreeControl((/**
         * @param {?} node
         * @return {?}
         */
        node => node.children));
        this.favorites$ = this.favNavsQuery.favorites$;
        this.menuItems$ = this.menuSearchService.menuItems$;
        this.hasChild = (/**
         * @param {?} _
         * @param {?} node
         * @return {?}
         */
        (_, node) => !!node.children && node.children.length > 0);
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        /** @type {?} */
        const menuItem = {
            id: this.id,
            title: this.title,
            link: this.link,
            icon: this.icon,
            tags: this.tags
        };
        this.menuItem = menuItem;
        this.isFavorited$ = this.favNavsQuery.isFavorited(menuItem);
        this.isFavorited$.subscribe((/**
         * @param {?} r
         * @return {?}
         */
        r => this.isFavorited = r));
        this.favoritesMobile$ = this.favorites$.pipe(map((/**
         * @param {?} itens
         * @return {?}
         */
        (itens) => {
            /** @type {?} */
            const favoritesMobile = [(/** @type {?} */ ({ title: 'Meus Favoritos', icon: 'fas fa-star', children: itens }))];
            return favoritesMobile;
        })));
    }
    /**
     * @protected
     * @return {?}
     */
    updateHasItems() {
        this.hasItems$.next(this.children.length - 1 > 0);
    }
    /**
     * @return {?}
     */
    ngAfterContentInit() {
        this.updateHasItems();
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.childrenSubscription && this.childrenSubscription.closed === false) {
            this.childrenSubscription.unsubscribe();
        }
        if (this.isFavoritedSubscription && this.isFavoritedSubscription.closed === false) {
            this.isFavoritedSubscription.unsubscribe();
        }
    }
    /**
     * @param {?} menu
     * @return {?}
     */
    getIsFavorited(menu) {
        /** @type {?} */
        const item = { link: menu.link, title: menu.title, icon: menu.icon };
        return this.favNavsQuery.getIsFavorited((/** @type {?} */ (item)));
    }
    /**
     * @param {?} menu
     * @return {?}
     */
    toggleFavorito(menu) {
        /** @type {?} */
        const item = { link: menu.link, title: menu.title, icon: menu.icon };
        this.favNavsService.toggleItem((/** @type {?} */ (item)));
    }
}
MenuItemComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-menu-item',
                template: "<mat-tree [dataSource]=\"favoritesMobile$ | async\" [treeControl]=\"treeControlMobile\"\n          class=\"menu-tree is-mobile\"\n          *ngIf=\"(favorites$ | async).length > 0\">\n\n  <mat-nested-tree-node *matTreeNodeDef=\"let node; when: hasChild\">\n    <button mat-button matTreeNodeToggle\n            [attr.aria-label]=\"'toggle ' + node.title\">\n      <mat-icon class=\"{{ node.icon }}\"></mat-icon>\n      {{ node.title }}\n\n      <mat-icon\n        class=\"{{ treeControlMobile.isExpanded(node) ? 'fas fa-caret-up' : 'fas fa-caret-down' }}\"></mat-icon>\n    </button>\n\n    <ul class=\"menu-tree-collapse\" [class.menu-tree-hidden]=\"!treeControlMobile.isExpanded(node)\">\n      <ng-container matTreeNodeOutlet></ng-container>\n    </ul>\n  </mat-nested-tree-node>\n\n  <mat-tree-node *matTreeNodeDef=\"let node\" matTreeNodeToggle>\n    <uikit-highlight [item]=\"node\">\n      <a role=\"option\" [routerLinkActive]=\"'active'\" [routerLink]=\"node.link ? node.link : ''\"\n         [routerLinkActiveOptions]=\"{exact:true}\" mat-button matTooltip=\"Ir para '{{ node.title }}'\"\n         attr.aria-label=\"Ir para {{ node.title }}\">\n        <mat-icon class=\"{{ node.icon }}\"></mat-icon>\n        {{ node.title }}\n      </a>\n    </uikit-highlight>\n  </mat-tree-node>\n</mat-tree>\n\n<mat-tree [dataSource]=\"menuItems$ | async\" [treeControl]=\"treeControl\" class=\"menu-tree\">\n\n  <!-- This is the tree node template for leaf nodes -->\n  <mat-tree-node *matTreeNodeDef=\"let node\" matTreeNodeToggle>\n    <uikit-highlight [item]=\"node\">\n      <!-- use a disabled button to provide padding for tree leaf -->\n      <a role=\"option\" [routerLinkActive]=\"'active'\" [routerLink]=\"node.link ? node.link : ''\"\n         [routerLinkActiveOptions]=\"{exact:true}\" mat-button matTooltip=\"Ir para '{{ node.title }}'\"\n         attr.aria-label=\"Ir para {{ node.title }}\">\n        <mat-icon class=\"{{ node.icon }}\"></mat-icon>\n        {{ node.title }}\n      </a>\n      <button mat-icon-button *ngIf=\"possuiRecursoDeFavoritos && node.link\">\n        <mat-icon class=\"fa-lg fas fa-star\"\n                  [matTooltip]=\"!getIsFavorited(node) ? 'Adicionar \\'' + node.title + '\\' aos favoritos' : 'Remover \\'' + node.title + '\\' dos favoritos'\"\n                  (click)=\"toggleFavorito(node)\" [color]=\"getIsFavorited(node) ? 'accent' : ''\">\n        </mat-icon>\n      </button>\n    </uikit-highlight>\n  </mat-tree-node>\n  <!-- This is the tree node template for expandable nodes -->\n  <mat-nested-tree-node *matTreeNodeDef=\"let node; when: hasChild\">\n    <li>\n      <button mat-button matTreeNodeToggle\n              [attr.aria-label]=\"'toggle ' + node.title\">\n        <mat-icon class=\"{{ node.icon }}\"></mat-icon>\n        {{ node.title }}\n        <mat-icon class=\"{{ treeControl.isExpanded(node) ? 'fas fa-caret-up' : 'fas fa-caret-down' }}\"></mat-icon>\n      </button>\n      <ul class=\"menu-tree-collapse\" [class.menu-tree-hidden]=\"!treeControl.isExpanded(node)\">\n        <ng-container matTreeNodeOutlet></ng-container>\n      </ul>\n    </li>\n  </mat-nested-tree-node>\n</mat-tree>\n"
            }] }
];
/** @nocollapse */
MenuItemComponent.ctorParameters = () => [
    { type: Router },
    { type: FavNavsService },
    { type: FavNavsQuery },
    { type: MenuSearchService }
];
MenuItemComponent.propDecorators = {
    id: [{ type: Input }],
    icon: [{ type: Input }],
    link: [{ type: Input }],
    title: [{ type: Input }],
    tags: [{ type: Input }],
    children: [{ type: ContentChildren, args: [MenuItemComponent, { descendants: false },] }]
};
if (false) {
    /** @type {?} */
    MenuItemComponent.prototype.id;
    /** @type {?} */
    MenuItemComponent.prototype.icon;
    /** @type {?} */
    MenuItemComponent.prototype.link;
    /** @type {?} */
    MenuItemComponent.prototype.title;
    /** @type {?} */
    MenuItemComponent.prototype.tags;
    /** @type {?} */
    MenuItemComponent.prototype.children;
    /** @type {?} */
    MenuItemComponent.prototype.hasItems$;
    /**
     * @type {?}
     * @protected
     */
    MenuItemComponent.prototype.childrenSubscription;
    /**
     * @type {?}
     * @protected
     */
    MenuItemComponent.prototype.isFavoritedSubscription;
    /** @type {?} */
    MenuItemComponent.prototype.isFavorited$;
    /** @type {?} */
    MenuItemComponent.prototype.isFavorited;
    /** @type {?} */
    MenuItemComponent.prototype.treeControlMobile;
    /** @type {?} */
    MenuItemComponent.prototype.possuiRecursoDeFavoritos;
    /** @type {?} */
    MenuItemComponent.prototype.treeControl;
    /** @type {?} */
    MenuItemComponent.prototype.favorites$;
    /** @type {?} */
    MenuItemComponent.prototype.favoritesMobile$;
    /** @type {?} */
    MenuItemComponent.prototype.menuItems$;
    /**
     * @type {?}
     * @protected
     */
    MenuItemComponent.prototype.menuItem;
    /** @type {?} */
    MenuItemComponent.prototype.hasChild;
    /**
     * @type {?}
     * @private
     */
    MenuItemComponent.prototype.router;
    /**
     * @type {?}
     * @private
     */
    MenuItemComponent.prototype.favNavsService;
    /**
     * @type {?}
     * @private
     */
    MenuItemComponent.prototype.favNavsQuery;
    /**
     * @type {?}
     * @protected
     */
    MenuItemComponent.prototype.menuSearchService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVudS1pdGVtLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvbGF5b3V0L25hdi9tZW51L21lbnUtaXRlbS9tZW51LWl0ZW0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQW1CLFNBQVMsRUFBRSxlQUFlLEVBQUUsS0FBSyxFQUFxQixTQUFTLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDaEgsT0FBTyxFQUFDLE1BQU0sRUFBQyxNQUFNLGlCQUFpQixDQUFDO0FBQ3ZDLE9BQU8sRUFBQyxlQUFlLEVBQTJCLE1BQU0sTUFBTSxDQUFDO0FBQy9ELE9BQU8sRUFBQyxjQUFjLEVBQUMsTUFBTSxvQ0FBb0MsQ0FBQztBQUNsRSxPQUFPLEVBQUMsWUFBWSxFQUFDLE1BQU0sa0NBQWtDLENBQUM7QUFFOUQsT0FBTyxFQUFDLGlCQUFpQixFQUFDLE1BQU0sbUJBQW1CLENBQUM7QUFDcEQsT0FBTyxFQUFDLEdBQUcsRUFBQyxNQUFNLGdCQUFnQixDQUFDO0FBQ25DLE9BQU8sRUFBQyxpQkFBaUIsRUFBQyxNQUFNLHVDQUF1QyxDQUFDO0FBT3hFLE1BQU0sT0FBTyxpQkFBaUI7Ozs7Ozs7SUFRNUIsWUFDVSxNQUFjLEVBQ2QsY0FBOEIsRUFDOUIsWUFBMEIsRUFDeEIsaUJBQW9DO1FBSHRDLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsaUJBQVksR0FBWixZQUFZLENBQWM7UUFDeEIsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQUl6QyxjQUFTLEdBQUcsSUFBSSxlQUFlLENBQVUsS0FBSyxDQUFDLENBQUM7UUFNaEQsZ0JBQVcsR0FBRyxLQUFLLENBQUM7UUFDcEIsc0JBQWlCLEdBQUcsSUFBSSxpQkFBaUI7Ozs7UUFBVyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUMsQ0FBQztRQUMzRSw2QkFBd0IsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLHdCQUF3QixDQUFDO1FBQ3hFLGdCQUFXLEdBQUcsSUFBSSxpQkFBaUI7Ozs7UUFBVyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUMsQ0FBQztRQUNyRSxlQUFVLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUM7UUFFMUMsZUFBVSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUM7UUFJL0MsYUFBUTs7Ozs7UUFBRyxDQUFDLENBQVMsRUFBRSxJQUFjLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBQztJQWxCN0YsQ0FBQzs7OztJQW9CRCxRQUFROztjQUNBLFFBQVEsR0FBYTtZQUN6QixFQUFFLEVBQUUsSUFBSSxDQUFDLEVBQUU7WUFDWCxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7WUFDakIsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJO1lBQ2YsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJO1lBQ2YsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJO1NBQ2hCO1FBQ0QsSUFBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7UUFDekIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUM1RCxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVM7Ozs7UUFBQyxDQUFDLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsQ0FBQyxFQUFDLENBQUM7UUFFdkQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEdBQUc7Ozs7UUFBQyxDQUFDLEtBQXNCLEVBQUUsRUFBRTs7a0JBQ3BFLGVBQWUsR0FBb0IsQ0FBQyxtQkFBQSxFQUFDLEtBQUssRUFBRSxnQkFBZ0IsRUFBRSxJQUFJLEVBQUUsYUFBYSxFQUFFLFFBQVEsRUFBRSxLQUFLLEVBQUMsRUFBTyxDQUFDO1lBQ2pILE9BQU8sZUFBZSxDQUFDO1FBQ3pCLENBQUMsRUFBQyxDQUFDLENBQUM7SUFDTixDQUFDOzs7OztJQUVTLGNBQWM7UUFDdEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO0lBQ3BELENBQUM7Ozs7SUFFRCxrQkFBa0I7UUFDaEIsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO0lBQ3hCLENBQUM7Ozs7SUFFRCxXQUFXO1FBQ1QsSUFBSSxJQUFJLENBQUMsb0JBQW9CLElBQUksSUFBSSxDQUFDLG9CQUFvQixDQUFDLE1BQU0sS0FBSyxLQUFLLEVBQUU7WUFDM0UsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQ3pDO1FBQ0QsSUFBSSxJQUFJLENBQUMsdUJBQXVCLElBQUksSUFBSSxDQUFDLHVCQUF1QixDQUFDLE1BQU0sS0FBSyxLQUFLLEVBQUU7WUFDakYsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQzVDO0lBQ0gsQ0FBQzs7Ozs7SUFFTSxjQUFjLENBQUMsSUFBYzs7Y0FDNUIsSUFBSSxHQUFHLEVBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJLEVBQUM7UUFDbEUsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxtQkFBQSxJQUFJLEVBQVksQ0FBQyxDQUFDO0lBQzVELENBQUM7Ozs7O0lBRU0sY0FBYyxDQUFDLElBQWM7O2NBQzVCLElBQUksR0FBRyxFQUFDLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSSxFQUFDO1FBQ2xFLElBQUksQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLG1CQUFBLElBQUksRUFBWSxDQUFDLENBQUM7SUFDbkQsQ0FBQzs7O1lBbEZGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsaUJBQWlCO2dCQUMzQix3bUdBQXlDO2FBRTFDOzs7O1lBYk8sTUFBTTtZQUVOLGNBQWM7WUFDZCxZQUFZO1lBSVosaUJBQWlCOzs7aUJBUXRCLEtBQUs7bUJBQ0wsS0FBSzttQkFDTCxLQUFLO29CQUNMLEtBQUs7bUJBQ0wsS0FBSzt1QkFDTCxlQUFlLFNBQUMsaUJBQWlCLEVBQUUsRUFBQyxXQUFXLEVBQUUsS0FBSyxFQUFDOzs7O0lBTHhELCtCQUEyQjs7SUFDM0IsaUNBQThCOztJQUM5QixpQ0FBMkI7O0lBQzNCLGtDQUE4Qjs7SUFDOUIsaUNBQWdDOztJQUNoQyxxQ0FBaUc7O0lBVWpHLHNDQUF1RDs7Ozs7SUFFdkQsaURBQTZDOzs7OztJQUM3QyxvREFBZ0Q7O0lBRWhELHlDQUF5Qzs7SUFDekMsd0NBQTJCOztJQUMzQiw4Q0FBa0Y7O0lBQ2xGLHFEQUErRTs7SUFDL0Usd0NBQTRFOztJQUM1RSx1Q0FBaUQ7O0lBQ2pELDZDQUF3Qjs7SUFDeEIsdUNBQXNEOzs7OztJQUV0RCxxQ0FBbUI7O0lBRW5CLHFDQUE2Rjs7Ozs7SUF2QjNGLG1DQUFzQjs7Ozs7SUFDdEIsMkNBQXNDOzs7OztJQUN0Qyx5Q0FBa0M7Ozs7O0lBQ2xDLDhDQUE4QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7QWZ0ZXJDb250ZW50SW5pdCwgQ29tcG9uZW50LCBDb250ZW50Q2hpbGRyZW4sIElucHV0LCBPbkRlc3Ryb3ksIE9uSW5pdCwgUXVlcnlMaXN0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7Um91dGVyfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHtCZWhhdmlvclN1YmplY3QsIE9ic2VydmFibGUsIFN1YnNjcmlwdGlvbn0gZnJvbSAncnhqcyc7XG5pbXBvcnQge0Zhdk5hdnNTZXJ2aWNlfSBmcm9tICcuLi8uLi9mYXZuYXYvc3RhdGUvZmF2bmF2cy5zZXJ2aWNlJztcbmltcG9ydCB7RmF2TmF2c1F1ZXJ5fSBmcm9tICcuLi8uLi9mYXZuYXYvc3RhdGUvZmF2bmF2cy5xdWVyeSc7XG5pbXBvcnQge01lbnVJdGVtfSBmcm9tICcuL21lbnUtaXRlbS5tb2RlbCc7XG5pbXBvcnQge05lc3RlZFRyZWVDb250cm9sfSBmcm9tICdAYW5ndWxhci9jZGsvdHJlZSc7XG5pbXBvcnQge21hcH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHtNZW51U2VhcmNoU2VydmljZX0gZnJvbSAnLi4vLi4vbWVudS1zZWFyY2gvbWVudS1zZWFyY2guc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ3Vpa2l0LW1lbnUtaXRlbScsXG4gIHRlbXBsYXRlVXJsOiAnLi9tZW51LWl0ZW0uY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZXM6IFtdXG59KVxuZXhwb3J0IGNsYXNzIE1lbnVJdGVtQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBBZnRlckNvbnRlbnRJbml0LCBPbkRlc3Ryb3kge1xuICBASW5wdXQoKSBwdWJsaWMgaWQ6IHN0cmluZztcbiAgQElucHV0KCkgcHVibGljIGljb24/OiBzdHJpbmc7XG4gIEBJbnB1dCgpIHB1YmxpYyBsaW5rPzogYW55O1xuICBASW5wdXQoKSBwdWJsaWMgdGl0bGU6IHN0cmluZztcbiAgQElucHV0KCkgcHVibGljIHRhZ3M/OiBzdHJpbmdbXTtcbiAgQENvbnRlbnRDaGlsZHJlbihNZW51SXRlbUNvbXBvbmVudCwge2Rlc2NlbmRhbnRzOiBmYWxzZX0pIGNoaWxkcmVuOiBRdWVyeUxpc3Q8TWVudUl0ZW1Db21wb25lbnQ+O1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsXG4gICAgcHJpdmF0ZSBmYXZOYXZzU2VydmljZTogRmF2TmF2c1NlcnZpY2UsXG4gICAgcHJpdmF0ZSBmYXZOYXZzUXVlcnk6IEZhdk5hdnNRdWVyeSxcbiAgICBwcm90ZWN0ZWQgbWVudVNlYXJjaFNlcnZpY2U6IE1lbnVTZWFyY2hTZXJ2aWNlLFxuICApIHtcbiAgfVxuXG4gIHB1YmxpYyBoYXNJdGVtcyQgPSBuZXcgQmVoYXZpb3JTdWJqZWN0PGJvb2xlYW4+KGZhbHNlKTtcblxuICBwcm90ZWN0ZWQgY2hpbGRyZW5TdWJzY3JpcHRpb246IFN1YnNjcmlwdGlvbjtcbiAgcHJvdGVjdGVkIGlzRmF2b3JpdGVkU3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XG5cbiAgcHVibGljIGlzRmF2b3JpdGVkJDogT2JzZXJ2YWJsZTxib29sZWFuPjtcbiAgcHVibGljIGlzRmF2b3JpdGVkID0gZmFsc2U7XG4gIHB1YmxpYyB0cmVlQ29udHJvbE1vYmlsZSA9IG5ldyBOZXN0ZWRUcmVlQ29udHJvbDxNZW51SXRlbT4obm9kZSA9PiBub2RlLmNoaWxkcmVuKTtcbiAgcHVibGljIHBvc3N1aVJlY3Vyc29EZUZhdm9yaXRvcyA9IHRoaXMuZmF2TmF2c1NlcnZpY2UucG9zc3VpUmVjdXJzb0RlRmF2b3JpdG9zO1xuICBwdWJsaWMgdHJlZUNvbnRyb2wgPSBuZXcgTmVzdGVkVHJlZUNvbnRyb2w8TWVudUl0ZW0+KG5vZGUgPT4gbm9kZS5jaGlsZHJlbik7XG4gIHB1YmxpYyBmYXZvcml0ZXMkID0gdGhpcy5mYXZOYXZzUXVlcnkuZmF2b3JpdGVzJDtcbiAgcHVibGljIGZhdm9yaXRlc01vYmlsZSQ7XG4gIHB1YmxpYyBtZW51SXRlbXMkID0gdGhpcy5tZW51U2VhcmNoU2VydmljZS5tZW51SXRlbXMkO1xuXG4gIHByb3RlY3RlZCBtZW51SXRlbTtcblxuICBwdWJsaWMgaGFzQ2hpbGQgPSAoXzogbnVtYmVyLCBub2RlOiBNZW51SXRlbSkgPT4gISFub2RlLmNoaWxkcmVuICYmIG5vZGUuY2hpbGRyZW4ubGVuZ3RoID4gMDtcblxuICBuZ09uSW5pdCgpIHtcbiAgICBjb25zdCBtZW51SXRlbTogTWVudUl0ZW0gPSB7XG4gICAgICBpZDogdGhpcy5pZCxcbiAgICAgIHRpdGxlOiB0aGlzLnRpdGxlLFxuICAgICAgbGluazogdGhpcy5saW5rLFxuICAgICAgaWNvbjogdGhpcy5pY29uLFxuICAgICAgdGFnczogdGhpcy50YWdzXG4gICAgfTtcbiAgICB0aGlzLm1lbnVJdGVtID0gbWVudUl0ZW07XG4gICAgdGhpcy5pc0Zhdm9yaXRlZCQgPSB0aGlzLmZhdk5hdnNRdWVyeS5pc0Zhdm9yaXRlZChtZW51SXRlbSk7XG4gICAgdGhpcy5pc0Zhdm9yaXRlZCQuc3Vic2NyaWJlKHIgPT4gdGhpcy5pc0Zhdm9yaXRlZCA9IHIpO1xuXG4gICAgdGhpcy5mYXZvcml0ZXNNb2JpbGUkID0gdGhpcy5mYXZvcml0ZXMkLnBpcGUobWFwKChpdGVuczogQXJyYXk8TWVudUl0ZW0+KSA9PiB7XG4gICAgICBjb25zdCBmYXZvcml0ZXNNb2JpbGU6IEFycmF5PE1lbnVJdGVtPiA9IFt7dGl0bGU6ICdNZXVzIEZhdm9yaXRvcycsIGljb246ICdmYXMgZmEtc3RhcicsIGNoaWxkcmVuOiBpdGVuc30gYXMgYW55XTtcbiAgICAgIHJldHVybiBmYXZvcml0ZXNNb2JpbGU7XG4gICAgfSkpO1xuICB9XG5cbiAgcHJvdGVjdGVkIHVwZGF0ZUhhc0l0ZW1zKCkge1xuICAgIHRoaXMuaGFzSXRlbXMkLm5leHQodGhpcy5jaGlsZHJlbi5sZW5ndGggLSAxID4gMCk7XG4gIH1cblxuICBuZ0FmdGVyQ29udGVudEluaXQoKTogdm9pZCB7XG4gICAgdGhpcy51cGRhdGVIYXNJdGVtcygpO1xuICB9XG5cbiAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XG4gICAgaWYgKHRoaXMuY2hpbGRyZW5TdWJzY3JpcHRpb24gJiYgdGhpcy5jaGlsZHJlblN1YnNjcmlwdGlvbi5jbG9zZWQgPT09IGZhbHNlKSB7XG4gICAgICB0aGlzLmNoaWxkcmVuU3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XG4gICAgfVxuICAgIGlmICh0aGlzLmlzRmF2b3JpdGVkU3Vic2NyaXB0aW9uICYmIHRoaXMuaXNGYXZvcml0ZWRTdWJzY3JpcHRpb24uY2xvc2VkID09PSBmYWxzZSkge1xuICAgICAgdGhpcy5pc0Zhdm9yaXRlZFN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyBnZXRJc0Zhdm9yaXRlZChtZW51OiBNZW51SXRlbSkge1xuICAgIGNvbnN0IGl0ZW0gPSB7bGluazogbWVudS5saW5rLCB0aXRsZTogbWVudS50aXRsZSwgaWNvbjogbWVudS5pY29ufTtcbiAgICByZXR1cm4gdGhpcy5mYXZOYXZzUXVlcnkuZ2V0SXNGYXZvcml0ZWQoaXRlbSBhcyBNZW51SXRlbSk7XG4gIH1cblxuICBwdWJsaWMgdG9nZ2xlRmF2b3JpdG8obWVudTogTWVudUl0ZW0pIHtcbiAgICBjb25zdCBpdGVtID0ge2xpbms6IG1lbnUubGluaywgdGl0bGU6IG1lbnUudGl0bGUsIGljb246IG1lbnUuaWNvbn07XG4gICAgdGhpcy5mYXZOYXZzU2VydmljZS50b2dnbGVJdGVtKGl0ZW0gYXMgTWVudUl0ZW0pO1xuICB9XG59XG4iXX0=