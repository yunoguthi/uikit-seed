/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { guid } from '@datorama/akita';
/**
 * @record
 */
export function MenuItem() { }
if (false) {
    /** @type {?} */
    MenuItem.prototype.id;
    /** @type {?|undefined} */
    MenuItem.prototype.children;
    /** @type {?} */
    MenuItem.prototype.title;
    /** @type {?|undefined} */
    MenuItem.prototype.icon;
    /** @type {?|undefined} */
    MenuItem.prototype.link;
    /** @type {?|undefined} */
    MenuItem.prototype.tags;
    /** @type {?|undefined} */
    MenuItem.prototype.nodeId;
}
/**
 * @param {?} item
 * @return {?}
 */
export function createItem(item) {
    return (/** @type {?} */ (Object.assign({}, item, { id: guid() })));
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVudS1pdGVtLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9sYXlvdXQvbmF2L21lbnUvbWVudS1pdGVtL21lbnUtaXRlbS5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFNLElBQUksRUFBRSxNQUFNLGlCQUFpQixDQUFDOzs7O0FBRTNDLDhCQVFDOzs7SUFQQyxzQkFBTzs7SUFDUCw0QkFBc0I7O0lBQ3RCLHlCQUFjOztJQUNkLHdCQUFjOztJQUNkLHdCQUFXOztJQUNYLHdCQUFnQjs7SUFDaEIsMEJBQVk7Ozs7OztBQUdkLE1BQU0sVUFBVSxVQUFVLENBQUMsSUFBYztJQUN2QyxPQUFPLHFDQUNGLElBQUksSUFDUCxFQUFFLEVBQUUsSUFBSSxFQUFFLEtBQ0MsQ0FBQztBQUNoQixDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSUQsIGd1aWQgfSBmcm9tICdAZGF0b3JhbWEvYWtpdGEnO1xuXG5leHBvcnQgaW50ZXJmYWNlIE1lbnVJdGVtIHtcbiAgaWQ6IElEO1xuICBjaGlsZHJlbj86IE1lbnVJdGVtW107XG4gIHRpdGxlOiBzdHJpbmc7XG4gIGljb24/OiBzdHJpbmc7XG4gIGxpbms/OiBhbnk7XG4gIHRhZ3M/OiBzdHJpbmdbXTtcbiAgbm9kZUlkPzogSUQ7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBjcmVhdGVJdGVtKGl0ZW06IE1lbnVJdGVtKSB7XG4gIHJldHVybiB7XG4gICAgLi4uaXRlbSxcbiAgICBpZDogZ3VpZCgpXG4gIH0gYXMgTWVudUl0ZW07XG59XG4iXX0=