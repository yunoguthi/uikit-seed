/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, EventEmitter } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
export class BreadcrumbService {
    /**
     * @param {?} router
     */
    constructor(router) {
        this.router = router;
        this.breadcrumbChanged = new EventEmitter(false);
        this.breadcrumbs = new Array();
        this.router.events.subscribe((/**
         * @param {?} routeEvent
         * @return {?}
         */
        (routeEvent) => { this.onRouteEvent(routeEvent); }));
    }
    /**
     * @param {?} route
     * @param {?} name
     * @return {?}
     */
    changeBreadcrumb(route, name) {
        /** @type {?} */
        const rootUrl = this.createRootUrl(route);
        /** @type {?} */
        const breadcrumb = this.breadcrumbs.find((/**
         * @param {?} bc
         * @return {?}
         */
        function (bc) { return bc.link === rootUrl; }));
        if (!breadcrumb) {
            return;
        }
        breadcrumb.title = name;
        this.breadcrumbChanged.emit(this.breadcrumbs);
    }
    /**
     * @private
     * @param {?} routeEvent
     * @return {?}
     */
    onRouteEvent(routeEvent) {
        if (!(routeEvent instanceof NavigationEnd)) {
            return;
        }
        /** @type {?} */
        let route = this.router.routerState.root.snapshot;
        /** @type {?} */
        let url = '';
        /** @type {?} */
        var breadCrumbIndex = 0;
        /** @type {?} */
        var newCrumbs = [];
        while (route.firstChild != null) {
            route = route.firstChild;
            if (route.routeConfig === null) {
                continue;
            }
            if (!route.routeConfig.path) {
                continue;
            }
            url += `/${this.createUrl(route)}`;
            if (!route.data['breadcrumb']) {
                continue;
            }
            /** @type {?} */
            var newCrumb = this.createBreadcrumb(route, url)
            // if (breadCrumbIndex < this.breadcrumbs.length) {
            //   var existing = this.breadcrumbs[breadCrumbIndex++];
            //   if (existing && existing.route == route.routeConfig) {
            //     newCrumb.title = existing.title;
            //   }
            // }
            ;
            // if (breadCrumbIndex < this.breadcrumbs.length) {
            //   var existing = this.breadcrumbs[breadCrumbIndex++];
            //   if (existing && existing.route == route.routeConfig) {
            //     newCrumb.title = existing.title;
            //   }
            // }
            newCrumbs.push(newCrumb);
        }
        this.breadcrumbs = newCrumbs;
        this.breadcrumbChanged.emit(this.breadcrumbs);
    }
    /**
     * @private
     * @param {?} route
     * @param {?} link
     * @return {?}
     */
    createBreadcrumb(route, link) {
        // Generates display text from data
        // -- Dynamic route params when ':[id]'
        /** @type {?} */
        const breadcrumb = route.data['breadcrumb'];
        /** @type {?} */
        let d = this.getTitleFormatted(breadcrumb, route);
        return {
            title: d,
            terminal: this.isTerminal(route),
            link: link,
            route: route.routeConfig
        };
    }
    /**
     * @param {?} breadcrumb
     * @param {?} route
     * @return {?}
     */
    getTitleFormatted(breadcrumb, route) {
        /** @type {?} */
        let d = '';
        /** @type {?} */
        const split = breadcrumb.split(' ');
        split.forEach((/**
         * @param {?} s
         * @return {?}
         */
        (s) => {
            d += `${s.indexOf(':') > -1 ? (route.params[s.slice(1)] ? route.params[s.slice(1)] : '') : s} `;
        }));
        d = d.slice(0, -1);
        return d;
    }
    /**
     * @private
     * @param {?} route
     * @return {?}
     */
    isTerminal(route) {
        return route.firstChild === null
            || route.firstChild.routeConfig === null
            || !route.firstChild.routeConfig.path;
    }
    /**
     * @private
     * @param {?} route
     * @return {?}
     */
    createUrl(route) {
        return route.url.map((/**
         * @param {?} s
         * @return {?}
         */
        function (s) { return s.toString(); })).join('/');
    }
    /**
     * @private
     * @param {?} route
     * @return {?}
     */
    createRootUrl(route) {
        /** @type {?} */
        let url = '';
        /** @type {?} */
        let next = route.root;
        while (next.firstChild !== null) {
            next = next.firstChild;
            if (next.routeConfig === null) {
                continue;
            }
            if (!next.routeConfig.path) {
                continue;
            }
            url += `/${this.createUrl(next)}`;
            if (next === route) {
                break;
            }
        }
        return url;
    }
}
BreadcrumbService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
BreadcrumbService.ctorParameters = () => [
    { type: Router }
];
if (false) {
    /** @type {?} */
    BreadcrumbService.prototype.breadcrumbChanged;
    /** @type {?} */
    BreadcrumbService.prototype.breadcrumbs;
    /**
     * @type {?}
     * @private
     */
    BreadcrumbService.prototype.router;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnJlYWRjcnVtYi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9sYXlvdXQvbmF2L2JyZWFkY3J1bWIvYnJlYWRjcnVtYnMvYnJlYWRjcnVtYi5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6RCxPQUFPLEVBQUUsTUFBTSxFQUFpQyxhQUFhLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUt2RixNQUFNLE9BQU8saUJBQWlCOzs7O0lBSzFCLFlBQW9CLE1BQWM7UUFBZCxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBSmxDLHNCQUFpQixHQUFHLElBQUksWUFBWSxDQUFlLEtBQUssQ0FBQyxDQUFDO1FBRW5ELGdCQUFXLEdBQUcsSUFBSSxLQUFLLEVBQWMsQ0FBQztRQUd6QyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxTQUFTOzs7O1FBQUMsQ0FBQyxVQUFVLEVBQUUsRUFBRSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQztJQUNyRixDQUFDOzs7Ozs7SUFFTSxnQkFBZ0IsQ0FBQyxLQUE2QixFQUFFLElBQVk7O2NBQ3pELE9BQU8sR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQzs7Y0FDbkMsVUFBVSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSTs7OztRQUFDLFVBQVUsRUFBRSxJQUFJLE9BQU8sRUFBRSxDQUFDLElBQUksS0FBSyxPQUFPLENBQUMsQ0FBQyxDQUFDLEVBQUM7UUFFdkYsSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUFFLE9BQU87U0FBRTtRQUU1QixVQUFVLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztRQUV4QixJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztJQUNsRCxDQUFDOzs7Ozs7SUFFTyxZQUFZLENBQUMsVUFBaUI7UUFDbEMsSUFBSSxDQUFDLENBQUMsVUFBVSxZQUFZLGFBQWEsQ0FBQyxFQUFFO1lBQUUsT0FBTztTQUFFOztZQUVuRCxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVE7O1lBQzdDLEdBQUcsR0FBRyxFQUFFOztZQUVSLGVBQWUsR0FBRyxDQUFDOztZQUNuQixTQUFTLEdBQUcsRUFBRTtRQUVsQixPQUFPLEtBQUssQ0FBQyxVQUFVLElBQUksSUFBSSxFQUFFO1lBQzdCLEtBQUssR0FBRyxLQUFLLENBQUMsVUFBVSxDQUFDO1lBRXpCLElBQUksS0FBSyxDQUFDLFdBQVcsS0FBSyxJQUFJLEVBQUU7Z0JBQUUsU0FBUzthQUFFO1lBQzdDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRTtnQkFBRSxTQUFTO2FBQUU7WUFFMUMsR0FBRyxJQUFJLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDO1lBRW5DLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFO2dCQUFFLFNBQVM7YUFBRTs7Z0JBRXhDLFFBQVEsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQztZQUVoRCxtREFBbUQ7WUFDbkQsd0RBQXdEO1lBRXhELDJEQUEyRDtZQUMzRCx1Q0FBdUM7WUFDdkMsTUFBTTtZQUNOLElBQUk7O1lBTkosbURBQW1EO1lBQ25ELHdEQUF3RDtZQUV4RCwyREFBMkQ7WUFDM0QsdUNBQXVDO1lBQ3ZDLE1BQU07WUFDTixJQUFJO1lBRUosU0FBUyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUM1QjtRQUVELElBQUksQ0FBQyxXQUFXLEdBQUcsU0FBUyxDQUFDO1FBQzdCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO0lBQ2xELENBQUM7Ozs7Ozs7SUFFTyxnQkFBZ0IsQ0FBQyxLQUE2QixFQUFFLElBQVk7Ozs7Y0FHNUQsVUFBVSxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDOztZQUV2QyxDQUFDLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUM7UUFFakQsT0FBTztZQUNMLEtBQUssRUFBRSxDQUFDO1lBQ04sUUFBUSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDO1lBQ2hDLElBQUksRUFBRSxJQUFJO1lBQ1YsS0FBSyxFQUFFLEtBQUssQ0FBQyxXQUFXO1NBQzNCLENBQUM7SUFDTixDQUFDOzs7Ozs7SUFFTSxpQkFBaUIsQ0FBQyxVQUFlLEVBQUUsS0FBNkI7O1lBQ2pFLENBQUMsR0FBRyxFQUFFOztjQUNKLEtBQUssR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQztRQUNuQyxLQUFLLENBQUMsT0FBTzs7OztRQUFDLENBQUMsQ0FBUyxFQUFFLEVBQUU7WUFDMUIsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQztRQUNsRyxDQUFDLEVBQUMsQ0FBQztRQUNILENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ25CLE9BQU8sQ0FBQyxDQUFDO0lBQ1gsQ0FBQzs7Ozs7O0lBRVMsVUFBVSxDQUFDLEtBQTZCO1FBQzVDLE9BQU8sS0FBSyxDQUFDLFVBQVUsS0FBSyxJQUFJO2VBQ3pCLEtBQUssQ0FBQyxVQUFVLENBQUMsV0FBVyxLQUFLLElBQUk7ZUFDckMsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUM7SUFDOUMsQ0FBQzs7Ozs7O0lBRU8sU0FBUyxDQUFDLEtBQTZCO1FBQzNDLE9BQU8sS0FBSyxDQUFDLEdBQUcsQ0FBQyxHQUFHOzs7O1FBQUMsVUFBVSxDQUFDLElBQUksT0FBTyxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDMUUsQ0FBQzs7Ozs7O0lBRU8sYUFBYSxDQUFDLEtBQTZCOztZQUMzQyxHQUFHLEdBQUcsRUFBRTs7WUFDUixJQUFJLEdBQUcsS0FBSyxDQUFDLElBQUk7UUFFckIsT0FBTyxJQUFJLENBQUMsVUFBVSxLQUFLLElBQUksRUFBRTtZQUM3QixJQUFJLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQztZQUV2QixJQUFJLElBQUksQ0FBQyxXQUFXLEtBQUssSUFBSSxFQUFFO2dCQUFFLFNBQVM7YUFBRTtZQUM1QyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUU7Z0JBQUUsU0FBUzthQUFFO1lBRXpDLEdBQUcsSUFBSSxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQztZQUVsQyxJQUFJLElBQUksS0FBSyxLQUFLLEVBQUU7Z0JBQUUsTUFBTTthQUFFO1NBQ2pDO1FBRUQsT0FBTyxHQUFHLENBQUM7SUFDZixDQUFDOzs7WUE1R0osVUFBVTs7OztZQUpGLE1BQU07Ozs7SUFNWCw4Q0FBMEQ7O0lBRTFELHdDQUE2Qzs7Ozs7SUFFakMsbUNBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBSb3V0ZXIsIEFjdGl2YXRlZFJvdXRlU25hcHNob3QsIEV2ZW50LCBOYXZpZ2F0aW9uRW5kIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcblxuaW1wb3J0IHsgQnJlYWRjcnVtYiB9IGZyb20gJy4vYnJlYWRjcnVtYic7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBCcmVhZGNydW1iU2VydmljZSB7XG4gICAgYnJlYWRjcnVtYkNoYW5nZWQgPSBuZXcgRXZlbnRFbWl0dGVyPEJyZWFkY3J1bWJbXT4oZmFsc2UpO1xuXG4gICAgcHVibGljIGJyZWFkY3J1bWJzID0gbmV3IEFycmF5PEJyZWFkY3J1bWI+KCk7XG5cbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIHJvdXRlcjogUm91dGVyKSB7XG4gICAgICAgIHRoaXMucm91dGVyLmV2ZW50cy5zdWJzY3JpYmUoKHJvdXRlRXZlbnQpID0+IHsgdGhpcy5vblJvdXRlRXZlbnQocm91dGVFdmVudCk7IH0pO1xuICAgIH1cblxuICAgIHB1YmxpYyBjaGFuZ2VCcmVhZGNydW1iKHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LCBuYW1lOiBzdHJpbmcpIHtcbiAgICAgICAgY29uc3Qgcm9vdFVybCA9IHRoaXMuY3JlYXRlUm9vdFVybChyb3V0ZSk7XG4gICAgICAgIGNvbnN0IGJyZWFkY3J1bWIgPSB0aGlzLmJyZWFkY3J1bWJzLmZpbmQoZnVuY3Rpb24gKGJjKSB7IHJldHVybiBiYy5saW5rID09PSByb290VXJsOyB9KTtcblxuICAgICAgICBpZiAoIWJyZWFkY3J1bWIpIHsgcmV0dXJuOyB9XG5cbiAgICAgICAgYnJlYWRjcnVtYi50aXRsZSA9IG5hbWU7XG5cbiAgICAgICAgdGhpcy5icmVhZGNydW1iQ2hhbmdlZC5lbWl0KHRoaXMuYnJlYWRjcnVtYnMpO1xuICAgIH1cblxuICAgIHByaXZhdGUgb25Sb3V0ZUV2ZW50KHJvdXRlRXZlbnQ6IEV2ZW50KSB7XG4gICAgICAgIGlmICghKHJvdXRlRXZlbnQgaW5zdGFuY2VvZiBOYXZpZ2F0aW9uRW5kKSkgeyByZXR1cm47IH1cblxuICAgICAgICBsZXQgcm91dGUgPSB0aGlzLnJvdXRlci5yb3V0ZXJTdGF0ZS5yb290LnNuYXBzaG90O1xuICAgICAgICBsZXQgdXJsID0gJyc7XG5cbiAgICAgICAgdmFyIGJyZWFkQ3J1bWJJbmRleCA9IDA7XG4gICAgICAgIHZhciBuZXdDcnVtYnMgPSBbXTtcblxuICAgICAgICB3aGlsZSAocm91dGUuZmlyc3RDaGlsZCAhPSBudWxsKSB7XG4gICAgICAgICAgICByb3V0ZSA9IHJvdXRlLmZpcnN0Q2hpbGQ7XG5cbiAgICAgICAgICAgIGlmIChyb3V0ZS5yb3V0ZUNvbmZpZyA9PT0gbnVsbCkgeyBjb250aW51ZTsgfVxuICAgICAgICAgICAgaWYgKCFyb3V0ZS5yb3V0ZUNvbmZpZy5wYXRoKSB7IGNvbnRpbnVlOyB9XG5cbiAgICAgICAgICAgIHVybCArPSBgLyR7dGhpcy5jcmVhdGVVcmwocm91dGUpfWA7XG5cbiAgICAgICAgICAgIGlmICghcm91dGUuZGF0YVsnYnJlYWRjcnVtYiddKSB7IGNvbnRpbnVlOyB9XG5cbiAgICAgICAgICAgIHZhciBuZXdDcnVtYiA9IHRoaXMuY3JlYXRlQnJlYWRjcnVtYihyb3V0ZSwgdXJsKVxuXG4gICAgICAgICAgICAvLyBpZiAoYnJlYWRDcnVtYkluZGV4IDwgdGhpcy5icmVhZGNydW1icy5sZW5ndGgpIHtcbiAgICAgICAgICAgIC8vICAgdmFyIGV4aXN0aW5nID0gdGhpcy5icmVhZGNydW1ic1ticmVhZENydW1iSW5kZXgrK107XG5cbiAgICAgICAgICAgIC8vICAgaWYgKGV4aXN0aW5nICYmIGV4aXN0aW5nLnJvdXRlID09IHJvdXRlLnJvdXRlQ29uZmlnKSB7XG4gICAgICAgICAgICAvLyAgICAgbmV3Q3J1bWIudGl0bGUgPSBleGlzdGluZy50aXRsZTtcbiAgICAgICAgICAgIC8vICAgfVxuICAgICAgICAgICAgLy8gfVxuXG4gICAgICAgICAgICBuZXdDcnVtYnMucHVzaChuZXdDcnVtYik7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLmJyZWFkY3J1bWJzID0gbmV3Q3J1bWJzO1xuICAgICAgICB0aGlzLmJyZWFkY3J1bWJDaGFuZ2VkLmVtaXQodGhpcy5icmVhZGNydW1icyk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBjcmVhdGVCcmVhZGNydW1iKHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LCBsaW5rOiBzdHJpbmcpOiBCcmVhZGNydW1iIHtcbiAgICAgIC8vIEdlbmVyYXRlcyBkaXNwbGF5IHRleHQgZnJvbSBkYXRhXG4gICAgICAvLyAtLSBEeW5hbWljIHJvdXRlIHBhcmFtcyB3aGVuICc6W2lkXSdcbiAgICAgIGNvbnN0IGJyZWFkY3J1bWIgPSByb3V0ZS5kYXRhWydicmVhZGNydW1iJ107XG5cbiAgICAgIGxldCBkID0gdGhpcy5nZXRUaXRsZUZvcm1hdHRlZChicmVhZGNydW1iLCByb3V0ZSk7XG5cbiAgICAgIHJldHVybiB7XG4gICAgICAgIHRpdGxlOiBkLFxuICAgICAgICAgIHRlcm1pbmFsOiB0aGlzLmlzVGVybWluYWwocm91dGUpLFxuICAgICAgICAgIGxpbms6IGxpbmssXG4gICAgICAgICAgcm91dGU6IHJvdXRlLnJvdXRlQ29uZmlnXG4gICAgICB9O1xuICB9XG5cbiAgcHVibGljIGdldFRpdGxlRm9ybWF0dGVkKGJyZWFkY3J1bWI6IGFueSwgcm91dGU6IEFjdGl2YXRlZFJvdXRlU25hcHNob3QpIHtcbiAgICBsZXQgZCA9ICcnO1xuICAgIGNvbnN0IHNwbGl0ID0gYnJlYWRjcnVtYi5zcGxpdCgnICcpO1xuICAgIHNwbGl0LmZvckVhY2goKHM6IHN0cmluZykgPT4ge1xuICAgICAgZCArPSBgJHtzLmluZGV4T2YoJzonKSA+IC0xID8gKHJvdXRlLnBhcmFtc1tzLnNsaWNlKDEpXSA/IHJvdXRlLnBhcmFtc1tzLnNsaWNlKDEpXSA6ICcnKSA6IHN9IGA7XG4gICAgfSk7XG4gICAgZCA9IGQuc2xpY2UoMCwgLTEpO1xuICAgIHJldHVybiBkO1xuICB9XG5cbiAgICBwcml2YXRlIGlzVGVybWluYWwocm91dGU6IEFjdGl2YXRlZFJvdXRlU25hcHNob3QpIHtcbiAgICAgICAgcmV0dXJuIHJvdXRlLmZpcnN0Q2hpbGQgPT09IG51bGxcbiAgICAgICAgICAgIHx8IHJvdXRlLmZpcnN0Q2hpbGQucm91dGVDb25maWcgPT09IG51bGxcbiAgICAgICAgICAgIHx8ICFyb3V0ZS5maXJzdENoaWxkLnJvdXRlQ29uZmlnLnBhdGg7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBjcmVhdGVVcmwocm91dGU6IEFjdGl2YXRlZFJvdXRlU25hcHNob3QpIHtcbiAgICAgICAgcmV0dXJuIHJvdXRlLnVybC5tYXAoZnVuY3Rpb24gKHMpIHsgcmV0dXJuIHMudG9TdHJpbmcoKTsgfSkuam9pbignLycpO1xuICAgIH1cblxuICAgIHByaXZhdGUgY3JlYXRlUm9vdFVybChyb3V0ZTogQWN0aXZhdGVkUm91dGVTbmFwc2hvdCkge1xuICAgICAgICBsZXQgdXJsID0gJyc7XG4gICAgICAgIGxldCBuZXh0ID0gcm91dGUucm9vdDtcblxuICAgICAgICB3aGlsZSAobmV4dC5maXJzdENoaWxkICE9PSBudWxsKSB7XG4gICAgICAgICAgICBuZXh0ID0gbmV4dC5maXJzdENoaWxkO1xuXG4gICAgICAgICAgICBpZiAobmV4dC5yb3V0ZUNvbmZpZyA9PT0gbnVsbCkgeyBjb250aW51ZTsgfVxuICAgICAgICAgICAgaWYgKCFuZXh0LnJvdXRlQ29uZmlnLnBhdGgpIHsgY29udGludWU7IH1cblxuICAgICAgICAgICAgdXJsICs9IGAvJHt0aGlzLmNyZWF0ZVVybChuZXh0KX1gO1xuXG4gICAgICAgICAgICBpZiAobmV4dCA9PT0gcm91dGUpIHsgYnJlYWs7IH1cbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiB1cmw7XG4gICAgfVxufVxuIl19