/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ApplicationRef } from '@angular/core';
import { BreadcrumbService } from './breadcrumb.service';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
export class BreadcrumbComponent {
    // breadcrumbs: Breadcrumb[];
    /**
     * @param {?} breadcrumbService
     * @param {?} router
     * @param {?} applicationRef
     */
    constructor(breadcrumbService, router, applicationRef) {
        this.breadcrumbService = breadcrumbService;
        this.router = router;
        this.applicationRef = applicationRef;
        this.breadcrumbsSubject = new BehaviorSubject([]);
        this.breadcrumbs$ = this.breadcrumbsSubject.asObservable();
        this.breadcrumbService.breadcrumbChanged.subscribe((/**
         * @param {?} crumbs
         * @return {?}
         */
        (crumbs) => { this.onBreadcrumbChange(crumbs); }));
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.breadcrumbsSubject.next(this.breadcrumbService.breadcrumbs);
    }
    /**
     * @private
     * @param {?} crumbs
     * @return {?}
     */
    onBreadcrumbChange(crumbs) {
        this.breadcrumbsSubject.next(crumbs);
        // this.applicationRef.tick();
    }
}
BreadcrumbComponent.decorators = [
    { type: Component, args: [{
                // tslint:disable-next-line:component-selector
                selector: 'breadcrumb',
                template: `<div #template>
    <ng-content></ng-content>
</div>
<div class="container" *ngIf="template.children.length == 0">
  <span *ngFor="let route of (breadcrumbs$ | async)">
  <a mat-button *ngIf="!route.terminal" href="" [routerLink]="[route.link]">{{ route.title }}</a>
  <a mat-button *ngIf="route.terminal">{{ route.title }}</a>
  </span>
</div>`
            }] }
];
/** @nocollapse */
BreadcrumbComponent.ctorParameters = () => [
    { type: BreadcrumbService },
    { type: Router },
    { type: ApplicationRef }
];
if (false) {
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.breadcrumbsSubject;
    /** @type {?} */
    BreadcrumbComponent.prototype.breadcrumbs$;
    /**
     * @type {?}
     * @private
     */
    BreadcrumbComponent.prototype.breadcrumbService;
    /**
     * @type {?}
     * @private
     */
    BreadcrumbComponent.prototype.router;
    /**
     * @type {?}
     * @private
     */
    BreadcrumbComponent.prototype.applicationRef;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnJlYWRjcnVtYi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL2xheW91dC9uYXYvYnJlYWRjcnVtYi9icmVhZGNydW1icy9icmVhZGNydW1iLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxjQUFjLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFHbEUsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDekQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUN2QyxPQUFPLEVBQUUsTUFBTSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFnQnpDLE1BQU0sT0FBTyxtQkFBbUI7Ozs7Ozs7SUFPNUIsWUFBb0IsaUJBQW9DLEVBQVUsTUFBYyxFQUFVLGNBQThCO1FBQXBHLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBbUI7UUFBVSxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQVUsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBTGhILHVCQUFrQixHQUFHLElBQUksZUFBZSxDQUFlLEVBQUUsQ0FBQyxDQUFDO1FBQzlELGlCQUFZLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFlBQVksRUFBRSxDQUFDO1FBS3pELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTOzs7O1FBQUMsQ0FBQyxNQUFvQixFQUFFLEVBQUUsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQztJQUNySCxDQUFDOzs7O0lBRUQsUUFBUTtRQUNOLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFdBQVcsQ0FBQyxDQUFDO0lBQ25FLENBQUM7Ozs7OztJQUVPLGtCQUFrQixDQUFDLE1BQW9CO1FBRTNDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDckMsOEJBQThCO0lBQ2xDLENBQUM7OztZQWpDSixTQUFTLFNBQUM7O2dCQUVQLFFBQVEsRUFBRSxZQUFZO2dCQUN0QixRQUFRLEVBQ1I7Ozs7Ozs7O09BUUc7YUFDTjs7OztZQWpCUSxpQkFBaUI7WUFFakIsTUFBTTtZQUxhLGNBQWM7Ozs7Ozs7SUF1QnhDLGlEQUFxRTs7SUFDckUsMkNBQTZEOzs7OztJQUkvQyxnREFBNEM7Ozs7O0lBQUUscUNBQXNCOzs7OztJQUFFLDZDQUFzQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBBcHBsaWNhdGlvblJlZiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBCcmVhZGNydW1iIH0gZnJvbSAnLi9icmVhZGNydW1iJztcbmltcG9ydCB7IEJyZWFkY3J1bWJTZXJ2aWNlIH0gZnJvbSAnLi9icmVhZGNydW1iLnNlcnZpY2UnO1xuaW1wb3J0IHsgQmVoYXZpb3JTdWJqZWN0IH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuXG5AQ29tcG9uZW50KHtcbiAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6Y29tcG9uZW50LXNlbGVjdG9yXG4gICAgc2VsZWN0b3I6ICdicmVhZGNydW1iJyxcbiAgICB0ZW1wbGF0ZTpcbiAgICBgPGRpdiAjdGVtcGxhdGU+XG4gICAgPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PlxuPC9kaXY+XG48ZGl2IGNsYXNzPVwiY29udGFpbmVyXCIgKm5nSWY9XCJ0ZW1wbGF0ZS5jaGlsZHJlbi5sZW5ndGggPT0gMFwiPlxuICA8c3BhbiAqbmdGb3I9XCJsZXQgcm91dGUgb2YgKGJyZWFkY3J1bWJzJCB8IGFzeW5jKVwiPlxuICA8YSBtYXQtYnV0dG9uICpuZ0lmPVwiIXJvdXRlLnRlcm1pbmFsXCIgaHJlZj1cIlwiIFtyb3V0ZXJMaW5rXT1cIltyb3V0ZS5saW5rXVwiPnt7IHJvdXRlLnRpdGxlIH19PC9hPlxuICA8YSBtYXQtYnV0dG9uICpuZ0lmPVwicm91dGUudGVybWluYWxcIj57eyByb3V0ZS50aXRsZSB9fTwvYT5cbiAgPC9zcGFuPlxuPC9kaXY+YFxufSlcbmV4cG9ydCBjbGFzcyBCcmVhZGNydW1iQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBwcm90ZWN0ZWQgYnJlYWRjcnVtYnNTdWJqZWN0ID0gbmV3IEJlaGF2aW9yU3ViamVjdDxCcmVhZGNydW1iW10+KFtdKTtcbiAgcHVibGljIGJyZWFkY3J1bWJzJCA9IHRoaXMuYnJlYWRjcnVtYnNTdWJqZWN0LmFzT2JzZXJ2YWJsZSgpO1xuXG4gICAgLy8gYnJlYWRjcnVtYnM6IEJyZWFkY3J1bWJbXTtcblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgYnJlYWRjcnVtYlNlcnZpY2U6IEJyZWFkY3J1bWJTZXJ2aWNlLCBwcml2YXRlIHJvdXRlcjogUm91dGVyLCBwcml2YXRlIGFwcGxpY2F0aW9uUmVmOiBBcHBsaWNhdGlvblJlZikge1xuICAgICAgdGhpcy5icmVhZGNydW1iU2VydmljZS5icmVhZGNydW1iQ2hhbmdlZC5zdWJzY3JpYmUoKGNydW1iczogQnJlYWRjcnVtYltdKSA9PiB7IHRoaXMub25CcmVhZGNydW1iQ2hhbmdlKGNydW1icyk7IH0pO1xuICAgIH1cblxuICAgIG5nT25Jbml0KCk6IHZvaWQge1xuICAgICAgdGhpcy5icmVhZGNydW1ic1N1YmplY3QubmV4dCh0aGlzLmJyZWFkY3J1bWJTZXJ2aWNlLmJyZWFkY3J1bWJzKTtcbiAgICB9XG5cbiAgICBwcml2YXRlIG9uQnJlYWRjcnVtYkNoYW5nZShjcnVtYnM6IEJyZWFkY3J1bWJbXSkge1xuXG4gICAgICAgIHRoaXMuYnJlYWRjcnVtYnNTdWJqZWN0Lm5leHQoY3J1bWJzKTtcbiAgICAgICAgLy8gdGhpcy5hcHBsaWNhdGlvblJlZi50aWNrKCk7XG4gICAgfVxufVxuIl19