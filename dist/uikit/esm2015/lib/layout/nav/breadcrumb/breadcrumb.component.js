/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ChangeDetectionStrategy, ApplicationRef, ViewChild, TemplateRef, ContentChild, ElementRef, Renderer2, NgZone, ChangeDetectorRef } from '@angular/core';
import { LayoutService } from '../../layout.service';
import { distinctUntilChanged, filter, map, flatMap, debounceTime } from 'rxjs/operators';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { Subscription, BehaviorSubject } from 'rxjs';
import { FavNavsService } from '../favnav/state/favnavs.service';
import { FavNavsQuery } from '../favnav/state/favnavs.query';
import { BreadcrumbService } from './breadcrumbs/breadcrumb.service';
import { PlatformLocation, Location } from '@angular/common';
import { ScrollDispatcher } from '@angular/cdk/scrolling';
import { PreviousRouteService } from '../../../components/previous-route.service';
export class BreadcrumbComponent {
    /**
     * @param {?} layoutService
     * @param {?} applicationRef
     * @param {?} router
     * @param {?} activatedRoute
     * @param {?} favNavsService
     * @param {?} favNavsQuery
     * @param {?} renderer
     * @param {?} zone
     * @param {?} changeDetector
     * @param {?} breadcrumbService
     * @param {?} angularLocation
     * @param {?} platformLocation
     * @param {?} scrollDispatcher
     * @param {?} previousRouteService
     */
    constructor(layoutService, applicationRef, router, activatedRoute, favNavsService, favNavsQuery, renderer, zone, changeDetector, breadcrumbService, angularLocation, platformLocation, scrollDispatcher, previousRouteService) {
        this.layoutService = layoutService;
        this.applicationRef = applicationRef;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.favNavsService = favNavsService;
        this.favNavsQuery = favNavsQuery;
        this.renderer = renderer;
        this.zone = zone;
        this.changeDetector = changeDetector;
        this.breadcrumbService = breadcrumbService;
        this.angularLocation = angularLocation;
        this.platformLocation = platformLocation;
        this.scrollDispatcher = scrollDispatcher;
        this.previousRouteService = previousRouteService;
        this.subscription = new Subscription();
        this.menuItem$ = new BehaviorSubject(null);
        this.menuItemSubscription$ = Subscription.EMPTY;
        this.isFavorited = false;
        this.isMobile$ = this.layoutService.isMobile$;
        this.navigationEndSubscription = Subscription.EMPTY;
        this.data$ = new BehaviorSubject(null);
        this.hasActionsSubject = new BehaviorSubject(false);
        this.hasActions$ = this.hasActionsSubject.asObservable();
        this.possuiRecursoDeFavoritos = this.favNavsService.possuiRecursoDeFavoritos;
        this.hiddenShell$ = this.layoutService.hiddenShell$;
        this.isFixed$ = this.layoutService.isFixed$.pipe(distinctUntilChanged());
        this.subscription.add(this.scrollDispatcher.scrolled().pipe(debounceTime(75)).subscribe((/**
         * @param {?} x
         * @return {?}
         */
        x => {
            setTimeout((/**
             * @return {?}
             */
            () => this.applicationRef.tick()));
        })));
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.desabilitarBotaoVoltar$ = this.previousRouteService.desabilitarBotaoVoltar;
        // this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe(nav => {
        //   const params = this.activatedRoute.snapshot.params;
        //   console.log('params', params);
        // });
        // this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe(nav => {
        // // this.activatedRoute.params.subscribe(params => {
        //   const params = this.activatedRoute.snapshot.params;
        //   const data = this.activatedRoute.root.firstChild.snapshot.data;
        //   if (data && data.breadcrumb) {
        //     let title: string = data.breadcrumb;
        //     const parametros = Object.keys(params);
        //     parametros.forEach(parametro => {
        //       title = title.replace(`:${parametro}`, params[parametro]);
        //     });
        //     this.breadcrumbService.changeBreadcrumb(this.activatedRoute.snapshot, data.breadcrumb);
        //   }
        // });
        //
        // this.breadcrumbService.breadcrumbChanged.subscribe(crumbs => {
        //   this.breadcrumbs = crumbs.map(crumb => ({ label: crumb.displayName, url: `#${crumb.url}` }));
        // });
        //
        this.activatedRoute.paramMap.pipe(map((/**
         * @return {?}
         */
        () => {
            /** @type {?} */
            let child = this.activatedRoute.firstChild;
            while (child) {
                if (child.firstChild) {
                    child = child.firstChild;
                }
                else if (child.snapshot.data) {
                    return child.snapshot.data;
                }
                else {
                    return null;
                }
            }
            return null;
        })))
            .subscribe((/**
         * @param {?} customData
         * @return {?}
         */
        customData => {
            /** @type {?} */
            let route = this.router.routerState.root.snapshot;
            while (route.firstChild != null) {
                route = route.firstChild;
            }
            /** @type {?} */
            const data = route.data;
            /** @type {?} */
            const favItem = this.getFavItem(data);
            // favItem.icon = customData ? customData.icon : null;
            this.data$.next(favItem);
            if (favItem) {
                this.isFavorited$ = this.favNavsQuery.isFavorited((/** @type {?} */ (favItem)));
                this.isFavorited$.subscribe((/**
                 * @param {?} r
                 * @return {?}
                 */
                r => this.isFavorited = r));
            }
        }));
        this.navigationEndSubscription = this.router.events.pipe(filter((/**
         * @param {?} event
         * @return {?}
         */
        event => event instanceof NavigationEnd)), map((/**
         * @return {?}
         */
        () => {
            /** @type {?} */
            let child = this.activatedRoute.firstChild;
            while (child) {
                if (child.firstChild) {
                    child = child.firstChild;
                }
                else if (child.snapshot.data) {
                    return child.snapshot.data;
                }
                else {
                    return null;
                }
            }
            return null;
        })))
            .subscribe((/**
         * @param {?} customData
         * @return {?}
         */
        customData => {
            /** @type {?} */
            let route = this.router.routerState.root.snapshot;
            while (route.firstChild != null) {
                route = route.firstChild;
            }
            /** @type {?} */
            const data = route.data;
            /** @type {?} */
            const favItem = this.getFavItem(data);
            // favItem.icon = customData ? customData.icon : null;
            this.data$.next(favItem);
            if (favItem) {
                this.isFavorited$ = this.favNavsQuery.isFavorited((/** @type {?} */ (favItem)));
                this.isFavorited$.subscribe((/**
                 * @param {?} r
                 * @return {?}
                 */
                r => this.isFavorited = r));
            }
        }));
        this.data$.subscribe((/**
         * @param {?} data
         * @return {?}
         */
        data => {
            this.menuItemSubscription$ = this.layoutService.menuItemsFlattened$.pipe(flatMap((/**
             * @param {?} r
             * @return {?}
             */
            r => r)), filter((/**
             * @param {?} r
             * @return {?}
             */
            r => {
                if (data == null || data.id == null) {
                    return false;
                }
                else {
                    /** @type {?} */
                    const retorno = r.id === data.id;
                    return retorno;
                }
            }))).subscribe((/**
             * @param {?} item
             * @return {?}
             */
            item => {
                this.menuItem$.next(item);
            }));
        }));
    }
    /**
     * @private
     * @param {?} customData
     * @return {?}
     */
    getFavItem(customData) {
        /** @type {?} */
        const data = customData;
        /** @type {?} */
        const lastBreadCrumb = this.breadcrumbService.breadcrumbs[this.breadcrumbService.breadcrumbs.length - 1];
        if (lastBreadCrumb != null) {
            /** @type {?} */
            const title = lastBreadCrumb.title;
            // this.breadcrumbService.getTitleFormatted(data.breadcrumb, this.activatedRoute.snapshot);
            /** @type {?} */
            const favItem = { link: this.router.url, title, icon: data ? data.icon : null };
            return favItem;
        }
    }
    /**
     * @return {?}
     */
    updateIfHasActions() {
        /** @type {?} */
        const elements = document.getElementsByClassName('uikit-actions');
        /** @type {?} */
        const element = elements.item(0);
        if (element != null) {
            /** @type {?} */
            const hasChildNodes = element.hasChildNodes();
            this.hasActionsSubject.next(hasChildNodes);
        }
        else {
            this.hasActionsSubject.next(false);
        }
    }
    /**
     * @return {?}
     */
    toggleFavoritos() {
        /** @type {?} */
        const customData = this.data$.getValue();
        /** @type {?} */
        const favItem = this.getFavItem(customData);
        this.favNavsService.toggleItem((/** @type {?} */ (favItem)));
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.navigationEndSubscription && this.navigationEndSubscription.closed == false) {
            this.navigationEndSubscription.unsubscribe();
        }
        if (this.menuItemSubscription$ && this.menuItemSubscription$.closed == false) {
            this.menuItemSubscription$.unsubscribe();
        }
        if (this.subscription && this.subscription.closed == false) {
            this.subscription.unsubscribe();
        }
    }
    /**
     * @return {?}
     */
    goBack() {
        //    if (document.referrer.indexOf(location.host) !== -1) {
        //history.go(-1);
        this.previousRouteService.removerRotaAcessada();
        this.angularLocation.back();
        //  } else {
        //      this.router.navigate([`../`], { relativeTo: this.activatedRoute });
        //}
    }
}
BreadcrumbComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-breadcrumb',
                template: "<div class=\"breadcrumb\"\n     [ngClass]=\"{'noshell': !(hiddenShell$ | async)}\"\n     *ngIf=\"(data$ | async) != null && (data$ | async).title != null\">\n\n  <button [disabled]=\"desabilitarBotaoVoltar$ | async\" type=\"button\" mat-icon-button color=\"\"  (click)=\"goBack()\">\n    <mat-icon class=\"fas fa-chevron-left\"></mat-icon>\n  </button>\n\n  <button tabindex=\"0\" *ngIf=\"possuiRecursoDeFavoritos; else naoPossuiRecursoDeFavoritos\" mat-icon-button\n    [class.btn-favorite]=\"possuiRecursoDeFavoritos\" [class.active]=\"isFavorited$ | async\"\n    (click)=\"toggleFavoritos()\">\n    <mat-icon class=\"fa-lg {{ (data$ | async)?.icon }}\"></mat-icon>\n  </button>\n\n  <ng-template #naoPossuiRecursoDeFavoritos>\n    <button tabindex=\"-1\"  mat-icon-button class=\"btn-favorite\" [class.btn-favorite]=\"possuiRecursoDeFavoritos\"\n      [class.active]=\"isFavorited$ | async\">\n      <mat-icon class=\"fa-lg {{ (data$ | async)?.icon }}\"></mat-icon>\n    </button>\n  </ng-template>\n\n  <breadcrumb #parent>\n    <ng-container\n     *ngFor=\"let route of (parent.breadcrumbs$ | async)\">\n      <a mat-button *ngIf=\"!route.terminal\" tabindex=\"1\" href=\"\" [routerLink]=\"[route.link]\">{{ route.title }}</a>\n      <a mat-button *ngIf=\"route.terminal\">{{ route.title }}</a>\n    </ng-container>\n  </breadcrumb>\n\n</div>\n",
                changeDetection: ChangeDetectionStrategy.Default,
                styles: [""]
            }] }
];
/** @nocollapse */
BreadcrumbComponent.ctorParameters = () => [
    { type: LayoutService },
    { type: ApplicationRef },
    { type: Router },
    { type: ActivatedRoute },
    { type: FavNavsService },
    { type: FavNavsQuery },
    { type: Renderer2 },
    { type: NgZone },
    { type: ChangeDetectorRef },
    { type: BreadcrumbService },
    { type: Location },
    { type: PlatformLocation },
    { type: ScrollDispatcher },
    { type: PreviousRouteService }
];
BreadcrumbComponent.propDecorators = {
    actions: [{ type: ContentChild, args: ['uikitActions',] }],
    templateActions: [{ type: ViewChild, args: ['templateActions',] }],
    templateFinder: [{ type: ViewChild, args: ['templateFinder',] }],
    templateFilters: [{ type: ViewChild, args: ['templateFilters',] }],
    templatePaginator: [{ type: ViewChild, args: ['templatePaginator',] }]
};
if (false) {
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.subscription;
    /** @type {?} */
    BreadcrumbComponent.prototype.menuItem$;
    /** @type {?} */
    BreadcrumbComponent.prototype.menuItemSubscription$;
    /** @type {?} */
    BreadcrumbComponent.prototype.isFavorited$;
    /** @type {?} */
    BreadcrumbComponent.prototype.isFavorited;
    /** @type {?} */
    BreadcrumbComponent.prototype.isMobile$;
    /** @type {?} */
    BreadcrumbComponent.prototype.navigationEndSubscription;
    /** @type {?} */
    BreadcrumbComponent.prototype.data$;
    /** @type {?} */
    BreadcrumbComponent.prototype.actions;
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.hasActionsSubject;
    /** @type {?} */
    BreadcrumbComponent.prototype.hasActions$;
    /** @type {?} */
    BreadcrumbComponent.prototype.templateActions;
    /** @type {?} */
    BreadcrumbComponent.prototype.templateFinder;
    /** @type {?} */
    BreadcrumbComponent.prototype.templateFilters;
    /** @type {?} */
    BreadcrumbComponent.prototype.templatePaginator;
    /** @type {?} */
    BreadcrumbComponent.prototype.possuiRecursoDeFavoritos;
    /** @type {?} */
    BreadcrumbComponent.prototype.hiddenShell$;
    /** @type {?} */
    BreadcrumbComponent.prototype.isFixed$;
    /** @type {?} */
    BreadcrumbComponent.prototype.desabilitarBotaoVoltar$;
    /** @type {?} */
    BreadcrumbComponent.prototype.layoutService;
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.applicationRef;
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.router;
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.activatedRoute;
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.favNavsService;
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.favNavsQuery;
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.renderer;
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.zone;
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.changeDetector;
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.breadcrumbService;
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.angularLocation;
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.platformLocation;
    /**
     * @type {?}
     * @private
     */
    BreadcrumbComponent.prototype.scrollDispatcher;
    /**
     * @type {?}
     * @protected
     */
    BreadcrumbComponent.prototype.previousRouteService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnJlYWRjcnVtYi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL2xheW91dC9uYXYvYnJlYWRjcnVtYi9icmVhZGNydW1iLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSx1QkFBdUIsRUFBRSxjQUFjLEVBQUUsU0FBUyxFQUFFLFdBQVcsRUFBK0IsWUFBWSxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFLGlCQUFpQixFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ2hOLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUNyRCxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRSxPQUFPLEVBQVksWUFBWSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDcEcsT0FBTyxFQUFFLE1BQU0sRUFBbUIsYUFBYSxFQUFTLGNBQWMsRUFBeUMsTUFBTSxpQkFBaUIsQ0FBQztBQUN2SSxPQUFPLEVBQUUsWUFBWSxFQUFFLGVBQWUsRUFBYyxNQUFNLE1BQU0sQ0FBQztBQUVqRSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0saUNBQWlDLENBQUM7QUFDakUsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBQzdELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ3JFLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxRQUFRLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUM3RCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUMxRCxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSw0Q0FBNEMsQ0FBQztBQVNsRixNQUFNLE9BQU8sbUJBQW1COzs7Ozs7Ozs7Ozs7Ozs7OztJQTJCOUIsWUFDUyxhQUE0QixFQUN6QixjQUE4QixFQUM5QixNQUFjLEVBQ2QsY0FBOEIsRUFDOUIsY0FBOEIsRUFDOUIsWUFBMEIsRUFDMUIsUUFBbUIsRUFDbkIsSUFBWSxFQUNaLGNBQWlDLEVBQ2pDLGlCQUFvQyxFQUNwQyxlQUF5QixFQUN6QixnQkFBa0MsRUFDcEMsZ0JBQWtDLEVBQ2hDLG9CQUEwQztRQWI3QyxrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUN6QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsaUJBQVksR0FBWixZQUFZLENBQWM7UUFDMUIsYUFBUSxHQUFSLFFBQVEsQ0FBVztRQUNuQixTQUFJLEdBQUosSUFBSSxDQUFRO1FBQ1osbUJBQWMsR0FBZCxjQUFjLENBQW1CO1FBQ2pDLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBbUI7UUFDcEMsb0JBQWUsR0FBZixlQUFlLENBQVU7UUFDekIscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUNwQyxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBQ2hDLHlCQUFvQixHQUFwQixvQkFBb0IsQ0FBc0I7UUF2QzVDLGlCQUFZLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUVyQyxjQUFTLEdBQUcsSUFBSSxlQUFlLENBQVcsSUFBSSxDQUFDLENBQUM7UUFDaEQsMEJBQXFCLEdBQUcsWUFBWSxDQUFDLEtBQUssQ0FBQztRQUczQyxnQkFBVyxHQUFHLEtBQUssQ0FBQztRQUNwQixjQUFTLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUM7UUFFekMsOEJBQXlCLEdBQUcsWUFBWSxDQUFDLEtBQUssQ0FBQztRQUMvQyxVQUFLLEdBQUcsSUFBSSxlQUFlLENBQU0sSUFBSSxDQUFDLENBQUM7UUFHcEMsc0JBQWlCLEdBQUcsSUFBSSxlQUFlLENBQVUsS0FBSyxDQUFDLENBQUM7UUFDM0QsZ0JBQVcsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsWUFBWSxFQUFFLENBQUM7UUFPcEQsNkJBQXdCLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyx3QkFBd0IsQ0FBQztRQUUvRSxpQkFBWSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDO1FBdUJ4QyxhQUFRLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUMsQ0FBQztRQUx6RSxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFNBQVM7Ozs7UUFBQyxDQUFDLENBQUMsRUFBRTtZQUMxRixVQUFVOzs7WUFBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxFQUFDLENBQUM7UUFDL0MsQ0FBQyxFQUFDLENBQUMsQ0FBQztJQUNOLENBQUM7Ozs7SUFNRCxRQUFRO1FBQ04sSUFBSSxDQUFDLHVCQUF1QixHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxzQkFBc0IsQ0FBQztRQUVoRiw4RkFBOEY7UUFDOUYsd0RBQXdEO1FBQ3hELG1DQUFtQztRQUNuQyxNQUFNO1FBRU4sOEZBQThGO1FBQzlGLHNEQUFzRDtRQUN0RCx3REFBd0Q7UUFDeEQsb0VBQW9FO1FBQ3BFLG1DQUFtQztRQUNuQywyQ0FBMkM7UUFDM0MsOENBQThDO1FBQzlDLHdDQUF3QztRQUN4QyxtRUFBbUU7UUFDbkUsVUFBVTtRQUNWLDhGQUE4RjtRQUM5RixNQUFNO1FBQ04sTUFBTTtRQUVOLEVBQUU7UUFDRixpRUFBaUU7UUFDakUsa0dBQWtHO1FBQ2xHLE1BQU07UUFHTixFQUFFO1FBRUYsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUc7OztRQUFDLEdBQUcsRUFBRTs7Z0JBQ3JDLEtBQUssR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFVBQVU7WUFDMUMsT0FBTyxLQUFLLEVBQUU7Z0JBQ1osSUFBSSxLQUFLLENBQUMsVUFBVSxFQUFFO29CQUNwQixLQUFLLEdBQUcsS0FBSyxDQUFDLFVBQVUsQ0FBQztpQkFDMUI7cUJBQU0sSUFBSSxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRTtvQkFDOUIsT0FBTyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQztpQkFDNUI7cUJBQU07b0JBQ0wsT0FBTyxJQUFJLENBQUM7aUJBQ2I7YUFDRjtZQUNELE9BQU8sSUFBSSxDQUFDO1FBQ2QsQ0FBQyxFQUFDLENBQUM7YUFDRixTQUFTOzs7O1FBQUMsVUFBVSxDQUFDLEVBQUU7O2dCQUNsQixLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVE7WUFDakQsT0FBTyxLQUFLLENBQUMsVUFBVSxJQUFJLElBQUksRUFBRTtnQkFDL0IsS0FBSyxHQUFHLEtBQUssQ0FBQyxVQUFVLENBQUM7YUFDMUI7O2tCQUNLLElBQUksR0FBRyxLQUFLLENBQUMsSUFBSTs7a0JBRWpCLE9BQU8sR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQztZQUNyQyxzREFBc0Q7WUFFdEQsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDekIsSUFBSSxPQUFPLEVBQUU7Z0JBQ1gsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxtQkFBQSxPQUFPLEVBQVksQ0FBQyxDQUFDO2dCQUN2RSxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVM7Ozs7Z0JBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsRUFBQyxDQUFDO2FBQ3hEO1FBQ0gsQ0FBQyxFQUFDLENBQUM7UUFFSCxJQUFJLENBQUMseUJBQXlCLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUN0RCxNQUFNOzs7O1FBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxLQUFLLFlBQVksYUFBYSxFQUFDLEVBQy9DLEdBQUc7OztRQUFDLEdBQUcsRUFBRTs7Z0JBQ0gsS0FBSyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsVUFBVTtZQUMxQyxPQUFPLEtBQUssRUFBRTtnQkFDWixJQUFJLEtBQUssQ0FBQyxVQUFVLEVBQUU7b0JBQ3BCLEtBQUssR0FBRyxLQUFLLENBQUMsVUFBVSxDQUFDO2lCQUMxQjtxQkFBTSxJQUFJLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFO29CQUM5QixPQUFPLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDO2lCQUM1QjtxQkFBTTtvQkFDTCxPQUFPLElBQUksQ0FBQztpQkFDYjthQUNGO1lBQ0QsT0FBTyxJQUFJLENBQUM7UUFDZCxDQUFDLEVBQUMsQ0FBQzthQUNGLFNBQVM7Ozs7UUFBQyxVQUFVLENBQUMsRUFBRTs7Z0JBQ2xCLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUTtZQUNqRCxPQUFPLEtBQUssQ0FBQyxVQUFVLElBQUksSUFBSSxFQUFFO2dCQUMvQixLQUFLLEdBQUcsS0FBSyxDQUFDLFVBQVUsQ0FBQzthQUMxQjs7a0JBQ0ssSUFBSSxHQUFHLEtBQUssQ0FBQyxJQUFJOztrQkFJakIsT0FBTyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDO1lBQ3JDLHNEQUFzRDtZQUN0RCxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUd6QixJQUFJLE9BQU8sRUFBRTtnQkFDWCxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLG1CQUFBLE9BQU8sRUFBWSxDQUFDLENBQUM7Z0JBQ3ZFLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUzs7OztnQkFBQyxDQUFDLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsQ0FBQyxFQUFDLENBQUM7YUFDeEQ7UUFDSCxDQUFDLEVBQUMsQ0FBQztRQU1MLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUzs7OztRQUFDLElBQUksQ0FBQyxFQUFFO1lBRTFCLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FDdEUsT0FBTzs7OztZQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFDLEVBQ2YsTUFBTTs7OztZQUFDLENBQUMsQ0FBQyxFQUFFO2dCQUNULElBQUksSUFBSSxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsRUFBRSxJQUFJLElBQUksRUFBRTtvQkFDbkMsT0FBTyxLQUFLLENBQUM7aUJBQ2Q7cUJBQU07OzBCQUNDLE9BQU8sR0FBRyxDQUFDLENBQUMsRUFBRSxLQUFLLElBQUksQ0FBQyxFQUFFO29CQUNoQyxPQUFPLE9BQU8sQ0FBQztpQkFDaEI7WUFDSCxDQUFDLEVBQUMsQ0FDSCxDQUFDLFNBQVM7Ozs7WUFBQyxJQUFJLENBQUMsRUFBRTtnQkFDakIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFJNUIsQ0FBQyxFQUFDLENBQUM7UUFDTCxDQUFDLEVBQUMsQ0FBQztJQUVMLENBQUM7Ozs7OztJQUNPLFVBQVUsQ0FBQyxVQUFVOztjQUNyQixJQUFJLEdBQUcsVUFBVTs7Y0FDakIsY0FBYyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFdBQVcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO1FBQ3hHLElBQUksY0FBYyxJQUFJLElBQUksRUFBRTs7a0JBQ3BCLEtBQUssR0FBRyxjQUFjLENBQUMsS0FBSzs7O2tCQUM1QixPQUFPLEdBQUcsRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRTtZQUMvRSxPQUFPLE9BQU8sQ0FBQztTQUNoQjtJQUNILENBQUM7Ozs7SUFFRCxrQkFBa0I7O2NBQ1YsUUFBUSxHQUFHLFFBQVEsQ0FBQyxzQkFBc0IsQ0FBQyxlQUFlLENBQUM7O2NBQzNELE9BQU8sR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztRQUNoQyxJQUFJLE9BQU8sSUFBSSxJQUFJLEVBQUU7O2tCQUNiLGFBQWEsR0FBRyxPQUFPLENBQUMsYUFBYSxFQUFFO1lBQzdDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7U0FDNUM7YUFBTTtZQUNMLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDcEM7SUFDSCxDQUFDOzs7O0lBS0QsZUFBZTs7Y0FDUCxVQUFVLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUU7O2NBQ2xDLE9BQU8sR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQztRQUMzQyxJQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxtQkFBQSxPQUFPLEVBQVksQ0FBQyxDQUFDO0lBQ3RELENBQUM7Ozs7SUFFRCxXQUFXO1FBQ1QsSUFBSSxJQUFJLENBQUMseUJBQXlCLElBQUksSUFBSSxDQUFDLHlCQUF5QixDQUFDLE1BQU0sSUFBSSxLQUFLLEVBQUU7WUFDcEYsSUFBSSxDQUFDLHlCQUF5QixDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQzlDO1FBRUQsSUFBSSxJQUFJLENBQUMscUJBQXFCLElBQUksSUFBSSxDQUFDLHFCQUFxQixDQUFDLE1BQU0sSUFBSSxLQUFLLEVBQUU7WUFDNUUsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQzFDO1FBRUQsSUFBSSxJQUFJLENBQUMsWUFBWSxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxJQUFJLEtBQUssRUFBRTtZQUMxRCxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQ2pDO0lBRUgsQ0FBQzs7OztJQUVNLE1BQU07UUFDWCw0REFBNEQ7UUFDdEQsaUJBQWlCO1FBQ3ZCLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO1FBQ2hELElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDMUIsWUFBWTtRQUNkLDJFQUEyRTtRQUN2RSxHQUFHO0lBQ0wsQ0FBQzs7O1lBdk9OLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsa0JBQWtCO2dCQUM1QixpMUNBQTBDO2dCQUUxQyxlQUFlLEVBQUUsdUJBQXVCLENBQUMsT0FBTzs7YUFDakQ7Ozs7WUFsQlEsYUFBYTtZQUQrQixjQUFjO1lBRzFELE1BQU07WUFBeUMsY0FBYztZQUc3RCxjQUFjO1lBQ2QsWUFBWTtZQVArSCxTQUFTO1lBQUUsTUFBTTtZQUFFLGlCQUFpQjtZQVEvSyxpQkFBaUI7WUFDQyxRQUFRO1lBQTFCLGdCQUFnQjtZQUNoQixnQkFBZ0I7WUFDaEIsb0JBQW9COzs7c0JBdUIxQixZQUFZLFNBQUMsY0FBYzs4QkFJM0IsU0FBUyxTQUFDLGlCQUFpQjs2QkFDM0IsU0FBUyxTQUFDLGdCQUFnQjs4QkFDMUIsU0FBUyxTQUFDLGlCQUFpQjtnQ0FDM0IsU0FBUyxTQUFDLG1CQUFtQjs7Ozs7OztJQW5COUIsMkNBQTRDOztJQUU1Qyx3Q0FBdUQ7O0lBQ3ZELG9EQUFrRDs7SUFFbEQsMkNBQXlDOztJQUN6QywwQ0FBMkI7O0lBQzNCLHdDQUFnRDs7SUFFaEQsd0RBQXNEOztJQUN0RCxvQ0FBOEM7O0lBRTlDLHNDQUFrRDs7Ozs7SUFDbEQsZ0RBQWtFOztJQUNsRSwwQ0FBMkQ7O0lBRTNELDhDQUFnRTs7SUFDaEUsNkNBQThEOztJQUM5RCw4Q0FBZ0U7O0lBQ2hFLGdEQUFvRTs7SUFFcEUsdURBQStFOztJQUUvRSwyQ0FBK0M7O0lBdUIvQyx1Q0FBMkU7O0lBRTNFLHNEQUFvRDs7SUF0QmxELDRDQUFtQzs7Ozs7SUFDbkMsNkNBQXdDOzs7OztJQUN4QyxxQ0FBd0I7Ozs7O0lBQ3hCLDZDQUF3Qzs7Ozs7SUFDeEMsNkNBQXdDOzs7OztJQUN4QywyQ0FBb0M7Ozs7O0lBQ3BDLHVDQUE2Qjs7Ozs7SUFDN0IsbUNBQXNCOzs7OztJQUN0Qiw2Q0FBMkM7Ozs7O0lBQzNDLGdEQUE4Qzs7Ozs7SUFDOUMsOENBQW1DOzs7OztJQUNuQywrQ0FBNEM7Ozs7O0lBQzVDLCtDQUEwQzs7Ozs7SUFDMUMsbURBQW9EIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIENoYW5nZURldGVjdGlvblN0cmF0ZWd5LCBBcHBsaWNhdGlvblJlZiwgVmlld0NoaWxkLCBUZW1wbGF0ZVJlZiwgT25EZXN0cm95LCBBZnRlckNvbnRlbnRJbml0LCBDb250ZW50Q2hpbGQsIEVsZW1lbnRSZWYsIFJlbmRlcmVyMiwgTmdab25lLCBDaGFuZ2VEZXRlY3RvclJlZiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTGF5b3V0U2VydmljZSB9IGZyb20gJy4uLy4uL2xheW91dC5zZXJ2aWNlJztcbmltcG9ydCB7IGRpc3RpbmN0VW50aWxDaGFuZ2VkLCBmaWx0ZXIsIG1hcCwgZmxhdE1hcCwgZGVib3VuY2UsIGRlYm91bmNlVGltZSB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcbmltcG9ydCB7IFJvdXRlciwgTmF2aWdhdGlvblN0YXJ0LCBOYXZpZ2F0aW9uRW5kLCBFdmVudCwgQWN0aXZhdGVkUm91dGUsIFJvdXRlc1JlY29nbml6ZWQsIFJvdXRlclN0YXRlU25hcHNob3QgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHsgU3Vic2NyaXB0aW9uLCBCZWhhdmlvclN1YmplY3QsIE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IE1lbnVJdGVtIH0gZnJvbSAnLi4vbWVudS9tZW51LWl0ZW0vbWVudS1pdGVtLm1vZGVsJztcbmltcG9ydCB7IEZhdk5hdnNTZXJ2aWNlIH0gZnJvbSAnLi4vZmF2bmF2L3N0YXRlL2Zhdm5hdnMuc2VydmljZSc7XG5pbXBvcnQgeyBGYXZOYXZzUXVlcnkgfSBmcm9tICcuLi9mYXZuYXYvc3RhdGUvZmF2bmF2cy5xdWVyeSc7XG5pbXBvcnQgeyBCcmVhZGNydW1iU2VydmljZSB9IGZyb20gJy4vYnJlYWRjcnVtYnMvYnJlYWRjcnVtYi5zZXJ2aWNlJztcbmltcG9ydCB7IFBsYXRmb3JtTG9jYXRpb24sIExvY2F0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7IFNjcm9sbERpc3BhdGNoZXIgfSBmcm9tICdAYW5ndWxhci9jZGsvc2Nyb2xsaW5nJztcbmltcG9ydCB7IFByZXZpb3VzUm91dGVTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vY29tcG9uZW50cy9wcmV2aW91cy1yb3V0ZS5zZXJ2aWNlJztcblxuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICd1aWtpdC1icmVhZGNydW1iJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2JyZWFkY3J1bWIuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9icmVhZGNydW1iLmNvbXBvbmVudC5zY3NzJ10sXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuRGVmYXVsdFxufSlcbmV4cG9ydCBjbGFzcyBCcmVhZGNydW1iQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xuXG4gIHByb3RlY3RlZCBzdWJzY3JpcHRpb24gPSBuZXcgU3Vic2NyaXB0aW9uKCk7XG5cbiAgcHVibGljIG1lbnVJdGVtJCA9IG5ldyBCZWhhdmlvclN1YmplY3Q8TWVudUl0ZW0+KG51bGwpO1xuICBwdWJsaWMgbWVudUl0ZW1TdWJzY3JpcHRpb24kID0gU3Vic2NyaXB0aW9uLkVNUFRZO1xuXG4gIHB1YmxpYyBpc0Zhdm9yaXRlZCQ6IE9ic2VydmFibGU8Ym9vbGVhbj47XG4gIHB1YmxpYyBpc0Zhdm9yaXRlZCA9IGZhbHNlO1xuICBwdWJsaWMgaXNNb2JpbGUkID0gdGhpcy5sYXlvdXRTZXJ2aWNlLmlzTW9iaWxlJDtcblxuICBwdWJsaWMgbmF2aWdhdGlvbkVuZFN1YnNjcmlwdGlvbiA9IFN1YnNjcmlwdGlvbi5FTVBUWTtcbiAgcHVibGljIGRhdGEkID0gbmV3IEJlaGF2aW9yU3ViamVjdDxhbnk+KG51bGwpO1xuXG4gIEBDb250ZW50Q2hpbGQoJ3Vpa2l0QWN0aW9ucycpIGFjdGlvbnM6IEVsZW1lbnRSZWY7XG4gIHByb3RlY3RlZCBoYXNBY3Rpb25zU3ViamVjdCA9IG5ldyBCZWhhdmlvclN1YmplY3Q8Ym9vbGVhbj4oZmFsc2UpO1xuICBwdWJsaWMgaGFzQWN0aW9ucyQgPSB0aGlzLmhhc0FjdGlvbnNTdWJqZWN0LmFzT2JzZXJ2YWJsZSgpO1xuXG4gIEBWaWV3Q2hpbGQoJ3RlbXBsYXRlQWN0aW9ucycpIHRlbXBsYXRlQWN0aW9uczogVGVtcGxhdGVSZWY8YW55PjtcbiAgQFZpZXdDaGlsZCgndGVtcGxhdGVGaW5kZXInKSB0ZW1wbGF0ZUZpbmRlcjogVGVtcGxhdGVSZWY8YW55PjtcbiAgQFZpZXdDaGlsZCgndGVtcGxhdGVGaWx0ZXJzJykgdGVtcGxhdGVGaWx0ZXJzOiBUZW1wbGF0ZVJlZjxhbnk+O1xuICBAVmlld0NoaWxkKCd0ZW1wbGF0ZVBhZ2luYXRvcicpIHRlbXBsYXRlUGFnaW5hdG9yOiBUZW1wbGF0ZVJlZjxhbnk+O1xuXG4gIHB1YmxpYyBwb3NzdWlSZWN1cnNvRGVGYXZvcml0b3MgPSB0aGlzLmZhdk5hdnNTZXJ2aWNlLnBvc3N1aVJlY3Vyc29EZUZhdm9yaXRvcztcblxuICBoaWRkZW5TaGVsbCQgPSB0aGlzLmxheW91dFNlcnZpY2UuaGlkZGVuU2hlbGwkO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHB1YmxpYyBsYXlvdXRTZXJ2aWNlOiBMYXlvdXRTZXJ2aWNlLFxuICAgIHByb3RlY3RlZCBhcHBsaWNhdGlvblJlZjogQXBwbGljYXRpb25SZWYsXG4gICAgcHJvdGVjdGVkIHJvdXRlcjogUm91dGVyLFxuICAgIHByb3RlY3RlZCBhY3RpdmF0ZWRSb3V0ZTogQWN0aXZhdGVkUm91dGUsXG4gICAgcHJvdGVjdGVkIGZhdk5hdnNTZXJ2aWNlOiBGYXZOYXZzU2VydmljZSxcbiAgICBwcm90ZWN0ZWQgZmF2TmF2c1F1ZXJ5OiBGYXZOYXZzUXVlcnksXG4gICAgcHJvdGVjdGVkIHJlbmRlcmVyOiBSZW5kZXJlcjIsXG4gICAgcHJvdGVjdGVkIHpvbmU6IE5nWm9uZSxcbiAgICBwcm90ZWN0ZWQgY2hhbmdlRGV0ZWN0b3I6IENoYW5nZURldGVjdG9yUmVmLFxuICAgIHByb3RlY3RlZCBicmVhZGNydW1iU2VydmljZTogQnJlYWRjcnVtYlNlcnZpY2UsXG4gICAgcHJvdGVjdGVkIGFuZ3VsYXJMb2NhdGlvbjogTG9jYXRpb24sXG4gICAgcHJvdGVjdGVkIHBsYXRmb3JtTG9jYXRpb246IFBsYXRmb3JtTG9jYXRpb24sXG4gICAgcHJpdmF0ZSBzY3JvbGxEaXNwYXRjaGVyOiBTY3JvbGxEaXNwYXRjaGVyLFxuICAgIHByb3RlY3RlZCBwcmV2aW91c1JvdXRlU2VydmljZTogUHJldmlvdXNSb3V0ZVNlcnZpY2UsXG4gICkge1xuICAgIHRoaXMuc3Vic2NyaXB0aW9uLmFkZCh0aGlzLnNjcm9sbERpc3BhdGNoZXIuc2Nyb2xsZWQoKS5waXBlKGRlYm91bmNlVGltZSg3NSkpLnN1YnNjcmliZSh4ID0+IHtcbiAgICAgIHNldFRpbWVvdXQoKCkgPT4gdGhpcy5hcHBsaWNhdGlvblJlZi50aWNrKCkpO1xuICAgIH0pKTtcbiAgfVxuXG4gIHB1YmxpYyBpc0ZpeGVkJCA9IHRoaXMubGF5b3V0U2VydmljZS5pc0ZpeGVkJC5waXBlKGRpc3RpbmN0VW50aWxDaGFuZ2VkKCkpO1xuXG4gIHB1YmxpYyBkZXNhYmlsaXRhckJvdGFvVm9sdGFyJDogT2JzZXJ2YWJsZTxib29sZWFuPjtcblxuICBuZ09uSW5pdCgpIHtcbiAgICB0aGlzLmRlc2FiaWxpdGFyQm90YW9Wb2x0YXIkID0gdGhpcy5wcmV2aW91c1JvdXRlU2VydmljZS5kZXNhYmlsaXRhckJvdGFvVm9sdGFyO1xuXG4gICAgLy8gdGhpcy5yb3V0ZXIuZXZlbnRzLnBpcGUoZmlsdGVyKGV2ZW50ID0+IGV2ZW50IGluc3RhbmNlb2YgTmF2aWdhdGlvbkVuZCkpLnN1YnNjcmliZShuYXYgPT4ge1xuICAgIC8vICAgY29uc3QgcGFyYW1zID0gdGhpcy5hY3RpdmF0ZWRSb3V0ZS5zbmFwc2hvdC5wYXJhbXM7XG4gICAgLy8gICBjb25zb2xlLmxvZygncGFyYW1zJywgcGFyYW1zKTtcbiAgICAvLyB9KTtcblxuICAgIC8vIHRoaXMucm91dGVyLmV2ZW50cy5waXBlKGZpbHRlcihldmVudCA9PiBldmVudCBpbnN0YW5jZW9mIE5hdmlnYXRpb25FbmQpKS5zdWJzY3JpYmUobmF2ID0+IHtcbiAgICAvLyAvLyB0aGlzLmFjdGl2YXRlZFJvdXRlLnBhcmFtcy5zdWJzY3JpYmUocGFyYW1zID0+IHtcbiAgICAvLyAgIGNvbnN0IHBhcmFtcyA9IHRoaXMuYWN0aXZhdGVkUm91dGUuc25hcHNob3QucGFyYW1zO1xuICAgIC8vICAgY29uc3QgZGF0YSA9IHRoaXMuYWN0aXZhdGVkUm91dGUucm9vdC5maXJzdENoaWxkLnNuYXBzaG90LmRhdGE7XG4gICAgLy8gICBpZiAoZGF0YSAmJiBkYXRhLmJyZWFkY3J1bWIpIHtcbiAgICAvLyAgICAgbGV0IHRpdGxlOiBzdHJpbmcgPSBkYXRhLmJyZWFkY3J1bWI7XG4gICAgLy8gICAgIGNvbnN0IHBhcmFtZXRyb3MgPSBPYmplY3Qua2V5cyhwYXJhbXMpO1xuICAgIC8vICAgICBwYXJhbWV0cm9zLmZvckVhY2gocGFyYW1ldHJvID0+IHtcbiAgICAvLyAgICAgICB0aXRsZSA9IHRpdGxlLnJlcGxhY2UoYDoke3BhcmFtZXRyb31gLCBwYXJhbXNbcGFyYW1ldHJvXSk7XG4gICAgLy8gICAgIH0pO1xuICAgIC8vICAgICB0aGlzLmJyZWFkY3J1bWJTZXJ2aWNlLmNoYW5nZUJyZWFkY3J1bWIodGhpcy5hY3RpdmF0ZWRSb3V0ZS5zbmFwc2hvdCwgZGF0YS5icmVhZGNydW1iKTtcbiAgICAvLyAgIH1cbiAgICAvLyB9KTtcblxuICAgIC8vXG4gICAgLy8gdGhpcy5icmVhZGNydW1iU2VydmljZS5icmVhZGNydW1iQ2hhbmdlZC5zdWJzY3JpYmUoY3J1bWJzID0+IHtcbiAgICAvLyAgIHRoaXMuYnJlYWRjcnVtYnMgPSBjcnVtYnMubWFwKGNydW1iID0+ICh7IGxhYmVsOiBjcnVtYi5kaXNwbGF5TmFtZSwgdXJsOiBgIyR7Y3J1bWIudXJsfWAgfSkpO1xuICAgIC8vIH0pO1xuXG5cbiAgICAvL1xuXG4gICAgdGhpcy5hY3RpdmF0ZWRSb3V0ZS5wYXJhbU1hcC5waXBlKG1hcCgoKSA9PiB7XG4gICAgICBsZXQgY2hpbGQgPSB0aGlzLmFjdGl2YXRlZFJvdXRlLmZpcnN0Q2hpbGQ7XG4gICAgICB3aGlsZSAoY2hpbGQpIHtcbiAgICAgICAgaWYgKGNoaWxkLmZpcnN0Q2hpbGQpIHtcbiAgICAgICAgICBjaGlsZCA9IGNoaWxkLmZpcnN0Q2hpbGQ7XG4gICAgICAgIH0gZWxzZSBpZiAoY2hpbGQuc25hcHNob3QuZGF0YSkge1xuICAgICAgICAgIHJldHVybiBjaGlsZC5zbmFwc2hvdC5kYXRhO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICByZXR1cm4gbnVsbDtcbiAgICB9KSlcbiAgICAuc3Vic2NyaWJlKGN1c3RvbURhdGEgPT4ge1xuICAgICAgbGV0IHJvdXRlID0gdGhpcy5yb3V0ZXIucm91dGVyU3RhdGUucm9vdC5zbmFwc2hvdDtcbiAgICAgIHdoaWxlIChyb3V0ZS5maXJzdENoaWxkICE9IG51bGwpIHtcbiAgICAgICAgcm91dGUgPSByb3V0ZS5maXJzdENoaWxkO1xuICAgICAgfVxuICAgICAgY29uc3QgZGF0YSA9IHJvdXRlLmRhdGE7XG5cbiAgICAgIGNvbnN0IGZhdkl0ZW0gPSB0aGlzLmdldEZhdkl0ZW0oZGF0YSk7XG4gICAgICAvLyBmYXZJdGVtLmljb24gPSBjdXN0b21EYXRhID8gY3VzdG9tRGF0YS5pY29uIDogbnVsbDtcblxuICAgICAgdGhpcy5kYXRhJC5uZXh0KGZhdkl0ZW0pO1xuICAgICAgaWYgKGZhdkl0ZW0pIHtcbiAgICAgICAgdGhpcy5pc0Zhdm9yaXRlZCQgPSB0aGlzLmZhdk5hdnNRdWVyeS5pc0Zhdm9yaXRlZChmYXZJdGVtIGFzIE1lbnVJdGVtKTtcbiAgICAgICAgdGhpcy5pc0Zhdm9yaXRlZCQuc3Vic2NyaWJlKHIgPT4gdGhpcy5pc0Zhdm9yaXRlZCA9IHIpO1xuICAgICAgfVxuICAgIH0pO1xuXG4gICAgdGhpcy5uYXZpZ2F0aW9uRW5kU3Vic2NyaXB0aW9uID0gdGhpcy5yb3V0ZXIuZXZlbnRzLnBpcGUoXG4gICAgICBmaWx0ZXIoZXZlbnQgPT4gZXZlbnQgaW5zdGFuY2VvZiBOYXZpZ2F0aW9uRW5kKSxcbiAgICAgIG1hcCgoKSA9PiB7XG4gICAgICAgIGxldCBjaGlsZCA9IHRoaXMuYWN0aXZhdGVkUm91dGUuZmlyc3RDaGlsZDtcbiAgICAgICAgd2hpbGUgKGNoaWxkKSB7XG4gICAgICAgICAgaWYgKGNoaWxkLmZpcnN0Q2hpbGQpIHtcbiAgICAgICAgICAgIGNoaWxkID0gY2hpbGQuZmlyc3RDaGlsZDtcbiAgICAgICAgICB9IGVsc2UgaWYgKGNoaWxkLnNuYXBzaG90LmRhdGEpIHtcbiAgICAgICAgICAgIHJldHVybiBjaGlsZC5zbmFwc2hvdC5kYXRhO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICB9KSlcbiAgICAgIC5zdWJzY3JpYmUoY3VzdG9tRGF0YSA9PiB7XG4gICAgICAgIGxldCByb3V0ZSA9IHRoaXMucm91dGVyLnJvdXRlclN0YXRlLnJvb3Quc25hcHNob3Q7XG4gICAgICAgIHdoaWxlIChyb3V0ZS5maXJzdENoaWxkICE9IG51bGwpIHtcbiAgICAgICAgICByb3V0ZSA9IHJvdXRlLmZpcnN0Q2hpbGQ7XG4gICAgICAgIH1cbiAgICAgICAgY29uc3QgZGF0YSA9IHJvdXRlLmRhdGE7XG5cblxuXG4gICAgICAgIGNvbnN0IGZhdkl0ZW0gPSB0aGlzLmdldEZhdkl0ZW0oZGF0YSk7XG4gICAgICAgIC8vIGZhdkl0ZW0uaWNvbiA9IGN1c3RvbURhdGEgPyBjdXN0b21EYXRhLmljb24gOiBudWxsO1xuICAgICAgICB0aGlzLmRhdGEkLm5leHQoZmF2SXRlbSk7XG5cblxuICAgICAgICBpZiAoZmF2SXRlbSkge1xuICAgICAgICAgIHRoaXMuaXNGYXZvcml0ZWQkID0gdGhpcy5mYXZOYXZzUXVlcnkuaXNGYXZvcml0ZWQoZmF2SXRlbSBhcyBNZW51SXRlbSk7XG4gICAgICAgICAgdGhpcy5pc0Zhdm9yaXRlZCQuc3Vic2NyaWJlKHIgPT4gdGhpcy5pc0Zhdm9yaXRlZCA9IHIpO1xuICAgICAgICB9XG4gICAgICB9KTtcblxuXG5cblxuXG4gICAgdGhpcy5kYXRhJC5zdWJzY3JpYmUoZGF0YSA9PiB7XG5cbiAgICAgIHRoaXMubWVudUl0ZW1TdWJzY3JpcHRpb24kID0gdGhpcy5sYXlvdXRTZXJ2aWNlLm1lbnVJdGVtc0ZsYXR0ZW5lZCQucGlwZShcbiAgICAgICAgZmxhdE1hcChyID0+IHIpLFxuICAgICAgICBmaWx0ZXIociA9PiB7XG4gICAgICAgICAgaWYgKGRhdGEgPT0gbnVsbCB8fCBkYXRhLmlkID09IG51bGwpIHtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgY29uc3QgcmV0b3JubyA9IHIuaWQgPT09IGRhdGEuaWQ7XG4gICAgICAgICAgICByZXR1cm4gcmV0b3JubztcbiAgICAgICAgICB9XG4gICAgICAgIH0pXG4gICAgICApLnN1YnNjcmliZShpdGVtID0+IHtcbiAgICAgICAgdGhpcy5tZW51SXRlbSQubmV4dChpdGVtKTtcblxuXG5cbiAgICAgIH0pO1xuICAgIH0pO1xuXG4gIH1cbiAgcHJpdmF0ZSBnZXRGYXZJdGVtKGN1c3RvbURhdGEpIHtcbiAgICBjb25zdCBkYXRhID0gY3VzdG9tRGF0YTtcbiAgICBjb25zdCBsYXN0QnJlYWRDcnVtYiA9IHRoaXMuYnJlYWRjcnVtYlNlcnZpY2UuYnJlYWRjcnVtYnNbdGhpcy5icmVhZGNydW1iU2VydmljZS5icmVhZGNydW1icy5sZW5ndGggLSAxXTtcbiAgICBpZiAobGFzdEJyZWFkQ3J1bWIgIT0gbnVsbCkge1xuICAgICAgY29uc3QgdGl0bGUgPSBsYXN0QnJlYWRDcnVtYi50aXRsZTsgLy8gdGhpcy5icmVhZGNydW1iU2VydmljZS5nZXRUaXRsZUZvcm1hdHRlZChkYXRhLmJyZWFkY3J1bWIsIHRoaXMuYWN0aXZhdGVkUm91dGUuc25hcHNob3QpO1xuICAgICAgY29uc3QgZmF2SXRlbSA9IHsgbGluazogdGhpcy5yb3V0ZXIudXJsLCB0aXRsZSwgaWNvbjogZGF0YSA/IGRhdGEuaWNvbiA6IG51bGwgfTtcbiAgICAgIHJldHVybiBmYXZJdGVtO1xuICAgIH1cbiAgfVxuXG4gIHVwZGF0ZUlmSGFzQWN0aW9ucygpIHtcbiAgICBjb25zdCBlbGVtZW50cyA9IGRvY3VtZW50LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ3Vpa2l0LWFjdGlvbnMnKTtcbiAgICBjb25zdCBlbGVtZW50ID0gZWxlbWVudHMuaXRlbSgwKTtcbiAgICBpZiAoZWxlbWVudCAhPSBudWxsKSB7XG4gICAgICBjb25zdCBoYXNDaGlsZE5vZGVzID0gZWxlbWVudC5oYXNDaGlsZE5vZGVzKCk7XG4gICAgICB0aGlzLmhhc0FjdGlvbnNTdWJqZWN0Lm5leHQoaGFzQ2hpbGROb2Rlcyk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuaGFzQWN0aW9uc1N1YmplY3QubmV4dChmYWxzZSk7XG4gICAgfVxuICB9XG5cblxuXG5cbiAgdG9nZ2xlRmF2b3JpdG9zKCkge1xuICAgIGNvbnN0IGN1c3RvbURhdGEgPSB0aGlzLmRhdGEkLmdldFZhbHVlKCk7XG4gICAgY29uc3QgZmF2SXRlbSA9IHRoaXMuZ2V0RmF2SXRlbShjdXN0b21EYXRhKTtcbiAgICB0aGlzLmZhdk5hdnNTZXJ2aWNlLnRvZ2dsZUl0ZW0oZmF2SXRlbSBhcyBNZW51SXRlbSk7XG4gIH1cblxuICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcbiAgICBpZiAodGhpcy5uYXZpZ2F0aW9uRW5kU3Vic2NyaXB0aW9uICYmIHRoaXMubmF2aWdhdGlvbkVuZFN1YnNjcmlwdGlvbi5jbG9zZWQgPT0gZmFsc2UpIHtcbiAgICAgIHRoaXMubmF2aWdhdGlvbkVuZFN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xuICAgIH1cblxuICAgIGlmICh0aGlzLm1lbnVJdGVtU3Vic2NyaXB0aW9uJCAmJiB0aGlzLm1lbnVJdGVtU3Vic2NyaXB0aW9uJC5jbG9zZWQgPT0gZmFsc2UpIHtcbiAgICAgIHRoaXMubWVudUl0ZW1TdWJzY3JpcHRpb24kLnVuc3Vic2NyaWJlKCk7XG4gICAgfVxuXG4gICAgaWYgKHRoaXMuc3Vic2NyaXB0aW9uICYmIHRoaXMuc3Vic2NyaXB0aW9uLmNsb3NlZCA9PSBmYWxzZSkge1xuICAgICAgdGhpcy5zdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcbiAgICB9XG5cbiAgfVxuXG4gIHB1YmxpYyBnb0JhY2soKSB7XG4gICAgLy8gICAgaWYgKGRvY3VtZW50LnJlZmVycmVyLmluZGV4T2YobG9jYXRpb24uaG9zdCkgIT09IC0xKSB7XG4gICAgICAgICAgLy9oaXN0b3J5LmdvKC0xKTtcbiAgICB0aGlzLnByZXZpb3VzUm91dGVTZXJ2aWNlLnJlbW92ZXJSb3RhQWNlc3NhZGEoKTtcbiAgICB0aGlzLmFuZ3VsYXJMb2NhdGlvbi5iYWNrKCk7XG4gICAgICAvLyAgfSBlbHNlIHtcbiAgICAvLyAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFtgLi4vYF0sIHsgcmVsYXRpdmVUbzogdGhpcy5hY3RpdmF0ZWRSb3V0ZSB9KTtcbiAgICAgICAgLy99XG4gICAgICB9XG59XG4iXX0=