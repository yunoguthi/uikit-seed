/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutComponent } from './layout.component';
import { HeaderComponent } from './header/header.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatBadgeModule } from '@angular/material/badge';
import { MatTooltipModule } from '@angular/material/tooltip';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatListModule } from '@angular/material/list';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { NavComponent } from './nav/nav.component';
import { MenuComponent } from './nav/menu/menu.component';
import { FavnavComponent } from './nav/favnav/favnav.component';
import { BreadcrumbComponent } from './nav/breadcrumb/breadcrumb.component';
import { SearchComponent } from './header/search/search.component';
import { NotificationComponent } from './header/notification/notification.component';
import { SysteminfoComponent } from './header/systeminfo/systeminfo.component';
import { AccessibilityComponent } from './header/accessibility/accessibility.component';
import { UserinfoComponent } from './header/userinfo/userinfo.component';
import { MatChipsModule } from '@angular/material/chips';
import { MatSliderModule } from '@angular/material/slider';
import { LayoutService } from './layout.service';
import { FavNavsService } from './nav/favnav/state/favnavs.service';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { CdkStepperModule } from '@angular/cdk/stepper';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatTabsModule } from '@angular/material/tabs';
import { MatSelectModule } from '@angular/material/select';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MenuItemComponent } from './nav/menu/menu-item/menu-item.component';
import { RouterModule } from '@angular/router';
import { HotkeyModule } from 'angular2-hotkeys';
import { MatSnackBarModule, MatTableModule, MatTreeModule } from '@angular/material';
import { NotificacaoComponent } from './header/notification/notificacao/notificacao.component';
import { SimpleDialogComponent } from '../components/simple-dialog/simple-dialog.component';
import { DialogService } from '../components/simple-dialog/dialog.service';
import { IaComponent } from './ia/ia.component';
import { BreadcrumbModule } from './nav/breadcrumb/breadcrumbs/breadcrumb.module';
import { UikitSharedModule } from '../shared/shared.module';
import { ToastModule } from './toast/toast.module';
import { PreviousRouteService } from '../components/previous-route.service';
import { HighlightComponent } from './nav/highlight/highlight.component';
import { MenuSearchComponent } from "./nav/menu-search/menu-search.component";
import { UikitRrippleService } from "../utils/uikit-ripple.service";
export class LayoutModule {
    /**
     * @param {?} previousRouteService
     */
    constructor(previousRouteService) {
        this.previousRouteService = previousRouteService;
    }
}
LayoutModule.decorators = [
    { type: NgModule, args: [{
                providers: [
                    LayoutService,
                    FavNavsService,
                    DialogService,
                    PreviousRouteService,
                    UikitRrippleService
                ],
                declarations: [
                    LayoutComponent,
                    HeaderComponent,
                    NavComponent,
                    MenuComponent,
                    FavnavComponent,
                    SearchComponent,
                    HighlightComponent,
                    NotificationComponent,
                    SysteminfoComponent,
                    AccessibilityComponent,
                    UserinfoComponent,
                    BreadcrumbComponent,
                    MenuItemComponent,
                    NotificacaoComponent,
                    SimpleDialogComponent,
                    BreadcrumbComponent,
                    MenuSearchComponent,
                    IaComponent
                ],
                imports: [
                    CommonModule,
                    RouterModule,
                    FormsModule,
                    ReactiveFormsModule,
                    HotkeyModule.forRoot(),
                    MatSidenavModule,
                    MatSnackBarModule,
                    MatToolbarModule,
                    MatButtonModule,
                    MatMenuModule,
                    MatIconModule,
                    MatBadgeModule,
                    MatTooltipModule,
                    DragDropModule,
                    MatExpansionModule,
                    MatListModule,
                    MatAutocompleteModule,
                    MatFormFieldModule,
                    MatInputModule,
                    ScrollingModule,
                    MatChipsModule,
                    MatSliderModule,
                    CdkStepperModule,
                    MatSlideToggleModule,
                    MatTabsModule,
                    MatSelectModule,
                    MatPaginatorModule,
                    MatTableModule,
                    MatTreeModule,
                    BreadcrumbModule,
                    UikitSharedModule,
                    ToastModule,
                ],
                exports: [
                    MatIconModule,
                    MatButtonModule,
                    MatSnackBarModule,
                    LayoutComponent,
                    HeaderComponent,
                    NavComponent,
                    MenuComponent,
                    FavnavComponent,
                    MatListModule,
                    SearchComponent,
                    NotificationComponent,
                    SysteminfoComponent,
                    AccessibilityComponent,
                    UserinfoComponent,
                    MenuItemComponent,
                    HotkeyModule,
                    NotificacaoComponent,
                    SimpleDialogComponent,
                    BreadcrumbModule,
                    MenuSearchComponent,
                    ToastModule
                ],
                entryComponents: [
                    LayoutComponent,
                    HeaderComponent,
                    NavComponent,
                    MenuComponent,
                    FavnavComponent,
                    SearchComponent,
                    NotificationComponent,
                    SysteminfoComponent,
                    AccessibilityComponent,
                    UserinfoComponent,
                    MenuItemComponent,
                    NotificacaoComponent,
                    SimpleDialogComponent,
                    BreadcrumbComponent,
                    IaComponent
                ]
            },] }
];
/** @nocollapse */
LayoutModule.ctorParameters = () => [
    { type: PreviousRouteService }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    LayoutModule.prototype.previousRouteService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGF5b3V0Lm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvbGF5b3V0L2xheW91dC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxRQUFRLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDdkMsT0FBTyxFQUFDLFlBQVksRUFBQyxNQUFNLGlCQUFpQixDQUFDO0FBQzdDLE9BQU8sRUFBQyxXQUFXLEVBQUUsbUJBQW1CLEVBQUMsTUFBTSxnQkFBZ0IsQ0FBQztBQUVoRSxPQUFPLEVBQUMsZUFBZSxFQUFDLE1BQU0sb0JBQW9CLENBQUM7QUFDbkQsT0FBTyxFQUFDLGVBQWUsRUFBQyxNQUFNLDJCQUEyQixDQUFDO0FBQzFELE9BQU8sRUFBQyxnQkFBZ0IsRUFBQyxNQUFNLDJCQUEyQixDQUFDO0FBQzNELE9BQU8sRUFBQyxlQUFlLEVBQUMsTUFBTSwwQkFBMEIsQ0FBQztBQUN6RCxPQUFPLEVBQUMsZ0JBQWdCLEVBQUMsTUFBTSwyQkFBMkIsQ0FBQztBQUMzRCxPQUFPLEVBQUMsYUFBYSxFQUFDLE1BQU0sd0JBQXdCLENBQUM7QUFDckQsT0FBTyxFQUFDLGFBQWEsRUFBQyxNQUFNLHdCQUF3QixDQUFDO0FBQ3JELE9BQU8sRUFBQyxjQUFjLEVBQUMsTUFBTSx5QkFBeUIsQ0FBQztBQUN2RCxPQUFPLEVBQUMsZ0JBQWdCLEVBQUMsTUFBTSwyQkFBMkIsQ0FBQztBQUMzRCxPQUFPLEVBQUMsY0FBYyxFQUFDLE1BQU0sd0JBQXdCLENBQUM7QUFDdEQsT0FBTyxFQUFDLGtCQUFrQixFQUFDLE1BQU0sNkJBQTZCLENBQUM7QUFDL0QsT0FBTyxFQUFDLGFBQWEsRUFBQyxNQUFNLHdCQUF3QixDQUFDO0FBQ3JELE9BQU8sRUFBQyxxQkFBcUIsRUFBQyxNQUFNLGdDQUFnQyxDQUFDO0FBQ3JFLE9BQU8sRUFBQyxrQkFBa0IsRUFBQyxNQUFNLDhCQUE4QixDQUFDO0FBQ2hFLE9BQU8sRUFBQyxjQUFjLEVBQUMsTUFBTSx5QkFBeUIsQ0FBQztBQUV2RCxPQUFPLEVBQUMsWUFBWSxFQUFDLE1BQU0scUJBQXFCLENBQUM7QUFDakQsT0FBTyxFQUFDLGFBQWEsRUFBQyxNQUFNLDJCQUEyQixDQUFDO0FBQ3hELE9BQU8sRUFBQyxlQUFlLEVBQUMsTUFBTSwrQkFBK0IsQ0FBQztBQUM5RCxPQUFPLEVBQUMsbUJBQW1CLEVBQUMsTUFBTSx1Q0FBdUMsQ0FBQztBQUMxRSxPQUFPLEVBQUMsZUFBZSxFQUFDLE1BQU0sa0NBQWtDLENBQUM7QUFDakUsT0FBTyxFQUFDLHFCQUFxQixFQUFDLE1BQU0sOENBQThDLENBQUM7QUFDbkYsT0FBTyxFQUFDLG1CQUFtQixFQUFDLE1BQU0sMENBQTBDLENBQUM7QUFDN0UsT0FBTyxFQUFDLHNCQUFzQixFQUFDLE1BQU0sZ0RBQWdELENBQUM7QUFDdEYsT0FBTyxFQUFDLGlCQUFpQixFQUFDLE1BQU0sc0NBQXNDLENBQUM7QUFDdkUsT0FBTyxFQUFDLGNBQWMsRUFBQyxNQUFNLHlCQUF5QixDQUFDO0FBRXZELE9BQU8sRUFBQyxlQUFlLEVBQUMsTUFBTSwwQkFBMEIsQ0FBQztBQUV6RCxPQUFPLEVBQUMsYUFBYSxFQUFDLE1BQU0sa0JBQWtCLENBQUM7QUFDL0MsT0FBTyxFQUFDLGNBQWMsRUFBQyxNQUFNLG9DQUFvQyxDQUFDO0FBRWxFLE9BQU8sRUFBQyxlQUFlLEVBQUMsTUFBTSx3QkFBd0IsQ0FBQztBQUN2RCxPQUFPLEVBQUMsZ0JBQWdCLEVBQUMsTUFBTSxzQkFBc0IsQ0FBQztBQUN0RCxPQUFPLEVBQUMsb0JBQW9CLEVBQUMsTUFBTSxnQ0FBZ0MsQ0FBQztBQUNwRSxPQUFPLEVBQUMsYUFBYSxFQUFDLE1BQU0sd0JBQXdCLENBQUM7QUFDckQsT0FBTyxFQUFDLGVBQWUsRUFBQyxNQUFNLDBCQUEwQixDQUFDO0FBQ3pELE9BQU8sRUFBQyxrQkFBa0IsRUFBQyxNQUFNLDZCQUE2QixDQUFDO0FBQy9ELE9BQU8sRUFBQyxpQkFBaUIsRUFBQyxNQUFNLDBDQUEwQyxDQUFDO0FBQzNFLE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUU3QyxPQUFPLEVBQUMsWUFBWSxFQUFDLE1BQU0sa0JBQWtCLENBQUM7QUFDOUMsT0FBTyxFQUFDLGlCQUFpQixFQUFFLGNBQWMsRUFBRSxhQUFhLEVBQUMsTUFBTSxtQkFBbUIsQ0FBQztBQUNuRixPQUFPLEVBQUMsb0JBQW9CLEVBQUMsTUFBTSx5REFBeUQsQ0FBQztBQUM3RixPQUFPLEVBQUMscUJBQXFCLEVBQUMsTUFBTSxxREFBcUQsQ0FBQztBQUMxRixPQUFPLEVBQUMsYUFBYSxFQUFDLE1BQU0sNENBQTRDLENBQUM7QUFDekUsT0FBTyxFQUFDLFdBQVcsRUFBQyxNQUFNLG1CQUFtQixDQUFDO0FBQzlDLE9BQU8sRUFBQyxnQkFBZ0IsRUFBQyxNQUFNLGdEQUFnRCxDQUFDO0FBQ2hGLE9BQU8sRUFBQyxpQkFBaUIsRUFBQyxNQUFNLHlCQUF5QixDQUFDO0FBRTFELE9BQU8sRUFBQyxXQUFXLEVBQUMsTUFBTSxzQkFBc0IsQ0FBQztBQUNqRCxPQUFPLEVBQUMsb0JBQW9CLEVBQUMsTUFBTSxzQ0FBc0MsQ0FBQztBQUMxRSxPQUFPLEVBQUMsa0JBQWtCLEVBQUMsTUFBTSxxQ0FBcUMsQ0FBQztBQUN2RSxPQUFPLEVBQUMsbUJBQW1CLEVBQUMsTUFBTSx5Q0FBeUMsQ0FBQztBQUM1RSxPQUFPLEVBQUMsbUJBQW1CLEVBQUMsTUFBTSwrQkFBK0IsQ0FBQztBQXlHbEUsTUFBTSxPQUFPLFlBQVk7Ozs7SUFDdkIsWUFBb0Isb0JBQTBDO1FBQTFDLHlCQUFvQixHQUFwQixvQkFBb0IsQ0FBc0I7SUFDOUQsQ0FBQzs7O1lBekdGLFFBQVEsU0FBQztnQkFDUixTQUFTLEVBQUU7b0JBQ1QsYUFBYTtvQkFDYixjQUFjO29CQUNkLGFBQWE7b0JBQ2Isb0JBQW9CO29CQUNwQixtQkFBbUI7aUJBQ3BCO2dCQUNELFlBQVksRUFBRTtvQkFDWixlQUFlO29CQUNmLGVBQWU7b0JBQ2YsWUFBWTtvQkFDWixhQUFhO29CQUNiLGVBQWU7b0JBQ2YsZUFBZTtvQkFDZixrQkFBa0I7b0JBQ2xCLHFCQUFxQjtvQkFDckIsbUJBQW1CO29CQUNuQixzQkFBc0I7b0JBQ3RCLGlCQUFpQjtvQkFDakIsbUJBQW1CO29CQUNuQixpQkFBaUI7b0JBQ2pCLG9CQUFvQjtvQkFDcEIscUJBQXFCO29CQUNyQixtQkFBbUI7b0JBQ25CLG1CQUFtQjtvQkFDbkIsV0FBVztpQkFDWjtnQkFDRCxPQUFPLEVBQUU7b0JBQ1AsWUFBWTtvQkFDWixZQUFZO29CQUNaLFdBQVc7b0JBQ1gsbUJBQW1CO29CQUNuQixZQUFZLENBQUMsT0FBTyxFQUFFO29CQUN0QixnQkFBZ0I7b0JBQ2hCLGlCQUFpQjtvQkFDakIsZ0JBQWdCO29CQUNoQixlQUFlO29CQUNmLGFBQWE7b0JBQ2IsYUFBYTtvQkFDYixjQUFjO29CQUNkLGdCQUFnQjtvQkFDaEIsY0FBYztvQkFDZCxrQkFBa0I7b0JBQ2xCLGFBQWE7b0JBQ2IscUJBQXFCO29CQUNyQixrQkFBa0I7b0JBQ2xCLGNBQWM7b0JBQ2QsZUFBZTtvQkFDZixjQUFjO29CQUNkLGVBQWU7b0JBQ2YsZ0JBQWdCO29CQUNoQixvQkFBb0I7b0JBQ3BCLGFBQWE7b0JBQ2IsZUFBZTtvQkFDZixrQkFBa0I7b0JBQ2xCLGNBQWM7b0JBQ2QsYUFBYTtvQkFDYixnQkFBZ0I7b0JBQ2hCLGlCQUFpQjtvQkFDakIsV0FBVztpQkFDWjtnQkFDRCxPQUFPLEVBQUU7b0JBQ1AsYUFBYTtvQkFDYixlQUFlO29CQUNmLGlCQUFpQjtvQkFDakIsZUFBZTtvQkFDZixlQUFlO29CQUNmLFlBQVk7b0JBQ1osYUFBYTtvQkFDYixlQUFlO29CQUNmLGFBQWE7b0JBQ2IsZUFBZTtvQkFDZixxQkFBcUI7b0JBQ3JCLG1CQUFtQjtvQkFDbkIsc0JBQXNCO29CQUN0QixpQkFBaUI7b0JBQ2pCLGlCQUFpQjtvQkFDakIsWUFBWTtvQkFDWixvQkFBb0I7b0JBQ3BCLHFCQUFxQjtvQkFDckIsZ0JBQWdCO29CQUNoQixtQkFBbUI7b0JBQ25CLFdBQVc7aUJBQ1o7Z0JBQ0QsZUFBZSxFQUFFO29CQUNmLGVBQWU7b0JBQ2YsZUFBZTtvQkFDZixZQUFZO29CQUNaLGFBQWE7b0JBQ2IsZUFBZTtvQkFDZixlQUFlO29CQUNmLHFCQUFxQjtvQkFDckIsbUJBQW1CO29CQUNuQixzQkFBc0I7b0JBQ3RCLGlCQUFpQjtvQkFDakIsaUJBQWlCO29CQUNqQixvQkFBb0I7b0JBQ3BCLHFCQUFxQjtvQkFDckIsbUJBQW1CO29CQUNuQixXQUFXO2lCQUNaO2FBQ0Y7Ozs7WUEzR08sb0JBQW9COzs7Ozs7O0lBNkdkLDRDQUFrRCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7TmdNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtDb21tb25Nb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQge0Zvcm1zTW9kdWxlLCBSZWFjdGl2ZUZvcm1zTW9kdWxlfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5cbmltcG9ydCB7TGF5b3V0Q29tcG9uZW50fSBmcm9tICcuL2xheW91dC5jb21wb25lbnQnO1xuaW1wb3J0IHtIZWFkZXJDb21wb25lbnR9IGZyb20gJy4vaGVhZGVyL2hlYWRlci5jb21wb25lbnQnO1xuaW1wb3J0IHtNYXRTaWRlbmF2TW9kdWxlfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9zaWRlbmF2JztcbmltcG9ydCB7TWF0QnV0dG9uTW9kdWxlfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9idXR0b24nO1xuaW1wb3J0IHtNYXRUb29sYmFyTW9kdWxlfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC90b29sYmFyJztcbmltcG9ydCB7TWF0TWVudU1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvbWVudSc7XG5pbXBvcnQge01hdEljb25Nb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2ljb24nO1xuaW1wb3J0IHtNYXRCYWRnZU1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvYmFkZ2UnO1xuaW1wb3J0IHtNYXRUb29sdGlwTW9kdWxlfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC90b29sdGlwJztcbmltcG9ydCB7RHJhZ0Ryb3BNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2Nkay9kcmFnLWRyb3AnO1xuaW1wb3J0IHtNYXRFeHBhbnNpb25Nb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2V4cGFuc2lvbic7XG5pbXBvcnQge01hdExpc3RNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2xpc3QnO1xuaW1wb3J0IHtNYXRBdXRvY29tcGxldGVNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2F1dG9jb21wbGV0ZSc7XG5pbXBvcnQge01hdEZvcm1GaWVsZE1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvZm9ybS1maWVsZCc7XG5pbXBvcnQge01hdElucHV0TW9kdWxlfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9pbnB1dCc7XG5cbmltcG9ydCB7TmF2Q29tcG9uZW50fSBmcm9tICcuL25hdi9uYXYuY29tcG9uZW50JztcbmltcG9ydCB7TWVudUNvbXBvbmVudH0gZnJvbSAnLi9uYXYvbWVudS9tZW51LmNvbXBvbmVudCc7XG5pbXBvcnQge0Zhdm5hdkNvbXBvbmVudH0gZnJvbSAnLi9uYXYvZmF2bmF2L2Zhdm5hdi5jb21wb25lbnQnO1xuaW1wb3J0IHtCcmVhZGNydW1iQ29tcG9uZW50fSBmcm9tICcuL25hdi9icmVhZGNydW1iL2JyZWFkY3J1bWIuY29tcG9uZW50JztcbmltcG9ydCB7U2VhcmNoQ29tcG9uZW50fSBmcm9tICcuL2hlYWRlci9zZWFyY2gvc2VhcmNoLmNvbXBvbmVudCc7XG5pbXBvcnQge05vdGlmaWNhdGlvbkNvbXBvbmVudH0gZnJvbSAnLi9oZWFkZXIvbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5jb21wb25lbnQnO1xuaW1wb3J0IHtTeXN0ZW1pbmZvQ29tcG9uZW50fSBmcm9tICcuL2hlYWRlci9zeXN0ZW1pbmZvL3N5c3RlbWluZm8uY29tcG9uZW50JztcbmltcG9ydCB7QWNjZXNzaWJpbGl0eUNvbXBvbmVudH0gZnJvbSAnLi9oZWFkZXIvYWNjZXNzaWJpbGl0eS9hY2Nlc3NpYmlsaXR5LmNvbXBvbmVudCc7XG5pbXBvcnQge1VzZXJpbmZvQ29tcG9uZW50fSBmcm9tICcuL2hlYWRlci91c2VyaW5mby91c2VyaW5mby5jb21wb25lbnQnO1xuaW1wb3J0IHtNYXRDaGlwc01vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvY2hpcHMnO1xuXG5pbXBvcnQge01hdFNsaWRlck1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvc2xpZGVyJztcblxuaW1wb3J0IHtMYXlvdXRTZXJ2aWNlfSBmcm9tICcuL2xheW91dC5zZXJ2aWNlJztcbmltcG9ydCB7RmF2TmF2c1NlcnZpY2V9IGZyb20gJy4vbmF2L2Zhdm5hdi9zdGF0ZS9mYXZuYXZzLnNlcnZpY2UnO1xuXG5pbXBvcnQge1Njcm9sbGluZ01vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY2RrL3Njcm9sbGluZyc7XG5pbXBvcnQge0Nka1N0ZXBwZXJNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2Nkay9zdGVwcGVyJztcbmltcG9ydCB7TWF0U2xpZGVUb2dnbGVNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3NsaWRlLXRvZ2dsZSc7XG5pbXBvcnQge01hdFRhYnNNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3RhYnMnO1xuaW1wb3J0IHtNYXRTZWxlY3RNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3NlbGVjdCc7XG5pbXBvcnQge01hdFBhZ2luYXRvck1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvcGFnaW5hdG9yJztcbmltcG9ydCB7TWVudUl0ZW1Db21wb25lbnR9IGZyb20gJy4vbmF2L21lbnUvbWVudS1pdGVtL21lbnUtaXRlbS5jb21wb25lbnQnO1xuaW1wb3J0IHtSb3V0ZXJNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5cbmltcG9ydCB7SG90a2V5TW9kdWxlfSBmcm9tICdhbmd1bGFyMi1ob3RrZXlzJztcbmltcG9ydCB7TWF0U25hY2tCYXJNb2R1bGUsIE1hdFRhYmxlTW9kdWxlLCBNYXRUcmVlTW9kdWxlfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XG5pbXBvcnQge05vdGlmaWNhY2FvQ29tcG9uZW50fSBmcm9tICcuL2hlYWRlci9ub3RpZmljYXRpb24vbm90aWZpY2FjYW8vbm90aWZpY2FjYW8uY29tcG9uZW50JztcbmltcG9ydCB7U2ltcGxlRGlhbG9nQ29tcG9uZW50fSBmcm9tICcuLi9jb21wb25lbnRzL3NpbXBsZS1kaWFsb2cvc2ltcGxlLWRpYWxvZy5jb21wb25lbnQnO1xuaW1wb3J0IHtEaWFsb2dTZXJ2aWNlfSBmcm9tICcuLi9jb21wb25lbnRzL3NpbXBsZS1kaWFsb2cvZGlhbG9nLnNlcnZpY2UnO1xuaW1wb3J0IHtJYUNvbXBvbmVudH0gZnJvbSAnLi9pYS9pYS5jb21wb25lbnQnO1xuaW1wb3J0IHtCcmVhZGNydW1iTW9kdWxlfSBmcm9tICcuL25hdi9icmVhZGNydW1iL2JyZWFkY3J1bWJzL2JyZWFkY3J1bWIubW9kdWxlJztcbmltcG9ydCB7VWlraXRTaGFyZWRNb2R1bGV9IGZyb20gJy4uL3NoYXJlZC9zaGFyZWQubW9kdWxlJztcblxuaW1wb3J0IHtUb2FzdE1vZHVsZX0gZnJvbSAnLi90b2FzdC90b2FzdC5tb2R1bGUnO1xuaW1wb3J0IHtQcmV2aW91c1JvdXRlU2VydmljZX0gZnJvbSAnLi4vY29tcG9uZW50cy9wcmV2aW91cy1yb3V0ZS5zZXJ2aWNlJztcbmltcG9ydCB7SGlnaGxpZ2h0Q29tcG9uZW50fSBmcm9tICcuL25hdi9oaWdobGlnaHQvaGlnaGxpZ2h0LmNvbXBvbmVudCc7XG5pbXBvcnQge01lbnVTZWFyY2hDb21wb25lbnR9IGZyb20gXCIuL25hdi9tZW51LXNlYXJjaC9tZW51LXNlYXJjaC5jb21wb25lbnRcIjtcbmltcG9ydCB7VWlraXRScmlwcGxlU2VydmljZX0gZnJvbSBcIi4uL3V0aWxzL3Vpa2l0LXJpcHBsZS5zZXJ2aWNlXCI7XG5cbkBOZ01vZHVsZSh7XG4gIHByb3ZpZGVyczogW1xuICAgIExheW91dFNlcnZpY2UsXG4gICAgRmF2TmF2c1NlcnZpY2UsXG4gICAgRGlhbG9nU2VydmljZSxcbiAgICBQcmV2aW91c1JvdXRlU2VydmljZSxcbiAgICBVaWtpdFJyaXBwbGVTZXJ2aWNlXG4gIF0sXG4gIGRlY2xhcmF0aW9uczogW1xuICAgIExheW91dENvbXBvbmVudCxcbiAgICBIZWFkZXJDb21wb25lbnQsXG4gICAgTmF2Q29tcG9uZW50LFxuICAgIE1lbnVDb21wb25lbnQsXG4gICAgRmF2bmF2Q29tcG9uZW50LFxuICAgIFNlYXJjaENvbXBvbmVudCxcbiAgICBIaWdobGlnaHRDb21wb25lbnQsXG4gICAgTm90aWZpY2F0aW9uQ29tcG9uZW50LFxuICAgIFN5c3RlbWluZm9Db21wb25lbnQsXG4gICAgQWNjZXNzaWJpbGl0eUNvbXBvbmVudCxcbiAgICBVc2VyaW5mb0NvbXBvbmVudCxcbiAgICBCcmVhZGNydW1iQ29tcG9uZW50LFxuICAgIE1lbnVJdGVtQ29tcG9uZW50LFxuICAgIE5vdGlmaWNhY2FvQ29tcG9uZW50LFxuICAgIFNpbXBsZURpYWxvZ0NvbXBvbmVudCxcbiAgICBCcmVhZGNydW1iQ29tcG9uZW50LFxuICAgIE1lbnVTZWFyY2hDb21wb25lbnQsXG4gICAgSWFDb21wb25lbnRcbiAgXSxcbiAgaW1wb3J0czogW1xuICAgIENvbW1vbk1vZHVsZSxcbiAgICBSb3V0ZXJNb2R1bGUsXG4gICAgRm9ybXNNb2R1bGUsXG4gICAgUmVhY3RpdmVGb3Jtc01vZHVsZSxcbiAgICBIb3RrZXlNb2R1bGUuZm9yUm9vdCgpLFxuICAgIE1hdFNpZGVuYXZNb2R1bGUsXG4gICAgTWF0U25hY2tCYXJNb2R1bGUsXG4gICAgTWF0VG9vbGJhck1vZHVsZSxcbiAgICBNYXRCdXR0b25Nb2R1bGUsXG4gICAgTWF0TWVudU1vZHVsZSxcbiAgICBNYXRJY29uTW9kdWxlLFxuICAgIE1hdEJhZGdlTW9kdWxlLFxuICAgIE1hdFRvb2x0aXBNb2R1bGUsXG4gICAgRHJhZ0Ryb3BNb2R1bGUsXG4gICAgTWF0RXhwYW5zaW9uTW9kdWxlLFxuICAgIE1hdExpc3RNb2R1bGUsXG4gICAgTWF0QXV0b2NvbXBsZXRlTW9kdWxlLFxuICAgIE1hdEZvcm1GaWVsZE1vZHVsZSxcbiAgICBNYXRJbnB1dE1vZHVsZSxcbiAgICBTY3JvbGxpbmdNb2R1bGUsXG4gICAgTWF0Q2hpcHNNb2R1bGUsXG4gICAgTWF0U2xpZGVyTW9kdWxlLFxuICAgIENka1N0ZXBwZXJNb2R1bGUsXG4gICAgTWF0U2xpZGVUb2dnbGVNb2R1bGUsXG4gICAgTWF0VGFic01vZHVsZSxcbiAgICBNYXRTZWxlY3RNb2R1bGUsXG4gICAgTWF0UGFnaW5hdG9yTW9kdWxlLFxuICAgIE1hdFRhYmxlTW9kdWxlLFxuICAgIE1hdFRyZWVNb2R1bGUsXG4gICAgQnJlYWRjcnVtYk1vZHVsZSxcbiAgICBVaWtpdFNoYXJlZE1vZHVsZSxcbiAgICBUb2FzdE1vZHVsZSxcbiAgXSxcbiAgZXhwb3J0czogW1xuICAgIE1hdEljb25Nb2R1bGUsXG4gICAgTWF0QnV0dG9uTW9kdWxlLFxuICAgIE1hdFNuYWNrQmFyTW9kdWxlLFxuICAgIExheW91dENvbXBvbmVudCxcbiAgICBIZWFkZXJDb21wb25lbnQsXG4gICAgTmF2Q29tcG9uZW50LFxuICAgIE1lbnVDb21wb25lbnQsXG4gICAgRmF2bmF2Q29tcG9uZW50LFxuICAgIE1hdExpc3RNb2R1bGUsXG4gICAgU2VhcmNoQ29tcG9uZW50LFxuICAgIE5vdGlmaWNhdGlvbkNvbXBvbmVudCxcbiAgICBTeXN0ZW1pbmZvQ29tcG9uZW50LFxuICAgIEFjY2Vzc2liaWxpdHlDb21wb25lbnQsXG4gICAgVXNlcmluZm9Db21wb25lbnQsXG4gICAgTWVudUl0ZW1Db21wb25lbnQsXG4gICAgSG90a2V5TW9kdWxlLFxuICAgIE5vdGlmaWNhY2FvQ29tcG9uZW50LFxuICAgIFNpbXBsZURpYWxvZ0NvbXBvbmVudCxcbiAgICBCcmVhZGNydW1iTW9kdWxlLFxuICAgIE1lbnVTZWFyY2hDb21wb25lbnQsXG4gICAgVG9hc3RNb2R1bGVcbiAgXSxcbiAgZW50cnlDb21wb25lbnRzOiBbXG4gICAgTGF5b3V0Q29tcG9uZW50LFxuICAgIEhlYWRlckNvbXBvbmVudCxcbiAgICBOYXZDb21wb25lbnQsXG4gICAgTWVudUNvbXBvbmVudCxcbiAgICBGYXZuYXZDb21wb25lbnQsXG4gICAgU2VhcmNoQ29tcG9uZW50LFxuICAgIE5vdGlmaWNhdGlvbkNvbXBvbmVudCxcbiAgICBTeXN0ZW1pbmZvQ29tcG9uZW50LFxuICAgIEFjY2Vzc2liaWxpdHlDb21wb25lbnQsXG4gICAgVXNlcmluZm9Db21wb25lbnQsXG4gICAgTWVudUl0ZW1Db21wb25lbnQsXG4gICAgTm90aWZpY2FjYW9Db21wb25lbnQsXG4gICAgU2ltcGxlRGlhbG9nQ29tcG9uZW50LFxuICAgIEJyZWFkY3J1bWJDb21wb25lbnQsXG4gICAgSWFDb21wb25lbnRcbiAgXVxufSlcbmV4cG9ydCBjbGFzcyBMYXlvdXRNb2R1bGUge1xuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHByZXZpb3VzUm91dGVTZXJ2aWNlOiBQcmV2aW91c1JvdXRlU2VydmljZSkge1xuICB9XG59XG4iXX0=