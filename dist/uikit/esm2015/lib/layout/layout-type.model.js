/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {number} */
const LayoutType = {
    normal: 0,
    fullscreen: 1,
    noshell: 2,
    noshellfullscreen: 3,
    noshellnobreadcrumb: 4,
    noshellnobreadcrumbfullscreen: 5,
};
export { LayoutType };
LayoutType[LayoutType.normal] = 'normal';
LayoutType[LayoutType.fullscreen] = 'fullscreen';
LayoutType[LayoutType.noshell] = 'noshell';
LayoutType[LayoutType.noshellfullscreen] = 'noshellfullscreen';
LayoutType[LayoutType.noshellnobreadcrumb] = 'noshellnobreadcrumb';
LayoutType[LayoutType.noshellnobreadcrumbfullscreen] = 'noshellnobreadcrumbfullscreen';
/**
 * @record
 */
export function FsDocumentElement() { }
if (false) {
    /** @type {?|undefined} */
    FsDocumentElement.prototype.msRequestFullscreen;
    /** @type {?|undefined} */
    FsDocumentElement.prototype.mozRequestFullScreen;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGF5b3V0LXR5cGUubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL2xheW91dC9sYXlvdXQtdHlwZS5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7SUFDRSxTQUFNO0lBQ04sYUFBVTtJQUNWLFVBQU87SUFDUCxvQkFBaUI7SUFDakIsc0JBQW1CO0lBQ25CLGdDQUE2Qjs7Ozs7Ozs7Ozs7O0FBRy9CLHVDQUdDOzs7SUFGQyxnREFBaUM7O0lBQ2pDLGlEQUFrQyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBlbnVtIExheW91dFR5cGUge1xuICBub3JtYWwsXG4gIGZ1bGxzY3JlZW4sXG4gIG5vc2hlbGwsXG4gIG5vc2hlbGxmdWxsc2NyZWVuLFxuICBub3NoZWxsbm9icmVhZGNydW1iLFxuICBub3NoZWxsbm9icmVhZGNydW1iZnVsbHNjcmVlblxufVxuXG5leHBvcnQgaW50ZXJmYWNlIEZzRG9jdW1lbnRFbGVtZW50IGV4dGVuZHMgSFRNTEVsZW1lbnQge1xuICBtc1JlcXVlc3RGdWxsc2NyZWVuPzogKCkgPT4gdm9pZDtcbiAgbW96UmVxdWVzdEZ1bGxTY3JlZW4/OiAoKSA9PiB2b2lkO1xufVxuIl19