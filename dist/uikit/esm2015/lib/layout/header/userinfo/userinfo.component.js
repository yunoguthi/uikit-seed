/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ChangeDetectionStrategy } from '@angular/core';
import { AuthService } from '../../../shared/auth/auth.service';
import { ToastService } from '../../toast/toast.service';
export class UserinfoComponent {
    /**
     * @param {?} authService
     * @param {?} toastService
     */
    constructor(authService, toastService) {
        this.authService = authService;
        this.toastService = toastService;
        this.user$ = this.authService.user.user$;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @return {?}
     */
    login() {
        return this.authService.authentication.login()
            .catch((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            this.toastService.error('Error ao realizar o login!', (/** @type {?} */ (error)));
        }));
    }
    /**
     * @return {?}
     */
    logout() {
        try {
            this.authService.authentication.logout();
        }
        catch (error) {
            this.toastService.error('Error ao realizar o logout!', (/** @type {?} */ (error)));
        }
    }
}
UserinfoComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-userinfo',
                template: "<button *ngIf=\"(user$ | async).authenticated === false; else notAuthenticated\" mat-raised-button (click)=\"login()\"\n  class=\"btn-user-login\" color=\"primary\" tabindex=\"6\">\n  <span>Fazer Login</span>\n</button>\n\n<ng-template #notAuthenticated>\n  <mat-menu #userInfoMenu=\"matMenu\">\n    <mat-list class=\"dropdown-list\" (click)=\"$event.stopPropagation()\">\n      <div class=\"logged-info\">\n        <mat-list>\n          <mat-list-item>\n            <div mat-line>\n              <h4>\n\n                <b>\n                  {{ (user$ | async).nickname ? (user$ | async).nickname : '' }}\n                </b>\n\n                <br>\n\n                <i style=\"font-size: 13px;\">\n                  {{ (user$ | async).nickname && (user$ | async).name ? ' registrado(a) civilmente como ' : '' }}\n                </i>\n\n                <br>\n\n                <b style=\"font-size: 13px;\">\n                  {{ (user$ | async).name ? (user$ | async).name : '' }}\n                </b>\n\n              </h4>\n            </div>\n            <div mat-line *ngIf=\"(user$ | async).email\">\n              <h4>{{ (user$ | async).email }}</h4>\n            </div>\n          </mat-list-item>\n        </mat-list>\n      </div>\n\n      <mat-list *ngIf=\"(user$ | async) as user\">\n        <mat-list-item class=\"profiles\" *ngIf=\"user.roles as roles\">\n          <div mat-line>\n            <h4>\n              <mat-icon class=\"fas fa-id-card\"></mat-icon>\n              {{ roles.length === 0 ? 'Sem perfil' : (roles.length === 1 ? 'Perfil' : 'Perfis') }}\n            </h4>\n          </div>\n          <div mat-line *ngFor=\"let role of roles\">\n            {{ role }}\n          </div>\n        </mat-list-item>\n      </mat-list>\n\n      <mat-list>\n        <mat-list-item style=\"min-height: auto !important;\">\n          <div mat-line>\n            <ng-content></ng-content>\n          </div>\n        </mat-list-item>\n      </mat-list>\n\n      <mat-divider></mat-divider>\n      <mat-action-list>\n        <a (click)=\"logout()\" mat-list-item>Sair</a>\n      </mat-action-list>\n    </mat-list>\n  </mat-menu>\n\n  <button tabindex=\"6\" mat-button class=\"btn-user-info\" color=\"primary\" [matMenuTriggerFor]=\"userInfoMenu\"\n    matTooltip=\"Usu\u00E1rio Autenticado\" aria-label=\"Usu\u00E1rio Autenticado\">\n    <div class=\"userinfo\">\n      <div class=\"photo\">\n        <div class=\"picture\">\n          <mat-icon *ngIf=\"!(user$ | async).picture\" aria-label=\"Example icon-button with a heart icon\"\n            class=\"fas fa-user\"></mat-icon>\n          <img *ngIf=\"(user$ | async).picture\" [src]=\"(user$ | async).picture\" alt=\"Foto do usu\u00E1rio logado\" />\n        </div>\n        <mat-icon class=\"fas fa-chevron-circle-down\"></mat-icon>\n      </div>\n\n      <div class=\"info\">\n        <span class=\"name\">{{ (user$ | async).nickname || (user$ | async).given_name || (user$ | async).name }}</span>\n        <!-- <span *ngFor=\"let role of (user$ | async).roles\" class=\"location\">{{ role }}</span> -->\n      </div>\n    </div>\n  </button>\n</ng-template>\n",
                changeDetection: ChangeDetectionStrategy.OnPush,
                styles: [""]
            }] }
];
/** @nocollapse */
UserinfoComponent.ctorParameters = () => [
    { type: AuthService },
    { type: ToastService }
];
if (false) {
    /** @type {?} */
    UserinfoComponent.prototype.user$;
    /**
     * @type {?}
     * @protected
     */
    UserinfoComponent.prototype.authService;
    /**
     * @type {?}
     * @protected
     */
    UserinfoComponent.prototype.toastService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlcmluZm8uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9sYXlvdXQvaGVhZGVyL3VzZXJpbmZvL3VzZXJpbmZvLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFNBQVMsRUFBVSx1QkFBdUIsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUV6RSxPQUFPLEVBQUMsV0FBVyxFQUFDLE1BQU0sbUNBQW1DLENBQUM7QUFDOUQsT0FBTyxFQUFDLFlBQVksRUFBQyxNQUFNLDJCQUEyQixDQUFDO0FBUXZELE1BQU0sT0FBTyxpQkFBaUI7Ozs7O0lBRzVCLFlBQ1ksV0FBd0IsRUFDeEIsWUFBMEI7UUFEMUIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsaUJBQVksR0FBWixZQUFZLENBQWM7UUFKdEMsVUFBSyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztJQUtwQyxDQUFDOzs7O0lBRUQsUUFBUTtJQUVSLENBQUM7Ozs7SUFFTSxLQUFLO1FBQ1YsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxLQUFLLEVBQUU7YUFDM0MsS0FBSzs7OztRQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ2IsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsNEJBQTRCLEVBQUUsbUJBQUEsS0FBSyxFQUFTLENBQUMsQ0FBQztRQUN4RSxDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7SUFFTSxNQUFNO1FBQ1gsSUFBSTtZQUNGLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLE1BQU0sRUFBRSxDQUFDO1NBQzFDO1FBQUMsT0FBTyxLQUFLLEVBQUU7WUFDZCxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyw2QkFBNkIsRUFBRSxtQkFBQSxLQUFLLEVBQVMsQ0FBQyxDQUFDO1NBQ3hFO0lBQ0gsQ0FBQzs7O1lBL0JGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsZ0JBQWdCO2dCQUMxQiw0a0dBQXdDO2dCQUV4QyxlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTs7YUFDaEQ7Ozs7WUFSTyxXQUFXO1lBQ1gsWUFBWTs7OztJQVNsQixrQ0FBb0M7Ozs7O0lBR2xDLHdDQUFrQzs7Ozs7SUFDbEMseUNBQW9DIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIE9uSW5pdCwgQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3l9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQge0F1dGhTZXJ2aWNlfSBmcm9tICcuLi8uLi8uLi9zaGFyZWQvYXV0aC9hdXRoLnNlcnZpY2UnO1xuaW1wb3J0IHtUb2FzdFNlcnZpY2V9IGZyb20gJy4uLy4uL3RvYXN0L3RvYXN0LnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICd1aWtpdC11c2VyaW5mbycsXG4gIHRlbXBsYXRlVXJsOiAnLi91c2VyaW5mby5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL3VzZXJpbmZvLmNvbXBvbmVudC5zY3NzJ10sXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoXG59KVxuZXhwb3J0IGNsYXNzIFVzZXJpbmZvQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgdXNlciQgPSB0aGlzLmF1dGhTZXJ2aWNlLnVzZXIudXNlciQ7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJvdGVjdGVkIGF1dGhTZXJ2aWNlOiBBdXRoU2VydmljZSxcbiAgICBwcm90ZWN0ZWQgdG9hc3RTZXJ2aWNlOiBUb2FzdFNlcnZpY2UpIHtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuXG4gIH1cblxuICBwdWJsaWMgbG9naW4oKSB7XG4gICAgcmV0dXJuIHRoaXMuYXV0aFNlcnZpY2UuYXV0aGVudGljYXRpb24ubG9naW4oKVxuICAgICAgLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgdGhpcy50b2FzdFNlcnZpY2UuZXJyb3IoJ0Vycm9yIGFvIHJlYWxpemFyIG8gbG9naW4hJywgZXJyb3IgYXMgRXJyb3IpO1xuICAgICAgfSk7XG4gIH1cblxuICBwdWJsaWMgbG9nb3V0KCkge1xuICAgIHRyeSB7XG4gICAgICB0aGlzLmF1dGhTZXJ2aWNlLmF1dGhlbnRpY2F0aW9uLmxvZ291dCgpO1xuICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICB0aGlzLnRvYXN0U2VydmljZS5lcnJvcignRXJyb3IgYW8gcmVhbGl6YXIgbyBsb2dvdXQhJywgZXJyb3IgYXMgRXJyb3IpO1xuICAgIH1cbiAgfVxufVxuIl19