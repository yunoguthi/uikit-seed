/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
export class NotificacaoComponent {
    // @Input() click: any;
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
NotificacaoComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-notificacao',
                template: `
  <a mat-list-item [class.is-read]="read">
  <div>
    <h4 *ngIf="nome">
      <mat-icon *ngIf="icone" mat-list-icon class="{{ icone }}"></mat-icon>
      {{ nome }}
    </h4>
    <p>
      {{ descricao }}
    </p>
  </div>
  <mat-icon matListIcon class="fa-lg far" [class.fa-eye]="!read" [class.fa-eye-slash]="read"></mat-icon>
  `,
                changeDetection: ChangeDetectionStrategy.OnPush
            }] }
];
/** @nocollapse */
NotificacaoComponent.ctorParameters = () => [];
NotificacaoComponent.propDecorators = {
    nome: [{ type: Input }],
    icone: [{ type: Input }],
    descricao: [{ type: Input }],
    data: [{ type: Input }],
    read: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    NotificacaoComponent.prototype.nome;
    /** @type {?} */
    NotificacaoComponent.prototype.icone;
    /** @type {?} */
    NotificacaoComponent.prototype.descricao;
    /** @type {?} */
    NotificacaoComponent.prototype.data;
    /** @type {?} */
    NotificacaoComponent.prototype.read;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2FjYW8uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9sYXlvdXQvaGVhZGVyL25vdGlmaWNhdGlvbi9ub3RpZmljYWNhby9ub3RpZmljYWNhby5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsdUJBQXVCLEVBQUUsS0FBSyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBb0IxRixNQUFNLE9BQU8sb0JBQW9COztJQVUvQixnQkFBZ0IsQ0FBQzs7OztJQUVqQixRQUFRO0lBQ1IsQ0FBQzs7O1lBL0JGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsbUJBQW1CO2dCQUM3QixRQUFRLEVBQUU7Ozs7Ozs7Ozs7OztHQVlUO2dCQUVELGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO2FBQ2hEOzs7OzttQkFHRSxLQUFLO29CQUNMLEtBQUs7d0JBQ0wsS0FBSzttQkFDTCxLQUFLO21CQUNMLEtBQUs7Ozs7SUFKTixvQ0FBdUI7O0lBQ3ZCLHFDQUF3Qjs7SUFDeEIseUNBQTJCOztJQUMzQixvQ0FBc0I7O0lBQ3RCLG9DQUF1QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneSwgSW5wdXQsIE91dHB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICd1aWtpdC1ub3RpZmljYWNhbycsXG4gIHRlbXBsYXRlOiBgXG4gIDxhIG1hdC1saXN0LWl0ZW0gW2NsYXNzLmlzLXJlYWRdPVwicmVhZFwiPlxuICA8ZGl2PlxuICAgIDxoNCAqbmdJZj1cIm5vbWVcIj5cbiAgICAgIDxtYXQtaWNvbiAqbmdJZj1cImljb25lXCIgbWF0LWxpc3QtaWNvbiBjbGFzcz1cInt7IGljb25lIH19XCI+PC9tYXQtaWNvbj5cbiAgICAgIHt7IG5vbWUgfX1cbiAgICA8L2g0PlxuICAgIDxwPlxuICAgICAge3sgZGVzY3JpY2FvIH19XG4gICAgPC9wPlxuICA8L2Rpdj5cbiAgPG1hdC1pY29uIG1hdExpc3RJY29uIGNsYXNzPVwiZmEtbGcgZmFyXCIgW2NsYXNzLmZhLWV5ZV09XCIhcmVhZFwiIFtjbGFzcy5mYS1leWUtc2xhc2hdPVwicmVhZFwiPjwvbWF0LWljb24+XG4gIGAsXG4gIHN0eWxlczogW10sXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoXG59KVxuZXhwb3J0IGNsYXNzIE5vdGlmaWNhY2FvQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBASW5wdXQoKSBub21lPzogc3RyaW5nO1xuICBASW5wdXQoKSBpY29uZT86IHN0cmluZztcbiAgQElucHV0KCkgZGVzY3JpY2FvOiBzdHJpbmc7XG4gIEBJbnB1dCgpIGRhdGE6IHN0cmluZztcbiAgQElucHV0KCkgcmVhZDogYm9vbGVhbjtcblxuICAvLyBASW5wdXQoKSBjbGljazogYW55O1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgbmdPbkluaXQoKSB7XG4gIH1cblxufVxuIl19