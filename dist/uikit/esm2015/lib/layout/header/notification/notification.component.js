/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ChangeDetectionStrategy } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { LayoutService } from '../../layout.service';
/**
 * @record
 */
export function Section() { }
if (false) {
    /** @type {?} */
    Section.prototype.icon;
    /** @type {?} */
    Section.prototype.name;
    /** @type {?} */
    Section.prototype.description;
    /** @type {?} */
    Section.prototype.read;
}
export class NotificationComponent {
    /**
     * @param {?} layoutService
     */
    constructor(layoutService) {
        this.layoutService = layoutService;
        this.folders = [
            {
                icon: 'fas fa-file-alt',
                name: 'Photos',
                description: 'Despachar o processo 000912.25 para o gabinete do Dr. Ricardo, no dia 15/12/2018. Ligar para confirmar recebimento.',
                read: true
            },
            {
                icon: 'fas fa-file-alt',
                name: 'Photos',
                description: 'Despachar o processo 000912.2335 para o gabinete do Dr. Ricardo, no dia 15/12/2018. Ligar para confirmar recebimento.',
                read: false
            },
            {
                icon: 'fas fa-file-alt',
                name: 'Photos',
                description: 'Despachar o processo 000912.23235 para o gabinete do Dr. Ricardo, no dia 15/12/2018. Ligar para confirmar recebimento.',
                read: false
            }
        ];
        // @ContentChildren(NotificacaoComponent, { descendants: true }) public notificacoesProjetadas: QueryList<NotificacaoComponent>;
        // public quantidadeDeNotificacoes = new BehaviorSubject<number>(0);
        this.notificacoes$ = this.layoutService.notificacoes$;
        // public notificacoesNaoLidas$ = this.layoutService.notificacoes$.pipe(flatMap(r => r), filter(r => r.read == false), toArray());
        this.quantidadeDeNotificacoesNaoLidas$ = new BehaviorSubject(0);
        this.subscriptions = [];
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.notificacoes$.subscribe((/**
         * @param {?} r
         * @return {?}
         */
        r => {
            /** @type {?} */
            const notNaoLidas = r.filter((/**
             * @param {?} a
             * @return {?}
             */
            a => a.read === false));
            this.quantidadeDeNotificacoesNaoLidas$.next(notNaoLidas.length);
        }));
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.subscriptions.forEach((/**
         * @param {?} s
         * @return {?}
         */
        s => {
            if (s && !s.closed) {
                s.unsubscribe();
            }
        }));
    }
    /**
     * @return {?}
     */
    ngAfterContentInit() {
        // this.notificacoesProjetadas.changes.subscribe(r => {
        //   const notificacoesNaoLidas = this.notificacoesProjetadas.filter(notif => notif.read === false);
        //   this.quantidadeDeNotificacoes.next(notificacoesNaoLidas.length);
        // });
        // this.notificacoesProjetadas.notifyOnChanges();
    }
}
NotificationComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-notification',
                template: "<mat-menu #notificationMenu=\"matMenu\">\n  <mat-list class=\"dropdown-list list-reader\" (click)=\"$event.stopPropagation()\">\n    <h3 mat-subheader>\n      Notifica\u00E7\u00F5es\n        <mat-chip color=\"accent\" selected>{{ (quantidadeDeNotificacoesNaoLidas$ | async) }} nova(s)</mat-chip>\n    </h3>\n    <mat-nav-list>\n\n\n\n        <uikit-notificacao [nome]=\"not.nome\" [icone]=\"not.icone\" [descricao]=\"not.descricao\" [data]=\"not.data\" [read]=\"not.read\" *ngFor=\"let not of (notificacoes$ | async)\" nome=\"teste\" descricao=\"awdadwa\" read=\"false\" ></uikit-notificacao>\n\n\n    </mat-nav-list>\n  </mat-list>\n\n  <!-- <mat-action-list>\n      <a  mat-list-item routerLink=\".\">ver todos</a>\n    </mat-action-list> -->\n</mat-menu>\n\n<button\n  tabindex=\"3\"\n  mat-icon-button\n  color=\"primary\"\n  [matMenuTriggerFor]=\"notificationMenu\"\n  matTooltip=\"Notifica\u00E7\u00F5es\"\n  aria-label=\"Notifica\u00E7\u00F5es\"\n  matBadge=\"{{ (quantidadeDeNotificacoesNaoLidas$ | async) }}\"\n  matBadgeColor=\"accent\"\n  matBadgeSize=\"small\"\n>\n  <mat-icon class=\"fa-2x far fa-bell\"></mat-icon>\n</button>\n",
                changeDetection: ChangeDetectionStrategy.OnPush,
                styles: [""]
            }] }
];
/** @nocollapse */
NotificationComponent.ctorParameters = () => [
    { type: LayoutService }
];
if (false) {
    /** @type {?} */
    NotificationComponent.prototype.folders;
    /** @type {?} */
    NotificationComponent.prototype.notificacoes$;
    /** @type {?} */
    NotificationComponent.prototype.quantidadeDeNotificacoesNaoLidas$;
    /** @type {?} */
    NotificationComponent.prototype.subscriptions;
    /**
     * @type {?}
     * @protected
     */
    NotificationComponent.prototype.layoutService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvbGF5b3V0L2hlYWRlci9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSx1QkFBdUIsRUFBMkQsTUFBTSxlQUFlLENBQUM7QUFFcEksT0FBTyxFQUFFLGVBQWUsRUFBZ0IsTUFBTSxNQUFNLENBQUM7QUFFckQsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLHNCQUFzQixDQUFDOzs7O0FBRXJELDZCQUtDOzs7SUFKQyx1QkFBYTs7SUFDYix1QkFBYTs7SUFDYiw4QkFBb0I7O0lBQ3BCLHVCQUFjOztBQVNoQixNQUFNLE9BQU8scUJBQXFCOzs7O0lBbUNoQyxZQUFzQixhQUE0QjtRQUE1QixrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQWhDbEQsWUFBTyxHQUFjO1lBQ25CO2dCQUNFLElBQUksRUFBRSxpQkFBaUI7Z0JBQ3ZCLElBQUksRUFBRSxRQUFRO2dCQUNkLFdBQVcsRUFDVCxxSEFBcUg7Z0JBQ3ZILElBQUksRUFBRSxJQUFJO2FBQ1g7WUFDRDtnQkFDRSxJQUFJLEVBQUUsaUJBQWlCO2dCQUN2QixJQUFJLEVBQUUsUUFBUTtnQkFDZCxXQUFXLEVBQ1QsdUhBQXVIO2dCQUN6SCxJQUFJLEVBQUUsS0FBSzthQUNaO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLGlCQUFpQjtnQkFDdkIsSUFBSSxFQUFFLFFBQVE7Z0JBQ2QsV0FBVyxFQUNULHdIQUF3SDtnQkFDMUgsSUFBSSxFQUFFLEtBQUs7YUFDWjtTQUNGLENBQUM7OztRQUlLLGtCQUFhLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUM7O1FBRWpELHNDQUFpQyxHQUFHLElBQUksZUFBZSxDQUFTLENBQUMsQ0FBQyxDQUFDO1FBRW5FLGtCQUFhLEdBQW1CLEVBQUUsQ0FBQztJQUVZLENBQUM7Ozs7SUFFdkQsUUFBUTtRQUNOLElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUzs7OztRQUFDLENBQUMsQ0FBQyxFQUFFOztrQkFDekIsV0FBVyxHQUFHLENBQUMsQ0FBQyxNQUFNOzs7O1lBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxLQUFLLEtBQUssRUFBQztZQUNuRCxJQUFJLENBQUMsaUNBQWlDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNsRSxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7SUFFRCxXQUFXO1FBQ1QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPOzs7O1FBQUMsQ0FBQyxDQUFDLEVBQUU7WUFDN0IsSUFBRyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsTUFBTSxFQUFFO2dCQUNqQixDQUFDLENBQUMsV0FBVyxFQUFFLENBQUM7YUFDakI7UUFDSCxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7SUFFRCxrQkFBa0I7UUFDaEIsdURBQXVEO1FBQ3ZELG9HQUFvRztRQUNwRyxxRUFBcUU7UUFDckUsTUFBTTtRQUNOLGlEQUFpRDtJQUNuRCxDQUFDOzs7WUFoRUYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxvQkFBb0I7Z0JBQzlCLGlvQ0FBNEM7Z0JBRTVDLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNOzthQUNoRDs7OztZQWRRLGFBQWE7Ozs7SUFrQnBCLHdDQXNCRTs7SUFJRiw4Q0FBd0Q7O0lBRXhELGtFQUEwRTs7SUFFMUUsOENBQTBDOzs7OztJQUU5Qiw4Q0FBc0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3ksIFF1ZXJ5TGlzdCwgQ29udGVudENoaWxkcmVuLCBBZnRlckNvbnRlbnRJbml0LCBPbkRlc3Ryb3kgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE5vdGlmaWNhY2FvQ29tcG9uZW50IH0gZnJvbSAnLi9ub3RpZmljYWNhby9ub3RpZmljYWNhby5jb21wb25lbnQnO1xuaW1wb3J0IHsgQmVoYXZpb3JTdWJqZWN0LCBTdWJzY3JpcHRpb24gfSBmcm9tICdyeGpzJztcbmltcG9ydCB7ICBmbGF0TWFwLCBmaWx0ZXIsIHRvQXJyYXkgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQgeyBMYXlvdXRTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbGF5b3V0LnNlcnZpY2UnO1xuXG5leHBvcnQgaW50ZXJmYWNlIFNlY3Rpb24ge1xuICBpY29uOiBzdHJpbmc7XG4gIG5hbWU6IHN0cmluZztcbiAgZGVzY3JpcHRpb246IHN0cmluZztcbiAgcmVhZDogYm9vbGVhbjtcbn1cblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAndWlraXQtbm90aWZpY2F0aW9uJyxcbiAgdGVtcGxhdGVVcmw6ICcuL25vdGlmaWNhdGlvbi5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL25vdGlmaWNhdGlvbi5jb21wb25lbnQuc2NzcyddLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaFxufSlcbmV4cG9ydCBjbGFzcyBOb3RpZmljYXRpb25Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSwgQWZ0ZXJDb250ZW50SW5pdCB7XG5cblxuICBmb2xkZXJzOiBTZWN0aW9uW10gPSBbXG4gICAge1xuICAgICAgaWNvbjogJ2ZhcyBmYS1maWxlLWFsdCcsXG4gICAgICBuYW1lOiAnUGhvdG9zJyxcbiAgICAgIGRlc2NyaXB0aW9uOlxuICAgICAgICAnRGVzcGFjaGFyIG8gcHJvY2Vzc28gMDAwOTEyLjI1IHBhcmEgbyBnYWJpbmV0ZSBkbyBEci4gUmljYXJkbywgbm8gZGlhIDE1LzEyLzIwMTguIExpZ2FyIHBhcmEgY29uZmlybWFyIHJlY2ViaW1lbnRvLicsXG4gICAgICByZWFkOiB0cnVlXG4gICAgfSxcbiAgICB7XG4gICAgICBpY29uOiAnZmFzIGZhLWZpbGUtYWx0JyxcbiAgICAgIG5hbWU6ICdQaG90b3MnLFxuICAgICAgZGVzY3JpcHRpb246XG4gICAgICAgICdEZXNwYWNoYXIgbyBwcm9jZXNzbyAwMDA5MTIuMjMzNSBwYXJhIG8gZ2FiaW5ldGUgZG8gRHIuIFJpY2FyZG8sIG5vIGRpYSAxNS8xMi8yMDE4LiBMaWdhciBwYXJhIGNvbmZpcm1hciByZWNlYmltZW50by4nLFxuICAgICAgcmVhZDogZmFsc2VcbiAgICB9LFxuICAgIHtcbiAgICAgIGljb246ICdmYXMgZmEtZmlsZS1hbHQnLFxuICAgICAgbmFtZTogJ1Bob3RvcycsXG4gICAgICBkZXNjcmlwdGlvbjpcbiAgICAgICAgJ0Rlc3BhY2hhciBvIHByb2Nlc3NvIDAwMDkxMi4yMzIzNSBwYXJhIG8gZ2FiaW5ldGUgZG8gRHIuIFJpY2FyZG8sIG5vIGRpYSAxNS8xMi8yMDE4LiBMaWdhciBwYXJhIGNvbmZpcm1hciByZWNlYmltZW50by4nLFxuICAgICAgcmVhZDogZmFsc2VcbiAgICB9XG4gIF07XG5cbiAgLy8gQENvbnRlbnRDaGlsZHJlbihOb3RpZmljYWNhb0NvbXBvbmVudCwgeyBkZXNjZW5kYW50czogdHJ1ZSB9KSBwdWJsaWMgbm90aWZpY2Fjb2VzUHJvamV0YWRhczogUXVlcnlMaXN0PE5vdGlmaWNhY2FvQ29tcG9uZW50PjtcbiAgLy8gcHVibGljIHF1YW50aWRhZGVEZU5vdGlmaWNhY29lcyA9IG5ldyBCZWhhdmlvclN1YmplY3Q8bnVtYmVyPigwKTtcbiAgcHVibGljIG5vdGlmaWNhY29lcyQgPSB0aGlzLmxheW91dFNlcnZpY2Uubm90aWZpY2Fjb2VzJDtcbiAgLy8gcHVibGljIG5vdGlmaWNhY29lc05hb0xpZGFzJCA9IHRoaXMubGF5b3V0U2VydmljZS5ub3RpZmljYWNvZXMkLnBpcGUoZmxhdE1hcChyID0+IHIpLCBmaWx0ZXIociA9PiByLnJlYWQgPT0gZmFsc2UpLCB0b0FycmF5KCkpO1xuICBwdWJsaWMgcXVhbnRpZGFkZURlTm90aWZpY2Fjb2VzTmFvTGlkYXMkID0gbmV3IEJlaGF2aW9yU3ViamVjdDxudW1iZXI+KDApO1xuXG4gIHB1YmxpYyBzdWJzY3JpcHRpb25zOiBTdWJzY3JpcHRpb25bXSA9IFtdO1xuXG4gIGNvbnN0cnVjdG9yKHByb3RlY3RlZCBsYXlvdXRTZXJ2aWNlOiBMYXlvdXRTZXJ2aWNlKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICB0aGlzLm5vdGlmaWNhY29lcyQuc3Vic2NyaWJlKHIgPT4ge1xuICAgICAgY29uc3Qgbm90TmFvTGlkYXMgPSByLmZpbHRlcihhID0+IGEucmVhZCA9PT0gZmFsc2UpO1xuICAgICAgdGhpcy5xdWFudGlkYWRlRGVOb3RpZmljYWNvZXNOYW9MaWRhcyQubmV4dChub3ROYW9MaWRhcy5sZW5ndGgpO1xuICAgIH0pO1xuICB9XG5cbiAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XG4gICAgdGhpcy5zdWJzY3JpcHRpb25zLmZvckVhY2gocyA9PiB7XG4gICAgICBpZihzICYmICFzLmNsb3NlZCkge1xuICAgICAgICBzLnVuc3Vic2NyaWJlKCk7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cblxuICBuZ0FmdGVyQ29udGVudEluaXQoKTogdm9pZCB7XG4gICAgLy8gdGhpcy5ub3RpZmljYWNvZXNQcm9qZXRhZGFzLmNoYW5nZXMuc3Vic2NyaWJlKHIgPT4ge1xuICAgIC8vICAgY29uc3Qgbm90aWZpY2Fjb2VzTmFvTGlkYXMgPSB0aGlzLm5vdGlmaWNhY29lc1Byb2pldGFkYXMuZmlsdGVyKG5vdGlmID0+IG5vdGlmLnJlYWQgPT09IGZhbHNlKTtcbiAgICAvLyAgIHRoaXMucXVhbnRpZGFkZURlTm90aWZpY2Fjb2VzLm5leHQobm90aWZpY2Fjb2VzTmFvTGlkYXMubGVuZ3RoKTtcbiAgICAvLyB9KTtcbiAgICAvLyB0aGlzLm5vdGlmaWNhY29lc1Byb2pldGFkYXMubm90aWZ5T25DaGFuZ2VzKCk7XG4gIH1cbn1cbiJdfQ==