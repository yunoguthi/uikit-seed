/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { ChangeDetectionStrategy, Component, ContentChildren, EventEmitter, Input, Output, QueryList, ViewChild, ViewChildren, Renderer2 } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MenuItemComponent } from '../../nav/menu/menu-item/menu-item.component';
import { SearchService } from './search.service';
import { Hotkey, HotkeysService } from 'angular2-hotkeys';
import { MatInput, MatButton } from '@angular/material';
import { Router } from '@angular/router';
import { searchAnimation } from './slide-animations';
import { SearchHistoricoService } from './search-historico.service';
import * as _ from 'lodash';
import { guid } from '@datorama/akita';
import { timer, Subscription } from 'rxjs';
import { ActiveDescendantKeyManager } from '@angular/cdk/a11y';
import { ENTER, ESCAPE, UP_ARROW, DOWN_ARROW } from '@angular/cdk/keycodes';
import { HighlightComponent } from '../../nav/highlight/highlight.component';
export class SearchComponent {
    /**
     * @param {?} fb
     * @param {?} searchService
     * @param {?} hotkeysService
     * @param {?} router
     * @param {?} searchHistorico
     * @param {?} renderer
     */
    constructor(fb, searchService, hotkeysService, router, searchHistorico, renderer) {
        this.fb = fb;
        this.searchService = searchService;
        this.hotkeysService = hotkeysService;
        this.router = router;
        this.searchHistorico = searchHistorico;
        this.renderer = renderer;
        this.adicionarGrupo = new EventEmitter();
        this.removerGrupo = new EventEmitter();
        this.placeholderText = 'Pesquisa (Ctrl + Alt + 3)';
        this.stateForm = this.fb.group({
            searchGroups: '',
        });
        this.searchGroups = [];
        this.subscription = new Subscription();
        this.openSearch = false;
        this.isAnimating = false;
    }
    /**
     * @return {?}
     */
    get hasResults() {
        return this.openSearch && this.searchGroupOptions.length;
    }
    /**
     * @return {?}
     */
    ngAfterViewInit() {
        if (document.body.getBoundingClientRect().width <= 970) {
            this.placeholderText = 'Pesquisa';
        }
        this.hotkeysService.add(new Hotkey('ctrl+alt+3', (/**
         * @param {?} event
         * @return {?}
         */
        (event) => {
            this.searchOpen.focus();
            return false; // Prevent bubbling
        })));
        timer(1000).subscribe((/**
         * @return {?}
         */
        () => {
            this.searchOpen.focus();
        }));
        this.keyManager = new ActiveDescendantKeyManager(this.items).withWrap().withTypeAhead();
        this.renderer.setStyle(this.buttonClose._elementRef.nativeElement, 'display', 'none');
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.stateForm.get('searchGroups').valueChanges.subscribe((/**
         * @param {?} value
         * @return {?}
         */
        value => {
            this.itemPesquisado = value;
            this.searchGroupOptions = [];
            this.searchService.buscar(value)
                .subscribe((/**
             * @param {?} itensMenuPesquisa
             * @return {?}
             */
            (itensMenuPesquisa) => {
                if (itensMenuPesquisa && itensMenuPesquisa.length > 0) {
                    _.map(itensMenuPesquisa, (/**
                     * @param {?} item
                     * @return {?}
                     */
                    (item) => this.searchGroupOptions.push(item)));
                }
                else {
                    this.searchGroupOptions.push((/** @type {?} */ ({
                        id: guid(),
                        isAviso: true,
                        title: `Não encontramos '${value}'`
                    })));
                }
            }));
        }));
        this.searchGroupOptions = this.searchHistorico.obterHistorico();
    }
    /**
     * @param {?} event
     * @param {?} searchContainer
     * @return {?}
     */
    openToSearch(event, searchContainer) {
        if (!this.searchOpened
            && event
            && event.code === 'KeyA'
            && event.key.length > 0) {
            this.searchOpened = true;
            this.open(searchContainer);
        }
    }
    /**
     * @param {?} searchContainer
     * @return {?}
     */
    onBlur(searchContainer) {
        this.subscription.add(timer(150).subscribe((/**
         * @return {?}
         */
        () => {
            if (!this.searchOpen.focused) {
                this.close();
                this.renderer.removeAttribute(searchContainer, 'opened');
                this.renderer.setStyle(this.buttonClose._elementRef.nativeElement, 'display', 'none');
            }
        })));
    }
    /**
     * @return {?}
     */
    closeToSearch() {
        if (!this.searchOpen.focused) {
            this.close();
        }
    }
    /**
     * @param {?} searchContainer
     * @return {?}
     */
    open(searchContainer) {
        this.searchOpen.focus();
        this.renderer.setAttribute(searchContainer, 'opened', '');
        this.renderer.removeStyle(this.buttonClose._elementRef.nativeElement, 'display');
    }
    /**
     * @return {?}
     */
    close() {
        this.searchOpened = false;
        ((/** @type {?} */ (document.activeElement))).blur();
        this.stateForm.controls.searchGroups.setValue('');
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
    /**
     * @param {?} searchContainer
     * @return {?}
     */
    toggle(searchContainer) {
        this.searchOpened = (!this.searchOpened);
        if (this.searchOpened) {
            this.open(searchContainer);
        }
        this.stateForm.controls.searchGroups.setValue('');
    }
    /**
     * @param {?} item
     * @param {?} group
     * @return {?}
     */
    goToAddHistoric(item, group) {
        this.searchHistorico.salvarHistorico(group);
        this.router.navigate([item.link[0]]);
        this.closeToSearch();
    }
    /**
     * @return {?}
     */
    get heightSize() {
        return null;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onKeyup(event) {
        switch (event.keyCode) {
            case ENTER:
                /** @type {?} */
                const item = this.keyManager.activeItem.item;
                this.goToAddHistoric(item, item);
                break;
            case ESCAPE:
                this.close();
                break;
            case UP_ARROW:
            case DOWN_ARROW:
                this.keyManager.onKeydown(event);
                this.activeItem(this.keyManager.activeItem.item, event.keyCode === UP_ARROW);
                break;
            default:
                this.keyManager.setFirstItemActive();
                this.activeItem(this.keyManager.activeItem.item, false);
                break;
        }
    }
    /**
     * @param {?} item
     * @param {?} up
     * @return {?}
     */
    activeItem(item, up) {
        if ((/** @type {?} */ (item)) && ((/** @type {?} */ (item))).items && ((/** @type {?} */ (item))).items.length > 0) {
            if (up) {
                this.keyManager.setPreviousItemActive();
            }
            else {
                this.keyManager.setNextItemActive();
            }
            this.activeItem(this.keyManager.activeItem.item, up);
        }
    }
    /**
     * @param {?} item
     * @return {?}
     */
    onMouseoverItem(item) {
        this.keyManager.setActiveItem(item);
    }
}
SearchComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-search',
                template: "<form [formGroup]=\"stateForm\" [class.is-opened]=\"searchOpened\" autocomplete=\"off\" >\n  <div search-container #searchContainer [attr.animating]=\"isAnimating ? '' : null\"\n    [attr.with-results]=\"hasResults ? '' : null\">\n\n    <button mat-icon-button class=\"btn-back\" (click)=\"toggle(searchContainer)\">\n      <i class='fa fa-search icon-search' search-icon></i>\n    </button>\n    <input tabindex=\"3\" matInput (keypress)=\"openToSearch($event,searchContainer)\" #searchOpen=\"matInput\"\n      placeholder=\"Pesquisa (Ctrl + Alt + 3)\" placeholder=\"{{placeholderText}}\" formControlName=\"searchGroups\"\n      (keyup)=\"onKeyup($event)\" (blur)=\"onBlur(searchContainer)\" />\n    <button mat-icon-button class=\"icon-sobre\" #buttonClose=\"matButton\" (click)=\"searchGroupOptions = []; toggle(searchContainer)\">\n      <i class='fa fa-times icon-search' search-icon></i>\n    </button>\n\n    <div resultados-container [ngStyle]='{ height: heightSize }'>\n      <div *ngFor='let groupMenu of searchGroupOptions' [attr.header]='groupMenu.isHeader && !groupMenu.isAviso'>\n        <div group-header *ngIf='!groupMenu.isAviso'>\n          <span>{{groupMenu.title}}</span>\n        </div>\n        <ng-template [ngTemplateOutlet]=\"conteudoMenu\" [ngTemplateOutletContext]=\"{ $implicit: groupMenu }\">\n        </ng-template>\n\n      </div>\n    </div>\n  </div>\n</form>\n\n<ng-template #conteudoMenu let-list>\n  <ul name=\"listaMenus\">\n    <uikit-highlight #searchitem *ngFor='let item of list.items' [attr.header]='item.isHeader && !item.isAviso' [item]=\"item\">\n      <div class=\"search-item-title\">\n        <div group-header *ngIf='item.isHeader && !item.isAviso'>\n          <span>\n            <i class=\"first-letter\" [attr.data-first]=\"item.title.substr(0, 1)\" *ngIf='!item.icon'> </i>\n            <i class='{{item.icon}}' *ngIf='item.icon'></i>\n            {{item.title}}\n          </span>\n        </div>\n\n        <a resultado (mouseover)=\"onMouseoverItem(searchitem)\" (click)='goToAddHistoric(item, item);$event.preventDefault()' href='#'\n          *ngIf='!item.isHeader && !item.isAviso'>\n          <span innerHTML='{{item.title | highlight: itemPesquisado}}'></span>\n        </a>\n\n        <ul *ngIf=\"item.items?.length > 0\">\n          <ng-container *ngTemplateOutlet=\"conteudoMenu; context:{ $implicit: item }\"></ng-container>\n        </ul>\n\n        <div aviso *ngIf='item.isAviso'>\n          {{item.title}}\n        </div>\n      </div>\n    </uikit-highlight>\n  </ul>\n</ng-template>\n\n<div hidden>\n  <ng-content select=\"[search]\"></ng-content>\n</div>\n",
                animations: [searchAnimation()],
                changeDetection: ChangeDetectionStrategy.OnPush,
                styles: ["@media screen and (max-width:970px){.btn-back{right:0!important;top:5px}[search-container]{position:relative!important;-webkit-transition:.1s ease-in-out!important;transition:.1s ease-in-out!important;right:0!important}[search-container] [search-container]{width:40px}[search-container] i[search-icon]{-webkit-transition:.1s ease-in-out,none;transition:.1s ease-in-out,none;width:40px;height:40px;line-height:40px;top:0;right:0;z-index:2;font-size:15px;color:#004bcb;text-align:center}[search-container] input{min-width:300px!important;max-width:970px!important;width:100%!important;color:#004bcb;position:fixed;top:0}[search-container]:not([opened]) input{margin-top:-100%!important}[search-container]:not([opened]) i{border-left:0!important}[search-container][opened]{right:0!important}[search-container][opened] input{min-width:300px!important;max-width:970px!important;width:100%!important;border-radius:0!important;height:63px!important;left:0;right:0;position:fixed;background:#f5f5f5;border-bottom:1px solid #dce4ec;top:0}[search-container][opened] [resultados-container]{left:0;margin-left:0!important;width:100%!important;min-width:300px!important}[search-container][opened] .fa-search{display:none}[search-container][opened] .icon-sobre{right:20px;position:fixed;color:#004bcb}}[search-container]{display:block;border-radius:0;z-index:1099;-webkit-user-select:none;right:170px;top:0}[search-container] input::-webkit-input-placeholder{color:#004bcb}[search-container] input:-moz-placeholder{color:#004bcb}[search-container] input::-moz-placeholder{color:#004bcb}[search-container] input:-ms-input-placeholder{color:#004bcb}[search-container] input{font-size:13px;color:#004bcb;padding:0 10px 0 40px;background:#ecf2fe;border-radius:5px;box-sizing:border-box;line-height:34px;font-weight:400;outline:0;border:none;border-bottom:2px solid #004bcb;width:250px;margin-bottom:3px;height:45px;-webkit-transition:.1s ease-in-out,none,.15s linear;transition:.1s ease-in-out,none,.15s linear}[search-container] i[search-icon]{-webkit-transition:.1s ease-in-out,none;transition:.1s ease-in-out,none;width:40px;height:40px;line-height:40px;right:0;position:absolute;top:0;z-index:2;font-size:15px;color:#004bcb;text-align:center}[search-container] .btn-back{right:-40px;top:5px}[search-container] button[clear-button]{top:7px;height:56px;width:39px;line-height:54px;position:absolute;right:0;z-index:3;border:none;visibility:hidden;background:0 0!important}[search-container] button[clear-button]:hover{box-shadow:inset 0 -56px 0 #003795}[search-container] button[clear-button] i{font-size:15px;font-weight:100}[search-container] [resultados-container]{visibility:hidden;display:none;height:0;position:fixed;padding-bottom:0}[search-container] div,[search-container] ul{padding:0 10px 1px;margin:0;list-style:none}[search-container] div div,[search-container] div li,[search-container] ul div,[search-container] ul li{line-height:1}[search-container] div div:not([header]):first-child::before,[search-container] div div[header]+div::before,[search-container] div div[header]+li::before,[search-container] div li:not([header]):first-child::before,[search-container] div li[header]+div::before,[search-container] div li[header]+li::before,[search-container] ul div:not([header]):first-child::before,[search-container] ul div[header]+div::before,[search-container] ul div[header]+li::before,[search-container] ul li:not([header]):first-child::before,[search-container] ul li[header]+div::before,[search-container] ul li[header]+li::before{height:15px!important;top:0}[search-container] div div i,[search-container] div li i,[search-container] ul div i,[search-container] ul li i{color:#aaa;margin-left:-4px}[search-container] div div a[resultado],[search-container] div li a[resultado],[search-container] ul div a[resultado],[search-container] ul li a[resultado]{padding-left:22px;padding-right:22px;display:block;text-decoration:none}[search-container] div div:not([header]) a,[search-container] div li:not([header]) a,[search-container] ul div:not([header]) a,[search-container] ul li:not([header]) a{display:block}[search-container] div div:not([header])::before,[search-container] div li:not([header])::before,[search-container] ul div:not([header])::before,[search-container] ul li:not([header])::before{clear:both;content:'';border-left:1px dotted #788896;border-bottom:1px dotted #788896;display:block;float:left;margin:0 5px;height:25px;width:15px;position:relative;top:-11px}[search-container] div div:not([header]) span,[search-container] div li:not([header]) span,[search-container] ul div:not([header]) span,[search-container] ul li:not([header]) span{display:block;text-decoration:none;line-height:24px;color:#fff;outline:0;font-size:14px!important;cursor:pointer;-ms-user-select:none;-moz-user-select:none;-webkit-user-select:none;font-weight:400;padding:0 10px 0 1px;-webkit-transition:.1s ease-in-out;transition:.1s ease-in-out}[search-container] div div:not([header]) span strong,[search-container] div li:not([header]) span strong,[search-container] ul div:not([header]) span strong,[search-container] ul li:not([header]) span strong{font-size:14px!important;text-decoration:underline;font-weight:400!important}[search-container] div div[header],[search-container] div li[header],[search-container] ul div[header],[search-container] ul li[header]{margin-top:8px}[search-container] div div[header]>[group-header],[search-container] div li[header]>[group-header],[search-container] ul div[header]>[group-header],[search-container] ul li[header]>[group-header]{padding-left:1px;padding-bottom:1px}[search-container] div div[header]>[group-header] span,[search-container] div li[header]>[group-header] span,[search-container] ul div[header]>[group-header] span,[search-container] ul li[header]>[group-header] span{list-style-type:none;outline:0;padding:0;text-transform:uppercase;cursor:pointer;-ms-user-select:none;-moz-user-select:none;-webkit-user-select:none;font-weight:500;font-size:13px!important;padding-left:10px!important;color:rgba(255,255,255,.5);margin-top:-4px}[search-container] div div[header]>[group-header] span.zmdi,[search-container] div li[header]>[group-header] span.zmdi,[search-container] ul div[header]>[group-header] span.zmdi,[search-container] ul li[header]>[group-header] span.zmdi{position:relative;left:-3px}[search-container] div div[header]>[group-header] .first-letter,[search-container] div li[header]>[group-header] .first-letter,[search-container] ul div[header]>[group-header] .first-letter,[search-container] ul li[header]>[group-header] .first-letter{color:rgba(128,128,128,.5);width:23px;height:23px;padding-top:7px;display:block;text-align:center;float:left;-webkit-transition:.1s linear,none;transition:.1s linear,none;z-index:2;position:relative;font-style:normal;font-weight:700;margin-left:-14px;font-size:13px;background:#002e7c}[search-container] div div[header]>[group-header] .first-letter:before,[search-container] div li[header]>[group-header] .first-letter:before,[search-container] ul div[header]>[group-header] .first-letter:before,[search-container] ul li[header]>[group-header] .first-letter:before{content:attr(data-first);position:absolute;margin-left:-5px;margin-top:-3px}[search-container] div div [aviso],[search-container] div li [aviso],[search-container] ul div [aviso],[search-container] ul li [aviso]{font-size:14px!important;padding:7px 0}[search-container][opened]{margin-top:0}[search-container][opened] button[clear-button]{top:2px!important;visibility:visible}[search-container][opened] button[clear-button] i{color:#002e7c}[search-container][opened] [resultados-container]{background:#fff;border-right:1px solid #dce4ec;border-left:1px solid #dce4ec;border-bottom:1px solid #dce4ec;margin-top:5px;margin-left:40px;min-width:600px;visibility:visible;display:block;overflow:auto;height:auto;max-height:calc(100vh - 250px)}[search-container][opened] input{color:#004bcb;padding:15px 15px 15px 40px;width:600px;height:45px;line-height:34px;top:0;margin-top:0;margin-bottom:3px}[search-container][opened] input::-webkit-input-placeholder{color:#004bcb}[search-container][opened] input:-moz-placeholder{color:#004bcb}[search-container][opened] input::-moz-placeholder{color:#004bcb}[search-container][opened] input:-ms-input-placeholder{color:#004bcb}[search-container][opened] i.icon-search{top:-15px;height:67px;line-height:70px;color:#007bff}[search-container][opened] i.icon-search:hover{background:0 0}[search-container][opened] .icon-sobre{margin-left:-40px}@media all and (max-width:1024px){.is-horizontal-menu [search-container]{margin-left:6px;right:10px!important}.is-horizontal-menu [search-container] i{border:none!important;width:30px!important;font-size:13px!important}.is-horizontal-menu [search-container] input{border:none;background:#ecf2fe;width:30px;height:30px;text-align:center;border-radius:20px;position:relative;vertical-align:middle;line-height:30px;padding:0 30px 0 0;margin:3px 0 0;cursor:pointer}.is-horizontal-menu [search-container][opened]{width:auto}.is-horizontal-menu [search-container][opened] button[clear-button]{width:29px;padding:0}.is-horizontal-menu [search-container][opened] button[clear-button] i{top:-2px!important;position:relative;right:1px}.is-horizontal-menu [search-container][opened] input{width:600px!important;border-radius:0;text-align:left;padding-left:15px;padding-right:35px;color:#004bcb;cursor:auto!important}.is-horizontal-menu [search-container][opened] input::-webkit-input-placeholder{color:#004bcb}.is-horizontal-menu [search-container][opened] input:-moz-placeholder{color:#004bcb}.is-horizontal-menu [search-container][opened] input::-moz-placeholder{color:#004bcb}.is-horizontal-menu [search-container][opened] input:-ms-input-placeholder{color:#004bcb}}.btn-primary,[search-container] button[clear-button]{color:#fff;background-color:#007bff;border-color:#007bff}.btn-primary:hover,[search-container] button:hover[clear-button]{color:#fff;background-color:#0069d9;border-color:#0062cc}.btn-primary.focus,.btn-primary:focus,[search-container] button.focus[clear-button],[search-container] button:focus[clear-button]{box-shadow:0 0 0 .2rem rgba(0,123,255,.5)}.btn-primary.disabled,.btn-primary:disabled,[search-container] button.disabled[clear-button],[search-container] button:disabled[clear-button]{background-color:#007bff;border-color:#007bff}.btn-primary:not([disabled]):not(.disabled).active,.btn-primary:not([disabled]):not(.disabled):active,.show>.btn-primary.dropdown-toggle,[search-container] .show>button.dropdown-toggle[clear-button],[search-container] button:not([disabled]):not(.disabled).active[clear-button],[search-container] button:not([disabled]):not(.disabled):active[clear-button]{color:#fff;background-color:#0062cc;border-color:#005cbf;box-shadow:0 0 0 .2rem rgba(0,123,255,.5)}"]
            }] }
];
/** @nocollapse */
SearchComponent.ctorParameters = () => [
    { type: FormBuilder },
    { type: SearchService },
    { type: HotkeysService },
    { type: Router },
    { type: SearchHistoricoService },
    { type: Renderer2 }
];
SearchComponent.propDecorators = {
    grupoPesquisado: [{ type: Input }],
    adicionarGrupo: [{ type: Output }],
    removerGrupo: [{ type: Output }],
    placeholderText: [{ type: Input }],
    children: [{ type: ContentChildren, args: [MenuItemComponent, { descendants: false },] }],
    items: [{ type: ViewChildren, args: [HighlightComponent,] }],
    searchInput: [{ type: ViewChild, args: ['search',] }],
    searchOpen: [{ type: ViewChild, args: ['searchOpen',] }],
    buttonClose: [{ type: ViewChild, args: ['buttonClose',] }]
};
if (false) {
    /** @type {?} */
    SearchComponent.prototype.grupoPesquisado;
    /** @type {?} */
    SearchComponent.prototype.adicionarGrupo;
    /** @type {?} */
    SearchComponent.prototype.removerGrupo;
    /** @type {?} */
    SearchComponent.prototype.placeholderText;
    /** @type {?} */
    SearchComponent.prototype.children;
    /** @type {?} */
    SearchComponent.prototype.items;
    /**
     * @type {?}
     * @private
     */
    SearchComponent.prototype.keyManager;
    /** @type {?} */
    SearchComponent.prototype.stateForm;
    /** @type {?} */
    SearchComponent.prototype.searchOpened;
    /** @type {?} */
    SearchComponent.prototype.searchGroups;
    /** @type {?} */
    SearchComponent.prototype.subscription;
    /** @type {?} */
    SearchComponent.prototype.searchGroupOptions;
    /** @type {?} */
    SearchComponent.prototype.highlight;
    /** @type {?} */
    SearchComponent.prototype.openSearch;
    /** @type {?} */
    SearchComponent.prototype.itemPesquisado;
    /** @type {?} */
    SearchComponent.prototype.searchInput;
    /** @type {?} */
    SearchComponent.prototype.searchOpen;
    /** @type {?} */
    SearchComponent.prototype.buttonClose;
    /** @type {?} */
    SearchComponent.prototype.isAnimating;
    /**
     * @type {?}
     * @private
     */
    SearchComponent.prototype.fb;
    /**
     * @type {?}
     * @protected
     */
    SearchComponent.prototype.searchService;
    /** @type {?} */
    SearchComponent.prototype.hotkeysService;
    /**
     * @type {?}
     * @protected
     */
    SearchComponent.prototype.router;
    /**
     * @type {?}
     * @private
     */
    SearchComponent.prototype.searchHistorico;
    /**
     * @type {?}
     * @protected
     */
    SearchComponent.prototype.renderer;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvbGF5b3V0L2hlYWRlci9zZWFyY2gvc2VhcmNoLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUVMLHVCQUF1QixFQUN2QixTQUFTLEVBQ1QsZUFBZSxFQUNmLFlBQVksRUFDWixLQUFLLEVBRUwsTUFBTSxFQUNOLFNBQVMsRUFDVCxTQUFTLEVBQ1QsWUFBWSxFQUNaLFNBQVMsRUFFVixNQUFNLGVBQWUsQ0FBQztBQUN2QixPQUFPLEVBQUMsV0FBVyxFQUFZLE1BQU0sZ0JBQWdCLENBQUM7QUFDdEQsT0FBTyxFQUFDLGlCQUFpQixFQUFDLE1BQU0sOENBQThDLENBQUM7QUFDL0UsT0FBTyxFQUFDLGFBQWEsRUFBQyxNQUFNLGtCQUFrQixDQUFDO0FBQy9DLE9BQU8sRUFBQyxNQUFNLEVBQUUsY0FBYyxFQUFDLE1BQU0sa0JBQWtCLENBQUM7QUFDeEQsT0FBTyxFQUFDLFFBQVEsRUFBRSxTQUFTLEVBQUMsTUFBTSxtQkFBbUIsQ0FBQztBQUN0RCxPQUFPLEVBQUMsTUFBTSxFQUFDLE1BQU0saUJBQWlCLENBQUM7QUFFdkMsT0FBTyxFQUFDLGVBQWUsRUFBQyxNQUFNLG9CQUFvQixDQUFDO0FBQ25ELE9BQU8sRUFBQyxzQkFBc0IsRUFBQyxNQUFNLDRCQUE0QixDQUFDO0FBQ2xFLE9BQU8sS0FBSyxDQUFDLE1BQU0sUUFBUSxDQUFDO0FBQzVCLE9BQU8sRUFBQyxJQUFJLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUNyQyxPQUFPLEVBQUMsS0FBSyxFQUFFLFlBQVksRUFBQyxNQUFNLE1BQU0sQ0FBQztBQUN6QyxPQUFPLEVBQUMsMEJBQTBCLEVBQUMsTUFBTSxtQkFBbUIsQ0FBQztBQUM3RCxPQUFPLEVBQUMsS0FBSyxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUUsVUFBVSxFQUFDLE1BQU0sdUJBQXVCLENBQUM7QUFDMUUsT0FBTyxFQUFDLGtCQUFrQixFQUFDLE1BQU0seUNBQXlDLENBQUM7QUFTM0UsTUFBTSxPQUFPLGVBQWU7Ozs7Ozs7OztJQWtDMUIsWUFDVSxFQUFlLEVBQ2IsYUFBNEIsRUFDL0IsY0FBOEIsRUFDM0IsTUFBYyxFQUNoQixlQUF1QyxFQUNyQyxRQUFtQjtRQUxyQixPQUFFLEdBQUYsRUFBRSxDQUFhO1FBQ2Isa0JBQWEsR0FBYixhQUFhLENBQWU7UUFDL0IsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzNCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDaEIsb0JBQWUsR0FBZixlQUFlLENBQXdCO1FBQ3JDLGFBQVEsR0FBUixRQUFRLENBQVc7UUF0Q3JCLG1CQUFjLEdBQUcsSUFBSSxZQUFZLEVBQWlCLENBQUM7UUFDbkQsaUJBQVksR0FBRyxJQUFJLFlBQVksRUFBaUIsQ0FBQztRQUczRCxvQkFBZSxHQUFHLDJCQUEyQixDQUFDO1FBTTlDLGNBQVMsR0FBYyxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztZQUNuQyxZQUFZLEVBQUUsRUFBRTtTQUNqQixDQUFDLENBQUM7UUFHSCxpQkFBWSxHQUFvQixFQUFFLENBQUM7UUFDbkMsaUJBQVksR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBSWxDLGVBQVUsR0FBRyxLQUFLLENBQUM7UUFNWixnQkFBVyxHQUFHLEtBQUssQ0FBQztJQWMzQixDQUFDOzs7O0lBWkQsSUFBSSxVQUFVO1FBQ1osT0FBTyxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLENBQUM7SUFDM0QsQ0FBQzs7OztJQVlELGVBQWU7UUFFYixJQUFJLFFBQVEsQ0FBQyxJQUFJLENBQUMscUJBQXFCLEVBQUUsQ0FBQyxLQUFLLElBQUksR0FBRyxFQUFFO1lBQ3RELElBQUksQ0FBQyxlQUFlLEdBQUcsVUFBVSxDQUFDO1NBQ25DO1FBRUQsSUFBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsSUFBSSxNQUFNLENBQUMsWUFBWTs7OztRQUFFLENBQUMsS0FBb0IsRUFBVyxFQUFFO1lBQ2pGLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDeEIsT0FBTyxLQUFLLENBQUMsQ0FBQyxtQkFBbUI7UUFDbkMsQ0FBQyxFQUFDLENBQUMsQ0FBQztRQUVKLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxTQUFTOzs7UUFBQyxHQUFHLEVBQUU7WUFDekIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUMxQixDQUFDLEVBQUMsQ0FBQztRQUVILElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSwwQkFBMEIsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDeEYsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLFNBQVMsRUFBRSxNQUFNLENBQUMsQ0FBQztJQUN4RixDQUFDOzs7O0lBRUQsUUFBUTtRQUNOLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxTQUFTOzs7O1FBQUMsS0FBSyxDQUFDLEVBQUU7WUFDaEUsSUFBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7WUFDNUIsSUFBSSxDQUFDLGtCQUFrQixHQUFHLEVBQUUsQ0FBQztZQUM3QixJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUM7aUJBQzdCLFNBQVM7Ozs7WUFBQyxDQUFDLGlCQUFpQixFQUFFLEVBQUU7Z0JBQzdCLElBQUksaUJBQWlCLElBQUksaUJBQWlCLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtvQkFDckQsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUI7Ozs7b0JBQUUsQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUMsQ0FBQztpQkFDeEU7cUJBQU07b0JBQ0wsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FDMUIsbUJBQUE7d0JBQ0UsRUFBRSxFQUFFLElBQUksRUFBRTt3QkFDVixPQUFPLEVBQUUsSUFBSTt3QkFDYixLQUFLLEVBQUUsb0JBQW9CLEtBQUssR0FBRztxQkFDcEMsRUFBaUIsQ0FDbkIsQ0FBQztpQkFDSDtZQUNILENBQUMsRUFDRixDQUFDO1FBQ04sQ0FBQyxFQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxjQUFjLEVBQUUsQ0FBQztJQUNsRSxDQUFDOzs7Ozs7SUFFRCxZQUFZLENBQUMsS0FBVSxFQUFFLGVBQW9CO1FBQzNDLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWTtlQUNqQixLQUFLO2VBQ0wsS0FBSyxDQUFDLElBQUksS0FBSyxNQUFNO2VBQ3JCLEtBQUssQ0FBQyxHQUFHLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUN6QixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztZQUN6QixJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO1NBQzVCO0lBQ0gsQ0FBQzs7Ozs7SUFFRCxNQUFNLENBQUMsZUFBb0I7UUFDekIsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLFNBQVM7OztRQUFDLEdBQUcsRUFBRTtZQUM5QyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLEVBQUU7Z0JBQzVCLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDYixJQUFJLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxlQUFlLEVBQUUsUUFBUSxDQUFDLENBQUM7Z0JBQ3pELElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxTQUFTLEVBQUUsTUFBTSxDQUFDLENBQUM7YUFDdkY7UUFDSCxDQUFDLEVBQUMsQ0FBQyxDQUFDO0lBQ04sQ0FBQzs7OztJQUVELGFBQWE7UUFDWCxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLEVBQUU7WUFDNUIsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO1NBQ2Q7SUFDSCxDQUFDOzs7OztJQUVELElBQUksQ0FBQyxlQUFvQjtRQUN2QixJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLGVBQWUsRUFBRSxRQUFRLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDMUQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLFNBQVMsQ0FBQyxDQUFDO0lBQ25GLENBQUM7Ozs7SUFFRCxLQUFLO1FBQ0gsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7UUFDMUIsQ0FBQyxtQkFBQSxRQUFRLENBQUMsYUFBYSxFQUFlLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUMvQyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ3BELENBQUM7Ozs7SUFFRCxXQUFXO1FBQ1QsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUNsQyxDQUFDOzs7OztJQUVELE1BQU0sQ0FBQyxlQUFvQjtRQUN6QixJQUFJLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDekMsSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFO1lBQ3JCLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7U0FDNUI7UUFDRCxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ3BELENBQUM7Ozs7OztJQUVELGVBQWUsQ0FBQyxJQUFrQixFQUFFLEtBQXFDO1FBQ3ZFLElBQUksQ0FBQyxlQUFlLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzVDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDckMsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ3ZCLENBQUM7Ozs7SUFFRCxJQUFJLFVBQVU7UUFDWixPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7Ozs7O0lBRUQsT0FBTyxDQUFDLEtBQVU7UUFDaEIsUUFBUSxLQUFLLENBQUMsT0FBTyxFQUFFO1lBQ3JCLEtBQUssS0FBSzs7c0JBQ0YsSUFBSSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLElBQUk7Z0JBQzVDLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO2dCQUNqQyxNQUFNO1lBQ1IsS0FBSyxNQUFNO2dCQUNULElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDYixNQUFNO1lBQ1IsS0FBSyxRQUFRLENBQUM7WUFDZCxLQUFLLFVBQVU7Z0JBQ2IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ2pDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxPQUFPLEtBQUssUUFBUSxDQUFDLENBQUM7Z0JBQzdFLE1BQU07WUFDUjtnQkFDRSxJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQixFQUFFLENBQUM7Z0JBQ3JDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO2dCQUN4RCxNQUFNO1NBQ1Q7SUFDSCxDQUFDOzs7Ozs7SUFFRCxVQUFVLENBQUMsSUFBa0MsRUFBRSxFQUFXO1FBQ3hELElBQUksbUJBQUEsSUFBSSxFQUFpQixJQUFJLENBQUMsbUJBQUEsSUFBSSxFQUFpQixDQUFDLENBQUMsS0FBSyxJQUFJLENBQUMsbUJBQUEsSUFBSSxFQUFpQixDQUFDLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDdEcsSUFBSSxFQUFFLEVBQUU7Z0JBQ04sSUFBSSxDQUFDLFVBQVUsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO2FBQ3pDO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxVQUFVLENBQUMsaUJBQWlCLEVBQUUsQ0FBQzthQUNyQztZQUNELElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1NBQ3REO0lBQ0gsQ0FBQzs7Ozs7SUFFRCxlQUFlLENBQUMsSUFBUztRQUN2QixJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN0QyxDQUFDOzs7WUE1TEYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxjQUFjO2dCQUN4QixnbEZBQXNDO2dCQUV0QyxVQUFVLEVBQUUsQ0FBQyxlQUFlLEVBQUUsQ0FBQztnQkFDL0IsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07O2FBQ2hEOzs7O1lBdEJPLFdBQVc7WUFFWCxhQUFhO1lBQ0wsY0FBYztZQUV0QixNQUFNO1lBR04sc0JBQXNCO1lBWDVCLFNBQVM7Ozs4QkEyQlIsS0FBSzs2QkFDTCxNQUFNOzJCQUNOLE1BQU07OEJBRU4sS0FBSzt1QkFHTCxlQUFlLFNBQUMsaUJBQWlCLEVBQUUsRUFBQyxXQUFXLEVBQUUsS0FBSyxFQUFDO29CQUN2RCxZQUFZLFNBQUMsa0JBQWtCOzBCQWUvQixTQUFTLFNBQUMsUUFBUTt5QkFDbEIsU0FBUyxTQUFDLFlBQVk7MEJBQ3RCLFNBQVMsU0FBQyxhQUFhOzs7O0lBekJ4QiwwQ0FBd0M7O0lBQ3hDLHlDQUE2RDs7SUFDN0QsdUNBQTJEOztJQUUzRCwwQ0FDOEM7O0lBRTlDLG1DQUFpRzs7SUFDakcsZ0NBQXVFOzs7OztJQUN2RSxxQ0FBbUU7O0lBRW5FLG9DQUVHOztJQUVILHVDQUFzQjs7SUFDdEIsdUNBQW1DOztJQUNuQyx1Q0FBa0M7O0lBRWxDLDZDQUFvQzs7SUFDcEMsb0NBQWtCOztJQUNsQixxQ0FBbUI7O0lBQ25CLHlDQUF1Qjs7SUFDdkIsc0NBQTJDOztJQUMzQyxxQ0FBOEM7O0lBQzlDLHNDQUFpRDs7SUFFakQsc0NBQTJCOzs7OztJQU96Qiw2QkFBdUI7Ozs7O0lBQ3ZCLHdDQUFzQzs7SUFDdEMseUNBQXFDOzs7OztJQUNyQyxpQ0FBd0I7Ozs7O0lBQ3hCLDBDQUErQzs7Ozs7SUFDL0MsbUNBQTZCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcbiAgQWZ0ZXJWaWV3SW5pdCxcbiAgQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3ksXG4gIENvbXBvbmVudCxcbiAgQ29udGVudENoaWxkcmVuLFxuICBFdmVudEVtaXR0ZXIsXG4gIElucHV0LFxuICBPbkluaXQsXG4gIE91dHB1dCxcbiAgUXVlcnlMaXN0LFxuICBWaWV3Q2hpbGQsXG4gIFZpZXdDaGlsZHJlbixcbiAgUmVuZGVyZXIyLFxuICBPbkRlc3Ryb3lcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0Zvcm1CdWlsZGVyLCBGb3JtR3JvdXB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7TWVudUl0ZW1Db21wb25lbnR9IGZyb20gJy4uLy4uL25hdi9tZW51L21lbnUtaXRlbS9tZW51LWl0ZW0uY29tcG9uZW50JztcbmltcG9ydCB7U2VhcmNoU2VydmljZX0gZnJvbSAnLi9zZWFyY2guc2VydmljZSc7XG5pbXBvcnQge0hvdGtleSwgSG90a2V5c1NlcnZpY2V9IGZyb20gJ2FuZ3VsYXIyLWhvdGtleXMnO1xuaW1wb3J0IHtNYXRJbnB1dCwgTWF0QnV0dG9ufSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XG5pbXBvcnQge1JvdXRlcn0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7R3J1cG9QZXNxdWlzYSwgSXRlbVBlc3F1aXNhfSBmcm9tICcuL3NlYXJjaC5tb2RlbCc7XG5pbXBvcnQge3NlYXJjaEFuaW1hdGlvbn0gZnJvbSAnLi9zbGlkZS1hbmltYXRpb25zJztcbmltcG9ydCB7U2VhcmNoSGlzdG9yaWNvU2VydmljZX0gZnJvbSAnLi9zZWFyY2gtaGlzdG9yaWNvLnNlcnZpY2UnO1xuaW1wb3J0ICogYXMgXyBmcm9tICdsb2Rhc2gnO1xuaW1wb3J0IHtndWlkfSBmcm9tICdAZGF0b3JhbWEvYWtpdGEnO1xuaW1wb3J0IHt0aW1lciwgU3Vic2NyaXB0aW9ufSBmcm9tICdyeGpzJztcbmltcG9ydCB7QWN0aXZlRGVzY2VuZGFudEtleU1hbmFnZXJ9IGZyb20gJ0Bhbmd1bGFyL2Nkay9hMTF5JztcbmltcG9ydCB7RU5URVIsIEVTQ0FQRSwgVVBfQVJST1csIERPV05fQVJST1d9IGZyb20gJ0Bhbmd1bGFyL2Nkay9rZXljb2Rlcyc7XG5pbXBvcnQge0hpZ2hsaWdodENvbXBvbmVudH0gZnJvbSAnLi4vLi4vbmF2L2hpZ2hsaWdodC9oaWdobGlnaHQuY29tcG9uZW50JztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAndWlraXQtc2VhcmNoJyxcbiAgdGVtcGxhdGVVcmw6ICcuL3NlYXJjaC5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL3NlYXJjaC5jb21wb25lbnQuc2NzcyddLFxuICBhbmltYXRpb25zOiBbc2VhcmNoQW5pbWF0aW9uKCldLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaFxufSlcbmV4cG9ydCBjbGFzcyBTZWFyY2hDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIEFmdGVyVmlld0luaXQsIE9uRGVzdHJveSB7XG4gIEBJbnB1dCgpIGdydXBvUGVzcXVpc2FkbzogR3J1cG9QZXNxdWlzYTtcbiAgQE91dHB1dCgpIGFkaWNpb25hckdydXBvID0gbmV3IEV2ZW50RW1pdHRlcjxHcnVwb1Blc3F1aXNhPigpO1xuICBAT3V0cHV0KCkgcmVtb3ZlckdydXBvID0gbmV3IEV2ZW50RW1pdHRlcjxHcnVwb1Blc3F1aXNhPigpO1xuXG4gIEBJbnB1dCgpXG4gIHBsYWNlaG9sZGVyVGV4dCA9ICdQZXNxdWlzYSAoQ3RybCArIEFsdCArIDMpJztcblxuICBAQ29udGVudENoaWxkcmVuKE1lbnVJdGVtQ29tcG9uZW50LCB7ZGVzY2VuZGFudHM6IGZhbHNlfSkgY2hpbGRyZW46IFF1ZXJ5TGlzdDxNZW51SXRlbUNvbXBvbmVudD47XG4gIEBWaWV3Q2hpbGRyZW4oSGlnaGxpZ2h0Q29tcG9uZW50KSBpdGVtczogUXVlcnlMaXN0PEhpZ2hsaWdodENvbXBvbmVudD47XG4gIHByaXZhdGUga2V5TWFuYWdlcjogQWN0aXZlRGVzY2VuZGFudEtleU1hbmFnZXI8SGlnaGxpZ2h0Q29tcG9uZW50PjtcblxuICBzdGF0ZUZvcm06IEZvcm1Hcm91cCA9IHRoaXMuZmIuZ3JvdXAoe1xuICAgIHNlYXJjaEdyb3VwczogJycsXG4gIH0pO1xuXG4gIHNlYXJjaE9wZW5lZDogYm9vbGVhbjtcbiAgc2VhcmNoR3JvdXBzOiBHcnVwb1Blc3F1aXNhW10gPSBbXTtcbiAgc3Vic2NyaXB0aW9uID0gbmV3IFN1YnNjcmlwdGlvbigpO1xuXG4gIHNlYXJjaEdyb3VwT3B0aW9uczogR3J1cG9QZXNxdWlzYVtdO1xuICBoaWdobGlnaHQ6IHN0cmluZztcbiAgb3BlblNlYXJjaCA9IGZhbHNlO1xuICBpdGVtUGVzcXVpc2Fkbzogc3RyaW5nO1xuICBAVmlld0NoaWxkKCdzZWFyY2gnKSBzZWFyY2hJbnB1dDogTWF0SW5wdXQ7XG4gIEBWaWV3Q2hpbGQoJ3NlYXJjaE9wZW4nKSBzZWFyY2hPcGVuOiBNYXRJbnB1dDtcbiAgQFZpZXdDaGlsZCgnYnV0dG9uQ2xvc2UnKSBidXR0b25DbG9zZTogTWF0QnV0dG9uO1xuXG4gIHB1YmxpYyBpc0FuaW1hdGluZyA9IGZhbHNlO1xuXG4gIGdldCBoYXNSZXN1bHRzKCkge1xuICAgIHJldHVybiB0aGlzLm9wZW5TZWFyY2ggJiYgdGhpcy5zZWFyY2hHcm91cE9wdGlvbnMubGVuZ3RoO1xuICB9XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSBmYjogRm9ybUJ1aWxkZXIsXG4gICAgcHJvdGVjdGVkIHNlYXJjaFNlcnZpY2U6IFNlYXJjaFNlcnZpY2UsXG4gICAgcHVibGljIGhvdGtleXNTZXJ2aWNlOiBIb3RrZXlzU2VydmljZSxcbiAgICBwcm90ZWN0ZWQgcm91dGVyOiBSb3V0ZXIsXG4gICAgcHJpdmF0ZSBzZWFyY2hIaXN0b3JpY286IFNlYXJjaEhpc3Rvcmljb1NlcnZpY2UsXG4gICAgcHJvdGVjdGVkIHJlbmRlcmVyOiBSZW5kZXJlcjIsXG4gICkge1xuICB9XG5cbiAgbmdBZnRlclZpZXdJbml0KCk6IHZvaWQge1xuXG4gICAgaWYgKGRvY3VtZW50LmJvZHkuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkud2lkdGggPD0gOTcwKSB7XG4gICAgICB0aGlzLnBsYWNlaG9sZGVyVGV4dCA9ICdQZXNxdWlzYSc7XG4gICAgfVxuXG4gICAgdGhpcy5ob3RrZXlzU2VydmljZS5hZGQobmV3IEhvdGtleSgnY3RybCthbHQrMycsIChldmVudDogS2V5Ym9hcmRFdmVudCk6IGJvb2xlYW4gPT4ge1xuICAgICAgdGhpcy5zZWFyY2hPcGVuLmZvY3VzKCk7XG4gICAgICByZXR1cm4gZmFsc2U7IC8vIFByZXZlbnQgYnViYmxpbmdcbiAgICB9KSk7XG5cbiAgICB0aW1lcigxMDAwKS5zdWJzY3JpYmUoKCkgPT4ge1xuICAgICAgdGhpcy5zZWFyY2hPcGVuLmZvY3VzKCk7XG4gICAgfSk7XG5cbiAgICB0aGlzLmtleU1hbmFnZXIgPSBuZXcgQWN0aXZlRGVzY2VuZGFudEtleU1hbmFnZXIodGhpcy5pdGVtcykud2l0aFdyYXAoKS53aXRoVHlwZUFoZWFkKCk7XG4gICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0aGlzLmJ1dHRvbkNsb3NlLl9lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQsICdkaXNwbGF5JywgJ25vbmUnKTtcbiAgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIHRoaXMuc3RhdGVGb3JtLmdldCgnc2VhcmNoR3JvdXBzJykudmFsdWVDaGFuZ2VzLnN1YnNjcmliZSh2YWx1ZSA9PiB7XG4gICAgICB0aGlzLml0ZW1QZXNxdWlzYWRvID0gdmFsdWU7XG4gICAgICB0aGlzLnNlYXJjaEdyb3VwT3B0aW9ucyA9IFtdO1xuICAgICAgdGhpcy5zZWFyY2hTZXJ2aWNlLmJ1c2Nhcih2YWx1ZSlcbiAgICAgICAgLnN1YnNjcmliZSgoaXRlbnNNZW51UGVzcXVpc2EpID0+IHtcbiAgICAgICAgICAgIGlmIChpdGVuc01lbnVQZXNxdWlzYSAmJiBpdGVuc01lbnVQZXNxdWlzYS5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgIF8ubWFwKGl0ZW5zTWVudVBlc3F1aXNhLCAoaXRlbSkgPT4gdGhpcy5zZWFyY2hHcm91cE9wdGlvbnMucHVzaChpdGVtKSk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICB0aGlzLnNlYXJjaEdyb3VwT3B0aW9ucy5wdXNoKFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgIGlkOiBndWlkKCksXG4gICAgICAgICAgICAgICAgICBpc0F2aXNvOiB0cnVlLFxuICAgICAgICAgICAgICAgICAgdGl0bGU6IGBOw6NvIGVuY29udHJhbW9zICcke3ZhbHVlfSdgXG4gICAgICAgICAgICAgICAgfSBhcyBHcnVwb1Blc3F1aXNhXG4gICAgICAgICAgICAgICk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICApO1xuICAgIH0pO1xuXG4gICAgdGhpcy5zZWFyY2hHcm91cE9wdGlvbnMgPSB0aGlzLnNlYXJjaEhpc3Rvcmljby5vYnRlckhpc3RvcmljbygpO1xuICB9XG5cbiAgb3BlblRvU2VhcmNoKGV2ZW50OiBhbnksIHNlYXJjaENvbnRhaW5lcjogYW55KTogdm9pZCB7XG4gICAgaWYgKCF0aGlzLnNlYXJjaE9wZW5lZFxuICAgICAgJiYgZXZlbnRcbiAgICAgICYmIGV2ZW50LmNvZGUgPT09ICdLZXlBJ1xuICAgICAgJiYgZXZlbnQua2V5Lmxlbmd0aCA+IDApIHtcbiAgICAgIHRoaXMuc2VhcmNoT3BlbmVkID0gdHJ1ZTtcbiAgICAgIHRoaXMub3BlbihzZWFyY2hDb250YWluZXIpO1xuICAgIH1cbiAgfVxuXG4gIG9uQmx1cihzZWFyY2hDb250YWluZXI6IGFueSkge1xuICAgIHRoaXMuc3Vic2NyaXB0aW9uLmFkZCh0aW1lcigxNTApLnN1YnNjcmliZSgoKSA9PiB7XG4gICAgICBpZiAoIXRoaXMuc2VhcmNoT3Blbi5mb2N1c2VkKSB7XG4gICAgICAgIHRoaXMuY2xvc2UoKTtcbiAgICAgICAgdGhpcy5yZW5kZXJlci5yZW1vdmVBdHRyaWJ1dGUoc2VhcmNoQ29udGFpbmVyLCAnb3BlbmVkJyk7XG4gICAgICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUodGhpcy5idXR0b25DbG9zZS5fZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LCAnZGlzcGxheScsICdub25lJyk7XG4gICAgICB9XG4gICAgfSkpO1xuICB9XG5cbiAgY2xvc2VUb1NlYXJjaCgpOiB2b2lkIHtcbiAgICBpZiAoIXRoaXMuc2VhcmNoT3Blbi5mb2N1c2VkKSB7XG4gICAgICB0aGlzLmNsb3NlKCk7XG4gICAgfVxuICB9XG5cbiAgb3BlbihzZWFyY2hDb250YWluZXI6IGFueSk6IHZvaWQge1xuICAgIHRoaXMuc2VhcmNoT3Blbi5mb2N1cygpO1xuICAgIHRoaXMucmVuZGVyZXIuc2V0QXR0cmlidXRlKHNlYXJjaENvbnRhaW5lciwgJ29wZW5lZCcsICcnKTtcbiAgICB0aGlzLnJlbmRlcmVyLnJlbW92ZVN0eWxlKHRoaXMuYnV0dG9uQ2xvc2UuX2VsZW1lbnRSZWYubmF0aXZlRWxlbWVudCwgJ2Rpc3BsYXknKTtcbiAgfVxuXG4gIGNsb3NlKCk6IHZvaWQge1xuICAgIHRoaXMuc2VhcmNoT3BlbmVkID0gZmFsc2U7XG4gICAgKGRvY3VtZW50LmFjdGl2ZUVsZW1lbnQgYXMgSFRNTEVsZW1lbnQpLmJsdXIoKTtcbiAgICB0aGlzLnN0YXRlRm9ybS5jb250cm9scy5zZWFyY2hHcm91cHMuc2V0VmFsdWUoJycpO1xuICB9XG5cbiAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XG4gICAgdGhpcy5zdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcbiAgfVxuXG4gIHRvZ2dsZShzZWFyY2hDb250YWluZXI6IGFueSk6IHZvaWQge1xuICAgIHRoaXMuc2VhcmNoT3BlbmVkID0gKCF0aGlzLnNlYXJjaE9wZW5lZCk7XG4gICAgaWYgKHRoaXMuc2VhcmNoT3BlbmVkKSB7XG4gICAgICB0aGlzLm9wZW4oc2VhcmNoQ29udGFpbmVyKTtcbiAgICB9XG4gICAgdGhpcy5zdGF0ZUZvcm0uY29udHJvbHMuc2VhcmNoR3JvdXBzLnNldFZhbHVlKCcnKTtcbiAgfVxuXG4gIGdvVG9BZGRIaXN0b3JpYyhpdGVtOiBJdGVtUGVzcXVpc2EsIGdyb3VwOiAoSXRlbVBlc3F1aXNhIHwgR3J1cG9QZXNxdWlzYSkpOiB2b2lkIHtcbiAgICB0aGlzLnNlYXJjaEhpc3Rvcmljby5zYWx2YXJIaXN0b3JpY28oZ3JvdXApO1xuICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFtpdGVtLmxpbmtbMF1dKTtcbiAgICB0aGlzLmNsb3NlVG9TZWFyY2goKTtcbiAgfVxuXG4gIGdldCBoZWlnaHRTaXplKCk6IHN0cmluZyB7XG4gICAgcmV0dXJuIG51bGw7XG4gIH1cblxuICBvbktleXVwKGV2ZW50OiBhbnkpOiB2b2lkIHtcbiAgICBzd2l0Y2ggKGV2ZW50LmtleUNvZGUpIHtcbiAgICAgIGNhc2UgRU5URVI6XG4gICAgICAgIGNvbnN0IGl0ZW0gPSB0aGlzLmtleU1hbmFnZXIuYWN0aXZlSXRlbS5pdGVtO1xuICAgICAgICB0aGlzLmdvVG9BZGRIaXN0b3JpYyhpdGVtLCBpdGVtKTtcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlIEVTQ0FQRTpcbiAgICAgICAgdGhpcy5jbG9zZSgpO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgVVBfQVJST1c6XG4gICAgICBjYXNlIERPV05fQVJST1c6XG4gICAgICAgIHRoaXMua2V5TWFuYWdlci5vbktleWRvd24oZXZlbnQpO1xuICAgICAgICB0aGlzLmFjdGl2ZUl0ZW0odGhpcy5rZXlNYW5hZ2VyLmFjdGl2ZUl0ZW0uaXRlbSwgZXZlbnQua2V5Q29kZSA9PT0gVVBfQVJST1cpO1xuICAgICAgICBicmVhaztcbiAgICAgIGRlZmF1bHQ6XG4gICAgICAgIHRoaXMua2V5TWFuYWdlci5zZXRGaXJzdEl0ZW1BY3RpdmUoKTtcbiAgICAgICAgdGhpcy5hY3RpdmVJdGVtKHRoaXMua2V5TWFuYWdlci5hY3RpdmVJdGVtLml0ZW0sIGZhbHNlKTtcbiAgICAgICAgYnJlYWs7XG4gICAgfVxuICB9XG5cbiAgYWN0aXZlSXRlbShpdGVtOiBHcnVwb1Blc3F1aXNhIHwgSXRlbVBlc3F1aXNhLCB1cDogYm9vbGVhbik6IHZvaWQge1xuICAgIGlmIChpdGVtIGFzIEdydXBvUGVzcXVpc2EgJiYgKGl0ZW0gYXMgR3J1cG9QZXNxdWlzYSkuaXRlbXMgJiYgKGl0ZW0gYXMgR3J1cG9QZXNxdWlzYSkuaXRlbXMubGVuZ3RoID4gMCkge1xuICAgICAgaWYgKHVwKSB7XG4gICAgICAgIHRoaXMua2V5TWFuYWdlci5zZXRQcmV2aW91c0l0ZW1BY3RpdmUoKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMua2V5TWFuYWdlci5zZXROZXh0SXRlbUFjdGl2ZSgpO1xuICAgICAgfVxuICAgICAgdGhpcy5hY3RpdmVJdGVtKHRoaXMua2V5TWFuYWdlci5hY3RpdmVJdGVtLml0ZW0sIHVwKTtcbiAgICB9XG4gIH1cblxuICBvbk1vdXNlb3Zlckl0ZW0oaXRlbTogYW55KTogdm9pZCB7XG4gICAgdGhpcy5rZXlNYW5hZ2VyLnNldEFjdGl2ZUl0ZW0oaXRlbSk7XG4gIH1cbn1cbiJdfQ==