/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Inject, Injectable, Optional } from '@angular/core';
import { combineLatest, of } from 'rxjs';
import { SEARCH_TOKEN } from '../../../utils/abstract/search-abstract.service';
import * as i0 from "@angular/core";
import * as i1 from "../../../utils/abstract/search-abstract.service";
export class SearchService {
    /**
     * @param {?} services
     */
    constructor(services) {
        this.services = services;
    }
    /**
     * @param {?} term
     * @return {?}
     */
    buscar(term) {
        if (!this.services) {
            console.error('Não existe serviço de busca configurado! Ler documentação para mais informações, de como utilizar.');
            return of();
        }
        /** @type {?} */
        const searchObservables = this.services.map((/**
         * @param {?} searchService
         * @return {?}
         */
        searchService => searchService.searchGroup(term)));
        /** @type {?} */
        const combinedObservable = combineLatest(searchObservables);
        return combinedObservable;
    }
    /**
     * @param {?} group
     * @return {?}
     */
    adicionarGrupoPesquisa(group) {
        this.services.map((/**
         * @param {?} groupSearch
         * @return {?}
         */
        groupSearch => groupSearch.addGroup(group)));
    }
}
SearchService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
SearchService.ctorParameters = () => [
    { type: Array, decorators: [{ type: Optional }, { type: Inject, args: [SEARCH_TOKEN,] }] }
];
/** @nocollapse */ SearchService.ngInjectableDef = i0.defineInjectable({ factory: function SearchService_Factory() { return new SearchService(i0.inject(i1.SEARCH_TOKEN, 8)); }, token: SearchService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @protected
     */
    SearchService.prototype.services;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL2xheW91dC9oZWFkZXIvc2VhcmNoL3NlYXJjaC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDN0QsT0FBTyxFQUFFLGFBQWEsRUFBYyxFQUFFLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFFckQsT0FBTyxFQUFFLFlBQVksRUFBeUIsTUFBTSxpREFBaUQsQ0FBQzs7O0FBS3RHLE1BQU0sT0FBTyxhQUFhOzs7O0lBRXhCLFlBQXdELFFBQWlDO1FBQWpDLGFBQVEsR0FBUixRQUFRLENBQXlCO0lBRXpGLENBQUM7Ozs7O0lBRUQsTUFBTSxDQUFDLElBQVk7UUFFakIsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDbEIsT0FBTyxDQUFDLEtBQUssQ0FBQyxvR0FBb0csQ0FBQyxDQUFDO1lBQ3BILE9BQU8sRUFBRSxFQUFFLENBQUM7U0FDYjs7Y0FFSyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUc7Ozs7UUFBQyxhQUFhLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEVBQUM7O2NBQ3ZGLGtCQUFrQixHQUFHLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQztRQUMzRCxPQUFPLGtCQUFrQixDQUFDO0lBQzVCLENBQUM7Ozs7O0lBRUQsc0JBQXNCLENBQUMsS0FBc0I7UUFDM0MsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHOzs7O1FBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxFQUFDLENBQUM7SUFDaEUsQ0FBQzs7O1lBdkJGLFVBQVUsU0FBQztnQkFDVixVQUFVLEVBQUUsTUFBTTthQUNuQjs7Ozt3Q0FHYyxRQUFRLFlBQUksTUFBTSxTQUFDLFlBQVk7Ozs7Ozs7O0lBQWhDLGlDQUE2RSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdCwgSW5qZWN0YWJsZSwgT3B0aW9uYWwgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IGNvbWJpbmVMYXRlc3QsIE9ic2VydmFibGUsIG9mIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBHcnVwb1Blc3F1aXNhLCBJdGVtUGVzcXVpc2EgfSBmcm9tICcuL3NlYXJjaC5tb2RlbCc7XG5pbXBvcnQgeyBTRUFSQ0hfVE9LRU4sIFNlYXJjaEFic3RyYWN0U2VydmljZSB9IGZyb20gJy4uLy4uLy4uL3V0aWxzL2Fic3RyYWN0L3NlYXJjaC1hYnN0cmFjdC5zZXJ2aWNlJztcblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgU2VhcmNoU2VydmljZSB7XG5cbiAgY29uc3RydWN0b3IoQE9wdGlvbmFsKCkgQEluamVjdChTRUFSQ0hfVE9LRU4pIHByb3RlY3RlZCBzZXJ2aWNlczogU2VhcmNoQWJzdHJhY3RTZXJ2aWNlW10pIHtcblxuICB9XG5cbiAgYnVzY2FyKHRlcm06IHN0cmluZyk6IE9ic2VydmFibGU8KEdydXBvUGVzcXVpc2EgfCBJdGVtUGVzcXVpc2EpW10+IHtcblxuICAgIGlmICghdGhpcy5zZXJ2aWNlcykge1xuICAgICAgY29uc29sZS5lcnJvcignTsOjbyBleGlzdGUgc2VydmnDp28gZGUgYnVzY2EgY29uZmlndXJhZG8hIExlciBkb2N1bWVudGHDp8OjbyBwYXJhIG1haXMgaW5mb3JtYcOnw7VlcywgZGUgY29tbyB1dGlsaXphci4nKTtcbiAgICAgIHJldHVybiBvZigpO1xuICAgIH1cblxuICAgIGNvbnN0IHNlYXJjaE9ic2VydmFibGVzID0gdGhpcy5zZXJ2aWNlcy5tYXAoc2VhcmNoU2VydmljZSA9PiBzZWFyY2hTZXJ2aWNlLnNlYXJjaEdyb3VwKHRlcm0pKTtcbiAgICBjb25zdCBjb21iaW5lZE9ic2VydmFibGUgPSBjb21iaW5lTGF0ZXN0KHNlYXJjaE9ic2VydmFibGVzKTtcbiAgICByZXR1cm4gY29tYmluZWRPYnNlcnZhYmxlO1xuICB9XG5cbiAgYWRpY2lvbmFyR3J1cG9QZXNxdWlzYShncm91cDogR3J1cG9QZXNxdWlzYVtdKSB7XG4gICAgdGhpcy5zZXJ2aWNlcy5tYXAoZ3JvdXBTZWFyY2ggPT4gZ3JvdXBTZWFyY2guYWRkR3JvdXAoZ3JvdXApKTtcbiAgfVxufVxuIl19