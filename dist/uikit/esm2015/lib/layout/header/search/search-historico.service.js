/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { LocalRepository } from '../../../shared/repositorio/local-repository';
import { of } from 'rxjs';
import * as _ from 'lodash';
import * as i0 from "@angular/core";
import * as i1 from "../../../shared/repositorio/local-repository";
export class SearchHistoricoService {
    /**
     * @param {?} repositorio
     */
    constructor(repositorio) {
        this.repositorio = repositorio;
    }
    /**
     * @return {?}
     */
    obterHistorico() {
        /** @type {?} */
        let listaHistorico;
        /** @type {?} */
        const key = 'historicoPesquisa';
        /** @type {?} */
        const item = this.repositorio.obterItem(key);
        if (item === false) {
            this.repositorio.salvarItem(key, []);
        }
        else {
            listaHistorico = item;
        }
        return listaHistorico;
    }
    /**
     * @param {?} historico
     * @return {?}
     */
    salvarHistorico(historico) {
        /** @type {?} */
        let listaHistorico;
        /** @type {?} */
        const key = 'historicoPesquisa';
        /** @type {?} */
        const itemAddHistorage = this.repositorio.obterItem(key);
        /** @type {?} */
        const itemRetorno = _.xorWith([historico], itemAddHistorage, this.isEqual);
        if (itemRetorno.length > 3) {
            listaHistorico = _.take(itemRetorno, 3);
        }
        else {
            listaHistorico = itemRetorno;
        }
        this.repositorio.salvarItem(key, listaHistorico);
        return of(null);
    }
    /**
     * @private
     * @param {?} p1
     * @param {?} p2
     * @return {?}
     */
    isEqual(p1, p2) {
        return _.isEqual({ x: p1.title, y: p1.title }, { x: p2.title, y: p2.title });
    }
}
SearchHistoricoService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
SearchHistoricoService.ctorParameters = () => [
    { type: LocalRepository }
];
/** @nocollapse */ SearchHistoricoService.ngInjectableDef = i0.defineInjectable({ factory: function SearchHistoricoService_Factory() { return new SearchHistoricoService(i0.inject(i1.LocalRepository)); }, token: SearchHistoricoService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    SearchHistoricoService.prototype.repositorio;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLWhpc3Rvcmljby5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9sYXlvdXQvaGVhZGVyL3NlYXJjaC9zZWFyY2gtaGlzdG9yaWNvLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFDLGVBQWUsRUFBQyxNQUFNLDhDQUE4QyxDQUFDO0FBRTdFLE9BQU8sRUFBbUIsRUFBRSxFQUFDLE1BQU0sTUFBTSxDQUFDO0FBQzFDLE9BQU8sS0FBSyxDQUFDLE1BQU0sUUFBUSxDQUFDOzs7QUFLNUIsTUFBTSxPQUFPLHNCQUFzQjs7OztJQUlqQyxZQUFZLFdBQTRCO1FBQ3RDLElBQUksQ0FBQyxXQUFXLEdBQUcsV0FBVyxDQUFDO0lBQ2pDLENBQUM7Ozs7SUFFRCxjQUFjOztZQUNSLGNBQWM7O2NBQ1osR0FBRyxHQUFHLG1CQUFtQjs7Y0FDekIsSUFBSSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFNLEdBQUcsQ0FBQztRQUNqRCxJQUFJLElBQUksS0FBSyxLQUFLLEVBQUU7WUFDbEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1NBQ3RDO2FBQU07WUFDTCxjQUFjLEdBQUcsSUFBSSxDQUFDO1NBQ3ZCO1FBQ0QsT0FBTyxjQUFjLENBQUM7SUFDeEIsQ0FBQzs7Ozs7SUFFRCxlQUFlLENBQUMsU0FBYzs7WUFDeEIsY0FBYzs7Y0FDWixHQUFHLEdBQUcsbUJBQW1COztjQUN6QixnQkFBZ0IsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBTSxHQUFHLENBQUM7O2NBQ3ZELFdBQVcsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsU0FBUyxDQUFDLEVBQUUsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUUxRSxJQUFJLFdBQVcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQzFCLGNBQWMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUN6QzthQUFNO1lBQ0wsY0FBYyxHQUFHLFdBQVcsQ0FBQztTQUM5QjtRQUNELElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLEdBQUcsRUFBRSxjQUFjLENBQUMsQ0FBQztRQUNqRCxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNsQixDQUFDOzs7Ozs7O0lBRU8sT0FBTyxDQUFDLEVBQU8sRUFBRSxFQUFPO1FBQzlCLE9BQU8sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxFQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsS0FBSyxFQUFDLEVBQUUsRUFBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLEtBQUssRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLEtBQUssRUFBQyxDQUFDLENBQUM7SUFDM0UsQ0FBQzs7O1lBeENGLFVBQVUsU0FBQztnQkFDVixVQUFVLEVBQUUsTUFBTTthQUNuQjs7OztZQVBPLGVBQWU7Ozs7Ozs7O0lBVXJCLDZDQUErQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SW5qZWN0YWJsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0xvY2FsUmVwb3NpdG9yeX0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL3JlcG9zaXRvcmlvL2xvY2FsLXJlcG9zaXRvcnknO1xuaW1wb3J0IHtJTG9jYWxSZXBvc2l0b3J5fSBmcm9tICcuLi8uLi8uLi9zaGFyZWQvcmVwb3NpdG9yaW8vaWxvY2FsLXJlcG9zaXRvcnknO1xuaW1wb3J0IHtmcm9tLCBPYnNlcnZhYmxlLCBvZn0gZnJvbSAncnhqcyc7XG5pbXBvcnQgKiBhcyBfIGZyb20gJ2xvZGFzaCc7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIFNlYXJjaEhpc3Rvcmljb1NlcnZpY2Uge1xuXG4gIHByaXZhdGUgcmVhZG9ubHkgcmVwb3NpdG9yaW86IElMb2NhbFJlcG9zaXRvcnk7XG5cbiAgY29uc3RydWN0b3IocmVwb3NpdG9yaW86IExvY2FsUmVwb3NpdG9yeSkge1xuICAgIHRoaXMucmVwb3NpdG9yaW8gPSByZXBvc2l0b3JpbztcbiAgfVxuXG4gIG9idGVySGlzdG9yaWNvKCkge1xuICAgIGxldCBsaXN0YUhpc3RvcmljbztcbiAgICBjb25zdCBrZXkgPSAnaGlzdG9yaWNvUGVzcXVpc2EnO1xuICAgIGNvbnN0IGl0ZW0gPSB0aGlzLnJlcG9zaXRvcmlvLm9idGVySXRlbTxhbnk+KGtleSk7XG4gICAgaWYgKGl0ZW0gPT09IGZhbHNlKSB7XG4gICAgICB0aGlzLnJlcG9zaXRvcmlvLnNhbHZhckl0ZW0oa2V5LCBbXSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGxpc3RhSGlzdG9yaWNvID0gaXRlbTtcbiAgICB9XG4gICAgcmV0dXJuIGxpc3RhSGlzdG9yaWNvO1xuICB9XG5cbiAgc2FsdmFySGlzdG9yaWNvKGhpc3RvcmljbzogYW55KTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICBsZXQgbGlzdGFIaXN0b3JpY287XG4gICAgY29uc3Qga2V5ID0gJ2hpc3Rvcmljb1Blc3F1aXNhJztcbiAgICBjb25zdCBpdGVtQWRkSGlzdG9yYWdlID0gdGhpcy5yZXBvc2l0b3Jpby5vYnRlckl0ZW08YW55PihrZXkpO1xuICAgIGNvbnN0IGl0ZW1SZXRvcm5vID0gXy54b3JXaXRoKFtoaXN0b3JpY29dLCBpdGVtQWRkSGlzdG9yYWdlLCB0aGlzLmlzRXF1YWwpO1xuXG4gICAgaWYgKGl0ZW1SZXRvcm5vLmxlbmd0aCA+IDMpIHtcbiAgICAgIGxpc3RhSGlzdG9yaWNvID0gXy50YWtlKGl0ZW1SZXRvcm5vLCAzKTtcbiAgICB9IGVsc2Uge1xuICAgICAgbGlzdGFIaXN0b3JpY28gPSBpdGVtUmV0b3JubztcbiAgICB9XG4gICAgdGhpcy5yZXBvc2l0b3Jpby5zYWx2YXJJdGVtKGtleSwgbGlzdGFIaXN0b3JpY28pO1xuICAgIHJldHVybiBvZihudWxsKTtcbiAgfVxuXG4gIHByaXZhdGUgaXNFcXVhbChwMTogYW55LCBwMjogYW55KSB7XG4gICAgcmV0dXJuIF8uaXNFcXVhbCh7eDogcDEudGl0bGUsIHk6IHAxLnRpdGxlfSwge3g6IHAyLnRpdGxlLCB5OiBwMi50aXRsZX0pO1xuICB9XG59XG4iXX0=