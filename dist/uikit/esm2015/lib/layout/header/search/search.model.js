/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as _ from 'lodash';
/**
 * @record
 */
export function ItemPesquisa() { }
if (false) {
    /** @type {?} */
    ItemPesquisa.prototype.id;
    /** @type {?} */
    ItemPesquisa.prototype.title;
    /** @type {?|undefined} */
    ItemPesquisa.prototype.icon;
    /** @type {?} */
    ItemPesquisa.prototype.tags;
    /** @type {?} */
    ItemPesquisa.prototype.link;
}
/**
 * @record
 */
export function GrupoPesquisa() { }
if (false) {
    /** @type {?} */
    GrupoPesquisa.prototype.id;
    /** @type {?} */
    GrupoPesquisa.prototype.title;
    /** @type {?|undefined} */
    GrupoPesquisa.prototype.icon;
    /** @type {?|undefined} */
    GrupoPesquisa.prototype.isHeader;
    /** @type {?|undefined} */
    GrupoPesquisa.prototype.isAviso;
    /** @type {?} */
    GrupoPesquisa.prototype.items;
}
/**
 * @return {?}
 */
export function criarNovoGrupo() {
    return (/** @type {?} */ ({
        title: '',
        items: []
    }));
}
/**
 * @param {?} itemParam
 * @return {?}
 */
export function popularItens(itemParam) {
    return _.map(itemParam, (/**
     * @param {?} item
     * @return {?}
     */
    (item) => {
        return (/** @type {?} */ ({
            id: item.id,
            title: item.title,
            tags: item.tags,
            link: item.link
        }));
    }));
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9sYXlvdXQvaGVhZGVyL3NlYXJjaC9zZWFyY2gubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sS0FBSyxDQUFDLE1BQU0sUUFBUSxDQUFDOzs7O0FBRzVCLGtDQU1DOzs7SUFMQywwQkFBTzs7SUFDUCw2QkFBYzs7SUFDZCw0QkFBYzs7SUFDZCw0QkFBZTs7SUFDZiw0QkFBYTs7Ozs7QUFHZixtQ0FPQzs7O0lBTkMsMkJBQU87O0lBQ1AsOEJBQWM7O0lBQ2QsNkJBQWM7O0lBQ2QsaUNBQW1COztJQUNuQixnQ0FBa0I7O0lBQ2xCLDhCQUF3Qzs7Ozs7QUFHMUMsTUFBTSxVQUFVLGNBQWM7SUFDNUIsT0FBTyxtQkFBQTtRQUNMLEtBQUssRUFBRSxFQUFFO1FBQ1QsS0FBSyxFQUFFLEVBQUU7S0FDVixFQUFpQixDQUFDO0FBQ3JCLENBQUM7Ozs7O0FBRUQsTUFBTSxVQUFVLFlBQVksQ0FBQyxTQUFnQjtJQUMzQyxPQUFPLENBQUMsQ0FBQyxHQUFHLENBQUMsU0FBUzs7OztJQUFFLENBQUMsSUFBSSxFQUFFLEVBQUU7UUFDL0IsT0FBTyxtQkFBQTtZQUNMLEVBQUUsRUFBRSxJQUFJLENBQUMsRUFBRTtZQUNYLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztZQUNqQixJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUk7WUFDZixJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUk7U0FDaEIsRUFBZ0IsQ0FBQztJQUNwQixDQUFDLEVBQUMsQ0FBQztBQUNMLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBfIGZyb20gJ2xvZGFzaCc7XG5pbXBvcnQge0lEfSBmcm9tICdAZGF0b3JhbWEvYWtpdGEnO1xuXG5leHBvcnQgaW50ZXJmYWNlIEl0ZW1QZXNxdWlzYSB7XG4gIGlkOiBJRDtcbiAgdGl0bGU6IHN0cmluZztcbiAgaWNvbj86IHN0cmluZztcbiAgdGFnczogc3RyaW5nW107XG4gIGxpbms6IHN0cmluZztcbn1cblxuZXhwb3J0IGludGVyZmFjZSBHcnVwb1Blc3F1aXNhIHtcbiAgaWQ6IElEO1xuICB0aXRsZTogc3RyaW5nO1xuICBpY29uPzogc3RyaW5nO1xuICBpc0hlYWRlcj86IGJvb2xlYW47XG4gIGlzQXZpc28/OiBib29sZWFuO1xuICBpdGVtczogSXRlbVBlc3F1aXNhW10gfCBHcnVwb1Blc3F1aXNhW107XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBjcmlhck5vdm9HcnVwbygpIHtcbiAgcmV0dXJuIHtcbiAgICB0aXRsZTogJycsXG4gICAgaXRlbXM6IFtdXG4gIH0gYXMgR3J1cG9QZXNxdWlzYTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIHBvcHVsYXJJdGVucyhpdGVtUGFyYW06IGFueVtdKSB7XG4gIHJldHVybiBfLm1hcChpdGVtUGFyYW0sIChpdGVtKSA9PiB7XG4gICAgcmV0dXJuIHtcbiAgICAgIGlkOiBpdGVtLmlkLFxuICAgICAgdGl0bGU6IGl0ZW0udGl0bGUsXG4gICAgICB0YWdzOiBpdGVtLnRhZ3MsXG4gICAgICBsaW5rOiBpdGVtLmxpbmtcbiAgICB9IGFzIEl0ZW1QZXNxdWlzYTtcbiAgfSk7XG59XG4iXX0=