/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { SearchStore } from './search.store';
import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import * as i0 from "@angular/core";
import * as i1 from "./search.store";
export class SearchQuery extends QueryEntity {
    /**
     * @param {?} store
     */
    constructor(store) {
        super(store);
        this.store = store;
    }
}
SearchQuery.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
SearchQuery.ctorParameters = () => [
    { type: SearchStore }
];
/** @nocollapse */ SearchQuery.ngInjectableDef = i0.defineInjectable({ factory: function SearchQuery_Factory() { return new SearchQuery(i0.inject(i1.SearchStore)); }, token: SearchQuery, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @protected
     */
    SearchQuery.prototype.store;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLnF1ZXJ5LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9sYXlvdXQvaGVhZGVyL3NlYXJjaC9zdGF0ZS9zZWFyY2gucXVlcnkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBYyxXQUFXLEVBQUMsTUFBTSxnQkFBZ0IsQ0FBQztBQUd4RCxPQUFPLEVBQUMsVUFBVSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBQyxXQUFXLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQzs7O0FBSzVDLE1BQU0sT0FBTyxXQUFZLFNBQVEsV0FBdUM7Ozs7SUFDdEUsWUFBc0IsS0FBa0I7UUFDdEMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBRE8sVUFBSyxHQUFMLEtBQUssQ0FBYTtJQUV4QyxDQUFDOzs7WUFORixVQUFVLFNBQUM7Z0JBQ1YsVUFBVSxFQUFFLE1BQU07YUFDbkI7Ozs7WUFSb0IsV0FBVzs7Ozs7Ozs7SUFVbEIsNEJBQTRCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtTZWFyY2hTdGF0ZSwgU2VhcmNoU3RvcmV9IGZyb20gJy4vc2VhcmNoLnN0b3JlJztcbmltcG9ydCB7R3J1cG9QZXNxdWlzYX0gZnJvbSAnLi4vc2VhcmNoLm1vZGVsJztcbmltcG9ydCB7bWFwfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQge0luamVjdGFibGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtRdWVyeUVudGl0eX0gZnJvbSAnQGRhdG9yYW1hL2FraXRhJztcblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgU2VhcmNoUXVlcnkgZXh0ZW5kcyBRdWVyeUVudGl0eTxTZWFyY2hTdGF0ZSwgR3J1cG9QZXNxdWlzYT4ge1xuICBjb25zdHJ1Y3Rvcihwcm90ZWN0ZWQgc3RvcmU6IFNlYXJjaFN0b3JlKSB7XG4gICAgc3VwZXIoc3RvcmUpO1xuICB9XG5cbiAgLypzZWFyY2hHcm91cERhdGEkID0gdGhpcy5zZWxlY3RBbGwoKS5waXBlKFxuICAgIG1hcCh0aGlzLmdldEdydXBvUGVzcXVpc2FEYXRhLmJpbmQodGhpcykpXG4gICk7XG5cblxuICBnZXRHcnVwb1Blc3F1aXNhRGF0YShzdHVkZW50czogQXJyYXk8R3J1cG9QZXNxdWlzYT4pOiB7IFtrZXk6IHN0cmluZ106IEFycmF5PEdydXBvUGVzcXVpc2E+IH0ge1xuICAgIHJldHVybiBzdHVkZW50cy5yZWR1Y2UoKFxuICAgICAgeyB0aXRsZTogbkFycmF5LCBpdGVtczogcUFycmF5fSxcbiAgICAgIHsgdGl0bGUsIGl0ZW1zIH0pID0+IHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIHRpdGxlOiBbLi4ubkFycmF5LCB0aXRsZV0sXG4gICAgICAgIGl0ZW1zOiBbLi4ucUFycmF5LCBpdGVtc11cbiAgICAgIH07XG4gICAgfSwgeyB0aXRsZTogW10sIGl0ZW1zOiBbXX0pO1xuICB9Ki9cbn1cbiJdfQ==