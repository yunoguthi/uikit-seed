/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { EntityStore, StoreConfig, transaction } from '@datorama/akita';
import * as i0 from "@angular/core";
/**
 * @record
 */
export function SearchState() { }
let SearchStore = class SearchStore extends EntityStore {
    constructor() {
        super();
    }
    /**
     * @param {?} valueJaSetado
     * @param {?} groupItem
     * @return {?}
     */
    adicionarGrupoPesquisa(valueJaSetado, groupItem) {
        // valueJaSetado.push(groupItem);
        /** @type {?} */
        const item = [...valueJaSetado, ...groupItem];
        this.set(item);
    }
    /**
     * @param {?} groupItem
     * @return {?}
     */
    addGroupBase(groupItem) {
        this.set(groupItem);
    }
    /**
     * @param {?} title
     * @return {?}
     */
    removerGrupoPesquisa(title) {
        this.remove(title);
    }
};
SearchStore.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
SearchStore.ctorParameters = () => [];
/** @nocollapse */ SearchStore.ngInjectableDef = i0.defineInjectable({ factory: function SearchStore_Factory() { return new SearchStore(); }, token: SearchStore, providedIn: "root" });
tslib_1.__decorate([
    transaction(),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, Object]),
    tslib_1.__metadata("design:returntype", void 0)
], SearchStore.prototype, "adicionarGrupoPesquisa", null);
tslib_1.__decorate([
    transaction(),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", void 0)
], SearchStore.prototype, "addGroupBase", null);
tslib_1.__decorate([
    transaction(),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String]),
    tslib_1.__metadata("design:returntype", void 0)
], SearchStore.prototype, "removerGrupoPesquisa", null);
SearchStore = tslib_1.__decorate([
    StoreConfig({
        name: 'search'
    }),
    tslib_1.__metadata("design:paramtypes", [])
], SearchStore);
export { SearchStore };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLnN0b3JlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi9sYXlvdXQvaGVhZGVyL3NlYXJjaC9zdGF0ZS9zZWFyY2guc3RvcmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFDQSxPQUFPLEVBQUMsVUFBVSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBYyxXQUFXLEVBQUUsV0FBVyxFQUFFLFdBQVcsRUFBQyxNQUFNLGlCQUFpQixDQUFDOzs7OztBQUVuRixpQ0FDQztJQVFZLFdBQVcsU0FBWCxXQUFZLFNBQVEsV0FBdUM7SUFDdEU7UUFDRSxLQUFLLEVBQUUsQ0FBQztJQUNWLENBQUM7Ozs7OztJQUdELHNCQUFzQixDQUFDLGFBQWEsRUFBRSxTQUFTOzs7Y0FFdkMsSUFBSSxHQUFHLENBQUMsR0FBRyxhQUFhLEVBQUUsR0FBRyxTQUFTLENBQUM7UUFDN0MsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNqQixDQUFDOzs7OztJQUdELFlBQVksQ0FBQyxTQUFTO1FBQ3BCLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDdEIsQ0FBQzs7Ozs7SUFHRCxvQkFBb0IsQ0FBQyxLQUFhO1FBQ2hDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDckIsQ0FBQztDQUNGLENBQUE7O1lBM0JBLFVBQVUsU0FBQztnQkFDVixVQUFVLEVBQUUsTUFBTTthQUNuQjs7Ozs7QUFVQztJQURDLFdBQVcsRUFBRTs7Ozt5REFLYjtBQUdEO0lBREMsV0FBVyxFQUFFOzs7OytDQUdiO0FBR0Q7SUFEQyxXQUFXLEVBQUU7Ozs7dURBR2I7QUFwQlUsV0FBVztJQUh2QixXQUFXLENBQUM7UUFDWCxJQUFJLEVBQUUsUUFBUTtLQUNmLENBQUM7O0dBQ1csV0FBVyxDQXFCdkI7U0FyQlksV0FBVyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7R3J1cG9QZXNxdWlzYX0gZnJvbSAnLi4vc2VhcmNoLm1vZGVsJztcbmltcG9ydCB7SW5qZWN0YWJsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0VudGl0eVN0YXRlLCBFbnRpdHlTdG9yZSwgU3RvcmVDb25maWcsIHRyYW5zYWN0aW9ufSBmcm9tICdAZGF0b3JhbWEvYWtpdGEnO1xuXG5leHBvcnQgaW50ZXJmYWNlIFNlYXJjaFN0YXRlIGV4dGVuZHMgRW50aXR5U3RhdGU8R3J1cG9QZXNxdWlzYT4ge1xufVxuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbkBTdG9yZUNvbmZpZyh7XG4gIG5hbWU6ICdzZWFyY2gnXG59KVxuZXhwb3J0IGNsYXNzIFNlYXJjaFN0b3JlIGV4dGVuZHMgRW50aXR5U3RvcmU8U2VhcmNoU3RhdGUsIEdydXBvUGVzcXVpc2E+IHtcbiAgY29uc3RydWN0b3IoKSB7XG4gICAgc3VwZXIoKTtcbiAgfVxuXG4gIEB0cmFuc2FjdGlvbigpXG4gIGFkaWNpb25hckdydXBvUGVzcXVpc2EodmFsdWVKYVNldGFkbywgZ3JvdXBJdGVtKSB7XG4gICAgLy8gdmFsdWVKYVNldGFkby5wdXNoKGdyb3VwSXRlbSk7XG4gICAgY29uc3QgaXRlbSA9IFsuLi52YWx1ZUphU2V0YWRvLCAuLi5ncm91cEl0ZW1dO1xuICAgIHRoaXMuc2V0KGl0ZW0pO1xuICB9XG5cbiAgQHRyYW5zYWN0aW9uKClcbiAgYWRkR3JvdXBCYXNlKGdyb3VwSXRlbSkge1xuICAgIHRoaXMuc2V0KGdyb3VwSXRlbSk7XG4gIH1cblxuICBAdHJhbnNhY3Rpb24oKVxuICByZW1vdmVyR3J1cG9QZXNxdWlzYSh0aXRsZTogc3RyaW5nKSB7XG4gICAgdGhpcy5yZW1vdmUodGl0bGUpO1xuICB9XG59XG4iXX0=