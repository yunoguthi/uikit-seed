/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ChangeDetectionStrategy } from '@angular/core';
/**
 * @record
 */
export function Section() { }
if (false) {
    /** @type {?} */
    Section.prototype.name;
    /** @type {?} */
    Section.prototype.date;
    /** @type {?} */
    Section.prototype.description;
    /** @type {?} */
    Section.prototype.read;
}
export class SysteminfoComponent {
    constructor() {
        this.folders = [
            {
                name: '2.0.2',
                date: '04/02/2019',
                description: 'Nesta versão reunimos grandes demandas de melhoria do sistema.',
                read: true
            },
            {
                name: '2.0.1.1',
                date: '27/11/2018',
                description: 'Para esta versão foi feito um trabalho detalhado de correção, uniformização e melhoria da funcionalidade de redistribuição de processos, muitos tribunais estavam reportando e encaminhando códigos pontuais de correção da redistribuição. A equipe do CNJ fez um trabalho detalhado de revisão e readequação da rotina, simplificando o código, melhorando a usabilidade e padronizando as operações. Com esse trabalho foram atendidas outras 30 demandas de 12 tribunais diferentes.',
                read: false
            }
        ];
    }
    /**
     * @return {?}
     */
    ngOnInit() { }
}
SysteminfoComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-systeminfo',
                template: "<mat-menu #systemInfo=\"matMenu\">\n  <mat-list class=\"dropdown-list list-reader\" (click)=\"$event.stopPropagation()\">\n    <h3 mat-subheader>\n      Informa\u00E7\u00F5es do sistema\n    </h3>\n    <ng-content></ng-content>\n    <!-- <mat-nav-list>\n      <a mat-list-item *ngFor=\"let folder of folders\" [class.is-read]=\"folder.read\">\n        <div>\n          <h4>\n            {{ folder.name }}\n          </h4>\n          <span class=\"date\">{{ folder.date }}</span>\n          <p>\n            {{ folder.description }}\n          </p>\n        </div>\n        <mat-icon matListIcon class=\"fa-lg far\" [class.fa-eye]=\"!folder.read\" [class.fa-eye-slash]=\"folder.read\"></mat-icon>\n\n        <mat-divider></mat-divider>\n      </a>\n    </mat-nav-list> -->\n  </mat-list>\n\n  <!-- <mat-action-list>\n      <a  mat-list-item routerLink=\".\">ver todos</a>\n    </mat-action-list> -->\n</mat-menu>\n\n<button\n  mat-icon-button\n  tabindex=\"4\"\n  color=\"primary\"\n  [matMenuTriggerFor]=\"systemInfo\"\n  matTooltip=\"Informa\u00E7\u00F5es do sistema\"\n  aria-label=\"Informa\u00E7\u00F5es do sistema\"\n>\n  <mat-icon class=\"fa-2x fas fa-info-circle\"></mat-icon>\n</button>\n",
                changeDetection: ChangeDetectionStrategy.OnPush,
                styles: [""]
            }] }
];
/** @nocollapse */
SysteminfoComponent.ctorParameters = () => [];
if (false) {
    /** @type {?} */
    SysteminfoComponent.prototype.folders;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3lzdGVtaW5mby5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AY25qL3Vpa2l0LyIsInNvdXJjZXMiOlsibGliL2xheW91dC9oZWFkZXIvc3lzdGVtaW5mby9zeXN0ZW1pbmZvLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSx1QkFBdUIsRUFBRSxNQUFNLGVBQWUsQ0FBQzs7OztBQUUzRSw2QkFLQzs7O0lBSkMsdUJBQWE7O0lBQ2IsdUJBQWE7O0lBQ2IsOEJBQW9COztJQUNwQix1QkFBYzs7QUFTaEIsTUFBTSxPQUFPLG1CQUFtQjtJQWtCOUI7UUFqQkEsWUFBTyxHQUFjO1lBQ25CO2dCQUNFLElBQUksRUFBRSxPQUFPO2dCQUNiLElBQUksRUFBRSxZQUFZO2dCQUNsQixXQUFXLEVBQ1QsZ0VBQWdFO2dCQUNsRSxJQUFJLEVBQUUsSUFBSTthQUNYO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLFNBQVM7Z0JBQ2YsSUFBSSxFQUFFLFlBQVk7Z0JBQ2xCLFdBQVcsRUFDVCwwZEFBMGQ7Z0JBQzVkLElBQUksRUFBRSxLQUFLO2FBQ1o7U0FDRixDQUFDO0lBRWEsQ0FBQzs7OztJQUVoQixRQUFRLEtBQUksQ0FBQzs7O1lBMUJkLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsa0JBQWtCO2dCQUM1Qix3ckNBQTBDO2dCQUUxQyxlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTs7YUFDaEQ7Ozs7OztJQUVDLHNDQWVFIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIENoYW5nZURldGVjdGlvblN0cmF0ZWd5IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmV4cG9ydCBpbnRlcmZhY2UgU2VjdGlvbiB7XG4gIG5hbWU6IHN0cmluZztcbiAgZGF0ZTogc3RyaW5nO1xuICBkZXNjcmlwdGlvbjogc3RyaW5nO1xuICByZWFkOiBib29sZWFuO1xufVxuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICd1aWtpdC1zeXN0ZW1pbmZvJyxcbiAgdGVtcGxhdGVVcmw6ICcuL3N5c3RlbWluZm8uY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9zeXN0ZW1pbmZvLmNvbXBvbmVudC5zY3NzJ10sXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoXG59KVxuZXhwb3J0IGNsYXNzIFN5c3RlbWluZm9Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBmb2xkZXJzOiBTZWN0aW9uW10gPSBbXG4gICAge1xuICAgICAgbmFtZTogJzIuMC4yJyxcbiAgICAgIGRhdGU6ICcwNC8wMi8yMDE5JyxcbiAgICAgIGRlc2NyaXB0aW9uOlxuICAgICAgICAnTmVzdGEgdmVyc8OjbyByZXVuaW1vcyBncmFuZGVzIGRlbWFuZGFzIGRlIG1lbGhvcmlhIGRvIHNpc3RlbWEuJyxcbiAgICAgIHJlYWQ6IHRydWVcbiAgICB9LFxuICAgIHtcbiAgICAgIG5hbWU6ICcyLjAuMS4xJyxcbiAgICAgIGRhdGU6ICcyNy8xMS8yMDE4JyxcbiAgICAgIGRlc2NyaXB0aW9uOlxuICAgICAgICAnUGFyYSBlc3RhIHZlcnPDo28gZm9pIGZlaXRvIHVtIHRyYWJhbGhvIGRldGFsaGFkbyBkZSBjb3JyZcOnw6NvLCB1bmlmb3JtaXphw6fDo28gZSBtZWxob3JpYSBkYSBmdW5jaW9uYWxpZGFkZSBkZSByZWRpc3RyaWJ1acOnw6NvIGRlIHByb2Nlc3NvcywgbXVpdG9zIHRyaWJ1bmFpcyBlc3RhdmFtIHJlcG9ydGFuZG8gZSBlbmNhbWluaGFuZG8gY8OzZGlnb3MgcG9udHVhaXMgZGUgY29ycmXDp8OjbyBkYSByZWRpc3RyaWJ1acOnw6NvLiBBIGVxdWlwZSBkbyBDTkogZmV6IHVtIHRyYWJhbGhvIGRldGFsaGFkbyBkZSByZXZpc8OjbyBlIHJlYWRlcXVhw6fDo28gZGEgcm90aW5hLCBzaW1wbGlmaWNhbmRvIG8gY8OzZGlnbywgbWVsaG9yYW5kbyBhIHVzYWJpbGlkYWRlIGUgcGFkcm9uaXphbmRvIGFzIG9wZXJhw6fDtWVzLiBDb20gZXNzZSB0cmFiYWxobyBmb3JhbSBhdGVuZGlkYXMgb3V0cmFzIDMwIGRlbWFuZGFzIGRlIDEyIHRyaWJ1bmFpcyBkaWZlcmVudGVzLicsXG4gICAgICByZWFkOiBmYWxzZVxuICAgIH1cbiAgXTtcblxuICBjb25zdHJ1Y3RvcigpIHt9XG5cbiAgbmdPbkluaXQoKSB7fVxufVxuIl19