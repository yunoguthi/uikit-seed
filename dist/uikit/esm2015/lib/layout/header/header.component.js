/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { ChangeDetectionStrategy, Component, Input, Renderer2, ViewChild } from '@angular/core';
import { NavQuery } from '../nav/state/nav.query';
import { NavService } from '../nav/state/nav.service';
import { MatToolbar } from '@angular/material';
import { LayoutService } from '../layout.service';
export class HeaderComponent {
    /**
     * @param {?} navQuery
     * @param {?} navService
     * @param {?} layoutService
     * @param {?} renderer
     */
    constructor(navQuery, navService, layoutService, renderer) {
        this.navQuery = navQuery;
        this.navService = navService;
        this.layoutService = layoutService;
        this.renderer = renderer;
        this.showNotifications = false;
        this.showUserInfo = false;
        this.showSystemInfo = false;
        this.showIa = false;
        this.showShortcuts = true;
        this.leftNav$ = this.navQuery.leftnav$;
        this.rightNav$ = this.navQuery.rightnav$;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @return {?}
     */
    toggle() {
        this.navService.toggleLeftNav();
    }
    /**
     * @return {?}
     */
    rightNavToggle() {
        this.navService.toggleRightNav();
    }
}
HeaderComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-header',
                template: "<mat-toolbar>\n\n  <button tabindex=\"1\" mat-flat-button color=\"primary\" type=\"button\" aria-label=\"Abri menu de navega\u00E7\u00E3o\"\n    aria-controls=\"navigation\" matTooltip=\"Abrir menu de navega\u00E7\u00E3o\" (click)=\"toggle()\"\n    class=\"btn-hamburger hamburger--slider\" [class.is-active]=\"(leftNav$ | async).opened\">\n    <span class=\"hamburger-box\">\n      <span class=\"hamburger-inner\"></span>\n    </span>\n  </button>\n\n  <span class=\"logotipo\">\n    <a href=\"#\" tabindex=\"1\">\n      <div class=\"logomobile is-mobile\"></div>\n      <div class=\"logodesk is-desktop\"></div>\n    </a>\n  </span>\n\n  <span class=\"header-nav\">\n    <uikit-notification *ngIf=\"showNotifications == true\">\n      <div notifications>\n        <ng-content select=\"[header-notifications]\"></ng-content>\n      </div>\n    </uikit-notification>\n    <uikit-systeminfo *ngIf=\"showSystemInfo == true\">\n      <div systeminfo>\n        <ng-content select=\"[header-systeminfo]\"></ng-content>\n      </div>\n    </uikit-systeminfo>\n    <uikit-accessibility [showIa]=\"showIa\" [showShortcuts]=\"showShortcuts\"></uikit-accessibility>\n    <uikit-userinfo *ngIf=\"showUserInfo == true\">\n      <div userinfo>\n\n        <ng-content select=\"[header-userinfo]\"></ng-content>\n\n      </div>\n    </uikit-userinfo>\n\n    <button *ngIf=\"showIa\" class=\"aikit-button\" mat-flat-button color=\"primary\" type=\"button\"\n      aria-label=\"Abri menu de navega\u00E7\u00E3o\" aria-controls=\"navigation\" (click)=\"rightNavToggle()\"\n      matTooltip=\"Abri menu de navega\u00E7\u00E3o\" matTooltip=\"Notifica\u00E7\u00F5es\" aria-label=\"Notifica\u00E7\u00F5es\" matBadge=\"2\"\n      matBadgeColor=\"accent\" matBadgeSize=\"small\" tabindex=\"7\">\n      <img src=\"../../assets/images/iakit-logotipo-mobile.svg\" />\n    </button>\n  </span>\n</mat-toolbar>",
                changeDetection: ChangeDetectionStrategy.OnPush,
                styles: [""]
            }] }
];
/** @nocollapse */
HeaderComponent.ctorParameters = () => [
    { type: NavQuery },
    { type: NavService },
    { type: LayoutService },
    { type: Renderer2 }
];
HeaderComponent.propDecorators = {
    showNotifications: [{ type: Input }],
    showUserInfo: [{ type: Input }],
    showSystemInfo: [{ type: Input }],
    showIa: [{ type: Input }],
    matToolbar: [{ type: ViewChild, args: [MatToolbar,] }],
    showShortcuts: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    HeaderComponent.prototype.showNotifications;
    /** @type {?} */
    HeaderComponent.prototype.showUserInfo;
    /** @type {?} */
    HeaderComponent.prototype.showSystemInfo;
    /** @type {?} */
    HeaderComponent.prototype.showIa;
    /** @type {?} */
    HeaderComponent.prototype.matToolbar;
    /** @type {?} */
    HeaderComponent.prototype.showShortcuts;
    /** @type {?} */
    HeaderComponent.prototype.leftNav$;
    /** @type {?} */
    HeaderComponent.prototype.rightNav$;
    /**
     * @type {?}
     * @protected
     */
    HeaderComponent.prototype.navQuery;
    /**
     * @type {?}
     * @protected
     */
    HeaderComponent.prototype.navService;
    /**
     * @type {?}
     * @protected
     */
    HeaderComponent.prototype.layoutService;
    /**
     * @type {?}
     * @protected
     */
    HeaderComponent.prototype.renderer;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVhZGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvbGF5b3V0L2hlYWRlci9oZWFkZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsdUJBQXVCLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBVSxTQUFTLEVBQUUsU0FBUyxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3RHLE9BQU8sRUFBQyxRQUFRLEVBQUMsTUFBTSx3QkFBd0IsQ0FBQztBQUNoRCxPQUFPLEVBQUMsVUFBVSxFQUFDLE1BQU0sMEJBQTBCLENBQUM7QUFDcEQsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLG1CQUFtQixDQUFDO0FBQzdDLE9BQU8sRUFBQyxhQUFhLEVBQUMsTUFBTSxtQkFBbUIsQ0FBQztBQVFoRCxNQUFNLE9BQU8sZUFBZTs7Ozs7OztJQUMxQixZQUFzQixRQUFrQixFQUFZLFVBQXNCLEVBQ3BELGFBQTRCLEVBQzVCLFFBQW1CO1FBRm5CLGFBQVEsR0FBUixRQUFRLENBQVU7UUFBWSxlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3BELGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBQzVCLGFBQVEsR0FBUixRQUFRLENBQVc7UUFHaEMsc0JBQWlCLEdBQUcsS0FBSyxDQUFDO1FBQzFCLGlCQUFZLEdBQUcsS0FBSyxDQUFDO1FBQ3JCLG1CQUFjLEdBQUcsS0FBSyxDQUFDO1FBQ3ZCLFdBQU0sR0FBRyxLQUFLLENBQUM7UUFFZixrQkFBYSxHQUFHLElBQUksQ0FBQztRQUV2QixhQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUM7UUFDbEMsY0FBUyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDO0lBVjNDLENBQUM7Ozs7SUFZRCxRQUFRO0lBQ1IsQ0FBQzs7OztJQUVELE1BQU07UUFDSixJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ2xDLENBQUM7Ozs7SUFFRCxjQUFjO1FBQ1osSUFBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLEVBQUUsQ0FBQztJQUNuQyxDQUFDOzs7WUEvQkYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxjQUFjO2dCQUN4QixxMkRBQXNDO2dCQUV0QyxlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTs7YUFDaEQ7Ozs7WUFWTyxRQUFRO1lBQ1IsVUFBVTtZQUVWLGFBQWE7WUFKc0MsU0FBUzs7O2dDQWtCakUsS0FBSzsyQkFDTCxLQUFLOzZCQUNMLEtBQUs7cUJBQ0wsS0FBSzt5QkFDTCxTQUFTLFNBQUMsVUFBVTs0QkFDcEIsS0FBSzs7OztJQUxOLDRDQUFtQzs7SUFDbkMsdUNBQThCOztJQUM5Qix5Q0FBZ0M7O0lBQ2hDLGlDQUF3Qjs7SUFDeEIscUNBQThDOztJQUM5Qyx3Q0FBOEI7O0lBRTlCLG1DQUF5Qzs7SUFDekMsb0NBQTJDOzs7OztJQWIvQixtQ0FBNEI7Ozs7O0lBQUUscUNBQWdDOzs7OztJQUM5RCx3Q0FBc0M7Ozs7O0lBQ3RDLG1DQUE2QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3ksIENvbXBvbmVudCwgSW5wdXQsIE9uSW5pdCwgUmVuZGVyZXIyLCBWaWV3Q2hpbGR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtOYXZRdWVyeX0gZnJvbSAnLi4vbmF2L3N0YXRlL25hdi5xdWVyeSc7XG5pbXBvcnQge05hdlNlcnZpY2V9IGZyb20gJy4uL25hdi9zdGF0ZS9uYXYuc2VydmljZSc7XG5pbXBvcnQge01hdFRvb2xiYXJ9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcbmltcG9ydCB7TGF5b3V0U2VydmljZX0gZnJvbSAnLi4vbGF5b3V0LnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICd1aWtpdC1oZWFkZXInLFxuICB0ZW1wbGF0ZVVybDogJy4vaGVhZGVyLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vaGVhZGVyLmNvbXBvbmVudC5zY3NzJ10sXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoXG59KVxuZXhwb3J0IGNsYXNzIEhlYWRlckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIGNvbnN0cnVjdG9yKHByb3RlY3RlZCBuYXZRdWVyeTogTmF2UXVlcnksIHByb3RlY3RlZCBuYXZTZXJ2aWNlOiBOYXZTZXJ2aWNlLFxuICAgICAgICAgICAgICBwcm90ZWN0ZWQgbGF5b3V0U2VydmljZTogTGF5b3V0U2VydmljZSxcbiAgICAgICAgICAgICAgcHJvdGVjdGVkIHJlbmRlcmVyOiBSZW5kZXJlcjIpIHtcbiAgfVxuXG4gIEBJbnB1dCgpIHNob3dOb3RpZmljYXRpb25zID0gZmFsc2U7XG4gIEBJbnB1dCgpIHNob3dVc2VySW5mbyA9IGZhbHNlO1xuICBASW5wdXQoKSBzaG93U3lzdGVtSW5mbyA9IGZhbHNlO1xuICBASW5wdXQoKSBzaG93SWEgPSBmYWxzZTtcbiAgQFZpZXdDaGlsZChNYXRUb29sYmFyKSBtYXRUb29sYmFyOiBNYXRUb29sYmFyO1xuICBASW5wdXQoKSBzaG93U2hvcnRjdXRzID0gdHJ1ZTtcblxuICBwdWJsaWMgbGVmdE5hdiQgPSB0aGlzLm5hdlF1ZXJ5LmxlZnRuYXYkO1xuICBwdWJsaWMgcmlnaHROYXYkID0gdGhpcy5uYXZRdWVyeS5yaWdodG5hdiQ7XG5cbiAgbmdPbkluaXQoKSB7XG4gIH1cblxuICB0b2dnbGUoKSB7XG4gICAgdGhpcy5uYXZTZXJ2aWNlLnRvZ2dsZUxlZnROYXYoKTtcbiAgfVxuXG4gIHJpZ2h0TmF2VG9nZ2xlKCkge1xuICAgIHRoaXMubmF2U2VydmljZS50b2dnbGVSaWdodE5hdigpO1xuICB9XG5cbn1cbiJdfQ==