/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UnauthorizedRoutingModule } from './unauthorized-routing.module';
import { UnauthorizedComponent } from './unauthorized.component';
import { MatButtonModule } from '@angular/material';
export class UnauthorizedModule {
}
UnauthorizedModule.decorators = [
    { type: NgModule, args: [{
                entryComponents: [],
                declarations: [UnauthorizedComponent],
                imports: [
                    CommonModule,
                    UnauthorizedRoutingModule,
                    MatButtonModule
                ],
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidW5hdXRob3JpemVkLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvdW5hdXRob3JpemVkL3VuYXV0aG9yaXplZC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRS9DLE9BQU8sRUFBRSx5QkFBeUIsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBQzFFLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ2pFLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQVdwRCxNQUFNLE9BQU8sa0JBQWtCOzs7WUFUOUIsUUFBUSxTQUFDO2dCQUNSLGVBQWUsRUFBRSxFQUFFO2dCQUNuQixZQUFZLEVBQUUsQ0FBQyxxQkFBcUIsQ0FBQztnQkFDckMsT0FBTyxFQUFFO29CQUNQLFlBQVk7b0JBQ1oseUJBQXlCO29CQUN6QixlQUFlO2lCQUNoQjthQUNGIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5cbmltcG9ydCB7IFVuYXV0aG9yaXplZFJvdXRpbmdNb2R1bGUgfSBmcm9tICcuL3VuYXV0aG9yaXplZC1yb3V0aW5nLm1vZHVsZSc7XG5pbXBvcnQgeyBVbmF1dGhvcml6ZWRDb21wb25lbnQgfSBmcm9tICcuL3VuYXV0aG9yaXplZC5jb21wb25lbnQnO1xuaW1wb3J0IHsgTWF0QnV0dG9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xuXG5ATmdNb2R1bGUoe1xuICBlbnRyeUNvbXBvbmVudHM6IFtdLFxuICBkZWNsYXJhdGlvbnM6IFtVbmF1dGhvcml6ZWRDb21wb25lbnRdLFxuICBpbXBvcnRzOiBbXG4gICAgQ29tbW9uTW9kdWxlLFxuICAgIFVuYXV0aG9yaXplZFJvdXRpbmdNb2R1bGUsXG4gICAgTWF0QnV0dG9uTW9kdWxlXG4gIF0sXG59KVxuZXhwb3J0IGNsYXNzIFVuYXV0aG9yaXplZE1vZHVsZSB7IH1cbiJdfQ==