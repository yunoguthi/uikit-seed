/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UnauthorizedComponent } from './unauthorized.component';
const ɵ0 = {
    allowedTypes: ['normal', 'noshell', 'noshellnobreadcrumb'],
    defaultType: 'normal'
};
/** @type {?} */
const routes = [
    {
        path: 'unauthorized',
        component: UnauthorizedComponent,
        data: ɵ0
    }
];
export class UnauthorizedRoutingModule {
}
UnauthorizedRoutingModule.decorators = [
    { type: NgModule, args: [{
                imports: [RouterModule.forChild(routes)],
                exports: [RouterModule]
            },] }
];
export { ɵ0 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidW5hdXRob3JpemVkLXJvdXRpbmcubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGNuai91aWtpdC8iLCJzb3VyY2VzIjpbImxpYi91bmF1dGhvcml6ZWQvdW5hdXRob3JpemVkLXJvdXRpbmcubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBVSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUN2RCxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztXQU12RDtJQUNKLFlBQVksRUFBRSxDQUFDLFFBQVEsRUFBRSxTQUFTLEVBQUUscUJBQXFCLENBQUM7SUFDMUQsV0FBVyxFQUFFLFFBQVE7Q0FDdEI7O01BUEMsTUFBTSxHQUFXO0lBQ3JCO1FBQ0UsSUFBSSxFQUFFLGNBQWM7UUFDcEIsU0FBUyxFQUFFLHFCQUFxQjtRQUNoQyxJQUFJLElBR0g7S0FDRjtDQUNGO0FBTUQsTUFBTSxPQUFPLHlCQUF5Qjs7O1lBSnJDLFFBQVEsU0FBQztnQkFDUixPQUFPLEVBQUUsQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUN4QyxPQUFPLEVBQUUsQ0FBQyxZQUFZLENBQUM7YUFDeEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgUm91dGVzLCBSb3V0ZXJNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHsgVW5hdXRob3JpemVkQ29tcG9uZW50IH0gZnJvbSAnLi91bmF1dGhvcml6ZWQuY29tcG9uZW50JztcblxuY29uc3Qgcm91dGVzOiBSb3V0ZXMgPSBbXG4gIHtcbiAgICBwYXRoOiAndW5hdXRob3JpemVkJyxcbiAgICBjb21wb25lbnQ6IFVuYXV0aG9yaXplZENvbXBvbmVudCxcbiAgICBkYXRhOiB7XG4gICAgICBhbGxvd2VkVHlwZXM6IFsnbm9ybWFsJywgJ25vc2hlbGwnLCAnbm9zaGVsbG5vYnJlYWRjcnVtYiddLFxuICAgICAgZGVmYXVsdFR5cGU6ICdub3JtYWwnXG4gICAgfVxuICB9XG5dO1xuXG5ATmdNb2R1bGUoe1xuICBpbXBvcnRzOiBbUm91dGVyTW9kdWxlLmZvckNoaWxkKHJvdXRlcyldLFxuICBleHBvcnRzOiBbUm91dGVyTW9kdWxlXVxufSlcbmV4cG9ydCBjbGFzcyBVbmF1dGhvcml6ZWRSb3V0aW5nTW9kdWxlIHt9XG4iXX0=