/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Inject } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { AuthenticationService } from '../shared/auth/authentication/authentication.service';
import { UserService } from '../shared/auth/authentication/user.service';
import { map } from 'rxjs/operators';
import { ToastService } from '../layout/toast/toast.service';
import { PreviousRouteService } from '../components/previous-route.service';
import { IAuthenticationServiceToken } from '../shared/auth/authentication/abstraction/provider-authentication-service.token';
export class UnauthorizedComponent {
    /**
     * @param {?} authenticationService
     * @param {?} userService
     * @param {?} ngLocation
     * @param {?} toastService
     * @param {?} previousRouteService
     * @param {?} router
     * @param {?} providerAuthenticationService
     */
    constructor(authenticationService, userService, ngLocation, toastService, previousRouteService, router, providerAuthenticationService) {
        this.authenticationService = authenticationService;
        this.userService = userService;
        this.ngLocation = ngLocation;
        this.toastService = toastService;
        this.previousRouteService = previousRouteService;
        this.router = router;
        this.providerAuthenticationService = providerAuthenticationService;
        this.imageSrc = require('@cnj/uikit/lib/theme/uikit/images/error_403.png');
        this.isAuthenticated$ = this.userService.user$.pipe(map((/**
         * @param {?} user
         * @return {?}
         */
        (user) => user.authenticated)));
        this.isLoading = false;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (((/** @type {?} */ (this.providerAuthenticationService))).needRenewUser) {
            this.isLoading = true;
        }
        /** @type {?} */
        const loginQueryParam = this.router.routerState.root.snapshot.queryParamMap.get('login');
        if (loginQueryParam === 'auto' && (!((/** @type {?} */ (this.providerAuthenticationService))).needRenewUser)) {
            this.login();
        }
    }
    /**
     * @return {?}
     */
    login() {
        try {
            /** @type {?} */
            const paginaAnterior = this.previousRouteService.getPreviousUrl();
            this.authenticationService.login({ enderecoParaVoltar: paginaAnterior });
        }
        catch (error) {
            this.toastService.error('Error ao realizar o login!', (/** @type {?} */ (error)));
        }
    }
    /**
     * @return {?}
     */
    goBack() {
        this.ngLocation.back();
    }
}
UnauthorizedComponent.decorators = [
    { type: Component, args: [{
                selector: 'uikit-unauthorized',
                template: "<div class=\"container-fluid\">\n  <div class=\"row\">\n    <div class=\"col\">\n      <div *ngIf=\"isLoading\" class=\"error\">\n          <div class=\"corpo-carregamento\">\n              <div class=\"area-quadros\">\n                <span class=\"primeiro\"></span>\n                <span class=\"segundo\"></span>\n                <span class=\"terceiro\"></span>\n                <span class=\"quarto\"></span>\n              </div>\n              <h3>Carregando...</h3>\n          </div> \n      </div>\n      <div *ngIf=\"!isLoading\" class=\"error\">\n        <div *ngIf=\"!(isAuthenticated$ | async);else alreadyAuthenticated\">\n          <!--INICIO DA IMAGEM N\u00C3O AUTENTICADO-->\n          <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"280px\" height=\"306px\" viewBox=\"0 0 293.98 317.04\">\n            <g id=\"Layer_2\" data-name=\"Layer 2\">\n                <g id=\"Layer_2-2\" data-name=\"Layer 2\">\n                    <path class=\"autenticado-1\" d=\"M251.38,47.53l.11,260.28-175,1-.6-21.86s-.27-28.07-.57-61.18L75,185.84c-.31-42.58-.52-90.33-.26-114.1.1-10.09-2-17.41,33.91-22L215.15,32.36S243.68,23.77,251.38,47.53Z\" />\n                    <polygon class=\"autenticado-2\" points=\"201.01 34.69 75.42 273.18 75.42 236.63 180.55 38.02 201.01 34.69\" />\n                    <polygon class=\"autenticado-2\" points=\"252.28 93.71 135.56 308.69 76 308.8 81.8 282.38 212.52 32.82 251.49 41.63 252.28 93.71\" />\n                    <path class=\"autenticado-3\" d=\"M75.63,245.32s22.16,11.32,35.45,0,4.35-20.46-.1-22.4C106.75,221.05,89.27,218.79,75.63,245.32Z\" />\n                    <path class=\"autenticado-3\" d=\"M67.47,232.6s21.84-14.86,18-33-16.83-14.47-20.9-11.32C60.74,191.25,49.71,206.37,67.47,232.6Z\" />\n                    <path class=\"autenticado-3\" d=\"M55.15,222.23s-7-33.13-30.34-37.51S.34,198.67,1.19,204C2.19,210.18,15.4,230.48,55.15,222.23Z\" />\n                    <path class=\"autenticado-4\" d=\"M75,185.86c-.31-42.58-.52-90.33-.27-114.11.11-10.08-2-17.41,33.91-22L215.22,32.38\" />\n                    <path class=\"autenticado-4\" d=\"M76,286.93s-.27-28.06-.57-61.17\" />\n                    <polyline class=\"autenticado-4\" points=\"251.45 47.55 251.57 307.83 78.51 308.8\" />\n                    <ellipse class=\"autenticado-3\" cx=\"238.84\" cy=\"26.4\" rx=\"25.53\" ry=\"23.96\" transform=\"matrix(0.3, -0.95, 0.95, 0.3, 142.69, 246.62)\" />\n                    <path class=\"autenticado-5\" d=\"M231.69,21V14.85s0-3.38,5.75-3.38,7.75,0,7.75,3.13v9\" />\n                    <path class=\"autenticado-6\" d=\"M248.69,23.6l-21.25,1.62v11s.12,5,10.62,5,10.63-7.37,10.63-7.37Z\" />\n                    <line class=\"autenticado-6\" x1=\"238.06\" y1=\"29.73\" x2=\"238.06\" y2=\"35.91\" />\n                    <path class=\"autenticado-3\" d=\"M230,167.05,96,173.24a6.21,6.21,0,0,1-6.38-6.33v-8.42a6.24,6.24,0,0,1,5.84-6.33l134-6.19a6.21,6.21,0,0,1,6.38,6.33v8.42A6.24,6.24,0,0,1,230,167.05Z\" />\n                    <path class=\"autenticado-3\" d=\"M230,204,96,210.17a6.21,6.21,0,0,1-6.38-6.33v-8.41a6.25,6.25,0,0,1,5.84-6.34l134-6.18a6.21,6.21,0,0,1,6.38,6.33v8.41A6.24,6.24,0,0,1,230,204Z\" />\n                    <path class=\"autenticado-7\" d=\"M231.8,233l-67,3.65c-1.74.09-3.19-1.61-3.19-3.74v-5c0-2,1.29-3.65,2.92-3.74l67-3.65c1.74-.09,3.19,1.61,3.19,3.74v5C234.71,231.24,233.43,232.89,231.8,233Z\" />\n                    <line class=\"autenticado-4\" x1=\"157.95\" y1=\"245.69\" x2=\"242.1\" y2=\"241.07\" />\n                    <rect class=\"autenticado-3\" x=\"38.98\" y=\"286.93\" width=\"47.83\" height=\"6.58\" />\n                    <path class=\"autenticado-3\" d=\"M84.19,293.52c0,10-5.68,15.28-5.68,15.28-5.1,6.23-11.65,5.42-15.88,5.42s-10.78.81-15.89-5.42c0,0-5.68-5.24-5.68-15.28Z\" />\n                    <line class=\"autenticado-4\" x1=\"32.59\" y1=\"316.04\" x2=\"93.21\" y2=\"316.04\" />\n                    <polyline class=\"autenticado-4\" points=\"15.81 199.22 59.19 224.6 63.56 286.93 69.56 202.97\" />\n                    <line class=\"autenticado-4\" x1=\"66.97\" y1=\"248.48\" x2=\"89.63\" y2=\"240.22\" />\n                    <line class=\"autenticado-4\" x1=\"5.76\" y1=\"307.83\" x2=\"24.81\" y2=\"307.83\" />\n                    <line class=\"autenticado-4\" x1=\"31.54\" y1=\"307.83\" x2=\"45.93\" y2=\"307.83\" />\n                    <line class=\"autenticado-4\" x1=\"251.45\" y1=\"307.83\" x2=\"292.98\" y2=\"307.83\" />\n                    <path class=\"autenticado-7\" d=\"M139,138.24l6.9-.4v1.58l-8.88.51-.07-14.87,2-.11Z\" />\n                    <path class=\"autenticado-7\" d=\"M147.22,133.71a6.44,6.44,0,0,1,1.28-4.11,4.56,4.56,0,0,1,3.53-1.79,4.14,4.14,0,0,1,3.57,1.38,5.85,5.85,0,0,1,1.32,4v.22a6.42,6.42,0,0,1-1.28,4.12,4.62,4.62,0,0,1-3.54,1.78,4.15,4.15,0,0,1-3.56-1.37,5.87,5.87,0,0,1-1.32-4Zm2,.11a5.16,5.16,0,0,0,.74,2.86,2.28,2.28,0,0,0,2.17,1,2.54,2.54,0,0,0,2.13-1.27,5.39,5.39,0,0,0,.72-2.94v-.22a5.15,5.15,0,0,0-.75-2.85,2.29,2.29,0,0,0-2.17-1,2.53,2.53,0,0,0-2.13,1.28,5.46,5.46,0,0,0-.71,2.93Z\" />\n                    <path class=\"autenticado-7\" d=\"M158.91,133.35A7.63,7.63,0,0,1,160,129a3.86,3.86,0,0,1,3.15-1.81,3.54,3.54,0,0,1,1.81.32,3.32,3.32,0,0,1,1.31,1.14l.23-1.45,1.57-.09.06,11.11a4.5,4.5,0,0,1-1.2,3.34,5.2,5.2,0,0,1-3.49,1.34,6.73,6.73,0,0,1-1.69-.13,6.4,6.4,0,0,1-1.6-.5l.5-1.59a4.93,4.93,0,0,0,1.28.42,6.22,6.22,0,0,0,1.48.1,3,3,0,0,0,2.1-.79,2.92,2.92,0,0,0,.64-2.07v-1.25a3.76,3.76,0,0,1-1.27,1.12,4.15,4.15,0,0,1-1.7.45,3.53,3.53,0,0,1-3.14-1.29,5.8,5.8,0,0,1-1.16-3.79Zm2,.1a4.49,4.49,0,0,0,.71,2.64,2.24,2.24,0,0,0,2.1.89,2.68,2.68,0,0,0,1.49-.5,3.41,3.41,0,0,0,1-1.23l0-5.09a2.89,2.89,0,0,0-1-1.06,2.52,2.52,0,0,0-1.48-.33,2.44,2.44,0,0,0-2.1,1.33,6.21,6.21,0,0,0-.69,3.14Z\" />\n                    <path class=\"autenticado-7\" d=\"M173.09,124l-2,.12V122l2-.11Zm.07,13.88-2,.12-.05-11,2-.12Z\" />\n                    <path class=\"autenticado-7\" d=\"M177.9,126.53l.15,1.63a4.45,4.45,0,0,1,1.35-1.44,3.75,3.75,0,0,1,1.86-.59A3.34,3.34,0,0,1,184,127a4.45,4.45,0,0,1,1,3.18l0,7-2,.12,0-6.94a2.94,2.94,0,0,0-.57-2,2.13,2.13,0,0,0-1.74-.51,2.86,2.86,0,0,0-1.5.5,3.31,3.31,0,0,0-1,1.21l0,8.06-2,.11-.05-11.05Z\" />\n                    <path class=\"autenticado-8\" d=\"M189.48,230.88l2.73-.15v.62l-3.51.2,0-5.87.78-.05Z\" />\n                    <path class=\"autenticado-8\" d=\"M192.74,229.1a2.58,2.58,0,0,1,.51-1.63,1.82,1.82,0,0,1,1.39-.71,1.66,1.66,0,0,1,1.41.55,2.29,2.29,0,0,1,.52,1.57V229a2.53,2.53,0,0,1-.51,1.62,1.84,1.84,0,0,1-1.4.71,1.66,1.66,0,0,1-1.41-.55,2.29,2.29,0,0,1-.52-1.56Zm.78,0a2,2,0,0,0,.29,1.13.9.9,0,0,0,.86.4,1,1,0,0,0,.84-.5,2.19,2.19,0,0,0,.29-1.16v-.09a2,2,0,0,0-.29-1.12.89.89,0,0,0-.86-.41,1,1,0,0,0-.84.5,2.22,2.22,0,0,0-.28,1.16Z\" />\n                    <path class=\"autenticado-8\" d=\"M197.36,229a3,3,0,0,1,.44-1.72,1.52,1.52,0,0,1,1.24-.72,1.47,1.47,0,0,1,.72.13,1.32,1.32,0,0,1,.52.45l.09-.57.62,0,0,4.39a1.74,1.74,0,0,1-.47,1.32,2.07,2.07,0,0,1-1.38.53,3.21,3.21,0,0,1-.67-.05,2.74,2.74,0,0,1-.63-.2l.2-.63a2.09,2.09,0,0,0,.5.17,2.38,2.38,0,0,0,.59,0,1.2,1.2,0,0,0,.83-.31,1.14,1.14,0,0,0,.25-.82v-.5a1.53,1.53,0,0,1-.5.45,1.58,1.58,0,0,1-.67.17,1.39,1.39,0,0,1-1.24-.51,2.23,2.23,0,0,1-.46-1.49Zm.78,0a1.74,1.74,0,0,0,.28,1,.9.9,0,0,0,.83.35,1.12,1.12,0,0,0,.59-.2,1.33,1.33,0,0,0,.39-.48v-2a1.08,1.08,0,0,0-.4-.42.91.91,0,0,0-.58-.13,1,1,0,0,0-.83.52,2.56,2.56,0,0,0-.27,1.24Z\" />\n                    <path class=\"autenticado-8\" d=\"M203,225.25l-.78,0v-.81l.78,0Zm0,5.48-.78.05,0-4.37.78,0Z\" />\n                    <path class=\"autenticado-8\" d=\"M204.86,226.26l.06.64a1.78,1.78,0,0,1,.53-.56,1.55,1.55,0,0,1,.74-.24,1.33,1.33,0,0,1,1.07.35,1.73,1.73,0,0,1,.39,1.26v2.76l-.78,0v-2.74a1.17,1.17,0,0,0-.23-.81.85.85,0,0,0-.68-.2,1.13,1.13,0,0,0-.6.2,1.28,1.28,0,0,0-.41.48l0,3.18-.78,0,0-4.36Z\" />\n                </g>\n            </g>\n           </svg>\n          <!--FIM DA IMAGEM N\u00C3O AUTENTICADO-->\n          <h3><b>Voc\u00EA n\u00E3o est\u00E1 autenticado!</b></h3>\n          <button mat-raised-button color=\"primary\" (click)=\"login()\">Fazer Login</button>\n        </div>\n        <ng-template #alreadyAuthenticated>\n          <!-- IMAGEM N\u00C3O TEM PERMISS\u00C3O-->\n          <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"280px\" height=\"306px\" viewBox=\"0 0 293.98 317.04\">\n            <g id=\"Layer_2\" data-name=\"Layer 2\">\n                <g id=\"Layer_2-2\" data-name=\"Layer 2\">\n                    <path class=\"naoautenticado-1\" d=\"M251.38,47.53l.11,260.28-175,1-.6-21.86s-.27-28.07-.57-61.18L75,185.84c-.31-42.58-.52-90.33-.26-114.1.1-10.09-2-17.41,33.91-22L215.15,32.36S243.68,23.77,251.38,47.53Z\" />\n                    <polygon class=\"naoautenticado-2\" points=\"201.01 34.69 75.42 273.18 75.42 236.63 180.55 38.02 201.01 34.69\" />\n                    <polygon class=\"naoautenticado-2\" points=\"252.28 93.71 135.56 308.69 76 308.8 81.8 282.38 212.52 32.82 251.49 41.63 252.28 93.71\" />\n                    <path class=\"naoautenticado-3\" d=\"M75.63,245.32s22.16,11.32,35.45,0,4.35-20.46-.1-22.4C106.75,221.05,89.27,218.79,75.63,245.32Z\" />\n                    <path class=\"naoautenticado-3\" d=\"M67.47,232.6s21.84-14.86,18-33-16.83-14.47-20.9-11.32C60.74,191.25,49.71,206.37,67.47,232.6Z\" />\n                    <path class=\"naoautenticado-3\" d=\"M55.15,222.23s-7-33.13-30.34-37.51S.34,198.67,1.19,204C2.19,210.18,15.4,230.48,55.15,222.23Z\" />\n                    <path class=\"naoautenticado-4\" d=\"M75,185.86c-.31-42.58-.52-90.33-.27-114.11.11-10.08-2-17.41,33.91-22L215.22,32.38\" />\n                    <path class=\"naoautenticado-4\" d=\"M76,286.93s-.27-28.06-.57-61.17\" />\n                    <polyline class=\"naoautenticado-4\" points=\"251.45 47.55 251.57 307.83 78.51 308.8\" />\n                    <ellipse class=\"naoautenticado-3\" cx=\"238.84\" cy=\"26.4\" rx=\"25.53\" ry=\"23.96\" transform=\"matrix(0.3, -0.95, 0.95, 0.3, 142.69, 246.62)\" />\n                    <path class=\"naoautenticado-5\" d=\"M231.69,21V14.85s0-3.38,5.75-3.38,7.75,0,7.75,3.13v9\" />\n                    <path class=\"naoautenticado-6\" d=\"M248.69,23.6l-21.25,1.62v11s.12,5,10.62,5,10.63-7.37,10.63-7.37Z\" />\n                    <line class=\"naoautenticado-6\" x1=\"238.06\" y1=\"29.73\" x2=\"238.06\" y2=\"35.91\" />\n                    <rect class=\"naoautenticado-3\" x=\"38.98\" y=\"286.93\" width=\"47.83\" height=\"6.58\" />\n                    <path class=\"naoautenticado-3\" d=\"M84.19,293.52c0,10-5.68,15.28-5.68,15.28-5.1,6.23-11.65,5.42-15.88,5.42s-10.78.81-15.89-5.42c0,0-5.68-5.24-5.68-15.28Z\" />\n                    <line class=\"naoautenticado-4\" x1=\"32.59\" y1=\"316.04\" x2=\"93.21\" y2=\"316.04\" />\n                    <polyline class=\"naoautenticado-4\" points=\"15.81 199.22 59.19 224.6 63.56 286.93 69.56 202.97\" />\n                    <line class=\"naoautenticado-4\" x1=\"66.97\" y1=\"248.48\" x2=\"89.63\" y2=\"240.22\" />\n                    <line class=\"naoautenticado-4\" x1=\"5.76\" y1=\"307.83\" x2=\"24.81\" y2=\"307.83\" />\n                    <line class=\"naoautenticado-4\" x1=\"31.54\" y1=\"307.83\" x2=\"45.93\" y2=\"307.83\" />\n                    <line class=\"naoautenticado-4\" x1=\"251.45\" y1=\"307.83\" x2=\"292.98\" y2=\"307.83\" />\n                    <path class=\"naoautenticado-7\" d=\"M169.64,103.69a67,67,0,1,0,67,67A67.07,67.07,0,0,0,169.64,103.69Zm0,117.23a50.3,50.3,0,0,1-50.25-50.24,49.65,49.65,0,0,1,9.3-29.1l70,70A49.55,49.55,0,0,1,169.64,220.92Zm40.94-21.14-70-70a49.55,49.55,0,0,1,29.1-9.3,50.3,50.3,0,0,1,50.24,50.24A49.62,49.62,0,0,1,210.58,199.78Z\" />\n                    <path class=\"naoautenticado-8\" d=\"M166.57,102a67,67,0,1,0,67,67A67.07,67.07,0,0,0,166.57,102Zm0,117.24A50.3,50.3,0,0,1,116.33,169a49.65,49.65,0,0,1,9.3-29.1l70,70.05A49.62,49.62,0,0,1,166.57,219.2Zm40.94-21.15-70-70a49.65,49.65,0,0,1,29.1-9.3A50.3,50.3,0,0,1,216.82,169,49.56,49.56,0,0,1,207.51,198.05Z\" />\n                </g>\n            </g>\n          </svg>\n          <!-- FIM DA IMAGEM N\u00C3O TEM PERMISS\u00C3O-->\n          <h3>Voc\u00EA n\u00E3o possui <b>permiss\u00E3o</b> para acessar esta p\u00E1gina!</h3>\n          <button mat-raised-button color=\"warn\" (click)=\"goBack()\">Clique aqui para voltar</button>\n        </ng-template>\n      </div>\n    </div>\n  </div>\n</div>\n",
                styles: ["@charset \"UTF-8\";.autenticado-1{fill:#edf3ff}.autenticado-2,.autenticado-3{fill:#d9e4fc}.autenticado-2{opacity:.15}.autenticado-3,.autenticado-4,.autenticado-5,.autenticado-6{stroke:#004bcb;stroke-miterlimit:10}.autenticado-3,.autenticado-4,.autenticado-6{stroke-linecap:round}.autenticado-3,.autenticado-4{stroke-width:2px}.autenticado-4,.autenticado-5,.autenticado-6{fill:none}.autenticado-5,.autenticado-6{stroke-width:3px}.autenticado-7{fill:#004bcb}.autenticado-8{fill:#fff}.mat-raised-button.mat-warn{background-color:#fb3e54}.naoautenticado-1,.naoautenticado-3,.naoautenticado-7{fill:#edf3ff}.naoautenticado-2{opacity:.15;fill:#fb3e54}.naoautenticado-3,.naoautenticado-4,.naoautenticado-5,.naoautenticado-6{stroke:#fb3e54;stroke-miterlimit:10}.naoautenticado-3,.naoautenticado-4,.naoautenticado-6{stroke-linecap:round}.naoautenticado-3,.naoautenticado-4{stroke-width:2px}.naoautenticado-4,.naoautenticado-5,.naoautenticado-6{fill:none}.naoautenticado-5,.naoautenticado-6{stroke-width:3px}.naoautenticado-8{fill:#fb3e54}.area-quadros{width:50px;height:50px;display:inline-block;-webkit-transform:rotate(45deg);transform:rotate(45deg);font-size:0}.area-quadros span{position:relative;width:25px;height:25px;-webkit-transform:scale(1.1);transform:scale(1.1);display:inline-block}.area-quadros span::before{content:'';background-color:#015efd;position:absolute;left:0;top:0;display:block;width:25px;height:25px;-webkit-transform-origin:100% 100%;transform-origin:100% 100%;-webkit-animation:2.5s linear infinite both folding;animation:2.5s linear infinite both folding}.area-quadros .segundo{-webkit-transform:rotateZ(90deg) scale(1.1);transform:rotateZ(90deg) scale(1.1)}.area-quadros .segundo::before{-webkit-animation-delay:.3s;animation-delay:.3s;background-color:#004bcb}.area-quadros .terceiro{-webkit-transform:rotateZ(270deg) scale(1.1);transform:rotateZ(270deg) scale(1.1)}.area-quadros .terceiro::before{-webkit-animation-delay:.9s;animation-delay:.9s;background-color:#0041af}.area-quadros .quarto{-webkit-transform:rotateZ(180deg) scale(1.1);transform:rotateZ(180deg) scale(1.1)}.area-quadros .quarto::before{-webkit-animation-delay:.6s;animation-delay:.6s;background-color:#003795}@-webkit-keyframes folding{0%,10%{-webkit-transform:perspective(140px) rotateX(-180deg);transform:perspective(140px) rotateX(-180deg);opacity:0}25%,75%{-webkit-transform:perspective(140px) rotateX(0);transform:perspective(140px) rotateX(0);opacity:1}100%,90%{-webkit-transform:perspective(140px) rotateY(180deg);transform:perspective(140px) rotateY(180deg);opacity:0}}@keyframes folding{0%,10%{-webkit-transform:perspective(140px) rotateX(-180deg);transform:perspective(140px) rotateX(-180deg);opacity:0}25%,75%{-webkit-transform:perspective(140px) rotateX(0);transform:perspective(140px) rotateX(0);opacity:1}100%,90%{-webkit-transform:perspective(140px) rotateY(180deg);transform:perspective(140px) rotateY(180deg);opacity:0}}.corpo-carregamento{position:fixed;left:50%;top:50%;margin-top:-50px;margin-left:-50px;width:100px;height:100px;text-align:center;color:#01286b}"]
            }] }
];
/** @nocollapse */
UnauthorizedComponent.ctorParameters = () => [
    { type: AuthenticationService },
    { type: UserService },
    { type: Location },
    { type: ToastService },
    { type: PreviousRouteService },
    { type: Router },
    { type: undefined, decorators: [{ type: Inject, args: [IAuthenticationServiceToken,] }] }
];
if (false) {
    /** @type {?} */
    UnauthorizedComponent.prototype.imageSrc;
    /** @type {?} */
    UnauthorizedComponent.prototype.isAuthenticated$;
    /** @type {?} */
    UnauthorizedComponent.prototype.isLoading;
    /**
     * @type {?}
     * @private
     */
    UnauthorizedComponent.prototype.authenticationService;
    /**
     * @type {?}
     * @private
     */
    UnauthorizedComponent.prototype.userService;
    /**
     * @type {?}
     * @private
     */
    UnauthorizedComponent.prototype.ngLocation;
    /**
     * @type {?}
     * @private
     */
    UnauthorizedComponent.prototype.toastService;
    /**
     * @type {?}
     * @private
     */
    UnauthorizedComponent.prototype.previousRouteService;
    /**
     * @type {?}
     * @private
     */
    UnauthorizedComponent.prototype.router;
    /**
     * @type {?}
     * @private
     */
    UnauthorizedComponent.prototype.providerAuthenticationService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidW5hdXRob3JpemVkLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BjbmovdWlraXQvIiwic291cmNlcyI6WyJsaWIvdW5hdXRob3JpemVkL3VuYXV0aG9yaXplZC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBMEIsU0FBUyxFQUFVLE1BQU0sRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUNqRixPQUFPLEVBQUMsUUFBUSxFQUFDLE1BQU0saUJBQWlCLENBQUM7QUFDekMsT0FBTyxFQUFDLE1BQU0sRUFBQyxNQUFNLGlCQUFpQixDQUFDO0FBQ3ZDLE9BQU8sRUFBQyxxQkFBcUIsRUFBQyxNQUFNLHNEQUFzRCxDQUFDO0FBQzNGLE9BQU8sRUFBQyxXQUFXLEVBQUMsTUFBTSw0Q0FBNEMsQ0FBQztBQUN2RSxPQUFPLEVBQUMsR0FBRyxFQUFDLE1BQU0sZ0JBQWdCLENBQUM7QUFDbkMsT0FBTyxFQUFDLFlBQVksRUFBQyxNQUFNLCtCQUErQixDQUFDO0FBQzNELE9BQU8sRUFBQyxvQkFBb0IsRUFBQyxNQUFNLHNDQUFzQyxDQUFDO0FBQzFFLE9BQU8sRUFBRSwyQkFBMkIsRUFBRSxNQUFNLGlGQUFpRixDQUFDO0FBVzlILE1BQU0sT0FBTyxxQkFBcUI7Ozs7Ozs7Ozs7SUFJaEMsWUFDVSxxQkFBNEMsRUFDNUMsV0FBd0IsRUFDeEIsVUFBb0IsRUFDcEIsWUFBMEIsRUFDMUIsb0JBQTBDLEVBQzFDLE1BQWMsRUFDdUIsNkJBQTZEO1FBTmxHLDBCQUFxQixHQUFyQixxQkFBcUIsQ0FBdUI7UUFDNUMsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsZUFBVSxHQUFWLFVBQVUsQ0FBVTtRQUNwQixpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUMxQix5QkFBb0IsR0FBcEIsb0JBQW9CLENBQXNCO1FBQzFDLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDdUIsa0NBQTZCLEdBQTdCLDZCQUE2QixDQUFnQztRQVY1RyxhQUFRLEdBQUcsT0FBTyxDQUFDLGlEQUFpRCxDQUFDLENBQUM7UUFDL0QscUJBQWdCLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUc7Ozs7UUFBQyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBQyxDQUFDLENBQUM7UUFDbEYsY0FBUyxHQUFHLEtBQUssQ0FBQztJQVV6QixDQUFDOzs7O0lBRUQsUUFBUTtRQUNOLElBQUksQ0FBQyxtQkFBQSxJQUFJLENBQUMsNkJBQTZCLEVBQTZCLENBQUMsQ0FBQyxhQUFhLEVBQUU7WUFDbkYsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7U0FDdkI7O2NBRUssZUFBZSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUM7UUFDeEYsSUFBSSxlQUFlLEtBQUssTUFBTSxJQUFJLENBQUMsQ0FBQyxDQUFDLG1CQUFBLElBQUksQ0FBQyw2QkFBNkIsRUFBNkIsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxFQUFFO1lBQ3BILElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztTQUNkO0lBQ0gsQ0FBQzs7OztJQUVNLEtBQUs7UUFDUixJQUFJOztrQkFDSSxjQUFjLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGNBQWMsRUFBRTtZQUNqRSxJQUFJLENBQUMscUJBQXFCLENBQUMsS0FBSyxDQUFDLEVBQUMsa0JBQWtCLEVBQUUsY0FBYyxFQUFDLENBQUMsQ0FBQztTQUN4RTtRQUFDLE9BQU8sS0FBSyxFQUFFO1lBQ2QsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsNEJBQTRCLEVBQUUsbUJBQUEsS0FBSyxFQUFTLENBQUMsQ0FBQztTQUN2RTtJQUNMLENBQUM7Ozs7SUFFTSxNQUFNO1FBQ1gsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUN6QixDQUFDOzs7WUExQ0YsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxvQkFBb0I7Z0JBQzlCLDBrWUFBNEM7O2FBRTdDOzs7O1lBZk8scUJBQXFCO1lBQ3JCLFdBQVc7WUFIWCxRQUFRO1lBS1IsWUFBWTtZQUNaLG9CQUFvQjtZQUxwQixNQUFNOzRDQTRCVCxNQUFNLFNBQUMsMkJBQTJCOzs7O0lBVnJDLHlDQUFzRTs7SUFDdEUsaURBQXlGOztJQUN6RiwwQ0FBeUI7Ozs7O0lBRXZCLHNEQUFvRDs7Ozs7SUFDcEQsNENBQWdDOzs7OztJQUNoQywyQ0FBNEI7Ozs7O0lBQzVCLDZDQUFrQzs7Ozs7SUFDbEMscURBQWtEOzs7OztJQUNsRCx1Q0FBc0I7Ozs7O0lBQ3RCLDhEQUEwRyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3ksIENvbXBvbmVudCwgT25Jbml0LCBJbmplY3R9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtMb2NhdGlvbn0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7Um91dGVyfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHtBdXRoZW50aWNhdGlvblNlcnZpY2V9IGZyb20gJy4uL3NoYXJlZC9hdXRoL2F1dGhlbnRpY2F0aW9uL2F1dGhlbnRpY2F0aW9uLnNlcnZpY2UnO1xuaW1wb3J0IHtVc2VyU2VydmljZX0gZnJvbSAnLi4vc2hhcmVkL2F1dGgvYXV0aGVudGljYXRpb24vdXNlci5zZXJ2aWNlJztcbmltcG9ydCB7bWFwfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQge1RvYXN0U2VydmljZX0gZnJvbSAnLi4vbGF5b3V0L3RvYXN0L3RvYXN0LnNlcnZpY2UnO1xuaW1wb3J0IHtQcmV2aW91c1JvdXRlU2VydmljZX0gZnJvbSAnLi4vY29tcG9uZW50cy9wcmV2aW91cy1yb3V0ZS5zZXJ2aWNlJztcbmltcG9ydCB7IElBdXRoZW50aWNhdGlvblNlcnZpY2VUb2tlbiB9IGZyb20gJy4uL3NoYXJlZC9hdXRoL2F1dGhlbnRpY2F0aW9uL2Fic3RyYWN0aW9uL3Byb3ZpZGVyLWF1dGhlbnRpY2F0aW9uLXNlcnZpY2UudG9rZW4nO1xuaW1wb3J0IHsgSVByb3ZpZGVyQXV0aGVudGljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vc2hhcmVkL2F1dGgvYXV0aGVudGljYXRpb24vYWJzdHJhY3Rpb24vcHJvdmlkZXItYXV0aGVudGljYXRpb24uc2VydmljZSc7XG5pbXBvcnQge09pZGNBdXRoZW50aWNhdGlvblNlcnZpY2V9IGZyb20gJy4uL3NoYXJlZC9hdXRoL2F1dGhlbnRpY2F0aW9uLW9pZGMvb2lkYy1hdXRoZW50aWNhdGlvbi5zZXJ2aWNlJztcblxuZGVjbGFyZSBmdW5jdGlvbiByZXF1aXJlKHBhdGg6IHN0cmluZyk7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ3Vpa2l0LXVuYXV0aG9yaXplZCcsXG4gIHRlbXBsYXRlVXJsOiAnLi91bmF1dGhvcml6ZWQuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi91bmF1dGhvcml6ZWQuY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBVbmF1dGhvcml6ZWRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBpbWFnZVNyYyA9IHJlcXVpcmUoJ0BjbmovdWlraXQvbGliL3RoZW1lL3Vpa2l0L2ltYWdlcy9lcnJvcl80MDMucG5nJyk7XG4gIHB1YmxpYyBpc0F1dGhlbnRpY2F0ZWQkID0gdGhpcy51c2VyU2VydmljZS51c2VyJC5waXBlKG1hcCgodXNlcikgPT4gdXNlci5hdXRoZW50aWNhdGVkKSk7XG4gIHB1YmxpYyBpc0xvYWRpbmcgPSBmYWxzZTtcbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSBhdXRoZW50aWNhdGlvblNlcnZpY2U6IEF1dGhlbnRpY2F0aW9uU2VydmljZSxcbiAgICBwcml2YXRlIHVzZXJTZXJ2aWNlOiBVc2VyU2VydmljZSxcbiAgICBwcml2YXRlIG5nTG9jYXRpb246IExvY2F0aW9uLFxuICAgIHByaXZhdGUgdG9hc3RTZXJ2aWNlOiBUb2FzdFNlcnZpY2UsXG4gICAgcHJpdmF0ZSBwcmV2aW91c1JvdXRlU2VydmljZTogUHJldmlvdXNSb3V0ZVNlcnZpY2UsXG4gICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcbiAgICBASW5qZWN0KElBdXRoZW50aWNhdGlvblNlcnZpY2VUb2tlbikgcHJpdmF0ZSBwcm92aWRlckF1dGhlbnRpY2F0aW9uU2VydmljZTogSVByb3ZpZGVyQXV0aGVudGljYXRpb25TZXJ2aWNlLFxuICApIHtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIGlmICgodGhpcy5wcm92aWRlckF1dGhlbnRpY2F0aW9uU2VydmljZSBhcyBPaWRjQXV0aGVudGljYXRpb25TZXJ2aWNlKS5uZWVkUmVuZXdVc2VyKSB7XG4gICAgICB0aGlzLmlzTG9hZGluZyA9IHRydWU7XG4gICAgfVxuXG4gICAgY29uc3QgbG9naW5RdWVyeVBhcmFtID0gdGhpcy5yb3V0ZXIucm91dGVyU3RhdGUucm9vdC5zbmFwc2hvdC5xdWVyeVBhcmFtTWFwLmdldCgnbG9naW4nKTtcbiAgICBpZiAobG9naW5RdWVyeVBhcmFtID09PSAnYXV0bycgJiYgKCEodGhpcy5wcm92aWRlckF1dGhlbnRpY2F0aW9uU2VydmljZSBhcyBPaWRjQXV0aGVudGljYXRpb25TZXJ2aWNlKS5uZWVkUmVuZXdVc2VyKSkge1xuICAgICAgdGhpcy5sb2dpbigpO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyBsb2dpbigpIHtcbiAgICAgIHRyeSB7XG4gICAgICAgIGNvbnN0IHBhZ2luYUFudGVyaW9yID0gdGhpcy5wcmV2aW91c1JvdXRlU2VydmljZS5nZXRQcmV2aW91c1VybCgpO1xuICAgICAgICB0aGlzLmF1dGhlbnRpY2F0aW9uU2VydmljZS5sb2dpbih7ZW5kZXJlY29QYXJhVm9sdGFyOiBwYWdpbmFBbnRlcmlvcn0pO1xuICAgICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgICAgdGhpcy50b2FzdFNlcnZpY2UuZXJyb3IoJ0Vycm9yIGFvIHJlYWxpemFyIG8gbG9naW4hJywgZXJyb3IgYXMgRXJyb3IpO1xuICAgICAgfVxuICB9XG5cbiAgcHVibGljIGdvQmFjaygpIHtcbiAgICB0aGlzLm5nTG9jYXRpb24uYmFjaygpO1xuICB9XG5cbn1cbiJdfQ==